# vINCI Platform License Notice

Do Not Translate or Localize

Notice that we at Comtrade 360 are licensing this software solution under the MIT license ONLY for the source code we have written. We are not licensing any other trademark or licensed property that might in any way be included in the final binary files.

The RIGHTS of any third-party solution in the form of dependencies, libraries, resources, assets and trademarked properties that might be included by any extension are THEIR OWN under the licenses the third-party developers might have provided if any.

In ADDITION, Comtarde 360 is not responsible for the development, distribution or maintenance of any of the third-party dependencies, library, resources, assets, trademarked properties that are linked with this project's source code files.

We have carefully investigated our own solution under this project as to not violate any of the third-party property licenses that might be associated with this project. In case of any emerging issues regarding third-party property licenses, Comtrade 360 will exercise their RIGHT to update the licensing information provided with this solution.
