package com.upb.vinci.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller for receiving data from camera.
 */
@RestController
@RequestMapping("/camera")
public class CameraResource {

    @GetMapping("/fitness")
    public String test() {
        return  "WORK";
    }
}
