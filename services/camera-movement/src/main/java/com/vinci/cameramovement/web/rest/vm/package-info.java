/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.cameramovement.web.rest.vm;
