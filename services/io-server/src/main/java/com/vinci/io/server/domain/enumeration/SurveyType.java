package com.vinci.io.server.domain.enumeration;

/**
 * The SurveyType enumeration.
 */
public enum SurveyType {
    WHOQOL_BREF, IPAQ, D_VAMS, USER_NEEDS, DIGITAL_SKILLS, FEELINGS,ELDERLY
}
