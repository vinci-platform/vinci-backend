package com.vinci.io.server.service.mapper;

import com.vinci.io.server.domain.ShoeData;
import com.vinci.io.server.service.dto.CreateShoeDataDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Mapper for the entity Device and its DTO DeviceDTO.
 */
@Service
public class ShoeDataMapper implements EntityMapper<CreateShoeDataDto, ShoeData> {

    @Override
    public ShoeData toEntity(CreateShoeDataDto dto, Long newDeviceId) {
        ShoeData shoeData = new ShoeData();
        shoeData.setId(null);
        shoeData.setData(dto.getData());
        shoeData.setTimestamp(dto.getTimestamp());
        shoeData.setDeviceId(newDeviceId); //id-ul luat din gateway
        return shoeData;
    }

    @Override
    public CreateShoeDataDto toDto(ShoeData entity) {
        return null;
    }

    @Override
    public List<ShoeData> toEntity(List<CreateShoeDataDto> dtoList) {
        return null;
    }

    @Override
    public List<CreateShoeDataDto> toDto(List<ShoeData> entityList) {
        return null;
    }
}
