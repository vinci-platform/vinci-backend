package com.vinci.io.server.service.kafka.util;

import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
public class FitbitWatchDataListenableFutureCallback implements ListenableFutureCallback<SendResult<String, FitbitWatchDataDTO>> {
    @Override
    public void onFailure(Throwable ex) {
        handleFailure(ex);
    }

    @Override
    public void onSuccess(SendResult<String, FitbitWatchDataDTO> result) {
        handleSuccess(result);
    }

    private void handleFailure(Throwable ex) {
        log.error("Error Sending the Message and the exception is {}",ex.getMessage());
        try {
            throw ex;
        } catch (Throwable throwable) {
            log.error("Error in OnFailure: {}", throwable.getMessage());
        }
    }

    private void handleSuccess(SendResult<String, FitbitWatchDataDTO> result) {
        log.debug("Message Sent Successfully for key : {} and value is {} , partitions is {},"
            ,result.getProducerRecord().key(),result.getProducerRecord().value(),result.getRecordMetadata().partition());
    }
}
