package com.vinci.io.server.error;

import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserExtraForUserIdException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_EXTRA_FOR_USER_ID";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "No userExtra found for userId";
    private static final String DETAILS_NO_USER_ID = "No userExtra found for userId: %s";

    public NoUserExtraForUserIdException(){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS);
    }
    public NoUserExtraForUserIdException(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }
    public NoUserExtraForUserIdException(Long userId) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,String.format(DETAILS_NO_USER_ID,userId)); }
    public NoUserExtraForUserIdException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoUserExtraForUserIdException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
