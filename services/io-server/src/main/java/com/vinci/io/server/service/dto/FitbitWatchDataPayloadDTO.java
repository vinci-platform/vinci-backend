package com.vinci.io.server.service.dto;

public class FitbitWatchDataPayloadDTO {

    private Long id;

    private FitbitWatchSleepDataDTO sleepData;

    private FitbitWatchActivityDataDTO activityData;

    private Long timestamp;

    private DeviceDTO device;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FitbitWatchSleepDataDTO getSleepData() {
        return sleepData;
    }

    public void setSleepData(FitbitWatchSleepDataDTO sleepData) {
        this.sleepData = sleepData;
    }

    public FitbitWatchActivityDataDTO getActivityData() {
        return activityData;
    }

    public void setActivityData(FitbitWatchActivityDataDTO activityData) {
        this.activityData = activityData;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public DeviceDTO getDevice() {
        return device;
    }

    public void setDevice(DeviceDTO device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchDataPayloadDTO)) {
            return false;
        }

        return id != null && id.equals(((FitbitWatchDataPayloadDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "FitbitWatchDataPayloadDTO{" +
            "id=" + id +
            ", sleepData=" + sleepData +
            ", activityData=" + activityData +
            ", timestamp=" + timestamp +
            ", device=" + device +
            '}';
    }
}
