package com.vinci.io.server.service;

import com.vinci.io.server.domain.SurveyData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.NoDeviceFoundException;
import com.vinci.io.server.error.NoEntityException;
import com.vinci.io.server.repository.SurveyDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Service Implementation for managing SurveyData.
 */
@Service
@Transactional
public class SurveyDataService {

    private static final String ENTITY_NAME = "ioserverSurveyData";
    private final DeviceType ENTITY_DEVICE_TYPE = DeviceType.SURVEY;

    private final Logger log = LoggerFactory.getLogger(SurveyDataService.class);

    private final SurveyDataRepository surveyDataRepository;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public SurveyDataService(SurveyDataRepository surveyDataRepository, DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.surveyDataRepository = surveyDataRepository;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Just saves a surveyData.
     *
     * @param surveyData the entity to save
     * @return the persisted entity
     */
    public SurveyData justSave(SurveyData surveyData) {
        log.debug("Request to save SurveyData : {}", surveyData);
        return surveyDataRepository.save(surveyData);
    }

    /**
     * Save a surveyData.
     *
     * @param surveyData the entity to save
     * @return the persisted entity or null if exception
     */
    public SurveyData save(SurveyData surveyData) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", surveyData);

        validateSurveyDataForCreation(surveyData);

        Set<DeviceDTO> surveyDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (surveyDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, surveyData.getDeviceId());
        }

        deviceServiceUtility.getByDeviceIdAndTypeFromSet(surveyDevicesForCurrentUser,surveyData.getDeviceId(),ENTITY_DEVICE_TYPE);

        return surveyDataRepository.save(surveyData);
    }

    /**
     * Update a surveyData.
     *
     * @param surveyData the entity to save
     * @return the persisted entity or null if exception
     */
    public SurveyData update(SurveyData surveyData) {
        log.debug("Request to update "+ENTITY_NAME+" : {}", surveyData);

        validateSurveyDataForUpdate(surveyData);

        Set<DeviceDTO> surveyDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (surveyDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, surveyData.getDeviceId());
        }

        deviceServiceUtility.getByDeviceIdAndTypeFromSet(surveyDevicesForCurrentUser,surveyData.getDeviceId(),ENTITY_DEVICE_TYPE);

        return surveyDataRepository.save(surveyData);
    }

    /**
     * Get one surveyData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SurveyData findOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" : {}", id);

        Set<DeviceDTO> surveyDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (surveyDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        SurveyData surveyData = surveyDataRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        deviceServiceUtility.validateDevices(id, surveyDevicesForCurrentUser, surveyData.getDeviceId(), ENTITY_NAME, ENTITY_DEVICE_TYPE);

        return surveyData;
    }

    /**
     * Delete the surveyData by id.
     *
     * @param id the id of the entity
     */
    public void justDelete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);
        surveyDataRepository.deleteById(id);
    }

    private void validateSurveyDataForCreation(SurveyData surveyData) {
        if (surveyData.getId() != null) {
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" cannot already have an ID", ENTITY_NAME, "idexists");
        }
    }

    private void validateSurveyDataForUpdate(SurveyData surveyData) {
        if (surveyData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }
}
