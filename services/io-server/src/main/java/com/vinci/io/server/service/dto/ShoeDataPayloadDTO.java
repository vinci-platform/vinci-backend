package com.vinci.io.server.service.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Device entity.
 */

public class ShoeDataPayloadDTO implements Serializable {

    @JsonProperty("step_counter")
    private Integer stepCounter;

    @JsonProperty("step_activity")
    private Integer stepActivity;

    public Integer getStepCounter() {
        return stepCounter;
    }

    public void setStep_counter(Integer stepCounter) {
        this.stepCounter = stepCounter;
    }

    public Integer getStepActivity() {
        return stepActivity;
    }

    public void setStep_activity(Integer stepActivity) {
        this.stepActivity = stepActivity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoeDataPayloadDTO shoePayloadDTO = (ShoeDataPayloadDTO) o;
        if (shoePayloadDTO.getStepActivity() == null || getStepActivity() == null) {
            return false;
        }
        return Objects.equals(getStepCounter(), shoePayloadDTO.getStepCounter());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getStepCounter());
    }

    @Override
    public String toString() {
        return "ShoeDataPayloadDTO{" +
            "stepActivity=" + getStepActivity() +
            ", stepActivity='" + getStepCounter() +
            "}";
    }
}
