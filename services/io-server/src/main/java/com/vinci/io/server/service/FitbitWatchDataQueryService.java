package com.vinci.io.server.service;

import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.domain.FitbitWatchData_;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.FitbitWatchDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.FitbitWatchDataCriteria;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import com.vinci.io.server.service.mapper.FitbitWatchDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for FitbitWatchData entities in the database.
 * The main input is a {@link FitbitWatchDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FitbitWatchDataDTO} or a {@link Page} of {@link FitbitWatchDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FitbitWatchDataQueryService extends QueryService<FitbitWatchData> {

    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.FITBIT_WATCH;
    private final Logger log = LoggerFactory.getLogger(FitbitWatchDataQueryService.class);

    private final FitbitWatchDataRepository fitbitwatchdataRepository;

    private final FitbitWatchDataMapper fitbitwatchdataMapper;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public FitbitWatchDataQueryService(FitbitWatchDataRepository fitbitwatchdataRepository, FitbitWatchDataMapper fitbitwatchdataMapper,
                                       DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.fitbitwatchdataRepository = fitbitwatchdataRepository;
        this.fitbitwatchdataMapper = fitbitwatchdataMapper;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Return a {@link List} of {@link FitbitWatchDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FitbitWatchDataDTO> justFindByCriteria(FitbitWatchDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FitbitWatchData> specification = createSpecification(criteria);
        return fitbitwatchdataMapper.toDto(fitbitwatchdataRepository.findAll(specification));
    }

    /**
     * Checks user from token.
     * Return a {@link Page} of {@link FitbitWatchDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FitbitWatchDataDTO> findByCriteria(FitbitWatchDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        log.debug("Received {} devices: {}",ENTITY_DEVICE_TYPE,devicesForCurrentUser);
        if (devicesForCurrentUser.isEmpty()) {
            log.debug("There is no {} for current user!", ENTITY_DEVICE_TYPE);
            return new PageImpl<>(Collections.emptyList());
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        final Specification<FitbitWatchData> specification = createSpecification(criteria);
        return fitbitwatchdataRepository.findAll(specification, page)
            .map(fitbitwatchdataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FitbitWatchDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FitbitWatchData> specification = createSpecification(criteria);
        return fitbitwatchdataRepository.count(specification);
    }

    /**
     * Function to convert {@link FitbitWatchDataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FitbitWatchData> createSpecification(FitbitWatchDataCriteria criteria) {
        Specification<FitbitWatchData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FitbitWatchData_.id));
            }
            if (criteria.getSleepData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSleepData(), FitbitWatchData_.sleepData));
            }
            if (criteria.getActivityData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityData(), FitbitWatchData_.activityData));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), FitbitWatchData_.timestamp));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), FitbitWatchData_.deviceId));
            }
        }
        return specification;
    }
}
