/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.io.server.web.rest.vm;
