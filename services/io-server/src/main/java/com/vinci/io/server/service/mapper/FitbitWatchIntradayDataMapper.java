package com.vinci.io.server.service.mapper;


import com.vinci.io.server.domain.*;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FitbitWatchIntradayData} and its DTO {@link FitbitWatchIntradayDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FitbitWatchIntradayDataMapper extends EntityMapper<FitbitWatchIntradayDataDTO, FitbitWatchIntradayData> {

    default FitbitWatchIntradayData fromId(Long id) {
        if (id == null) {
            return null;
        }
        FitbitWatchIntradayData fitbitWatchIntradayData = new FitbitWatchIntradayData();
        fitbitWatchIntradayData.setId(id);
        return fitbitWatchIntradayData;
    }
}
