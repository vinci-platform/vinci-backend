package com.vinci.io.server.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Fitbitwatchdata.
 */
@Entity
@Table(name = "fitbit_watch_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FitbitWatchData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Column(name = "sleep_data")
    private String sleepData;

    @Lob
    @Column(name = "activity_data")
    private String activityData;

    @Column(name = "timestamp")
    private Long timestamp;

    @Column(name = "device_id")
    private Long deviceId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSleepData() {
        return sleepData;
    }

    public FitbitWatchData() {
    }

    public FitbitWatchData(Long id, String sleepData, String activityData, Long timestamp, Long deviceId) {
        this.id = id;
        this.sleepData = sleepData;
        this.activityData = activityData;
        this.timestamp = timestamp;
        this.deviceId = deviceId;
    }

    public FitbitWatchData sleepData(String sleepData) {
        this.sleepData = sleepData;
        return this;
    }

    public void setSleepData(String sleepData) {
        this.sleepData = sleepData;
    }

    public String getActivityData() {
        return activityData;
    }

    public FitbitWatchData activityData(String activityData) {
        this.activityData = activityData;
        return this;
    }

    public void setActivityData(String activityData) {
        this.activityData = activityData;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public FitbitWatchData timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public FitbitWatchData deviceId(Long deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchData)) {
            return false;
        }
        return id != null && id.equals(((FitbitWatchData) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FitbitWatchData{" +
            "id=" + getId() +
            ", sleepData='" + getSleepData() + "'" +
            ", activityData='" + getActivityData() + "'" +
            ", timestamp=" + getTimestamp() +
            ", deviceId=" + getDeviceId() +
            "}";
    }
}
