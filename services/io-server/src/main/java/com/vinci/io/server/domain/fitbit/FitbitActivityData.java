package com.vinci.io.server.domain.fitbit;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FitbitActivityData {
    private String time;
    private Long timestamp;
    private Integer heart;
    private Integer steps;
    private Integer calories;
    private Integer floors;
    private Integer elevation;
    private Integer distance;

    @Override
    public String toString() {
        return "FitbitActivityData{" +
            "time='" + time + '\'' +
            ", timestamp=" + timestamp +
            ", heart=" + heart +
            ", steps=" + steps +
            ", calories=" + calories +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FitbitActivityData that = (FitbitActivityData) o;
        return timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp);
    }
}
