package com.vinci.io.server.service.utility;

import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.AuthorizationException;
import com.vinci.io.server.error.NoDeviceFoundException;
import com.vinci.io.server.service.DeviceService;
import com.vinci.io.server.service.dto.DeviceDTO;
import io.github.jhipster.service.filter.LongFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DeviceServiceUtility {

    private final DeviceService deviceService;


    public DeviceServiceUtility(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    /**
     * Checks if there is any device that has the same id as the deviceId of data, if not AuthorizationException is thrown
     * @param devices devices that are checked for the existence of appropriate device
     * @param data_deviceId deviceId that is searched for
     * @param data_id for logs and exception message
     * @param entityName for logs and exception message
     * @param entityDeviceType for logs and exception message
     */
    public void validateDevices(Long data_id, Set<DeviceDTO> devices, Long data_deviceId, String entityName, DeviceType entityDeviceType) {
        boolean doesAnyDeviceMatchDataDeviceId = devices
            .stream()
            .map(DeviceDTO::getId)
            .anyMatch(deviceId -> deviceId.equals(data_deviceId));
        if (!doesAnyDeviceMatchDataDeviceId){
            log.info(entityName+" with id "+ data_id +" does not belong to current users "+entityDeviceType+" device or to any of his associates!");
            throw new AuthorizationException(String.format("%s with id %s does not belong to current users %s device or to any of his associates!"
                ,entityName, data_id,entityDeviceType));
        }
    }

    public DeviceDTO getByDeviceIdAndTypeFromSet(Set<DeviceDTO> devicesByType, Long dataDeviceId, DeviceType deviceType) {
        Optional<DeviceDTO> deviceOfUserOrAssociates = devicesByType
            .stream()
            .filter(deviceDTO -> deviceDTO.getId().equals(dataDeviceId))
            .findFirst();
        deviceOfUserOrAssociates.orElseThrow(() -> {
            log.debug("Current user and his associates don't have a device with id {} from update object!", dataDeviceId);
            return new NoDeviceFoundException(String.format("Current user and his associates don't have device %s with id %s from create object!"
                , deviceType, dataDeviceId));
        });
        return deviceOfUserOrAssociates.get();
    }

    public LongFilter setDeviceIdFilter(List<Long> listOfDeviceIds, LongFilter deviceIdFilter) {
        if (deviceIdFilter == null) {
            deviceIdFilter = new LongFilter();
            deviceIdFilter.setIn(listOfDeviceIds);
            return deviceIdFilter;
        }
        if (deviceIdFilter.getEquals() != null) {
            LongFilter finalDeviceIdFilter = deviceIdFilter;
            Optional<Long> equalsId = listOfDeviceIds.stream().filter(aLong -> aLong.equals(finalDeviceIdFilter.getEquals())).findFirst();
            if (equalsId.isPresent()) return deviceIdFilter;
        }
        if (deviceIdFilter.getIn() != null) {
            deviceIdFilter.setIn(deviceIdFilter
                .getIn()
                .stream()
                .filter(listOfDeviceIds::contains)
                .collect(Collectors.toList()));
            return deviceIdFilter;
        }
        deviceIdFilter.setIn(listOfDeviceIds);
        return deviceIdFilter;
    }
}
