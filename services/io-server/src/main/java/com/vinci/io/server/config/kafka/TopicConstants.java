package com.vinci.io.server.config.kafka;

public interface TopicConstants {
    String TOPIC = "KAFKA_TOPIC_EXAMPLE";
    String TOPIC_FITBIT_WATCH_DATA = "KAFKA_TOPIC_FITBIT_WATCH_DATA";
    String TOPIC_FITBIT_WATCH_INTRADAY_DATA = "KAFKA_TOPIC_FITBIT_WATCH_INTRADAY_DATA";
}
