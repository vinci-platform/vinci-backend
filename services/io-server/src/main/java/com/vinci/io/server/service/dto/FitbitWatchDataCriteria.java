package com.vinci.io.server.service.dto;

import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.web.rest.FitbitWatchDataResource;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link FitbitWatchData} entity. This class is used
 * in {@link FitbitWatchDataResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fitbit-watch-data?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FitbitWatchDataCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter sleepData;

    private StringFilter activityData;

    private LongFilter timestamp;

    private LongFilter deviceId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSleepData() {
        return sleepData;
    }

    public void setSleepData(StringFilter sleepData) {
        this.sleepData = sleepData;
    }

    public StringFilter getActivityData() {
        return activityData;
    }

    public void setActivityData(StringFilter activityData) {
        this.activityData = activityData;
    }

    public LongFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LongFilter timestamp) {
        this.timestamp = timestamp;
    }

    public LongFilter getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(LongFilter deviceId) {
        this.deviceId = deviceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FitbitWatchDataCriteria that = (FitbitWatchDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(sleepData, that.sleepData) &&
            Objects.equals(activityData, that.activityData) &&
            Objects.equals(timestamp, that.timestamp) &&
            Objects.equals(deviceId, that.deviceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        sleepData,
        activityData,
        timestamp,
        deviceId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FitbitWatchDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (sleepData != null ? "sleepData=" + sleepData + ", " : "") +
                (activityData != null ? "activityData=" + activityData + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (deviceId != null ? "deviceId=" + deviceId + ", " : "") +
            "}";
    }

}
