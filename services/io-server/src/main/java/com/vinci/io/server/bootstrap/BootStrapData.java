package com.vinci.io.server.bootstrap;

import com.vinci.io.server.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("dev")
@Component
@Slf4j
public class BootStrapData implements CommandLineRunner {

    private final CameraFitnessDataRepository cameraFitnessDataRepository;
    private final CameraMovementDataRepository cameraMovementDataRepository;
    private final ShoeDataRepository shoeDataRepository;
    private final SurveyDataRepository surveyDataRepository;
    private final WatchDataRepository watchDataRepository;

    public BootStrapData(CameraFitnessDataRepository cameraFitnessDataRepository,
                         CameraMovementDataRepository cameraMovementDataRepository,
                         ShoeDataRepository shoeDataRepository,
                         SurveyDataRepository surveyDataRepository,
                         WatchDataRepository watchDataRepository) {
        this.cameraFitnessDataRepository = cameraFitnessDataRepository;
        this.cameraMovementDataRepository = cameraMovementDataRepository;
        this.shoeDataRepository = shoeDataRepository;
        this.surveyDataRepository = surveyDataRepository;
        this.watchDataRepository = watchDataRepository;
    }

    @Override
    public void run(String... args){
        log.info("####  BOOTSTRAP DATA (COMMAND LINE RUNNER): ");
    }
}
