package com.vinci.io.server.web.rest.beans;

import com.vinci.io.server.config.Constants;
import com.vinci.io.server.service.dto.DeviceDTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    private Long id;

    private Long userExtraId;

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String gender;

    @Size(max = 50)
    private String lastName;

    @Size(max = 100)
    private String address;

    @Size(min = 10, max = 10)
    private String phone;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private boolean activated = false;

    @Size(min = 2, max = 6)
    private String langKey;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private String role;

    private Set<String> authorities;

    private String uuid;

    private Set<DeviceDTO> personalDevices;
    private Set<DeviceDTO> patientsOfFamily;
    private Set<DeviceDTO> patientsOfOrganization;

    private String maritalStatus;

    private String education;

    private String friends;

    public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() { return maritalStatus; }

    public void setMaritalStatus(String maritalStatus) { this.maritalStatus = maritalStatus; }

    public String getEducation() { return education; }

    public void setEducation(String education) { this.education = education; }

    public String getFriends() { return friends; }

    public void setFriends(String friends) { this.friends = friends; }

    public Set<DeviceDTO> getPersonalDevices() {
        return personalDevices;
    }

    public void setPersonalDevices(Set<DeviceDTO> personalDevices) {
        this.personalDevices = personalDevices;
    }

    public Set<DeviceDTO> getPatientsOfFamily() {
        return patientsOfFamily;
    }

    public void setPatientsOfFamily(Set<DeviceDTO> patientsOfFamily) {
        this.patientsOfFamily = patientsOfFamily;
    }

    public Set<DeviceDTO> getPatientsOfOrganization() {
        return patientsOfOrganization;
    }

    public void setPatientsOfOrganization(Set<DeviceDTO> patientsOfOrganization) {
        this.patientsOfOrganization = patientsOfOrganization;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", createdBy=" + createdBy +
            ", createdDate=" + createdDate +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            ", authorities=" + authorities +
            "}";
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Set<DeviceDTO> getAllDevices() {
        Set<DeviceDTO> allDevices = new HashSet<>();
        allDevices.addAll(personalDevices);
        allDevices.addAll(patientsOfFamily);
        allDevices.addAll(patientsOfOrganization);
        return allDevices;
    }
}
