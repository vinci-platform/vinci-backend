package com.vinci.io.server.web.rest.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WatchDataImportBean {
//    @JsonProperty("_id")
    private String recordId;
//    @JsonProperty("_xt")
    private String _xt;
//    @JsonProperty("ei")
    private String IMEI;
//    @JsonProperty("si")
    private String deviceId;
//    @JsonProperty("dt")
    private String timestamp;
//    @JsonProperty("s")
    private String IP;
//    @JsonProperty("c")
    private String payload;
//    @JsonProperty("y")
    private String year;
//    @JsonProperty("m")
    private String month;
//    @JsonProperty("d")
    private String day;
//    @JsonProperty("p")
    private String alertType;

    @JsonCreator
    public WatchDataImportBean(@JsonProperty("_id") String recordId,
                               @JsonProperty("_xt") String _xt,
                               @JsonProperty("ei") String IMEI,
                               @JsonProperty("si") String deviceId,
                               @JsonProperty("dt") String timestamp,
                               @JsonProperty("s") String IP,
                               @JsonProperty("c") String payload,
                               @JsonProperty("y") String year,
                               @JsonProperty("m") String month,
                               @JsonProperty("d") String day,
                               @JsonProperty("p") String alertType) {
        this.recordId = recordId;
        this._xt = _xt;
        this.IMEI = IMEI;
        this.deviceId = deviceId;
        this.timestamp = timestamp;
        this.IP = IP;
        this.payload = payload;
        this.year = year;
        this.month = month;
        this.day = day;
        this.alertType = alertType;
    }
}
