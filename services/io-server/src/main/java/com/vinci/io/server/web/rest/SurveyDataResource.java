package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.SurveyData;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.SurveyDataQueryService;
import com.vinci.io.server.service.SurveyDataService;
import com.vinci.io.server.service.dto.SurveyDataCriteria;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing SurveyData.
 */
@RestController
@RequestMapping("/api")
public class SurveyDataResource {

    private final Logger log = LoggerFactory.getLogger(SurveyDataResource.class);

    private static final String ENTITY_NAME = "ioserverSurveyData";

    private final SurveyDataService surveyDataService;

    private final SurveyDataQueryService surveyDataQueryService;

    public SurveyDataResource(SurveyDataService surveyDataService, SurveyDataQueryService surveyDataQueryService) {
        this.surveyDataService = surveyDataService;
        this.surveyDataQueryService = surveyDataQueryService;
    }

    /**
     * POST  /survey-data : Create a new surveyData.
     *
     * @param surveyData the surveyData to create
     * @return the ResponseEntity with status 201 (Created) and with body the new surveyData, or with status 400 (Bad Request) if the surveyData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/survey-data")
    public ResponseEntity<SurveyData> createSurveyData(@RequestBody @Valid SurveyData surveyData) throws URISyntaxException {
        log.debug("REST request to save SurveyData : {}", surveyData);
        SurveyData result = surveyDataService.save(surveyData);
        return ResponseEntity.created(new URI("/api/survey-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /survey-data/import : Import a  new surveyData from Survey microservice.
     *
     * @param surveyData the surveyData to create
     * @return the ResponseEntity with status 201 (Created) and with body the new surveyData, or with status 400 (Bad Request) if the surveyData has already an ID
     */
    @PostMapping("/survey-data/import")
    public String saveSurveyData(@RequestBody @Valid SurveyData surveyData) {
        log.debug("REST request to save SurveyData : {}", surveyData);
        SurveyData result = surveyDataService.save(surveyData);
        return result.getIdentifier();
    }

    /**
     * PUT  /survey-data : Updates an existing surveyData.
     *
     * @param surveyData the surveyData to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated surveyData,
     * or with status 400 (Bad Request) if the surveyData is not valid,
     * or with status 500 (Internal Server Error) if the surveyData couldn't be updated
     */
    @PutMapping("/survey-data")
    public ResponseEntity<SurveyData> updateSurveyData(@RequestBody @Valid SurveyData surveyData) {
        log.debug("REST request to update SurveyData : {}", surveyData);
        SurveyData result = surveyDataService.update(surveyData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, surveyData.getId().toString()))
            .body(result);
    }

    /**
     * GET  /survey-data : get all the surveyData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @GetMapping("/survey-data")
    public ResponseEntity<List<SurveyData>> getAllSurveyData(SurveyDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SurveyData by criteria: {}", criteria);
        Page<SurveyData> page = surveyDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/survey-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /survey-data/count : count all the surveyData.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count in body
     */
    @GetMapping("/survey-data/count")
    public ResponseEntity<Long> countSurveyData(SurveyDataCriteria criteria) {
        log.debug("REST request to count SurveyData by criteria: {}", criteria);
        return ResponseEntity.ok().body(surveyDataQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /survey-data/:id : get the "id" surveyData.
     *
     * @param id the id of the surveyData to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the surveyData, or with status 404 (Not Found)
     */
    @GetMapping("/survey-data/{id}")
    public ResponseEntity<SurveyData> getSurveyData(@PathVariable Long id) {
        log.debug("REST request to get SurveyData by id: {}", id);
        return ResponseEntity.ok(surveyDataService.findOne(id));
    }

    /**
     * DELETE  /survey-data/:id : delete the "id" surveyData.
     *
     * @param id the id of the surveyData to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/survey-data/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteSurveyData(@PathVariable Long id) {
        log.debug("REST request to delete SurveyData : {}", id);
        surveyDataService.justDelete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
