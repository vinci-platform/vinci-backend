package com.vinci.io.server.repository;

import com.vinci.io.server.domain.ShoeData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ShoeData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoeDataRepository extends JpaRepository<ShoeData, Long>, JpaSpecificationExecutor<ShoeData> {
    List<ShoeData> findAllByDeviceIdAndTimestampGreaterThanEqual(Long deviceId, Long startTimestampMillis);
}
