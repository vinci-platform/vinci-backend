package com.vinci.io.server.repository;

import com.vinci.io.server.domain.CameraMovementData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CameraMovementData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CameraMovementDataRepository extends JpaRepository<CameraMovementData, Long>, JpaSpecificationExecutor<CameraMovementData> {

}
