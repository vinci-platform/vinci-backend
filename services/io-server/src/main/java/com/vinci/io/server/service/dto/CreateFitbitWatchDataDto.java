package com.vinci.io.server.service.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


public class CreateFitbitWatchDataDto implements Serializable {

    @NotNull
    private String deviceUUID;
    @NotNull
    private Long timestamp;
    @NotNull
    private String data;

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateFitbitWatchDataDto that = (CreateFitbitWatchDataDto) o;
        return deviceUUID.equals(that.deviceUUID) && timestamp.equals(that.timestamp) && data.equals(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceUUID, timestamp, data);
    }

    @Override
    public String toString() {
        return "CreateFitbitWatchDataDto{" +
            "deviceUUID='" + deviceUUID + '\'' +
            ", timestamp=" + timestamp +
            ", data='" + data + '\'' +
            '}';
    }
}
