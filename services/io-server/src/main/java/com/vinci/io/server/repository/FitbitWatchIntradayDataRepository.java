package com.vinci.io.server.repository;

import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.domain.FitbitWatchIntradayData;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data repository for the FitbitWatchIntradayData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FitbitWatchIntradayDataRepository extends JpaRepository<FitbitWatchIntradayData, Long>, JpaSpecificationExecutor<FitbitWatchIntradayData> {

    @Query("SELECT max(fitbitWatchIntradayData.timestamp) FROM FitbitWatchIntradayData fitbitWatchIntradayData WHERE fitbitWatchIntradayData.deviceId = :id")
    Optional<Long> findMaxTimestampByDeviceId(@Param("id") Long id);

    List<FitbitWatchIntradayData> findAllByTimestampAndDeviceId(Long timestamp, Long deviceId);
}
