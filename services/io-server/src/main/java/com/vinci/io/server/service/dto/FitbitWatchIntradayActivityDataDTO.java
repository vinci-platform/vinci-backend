package com.vinci.io.server.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FitbitWatchIntradayActivityDataDTO {
    private Object time;
    private Integer heart;
    private Integer steps;
    private Integer calories;
    private Integer floors;
    private Integer elevation;
    private Integer distance;
    private Long timestamp;
}
