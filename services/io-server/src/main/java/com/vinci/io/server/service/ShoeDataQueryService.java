package com.vinci.io.server.service;

import com.vinci.io.server.domain.ShoeData;
import com.vinci.io.server.domain.ShoeData_;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.ShoeDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.ShoeDataCriteria;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for ShoeData entities in the database.
 * The main input is a {@link ShoeDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoeData} or a {@link Page} of {@link ShoeData} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoeDataQueryService extends QueryService<ShoeData> {

    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.SHOE;
    private final Logger log = LoggerFactory.getLogger(ShoeDataQueryService.class);

    private final ShoeDataRepository shoeDataRepository;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public ShoeDataQueryService(ShoeDataRepository shoeDataRepository, DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.shoeDataRepository = shoeDataRepository;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Return a {@link Page} of {@link ShoeData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoeData> findByCriteria(ShoeDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        log.debug("Received {} devices: {}",ENTITY_DEVICE_TYPE,devicesForCurrentUser);
        if (devicesForCurrentUser.isEmpty()) {
            log.debug("There is no {} for current user!", ENTITY_DEVICE_TYPE);
            return new PageImpl<>(Collections.emptyList());
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        final Specification<ShoeData> specification = createSpecification(criteria);
        return shoeDataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoeDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoeData> specification = createSpecification(criteria);
        return shoeDataRepository.count(specification);
    }

    /**
     * Function to convert ShoeDataCriteria to a {@link Specification}
     */
    private Specification<ShoeData> createSpecification(ShoeDataCriteria criteria) {
        Specification<ShoeData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShoeData_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), ShoeData_.timestamp));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), ShoeData_.deviceId));
            }
        }
        return specification;
    }
}
