package com.vinci.io.server.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FitbitWatchIntradayDataPayloadDTO {

    private Long id;

    private List<FitbitWatchIntradayActivityDataDTO> data;

    private Long timestamp;

    private DeviceDTO device;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<FitbitWatchIntradayActivityDataDTO> getData() {
        return data;
    }

    public void setData(List<FitbitWatchIntradayActivityDataDTO> data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public DeviceDTO getDevice() {
        return device;
    }

    public void setDevice(DeviceDTO device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchIntradayDataPayloadDTO)) {
            return false;
        }

        return id != null && id.equals(((FitbitWatchIntradayDataPayloadDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FitbitWatchIntradayDataPayloadDTO{" +
            "id=" + id +
            ", data=" + data +
            ", timestamp=" + timestamp +
            ", device=" + device +
            '}';
    }
}
