package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.WatchDataQueryService;
import com.vinci.io.server.service.WatchDataService;
import com.vinci.io.server.service.dto.CreateWatchDataDto;
import com.vinci.io.server.service.dto.WatchDataCriteria;
import com.vinci.io.server.web.rest.adapters.WatchDataAdapter;
import com.vinci.io.server.web.rest.beans.MeasuredData;
import com.vinci.io.server.web.rest.beans.WatchDataRequestBean;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * REST controller for managing WatchData.
 */
@RestController
@RequestMapping("/api")
public class WatchDataResource {

    private static final String ENTITY_NAME = "ioserverWatchData";

    private final Logger log = LoggerFactory.getLogger(WatchDataResource.class);

    private final WatchDataService watchDataService;

    private final WatchDataQueryService watchDataQueryService;

    private final WatchDataAdapter watchDataAdapter;

    public WatchDataResource(WatchDataService watchDataService, WatchDataQueryService watchDataQueryService, WatchDataAdapter watchDataAdapter) {
        this.watchDataService = watchDataService;
        this.watchDataQueryService = watchDataQueryService;
        this.watchDataAdapter = watchDataAdapter;
    }

    /**
     * POST  /watch-data : Create a new watchData.
     *
     * @param createWatchDataDto the CreateWatchDataDto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new watchData, or with status 400 (Bad Request) if the watchData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/watch-data")
    public ResponseEntity<WatchData> createWatchData(@RequestBody CreateWatchDataDto createWatchDataDto) throws Exception {
        log.debug("REST request to save CreateWatchDataDto : {}", createWatchDataDto);
        WatchData result = watchDataService.saveDto(createWatchDataDto);
        return ResponseEntity.created(new URI("/api/watch-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping(value = "/watch-data/import")
    public ResponseEntity<String> importWatchData(@RequestBody String watchData) {
        log.debug("REST request to save a list of watchData: {}", watchData);
        return ResponseEntity
            .ok(watchDataService.saveAllImportedData(watchData));
    }

    /**
     * PUT  /watch-data : Updates an existing watchData.
     *
     * @param watchData the watchData to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated watchData,
     * or with status 400 (Bad Request) if the watchData is not valid,
     * or with status 500 (Internal Server Error) if the watchData couldn't be updated
     */
    @PutMapping("/watch-data")
    public ResponseEntity<WatchData> updateWatchData(@RequestBody WatchData watchData) {
        log.debug("REST request to update WatchData : {}", watchData);
        WatchData result = watchDataService.update(watchData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, watchData.getId().toString()))
            .body(result);
    }

    @PostMapping("/watch-data/orderedByTypeStepsAndCoordinates")
    public ResponseEntity<Map<String, List<MeasuredData>>> getStepsAndCoordinatesForFilter(@RequestBody WatchDataRequestBean request) throws Exception {
        log.debug("REST request to retrieve WatchData based on request parameters: {}", request);
        if (request == null) {
            throw new BadRequestAlertException("Received invalid parameters.", ENTITY_NAME, "payloadnull");
        }
        Map<String, List<MeasuredData>> entityMap = watchDataAdapter.getOrderedAndFilteredStepsAndCoordinatesAndCheckCurrentUserDevice(request);
        return ResponseEntity.ok().body(entityMap);
    }

    @PostMapping("/watch-data/orderedByType")
    public ResponseEntity<Map<String, List<WatchData>>> getWatchDataForFilter(@RequestBody WatchDataRequestBean request) throws Exception {
        log.debug("REST request to retrieve WatchData based on request parameters: {}", request);
        if (request == null) {
            throw new BadRequestAlertException("Received invalid parameters.", ENTITY_NAME, "payloadnull");
        }
        Map<String, List<WatchData>> entityMap = watchDataAdapter.getOrderedAndFilteredAlertsAndCheackCurrentUserDevice(request);
        return ResponseEntity.ok().body(entityMap);
    }

    /**
     * GET  /watch-data : get all the watchData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of watchData in body
     */
    @GetMapping("/watch-data")
    public ResponseEntity<List<WatchData>> getAllWatchData(WatchDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get WatchData by criteria: {}", criteria);
        Page<WatchData> page = watchDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/watch-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /watch-data/count : count all the watchData.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/watch-data/count")
    public ResponseEntity<Long> countWatchData(WatchDataCriteria criteria) {
        log.debug("REST request to count WatchData by criteria: {}", criteria);
        return ResponseEntity.ok().body(watchDataQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /watch-data/:id : get the "id" watchData.
     *
     * @param id the id of the watchData to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the watchData, or with status 404 (Not Found)
     */
    @GetMapping("/watch-data/{id}")
    public ResponseEntity<WatchData> getWatchData(@PathVariable Long id) {
        log.debug("REST request to get WatchData by id: {}", id);
        return ResponseEntity.ok(watchDataService.findOne(id));
    }

    /**
     * DELETE  /watch-data/:id : delete the "id" watchData.
     *
     * @param id the id of the watchData to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/watch-data/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteWatchData(@PathVariable Long id) {
        log.debug("REST request to delete WatchData : {}", id);
        watchDataService.justDelete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
