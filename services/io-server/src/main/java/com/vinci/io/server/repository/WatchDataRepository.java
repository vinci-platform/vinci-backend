package com.vinci.io.server.repository;

import com.vinci.io.server.domain.WatchData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the WatchData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WatchDataRepository extends JpaRepository<WatchData, Long>, JpaSpecificationExecutor<WatchData> {
    List<WatchData> findAllByDeviceIdAndTimestampGreaterThanEqual(Long deviceId, Long startTimestampMillis);
}
