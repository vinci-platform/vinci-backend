package com.vinci.io.server.web.rest.beans;

import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WatchDataRequestBean {
    private String deviceId;
    private LocalDateTime begin;
    private LocalDateTime end;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("deviceId", deviceId)
            .add("begin", begin.toString())
            .add("end", end.toString())
            .toString();
    }
}
