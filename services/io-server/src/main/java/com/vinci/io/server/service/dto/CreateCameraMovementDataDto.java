package com.vinci.io.server.service.dto;

import com.vinci.io.server.domain.CameraMovementData;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * CreateCameraMovementDataDto used when creating a CameraMovementData
 */
public class CreateCameraMovementDataDto implements Serializable {

    @NotNull
    private String deviceUUID;

    @NotNull
    private Long timestamp;

    @NotNull
    private String data;

    public CreateCameraMovementDataDto deviceId(String deviceUUID) {
        this.deviceUUID = deviceUUID;
        return this;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    public CreateCameraMovementDataDto timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public CreateCameraMovementDataDto data(String data) {
        this.data = data;
        return this;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CreateCameraMovementDataDto createCameraMovementDataDto = (CreateCameraMovementDataDto) o;
        if (createCameraMovementDataDto.getDeviceUUID() == null || getDeviceUUID() == null) {
            return false;
        }
        return Objects.equals(getDeviceUUID(), createCameraMovementDataDto.getDeviceUUID());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getDeviceUUID());
    }

    @Override
    public String toString() {
        return "CreateCameraMovementDataDto{" +
            "uuid=" + getDeviceUUID() +
            ", timestamp='" + getTimestamp() + "'" +
            ", data='" + getData() +
            "}";
    }
}
