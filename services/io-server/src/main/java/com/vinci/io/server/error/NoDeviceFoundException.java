package com.vinci.io.server.error;

import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoDeviceFoundException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_DEVICE_FOUND_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "There is no device!";
    private static final String DETAILS_WITH_INFO = "There is no device found for type %s and id %s";
    private static final String DETAILS_WITH_TYPE = "There is no device found for type %s";

    public NoDeviceFoundException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public NoDeviceFoundException(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, details); }
    public NoDeviceFoundException(DeviceType deviceType, Long deviceId) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_INFO,deviceType.name(),deviceId)); }
    public NoDeviceFoundException(DeviceType deviceType) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_TYPE,deviceType.name())); }
    public NoDeviceFoundException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoDeviceFoundException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
