package com.vinci.io.server.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the FitbitWatchIntradayData entity. This class is used in FitbitWatchIntradayDataResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /fitbit-watch-intraday-data?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FitbitWatchIntradayDataCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter data;

    private LongFilter timestamp;

    private LongFilter deviceId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getData() {
        return data;
    }

    public void setData(StringFilter data) {
        this.data = data;
    }

    public LongFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LongFilter timestamp) {
        this.timestamp = timestamp;
    }

    public LongFilter getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(LongFilter deviceId) {
        this.deviceId = deviceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FitbitWatchIntradayDataCriteria that = (FitbitWatchIntradayDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(data, that.data) &&
            Objects.equals(timestamp, that.timestamp) &&
            Objects.equals(deviceId, that.deviceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        data,
        timestamp,
        deviceId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FitbitWatchIntradayDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (deviceId != null ? "deviceId=" + deviceId + ", " : "") +
            "}";
    }
}
