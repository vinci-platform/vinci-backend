package com.vinci.io.server.web.rest.feign.handler;

import feign.Response;

public interface FeignHttpExceptionHandler {
    Exception handle(Response response);
}
