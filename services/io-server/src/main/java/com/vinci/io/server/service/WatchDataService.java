package com.vinci.io.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.AuthorizationException;
import com.vinci.io.server.error.NoDeviceFoundException;
import com.vinci.io.server.error.NoEntityException;
import com.vinci.io.server.repository.WatchDataRepository;
import com.vinci.io.server.service.dto.CreateWatchDataDto;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.WatchDataCriteria;
import com.vinci.io.server.service.dto.WatchDataDTO;
import com.vinci.io.server.service.mapper.WatchDataMapper;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing WatchData.
 */
@Service
@Transactional
public class WatchDataService {

    private static final String ENTITY_NAME = "ioserverWatchData";
    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.WATCH;

    private final Logger log = LoggerFactory.getLogger(WatchDataService.class);

    private final WatchDataRepository watchDataRepository;

    private final WatchDataMapper watchDataMapper;

    private final GatewayClient gatewayClient;

    private final WatchDataQueryService watchDataQueryService;

    private final DeviceService deviceService;

    private final ObjectMapper objectMapper;

    public WatchDataService(WatchDataRepository watchDataRepository, WatchDataMapper watchDataMapper, GatewayClient gatewayClient,
                            WatchDataQueryService watchDataQueryService, DeviceService deviceService, ObjectMapper objectMapper) {
        this.watchDataRepository = watchDataRepository;
        this.watchDataMapper = watchDataMapper;
        this.gatewayClient = gatewayClient;
        this.watchDataQueryService = watchDataQueryService;
        this.deviceService = deviceService;
        this.objectMapper = objectMapper;
    }

    /**
     * Save a custom watchData.
     *
     * @param createWatchDataDto the model to save
     * @return the persisted entity or null if there is exception
     * @throws InternalServerErrorException if the device was not found in gateway
     */
    public WatchData saveDto(CreateWatchDataDto createWatchDataDto) {
        log.debug("Request to save CreateWatchDataDto : {}", createWatchDataDto);

        Set<DeviceDTO> watchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (watchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, createWatchDataDto.getDeviceId());
        }
        Optional<DeviceDTO> deviceOfUserOrAssociates = watchDevicesForCurrentUser
            .stream()
            .filter(deviceDTO -> deviceDTO.getId().equals(createWatchDataDto.getDeviceId()))
            .findFirst();
        deviceOfUserOrAssociates.orElseThrow(() -> {
            log.debug("Current user and his associates don't have device with id {} from create object!",createWatchDataDto.getDeviceId());
            return new NoDeviceFoundException(String.format("Current user and his associates don't have device %s with id %s from create object!"
                ,ENTITY_DEVICE_TYPE, createWatchDataDto.getDeviceId()));
        });
        return watchDataRepository.save(watchDataMapper.toEntity(createWatchDataDto, deviceOfUserOrAssociates.get().getId()));
    }

    /**
     * Save a custom watchData.
     *
     * @param watchData the model to update
     * @return the persisted entity or null if there is exception
     * @throws InternalServerErrorException if the device was not found in gateway
     */
    public WatchData update(WatchData watchData) {
        log.debug("Request to update watchData : {}", watchData);
        validateWatchDataForCreation(watchData);
        Set<DeviceDTO> watchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (watchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, watchData.getDeviceId());
        }
        Optional<DeviceDTO> deviceOfUserOrAssociates = watchDevicesForCurrentUser
            .stream()
            .filter(deviceDTO -> deviceDTO.getId().equals(watchData.getDeviceId()))
            .findFirst();
        deviceOfUserOrAssociates.orElseThrow(() -> {
            log.debug("Current user and his associates don't have device with id {} from update object!",watchData.getDeviceId());
            return new NoDeviceFoundException(String.format("Current user and his associates don't have device %s with id %s from create object!"
                ,ENTITY_DEVICE_TYPE, watchData.getDeviceId()));
        });
        return watchDataRepository.save(watchData);
    }

    /**
     * Save a watchData.
     *
     * @param watchData the entity to save
     * @return the persisted entity
     */
    public WatchData justSave(WatchData watchData) {
        log.debug("Request to save WatchData : {}", watchData);
        return watchDataRepository.save(watchData);
    }

    /**
     * Save a list of watchData entities
     *
     * @param watchDataImport string of array of WatchData
     * @return message of number of saved entities
     */
    public String saveAllImportedData(String watchDataImport) {
        log.debug("Request to save watchDataImport list : {}", watchDataImport);

        List<WatchData> records = getRecordsFromString(watchDataImport);

        validateRecordsForCreation(records);

        Set<DeviceDTO> watchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (watchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        List<WatchData> filteredRecords = records
            .stream()
            .filter(watchData -> {
                watchData.setTimestamp(Instant.now().toEpochMilli());
                return watchDevicesForCurrentUser.contains(new DeviceDTO().id(watchData.getDeviceId()));
            })
            .collect(Collectors.toList());

        List<WatchData> result = watchDataRepository.saveAll(filteredRecords);
        return Strings.lenientFormat("Successfully saved a list of %s entities", result.size());
    }
    /**
     * Get one watchData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WatchData findOne(Long id) {
        log.debug("Request to get WatchData by id: {}", id);

        Set<DeviceDTO> watchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (watchDevicesForCurrentUser.isEmpty()) {
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        WatchData watchData = watchDataRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));
        boolean isWatchDataDeviceIdValidForCurrentUsersDevices = watchDevicesForCurrentUser
            .stream()
            .map(DeviceDTO::getId)
            .anyMatch(deviceId -> deviceId.equals(watchData.getDeviceId()));
        if (!isWatchDataDeviceIdValidForCurrentUsersDevices){
            log.info(ENTITY_NAME+" with id "+id+" does not belong to current users "+ENTITY_DEVICE_TYPE+" device or to any of his associates!");
            throw new AuthorizationException(String.format("%s with id %s does not belong to current users %s device or to any of his associates!"
                ,ENTITY_NAME,id,ENTITY_DEVICE_TYPE));
        }
        return watchData;
    }

    /**
     * Just delete the watchData by id.
     *
     * @param id the id of the entity
     */
    public void justDelete(Long id) {
        log.debug("Request to delete WatchData : {}", id);
        watchDataRepository.deleteById(id);
    }

    /**
     * Get a list of watchData by deviceId.
     *
     * @param deviceId of the entity
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<WatchData> getWatchDataByDeviceId(Long deviceId) {
        log.debug("Request to get WatchData for a device with id: {}", deviceId);
        Optional<DeviceDTO> deviceDTO = gatewayClient.getDeviceById(deviceId);
        if (!deviceDTO.isPresent()) {
            throw new InternalServerErrorException("The device with id " + deviceId + " was not found in gateway");
        }
        Long startTimestampMillis = deviceDTO.get().getStartTimestamp().toEpochMilli();
        return watchDataRepository.findAllByDeviceIdAndTimestampGreaterThanEqual(deviceId, startTimestampMillis);
    }

    public List<WatchDataDTO> findByCriteria(WatchDataCriteria criteria) {
        List<WatchData> queryList = watchDataQueryService.justFindByCriteria(criteria);
        List<WatchDataDTO> entityList = new ArrayList<>();
        queryList.forEach(watchData -> {
            Optional<DeviceDTO> deviceDTO = gatewayClient.getDeviceById(watchData.getDeviceId());
            if (deviceDTO.isPresent()) {
                WatchDataDTO watchDataDTO = new WatchDataDTO();
                watchDataDTO.setId(watchData.getDeviceId());
                watchDataDTO.setData(watchData.getData());
                watchDataDTO.setTimestamp(watchData.getTimestamp());
                watchDataDTO.setDevice(deviceDTO.get());
                entityList.add(watchDataDTO);
            }
        });
        return entityList;
    }

    private void validateRecordsForCreation(List<WatchData> records) {
        for (WatchData record : records) {
            if (record.getId() != null)
                throw new BadRequestAlertException("record should not have an id when saving",ENTITY_NAME,"idexists");
        }
    }

    private List<WatchData> getRecordsFromString(String watchDataImport) {
        try {
            return Arrays.asList(objectMapper.readValue(watchDataImport, WatchData[].class));
        } catch (IOException e) {
            throw new BadRequestAlertException("Cant deserialize request string!",ENTITY_NAME,"badBodyString");
        }
    }

    private void validateWatchDataForCreation(WatchData watchData) {
        if (watchData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (watchData.getDeviceId() == null){
            log.debug("There is no deviceID for {}!",ENTITY_NAME);
            throw new BadRequestAlertException("A new"+ENTITY_NAME+" must have deviceId", ENTITY_NAME, "deviceIdDoesntExist");
        }
    }
}
