package com.vinci.io.server.error;

import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.Arrays;

public class IncorrectDeviceTypeException extends AbstractThrowableProblem {

    private static final String TITLE = "INCORRECT_DEVICE_TYPE_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "DeviceType %s is incorrect!\nCorrect device types: " + Arrays.toString(DeviceType.values());

    public IncorrectDeviceTypeException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public IncorrectDeviceTypeException(String incorrectDeviceType) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DEFAULT_DETAILS,incorrectDeviceType)); }
    public IncorrectDeviceTypeException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "IncorrectDeviceTypeException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
