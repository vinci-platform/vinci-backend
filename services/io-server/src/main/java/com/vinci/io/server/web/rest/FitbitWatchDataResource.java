package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.FitbitWatchDataQueryService;
import com.vinci.io.server.service.FitbitWatchDataService;
import com.vinci.io.server.service.dto.FitbitWatchDataCriteria;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import com.vinci.io.server.service.dto.FitbitWatchDataPayloadDTO;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing {@link FitbitWatchData}.
 */
@RestController
@RequestMapping("/api")
public class FitbitWatchDataResource {

    private final Logger log = LoggerFactory.getLogger(FitbitWatchDataResource.class);

    private static final String ENTITY_NAME = "ioserverFitbitWatchData";

    private final FitbitWatchDataService fitbitWatchDataService;

    private final FitbitWatchDataQueryService fitbitWatchDataQueryService;

    public FitbitWatchDataResource(FitbitWatchDataService fitbitWatchDataService, FitbitWatchDataQueryService fitbitWatchDataQueryService) {
        this.fitbitWatchDataService = fitbitWatchDataService;
        this.fitbitWatchDataQueryService = fitbitWatchDataQueryService;
    }

    /**
     * {@code POST  /fitbit-watch-data} : Create a new fitbitWatchData.
     *
     * @param fitbitWatchDataDTO the fitbitwatchdataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fitbitWatchDataDTO, or with status {@code 400 (Bad Request)} if the fitbitWatchData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fitbit-watch-data")
    public ResponseEntity<FitbitWatchDataDTO> createFitbitWatchData(@RequestBody @Valid FitbitWatchDataDTO fitbitWatchDataDTO) throws URISyntaxException {
        log.debug("REST request to save fitbitWatchData : {}", fitbitWatchDataDTO);
        FitbitWatchDataDTO result = fitbitWatchDataService.saveDTO(fitbitWatchDataDTO);
        return ResponseEntity.created(new URI("/api/fitbit-watch-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping(value = "/fitbit-watch-data/sync")
    public ResponseEntity<String> syncFitbitWatchData(@RequestBody String fitbitWatchData) {
        log.debug("REST request to save a list of fitbitWatchData: {}", fitbitWatchData);
        return ResponseEntity
            .ok(fitbitWatchDataService.saveSync(fitbitWatchData));
    }

    /**
     * PUT  /fitbit-watch-data : Updates an existing fitbitWatchData.
     *
     * @param fitbitWatchDataDTO the fitbitWatchDataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fitbitWatchDataDTO,
     * or with status 400 (Bad Request) if the fitbitWatchDataDTO is not valid,
     * or with status 500 (Internal Server Error) if the fitbitWatchDataDTO couldn't be updated
     */
    @PutMapping("/fitbit-watch-data")
    public ResponseEntity<FitbitWatchDataDTO> updateFitbitWatchData(@RequestBody @Valid FitbitWatchDataDTO fitbitWatchDataDTO) {
        log.debug("REST request to update Fitbitwatchdata : {}", fitbitWatchDataDTO);
        FitbitWatchDataDTO result = fitbitWatchDataService.updateDto(fitbitWatchDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fitbit-watch-data : get all the fitbitWatchData.
     *
     * DOESNT SUPPORT EQUALS AND IN FILTERS FOR ACTIVITY AND SLEEP DATA
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of fitbitWatchData in body
     */
    @GetMapping("/fitbit-watch-data")
    public ResponseEntity<List<FitbitWatchDataDTO>> getAllFitbitWatchData(FitbitWatchDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get FitbitWatchData by criteria: {}", criteria);
        Page<FitbitWatchDataDTO> page = fitbitWatchDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fitbit-watch-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fitbit-watch-data/payload} : get all the fitbitWatchData.
     *
     * DOESNT SUPPORT EQUALS AND IN FILTERS FOR ACTIVITY AND SLEEP DATA
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of FitbitWatchDataPayloadDTO in body.
     */
    @GetMapping("/fitbit-watch-data/payload")
    public ResponseEntity<List<FitbitWatchDataPayloadDTO>> getAllFitbitWatchDataPayload(FitbitWatchDataCriteria criteria) {
        log.debug("REST request to get fitbitWatchDataPayload by criteria: {}", criteria);
        return ResponseEntity.ok().body(fitbitWatchDataService.findByCriteria(criteria));
    }

    /**
     * {@code GET  /fitbit-watch-data/count} : count all the fitbitWatchData.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fitbit-watch-data/count")
    public ResponseEntity<Long> countFitbitWatchData(FitbitWatchDataCriteria criteria) {
        log.debug("REST request to count fitbitWatchData by criteria: {}", criteria);
        return ResponseEntity.ok().body(fitbitWatchDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fitbit-watch-data/:id} : get the "id" fitbitWatchData.
     *
     * @param id the id of the fitbitWatchDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fitbitWatchDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fitbit-watch-data/{id}")
    public ResponseEntity<FitbitWatchDataDTO> getFitbitWatchData(@PathVariable Long id) {
        log.debug("REST request to get fitbitWatchData by id: {}", id);
        return ResponseEntity.ok(fitbitWatchDataService.findOne(id));
    }

    /**
     * DELETE  /fitbit-watch-data/:id : delete the "id" fitbitWatchData.
     *
     * @param id the id of the fitbitWatchDataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fitbit-watch-data/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteFitbitWatchData(@PathVariable Long id) {
        log.debug("REST request to delete fitbitWatchData : {}", id);
        fitbitWatchDataService.justDelete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
