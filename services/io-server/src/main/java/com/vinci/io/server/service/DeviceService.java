package com.vinci.io.server.service;

import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.NoUserForTokenException;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.web.rest.beans.UserDTO;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DeviceService {

    private final GatewayClient gatewayClient;

    public DeviceService(GatewayClient gatewayClient) {
        this.gatewayClient = gatewayClient;
    }

    /**
     * @param deviceType
     * @return Optional with device of type for user in token, or empty Optional if there is no device of certain type
     */
    public Set<DeviceDTO> getDeviceByTypeForCurrentUser(DeviceType deviceType){
        //todo: do try catch for feign call
        UserDTO currentUser = gatewayClient.getUserWithAuthorities()
            .orElseThrow(NoUserForTokenException::new);
        log.debug("Received UserDTO: {}", currentUser);

        Set<DeviceDTO> devicesForCurrentUserAndType = currentUser
            .getAllDevices()
            .stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(deviceType))
            .collect(Collectors.toSet());

        if (devicesForCurrentUserAndType.isEmpty()) {
            log.debug("No {} devices for userExtraId: {}; userId: {}! Or his associates!", deviceType, currentUser.getUserExtraId(), currentUser.getId());
            return Collections.emptySet();
        }
        return devicesForCurrentUserAndType;
    }

    public Set<DeviceDTO> getPersonalDeviceByType(DeviceType deviceType) {
        //todo: do try catch for feign call
        UserDTO currentUser = gatewayClient.getUserWithAuthorities()
            .orElseThrow(NoUserForTokenException::new);
        log.debug("Received UserDTO: {}", currentUser);

        Set<DeviceDTO> devicesForCurrentUserAndType = currentUser
            .getPersonalDevices()
            .stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(deviceType))
            .collect(Collectors.toSet());

        if (devicesForCurrentUserAndType.isEmpty()) {
            log.debug("No {} personal devices for userExtraId: {}; userId: {}!", deviceType, currentUser.getUserExtraId(), currentUser.getId());
            return Collections.emptySet();
        }
        return devicesForCurrentUserAndType;
    }
}
