package com.vinci.io.server.service;

import com.vinci.io.server.domain.CameraFitnessData;
import com.vinci.io.server.domain.CameraFitnessData_;
import com.vinci.io.server.repository.CameraFitnessDataRepository;
import com.vinci.io.server.service.dto.CameraFitnessDataCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CameraFitnessData entities in the database.
 * The main input is a {@link CameraFitnessDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CameraFitnessData} or a {@link Page} of {@link CameraFitnessData} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CameraFitnessDataQueryService extends QueryService<CameraFitnessData> {

    private final Logger log = LoggerFactory.getLogger(CameraFitnessDataQueryService.class);

    private final CameraFitnessDataRepository cameraFitnessDataRepository;

    public CameraFitnessDataQueryService(CameraFitnessDataRepository cameraFitnessDataRepository) {
        this.cameraFitnessDataRepository = cameraFitnessDataRepository;
    }

    /**
     * Return a {@link List} of {@link CameraFitnessData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CameraFitnessData> findByCriteria(CameraFitnessDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CameraFitnessData> specification = createSpecification(criteria);
        return cameraFitnessDataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CameraFitnessData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CameraFitnessData> findByCriteria(CameraFitnessDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CameraFitnessData> specification = createSpecification(criteria);
        return cameraFitnessDataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CameraFitnessDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CameraFitnessData> specification = createSpecification(criteria);
        return cameraFitnessDataRepository.count(specification);
    }

    /**
     * Function to convert CameraFitnessDataCriteria to a {@link Specification}
     */
    private Specification<CameraFitnessData> createSpecification(CameraFitnessDataCriteria criteria) {
        Specification<CameraFitnessData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CameraFitnessData_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), CameraFitnessData_.timestamp));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), CameraFitnessData_.deviceId));
            }
        }
        return specification;
    }
}
