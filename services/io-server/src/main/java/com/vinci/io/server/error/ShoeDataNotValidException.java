package com.vinci.io.server.error;

import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ShoeDataNotValidException extends AbstractThrowableProblem {

    private static final String TITLE = "SHOE_DATA_NOT_VALID_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "Shoe data is not valid!";
    private static final String DETAILS_WITH_DATA = "Shoe data is not valid! ShoeData.data: %s";

    public ShoeDataNotValidException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public ShoeDataNotValidException(String shoeData) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_DATA,shoeData)); }
    public ShoeDataNotValidException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "ShoeDataNotValidException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
