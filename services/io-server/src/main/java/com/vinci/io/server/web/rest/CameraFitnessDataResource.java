package com.vinci.io.server.web.rest;
import com.vinci.io.server.domain.CameraFitnessData;
import com.vinci.io.server.service.CameraFitnessDataQueryService;
import com.vinci.io.server.service.CameraFitnessDataService;
import com.vinci.io.server.service.dto.CameraFitnessDataCriteria;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CameraFitnessData.
 */
@RestController
@RequestMapping("/api")
public class CameraFitnessDataResource {

    private final Logger log = LoggerFactory.getLogger(CameraFitnessDataResource.class);

    private static final String ENTITY_NAME = "ioserverCameraFitnessData";

    private final CameraFitnessDataService cameraFitnessDataService;

    private final CameraFitnessDataQueryService cameraFitnessDataQueryService;

    public CameraFitnessDataResource(CameraFitnessDataService cameraFitnessDataService, CameraFitnessDataQueryService cameraFitnessDataQueryService) {
        this.cameraFitnessDataService = cameraFitnessDataService;
        this.cameraFitnessDataQueryService = cameraFitnessDataQueryService;
    }

    /**
     * POST  /camera-fitness-data : Create a new cameraFitnessData.
     *
     * @param cameraFitnessData the cameraFitnessData to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cameraFitnessData, or with status 400 (Bad Request) if the cameraFitnessData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/camera-fitness-data")
    public ResponseEntity<CameraFitnessData> createCameraFitnessData(@RequestBody CameraFitnessData cameraFitnessData) throws URISyntaxException {
        log.debug("REST request to save CameraFitnessData : {}", cameraFitnessData);
        if (cameraFitnessData.getId() != null) {
            throw new BadRequestAlertException("A new cameraFitnessData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CameraFitnessData result = cameraFitnessDataService.save(cameraFitnessData);
        return ResponseEntity.created(new URI("/api/camera-fitness-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /camera-fitness-data : Updates an existing cameraFitnessData.
     *
     * @param cameraFitnessData the cameraFitnessData to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cameraFitnessData,
     * or with status 400 (Bad Request) if the cameraFitnessData is not valid,
     * or with status 500 (Internal Server Error) if the cameraFitnessData couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/camera-fitness-data")
    public ResponseEntity<CameraFitnessData> updateCameraFitnessData(@RequestBody CameraFitnessData cameraFitnessData) throws URISyntaxException {
        log.debug("REST request to update CameraFitnessData : {}", cameraFitnessData);
        if (cameraFitnessData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CameraFitnessData result = cameraFitnessDataService.save(cameraFitnessData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cameraFitnessData.getId().toString()))
            .body(result);
    }

    /**
     * GET  /camera-fitness-data : get all the cameraFitnessData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of cameraFitnessData in body
     */
    @GetMapping("/camera-fitness-data")
    public ResponseEntity<List<CameraFitnessData>> getAllCameraFitnessData(CameraFitnessDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CameraFitnessData by criteria: {}", criteria);
        Page<CameraFitnessData> page = cameraFitnessDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/camera-fitness-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /camera-fitness-data/count : count all the cameraFitnessData.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/camera-fitness-data/count")
    public ResponseEntity<Long> countCameraFitnessData(CameraFitnessDataCriteria criteria) {
        log.debug("REST request to count CameraFitnessData by criteria: {}", criteria);
        return ResponseEntity.ok().body(cameraFitnessDataQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /camera-fitness-data/:id : get the "id" cameraFitnessData.
     *
     * @param id the id of the cameraFitnessData to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cameraFitnessData, or with status 404 (Not Found)
     */
    @GetMapping("/camera-fitness-data/{id}")
    public ResponseEntity<CameraFitnessData> getCameraFitnessData(@PathVariable Long id) {
        log.debug("REST request to get CameraFitnessData : {}", id);
        Optional<CameraFitnessData> cameraFitnessData = cameraFitnessDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cameraFitnessData);
    }

    /**
     * DELETE  /camera-fitness-data/:id : delete the "id" cameraFitnessData.
     *
     * @param id the id of the cameraFitnessData to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/camera-fitness-data/{id}")
    public ResponseEntity<Void> deleteCameraFitnessData(@PathVariable Long id) {
        log.debug("REST request to delete CameraFitnessData : {}", id);
        cameraFitnessDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
