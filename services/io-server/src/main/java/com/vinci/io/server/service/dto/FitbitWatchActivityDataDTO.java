package com.vinci.io.server.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FitbitWatchActivityDataDTO {
    Integer activityCalories;
    Integer caloriesBMR;
    Integer caloriesOut;
    Integer elevation;
    Integer fairlyActiveMinutes;
    Integer floors;
    Integer lightlyActiveMinutes;
    Integer marginalCalories;
    Integer restingHeartRate;
    Integer sedentaryMinutes;
    Integer steps;
    Integer veryActiveMinutes;
    List<HeartRateZones> heartRateZones;
    List<Distance> distances;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Distance {
        String activity;
        String distance;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HeartRateZones {

        private Integer caloriesOut;
        private Integer max;
        private Integer min;
        private Integer minutes;
        private String name;
    }
}
