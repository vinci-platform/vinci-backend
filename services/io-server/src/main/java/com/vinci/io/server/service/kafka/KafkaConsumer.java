package com.vinci.io.server.service.kafka;

import com.vinci.io.server.config.kafka.TopicConstants;
import com.vinci.io.server.service.FitbitWatchDataService;
import com.vinci.io.server.service.FitbitWatchIntradayDataService;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class KafkaConsumer {

    private final FitbitWatchDataService fitbitWatchDataService;
    private final FitbitWatchIntradayDataService fitbitWatchIntradayDataService;

    public KafkaConsumer(FitbitWatchDataService fitbitWatchDataService, FitbitWatchIntradayDataService fitbitWatchIntradayDataService) {
        this.fitbitWatchDataService = fitbitWatchDataService;
        this.fitbitWatchIntradayDataService = fitbitWatchIntradayDataService;
    }

    @KafkaListener(topics = TopicConstants.TOPIC_FITBIT_WATCH_DATA, containerFactory = "fitbitWatchDataDTOKafkaListenerContainerFactory")
    public void consumeFitbitWatchData(FitbitWatchDataDTO fitbitWatchData) {
        log.debug("fitbitWatchDataString received: {} ; from topic: {}",fitbitWatchData, TopicConstants.TOPIC_FITBIT_WATCH_DATA);
        fitbitWatchDataService.saveOrUpdateNewKafkaRecord(fitbitWatchData);
    }

    @KafkaListener(topics = TopicConstants.TOPIC_FITBIT_WATCH_INTRADAY_DATA, containerFactory = "fitbitWatchIntradayDataDTOKafkaListenerContainerFactory")
    public void consumeFitbitWatchIntradayData(FitbitWatchIntradayDataDTO fitbitWatchIntradayDataDTO) {
        log.debug("fitbitWatchIntradayDataDTO received: {} ; from topic: {}",fitbitWatchIntradayDataDTO, TopicConstants.TOPIC_FITBIT_WATCH_INTRADAY_DATA);
        try {
            fitbitWatchIntradayDataService.saveOrUpdateNewKafkaRecord(fitbitWatchIntradayDataDTO);
        } catch (IOException e) {
            log.error("There has been an error parsing in saveOrUpdateNewKafkaRecord(fitbitWatchIntradayDataDTO) method");
            log.error("Error in saveOrUpdateNewKafkaRecord", e);
        }
    }
}
