package com.vinci.io.server.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link FitbitWatchIntradayDataDTO} entity.
 */
public class FitbitWatchIntradayDataDTO implements Serializable {

    private Long id;

    private String data;

    private Long timestamp;

    private Long deviceId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchIntradayDataDTO)) {
            return false;
        }

        return id != null && id.equals(((FitbitWatchIntradayDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FitbitWatchIntradayDataDTO{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", timestamp=" + getTimestamp() +
            ", deviceId=" + getDeviceId() +
            "}";
    }
}
