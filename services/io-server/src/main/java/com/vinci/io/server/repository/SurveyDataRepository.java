package com.vinci.io.server.repository;

import com.vinci.io.server.domain.SurveyData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SurveyData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SurveyDataRepository extends JpaRepository<SurveyData, Long>, JpaSpecificationExecutor<SurveyData> {

}
