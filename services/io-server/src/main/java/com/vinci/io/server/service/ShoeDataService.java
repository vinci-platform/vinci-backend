package com.vinci.io.server.service;

import com.vinci.io.server.domain.ShoeData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.*;
import com.vinci.io.server.repository.ShoeDataRepository;
import com.vinci.io.server.service.dto.*;
import com.vinci.io.server.service.mapper.ShoeDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Set;

/**
 * Service Implementation for managing ShoeData.
 */
@Service
@Transactional
public class ShoeDataService {

    private static final String ENTITY_NAME = "ioserverShoeData";
    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.SHOE;

    private final Logger log = LoggerFactory.getLogger(ShoeDataService.class);

    private final ShoeDataRepository shoeDataRepository;

    private final ShoeDataMapper shoeDataMapper;

    private final ObjectMapper objectMapper;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public ShoeDataService(ShoeDataRepository shoeDataRepository, ShoeDataMapper shoeDataMapper, DeviceService deviceService,
                           DeviceServiceUtility deviceServiceUtility) {
        this.shoeDataRepository = shoeDataRepository;
        this.shoeDataMapper = shoeDataMapper;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Save a custom shoeData.
     *
     * @param createShoeDataDto the model to save
     * @return the persisted entity
     * @throws InternalServerErrorException if the device was not found in gateway
     */
    public ShoeData saveDto(CreateShoeDataDto createShoeDataDto) throws InternalServerErrorException {
        log.debug("Request to save "+ENTITY_NAME+" : {}", createShoeDataDto);
        validateShoeDataForCreation(createShoeDataDto);

        Set<DeviceDTO> shoeDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (shoeDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, createShoeDataDto.getDeviceId());
        }

        DeviceDTO shoeDevice = deviceServiceUtility.getByDeviceIdAndTypeFromSet(shoeDevicesForCurrentUser, createShoeDataDto.getDeviceId(), ENTITY_DEVICE_TYPE);

        return shoeDataRepository.save(shoeDataMapper.toEntity(createShoeDataDto, shoeDevice.getId()));
    }

    /**
     * Save a shoeData.
     *
     * @param shoeData the entity to save
     * @return the persisted entity
     */
    public ShoeData justSave(ShoeData shoeData) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", shoeData);
        return shoeDataRepository.save(shoeData);
    }

    /**
     * Get one shoeData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShoeDataDTO findOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" by id: {}", id);

        Set<DeviceDTO> shoeDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (shoeDevicesForCurrentUser.isEmpty()) {
            log.debug("No devices for type {} found!",ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        ShoeData shoeData = shoeDataRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        deviceServiceUtility.validateDevices(id, shoeDevicesForCurrentUser, shoeData.getDeviceId(), ENTITY_NAME, ENTITY_DEVICE_TYPE);

        return createShoeDataDto(shoeDevicesForCurrentUser, shoeData);
    }

    /**
     * Delete the shoeData by id.
     *
     * @param id the id of the entity
     */
    public void justDelete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);
        shoeDataRepository.deleteById(id);
    }

    public ShoeData update(ShoeData shoeData) {
        log.debug("Request to update "+ENTITY_NAME+" : {}", shoeData);
        validateShoeDataForUpdate(shoeData);
        try {
            objectMapper.readValue(shoeData.getData(), ShoeDataPayloadDTO.class);
        }
        catch (IOException e) {
            log.debug("Could not convert "+ENTITY_NAME+": " + shoeData.getData());
            throw new ShoeDataNotValidException(shoeData.getData());
        }

        Set<DeviceDTO> shoeDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (shoeDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, shoeData.getDeviceId());
        }

        deviceServiceUtility.getByDeviceIdAndTypeFromSet(shoeDevicesForCurrentUser, shoeData.getDeviceId(), ENTITY_DEVICE_TYPE);

        return shoeDataRepository.save(shoeData);
    }

    private ShoeDataDTO createShoeDataDto(Set<DeviceDTO> shoeDevicesForCurrentUser, ShoeData shoeData) {
        ShoeDataDTO shoeDataDTO = new ShoeDataDTO();
        shoeDataDTO.setId(shoeData.getId());
        try {
            shoeDataDTO.setData(objectMapper.readValue(shoeData.getData(), ShoeDataPayloadDTO.class));
        } catch (IOException e) {
            log.error(ENTITY_NAME+".data in database is not valid. "+ENTITY_NAME+": {}", shoeData);
            throw new ShoeDataInDatabaseNotValid(shoeData);
        }
        shoeDataDTO.setTimestamp(shoeData.getTimestamp());
        shoeDataDTO.setDevice(
            shoeDevicesForCurrentUser
                .stream()
                .filter(deviceDTO -> deviceDTO.getId().equals(shoeData.getDeviceId()))
                .findFirst()
                .get());
        return shoeDataDTO;
    }

    private void validateShoeDataForUpdate(ShoeData shoeData) {
        if (shoeData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (shoeData.getDeviceId() == null){
            log.debug("There is no deviceID for {}!",ENTITY_NAME);
            throw new BadRequestAlertException("A new"+ENTITY_NAME+" must have deviceId", ENTITY_NAME, "deviceIdDoesntExist");
        }
    }

    private void validateShoeDataForCreation(CreateShoeDataDto createShoeDataDto) {
        try {
            objectMapper.readValue(createShoeDataDto.getData(), ShoeDataPayloadDTO.class);
        }
        catch (IOException e) {
            log.debug("Could not convert "+ENTITY_NAME+": " + createShoeDataDto.getData());
            throw new ShoeDataNotValidException(createShoeDataDto.getData());
        }
    }
}
