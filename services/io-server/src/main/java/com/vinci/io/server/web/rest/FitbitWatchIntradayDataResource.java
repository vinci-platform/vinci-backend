package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.FitbitWatchIntradayData;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.FitbitWatchIntradayDataQueryService;
import com.vinci.io.server.service.FitbitWatchIntradayDataService;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataCriteria;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataPayloadDTO;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing {@link FitbitWatchIntradayData}.
 */
@RestController
@RequestMapping("/api")
public class FitbitWatchIntradayDataResource {

    private final Logger log = LoggerFactory.getLogger(FitbitWatchIntradayDataResource.class);

    private static final String ENTITY_NAME = "ioserverFitbitWatchIntradayData";

    private final FitbitWatchIntradayDataService fitbitWatchIntradayDataService;

    private final FitbitWatchIntradayDataQueryService fitbitWatchIntradayDataQueryService;

    public FitbitWatchIntradayDataResource(FitbitWatchIntradayDataService fitbitWatchIntradayDataService, FitbitWatchIntradayDataQueryService fitbitWatchIntradayDataQueryService) {
        this.fitbitWatchIntradayDataService = fitbitWatchIntradayDataService;
        this.fitbitWatchIntradayDataQueryService = fitbitWatchIntradayDataQueryService;
    }

    /**
     * {@code POST  /fitbit-watch-intraday-data} : Create a new fitbitwatchintradaydata.
     *
     * @param fitbitWatchIntradayData the fitbitWatchIntradayData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fitbitWatchIntradayData, or with status {@code 400 (Bad Request)} if the fitbitWatchIntradayData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fitbit-watch-intraday-data")
    public ResponseEntity<FitbitWatchIntradayData> createFitbitWatchIntradayData(@RequestBody @Valid FitbitWatchIntradayData fitbitWatchIntradayData) throws URISyntaxException {
        log.debug("REST request to save fitbitWatchIntradayData : {}", fitbitWatchIntradayData);
        FitbitWatchIntradayData result = fitbitWatchIntradayDataService.save(fitbitWatchIntradayData);
        return ResponseEntity.created(new URI("/api/fitbit-watch-intraday-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fitbit-watch-intraday-data} : Updates an existing fitbitwatchintradaydata.
     *
     * @param fitbitWatchIntradayData the fitbitwatchintradaydata to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fitbitWatchIntradayData,
     * or with status {@code 400 (Bad Request)} if the fitbitWatchIntradayData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fitbitWatchIntradayData couldn't be updated.
     */
    @PutMapping("/fitbit-watch-intraday-data")
    public ResponseEntity<FitbitWatchIntradayData> updateFitbitWatchIntradayData(@RequestBody @Valid FitbitWatchIntradayData fitbitWatchIntradayData) {
        log.debug("REST request to update fitbitWatchIntradayData : {}", fitbitWatchIntradayData);
        FitbitWatchIntradayData result = fitbitWatchIntradayDataService.update(fitbitWatchIntradayData);
        return ResponseEntity.ok()
             .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fitbit-watch-intraday-data : get all the fitbitWatchIntradayData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of fitbitWatchIntradayData in body
     */
    @GetMapping("/fitbit-watch-intraday-data")
    public ResponseEntity<List<FitbitWatchIntradayDataDTO>> getAllFitbitWatchIntradayData(FitbitWatchIntradayDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get FitbitWatchIntradayData by criteria: {}", criteria);
        Page<FitbitWatchIntradayDataDTO> page = fitbitWatchIntradayDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fitbit-watch-intraday-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fitbit-watch-intraday-data/payload} : get all the FitbitWatchIntradayDataPayloadDTO.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of FitbitWatchIntradayDataPayloadDTO in body.
     */
    @GetMapping("/fitbit-watch-intraday-data/payload")
    public ResponseEntity<List<FitbitWatchIntradayDataPayloadDTO>> getAllFitbitWatchIntradayDataPayload(FitbitWatchIntradayDataCriteria criteria) {
        log.debug("REST request to get FitbitWatchIntradayData by criteria: {}", criteria);
        return ResponseEntity.ok().body(fitbitWatchIntradayDataService.findByCriteria(criteria));
    }

    /**
     * {@code GET  /fitbit-watch-intraday-data/count} : count all the fitbitwatchintradaydata.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fitbit-watch-intraday-data/count")
    public ResponseEntity<Long> countFitbitWatchIntradayData(FitbitWatchIntradayDataCriteria criteria) {
        log.debug("REST request to count FitbiWatchIntradayData by criteria: {}", criteria);
        return ResponseEntity.ok().body(fitbitWatchIntradayDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fitbit-watch-intraday-data/:id} : get the "id" fitbitwatchintradaydata.
     *
     * @param id the id of the fitbitWatchIntradayDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the FitbitWatchIntradayDataDTO}.
     */
    @GetMapping("/fitbit-watch-intraday-data/{id}")
    public ResponseEntity<FitbitWatchIntradayDataDTO> getFitbitWatchIntradayData(@PathVariable Long id) {
        log.debug("REST request to get fitbitwatchintradaydata by id: {}", id);
        return ResponseEntity.ok(fitbitWatchIntradayDataService.findOne(id));
    }

    /**
     * {@code DELETE  /fitbit-watch-intraday-data/:id} : delete the "id" fitbitwatchintradaydata.
     *
     * @param id the id of the fitbitwatchintradaydataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fitbit-watch-intraday-data/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteFitbitWatchIntradayData(@PathVariable Long id) {
        log.debug("REST request to delete fitbitwatchintradaydata : {}", id);
        fitbitWatchIntradayDataService.justDelete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping(value = "/fitbit-watch-intraday-data/sync")
    public ResponseEntity<String> syncFitbitWatchIntradayData(@RequestBody String fitbitWatchIntradayData) throws IOException {
        log.debug("REST request to save a list of fitbitWatchIntradayData: {}", fitbitWatchIntradayData);
        return ResponseEntity
            .ok(fitbitWatchIntradayDataService.saveSync(fitbitWatchIntradayData));
    }

    @GetMapping("/fitbit-watch-intraday-data/latest-timestamp/{userLogin}")
    public ResponseEntity<Long> getLatestTimestampForUser(@PathVariable String userLogin){
        log.debug("REST request to get latest timestamp for fitbit intraday data, for userLogin: {}", userLogin);
        return ResponseEntity.ok().body(fitbitWatchIntradayDataService.getLatestTimestampByUserLogin(userLogin));
    }
}
