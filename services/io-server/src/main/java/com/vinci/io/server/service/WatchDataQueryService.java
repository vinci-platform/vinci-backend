package com.vinci.io.server.service;

import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.domain.WatchData_;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.WatchDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.WatchDataCriteria;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for WatchData entities in the database.
 * The main input is a {@link WatchDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WatchData} or a {@link Page} of {@link WatchData} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WatchDataQueryService extends QueryService<WatchData> {

    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.WATCH;
    private final Logger log = LoggerFactory.getLogger(WatchDataQueryService.class);

    private final WatchDataRepository watchDataRepository;

    private final GatewayClient gatewayClient;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public WatchDataQueryService(WatchDataRepository watchDataRepository, GatewayClient gatewayClient, DeviceService deviceService,
                                 DeviceServiceUtility deviceServiceUtility) {
        this.watchDataRepository = watchDataRepository;
        this.gatewayClient = gatewayClient;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Return a {@link List} of {@link WatchData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WatchData> justFindByCriteria(WatchDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<WatchData> specification = createSpecificationAndCheckDeviceFromGateway(criteria);
        return watchDataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link WatchData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WatchData> findByCriteria(WatchDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        log.debug("Received {} devices: {}",ENTITY_DEVICE_TYPE,devicesForCurrentUser);
        if (devicesForCurrentUser.isEmpty()) {
            log.debug("There is no {} for current user!", ENTITY_DEVICE_TYPE);
            return new PageImpl<>(Collections.emptyList());
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        Instant timestamp = devicesForCurrentUser.stream().findAny().get().getStartTimestamp();
        if (timestamp != null) {
            Long startTimestampMillis = timestamp.toEpochMilli();
            LongFilter timestampFilter = new LongFilter();
            timestampFilter.setGreaterOrEqualThan(startTimestampMillis);
            criteria.setTimestamp(timestampFilter);
        }

        final Specification<WatchData> specification = createSpecification(criteria);
        return watchDataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WatchDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<WatchData> specification = createSpecificationAndCheckDeviceFromGateway(criteria);
        return watchDataRepository.count(specification);
    }

    /**
     * Function to convert WatchDataCriteria to a {@link Specification}
     */
    private Specification<WatchData> createSpecificationAndCheckDeviceFromGateway(WatchDataCriteria criteria) {
        Specification<WatchData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getDeviceId() != null) {
                Optional<DeviceDTO> deviceDTO = gatewayClient.getDeviceById(criteria.getDeviceId().getEquals());
                if (!deviceDTO.isPresent()) {
                    throw new InternalServerErrorException("The device with id " + criteria.getDeviceId().getEquals() + " was not found in gateway");
                }
                Instant timestamp = deviceDTO.get().getStartTimestamp();
                if (timestamp != null) {
                    Long startTimestampMillis = timestamp.toEpochMilli();
                    LongFilter timestampFilter = new LongFilter();
                    timestampFilter.setGreaterOrEqualThan(startTimestampMillis);
                    criteria.setTimestamp(timestampFilter);
                }

                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), WatchData_.deviceId));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WatchData_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), WatchData_.timestamp));
            }
        }
        return specification;
    }
    /**
     * Function to convert WatchDataCriteria to a {@link Specification}
     */
    private Specification<WatchData> createSpecification(WatchDataCriteria criteria) {
        Specification<WatchData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), WatchData_.deviceId));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WatchData_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), WatchData_.timestamp));
            }
        }
        return specification;
    }
}
