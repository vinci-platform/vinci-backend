package com.vinci.io.server.service;

import com.vinci.io.server.domain.CameraMovementData;
import com.vinci.io.server.repository.CameraMovementDataRepository;
import com.vinci.io.server.service.dto.CreateCameraMovementDataDto;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.mapper.CameraMovementDataMapper;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CameraMovementData.
 */
@Service
@Transactional
public class CameraMovementDataService {

    private final Logger log = LoggerFactory.getLogger(CameraMovementDataService.class);

    private final CameraMovementDataRepository cameraMovementDataRepository;

    private final CameraMovementDataMapper cameraMovementDataMapper;

    private final GatewayClient gatewayClient;

    public CameraMovementDataService(CameraMovementDataRepository cameraMovementDataRepository, GatewayClient gatewayClient, CameraMovementDataMapper cameraMovementDataMapper) {
        this.cameraMovementDataRepository = cameraMovementDataRepository;
        this.gatewayClient = gatewayClient;
        this.cameraMovementDataMapper = cameraMovementDataMapper;
    }

    /**
     * Save a custom cameraMovementData.
     *
     * @param createCameraMovementDataDto the model to save
     * @return the persisted entity
     * @throws InternalServerErrorException if the device was not found in gateway
     */
    public CameraMovementData saveDto(CreateCameraMovementDataDto createCameraMovementDataDto) throws InternalServerErrorException {
        log.debug("Request to save CreateWatchDataDto : {}", createCameraMovementDataDto);
        Optional<DeviceDTO> deviceDTO = gatewayClient.getDeviceByUid(createCameraMovementDataDto.getDeviceUUID());
        if (!deviceDTO.isPresent()) {
            throw new InternalServerErrorException("The device with uuid " + createCameraMovementDataDto.getDeviceUUID() + " was not found in gateway");
        }
        CameraMovementData cameraMovementData = cameraMovementDataMapper.toEntity(createCameraMovementDataDto, deviceDTO.get().getId());
        return cameraMovementDataRepository.save(cameraMovementData);
    }

    /**
     * Save a cameraMovementData.
     *
     * @param cameraMovementData the entity to save
     * @return the persisted entity
     */
    public CameraMovementData save(CameraMovementData cameraMovementData) {
        log.debug("Request to save CameraMovementData : {}", cameraMovementData);
        return cameraMovementDataRepository.save(cameraMovementData);
    }

    /**
     * Get all the cameraMovementData.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CameraMovementData> findAll(Pageable pageable) {
        log.debug("Request to get all CameraMovementData");
        return cameraMovementDataRepository.findAll(pageable);
    }


    /**
     * Get one cameraMovementData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CameraMovementData> findOne(Long id) {
        log.debug("Request to get CameraMovementData : {}", id);
        return cameraMovementDataRepository.findById(id);
    }

    /**
     * Delete the cameraMovementData by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CameraMovementData : {}", id);
        cameraMovementDataRepository.deleteById(id);
    }
}
