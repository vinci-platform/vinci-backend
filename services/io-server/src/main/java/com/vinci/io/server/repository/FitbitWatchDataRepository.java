package com.vinci.io.server.repository;

import com.vinci.io.server.domain.FitbitWatchData;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Fitbitwatchdata entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FitbitWatchDataRepository extends JpaRepository<FitbitWatchData, Long>, JpaSpecificationExecutor<FitbitWatchData> {
    List<FitbitWatchData> findAllByTimestampAndDeviceId(Long timestamp, Long deviceId);
}
