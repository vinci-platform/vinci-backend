package com.vinci.io.server.error.fitbit;

import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class RefreshTokenException extends AbstractThrowableProblem {
    private static final String TITLE = "REFRESH_TOKEN_EXCEPTION";
    private static final Status STATUS_TYPE = Status.UNAUTHORIZED;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    public RefreshTokenException(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }
    public RefreshTokenException(ErrorResponse feignErrorResponse) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail());
    }

    @Override
    public String toString() {
        return "RefreshTokenException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
