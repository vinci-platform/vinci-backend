package com.vinci.io.server.service;

import com.google.common.base.Strings;
import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.error.FitbitWatchDataCanNotBeParsedFromStringException;
import com.vinci.io.server.error.NoDeviceFoundException;
import com.vinci.io.server.error.NoEntityException;
import com.vinci.io.server.repository.FitbitWatchDataRepository;
import com.vinci.io.server.service.dto.*;
import com.vinci.io.server.service.mapper.FitbitWatchDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing FitbitWatchData.
 */
@Service
@Transactional
public class FitbitWatchDataService {

    private static final String ENTITY_NAME = "ioserverFitbitWatchData";
    private final DeviceType ENTITY_DEVICE_TYPE = DeviceType.FITBIT_WATCH;

    private final Logger log = LoggerFactory.getLogger(FitbitWatchDataService.class);

    private final FitbitWatchDataRepository fitbitWatchDataRepository;

    private final FitbitWatchDataMapper fitbitWatchDataMapper;

    private final FitbitWatchDataQueryService fitbitWatchDataQueryService;

    private final DeviceService deviceService;

    private final ObjectMapper objectMapper;

    private final DeviceServiceUtility deviceServiceUtility;

    public FitbitWatchDataService(FitbitWatchDataRepository fitbitWatchDataRepository, FitbitWatchDataMapper fitbitWatchDataMapper,
                                  FitbitWatchDataQueryService fitbitWatchDataQueryService, DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.fitbitWatchDataRepository = fitbitWatchDataRepository;
        this.fitbitWatchDataMapper = fitbitWatchDataMapper;
        this.fitbitWatchDataQueryService = fitbitWatchDataQueryService;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Save a FitbitWatchData.
     *
     * @param fitbitWatchDataDTO the entity to save.
     * @return the persisted entity or null if there is exception
     */
    public FitbitWatchDataDTO saveDTO(FitbitWatchDataDTO fitbitWatchDataDTO) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", fitbitWatchDataDTO);
        validateFitbitWatchDataDtoForCreation(fitbitWatchDataDTO);

        Set<DeviceDTO> fitbitWatchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitWatchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, fitbitWatchDataDTO.getDeviceId());
        }
        deviceServiceUtility.getByDeviceIdAndTypeFromSet(fitbitWatchDevicesForCurrentUser, fitbitWatchDataDTO.getDeviceId(),ENTITY_DEVICE_TYPE);

        return justSave(fitbitWatchDataDTO);
    }

    /**
     * Just saves fitbitWatchData entity.
     *
     * @param fitbitWatchDataDTO the entity to save
     * @return the persisted entity
     */
    public FitbitWatchDataDTO justSave(FitbitWatchDataDTO fitbitWatchDataDTO) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", fitbitWatchDataDTO);
        FitbitWatchData fitbitwatchdata = fitbitWatchDataMapper.toEntity(fitbitWatchDataDTO, fitbitWatchDataDTO.getId());
        fitbitwatchdata = fitbitWatchDataRepository.save(fitbitwatchdata);
        return fitbitWatchDataMapper.toDto(fitbitwatchdata);
    }

    /**
     * Just saves fitbitWatchData entity.
     *
     * @param fitbitWatchData the entity to save
     * @return the persisted entity
     */
    public FitbitWatchData justSave(FitbitWatchData fitbitWatchData) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", fitbitWatchData);
        return fitbitWatchDataRepository.save(fitbitWatchData);
    }

    /**
     * Save a FitbitWatchData.
     *
     * @param fitbitWatchDataString string entity to save
     * @return message of successfully created entity or null if there is exception
     */
    public String saveSync(String fitbitWatchDataString) {
        log.debug("Request to save sync "+ENTITY_NAME+" : {}", fitbitWatchDataString);
        FitbitWatchData fitbitWatchDataToSave = getFitbitWatchDataFromString(fitbitWatchDataString);
        validateFitbitWatchDataForCreation(fitbitWatchDataToSave);

        Set<DeviceDTO> fitbitWatchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitWatchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }
        deviceServiceUtility.getByDeviceIdAndTypeFromSet(fitbitWatchDevicesForCurrentUser, fitbitWatchDataToSave.getDeviceId(), ENTITY_DEVICE_TYPE);

        justSave(fitbitWatchDataToSave);

        return Strings.lenientFormat("Successfully saved a "+ENTITY_NAME+" entity");
    }

    /**
     * Get one fitbitWatchData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public FitbitWatchDataDTO findOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" by id: {}", id);

        Set<DeviceDTO> fitbitDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitDevicesForCurrentUser.isEmpty()) {
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        FitbitWatchData fitbitWatchData = fitbitWatchDataRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        deviceServiceUtility.validateDevices(id, fitbitDevicesForCurrentUser, fitbitWatchData.getDeviceId(), ENTITY_NAME, ENTITY_DEVICE_TYPE);

        return fitbitWatchDataMapper.toDto(fitbitWatchData);
    }

    /**
     * Delete the fitbitWatchData by id.
     *
     * @param id the id of the entity
     */
    public void justDelete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);
        fitbitWatchDataRepository.deleteById(id);
    }

    public List<FitbitWatchDataPayloadDTO> findByCriteria(FitbitWatchDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (devicesForCurrentUser.isEmpty()) {
            return Collections.emptyList();
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        List<FitbitWatchDataDTO> queryList = fitbitWatchDataQueryService.justFindByCriteria(criteria);
        return getFitbitWatchDataPayloadFromDto(queryList, devicesForCurrentUser);
    }

    /**
     * Update a FitbitWatchData.
     *
     * @param fitbitWatchDataDTO the entity to save.
     * @return the persisted entity or null if there is exception
     */
    public FitbitWatchDataDTO updateDto(FitbitWatchDataDTO fitbitWatchDataDTO) {
        log.debug("Request to update "+ENTITY_NAME+" : {}", fitbitWatchDataDTO);
        validateFitbitWatchDataDtoForUpdate(fitbitWatchDataDTO);

        Set<DeviceDTO> fitbitWatchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitWatchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, fitbitWatchDataDTO.getDeviceId());
        }
        deviceServiceUtility.getByDeviceIdAndTypeFromSet(fitbitWatchDevicesForCurrentUser, fitbitWatchDataDTO.getDeviceId(), ENTITY_DEVICE_TYPE);

        return justSave(fitbitWatchDataDTO);
    }

    /**
     * Method for saving records that are sent from fitbit watch service
     */
    public void saveOrUpdateNewKafkaRecord(FitbitWatchDataDTO fitbitWatchDataDTO) {
        log.debug("saveOrUpdateNewKafkaRecord(): {}", fitbitWatchDataDTO);
        FitbitWatchData fitbitWatchDataToSaveOrUpdate = fitbitWatchDataMapper.toEntity(fitbitWatchDataDTO,fitbitWatchDataDTO.getId());
        List<FitbitWatchData> allByTimestampAndDeviceId =
            fitbitWatchDataRepository.findAllByTimestampAndDeviceId(fitbitWatchDataToSaveOrUpdate.getTimestamp(), fitbitWatchDataToSaveOrUpdate.getDeviceId());
        if (allByTimestampAndDeviceId.size() != 0) {
            FitbitWatchDataDTO existingFitbitWatchDataDto = getOneFromList(allByTimestampAndDeviceId);
            fitbitWatchDataToSaveOrUpdate.setId(existingFitbitWatchDataDto.getId());
        }
        justSave(fitbitWatchDataToSaveOrUpdate);
    }

    private List<FitbitWatchDataPayloadDTO> getFitbitWatchDataPayloadFromDto(List<FitbitWatchDataDTO> queryList, Set<DeviceDTO> devicesForCurrentUser) {

        List<FitbitWatchDataPayloadDTO> payloadList = new ArrayList<>();
        queryList.forEach(fitbitWatchData -> {
            FitbitWatchDataPayloadDTO fitbitWatchDataDTO = new FitbitWatchDataPayloadDTO();
            fitbitWatchDataDTO.setId(fitbitWatchData.getId());
            try {
                FitbitWatchSleepDataDTO fitbitWatchSleepDataDTO = objectMapper.readValue(fitbitWatchData.getSleepData(), FitbitWatchSleepDataDTO.class);
                FitbitWatchActivityDataDTO fitbitWatchActivityDataDTO = objectMapper.readValue(fitbitWatchData.getActivityData(), FitbitWatchActivityDataDTO.class);
                fitbitWatchDataDTO.setActivityData(fitbitWatchActivityDataDTO);
                fitbitWatchDataDTO.setSleepData(fitbitWatchSleepDataDTO);
            } catch (IOException e) {
                throw new InternalServerErrorException("Could not convert "+ENTITY_NAME+" (from database)" + fitbitWatchData.getSleepData() +
                    fitbitWatchData.getActivityData());
            }
            fitbitWatchDataDTO.setTimestamp(fitbitWatchData.getTimestamp());
            fitbitWatchDataDTO.setDevice(devicesForCurrentUser.stream().filter(deviceDTO -> deviceDTO.getId().equals(fitbitWatchData.getDeviceId())).findAny().get());
            payloadList.add(fitbitWatchDataDTO);
        });
        return payloadList;
    }

    private FitbitWatchDataDTO getOneFromList(List<FitbitWatchData> fitbitWatchDataList) {
        int listSize = fitbitWatchDataList.size();
        if (listSize == 0) throw new RuntimeException("getOneFromList(List<FitbitWatchData> list): list should not be null null!!!");
        if (listSize == 1) return fitbitWatchDataMapper.toDto(fitbitWatchDataList.get(0));

        log.error("there are {} fitbitWatchData records in database with timestamp {} and deviceId: {}. Some will be deleted so only one can remain!!!",
            listSize,
            fitbitWatchDataList.get(0).getTimestamp(),
            fitbitWatchDataList.get(0).getDeviceId());
        return deleteDuplicates(fitbitWatchDataList);
    }

    private FitbitWatchDataDTO deleteDuplicates(List<FitbitWatchData> listWithDuplicates) {
        FitbitWatchData fitbitWatchData = listWithDuplicates.get(0);
        listWithDuplicates.remove(0);
        listWithDuplicates.forEach(fitbitWatchDataRepository::delete);
        fitbitWatchDataRepository.flush();

        return fitbitWatchDataMapper.toDto(fitbitWatchData);
    }

    private void validateFitbitWatchDataForCreation(FitbitWatchData fitbitWatchDataToSave) {
        if (fitbitWatchDataToSave.getId() != null){
            log.debug("There is no id for {}!",ENTITY_NAME);
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" must not have id", ENTITY_NAME, "idNotNull");
        }
        if (fitbitWatchDataToSave.getDeviceId() == null){
            log.debug("There is no deviceID for {}!",ENTITY_NAME);
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" must have deviceId", ENTITY_NAME, "deviceIdDoesntExist");
        }
        if (fitbitWatchDataToSave.getTimestamp() == null){
            log.debug("There is no timestamp for {}!",ENTITY_NAME);
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" must have timestamp", ENTITY_NAME, "timestampDoesntExist");
        }
    }

    private FitbitWatchData getFitbitWatchDataFromString(String fitbitWatchDataString) {
        try {
            return objectMapper.readValue(fitbitWatchDataString, FitbitWatchData.class);
        } catch (IOException exception) {
            log.error("FitbitWatchData cant be parsed from string. Data: {}",fitbitWatchDataString);
            throw new FitbitWatchDataCanNotBeParsedFromStringException(fitbitWatchDataString);
        }
    }

    private void validateFitbitWatchDataDtoForUpdate(FitbitWatchDataDTO fitbitWatchDataDTO) {
        if (fitbitWatchDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    private void validateFitbitWatchDataDtoForCreation(FitbitWatchDataDTO fitbitWatchDataDTO) {
        if (fitbitWatchDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new fitbitwatchdata cannot already have an ID", ENTITY_NAME, "id exists");
        }
    }

}








