package com.vinci.io.server.service;

import com.vinci.io.server.domain.SurveyData;
import com.vinci.io.server.domain.SurveyData_;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.SurveyDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.SurveyDataCriteria;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for SurveyData entities in the database.
 * The main input is a {@link SurveyDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SurveyData} or a {@link Page} of {@link SurveyData} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SurveyDataQueryService extends QueryService<SurveyData> {

    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.SURVEY;

    private final Logger log = LoggerFactory.getLogger(SurveyDataQueryService.class);

    private final SurveyDataRepository surveyDataRepository;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public SurveyDataQueryService(SurveyDataRepository surveyDataRepository, DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.surveyDataRepository = surveyDataRepository;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Return a {@link Page} of {@link SurveyData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SurveyData> findByCriteria(SurveyDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        log.debug("Received {} devices from fitbit server: {}",ENTITY_DEVICE_TYPE,devicesForCurrentUser);
        if (devicesForCurrentUser.isEmpty()) {
            log.error("There is no {} for current user!", ENTITY_DEVICE_TYPE);
            return new PageImpl<>(Collections.emptyList());
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        final Specification<SurveyData> specification = createSpecification(criteria);
        return surveyDataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SurveyDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SurveyData> specification = createSpecification(criteria);
        return surveyDataRepository.count(specification);
    }

    /**
     * Function to convert SurveyDataCriteria to a {@link Specification}
     */
    private Specification<SurveyData> createSpecification(SurveyDataCriteria criteria) {
        Specification<SurveyData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SurveyData_.id));
            }
            if (criteria.getIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIdentifier(), SurveyData_.identifier));
            }
            if (criteria.getSurveyType() != null) {
                specification = specification.and(buildSpecification(criteria.getSurveyType(), SurveyData_.surveyType));
            }
            if (criteria.getScoringResult() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScoringResult(), SurveyData_.scoringResult));
            }
            if (criteria.getCreatedTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedTime(), SurveyData_.createdTime));
            }
            if (criteria.getEndTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndTime(), SurveyData_.endTime));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), SurveyData_.deviceId));
            }
        }
        return specification;
    }
}
