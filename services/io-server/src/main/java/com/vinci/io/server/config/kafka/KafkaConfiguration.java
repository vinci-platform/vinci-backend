package com.vinci.io.server.config.kafka;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
public class KafkaConfiguration {
}
