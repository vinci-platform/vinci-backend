package com.vinci.io.server.service;

import com.vinci.io.server.domain.CameraFitnessData;
import com.vinci.io.server.repository.CameraFitnessDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CameraFitnessData.
 */
@Service
@Transactional
public class CameraFitnessDataService {

    private final Logger log = LoggerFactory.getLogger(CameraFitnessDataService.class);

    private final CameraFitnessDataRepository cameraFitnessDataRepository;

    public CameraFitnessDataService(CameraFitnessDataRepository cameraFitnessDataRepository) {
        this.cameraFitnessDataRepository = cameraFitnessDataRepository;
    }

    /**
     * Save a cameraFitnessData.
     *
     * @param cameraFitnessData the entity to save
     * @return the persisted entity
     */
    public CameraFitnessData save(CameraFitnessData cameraFitnessData) {
        log.debug("Request to save CameraFitnessData : {}", cameraFitnessData);
        return cameraFitnessDataRepository.save(cameraFitnessData);
    }

    /**
     * Get all the cameraFitnessData.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CameraFitnessData> findAll(Pageable pageable) {
        log.debug("Request to get all CameraFitnessData");
        return cameraFitnessDataRepository.findAll(pageable);
    }


    /**
     * Get one cameraFitnessData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CameraFitnessData> findOne(Long id) {
        log.debug("Request to get CameraFitnessData : {}", id);
        return cameraFitnessDataRepository.findById(id);
    }

    /**
     * Delete the cameraFitnessData by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CameraFitnessData : {}", id);
        cameraFitnessDataRepository.deleteById(id);
    }
}
