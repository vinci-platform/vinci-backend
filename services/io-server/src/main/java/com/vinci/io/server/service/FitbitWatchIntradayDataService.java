package com.vinci.io.server.service;

import com.google.common.base.Strings;
import com.vinci.io.server.domain.FitbitWatchIntradayData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.domain.fitbit.FitbitActivityData;
import com.vinci.io.server.error.NoDeviceFoundException;
import com.vinci.io.server.error.NoEntityException;
import com.vinci.io.server.repository.FitbitWatchIntradayDataRepository;
import com.vinci.io.server.service.dto.*;
import com.vinci.io.server.service.mapper.FitbitWatchIntradayDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.errors.InternalServerErrorException;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing FitbitWatchIntradayData.
 */
@Service
@Transactional
public class FitbitWatchIntradayDataService {

    private static final String ENTITY_NAME = "ioserverFitbitWatchIntradayData";
    private final DeviceType ENTITY_DEVICE_TYPE = DeviceType.FITBIT_WATCH;

    private final Logger log = LoggerFactory.getLogger(FitbitWatchIntradayDataService.class);

    private final FitbitWatchIntradayDataRepository fitbitWatchIntradayDataRepository;

    private final FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper;

    private final FitbitWatchIntradayDataQueryService fitbitWatchIntradayDataQueryService;

    private final GatewayClient gatewayClient;

    private final DeviceService deviceService;

    private final ObjectMapper objectMapper;

    private final DeviceServiceUtility deviceServiceUtility;

    public FitbitWatchIntradayDataService(FitbitWatchIntradayDataRepository fitbitWatchIntradayDataRepository, FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper,
                                          FitbitWatchIntradayDataQueryService fitbitWatchIntradayDataQueryService, GatewayClient gatewayClient, DeviceService deviceService,
                                          DeviceServiceUtility deviceServiceUtility) {
        this.fitbitWatchIntradayDataRepository = fitbitWatchIntradayDataRepository;
        this.fitbitWatchIntradayDataMapper = fitbitWatchIntradayDataMapper;
        this.fitbitWatchIntradayDataQueryService = fitbitWatchIntradayDataQueryService;
        this.gatewayClient = gatewayClient;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Just saves tje fitbitWatchIntradayData.
     *
     * @param fitbitWatchIntradayData the entity to save
     * @return the persisted entity
     */
    public FitbitWatchIntradayData justSave(FitbitWatchIntradayData fitbitWatchIntradayData) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", fitbitWatchIntradayData);
        return fitbitWatchIntradayDataRepository.save(fitbitWatchIntradayData);
    }

    /**
     * Checks user in token.
     * Save a fitbitWatchIntradayData.
     *
     * @param fitbitWatchIntradayData the entity to save
     * @return the persisted entity or null if exception
     */
    public FitbitWatchIntradayData save(FitbitWatchIntradayData fitbitWatchIntradayData) {
        log.debug("Request to save "+ENTITY_NAME+" : {}", fitbitWatchIntradayData);
        validateFitbitWatchIntradayDataForCreation(fitbitWatchIntradayData);

        Set<DeviceDTO> fitbitWatchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitWatchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }
        deviceServiceUtility.getByDeviceIdAndTypeFromSet(fitbitWatchDevicesForCurrentUser,fitbitWatchIntradayData.getDeviceId(),ENTITY_DEVICE_TYPE);

        return fitbitWatchIntradayDataRepository.save(fitbitWatchIntradayData);
    }

    /**
     * Save a fitbitWatchIntradayData.
     *
     * @param fitbitWatchIntradayDataString the string entity to save
     * @return the persisted entity or null if exception
     */
    public String saveSync(String fitbitWatchIntradayDataString) throws IOException {
        log.debug("Request to sync "+ENTITY_NAME+" : {}", fitbitWatchIntradayDataString);

        FitbitWatchIntradayData fitbitWatchIntradayData = objectMapper.readValue(fitbitWatchIntradayDataString, FitbitWatchIntradayData.class);

        save(fitbitWatchIntradayData);

        return Strings.lenientFormat("Successfully saved a "+ENTITY_NAME+" entity");
    }

   /**
     * Get one fitbitWatchIntradayData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public FitbitWatchIntradayDataDTO findOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" by id: {}", id);

        Set<DeviceDTO> fitbitDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitDevicesForCurrentUser.isEmpty()) {
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE);
        }

        FitbitWatchIntradayData fitbitWatchIntradayData = fitbitWatchIntradayDataRepository.findById(id)
        .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        deviceServiceUtility.validateDevices(id, fitbitDevicesForCurrentUser, fitbitWatchIntradayData.getDeviceId(), ENTITY_NAME, ENTITY_DEVICE_TYPE);

        return fitbitWatchIntradayDataMapper.toDto(fitbitWatchIntradayData);
    }

    /**
     * Delete the fitbitWatchIntradayData by id.
     *
     * @param id the id of the entity.
     */
    public void justDelete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);
        fitbitWatchIntradayDataRepository.deleteById(id);
    }

    public List<FitbitWatchIntradayDataPayloadDTO> findByCriteria(FitbitWatchIntradayDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (devicesForCurrentUser.isEmpty()) {
            return Collections.emptyList();
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        List<FitbitWatchIntradayDataDTO> queryList = fitbitWatchIntradayDataQueryService.justFindByCriteria(criteria);
        return transformDtosToPayloads(devicesForCurrentUser, queryList);
    }

    public Long getLatestTimestampByUserLogin(String userLogin) {
        log.debug("Get the latest timestamp for "+ENTITY_NAME+" for userLogin: {}",userLogin);

        List<DeviceDTO> fitbitWatchDevices = gatewayClient.getDeviceListByUserLogin(userLogin);
        log.debug("fitbitWatchDevices received from gateway: {}",fitbitWatchDevices);

        fitbitWatchDevices = fitbitWatchDevices.stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(ENTITY_DEVICE_TYPE))
            .collect(Collectors.toList());
        if (fitbitWatchDevices.isEmpty()) {
            log.info("User with login: {}, has no {} devices.",userLogin,ENTITY_DEVICE_TYPE);
            throw new InternalServerErrorException("There should always be a fitbit device, there are no fitbit devices for userLogin: " + userLogin);
        }
        DeviceDTO fitbitDevice = fitbitWatchDevices.stream().findFirst().get();
        Optional<Long> maxTimestampByDeviceId = fitbitWatchIntradayDataRepository.findMaxTimestampByDeviceId(fitbitDevice.getId());
        return maxTimestampByDeviceId.orElse(0L);
    }

    /**
     * Update a fitbitWatchIntradayData.
     *
     * @param fitbitWatchIntradayData the entity to save
     * @return the persisted entity or null if exception
     */
    public FitbitWatchIntradayData update(FitbitWatchIntradayData fitbitWatchIntradayData) {
        log.debug("Request to update "+ENTITY_NAME+" : {}", fitbitWatchIntradayData);
        validateFitbitWatchIntradayDataForUpdate(fitbitWatchIntradayData);

        Set<DeviceDTO> fitbitWatchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(ENTITY_DEVICE_TYPE);
        if (fitbitWatchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} devices for current user or his associates!", ENTITY_DEVICE_TYPE);
            throw new NoDeviceFoundException(ENTITY_DEVICE_TYPE, fitbitWatchIntradayData.getDeviceId());
        }

        deviceServiceUtility.getByDeviceIdAndTypeFromSet(fitbitWatchDevicesForCurrentUser, fitbitWatchIntradayData.getDeviceId(), ENTITY_DEVICE_TYPE);

        return fitbitWatchIntradayDataRepository.save(fitbitWatchIntradayData);
    }

    public void saveOrUpdateNewKafkaRecord(FitbitWatchIntradayDataDTO fitbitWatchIntradayDataDTO) throws IOException {
        log.debug("saveOrUpdateNewKafkaRecord: {}",fitbitWatchIntradayDataDTO);
        FitbitWatchIntradayData fitbitWatchIntradayDataToSaveOrUpdate = fitbitWatchIntradayDataMapper.toEntity(fitbitWatchIntradayDataDTO,fitbitWatchIntradayDataDTO.getId());

        List<FitbitWatchIntradayData> allByTimestampAndDeviceId =
            fitbitWatchIntradayDataRepository.findAllByTimestampAndDeviceId(fitbitWatchIntradayDataToSaveOrUpdate.getTimestamp(), fitbitWatchIntradayDataToSaveOrUpdate.getDeviceId());
        if (allByTimestampAndDeviceId.size() != 0) {
            FitbitWatchIntradayDataDTO existingFitbitWatchIntradayDataDto = getOneFromList(allByTimestampAndDeviceId);
            List<FitbitActivityData> existingFitbitActivityData = new ArrayList<>(Arrays.asList(objectMapper.readValue(
                existingFitbitWatchIntradayDataDto.getData(), FitbitActivityData[].class)));
            List<FitbitActivityData> newFitbitActivityData = new ArrayList<>(Arrays.asList(objectMapper.readValue(
                fitbitWatchIntradayDataToSaveOrUpdate.getData(),FitbitActivityData[].class)));
            addToListWithoutDuplicates(existingFitbitActivityData,newFitbitActivityData);
            fitbitWatchIntradayDataToSaveOrUpdate.setData(objectMapper.writeValueAsString(existingFitbitActivityData));
            fitbitWatchIntradayDataToSaveOrUpdate.setId(existingFitbitWatchIntradayDataDto.getId());
        }
        justSave(fitbitWatchIntradayDataToSaveOrUpdate);
    }

    private List<FitbitWatchIntradayDataPayloadDTO> transformDtosToPayloads(Set<DeviceDTO> devicesForCurrentUser, List<FitbitWatchIntradayDataDTO> queryList) {
        List<FitbitWatchIntradayDataPayloadDTO> entityList = new ArrayList<>();

        queryList.forEach(fitbitWatchData -> {
            FitbitWatchIntradayDataPayloadDTO fitbitWatchIntradayDataDTO = new FitbitWatchIntradayDataPayloadDTO();
            fitbitWatchIntradayDataDTO.setId(fitbitWatchData.getId());
            try {
                List<FitbitWatchIntradayActivityDataDTO> fitbitWatchIntradayActivityDataDTO = Arrays.asList(objectMapper.readValue(
                    fitbitWatchData.getData(), FitbitWatchIntradayActivityDataDTO[].class));
                fitbitWatchIntradayDataDTO.setData(fitbitWatchIntradayActivityDataDTO);

            } catch (IOException e) {
                throw new InternalServerErrorException("Could not convert fitbitWatchIntradayData" + fitbitWatchData.getData());
            }
            fitbitWatchIntradayDataDTO.setTimestamp(fitbitWatchData.getTimestamp());
            fitbitWatchIntradayDataDTO.setDevice(devicesForCurrentUser.stream().filter(deviceDTO -> deviceDTO.getId().equals(fitbitWatchData.getDeviceId())).findAny().get());
            entityList.add(fitbitWatchIntradayDataDTO);
        });
        return entityList;
    }

    private void addToListWithoutDuplicates(List<FitbitActivityData> existingFitbitActivityData, List<FitbitActivityData> newFitbitActivityData) {
        existingFitbitActivityData.remove(existingFitbitActivityData.size() - 1);
        existingFitbitActivityData.addAll(newFitbitActivityData);
    }

    private FitbitWatchIntradayDataDTO getOneFromList(List<FitbitWatchIntradayData> fitbitWatchIntradayDataList) {
        int listSize = fitbitWatchIntradayDataList.size();
        if (listSize == 0) throw new RuntimeException("getOneFromList(List<FitbitWatchIntradayData> list): list should not be null null!!!");
        if (listSize == 1) return fitbitWatchIntradayDataMapper.toDto(fitbitWatchIntradayDataList.get(0));

        log.error("there are {} fitbitWatchIntradayDataList records in database with timestamp {} and deviceId: {}. Some will be deleted so only one can remain!!!",
            listSize, fitbitWatchIntradayDataList.get(0).getTimestamp(), fitbitWatchIntradayDataList.get(0).getDeviceId());
        return deleteDuplicates(fitbitWatchIntradayDataList);
    }

    private FitbitWatchIntradayDataDTO deleteDuplicates(List<FitbitWatchIntradayData> listWithDuplicates) {
        FitbitWatchIntradayData fitbitWatchIntradayData = listWithDuplicates.get(0);
        listWithDuplicates.remove(0);
        listWithDuplicates.forEach(fitbitWatchIntradayDataRepository::delete);
        fitbitWatchIntradayDataRepository.flush();

        return fitbitWatchIntradayDataMapper.toDto(fitbitWatchIntradayData);
    }

    private void validateFitbitWatchIntradayDataForCreation(FitbitWatchIntradayData fitbitWatchIntradayData) {
        if (fitbitWatchIntradayData.getId() != null) {
            throw new BadRequestAlertException("A new fitbitWatchIntradayDataDTO cannot already have an ID", ENTITY_NAME, "id exists");
        }
    }

    private void validateFitbitWatchIntradayDataForUpdate(FitbitWatchIntradayData fitbitWatchIntradayData) {
        if (fitbitWatchIntradayData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

}
