package com.vinci.io.server.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * CreateShoeDataDto used when creating a ShoeData
 */
public class CreateShoeDataDto implements Serializable {

    @NotNull
    private Long deviceId;

    @NotNull
    private Long timestamp;

    @NotNull
    private String data;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateShoeDataDto that = (CreateShoeDataDto) o;
        return deviceId.equals(that.deviceId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getDeviceId());
    }

    @Override
    public String toString() {
        return "CreateShoeDataDto{" +
            "deviceId=" + getDeviceId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", data='" + getData() +
            "}";
    }
}
