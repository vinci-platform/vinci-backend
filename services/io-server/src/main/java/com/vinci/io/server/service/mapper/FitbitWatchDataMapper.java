package com.vinci.io.server.service.mapper;


import com.vinci.io.server.domain.*;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FitbitWatchData} and its DTO {@link FitbitWatchDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FitbitWatchDataMapper extends EntityMapper<FitbitWatchDataDTO, FitbitWatchData> {

    default FitbitWatchData fromId(Long id) {
        if (id == null) {
            return null;
        }
        FitbitWatchData fitbitWatchData = new FitbitWatchData();
        fitbitWatchData.setId(id);
        return fitbitWatchData;
    }
}
