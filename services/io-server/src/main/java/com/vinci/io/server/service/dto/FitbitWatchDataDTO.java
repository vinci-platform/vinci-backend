package com.vinci.io.server.service.dto;
import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the FitbitWatchData entity.
 */
@Builder
public class FitbitWatchDataDTO implements Serializable {

    private Long id;

    private String sleepData;

    private String activityData;

    @NotNull
    private Long timestamp;

    @NotNull
    private Long deviceId;

    public FitbitWatchDataDTO() {
    }
    public FitbitWatchDataDTO(Long id, String sleepData, String activityData, Long timestamp, Long deviceId) {
        this.id = id;
        this.sleepData = sleepData;
        this.activityData = activityData;
        this.timestamp = timestamp;
        this.deviceId = deviceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSleepData() {
        return sleepData;
    }

    public void setSleepData(String sleepData) {
        this.sleepData = sleepData;
    }

    public String getActivityData() {
        return activityData;
    }

    public void setActivityData(String activityData) {
        this.activityData = activityData;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchDataDTO)) {
            return false;
        }

        return id != null && id.equals(((FitbitWatchDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FitbitWatchDataDTO{" +
            "id=" + getId() +
            ", sleepData='" + getSleepData() + "'" +
            ", activityData='" + getActivityData() + "'" +
            ", timestamp=" + getTimestamp() +
            ", deviceId=" + getDeviceId() +
            "}";
    }
}
