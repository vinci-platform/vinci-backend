package com.vinci.io.server.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class FitbitWatchSleepDataDTO {
    List<Sleep> sleep;

    Summary summary;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class Sleep {
        Long duration;
        Integer efficiency;
        String endTime;
        String startTime;
        Boolean isMainSleep;
        Integer minutesAfterWakeup;
        Integer minutesAsleep;
        Integer minutesAwake;
        Integer minutesToFallAsleep;
        Integer timeInBed;
        Levels levels;


    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class Levels {
        List<LevelData> data;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class LevelData {
        String dateTime;
        String level;
        Integer seconds;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class Summary {
        Stages stages;
        Integer totalMinutesAsleep;
        Integer totalTimeInBed;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class Stages {
        Integer deep;
        Integer light;
        Integer rem;
        Integer wake;
    }

}
