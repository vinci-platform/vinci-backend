package com.vinci.io.server.service.mapper;

import com.vinci.io.server.domain.CameraMovementData;
import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.service.dto.CreateCameraMovementDataDto;
import com.vinci.io.server.service.dto.CreateWatchDataDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Mapper for the entity CameraMovementData and its DTO CamerMovementDataDTO.
 */
@Service
public class CameraMovementDataMapper implements EntityMapper<CreateCameraMovementDataDto, CameraMovementData> {

    @Override
    public CameraMovementData toEntity(CreateCameraMovementDataDto dto, Long newDeviceId) {
        CameraMovementData cameraMovementData = new CameraMovementData();
        cameraMovementData.setId(null);
        cameraMovementData.setData(dto.getData());
        cameraMovementData.setTimestamp(dto.getTimestamp());
        cameraMovementData.setDeviceId(newDeviceId); //id-ul luat din gateway
        return cameraMovementData;
    }

    @Override
    public CreateCameraMovementDataDto toDto(CameraMovementData entity) {
        return null;
    }

    @Override
    public List<CameraMovementData> toEntity(List<CreateCameraMovementDataDto> dtoList) {
        return null;
    }

    @Override
    public List<CreateCameraMovementDataDto> toDto(List<CameraMovementData> entityList) {
        return null;
    }
}
