package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.ShoeData;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.ShoeDataQueryService;
import com.vinci.io.server.service.ShoeDataService;
import com.vinci.io.server.service.dto.CreateShoeDataDto;
import com.vinci.io.server.service.dto.ShoeDataCriteria;
import com.vinci.io.server.service.dto.ShoeDataDTO;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
/**
 * REST controller for managing ShoeData.
 */
@RestController
@RequestMapping("/api")
public class ShoeDataResource {

    private final Logger log = LoggerFactory.getLogger(ShoeDataResource.class);

    private static final String ENTITY_NAME = "ioserverShoeData";

    private final ShoeDataService shoeDataService;

    private final ShoeDataQueryService shoeDataQueryService;


    public ShoeDataResource(ShoeDataService shoeDataService, ShoeDataQueryService shoeDataQueryService) {
        this.shoeDataService = shoeDataService;
        this.shoeDataQueryService = shoeDataQueryService;
    }

    /**
     * POST  /shoe-data : Create a new shoeData.
     *
     * @param createShoeDataDto the ShoeDataDto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shoeData, or with status 400 (Bad Request) if the shoeData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shoe-data")
    public ResponseEntity<ShoeData> createShoeData(@RequestBody @Valid CreateShoeDataDto createShoeDataDto) throws URISyntaxException {
        log.debug("REST request to save CreateShoeDataDto : {}", createShoeDataDto);
        ShoeData result = shoeDataService.saveDto(createShoeDataDto);
        return ResponseEntity.created(new URI("/api/shoe-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shoe-data : Updates an existing shoeData.
     *
     * @param shoeData the shoeData to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shoeData,
     * or with status 400 (Bad Request) if the shoeData is not valid,
     * or with status 500 (Internal Server Error) if the shoeData couldn't be updated
     */
    @PutMapping("/shoe-data")
    public ResponseEntity<ShoeData> updateShoeData(@RequestBody @Valid ShoeData shoeData) {
        log.debug("REST request to update ShoeData : {}", shoeData);
        ShoeData result = shoeDataService.update(shoeData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shoeData.getId().toString()))
            .body(result);
    }

   /**
     * GET  /shoe-data : get all the shoeData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of shoeData in body
     */
    @GetMapping("/shoe-data")
    public ResponseEntity<List<ShoeData>> getAllShoeData(ShoeDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShoeData by criteria: {}", criteria);
        Page<ShoeData> page = shoeDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shoe-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /shoe-data/count : count all the shoeData.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/shoe-data/count")
    public ResponseEntity<Long> countShoeData(ShoeDataCriteria criteria) {
        log.debug("REST request to count ShoeData by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoeDataQueryService.countByCriteria(criteria));
    }

   /**
     * GET  /shoe-data/:id : get the "id" shoeData.
     *
     * @param id the id of the shoeData to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shoeData, or with status 404 (Not Found)
     */
    @GetMapping("/shoe-data/{id}")
    public ResponseEntity<ShoeDataDTO> getShoeData(@PathVariable Long id) {
        log.debug("REST request to get ShoeData by id: {}", id);
        return ResponseEntity.ok(shoeDataService.findOne(id));
    }

    /**
     * DELETE  /shoe-data/:id : delete the "id" shoeData.
     *
     * @param id the id of the shoeData to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shoe-data/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteShoeData(@PathVariable Long id) {
        log.debug("REST request to delete ShoeData : {}", id);
        shoeDataService.justDelete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
