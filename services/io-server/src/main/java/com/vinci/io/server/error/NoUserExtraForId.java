package com.vinci.io.server.error;

import com.vinci.io.server.web.rest.errors.ErrorConstants;
import com.vinci.io.server.web.rest.errors.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserExtraForId extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_EXTRA_FOR_ID";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "No UserExtra for id!";
    private static final String DETAILS_WITH_USER_EXTRA_ID = "No UserExtra for id: %s!";

    public NoUserExtraForId(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public NoUserExtraForId(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, details); }
    public NoUserExtraForId(Long userExtraId) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_USER_EXTRA_ID,userExtraId)); }
    public NoUserExtraForId(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoUserExtraForId{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
