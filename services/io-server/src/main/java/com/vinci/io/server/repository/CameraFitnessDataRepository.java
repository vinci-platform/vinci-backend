package com.vinci.io.server.repository;

import com.vinci.io.server.domain.CameraFitnessData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CameraFitnessData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CameraFitnessDataRepository extends JpaRepository<CameraFitnessData, Long>, JpaSpecificationExecutor<CameraFitnessData> {

}
