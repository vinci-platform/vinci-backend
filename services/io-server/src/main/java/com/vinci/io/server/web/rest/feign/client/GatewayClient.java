package com.vinci.io.server.web.rest.feign.client;

import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.web.rest.beans.UserDTO;
import com.vinci.io.server.web.rest.feign.handler.ClientExceptionHandler;
import com.vinci.io.server.web.rest.feign.handler.HandleFeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@FeignClient(value = "gateway")
public interface GatewayClient {


    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/devices/uuid")
    Optional<DeviceDTO> getDeviceByUid(@RequestParam(name = "uuid") String uuid);

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/devices/id")
    Optional<DeviceDTO> getDeviceById(@RequestParam(name = "id") Long id);

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/user-devices/{login}")
    List<DeviceDTO> getDeviceListByUserLogin(@PathVariable(name = "login") String login);

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/users/getUserWithAuthorities")
    Optional<UserDTO> getUserWithAuthorities();
}
