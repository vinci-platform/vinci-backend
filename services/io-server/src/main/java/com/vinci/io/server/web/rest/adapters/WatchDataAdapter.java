package com.vinci.io.server.web.rest.adapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.service.DeviceService;
import com.vinci.io.server.service.WatchDataService;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.web.rest.beans.MeasuredData;
import com.vinci.io.server.web.rest.beans.WatchDataImportBean;
import com.vinci.io.server.web.rest.beans.WatchDataRequestBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class WatchDataAdapter {

    private final Logger log = LoggerFactory.getLogger(WatchDataAdapter.class);

    private static final String LOCATION_ALERT = "H02";

    private final WatchDataService watchDataService;

    private final ObjectMapper mapper = new ObjectMapper();
    private static final String ENTITY_NAME = "ioserverWatchData";
    private final DeviceService deviceService;

    public WatchDataAdapter(WatchDataService watchDataService, DeviceService deviceService) {
        this.watchDataService = watchDataService;
        this.deviceService = deviceService;
    }

    public Map<String, List<MeasuredData>> getOrderedAndFilteredStepsAndCoordinatesAndCheckCurrentUserDevice(WatchDataRequestBean request) throws Exception {
        log.info("Request to get ordered and filtered steps and coordinates by deviceId {} and time: {}-{}", request.getBegin(),request.getEnd(),request.getDeviceId());

        checkCurrentUserDevice(request);

        return  getOrderedAndFilteredStepsAndCoordinates(request);
    }

    public Map<String, List<WatchData>> getOrderedAndFilteredAlertsAndCheackCurrentUserDevice(WatchDataRequestBean request) throws Exception {
        log.info("Request to get ordered and filtered watch alerts by deviceId {} and time: {}-{}", request.getBegin(),request.getEnd(),request.getDeviceId());

        checkCurrentUserDevice(request);

        return getOrderedAndFilteredAlerts(request);
    }

    public Map<String, List<WatchData>> getOrderedAndFilteredAlerts(WatchDataRequestBean request) throws IOException {
        Map<String, List<WatchData>> results = Maps.newHashMap();

        List<WatchData> locationAlerts = Lists.newLinkedList();

        List<WatchData> allRecords = getFilteredRecords(request);
        for (WatchData record : allRecords) {
            String data = record.getData();
            WatchDataImportBean bean = mapper.readValue(data, WatchDataImportBean.class);
            if (bean.getAlertType() == null) {
                log.debug("Skipped alert with id {} because its type is not valid.", bean.getRecordId());
                continue;
            }
            switch (bean.getAlertType()) {
                case LOCATION_ALERT:
                    String registeredLog = bean.getPayload();
                    String[] logParts = registeredLog.split(";");

                    String coordinates = logParts[9] + ";" + logParts[10];
                    record.setData(coordinates);
                    locationAlerts.add(record);
                    break;
                default:
                    log.error("Alert type {} is not supported for alert with id {}", bean.getAlertType(), bean.getRecordId());
            }
        }

        results.put(LOCATION_ALERT, locationAlerts);

        return results;
    }

    public Map<String, List<MeasuredData>> getOrderedAndFilteredStepsAndCoordinates(WatchDataRequestBean request) throws IOException {
        Map<String, List<MeasuredData>> result = Maps.newHashMap();
        MeasuredData measure;
        List<MeasuredData> locationAlerts = Lists.newLinkedList();

        List<WatchData> allRecords = getFilteredRecords(request);
        for (WatchData record : allRecords) {
            String data = record.getData();
            WatchDataImportBean bean = mapper.readValue(data, WatchDataImportBean.class);
            if (bean.getAlertType() == null) {
                log.debug("Skipped alert with id {} because its type is not valid.", bean.getRecordId());
                continue;
            }
            switch (bean.getAlertType()) {
                case LOCATION_ALERT:
                    measure = new MeasuredData();
                    String registeredLog = bean.getPayload();
                    String[] logParts = registeredLog.split(";");

                    String coordinates = logParts[9] + ";" + logParts[10];
                    String steps = logParts[11];
                    measure.setId(record.getId());
                    measure.setCoordinates(coordinates);
                    measure.setSteps(steps);
                    measure.setTimestamp(getTimestampFromStringDate(bean.getTimestamp()));
                    measure.setDeviceId(record.getDeviceId());

                    locationAlerts.add(measure);
                    break;
                default:
                    log.error("Alert type {} is not supported for alert with id {}", bean.getAlertType(), bean.getRecordId());
            }
        }

        result.put(LOCATION_ALERT, locationAlerts);

        return result;
    }

    private boolean matchingRequestParameters(String data, WatchDataRequestBean request) {
        WatchDataImportBean alert;
        try {
            alert = mapper.readValue(data, WatchDataImportBean.class);
        } catch (IOException e) {
            log.error("Found invalid json string. Returning false");
            return false;
        }
        LocalDateTime alertDateTime = LocalDateTime.parse(alert.getTimestamp(), DateTimeFormatter.ISO_DATE_TIME);
        log.debug("TimeFrame match {}",
            alertDateTime.isAfter(request.getBegin()) && alertDateTime.isBefore(request.getEnd()));
        return alertDateTime.isAfter(request.getBegin())
            && alertDateTime.isBefore(request.getEnd());
    }

    private List<WatchData> getFilteredRecords(WatchDataRequestBean request) {
        return watchDataService.getWatchDataByDeviceId(Long.valueOf(request.getDeviceId()))
            .stream()
            .filter(record -> matchingRequestParameters(record.getData(), request))
            .collect(Collectors.toList());
    }


    private Long getTimestampFromStringDate(String localDateTimeString) {
        LocalDateTime localDateTime = LocalDateTime.parse(localDateTimeString, DateTimeFormatter.ISO_DATE_TIME);
        return localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond();
    }

    private void checkCurrentUserDevice(WatchDataRequestBean request) throws Exception {
        Set<DeviceDTO> watchDevicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(DeviceType.WATCH);
        if (watchDevicesForCurrentUser.isEmpty()) {
            log.debug("There are no {} , devices for current user or his associates!", DeviceType.WATCH);
        }

        Long deviceId;
        try {
            deviceId = Long.valueOf(request.getDeviceId());
        } catch (NumberFormatException numberFormatException) {
            throw new Exception("DeviceId must be a number!");
        }
        Optional<DeviceDTO> deviceForDeviceIdFromRequest = watchDevicesForCurrentUser
            .stream()
            .filter(deviceDTO -> deviceDTO.equals(new DeviceDTO().id(deviceId)))
            .findFirst();
        if (!deviceForDeviceIdFromRequest.isPresent()) {
            log.info("Current user and his associates don't have device of type {} and deviceId {}",DeviceType.WATCH,deviceId);
            throw new Exception(String.format("Current user and his associates don't have device of type %s and deviceId %s",DeviceType.WATCH,deviceId));
        }
    }
}
