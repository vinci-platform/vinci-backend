package com.vinci.io.server.service.mapper;

import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.service.dto.CreateWatchDataDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Mapper for the entity Device and its DTO DeviceDTO.
 */
@Service
public class WatchDataMapper implements EntityMapper<CreateWatchDataDto, WatchData> {

    @Override
    public WatchData toEntity(CreateWatchDataDto dto, Long newDeviceId) {
        WatchData watchData = new WatchData();
        watchData.setId(null);
        watchData.setData(dto.getData());
        watchData.setTimestamp(dto.getTimestamp());
        watchData.setDeviceId(newDeviceId); //id-ul luat din gateway
        return watchData;
    }

    @Override
    public CreateWatchDataDto toDto(WatchData entity) {
        return null;
    }

    @Override
    public List<WatchData> toEntity(List<CreateWatchDataDto> dtoList) {
        return null;
    }

    @Override
    public List<CreateWatchDataDto> toDto(List<WatchData> entityList) {
        return null;
    }
}
