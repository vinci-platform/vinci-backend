package com.vinci.io.server.service;

import com.vinci.io.server.domain.FitbitWatchIntradayData;
import com.vinci.io.server.domain.FitbitWatchIntradayData_;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.FitbitWatchIntradayDataRepository;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataCriteria;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import com.vinci.io.server.service.mapper.FitbitWatchIntradayDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for FitbitWatchIntradayData entities in the database.
 * The main input is a {@link FitbitWatchIntradayDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FitbitWatchIntradayDataDTO} or a {@link Page} of {@link FitbitWatchIntradayDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FitbitWatchIntradayDataQueryService extends QueryService<FitbitWatchIntradayData> {

    private static final DeviceType ENTITY_DEVICE_TYPE = DeviceType.FITBIT_WATCH;
    private final Logger log = LoggerFactory.getLogger(FitbitWatchIntradayDataQueryService.class);

    private final FitbitWatchIntradayDataRepository fitbitWatchIntradayDataRepository;

    private final FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper;

    private final DeviceService deviceService;

    private final DeviceServiceUtility deviceServiceUtility;

    public FitbitWatchIntradayDataQueryService(FitbitWatchIntradayDataRepository fitbitWatchIntradayDataRepository, FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper,
                                               DeviceService deviceService, DeviceServiceUtility deviceServiceUtility) {
        this.fitbitWatchIntradayDataRepository = fitbitWatchIntradayDataRepository;
        this.fitbitWatchIntradayDataMapper = fitbitWatchIntradayDataMapper;
        this.deviceService = deviceService;
        this.deviceServiceUtility = deviceServiceUtility;
    }

    /**
     * Checks user in token.
     * Return a {@link List} of {@link FitbitWatchIntradayDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FitbitWatchIntradayDataDTO> justFindByCriteria(FitbitWatchIntradayDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FitbitWatchIntradayData> specification = createSpecification(criteria);
        return fitbitWatchIntradayDataMapper.toDto(fitbitWatchIntradayDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FitbitWatchIntradayDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FitbitWatchIntradayDataDTO> findByCriteria(FitbitWatchIntradayDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        Set<DeviceDTO> devicesForCurrentUser = deviceService.getDeviceByTypeForCurrentUser(DeviceType.FITBIT_WATCH);
        log.debug("Received {} devices: {}",ENTITY_DEVICE_TYPE,devicesForCurrentUser);
        if (devicesForCurrentUser.isEmpty()) {
            log.debug("There is no {} for current user!", ENTITY_DEVICE_TYPE);
            return new PageImpl<>(Collections.emptyList());
        }

        LongFilter deviceIdFilter = criteria.getDeviceId();
        List<Long> listOfDeviceIds = devicesForCurrentUser.stream().map(DeviceDTO::getId).collect(Collectors.toList());

        criteria.setDeviceId(deviceServiceUtility.setDeviceIdFilter(listOfDeviceIds, deviceIdFilter));

        final Specification<FitbitWatchIntradayData> specification = createSpecification(criteria);
        return fitbitWatchIntradayDataRepository.findAll(specification, page)
            .map(fitbitWatchIntradayDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FitbitWatchIntradayDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FitbitWatchIntradayData> specification = createSpecification(criteria);
        return fitbitWatchIntradayDataRepository.count(specification);
    }

    /**
     * Function to convert {@link FitbitWatchIntradayDataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FitbitWatchIntradayData> createSpecification(FitbitWatchIntradayDataCriteria criteria) {
        Specification<FitbitWatchIntradayData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FitbitWatchIntradayData_.id));
            }
            if (criteria.getData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getData(), FitbitWatchIntradayData_.data));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), FitbitWatchIntradayData_.timestamp));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), FitbitWatchIntradayData_.deviceId));
            }
        }
        return specification;
    }
}
