package com.vinci.io.server.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Objects;

/**
 * A FitbitWatchIntradayData.
 */
@Entity
@Table(name = "fitbit_watch_intraday_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FitbitWatchIntradayData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Lob
    @Column(name = "data")
    private String data;

    @NotNull
    @Column(name = "timestamp")
    private Long timestamp;

    @NotNull
    @Column(name = "device_id")
    private Long deviceId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public FitbitWatchIntradayData data(String data) {
        this.data = data;
        return this;
    }

    public FitbitWatchIntradayData() {
    }

    public FitbitWatchIntradayData(Long id, String data, Long timestamp, Long deviceId) {
        this.id = id;
        this.data = data;
        this.timestamp = timestamp;
        this.deviceId = deviceId;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public FitbitWatchIntradayData timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public FitbitWatchIntradayData deviceId(Long deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FitbitWatchIntradayData)) {
            return false;
        }
        return id != null && id.equals(((FitbitWatchIntradayData) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FitbitWatchIntradayData{" +
            "id=" + id +
            ", data='" + data + '\'' +
            ", timestamp=" + timestamp +
            ", deviceId=" + deviceId +
            '}';
    }
}
