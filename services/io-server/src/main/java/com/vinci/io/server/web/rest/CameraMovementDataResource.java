package com.vinci.io.server.web.rest;
import com.vinci.io.server.domain.CameraMovementData;
import com.vinci.io.server.service.CameraMovementDataQueryService;
import com.vinci.io.server.service.CameraMovementDataService;
import com.vinci.io.server.service.dto.CameraMovementDataCriteria;
import com.vinci.io.server.service.dto.CreateCameraMovementDataDto;
import com.vinci.io.server.web.rest.errors.BadRequestAlertException;
import com.vinci.io.server.web.rest.util.HeaderUtil;
import com.vinci.io.server.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CameraMovementData.
 */
@RestController
@RequestMapping("/api")
public class CameraMovementDataResource {

    private final Logger log = LoggerFactory.getLogger(CameraMovementDataResource.class);

    private static final String ENTITY_NAME = "ioserverCameraMovementData";

    private final CameraMovementDataService cameraMovementDataService;

    private final CameraMovementDataQueryService cameraMovementDataQueryService;

    public CameraMovementDataResource(CameraMovementDataService cameraMovementDataService, CameraMovementDataQueryService cameraMovementDataQueryService) {
        this.cameraMovementDataService = cameraMovementDataService;
        this.cameraMovementDataQueryService = cameraMovementDataQueryService;
    }

    /**
     * POST  /camera-movement-data : Create a new cameraMovementData.
     *
     * @param createCameraMovementDataDto the cameraMovementData to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cameraMovementData, or with status 400 (Bad Request) if the cameraMovementData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/camera-movement-data")
    public ResponseEntity<CameraMovementData> createCameraMovementData(@RequestBody CreateCameraMovementDataDto createCameraMovementDataDto) throws URISyntaxException {
        CameraMovementData result = cameraMovementDataService.saveDto(createCameraMovementDataDto);
        return ResponseEntity.created(new URI("/api/camera-movement-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /camera-movement-data : Updates an existing cameraMovementData.
     *
     * @param cameraMovementData the cameraMovementData to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cameraMovementData,
     * or with status 400 (Bad Request) if the cameraMovementData is not valid,
     * or with status 500 (Internal Server Error) if the cameraMovementData couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/camera-movement-data")
    public ResponseEntity<CameraMovementData> updateCameraMovementData(@RequestBody CameraMovementData cameraMovementData) throws URISyntaxException {
        log.debug("REST request to update CameraMovementData : {}", cameraMovementData);
        if (cameraMovementData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CameraMovementData result = cameraMovementDataService.save(cameraMovementData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cameraMovementData.getId().toString()))
            .body(result);
    }

    /**
     * GET  /camera-movement-data : get all the cameraMovementData.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of cameraMovementData in body
     */
    @GetMapping("/camera-movement-data")
    public ResponseEntity<List<CameraMovementData>> getAllCameraMovementData(CameraMovementDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CameraMovementData by criteria: {}", criteria);
        Page<CameraMovementData> page = cameraMovementDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/camera-movement-data");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /camera-movement-data/count : count all the cameraMovementData.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/camera-movement-data/count")
    public ResponseEntity<Long> countCameraMovementData(CameraMovementDataCriteria criteria) {
        log.debug("REST request to count CameraMovementData by criteria: {}", criteria);
        return ResponseEntity.ok().body(cameraMovementDataQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /camera-movement-data/:id : get the "id" cameraMovementData.
     *
     * @param id the id of the cameraMovementData to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cameraMovementData, or with status 404 (Not Found)
     */
    @GetMapping("/camera-movement-data/{id}")
    public ResponseEntity<CameraMovementData> getCameraMovementData(@PathVariable Long id) {
        log.debug("REST request to get CameraMovementData : {}", id);
        Optional<CameraMovementData> cameraMovementData = cameraMovementDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cameraMovementData);
    }

    /**
     * DELETE  /camera-movement-data/:id : delete the "id" cameraMovementData.
     *
     * @param id the id of the cameraMovementData to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/camera-movement-data/{id}")
    public ResponseEntity<Void> deleteCameraMovementData(@PathVariable Long id) {
        log.debug("REST request to delete CameraMovementData : {}", id);
        cameraMovementDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
