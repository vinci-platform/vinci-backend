package com.vinci.io.server.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * CreateWatchDataDto used when creating a WatchData
 */
public class CreateWatchDataDto implements Serializable {

    @NotNull
    private Long deviceId;

    @NotNull
    private Long timestamp;

    @NotNull
    private String data;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }



    @Override
    public int hashCode() {
        return Objects.hashCode(getDeviceId());
    }

    @Override
    public String toString() {
        return "CreateWatchDataDto{" +
            "deviceId" + getDeviceId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", data='" + getData() +
            "}";
    }
}
