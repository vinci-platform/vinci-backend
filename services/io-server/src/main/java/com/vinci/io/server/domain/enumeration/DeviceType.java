package com.vinci.io.server.domain.enumeration;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    WATCH, SHOE, CAMERA_FITNESS, CAMERA_MOVEMENT, FITBIT_WATCH, SURVEY
}
