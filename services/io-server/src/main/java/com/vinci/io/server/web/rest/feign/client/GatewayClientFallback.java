package com.vinci.io.server.web.rest.feign.client;

import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.web.rest.beans.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class GatewayClientFallback implements GatewayClient {

    private final Logger log = LoggerFactory.getLogger(GatewayClientFallback.class);

    @Override
    public Optional<DeviceDTO> getDeviceByUid(String uuid) {
        log.info("==> GatewayClientFallback::getDeviceByUid(String uuid)");
        DeviceDTO device = new DeviceDTO();
        device.setId(1L);
        //todo: throw exception
        return Optional.of(device);
    }

    @Override
    public Optional<DeviceDTO> getDeviceById(Long id) {
        log.info("==> GatewayClientFallback::getDeviceById(Long id)");
        DeviceDTO device = new DeviceDTO();
        device.setId(1L);
        //todo: throw exception
        return Optional.of(device);
    }

    @Override
    public List<DeviceDTO> getDeviceListByUserLogin(String login) {
        log.info("==> getDeviceListByUserLogin(String login)");
        //todo: throw exception
        return Collections.emptyList();
    }

    @Override
    public Optional<UserDTO> getUserWithAuthorities() {
        log.info("==> getUserWithAuthorities()");
        //todo: throw exception
        return Optional.empty();
    }
}
