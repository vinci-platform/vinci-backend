package com.vinci.io.server.service.kafka.util;

import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
public class FitbitWatchIntradayDataListenableFutureCallback implements ListenableFutureCallback<SendResult<String, FitbitWatchIntradayDataDTO>> {

    @Override
    public void onFailure(Throwable ex) {
        handleFailure(ex);
    }

    @Override
    public void onSuccess(SendResult<String, FitbitWatchIntradayDataDTO> result) {
        handleSuccess(result);
    }

    private void handleFailure(Throwable ex) {
        log.error("Error Sending the Message and the exception is {}",ex.getMessage());
        try {
            throw ex;
        } catch (Throwable throwable) {
            log.error("Error in OnFailure: {}", throwable.getMessage());
        }
    }

    private void handleSuccess(SendResult<String, FitbitWatchIntradayDataDTO> result) {
        log.debug("Message Sent Successfully for key : {} and value is {} , partitions is {},"
            ,result.getProducerRecord().key(),result.getProducerRecord().value(),result.getRecordMetadata().partition());
    }
}
