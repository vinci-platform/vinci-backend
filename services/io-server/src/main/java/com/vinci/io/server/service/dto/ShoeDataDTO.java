package com.vinci.io.server.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Device entity.
 */
public class ShoeDataDTO implements Serializable {

    private Long id;

    @NotNull
    private ShoeDataPayloadDTO data;

    @NotNull
    private Long timestamp;

    @NotNull
    private DeviceDTO device;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoeDataPayloadDTO getData() {
        return data;
    }

    public void setData(ShoeDataPayloadDTO data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public DeviceDTO getDevice() {
        return device;
    }

    public void setDevice(DeviceDTO device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoeDataDTO shoeDTO = (ShoeDataDTO) o;
        if (shoeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shoeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShoeDataDTO{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", device='" + getDevice() +
            "}";
    }
}
