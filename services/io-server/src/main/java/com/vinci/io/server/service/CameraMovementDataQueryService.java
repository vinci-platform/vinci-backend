package com.vinci.io.server.service;

import com.vinci.io.server.domain.CameraMovementData;
import com.vinci.io.server.domain.CameraMovementData_;
import com.vinci.io.server.repository.CameraMovementDataRepository;
import com.vinci.io.server.service.dto.CameraMovementDataCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CameraMovementData entities in the database.
 * The main input is a {@link CameraMovementDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CameraMovementData} or a {@link Page} of {@link CameraMovementData} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CameraMovementDataQueryService extends QueryService<CameraMovementData> {

    private final Logger log = LoggerFactory.getLogger(CameraMovementDataQueryService.class);

    private final CameraMovementDataRepository cameraMovementDataRepository;

    public CameraMovementDataQueryService(CameraMovementDataRepository cameraMovementDataRepository) {
        this.cameraMovementDataRepository = cameraMovementDataRepository;
    }

    /**
     * Return a {@link List} of {@link CameraMovementData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CameraMovementData> findByCriteria(CameraMovementDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CameraMovementData> specification = createSpecification(criteria);
        return cameraMovementDataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CameraMovementData} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CameraMovementData> findByCriteria(CameraMovementDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CameraMovementData> specification = createSpecification(criteria);
        return cameraMovementDataRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CameraMovementDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CameraMovementData> specification = createSpecification(criteria);
        return cameraMovementDataRepository.count(specification);
    }

    /**
     * Function to convert CameraMovementDataCriteria to a {@link Specification}
     */
    private Specification<CameraMovementData> createSpecification(CameraMovementDataCriteria criteria) {
        Specification<CameraMovementData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CameraMovementData_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), CameraMovementData_.timestamp));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeviceId(), CameraMovementData_.deviceId));
            }
        }
        return specification;
    }
}
