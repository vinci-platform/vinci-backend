package com.vinci.io.server.service;

import com.vinci.io.server.error.NoUserForTokenException;
import com.vinci.io.server.web.rest.beans.UserDTO;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    private final GatewayClient gatewayClient;

    public UserService(GatewayClient gatewayClient) {
        this.gatewayClient = gatewayClient;
    }

    public UserDTO getValidatedCurrentUser() {
        return gatewayClient.getUserWithAuthorities()
            .orElseThrow(() -> new NoUserForTokenException("No valid user in token!"));
    }

}
