package com.vinci.io.server.service.mapper;

import org.junit.Test;
import org.junit.Before;
import static org.assertj.core.api.Assertions.assertThat;

public class FitbitWatchDataMapperTest {

    private FitbitWatchDataMapper fitbitwatchdataMapper;

    @Before
    public void setUp() {
        fitbitwatchdataMapper = new FitbitWatchDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(fitbitwatchdataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(fitbitwatchdataMapper.fromId(null)).isNull();
    }
}
