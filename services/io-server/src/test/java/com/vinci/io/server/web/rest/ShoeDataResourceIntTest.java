package com.vinci.io.server.web.rest;

import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.ShoeData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.ShoeDataRepository;
import com.vinci.io.server.service.DeviceService;
import com.vinci.io.server.service.ShoeDataQueryService;
import com.vinci.io.server.service.ShoeDataService;
import com.vinci.io.server.service.mapper.ShoeDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.*;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.ORGANIZATION_DEVICES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ShoeDataResource REST controller.
 *
 * @see ShoeDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IoserverApp.class)
public class ShoeDataResourceIntTest extends SetupController{

    private static final String DEFAULT_DATA = "{\"step_counter\":1,\"step_activity\":1}";
    private static final String UPDATED_DATA = "{\"step_counter\":2,\"step_activity\":2}";

    private static final Integer DEFAULT_STEP_COUNTER = 1;
    private static final Integer DEFAULT_STEP_ACTIVITY = 1;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;


    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.SHOE;
    private static final DeviceType DIFFERENT_DEVICE_TYPE = DeviceType.WATCH;

    @Autowired
    private ShoeDataRepository shoeDataRepository;

    private ShoeDataService shoeDataService;

    @Autowired
    private ShoeDataMapper shoeDataMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Mock
    private GatewayClient gatewayClientMock;

    private MockMvc restShoeDataMockMvc;

    private ShoeData shoeData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeviceService deviceService = new DeviceService(gatewayClientMock);
        DeviceServiceUtility deviceServiceUtility = new DeviceServiceUtility(deviceService);
        ShoeDataQueryService shoeDataQueryService = new ShoeDataQueryService(shoeDataRepository, deviceService, deviceServiceUtility);
        shoeDataService = new ShoeDataService(shoeDataRepository, shoeDataMapper, deviceService, deviceServiceUtility);
        final ShoeDataResource shoeDataResource = new ShoeDataResource(shoeDataService, shoeDataQueryService);
        this.restShoeDataMockMvc = MockMvcBuilders.standaloneSetup(shoeDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoeData createEntity(EntityManager em) {
        return new ShoeData()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
    }

    @Before
    public void initTest() {
        shoeData = createEntity(em);
        setupDeviceDto(DEFAULT_DEVICE_TYPE);
        setupUserDto();
    }

    @Test
    @Transactional
    public void createShoeData_NoDeviceId() throws Exception {
        //setup
        shoeData.setDeviceId(null);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsPacient() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createShoeData_AsPacient_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsPacient_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsPacient_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        shoeData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createShoeData_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        shoeData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacianet_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacianet_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacianet_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacianet_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        shoeData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createShoeData_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        shoeData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacient_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacient_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacient_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createShoeData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        shoeData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createShoeDataRestCall_IsBadRequest();
    }

    private void createShoeDataRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = shoeDataRepository.findAll().size();
        // Create the ShoeData
        restShoeDataMockMvc.perform(post("/api/shoe-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoeData)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the ShoeData in the database
        List<ShoeData> shoeDataList = shoeDataRepository.findAll();
        assertThat(shoeDataList).hasSize(databaseSizeBeforeTest + 1);
        ShoeData testShoeData = shoeDataList.get(shoeDataList.size() - 1);
        assertThat(testShoeData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testShoeData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testShoeData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createShoeDataRestCall_IsBadRequest() throws Exception {
        // Create the ShoeData
        restShoeDataMockMvc.perform(post("/api/shoe-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoeData)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateShoeData_IdNotNull() throws Exception {
        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(null);

        int databaseSizeBeforeTest = shoeDataRepository.findAll().size();
        restShoeDataMockMvc.perform(put("/api/shoe-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoeData)))
            .andExpect(status().isBadRequest());

        // Validate the ShoeData in the database
        List<ShoeData> shoeDataList = shoeDataRepository.findAll();
        assertThat(shoeDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateShoeData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateShoeData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsPacient_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsPacient_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateShoeData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateShoeData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateShoeData_OfPacient_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataService.justSave(shoeData);

        updateShoeDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    private void updateShoeDataRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = shoeDataRepository.findAll().size();
        // Update the shoeData
        ShoeData updatedShoeData = shoeDataRepository.findById(shoeData.getId()).get();
        // Disconnect from session so that the updates on updatedShoeData are not directly saved in db
        em.detach(updatedShoeData);
        updatedShoeData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);

        restShoeDataMockMvc.perform(put("/api/shoe-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedShoeData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data").value(UPDATED_DATA))
            .andExpect(jsonPath("$.timestamp").value(UPDATED_TIMESTAMP))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the ShoeData in the database
        List<ShoeData> shoeDataList = shoeDataRepository.findAll();
        assertThat(shoeDataList).hasSize(databaseSizeBeforeTest);
        ShoeData testShoeData = shoeDataList.get(shoeDataList.size() - 1);
        assertThat(testShoeData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testShoeData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testShoeData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void updateShoeDataRestCall_IsBadRequest(Long updateDeviceId) throws Exception {
        int databaseSizeBeforeTest = shoeDataRepository.findAll().size();
        // Update the shoeData
        ShoeData updatedShoeData = shoeDataRepository.findById(shoeData.getId()).get();
        // Disconnect from session so that the updates on updatedShoeData are not directly saved in db
        em.detach(updatedShoeData);
        updatedShoeData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(updateDeviceId);

        restShoeDataMockMvc.perform(put("/api/shoe-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedShoeData)))
            .andExpect(status().isBadRequest());

        // Validate the ShoeData in the database
        List<ShoeData> shoeDataList = shoeDataRepository.findAll();
        assertThat(shoeDataList).hasSize(databaseSizeBeforeTest);
        ShoeData testShoeData = shoeDataList.get(shoeDataList.size() - 1);
        assertThat(testShoeData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testShoeData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testShoeData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllShoeData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllShoeData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllShoeData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllShoeData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllShoeData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllShoeData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllShoeData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllShoeData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllShoeData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllShoeData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeDataRestCall_EmptyList();
    }

    private void getAllShoeDataRestCall_IsOk() throws Exception {
        // Get all the shoeDataList
        restShoeDataMockMvc.perform(get("/api/shoe-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoeData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllShoeDataRestCall_EmptyList() throws Exception {
        // Get all the shoeDataList
        restShoeDataMockMvc.perform(get("/api/shoe-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getAllShoeData_FiltersWithEquals_AsPacient() throws Exception {
        // setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllShoeData_FiltersWithEquals_AsFamily() throws Exception {
        // setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllShoeData_FiltersWithEquals_AsOrganization() throws Exception {
        // setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getAllShoeData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    private void getAllShoeData_DeviceIdPlusRestOfFilters(Long validDeviceId, Long invalidDeviceId) throws Exception {
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDeviceId.intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdEqualsFilter, invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDeviceId.intValue() + "," + invalidDeviceId.intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdInFilter, invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultShoeDataShouldBeFound(validFilter,shoeData);
        defaultShoeDataShouldNotBeFound(invalidFilter);

//        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"data",DEFAULT_DATA,UPDATED_DATA);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",shoeData.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultShoeDataShouldBeFound(filter+validValue,shoeData);
        defaultShoeDataShouldNotBeFound(filter+invalidValue);
        defaultShoeDataShouldBeFound(validFilter+"&"+filter+validValue,shoeData);
        defaultShoeDataShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultShoeDataShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultShoeDataShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultShoeDataShouldBeFound(String filter,ShoeData _shoeData) throws Exception {
        restShoeDataMockMvc.perform(get("/api/shoe-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_shoeData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultShoeDataShouldNotBeFound(String filter) throws Exception {
        System.out.println(filter);
        restShoeDataMockMvc.perform(get("/api/shoe-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

    }

    @Test
    @Transactional
    public void getShoeDataById_AsPacient() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsPacient_NoDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getShoeDataById_AsPacient_BadDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setDeviceId(BAD_DEVICE_ID);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsPacient_BadId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(-1L);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getShoeDataById_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getShoeDataById_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setDeviceId(BAD_DEVICE_ID);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(-1L);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setDeviceId(BAD_DEVICE_ID);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(-1L);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getShoeDataById_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getShoeDataById_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setDeviceId(BAD_DEVICE_ID);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getShoeDataById_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(-1L);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setDeviceId(BAD_DEVICE_ID);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getShoeDataById_OfPacient_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeData.setId(-1L);
        shoeDataRepository.saveAndFlush(shoeData);

        getShoeDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    private void getShoeDataByIdRestCall_IsOk() throws Exception {
        // Get the shoeData
        restShoeDataMockMvc.perform(get("/api/shoe-data/{id}", shoeData.getId()))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shoeData.getId().intValue()))
            .andExpect(jsonPath("$.data.step_counter").value(DEFAULT_STEP_COUNTER))
            .andExpect(jsonPath("$.data.step_activity").value(DEFAULT_STEP_ACTIVITY))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.device.id").value(DEFAULT_DEVICE_ID.intValue()));
    }

    private void getShoeDataByIdRestCall_IsUnauthorized() throws Exception {
        // Get the shoeData
        restShoeDataMockMvc.perform(get("/api/shoe-data/{id}", shoeData.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getShoeDataByIdRestCall_IsBadRequest(MediaType returnMediaType) throws Exception {
        // Get the shoeData
        restShoeDataMockMvc.perform(get("/api/shoe-data/{id}", shoeData.getId()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(returnMediaType));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoeData.class);
        ShoeData shoeData1 = new ShoeData();
        shoeData1.setId(1L);
        ShoeData shoeData2 = new ShoeData();
        shoeData2.setId(shoeData1.getId());
        assertThat(shoeData1).isEqualTo(shoeData2);
        shoeData2.setId(2L);
        assertThat(shoeData1).isNotEqualTo(shoeData2);
        shoeData1.setId(null);
        assertThat(shoeData1).isNotEqualTo(shoeData2);
    }

    @Test
    @Transactional
    public void deleteShoeDataById_AsAdmin() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        shoeDataRepository.saveAndFlush(shoeData);

        int numberOfEntitiesBeforeTest = shoeDataRepository.findAll().size();
        // Get the shoeData
        restShoeDataMockMvc.perform(delete("/api/shoe-data/{id}", shoeData.getId()))
            .andExpect(status().isOk());

        List<ShoeData> listInDb = shoeDataRepository.findAll();
        assertThat(listInDb.size()).isEqualTo(numberOfEntitiesBeforeTest-1);
    }
}
