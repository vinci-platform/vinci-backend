package com.vinci.io.server.service.mapper;

import org.junit.Test;
import org.junit.Before;
import static org.assertj.core.api.Assertions.assertThat;

public class FitbitWatchIntradayDataMapperTest {

    private FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper;

    @Before
    public void setUp() {
        fitbitWatchIntradayDataMapper = new FitbitWatchIntradayDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(fitbitWatchIntradayDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(fitbitWatchIntradayDataMapper.fromId(null)).isNull();
    }
}
