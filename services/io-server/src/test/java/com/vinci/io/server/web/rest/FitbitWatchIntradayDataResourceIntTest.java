package com.vinci.io.server.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.FitbitWatchIntradayData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.FitbitWatchIntradayDataRepository;
import com.vinci.io.server.service.*;
import com.vinci.io.server.service.mapper.FitbitWatchIntradayDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.*;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.ORGANIZATION_DEVICES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FitbitWatchIntradayDataResource} REST controller.
 */
@SpringBootTest(classes = IoserverApp.class)
@AutoConfigureMockMvc
@WithMockUser
@RunWith(SpringRunner.class)
public class FitbitWatchIntradayDataResourceIntTest extends SetupController {

    private static final String DEFAULT_DATA = "[{\"time\":\"00:00:00\",\"timestamp\":1635120000000,\"heart\":null,\"steps\":0,\"calories\":1,\"floors\":0,\"elevation\":0,\"distance\":0}]";
    private static final String UPDATED_DATA = "[{\"time\":\"00:00:01\",\"timestamp\":1635120000001,\"heart\":null,\"steps\":1,\"calories\":2,\"floors\":1,\"elevation\":1,\"distance\":1}]";

    //from above strings
    public static final Integer DEFAULT_DATA_STEPS = 0;
    public static final Integer UPDATE_DATA_STEPS = 1;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;
    private static final Long LATEST_TIMESTAMP = Long.MAX_VALUE;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.FITBIT_WATCH;
    private static final DeviceType DIFFERENT_DEVICE_TYPE = DeviceType.WATCH;


    @Autowired
    private FitbitWatchIntradayDataRepository fitbitWatchIntradayDataRepository;

    @Autowired
    private FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper;

//    @Autowired
    private FitbitWatchIntradayDataService fitbitWatchIntradayDataService;

//    @Autowired
    private FitbitWatchIntradayDataQueryService fitbitWatchIntradayDataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private MockMvc restFitbitWatchIntradayDataMockMvc;

    @Mock
    private GatewayClient gatewayClientMock;

    @Autowired
    private ObjectMapper objectMapper;

    private FitbitWatchIntradayData fitbitWatchIntradayData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeviceService deviceService = new DeviceService(gatewayClientMock);
        DeviceServiceUtility deviceServiceUtility = new DeviceServiceUtility(deviceService);
        fitbitWatchIntradayDataQueryService = new FitbitWatchIntradayDataQueryService(fitbitWatchIntradayDataRepository, fitbitWatchIntradayDataMapper, deviceService, deviceServiceUtility);
        fitbitWatchIntradayDataService = new FitbitWatchIntradayDataService(fitbitWatchIntradayDataRepository, fitbitWatchIntradayDataMapper,
            fitbitWatchIntradayDataQueryService, gatewayClientMock, deviceService, deviceServiceUtility);
        final FitbitWatchIntradayDataResource fitbitWatchDataResource = new FitbitWatchIntradayDataResource(fitbitWatchIntradayDataService, fitbitWatchIntradayDataQueryService);
        this.restFitbitWatchIntradayDataMockMvc = MockMvcBuilders.standaloneSetup(fitbitWatchDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FitbitWatchIntradayData createEntity(EntityManager em) {
        FitbitWatchIntradayData fitbitWatchIntradayData = new FitbitWatchIntradayData()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
        return fitbitWatchIntradayData;
    }

    private void initTest() {
        fitbitWatchIntradayData = createEntity(em);
        setupDeviceDto(DEFAULT_DEVICE_TYPE);
        setupUserDto();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_NoDeviceId() throws Exception {
        //setup
        fitbitWatchIntradayData.setDeviceId(null);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_IdNotNull() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setId(1L);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsPacient() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsCreated();
        em.clear();
        fitbitWatchIntradayData.setId(null);
        createFitbitWatchIntradayDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsPacient_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsPacient_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsPacient_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacianet_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacianet_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacianet_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacianet_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacient_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacient_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacient_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchIntradayData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchIntradayDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchIntradayDataSyncRestCall_IsBadRequest();
    }

    private void createFitbitWatchIntradayDataRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        // Create the FitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(post("/api/fitbit-watch-intraday-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitWatchIntradayData)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the FitbitWatchIntradayData in the database
        List<FitbitWatchIntradayData> fitbitWatchIntradayDataList = fitbitWatchIntradayDataRepository.findAll();
        assertThat(fitbitWatchIntradayDataList).hasSize(databaseSizeBeforeTest + 1);
        FitbitWatchIntradayData testFitbitWatchIntradayData = fitbitWatchIntradayDataList.get(fitbitWatchIntradayDataList.size() - 1);
        assertThat(testFitbitWatchIntradayData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testFitbitWatchIntradayData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchIntradayData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createFitbitWatchIntradayDataRestCall_IsBadRequest() throws Exception {
        // Create the FitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(post("/api/fitbit-watch-intraday-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitWatchIntradayData)))
            .andExpect(status().isBadRequest());
    }

    private void createFitbitWatchIntradayDataSyncRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        // Create the FitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(post("/api/fitbit-watch-intraday-data/sync")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(fitbitWatchIntradayData))))
            .andExpect(status().isOk());

        // Validate the FitbitWatchIntradayData in the database
        List<FitbitWatchIntradayData> fitbitWatchIntradayDataListFromDatabase = fitbitWatchIntradayDataRepository.findAll();
        assertThat(fitbitWatchIntradayDataListFromDatabase).hasSize(databaseSizeBeforeTest + 1);
        FitbitWatchIntradayData testFitbitWatchIntradayData = fitbitWatchIntradayDataListFromDatabase.get(fitbitWatchIntradayDataListFromDatabase.size() - 1);
        assertThat(testFitbitWatchIntradayData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testFitbitWatchIntradayData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchIntradayData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createFitbitWatchIntradayDataSyncRestCall_IsBadRequest() throws Exception {
        // Create the FitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(post("/api/fitbit-watch-intraday-data/sync")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(fitbitWatchIntradayData))))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_IdNotNull() throws Exception {
        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(null);

        int databaseSizeBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        restFitbitWatchIntradayDataMockMvc.perform(put("/api/fitbit-watch-intraday-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitWatchIntradayData)))
            .andExpect(status().isBadRequest());

        // Validate the FitbitWatchIntradayData in the database
        List<FitbitWatchIntradayData> fitbitWatchIntradayDataList = fitbitWatchIntradayDataRepository.findAll();
        assertThat(fitbitWatchIntradayDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsPacient_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsPacient_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchIntradayData_OfPacient_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataService.justSave(fitbitWatchIntradayData);

        updateFitbitWatchIntradayDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    private void updateFitbitWatchIntradayDataRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        // Update the fitbitWatchIntradayData
        FitbitWatchIntradayData updatedFitbitWatchIntradayData = fitbitWatchIntradayDataRepository.findById(fitbitWatchIntradayData.getId()).get();
        // Disconnect from session so that the updates on updatedFitbitWatchIntradayData are not directly saved in db
        em.detach(updatedFitbitWatchIntradayData);
        updatedFitbitWatchIntradayData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);

        restFitbitWatchIntradayDataMockMvc.perform(put("/api/fitbit-watch-intraday-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFitbitWatchIntradayData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data").value(UPDATED_DATA))
            .andExpect(jsonPath("$.timestamp").value(UPDATED_TIMESTAMP))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the FitbitWatchIntradayData in the database
        List<FitbitWatchIntradayData> fitbitWatchIntradayDataList = fitbitWatchIntradayDataRepository.findAll();
        assertThat(fitbitWatchIntradayDataList).hasSize(databaseSizeBeforeTest);
        FitbitWatchIntradayData testFitbitWatchIntradayData = fitbitWatchIntradayDataList.get(fitbitWatchIntradayDataList.size() - 1);
        assertThat(testFitbitWatchIntradayData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testFitbitWatchIntradayData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testFitbitWatchIntradayData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void updateFitbitWatchIntradayDataRestCall_IsBadRequest(Long updateDeviceId) throws Exception {
        int databaseSizeBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        // Update the fitbitWatchIntradayData
        FitbitWatchIntradayData updatedFitbitWatchIntradayData = fitbitWatchIntradayDataRepository.findById(fitbitWatchIntradayData.getId()).get();
        // Disconnect from session so that the updates on updatedFitbitWatchIntradayData are not directly saved in db
        em.detach(updatedFitbitWatchIntradayData);
        updatedFitbitWatchIntradayData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(updateDeviceId);

        restFitbitWatchIntradayDataMockMvc.perform(put("/api/fitbit-watch-intraday-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFitbitWatchIntradayData)))
            .andExpect(status().isBadRequest());

        // Validate the FitbitWatchIntradayData in the database
        List<FitbitWatchIntradayData> fitbitWatchIntradayDataList = fitbitWatchIntradayDataRepository.findAll();
        assertThat(fitbitWatchIntradayDataList).hasSize(databaseSizeBeforeTest);
        FitbitWatchIntradayData testFitbitWatchIntradayData = fitbitWatchIntradayDataList.get(fitbitWatchIntradayDataList.size() - 1);
        assertThat(testFitbitWatchIntradayData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testFitbitWatchIntradayData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchIntradayData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_IsOk();
        getAllFitbitWatchIntradayDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_EmptyList();
        getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_IsOk();
        getAllFitbitWatchIntradayDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_IsOk();
        getAllFitbitWatchIntradayDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_EmptyList();
        getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_EmptyList();
        getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_IsOk();
        getAllFitbitWatchIntradayDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_EmptyList();
        getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_IsOk();
        getAllFitbitWatchIntradayDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayDataRestCall_EmptyList();
        getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList();
    }

    private void getAllFitbitWatchIntradayDataRestCall_IsOk() throws Exception {
        // Get all the fitbitWatchIntradayDataList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitWatchIntradayData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllFitbitWatchIntradayDataRestCall_EmptyList() throws Exception {
        // Get all the fitbitWatchIntradayDataList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    private void getAllFitbitWatchIntradayDataPayloadRestCall_IsOk() throws Exception {
        // Get all the fitbitWatchIntradayDataPayloadList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/payload?sort=id,desc"))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitWatchIntradayData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data.[*].steps").value(hasItem(DEFAULT_DATA_STEPS)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].device.id").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllFitbitWatchIntradayDataPayloadRestCall_EmptyList() throws Exception {
        // Get all the fitbitWatchIntradayDataPayloadList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/payload?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_FiltersWithEquals_AsPacient() throws Exception {
        // setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_FiltersWithEquals_AsFamily() throws Exception {
        // setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchIntradayData_FiltersWithEquals_AsOrganization() throws Exception {
        // setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getAllFitbitWatchIntradayData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    private void getAllFitbitWatchIntradayData_DeviceIdPlusRestOfFilters(Long validDeviceId, Long invalidDeviceId) throws Exception {
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDeviceId.intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdEqualsFilter, invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDeviceId.intValue() + "," + invalidDeviceId.intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdInFilter, invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultFitbitWatchIntradayDataShouldBeFound(validFilter,fitbitWatchIntradayData);
        defaultFitbitWatchIntradayDataShouldNotBeFound(invalidFilter);
        testValidAndInvalidFilters(validFilter,invalidFilter,"data.contains=","\"steps\":0","\"steps\":1");
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",fitbitWatchIntradayData.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultFitbitWatchIntradayDataShouldBeFound(filter+validValue,fitbitWatchIntradayData);
        defaultFitbitWatchIntradayDataShouldNotBeFound(filter+invalidValue);
        defaultFitbitWatchIntradayDataShouldBeFound(validFilter+"&"+filter+validValue,fitbitWatchIntradayData);
        defaultFitbitWatchIntradayDataShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultFitbitWatchIntradayDataShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultFitbitWatchIntradayDataShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
//        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
//        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultFitbitWatchIntradayDataShouldBeFound(String filter,FitbitWatchIntradayData _fitbitWatchIntradayData) throws Exception {
        // Get all the fitbitWatchIntradayDataList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_fitbitWatchIntradayData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));

        // Get all the fitbitWatchIntradayDataPayloadList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/payload?sort=id,desc&"+filter))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_fitbitWatchIntradayData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data.[*].steps").value(hasItem(DEFAULT_DATA_STEPS)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].device.id").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultFitbitWatchIntradayDataShouldNotBeFound(String filter) throws Exception {
        // Get all the fitbitWatchIntradayDatadList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Get all the fitbitWatchIntradayDataPayloadList
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/payload?sort=id,desc&"+filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsPacient() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsPacient_NoDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsPacient_BadDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsPacient_BadId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(-1L);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(-1L);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(-1L);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(-1L);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setDeviceId(BAD_DEVICE_ID);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchIntradayDataById_OfPacient_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayData.setId(-1L);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    private void getFitbitWatchIntradayDataByIdRestCall_IsOk() throws Exception {
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/{id}", fitbitWatchIntradayData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fitbitWatchIntradayData.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    private void getFitbitWatchIntradayDataByIdRestCall_IsUnauthorized() throws Exception {
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/{id}", fitbitWatchIntradayData.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getFitbitWatchIntradayDataByIdRestCall_IsBadRequest(MediaType returnMediaType) throws Exception {
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/{id}", fitbitWatchIntradayData.getId()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(returnMediaType));
    }

    @Test
    @Transactional
    public void getLatestTimestamp() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getDeviceListByUserLogin(any())).thenReturn(getDeviceDtoList());

        setupMultipleFitbitDataWithDifferentTimestamps(DEFAULT_DEVICE_ID);

        getLatestTimestampByUserLogin_IsOk(LATEST_TIMESTAMP.intValue());
    }

    @Test
    @Transactional
    public void getLatestTimestamp_NoDevices() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getDeviceListByUserLogin(any())).thenReturn(Collections.emptyList());

        setupMultipleFitbitDataWithDifferentTimestamps(DEFAULT_DEVICE_ID);

        getLatestTimestampByUserLogin_InternalServerError();
    }

    @Test
    @Transactional
    public void getLatestTimestamp_NoDataForDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getDeviceListByUserLogin(any())).thenReturn(getDeviceDtoList());

//        setupMultipleFitbitDataWithDifferentTimestamps(DEFAULT_DEVICE_ID);

        getLatestTimestampByUserLogin_IsOk(0);
    }

    @Test
    @Transactional
    public void getLatestTimestamp_NoDataForDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getDeviceListByUserLogin(any())).thenReturn(getDeviceDtoList());

        setupMultipleFitbitDataWithDifferentTimestamps(UPDATED_DEVICE_ID);

        getLatestTimestampByUserLogin_IsOk(0);
    }

    private void getLatestTimestampByUserLogin_IsOk(Integer returnValue) throws Exception {
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/latest-timestamp/ANY_USER_LOGIN"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(returnValue));
    }

    private void getLatestTimestampByUserLogin_InternalServerError() throws Exception {
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(get("/api/fitbit-watch-intraday-data/latest-timestamp/ANY_USER_LOGIN", fitbitWatchIntradayData.getId()))
            .andExpect(status().isInternalServerError());
    }

    private void setupMultipleFitbitDataWithDifferentTimestamps(Long deviceId) {
        FitbitWatchIntradayData entity1 = createEntity(em);
        entity1.setData(UPDATED_DATA);
        entity1.setTimestamp(Long.MIN_VALUE);
        entity1.setDeviceId(deviceId);
        fitbitWatchIntradayDataRepository.saveAndFlush(entity1);

        fitbitWatchIntradayData.setTimestamp(LATEST_TIMESTAMP);
        fitbitWatchIntradayData.setDeviceId(deviceId);
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        FitbitWatchIntradayData entity2 = createEntity(em);
        entity2.setData(UPDATED_DATA);
        entity2.setTimestamp(Long.MAX_VALUE - 10000);
        entity2.setDeviceId(deviceId);
        fitbitWatchIntradayDataRepository.saveAndFlush(entity2);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchIntradayData.class);
        FitbitWatchIntradayData fitbitWatchIntradayData1 = new FitbitWatchIntradayData();
        fitbitWatchIntradayData1.setId(1L);
        FitbitWatchIntradayData fitbitWatchIntradayData2 = new FitbitWatchIntradayData();
        fitbitWatchIntradayData2.setId(fitbitWatchIntradayData1.getId());
        assertThat(fitbitWatchIntradayData1).isEqualTo(fitbitWatchIntradayData2);
        fitbitWatchIntradayData2.setId(2L);
        assertThat(fitbitWatchIntradayData1).isNotEqualTo(fitbitWatchIntradayData2);
        fitbitWatchIntradayData1.setId(null);
        assertThat(fitbitWatchIntradayData1).isNotEqualTo(fitbitWatchIntradayData2);
    }

    @Test
    @Transactional
    public void deleteFitbitWatchIntradayDataById_AsAdmin() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitWatchIntradayDataRepository.saveAndFlush(fitbitWatchIntradayData);

        int numberOfEntitiesBeforeTest = fitbitWatchIntradayDataRepository.findAll().size();
        // Get the fitbitWatchIntradayData
        restFitbitWatchIntradayDataMockMvc.perform(delete("/api/fitbit-watch-intraday-data/{id}", fitbitWatchIntradayData.getId()))
            .andExpect(status().isOk());

        List<FitbitWatchIntradayData> listInDb = fitbitWatchIntradayDataRepository.findAll();
        assertThat(listInDb.size()).isEqualTo(numberOfEntitiesBeforeTest-1);
    }

}
