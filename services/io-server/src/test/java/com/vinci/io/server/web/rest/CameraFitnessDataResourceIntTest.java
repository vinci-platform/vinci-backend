package com.vinci.io.server.web.rest;

import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.CameraFitnessData;
import com.vinci.io.server.repository.CameraFitnessDataRepository;
import com.vinci.io.server.service.CameraFitnessDataQueryService;
import com.vinci.io.server.service.CameraFitnessDataService;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CameraFitnessDataResource REST controller.
 *
 * @see CameraFitnessDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IoserverApp.class)
public class CameraFitnessDataResourceIntTest {

    private static final String DEFAULT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;

    @Autowired
    private CameraFitnessDataRepository cameraFitnessDataRepository;

    @Autowired
    private CameraFitnessDataService cameraFitnessDataService;

    @Autowired
    private CameraFitnessDataQueryService cameraFitnessDataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCameraFitnessDataMockMvc;

    private CameraFitnessData cameraFitnessData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CameraFitnessDataResource cameraFitnessDataResource = new CameraFitnessDataResource(cameraFitnessDataService, cameraFitnessDataQueryService);
        this.restCameraFitnessDataMockMvc = MockMvcBuilders.standaloneSetup(cameraFitnessDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CameraFitnessData createEntity(EntityManager em) {
        CameraFitnessData cameraFitnessData = new CameraFitnessData()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
        return cameraFitnessData;
    }

    @Before
    public void initTest() {
        cameraFitnessData = createEntity(em);
    }

    @Test
    @Transactional
    public void createCameraFitnessData() throws Exception {
        int databaseSizeBeforeCreate = cameraFitnessDataRepository.findAll().size();

        // Create the CameraFitnessData
        restCameraFitnessDataMockMvc.perform(post("/api/camera-fitness-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cameraFitnessData)))
            .andExpect(status().isCreated());

        // Validate the CameraFitnessData in the database
        List<CameraFitnessData> cameraFitnessDataList = cameraFitnessDataRepository.findAll();
        assertThat(cameraFitnessDataList).hasSize(databaseSizeBeforeCreate + 1);
        CameraFitnessData testCameraFitnessData = cameraFitnessDataList.get(cameraFitnessDataList.size() - 1);
        assertThat(testCameraFitnessData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testCameraFitnessData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testCameraFitnessData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void createCameraFitnessDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cameraFitnessDataRepository.findAll().size();

        // Create the CameraFitnessData with an existing ID
        cameraFitnessData.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCameraFitnessDataMockMvc.perform(post("/api/camera-fitness-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cameraFitnessData)))
            .andExpect(status().isBadRequest());

        // Validate the CameraFitnessData in the database
        List<CameraFitnessData> cameraFitnessDataList = cameraFitnessDataRepository.findAll();
        assertThat(cameraFitnessDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessData() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cameraFitnessData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    @Test
    @Transactional
    public void getCameraFitnessData() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get the cameraFitnessData
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data/{id}", cameraFitnessData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cameraFitnessData.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where timestamp equals to DEFAULT_TIMESTAMP
        defaultCameraFitnessDataShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the cameraFitnessDataList where timestamp equals to UPDATED_TIMESTAMP
        defaultCameraFitnessDataShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultCameraFitnessDataShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the cameraFitnessDataList where timestamp equals to UPDATED_TIMESTAMP
        defaultCameraFitnessDataShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where timestamp is not null
        defaultCameraFitnessDataShouldBeFound("timestamp.specified=true");

        // Get all the cameraFitnessDataList where timestamp is null
        defaultCameraFitnessDataShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultCameraFitnessDataShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the cameraFitnessDataList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultCameraFitnessDataShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultCameraFitnessDataShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the cameraFitnessDataList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultCameraFitnessDataShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllCameraFitnessDataByDeviceIdIsEqualToSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where deviceId equals to DEFAULT_DEVICE_ID
        defaultCameraFitnessDataShouldBeFound("deviceId.equals=" + DEFAULT_DEVICE_ID);

        // Get all the cameraFitnessDataList where deviceId equals to UPDATED_DEVICE_ID
        defaultCameraFitnessDataShouldNotBeFound("deviceId.equals=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByDeviceIdIsInShouldWork() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where deviceId in DEFAULT_DEVICE_ID or UPDATED_DEVICE_ID
        defaultCameraFitnessDataShouldBeFound("deviceId.in=" + DEFAULT_DEVICE_ID + "," + UPDATED_DEVICE_ID);

        // Get all the cameraFitnessDataList where deviceId equals to UPDATED_DEVICE_ID
        defaultCameraFitnessDataShouldNotBeFound("deviceId.in=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByDeviceIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where deviceId is not null
        defaultCameraFitnessDataShouldBeFound("deviceId.specified=true");

        // Get all the cameraFitnessDataList where deviceId is null
        defaultCameraFitnessDataShouldNotBeFound("deviceId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByDeviceIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where deviceId greater than or equals to DEFAULT_DEVICE_ID
        defaultCameraFitnessDataShouldBeFound("deviceId.greaterOrEqualThan=" + DEFAULT_DEVICE_ID);

        // Get all the cameraFitnessDataList where deviceId greater than or equals to UPDATED_DEVICE_ID
        defaultCameraFitnessDataShouldNotBeFound("deviceId.greaterOrEqualThan=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraFitnessDataByDeviceIdIsLessThanSomething() throws Exception {
        // Initialize the database
        cameraFitnessDataRepository.saveAndFlush(cameraFitnessData);

        // Get all the cameraFitnessDataList where deviceId less than or equals to DEFAULT_DEVICE_ID
        defaultCameraFitnessDataShouldNotBeFound("deviceId.lessThan=" + DEFAULT_DEVICE_ID);

        // Get all the cameraFitnessDataList where deviceId less than or equals to UPDATED_DEVICE_ID
        defaultCameraFitnessDataShouldBeFound("deviceId.lessThan=" + UPDATED_DEVICE_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCameraFitnessDataShouldBeFound(String filter) throws Exception {
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cameraFitnessData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));

        // Check, that the count call also returns 1
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCameraFitnessDataShouldNotBeFound(String filter) throws Exception {
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCameraFitnessData() throws Exception {
        // Get the cameraFitnessData
        restCameraFitnessDataMockMvc.perform(get("/api/camera-fitness-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCameraFitnessData() throws Exception {
        // Initialize the database
        cameraFitnessDataService.save(cameraFitnessData);

        int databaseSizeBeforeUpdate = cameraFitnessDataRepository.findAll().size();

        // Update the cameraFitnessData
        CameraFitnessData updatedCameraFitnessData = cameraFitnessDataRepository.findById(cameraFitnessData.getId()).get();
        // Disconnect from session so that the updates on updatedCameraFitnessData are not directly saved in db
        em.detach(updatedCameraFitnessData);
        updatedCameraFitnessData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(UPDATED_DEVICE_ID);

        restCameraFitnessDataMockMvc.perform(put("/api/camera-fitness-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCameraFitnessData)))
            .andExpect(status().isOk());

        // Validate the CameraFitnessData in the database
        List<CameraFitnessData> cameraFitnessDataList = cameraFitnessDataRepository.findAll();
        assertThat(cameraFitnessDataList).hasSize(databaseSizeBeforeUpdate);
        CameraFitnessData testCameraFitnessData = cameraFitnessDataList.get(cameraFitnessDataList.size() - 1);
        assertThat(testCameraFitnessData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testCameraFitnessData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testCameraFitnessData.getDeviceId()).isEqualTo(UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCameraFitnessData() throws Exception {
        int databaseSizeBeforeUpdate = cameraFitnessDataRepository.findAll().size();

        // Create the CameraFitnessData

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCameraFitnessDataMockMvc.perform(put("/api/camera-fitness-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cameraFitnessData)))
            .andExpect(status().isBadRequest());

        // Validate the CameraFitnessData in the database
        List<CameraFitnessData> cameraFitnessDataList = cameraFitnessDataRepository.findAll();
        assertThat(cameraFitnessDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCameraFitnessData() throws Exception {
        // Initialize the database
        cameraFitnessDataService.save(cameraFitnessData);

        int databaseSizeBeforeDelete = cameraFitnessDataRepository.findAll().size();

        // Delete the cameraFitnessData
        restCameraFitnessDataMockMvc.perform(delete("/api/camera-fitness-data/{id}", cameraFitnessData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CameraFitnessData> cameraFitnessDataList = cameraFitnessDataRepository.findAll();
        assertThat(cameraFitnessDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CameraFitnessData.class);
        CameraFitnessData cameraFitnessData1 = new CameraFitnessData();
        cameraFitnessData1.setId(1L);
        CameraFitnessData cameraFitnessData2 = new CameraFitnessData();
        cameraFitnessData2.setId(cameraFitnessData1.getId());
        assertThat(cameraFitnessData1).isEqualTo(cameraFitnessData2);
        cameraFitnessData2.setId(2L);
        assertThat(cameraFitnessData1).isNotEqualTo(cameraFitnessData2);
        cameraFitnessData1.setId(null);
        assertThat(cameraFitnessData1).isNotEqualTo(cameraFitnessData2);
    }
}
