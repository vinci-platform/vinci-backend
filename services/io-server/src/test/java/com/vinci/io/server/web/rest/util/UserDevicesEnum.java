package com.vinci.io.server.web.rest.util;

public enum UserDevicesEnum {
    PERSONAL_DEVICES,FAMILY_DEVICES,ORGANIZATION_DEVICES
}
