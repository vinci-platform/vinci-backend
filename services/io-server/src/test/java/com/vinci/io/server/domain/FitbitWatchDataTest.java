package com.vinci.io.server.domain;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.vinci.io.server.web.rest.TestUtil;

public class FitbitWatchDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchData.class);
        FitbitWatchData fitbitWatchData1 = new FitbitWatchData();
        fitbitWatchData1.setId(1L);
        FitbitWatchData fitbitWatchData2 = new FitbitWatchData();
        fitbitWatchData2.setId(fitbitWatchData1.getId());
        assertThat(fitbitWatchData1).isEqualTo(fitbitWatchData2);
        fitbitWatchData2.setId(2L);
        assertThat(fitbitWatchData1).isNotEqualTo(fitbitWatchData2);
        fitbitWatchData1.setId(null);
        assertThat(fitbitWatchData1).isNotEqualTo(fitbitWatchData2);
    }
}
