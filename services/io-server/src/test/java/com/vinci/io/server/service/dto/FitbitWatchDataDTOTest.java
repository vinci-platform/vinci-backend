package com.vinci.io.server.service.dto;

import com.vinci.io.server.web.rest.TestUtil;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class FitbitWatchDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchDataDTO.class);
        FitbitWatchDataDTO fitbitWatchDataDTO1 = new FitbitWatchDataDTO();
        fitbitWatchDataDTO1.setId(1L);
        FitbitWatchDataDTO fitbitWatchDataDTO2 = new FitbitWatchDataDTO();
        assertThat(fitbitWatchDataDTO1).isNotEqualTo(fitbitWatchDataDTO2);
        fitbitWatchDataDTO2.setId(fitbitWatchDataDTO1.getId());
        assertThat(fitbitWatchDataDTO1).isEqualTo(fitbitWatchDataDTO2);
        fitbitWatchDataDTO2.setId(2L);
        assertThat(fitbitWatchDataDTO1).isNotEqualTo(fitbitWatchDataDTO2);
        fitbitWatchDataDTO1.setId(null);
        assertThat(fitbitWatchDataDTO1).isNotEqualTo(fitbitWatchDataDTO2);
    }
}
