package com.vinci.io.server.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.FitbitWatchData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.FitbitWatchDataRepository;
import com.vinci.io.server.service.*;
import com.vinci.io.server.service.mapper.FitbitWatchDataMapper;

import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.*;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.ORGANIZATION_DEVICES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FitbitWatchDataResource} REST controller.
 */
@SpringBootTest(classes = IoserverApp.class)
@AutoConfigureMockMvc
@WithMockUser
@RunWith(SpringRunner.class)
public class FitbitWatchDataResourceIntTest extends SetupController{

    private static final String DEFAULT_SLEEP_DATA = "{\"sleep\":[{\"duration\":28920000,\"efficiency\":93,\"endTime\":\"2021-11-09T09:15:00.000\",\"startTime\":\"2021-11-09T01:12:30.000\",\"isMainSleep\":true,\"minutesAfterWakeup\":0,\"minutesAsleep\":437,\"minutesAwake\":45,\"minutesToFallAsleep\":0,\"timeInBed\":482,\"levels\":{\"data\":[{\"dateTime\":\"2021-11-09T01:12:30.000\",\"level\":\"wake\",\"seconds\":30},{\"dateTime\":\"2021-11-09T01:13:00.000\",\"level\":\"light\",\"seconds\":600},{\"dateTime\":\"2021-11-09T01:23:00.000\",\"level\":\"deep\",\"seconds\":2910},{\"dateTime\":\"2021-11-09T02:11:30.000\",\"level\":\"light\",\"seconds\":840},{\"dateTime\":\"2021-11-09T02:25:30.000\",\"level\":\"rem\",\"seconds\":420},{\"dateTime\":\"2021-11-09T02:32:30.000\",\"level\":\"light\",\"seconds\":3120},{\"dateTime\":\"2021-11-09T03:24:30.000\",\"level\":\"deep\",\"seconds\":1350},{\"dateTime\":\"2021-11-09T03:47:00.000\",\"level\":\"light\",\"seconds\":480},{\"dateTime\":\"2021-11-09T03:55:00.000\",\"level\":\"rem\",\"seconds\":1740},{\"dateTime\":\"2021-11-09T04:24:00.000\",\"level\":\"light\",\"seconds\":1830},{\"dateTime\":\"2021-11-09T04:54:30.000\",\"level\":\"deep\",\"seconds\":330},{\"dateTime\":\"2021-11-09T05:00:00.000\",\"level\":\"light\",\"seconds\":2430},{\"dateTime\":\"2021-11-09T05:40:30.000\",\"level\":\"rem\",\"seconds\":660},{\"dateTime\":\"2021-11-09T05:51:30.000\",\"level\":\"light\",\"seconds\":2820},{\"dateTime\":\"2021-11-09T06:38:30.000\",\"level\":\"deep\",\"seconds\":1410},{\"dateTime\":\"2021-11-09T07:02:00.000\",\"level\":\"light\",\"seconds\":720},{\"dateTime\":\"2021-11-09T07:14:00.000\",\"level\":\"rem\",\"seconds\":2640},{\"dateTime\":\"2021-11-09T07:58:00.000\",\"level\":\"light\",\"seconds\":2820},{\"dateTime\":\"2021-11-09T08:45:00.000\",\"level\":\"wake\",\"seconds\":630},{\"dateTime\":\"2021-11-09T08:55:30.000\",\"level\":\"light\",\"seconds\":270},{\"dateTime\":\"2021-11-09T09:00:00.000\",\"level\":\"wake\",\"seconds\":660},{\"dateTime\":\"2021-11-09T09:11:00.000\",\"level\":\"rem\",\"seconds\":240}]}}],\"summary\":{\"stages\":{\"deep\":96,\"light\":248,\"rem\":93,\"wake\":45},\"totalMinutesAsleep\":437,\"totalTimeInBed\":482}}";
    private static final String UPDATED_SLEEP_DATA = "{\"sleep\":[{\"duration\":28920001,\"efficiency\":94,\"endTime\":\"2021-11-09T09:15:00.000\",\"startTime\":\"2021-11-09T01:12:30.000\",\"isMainSleep\":true,\"minutesAfterWakeup\":0,\"minutesAsleep\":437,\"minutesAwake\":45,\"minutesToFallAsleep\":0,\"timeInBed\":482,\"levels\":{\"data\":[{\"dateTime\":\"2021-11-09T01:12:30.000\",\"level\":\"wake\",\"seconds\":30},{\"dateTime\":\"2021-11-09T01:13:00.000\",\"level\":\"light\",\"seconds\":600},{\"dateTime\":\"2021-11-09T01:23:00.000\",\"level\":\"deep\",\"seconds\":2910},{\"dateTime\":\"2021-11-09T02:11:30.000\",\"level\":\"light\",\"seconds\":840},{\"dateTime\":\"2021-11-09T02:25:30.000\",\"level\":\"rem\",\"seconds\":420},{\"dateTime\":\"2021-11-09T02:32:30.000\",\"level\":\"light\",\"seconds\":3120},{\"dateTime\":\"2021-11-09T03:24:30.000\",\"level\":\"deep\",\"seconds\":1350},{\"dateTime\":\"2021-11-09T03:47:00.000\",\"level\":\"light\",\"seconds\":480},{\"dateTime\":\"2021-11-09T03:55:00.000\",\"level\":\"rem\",\"seconds\":1740},{\"dateTime\":\"2021-11-09T04:24:00.000\",\"level\":\"light\",\"seconds\":1830},{\"dateTime\":\"2021-11-09T04:54:30.000\",\"level\":\"deep\",\"seconds\":330},{\"dateTime\":\"2021-11-09T05:00:00.000\",\"level\":\"light\",\"seconds\":2430},{\"dateTime\":\"2021-11-09T05:40:30.000\",\"level\":\"rem\",\"seconds\":660},{\"dateTime\":\"2021-11-09T05:51:30.000\",\"level\":\"light\",\"seconds\":2820},{\"dateTime\":\"2021-11-09T06:38:30.000\",\"level\":\"deep\",\"seconds\":1410},{\"dateTime\":\"2021-11-09T07:02:00.000\",\"level\":\"light\",\"seconds\":720},{\"dateTime\":\"2021-11-09T07:14:00.000\",\"level\":\"rem\",\"seconds\":2640},{\"dateTime\":\"2021-11-09T07:58:00.000\",\"level\":\"light\",\"seconds\":2820},{\"dateTime\":\"2021-11-09T08:45:00.000\",\"level\":\"wake\",\"seconds\":630},{\"dateTime\":\"2021-11-09T08:55:30.000\",\"level\":\"light\",\"seconds\":270},{\"dateTime\":\"2021-11-09T09:00:00.000\",\"level\":\"wake\",\"seconds\":660},{\"dateTime\":\"2021-11-09T09:11:00.000\",\"level\":\"rem\",\"seconds\":240}]}}],\"summary\":{\"stages\":{\"deep\":96,\"light\":248,\"rem\":93,\"wake\":45},\"totalMinutesAsleep\":437,\"totalTimeInBed\":482}}";

    public static final Integer DEFAULT_SLEEP_DATA_SLEEP_DURATION = 28920000;

    private static final String DEFAULT_ACTIVITY_DATA = "{\"activityCalories\":529,\"caloriesBMR\":1856,\"caloriesOut\":2415,\"elevation\":0,\"fairlyActiveMinutes\":0,\"floors\":0,\"lightlyActiveMinutes\":135,\"marginalCalories\":231,\"restingHeartRate\":58,\"sedentaryMinutes\":823,\"steps\":1545,\"veryActiveMinutes\":0,\"heartRateZones\":[{\"caloriesOut\":2337,\"max\":112,\"min\":30,\"minutes\":1422,\"name\":\"Out of Range\"},{\"caloriesOut\":68,\"max\":140,\"min\":112,\"minutes\":11,\"name\":\"Fat Burn\"},{\"caloriesOut\":0,\"max\":174,\"min\":140,\"minutes\":0,\"name\":\"Cardio\"},{\"caloriesOut\":0,\"max\":220,\"min\":174,\"minutes\":0,\"name\":\"Peak\"}],\"distances\":[{\"activity\":\"total\",\"distance\":\"1.15\"},{\"activity\":\"tracker\",\"distance\":\"1.15\"},{\"activity\":\"loggedActivities\",\"distance\":\"0\"},{\"activity\":\"veryActive\",\"distance\":\"0\"},{\"activity\":\"moderatelyActive\",\"distance\":\"0\"},{\"activity\":\"lightlyActive\",\"distance\":\"1.14\"},{\"activity\":\"sedentaryActive\",\"distance\":\"0\"}]}";
    private static final String UPDATED_ACTIVITY_DATA = "{\"activityCalories\":530,\"caloriesBMR\":1857,\"caloriesOut\":2416,\"elevation\":0,\"fairlyActiveMinutes\":0,\"floors\":0,\"lightlyActiveMinutes\":136,\"marginalCalories\":232,\"restingHeartRate\":59,\"sedentaryMinutes\":824,\"steps\":1546,\"veryActiveMinutes\":1,\"heartRateZones\":[{\"caloriesOut\":2338,\"max\":113,\"min\":31,\"minutes\":1423,\"name\":\"Out of Range\"},{\"caloriesOut\":69,\"max\":141,\"min\":113,\"minutes\":12,\"name\":\"Fat Burn\"},{\"caloriesOut\":1,\"max\":175,\"min\":141,\"minutes\":1,\"name\":\"Cardio\"},{\"caloriesOut\":1,\"max\":221,\"min\":175,\"minutes\":1,\"name\":\"Peak\"}],\"distances\":[{\"activity\":\"total\",\"distance\":\"1.16\"},{\"activity\":\"tracker\",\"distance\":\"1.16\"},{\"activity\":\"loggedActivities\",\"distance\":\"1\"},{\"activity\":\"veryActive\",\"distance\":\"1\"},{\"activity\":\"moderatelyActive\",\"distance\":\"1\"},{\"activity\":\"lightlyActive\",\"distance\":\"1.15\"},{\"activity\":\"sedentaryActive\",\"distance\":\"1\"}]}";

    public static final Integer DEFAULT_ACTIVITY_DATA_ACTIVITY_CALORIES = 529;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.FITBIT_WATCH;
    private static final DeviceType DIFFERENT_DEVICE_TYPE = DeviceType.WATCH;

    @Autowired
    private FitbitWatchDataRepository fitbitwatchdataRepository;

    @Autowired
    private FitbitWatchDataMapper fitbitwatchdataMapper;

//    @Autowired
    private FitbitWatchDataService fitbitwatchdataService;

//    @Autowired
    private FitbitWatchDataQueryService fitbitwatchdataQueryService;

    @Autowired
    private DeviceServiceUtility deviceServiceUtility;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private MockMvc restFitbitwatchdataMockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private GatewayClient gatewayClientMock;

    private FitbitWatchData fitbitwatchdata;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeviceService deviceService = new DeviceService(gatewayClientMock);
        fitbitwatchdataQueryService = new FitbitWatchDataQueryService(fitbitwatchdataRepository, fitbitwatchdataMapper, deviceService, deviceServiceUtility);
        fitbitwatchdataService = new FitbitWatchDataService(fitbitwatchdataRepository, fitbitwatchdataMapper,
            fitbitwatchdataQueryService, deviceService, deviceServiceUtility);
        final FitbitWatchDataResource fitbitFitbitWatchDataResource = new FitbitWatchDataResource(fitbitwatchdataService, fitbitwatchdataQueryService);
        this.restFitbitwatchdataMockMvc = MockMvcBuilders.standaloneSetup(fitbitFitbitWatchDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }



    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FitbitWatchData createEntity(EntityManager em) {
        return new FitbitWatchData()
            .sleepData(DEFAULT_SLEEP_DATA)
            .activityData(DEFAULT_ACTIVITY_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
    }

    private void initTest() {
        fitbitwatchdata = createEntity(em);
        setupDeviceDto(DEFAULT_DEVICE_TYPE);
        setupUserDto();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_NoDeviceId() throws Exception {
        //setup
        fitbitwatchdata.setDeviceId(null);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_IdNotNull() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setId(1L);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsPacient() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsCreated();
        em.clear();
        fitbitwatchdata.setId(null);
        createFitbitWatchDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsPacient_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsPacient_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsPacient_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacianet_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacianet_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacianet_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacianet_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacient_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsCreated();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacient_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createFitbitWatchData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createFitbitWatchDataRestCall_IsBadRequest();
        em.clear();
        createFitbitWatchDataSyncRestCall_IsBadRequest();
    }

    private void createFitbitWatchDataRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = fitbitwatchdataRepository.findAll().size();
        // Create the FitbitWatchData
        restFitbitwatchdataMockMvc.perform(post("/api/fitbit-watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitwatchdata)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.sleepData").value(DEFAULT_SLEEP_DATA))
            .andExpect(jsonPath("$.activityData").value(DEFAULT_ACTIVITY_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the FitbitWatchData in the database
        List<FitbitWatchData> fitbitwatchdataList = fitbitwatchdataRepository.findAll();
        assertThat(fitbitwatchdataList).hasSize(databaseSizeBeforeTest + 1);
        FitbitWatchData testFitbitWatchData = fitbitwatchdataList.get(fitbitwatchdataList.size() - 1);
        assertThat(testFitbitWatchData.getSleepData()).isEqualTo(DEFAULT_SLEEP_DATA);
        assertThat(testFitbitWatchData.getActivityData()).isEqualTo(DEFAULT_ACTIVITY_DATA);
        assertThat(testFitbitWatchData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createFitbitWatchDataRestCall_IsBadRequest() throws Exception {
        // Create the FitbitWatchData
        restFitbitwatchdataMockMvc.perform(post("/api/fitbit-watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitwatchdata)))
            .andExpect(status().isBadRequest());
    }

    private void createFitbitWatchDataSyncRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = fitbitwatchdataRepository.findAll().size();
        // Create the FitbitWatchData
        restFitbitwatchdataMockMvc.perform(post("/api/fitbit-watch-data/sync")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(fitbitwatchdata))))
            .andExpect(status().isOk());

        // Validate the FitbitWatchData in the database
        List<FitbitWatchData> fitbitwatchdataListFromDatabase = fitbitwatchdataRepository.findAll();
        assertThat(fitbitwatchdataListFromDatabase).hasSize(databaseSizeBeforeTest + 1);
        FitbitWatchData testFitbitWatchData = fitbitwatchdataListFromDatabase.get(fitbitwatchdataListFromDatabase.size() - 1);
        assertThat(testFitbitWatchData.getSleepData()).isEqualTo(DEFAULT_SLEEP_DATA);
        assertThat(testFitbitWatchData.getActivityData()).isEqualTo(DEFAULT_ACTIVITY_DATA);
        assertThat(testFitbitWatchData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createFitbitWatchDataSyncRestCall_IsBadRequest() throws Exception {
        // Create the FitbitWatchData
        restFitbitwatchdataMockMvc.perform(post("/api/fitbit-watch-data/sync")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(fitbitwatchdata))))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_IdNotNull() throws Exception {
        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(null);

        int databaseSizeBeforeTest = fitbitwatchdataRepository.findAll().size();
        restFitbitwatchdataMockMvc.perform(put("/api/fitbit-watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fitbitwatchdata)))
            .andExpect(status().isBadRequest());

        // Validate the FitbitWatchData in the database
        List<FitbitWatchData> fitbitwatchdataList = fitbitwatchdataRepository.findAll();
        assertThat(fitbitwatchdataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsPacient_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsPacient_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateFitbitWatchData_OfPacient_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataService.justSave(fitbitwatchdata);

        updateFitbitWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    private void updateFitbitWatchDataRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = fitbitwatchdataRepository.findAll().size();
        // Update the fitbitwatchdata
        FitbitWatchData updatedFitbitWatchData = fitbitwatchdataRepository.findById(fitbitwatchdata.getId()).get();
        // Disconnect from session so that the updates on updatedFitbitWatchData are not directly saved in db
        em.detach(updatedFitbitWatchData);
        updatedFitbitWatchData
            .sleepData(UPDATED_SLEEP_DATA)
            .activityData(UPDATED_ACTIVITY_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);

        restFitbitwatchdataMockMvc.perform(put("/api/fitbit-watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFitbitWatchData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.sleepData").value(UPDATED_SLEEP_DATA))
            .andExpect(jsonPath("$.activityData").value(UPDATED_ACTIVITY_DATA))
            .andExpect(jsonPath("$.timestamp").value(UPDATED_TIMESTAMP))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the FitbitWatchData in the database
        List<FitbitWatchData> fitbitwatchdataList = fitbitwatchdataRepository.findAll();
        assertThat(fitbitwatchdataList).hasSize(databaseSizeBeforeTest);
        FitbitWatchData testFitbitWatchData = fitbitwatchdataList.get(fitbitwatchdataList.size() - 1);
        assertThat(testFitbitWatchData.getSleepData()).isEqualTo(UPDATED_SLEEP_DATA);
        assertThat(testFitbitWatchData.getActivityData()).isEqualTo(UPDATED_ACTIVITY_DATA);
        assertThat(testFitbitWatchData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testFitbitWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void updateFitbitWatchDataRestCall_IsBadRequest(Long updateDeviceId) throws Exception {
        int databaseSizeBeforeTest = fitbitwatchdataRepository.findAll().size();
        // Update the fitbitwatchdata
        FitbitWatchData updatedFitbitWatchData = fitbitwatchdataRepository.findById(fitbitwatchdata.getId()).get();
        // Disconnect from session so that the updates on updatedFitbitWatchData are not directly saved in db
        em.detach(updatedFitbitWatchData);
        updatedFitbitWatchData
            .sleepData(UPDATED_SLEEP_DATA)
            .activityData(UPDATED_ACTIVITY_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(updateDeviceId);

        restFitbitwatchdataMockMvc.perform(put("/api/fitbit-watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFitbitWatchData)))
            .andExpect(status().isBadRequest());

        // Validate the FitbitWatchData in the database
        List<FitbitWatchData> fitbitwatchdataList = fitbitwatchdataRepository.findAll();
        assertThat(fitbitwatchdataList).hasSize(databaseSizeBeforeTest);
        FitbitWatchData testFitbitWatchData = fitbitwatchdataList.get(fitbitwatchdataList.size() - 1);
        assertThat(testFitbitWatchData.getSleepData()).isEqualTo(DEFAULT_SLEEP_DATA);
        assertThat(testFitbitWatchData.getActivityData()).isEqualTo(DEFAULT_ACTIVITY_DATA);
        assertThat(testFitbitWatchData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testFitbitWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_IsOk();
        getAllFitbitWatchDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_EmptyList();
        getAllFitbitWatchDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_IsOk();
        getAllFitbitWatchDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_IsOk();
        getAllFitbitWatchDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_EmptyList();
        getAllFitbitWatchDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_EmptyList();
        getAllFitbitWatchDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_IsOk();
        getAllFitbitWatchDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_EmptyList();
        getAllFitbitWatchDataPayloadRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_IsOk();
        getAllFitbitWatchDataPayloadRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchDataRestCall_EmptyList();
        getAllFitbitWatchDataPayloadRestCall_EmptyList();
    }

    private void getAllFitbitWatchDataRestCall_IsOk() throws Exception {
        // Get all the fitbitwatchdataList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitwatchdata.getId().intValue())))
            .andExpect(jsonPath("$.[*].sleepData").value(hasItem(DEFAULT_SLEEP_DATA)))
            .andExpect(jsonPath("$.[*].activityData").value(hasItem(DEFAULT_ACTIVITY_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllFitbitWatchDataRestCall_EmptyList() throws Exception {
        // Get all the fitbitwatchdataList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    private void getAllFitbitWatchDataPayloadRestCall_IsOk() throws Exception {
        // Get all the fitbitwatchdataPayloadList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/payload?sort=id,desc"))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitwatchdata.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityData.activityCalories").value(hasItem(DEFAULT_ACTIVITY_DATA_ACTIVITY_CALORIES)))
            .andExpect(jsonPath("$.[*].sleepData.sleep.[*].duration").value(hasItem(DEFAULT_SLEEP_DATA_SLEEP_DURATION)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].device.id").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllFitbitWatchDataPayloadRestCall_EmptyList() throws Exception {
        // Get all the fitbitwatchdataPayloadList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/payload?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_FiltersWithEquals_AsPacient() throws Exception {
        // setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_FiltersWithEquals_AsFamily() throws Exception {
        // setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllFitbitWatchData_FiltersWithEquals_AsOrganization() throws Exception {
        // setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getAllFitbitWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    private void getAllFitbitWatchData_DeviceIdPlusRestOfFilters(Long validDeviceId, Long invalidDeviceId) throws Exception {
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDeviceId.intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdEqualsFilter, invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDeviceId.intValue() + "," + invalidDeviceId.intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdInFilter, invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultFitbitWatchDataShouldBeFound(validFilter,fitbitwatchdata);
        defaultFitbitWatchDataShouldNotBeFound(invalidFilter);
        testValidAndInvalidFilters(validFilter,invalidFilter,"sleepData.contains=","\"duration\":28920000","\"duration\":28920001");
        testValidAndInvalidFilters(validFilter,invalidFilter,"activityData.contains=","\"activityCalories\":529","\"activityCalories\":530");
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",fitbitwatchdata.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultFitbitWatchDataShouldBeFound(filter+validValue,fitbitwatchdata);
        defaultFitbitWatchDataShouldNotBeFound(filter+invalidValue);
        defaultFitbitWatchDataShouldBeFound(validFilter+"&"+filter+validValue,fitbitwatchdata);
        defaultFitbitWatchDataShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultFitbitWatchDataShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultFitbitWatchDataShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
//        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
//        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultFitbitWatchDataShouldBeFound(String filter,FitbitWatchData _fitbitwatchdata) throws Exception {
        // Get all the fitbitwatchdataList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_fitbitwatchdata.getId().intValue())))
            .andExpect(jsonPath("$.[*].sleepData").value(hasItem(DEFAULT_SLEEP_DATA)))
            .andExpect(jsonPath("$.[*].activityData").value(hasItem(DEFAULT_ACTIVITY_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));

        // Get all the fitbitwatchdataPayloadList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/payload?sort=id,desc&"+filter))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_fitbitwatchdata.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityData.activityCalories").value(hasItem(DEFAULT_ACTIVITY_DATA_ACTIVITY_CALORIES)))
            .andExpect(jsonPath("$.[*].sleepData.sleep.[*].duration").value(hasItem(DEFAULT_SLEEP_DATA_SLEEP_DURATION)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].device.id").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultFitbitWatchDataShouldNotBeFound(String filter) throws Exception {
        // Get all the fitbitwatchdatadList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Get all the fitbitwatchdataPayloadList
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/payload?sort=id,desc&"+filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsPacient() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsPacient_NoDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsPacient_BadDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsPacient_BadId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(-1L);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(-1L);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(-1L);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(-1L);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setDeviceId(BAD_DEVICE_ID);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getFitbitWatchDataById_OfPacient_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdata.setId(-1L);
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    private void getFitbitWatchDataByIdRestCall_IsOk() throws Exception {
        // Get the fitbitwatchdata
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/{id}", fitbitwatchdata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fitbitwatchdata.getId().intValue()))
            .andExpect(jsonPath("$.sleepData").value(DEFAULT_SLEEP_DATA))
            .andExpect(jsonPath("$.activityData").value(DEFAULT_ACTIVITY_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    private void getFitbitWatchDataByIdRestCall_IsUnauthorized() throws Exception {
        // Get the fitbitwatchdata
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/{id}", fitbitwatchdata.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getFitbitWatchDataByIdRestCall_IsBadRequest(MediaType returnMediaType) throws Exception {
        // Get the fitbitwatchdata
        restFitbitwatchdataMockMvc.perform(get("/api/fitbit-watch-data/{id}", fitbitwatchdata.getId()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(returnMediaType));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchData.class);
        FitbitWatchData fitbitwatchdata1 = new FitbitWatchData();
        fitbitwatchdata1.setId(1L);
        FitbitWatchData fitbitwatchdata2 = new FitbitWatchData();
        fitbitwatchdata2.setId(fitbitwatchdata1.getId());
        assertThat(fitbitwatchdata1).isEqualTo(fitbitwatchdata2);
        fitbitwatchdata2.setId(2L);
        assertThat(fitbitwatchdata1).isNotEqualTo(fitbitwatchdata2);
        fitbitwatchdata1.setId(null);
        assertThat(fitbitwatchdata1).isNotEqualTo(fitbitwatchdata2);
    }

    @Test
    @Transactional
    public void deleteFitbitWatchDataById_AsAdmin() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        fitbitwatchdataRepository.saveAndFlush(fitbitwatchdata);

        int numberOfEntitiesBeforeTest = fitbitwatchdataRepository.findAll().size();
        // Get the fitbitwatchdata
        restFitbitwatchdataMockMvc.perform(delete("/api/fitbit-watch-data/{id}", fitbitwatchdata.getId()))
            .andExpect(status().isOk());

        List<FitbitWatchData> listInDb = fitbitwatchdataRepository.findAll();
        assertThat(listInDb.size()).isEqualTo(numberOfEntitiesBeforeTest-1);
    }
}
