package com.vinci.io.server.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.WatchData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.WatchDataRepository;
import com.vinci.io.server.service.DeviceService;
import com.vinci.io.server.service.WatchDataQueryService;
import com.vinci.io.server.service.WatchDataService;
import com.vinci.io.server.service.mapper.WatchDataMapper;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.adapters.WatchDataAdapter;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.*;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.ORGANIZATION_DEVICES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the WatchDataResource REST controller.
 *
 * @see WatchDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IoserverApp.class)
public class WatchDataResourceIntTest extends SetupController{

    private static final String DEFAULT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.WATCH;
    private static final DeviceType DIFFERENT_DEVICE_TYPE = DeviceType.SURVEY;

    @Autowired
    private WatchDataRepository watchDataRepository;

    private WatchDataService watchDataService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private WatchDataAdapter watchDataAdapter;

    @Autowired
    private WatchDataMapper watchDataMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private GatewayClient gatewayClientMock;

    private MockMvc restWatchDataMockMvc;

    private WatchData watchData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeviceService deviceService = new DeviceService(gatewayClientMock);
        DeviceServiceUtility deviceServiceUtility = new DeviceServiceUtility(deviceService);
        WatchDataQueryService watchDataQueryService = new WatchDataQueryService(watchDataRepository, gatewayClientMock, deviceService, deviceServiceUtility);
        watchDataService = new WatchDataService(watchDataRepository, watchDataMapper, gatewayClientMock,
            watchDataQueryService, deviceService, objectMapper);
        final WatchDataResource watchDataResource = new WatchDataResource(watchDataService, watchDataQueryService, watchDataAdapter);
        this.restWatchDataMockMvc = MockMvcBuilders.standaloneSetup(watchDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WatchData createEntity(EntityManager em) {
        return new WatchData()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
    }

    private void initTest() {
        watchData = createEntity(em);
        setupDeviceDto(DEFAULT_DEVICE_TYPE);
        setupUserDto();
    }

    @Test
    @Transactional
    public void createWatchData_NoDeviceId() throws Exception {
        //setup
        watchData.setDeviceId(null);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void createWatchData_IdNotNull() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        watchData.setId(1L);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsPacient() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsCreated();
        em.clear();
        createWatchDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createWatchData_AsPacient_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsPacient_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsPacient_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        watchData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void createWatchData_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsCreated();
        em.clear();
        createWatchDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createWatchData_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        watchData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacianet_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsCreated();
        em.clear();
        createWatchDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacianet_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacianet_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacianet_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        watchData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void createWatchData_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsCreated();
        em.clear();
        createWatchDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createWatchData_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        watchData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacient_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsCreated();
        em.clear();
        createWatchDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacient_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createWatchData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        watchData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createWatchDataRestCall_IsBadRequest();
        em.clear();
        createWatchDataImportRestCall_EmptyList();
    }

    private void createWatchDataRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        // Create the WatchData
        restWatchDataMockMvc.perform(post("/api/watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(watchData)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the WatchData in the database
        List<WatchData> watchDataList = watchDataRepository.findAll();
        assertThat(watchDataList).hasSize(databaseSizeBeforeTest + 1);
        WatchData testWatchData = watchDataList.get(watchDataList.size() - 1);
        assertThat(testWatchData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testWatchData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createWatchDataRestCall_IsBadRequest() throws Exception {
        // Create the WatchData
        restWatchDataMockMvc.perform(post("/api/watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(watchData)))
            .andExpect(status().isBadRequest());
    }

    private void createWatchDataImportRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        // Create the WatchData
        restWatchDataMockMvc.perform(post("/api/watch-data/import")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(new ArrayList<WatchData>(){{add(watchData);}}))))
            .andExpect(status().isOk());

        // Validate the WatchData in the database
        List<WatchData> watchDataListFromDatabase = watchDataRepository.findAll();
        assertThat(watchDataListFromDatabase).hasSize(databaseSizeBeforeTest + 1);
        WatchData testWatchData = watchDataListFromDatabase.get(watchDataListFromDatabase.size() - 1);
        assertThat(testWatchData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testWatchData.getTimestamp()).isGreaterThan(Instant.now().toEpochMilli()-10);
        assertThat(testWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createWatchDataImportRestCall_IsBadRequest() throws Exception {
        // Create the WatchData
        restWatchDataMockMvc.perform(post("/api/watch-data/import")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(new ArrayList<WatchData>(){{add(watchData);}}))))
            .andExpect(status().isBadRequest());
    }

    private void createWatchDataImportRestCall_EmptyList() throws Exception {
        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        // Create the WatchData
        restWatchDataMockMvc.perform(post("/api/watch-data/import")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectMapper.writeValueAsString(new ArrayList<WatchData>(){{add(watchData);}}))))
            .andExpect(status().isOk());
        // Validate the WatchData in the database
        List<WatchData> watchDataListFromDatabase = watchDataRepository.findAll();
        assertThat(watchDataListFromDatabase).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateWatchData_IdNotNull() throws Exception {
        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(null);

        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        restWatchDataMockMvc.perform(put("/api/watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(watchData)))
            .andExpect(status().isBadRequest());

        // Validate the WatchData in the database
        List<WatchData> watchDataList = watchDataRepository.findAll();
        assertThat(watchDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateWatchData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateWatchData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsPacient_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsPacient_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateWatchData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateWatchData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateWatchData_OfPacient_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataService.justSave(watchData);

        updateWatchDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    private void updateWatchDataRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        // Update the watchData
        WatchData updatedWatchData = watchDataRepository.findById(watchData.getId()).get();
        // Disconnect from session so that the updates on updatedWatchData are not directly saved in db
        em.detach(updatedWatchData);
        updatedWatchData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);

        restWatchDataMockMvc.perform(put("/api/watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWatchData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data").value(UPDATED_DATA))
            .andExpect(jsonPath("$.timestamp").value(UPDATED_TIMESTAMP))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the WatchData in the database
        List<WatchData> watchDataList = watchDataRepository.findAll();
        assertThat(watchDataList).hasSize(databaseSizeBeforeTest);
        WatchData testWatchData = watchDataList.get(watchDataList.size() - 1);
        assertThat(testWatchData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testWatchData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void updateWatchDataRestCall_IsBadRequest(Long updateDeviceId) throws Exception {
        int databaseSizeBeforeTest = watchDataRepository.findAll().size();
        // Update the watchData
        WatchData updatedWatchData = watchDataRepository.findById(watchData.getId()).get();
        // Disconnect from session so that the updates on updatedWatchData are not directly saved in db
        em.detach(updatedWatchData);
        updatedWatchData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(updateDeviceId);

        restWatchDataMockMvc.perform(put("/api/watch-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWatchData)))
            .andExpect(status().isBadRequest());

        // Validate the WatchData in the database
        List<WatchData> watchDataList = watchDataRepository.findAll();
        assertThat(watchDataList).hasSize(databaseSizeBeforeTest);
        WatchData testWatchData = watchDataList.get(watchDataList.size() - 1);
        assertThat(testWatchData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testWatchData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testWatchData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllWatchData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllWatchData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllWatchData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllWatchData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllWatchData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllWatchData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllWatchData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllWatchData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllWatchData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllWatchData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchDataRestCall_EmptyList();
    }

    private void getAllWatchDataRestCall_IsOk() throws Exception {
        // Get all the watchDataList
        restWatchDataMockMvc.perform(get("/api/watch-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(watchData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllWatchDataRestCall_EmptyList() throws Exception {
        // Get all the watchDataList
        restWatchDataMockMvc.perform(get("/api/watch-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getAllWatchData_FiltersWithEquals_AsPacient() throws Exception {
        // setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllWatchData_FiltersWithEquals_AsFamily() throws Exception {
        // setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllWatchData_FiltersWithEquals_AsOrganization() throws Exception {
        // setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getAllWatchData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    private void getAllWatchData_DeviceIdPlusRestOfFilters(Long validDeviceId, Long invalidDeviceId) throws Exception {
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDeviceId.intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdEqualsFilter, invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDeviceId.intValue() + "," + invalidDeviceId.intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdInFilter, invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultWatchDataShouldBeFound(validFilter,watchData);
        defaultWatchDataShouldNotBeFound(invalidFilter);

//        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"data",DEFAULT_DATA,UPDATED_DATA);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",watchData.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultWatchDataShouldBeFound(filter+validValue,watchData);
        defaultWatchDataShouldNotBeFound(filter+invalidValue);
        defaultWatchDataShouldBeFound(validFilter+"&"+filter+validValue,watchData);
        defaultWatchDataShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultWatchDataShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultWatchDataShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultWatchDataShouldBeFound(String filter,WatchData _watchData) throws Exception {
        restWatchDataMockMvc.perform(get("/api/watch-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_watchData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultWatchDataShouldNotBeFound(String filter) throws Exception {
        System.out.println(filter);
        restWatchDataMockMvc.perform(get("/api/watch-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

    }

    @Test
    @Transactional
    public void getWatchDataById_AsPacient() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsPacient_NoDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getWatchDataById_AsPacient_BadDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setDeviceId(BAD_DEVICE_ID);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsPacient_BadId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(-1L);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getWatchDataById_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getWatchDataById_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setDeviceId(BAD_DEVICE_ID);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(-1L);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setDeviceId(BAD_DEVICE_ID);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(-1L);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getWatchDataById_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getWatchDataById_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setDeviceId(BAD_DEVICE_ID);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getWatchDataById_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(-1L);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setDeviceId(BAD_DEVICE_ID);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getWatchDataById_OfPacient_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchData.setId(-1L);
        watchDataRepository.saveAndFlush(watchData);

        getWatchDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    private void getWatchDataByIdRestCall_IsOk() throws Exception {
        // Get the watchData
        restWatchDataMockMvc.perform(get("/api/watch-data/{id}", watchData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(watchData.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    private void getWatchDataByIdRestCall_IsUnauthorized() throws Exception {
        // Get the watchData
        restWatchDataMockMvc.perform(get("/api/watch-data/{id}", watchData.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getWatchDataByIdRestCall_IsBadRequest(MediaType returnMediaType) throws Exception {
        // Get the watchData
        restWatchDataMockMvc.perform(get("/api/watch-data/{id}", watchData.getId()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(returnMediaType));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WatchData.class);
        WatchData watchData1 = new WatchData();
        watchData1.setId(1L);
        WatchData watchData2 = new WatchData();
        watchData2.setId(watchData1.getId());
        assertThat(watchData1).isEqualTo(watchData2);
        watchData2.setId(2L);
        assertThat(watchData1).isNotEqualTo(watchData2);
        watchData1.setId(null);
        assertThat(watchData1).isNotEqualTo(watchData2);
    }

    @Test
    @Transactional
    public void deleteWatchDataById_AsAdmin() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        watchDataRepository.saveAndFlush(watchData);

        int numberOfEntitiesBeforeTest = watchDataRepository.findAll().size();
        // Get the watchData
        restWatchDataMockMvc.perform(delete("/api/watch-data/{id}", watchData.getId()))
            .andExpect(status().isOk());

        List<WatchData> listInDb = watchDataRepository.findAll();
        assertThat(listInDb.size()).isEqualTo(numberOfEntitiesBeforeTest-1);
    }
}
