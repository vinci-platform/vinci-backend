package com.vinci.io.server.web.rest;

import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.CameraMovementData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.repository.CameraMovementDataRepository;
import com.vinci.io.server.service.CameraMovementDataQueryService;
import com.vinci.io.server.service.CameraMovementDataService;
import com.vinci.io.server.service.dto.CreateCameraMovementDataDto;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.service.mapper.CameraMovementDataMapper;
import com.vinci.io.server.web.rest.beans.UserDTO;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CameraMovementDataResource REST controller.
 *
 * @see CameraMovementDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IoserverApp.class)
public class CameraMovementDataResourceIntTest {

    private static final String DEFAULT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_DEVICE_ID = 1L;
    private static final Long UPDATED_DEVICE_ID = 2L;

    private static final String DEFAULT_DEVICE_UUID = "1";
    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.CAMERA_MOVEMENT;

    private static final String DEFAULT_DEVICE_NAME = "AAAAAAAAAAAAAAAA";
    private static final String DEFAULT_DEVICE_DESCRIPTION = "AAAAAAAAAAAAAAAA";
    private static final Boolean DEFAULT_DEVICE_ACTIVE = true;
    private static final Long DEFAULT_USER_EXTRA_ID = 1L;

    static private final Long DEFAULT_USER_DTO_ID = 1L;
    static private final Long DEFAULT_USER_DTO_USER_EXTRA_ID = 1L;
    static private final String DEFAULT_USER_DTO_LOGIN = "user";
    static private final String DEFAULT_USER_DTO_FIRSTNAME = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_GENDER = "MALE";
    static private final String DEFAULT_USER_DTO_LASTNAME = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_ADDRESS = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_PHONE = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_EMAIL = "AAAAAAAAAAAAAAAA";
    static private final boolean DEFAULT_USER_DTO_ACTIVATED = true;
    static private final String DEFAULT_USER_DTO_LANG_KEY = "en";
    static private final String DEFAULT_USER_DTO_CREATED_BY = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_LAST_MODIFIED_BY = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_ROLE = "ROLE_USER,ROLE_PATIENT";
    static private final Set<String> DEFAULT_USER_DTO_AUTHORITIES = new HashSet<String>(){{
        add("ROLE_USER");
        add("ROLE_PATIENT");
    }};
    static private final String DEFAULT_USER_DTO_UUID = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_MARITAL_STATUS = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_EDUCATION = "AAAAAAAAAAAAAAAA";
    static private final String DEFAULT_USER_DTO_FRIENDS = "AAAAAAAAAAAAAAAA";

    @Autowired
    private CameraMovementDataRepository cameraMovementDataRepository;

//    @Autowired
    private CameraMovementDataService cameraMovementDataService;

    @Autowired
    private CameraMovementDataMapper cameraMovementDataMapper;

    @Autowired
    private CameraMovementDataQueryService cameraMovementDataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Mock
    private GatewayClient gatewayClientMock;

    private MockMvc restCameraMovementDataMockMvc;

    private CameraMovementData cameraMovementData;

    private CreateCameraMovementDataDto createCameraMovementDataDto;

    private DeviceDTO deviceDTO;

    private UserDTO userDTO;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        cameraMovementDataService = new CameraMovementDataService(cameraMovementDataRepository, gatewayClientMock, cameraMovementDataMapper);
        final CameraMovementDataResource cameraMovementDataResource = new CameraMovementDataResource(cameraMovementDataService, cameraMovementDataQueryService);
        this.restCameraMovementDataMockMvc = MockMvcBuilders.standaloneSetup(cameraMovementDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CameraMovementData createEntity(EntityManager em) {
        CameraMovementData cameraMovementData = new CameraMovementData()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_ID);
        return cameraMovementData;
    }

    public static DeviceDTO createDeviceDto(EntityManager em) {
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setId(DEFAULT_DEVICE_ID);
        deviceDTO.setName(DEFAULT_DEVICE_NAME);
        deviceDTO.setDescription(DEFAULT_DEVICE_DESCRIPTION);
        deviceDTO.setUuid(DEFAULT_DEVICE_UUID);
        deviceDTO.setDeviceType(DEFAULT_DEVICE_TYPE);
        deviceDTO.setActive(DEFAULT_DEVICE_ACTIVE);
        deviceDTO.setUserExtraId(DEFAULT_USER_EXTRA_ID);
        return deviceDTO;
    }

    public static UserDTO createUserDto(EntityManager em) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(DEFAULT_USER_DTO_ID);
        userDTO.setUserExtraId(DEFAULT_USER_DTO_USER_EXTRA_ID);
        userDTO.setLogin(DEFAULT_USER_DTO_LOGIN);
        userDTO.setFirstName(DEFAULT_USER_DTO_FIRSTNAME);
        userDTO.setGender(DEFAULT_USER_DTO_GENDER);
        userDTO.setLastName(DEFAULT_USER_DTO_LASTNAME);
        userDTO.setAddress(DEFAULT_USER_DTO_ADDRESS);
        userDTO.setPhone(DEFAULT_USER_DTO_PHONE);
        userDTO.setEmail(DEFAULT_USER_DTO_EMAIL);
        userDTO.setActivated(DEFAULT_USER_DTO_ACTIVATED);
        userDTO.setLangKey(DEFAULT_USER_DTO_LANG_KEY);
        userDTO.setCreatedBy(DEFAULT_USER_DTO_CREATED_BY);
        userDTO.setLastModifiedBy(DEFAULT_USER_DTO_LAST_MODIFIED_BY);
        userDTO.setRole(DEFAULT_USER_DTO_ROLE);
        userDTO.setAuthorities(DEFAULT_USER_DTO_AUTHORITIES);
        userDTO.setUuid(DEFAULT_USER_DTO_UUID);
        userDTO.setMaritalStatus(DEFAULT_USER_DTO_MARITAL_STATUS);
        userDTO.setEducation(DEFAULT_USER_DTO_EDUCATION);
        userDTO.setFriends(DEFAULT_USER_DTO_FRIENDS);

        return userDTO;
    }
    /**
     * Create an entityDTO for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CreateCameraMovementDataDto createDTOEntity(EntityManager em) {
        CreateCameraMovementDataDto cameraMovementData = new CreateCameraMovementDataDto()
            .data(DEFAULT_DATA)
            .timestamp(DEFAULT_TIMESTAMP)
            .deviceId(DEFAULT_DEVICE_UUID);
        return cameraMovementData;
    }

    @Before
    public void initTest() {
        createCameraMovementDataDto = createDTOEntity(em);
        cameraMovementData = createEntity(em);
        deviceDTO = createDeviceDto(em);
        userDTO = createUserDto(em);
    }

    @Test
    @Transactional
    public void createCameraMovementData() throws Exception {
        int databaseSizeBeforeCreate = cameraMovementDataRepository.findAll().size();


        when(gatewayClientMock.getDeviceByUid(any())).thenReturn(Optional.of(deviceDTO));
        // Create the CameraMovementData
        restCameraMovementDataMockMvc.perform(post("/api/camera-movement-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(createCameraMovementDataDto)))
            .andExpect(status().isCreated());

        // Validate the CameraMovementData in the database
        List<CameraMovementData> cameraMovementDataList = cameraMovementDataRepository.findAll();
        assertThat(cameraMovementDataList).hasSize(databaseSizeBeforeCreate + 1);
        CameraMovementData testCameraMovementData = cameraMovementDataList.get(cameraMovementDataList.size() - 1);
        assertThat(testCameraMovementData.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testCameraMovementData.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testCameraMovementData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }
//    commented out because POST is done with createcreateCameraMovementDataDto which does not have id
//    @Test
//    @Transactional
//    public void createCameraMovementDataWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = cameraMovementDataRepository.findAll().size();
//
//        // Create the CameraMovementData with an existing ID
//        cameraMovementData.setId(1L);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restCameraMovementDataMockMvc.perform(post("/api/camera-movement-data")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(cameraMovementData)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the CameraMovementData in the database
//        List<CameraMovementData> cameraMovementDataList = cameraMovementDataRepository.findAll();
//        assertThat(cameraMovementDataList).hasSize(databaseSizeBeforeCreate);
//    }

    @Test
    @Transactional
    public void getAllCameraMovementData() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cameraMovementData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    @Test
    @Transactional
    public void getCameraMovementData() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get the cameraMovementData
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data/{id}", cameraMovementData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cameraMovementData.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where timestamp equals to DEFAULT_TIMESTAMP
        defaultCameraMovementDataShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the cameraMovementDataList where timestamp equals to UPDATED_TIMESTAMP
        defaultCameraMovementDataShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultCameraMovementDataShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the cameraMovementDataList where timestamp equals to UPDATED_TIMESTAMP
        defaultCameraMovementDataShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where timestamp is not null
        defaultCameraMovementDataShouldBeFound("timestamp.specified=true");

        // Get all the cameraMovementDataList where timestamp is null
        defaultCameraMovementDataShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultCameraMovementDataShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the cameraMovementDataList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultCameraMovementDataShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultCameraMovementDataShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the cameraMovementDataList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultCameraMovementDataShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllCameraMovementDataByDeviceIdIsEqualToSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where deviceId equals to DEFAULT_DEVICE_ID
        defaultCameraMovementDataShouldBeFound("deviceId.equals=" + DEFAULT_DEVICE_ID);

        // Get all the cameraMovementDataList where deviceId equals to UPDATED_DEVICE_ID
        defaultCameraMovementDataShouldNotBeFound("deviceId.equals=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByDeviceIdIsInShouldWork() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where deviceId in DEFAULT_DEVICE_ID or UPDATED_DEVICE_ID
        defaultCameraMovementDataShouldBeFound("deviceId.in=" + DEFAULT_DEVICE_ID + "," + UPDATED_DEVICE_ID);

        // Get all the cameraMovementDataList where deviceId equals to UPDATED_DEVICE_ID
        defaultCameraMovementDataShouldNotBeFound("deviceId.in=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByDeviceIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where deviceId is not null
        defaultCameraMovementDataShouldBeFound("deviceId.specified=true");

        // Get all the cameraMovementDataList where deviceId is null
        defaultCameraMovementDataShouldNotBeFound("deviceId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByDeviceIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where deviceId greater than or equals to DEFAULT_DEVICE_ID
        defaultCameraMovementDataShouldBeFound("deviceId.greaterOrEqualThan=" + DEFAULT_DEVICE_ID);

        // Get all the cameraMovementDataList where deviceId greater than or equals to UPDATED_DEVICE_ID
        defaultCameraMovementDataShouldNotBeFound("deviceId.greaterOrEqualThan=" + UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllCameraMovementDataByDeviceIdIsLessThanSomething() throws Exception {
        // Initialize the database
        cameraMovementDataRepository.saveAndFlush(cameraMovementData);

        // Get all the cameraMovementDataList where deviceId less than or equals to DEFAULT_DEVICE_ID
        defaultCameraMovementDataShouldNotBeFound("deviceId.lessThan=" + DEFAULT_DEVICE_ID);

        // Get all the cameraMovementDataList where deviceId less than or equals to UPDATED_DEVICE_ID
        defaultCameraMovementDataShouldBeFound("deviceId.lessThan=" + UPDATED_DEVICE_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCameraMovementDataShouldBeFound(String filter) throws Exception {
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cameraMovementData.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));

        // Check, that the count call also returns 1
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCameraMovementDataShouldNotBeFound(String filter) throws Exception {
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCameraMovementData() throws Exception {
        // Get the cameraMovementData
        restCameraMovementDataMockMvc.perform(get("/api/camera-movement-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCameraMovementData() throws Exception {
        // Initialize the database
        cameraMovementDataService.save(cameraMovementData);

        int databaseSizeBeforeUpdate = cameraMovementDataRepository.findAll().size();

        // Update the cameraMovementData
        CameraMovementData updatedCameraMovementData = cameraMovementDataRepository.findById(cameraMovementData.getId()).get();
        // Disconnect from session so that the updates on updatedCameraMovementData are not directly saved in db
        em.detach(updatedCameraMovementData);
        updatedCameraMovementData
            .data(UPDATED_DATA)
            .timestamp(UPDATED_TIMESTAMP)
            .deviceId(UPDATED_DEVICE_ID);

        restCameraMovementDataMockMvc.perform(put("/api/camera-movement-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCameraMovementData)))
            .andExpect(status().isOk());

        // Validate the CameraMovementData in the database
        List<CameraMovementData> cameraMovementDataList = cameraMovementDataRepository.findAll();
        assertThat(cameraMovementDataList).hasSize(databaseSizeBeforeUpdate);
        CameraMovementData testCameraMovementData = cameraMovementDataList.get(cameraMovementDataList.size() - 1);
        assertThat(testCameraMovementData.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testCameraMovementData.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testCameraMovementData.getDeviceId()).isEqualTo(UPDATED_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCameraMovementData() throws Exception {
        int databaseSizeBeforeUpdate = cameraMovementDataRepository.findAll().size();

        // Create the CameraMovementData

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCameraMovementDataMockMvc.perform(put("/api/camera-movement-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cameraMovementData)))
            .andExpect(status().isBadRequest());

        // Validate the CameraMovementData in the database
        List<CameraMovementData> cameraMovementDataList = cameraMovementDataRepository.findAll();
        assertThat(cameraMovementDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCameraMovementData() throws Exception {
        // Initialize the database
        System.out.println(cameraMovementData);
        cameraMovementDataService
            .save(cameraMovementData);

        int databaseSizeBeforeDelete = cameraMovementDataRepository.findAll().size();

        // Delete the cameraMovementData
        restCameraMovementDataMockMvc.perform(delete("/api/camera-movement-data/{id}", cameraMovementData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CameraMovementData> cameraMovementDataList = cameraMovementDataRepository.findAll();
        assertThat(cameraMovementDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CameraMovementData.class);
        CameraMovementData cameraMovementData1 = new CameraMovementData();
        cameraMovementData1.setId(1L);
        CameraMovementData cameraMovementData2 = new CameraMovementData();
        cameraMovementData2.setId(cameraMovementData1.getId());
        assertThat(cameraMovementData1).isEqualTo(cameraMovementData2);
        cameraMovementData2.setId(2L);
        assertThat(cameraMovementData1).isNotEqualTo(cameraMovementData2);
        cameraMovementData1.setId(null);
        assertThat(cameraMovementData1).isNotEqualTo(cameraMovementData2);
    }
}
