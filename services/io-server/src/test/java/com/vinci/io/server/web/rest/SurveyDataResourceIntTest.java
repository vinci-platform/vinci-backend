package com.vinci.io.server.web.rest;

import com.vinci.io.server.IoserverApp;
import com.vinci.io.server.domain.SurveyData;
import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.domain.enumeration.SurveyType;
import com.vinci.io.server.repository.SurveyDataRepository;
import com.vinci.io.server.service.DeviceService;
import com.vinci.io.server.service.SurveyDataQueryService;
import com.vinci.io.server.service.SurveyDataService;
import com.vinci.io.server.service.utility.DeviceServiceUtility;
import com.vinci.io.server.web.rest.errors.ExceptionTranslator;
import com.vinci.io.server.web.rest.feign.client.GatewayClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.vinci.io.server.web.rest.TestUtil.createFormattingConversionService;
import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the SurveyDataResource REST controller.
 *
 * @see SurveyDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IoserverApp.class)
public class SurveyDataResourceIntTest extends SetupController {

    private static final String DEFAULT_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIER = "BBBBBBBBBB";

    private static final SurveyType DEFAULT_SURVEY_TYPE = SurveyType.WHOQOL_BREF;
    private static final SurveyType UPDATED_SURVEY_TYPE = SurveyType.IPAQ;

    private static final String DEFAULT_ASSESMENT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_ASSESMENT_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_SCORING_RESULT = 1L;
    private static final Long UPDATED_SCORING_RESULT = 2L;

    private static final Instant DEFAULT_CREATED_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ADDITIONAL_INFO = "AAAAAAAAAA";
    private static final String UPDATED_ADDITIONAL_INFO = "BBBBBBBBBB";

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.SURVEY;
    private static final DeviceType DIFFERENT_DEVICE_TYPE = DeviceType.WATCH;

    private static final String ENTITY_NAME = "ioserverSurveyData";

    @Autowired
    private SurveyDataRepository surveyDataRepository;

//    @Autowired
    private SurveyDataService surveyDataService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    @Mock
    private GatewayClient gatewayClientMock;

    private MockMvc restSurveyDataMockMvc;

    private SurveyData surveyData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeviceService deviceService = new DeviceService(gatewayClientMock);
        DeviceServiceUtility deviceServiceUtility = new DeviceServiceUtility(deviceService);
        SurveyDataQueryService surveyDataQueryService = new SurveyDataQueryService(surveyDataRepository, deviceService, deviceServiceUtility);
        surveyDataService = new SurveyDataService(surveyDataRepository, deviceService, deviceServiceUtility);
        final SurveyDataResource surveyDataResource = new SurveyDataResource(surveyDataService, surveyDataQueryService);
        this.restSurveyDataMockMvc = MockMvcBuilders.standaloneSetup(surveyDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SurveyData createEntity(EntityManager em) {
        return new SurveyData()
            .identifier(DEFAULT_IDENTIFIER)
            .surveyType(DEFAULT_SURVEY_TYPE)
            .assesmentData(DEFAULT_ASSESMENT_DATA)
            .scoringResult(DEFAULT_SCORING_RESULT)
            .createdTime(DEFAULT_CREATED_TIME)
            .endTime(DEFAULT_END_TIME)
            .additionalInfo(DEFAULT_ADDITIONAL_INFO)
            .deviceId(DEFAULT_DEVICE_ID);
    }

    private void initTest() {
        surveyData = createEntity(em);
        setupDeviceDto(DEFAULT_DEVICE_TYPE);
        setupUserDto();
    }

    @Test
    @Transactional
    public void createSurveyData_NoDeviceId() throws Exception {
        //setup
        surveyData.setDeviceId(null);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_IdNotNull() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        surveyData.setId(1L);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsPacient() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsCreated();
        em.clear();
        createSurveyDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createSurveyData_AsPacient_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsPacient_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsPacient_NoBadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        surveyData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsCreated();
        em.clear();
        createSurveyDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createSurveyData_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        surveyData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacianet_AsFamily() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsCreated();
        em.clear();
        createSurveyDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacianet_AsFamily_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacianet_AsFamily_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacianet_AsFamily_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(FAMILY_DEVICES);
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        surveyData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsCreated();
        em.clear();
        createSurveyDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createSurveyData_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(PERSONAL_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        surveyData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacient_AsOrganization() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsCreated();
        em.clear();
        createSurveyDataImportRestCall_IsOk();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacient_AsOrganization_NoDevices() throws Exception {
        //setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacient_AsOrganization_NoSurveyDevices() throws Exception {
        //setup
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createSurveyData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        //setup
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        surveyData.setDeviceId(BAD_DEVICE_ID);

        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        createSurveyDataRestCall_IsBadRequest();
        em.clear();
        createSurveyDataImportRestCall_IsBadRequest();
    }

    private void createSurveyDataRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // Create the SurveyData
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyData)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER))
            .andExpect(jsonPath("$.surveyType").value(DEFAULT_SURVEY_TYPE.name()))
            .andExpect(jsonPath("$.assesmentData").value(DEFAULT_ASSESMENT_DATA))
            .andExpect(jsonPath("$.scoringResult").value(DEFAULT_SCORING_RESULT.intValue()))
            .andExpect(jsonPath("$.createdTime").value(DEFAULT_CREATED_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.additionalInfo").value(DEFAULT_ADDITIONAL_INFO))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest + 1);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(DEFAULT_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(DEFAULT_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(DEFAULT_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(DEFAULT_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(DEFAULT_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(DEFAULT_ADDITIONAL_INFO);
        assertThat(testSurveyData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createSurveyDataRestCall_IsBadRequest() throws Exception {
        // Create the SurveyData
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyData)))
            .andExpect(status().isBadRequest());
    }

    private void createSurveyDataImportRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // Create the SurveyData
        restSurveyDataMockMvc.perform(post("/api/survey-data/import")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(surveyData.getIdentifier()));

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest + 1);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(DEFAULT_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(DEFAULT_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(DEFAULT_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(DEFAULT_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(DEFAULT_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(DEFAULT_ADDITIONAL_INFO);
        assertThat(testSurveyData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void createSurveyDataImportRestCall_IsBadRequest() throws Exception {
        // Create the SurveyData
        restSurveyDataMockMvc.perform(post("/api/survey-data/import")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyData)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateSurveyData_IdNotNull() throws Exception {
        //mock rest call to gateway
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(null);

        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyData)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateSurveyData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsPacient_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsPacient_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateSurveyData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsFamily_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsFamily_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateSurveyData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        // Initialize the database
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateSurveyData_OfPacient_AsOrganization_NoSurveyDevice() throws Exception {
        // Initialize the database
        deviceDTO.setDeviceType(DIFFERENT_DEVICE_TYPE);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataService.justSave(surveyData);

        updateSurveyDataRestCall_IsBadRequest(DEFAULT_DEVICE_ID);
    }

    private void updateSurveyDataRestCall_IsOk() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // Update the surveyData
        SurveyData updatedSurveyData = surveyDataRepository.findById(surveyData.getId()).get();
        // Disconnect from session so that the updates on updatedSurveyData are not directly saved in db
        em.detach(updatedSurveyData);
        updatedSurveyData
            .identifier(UPDATED_IDENTIFIER)
            .surveyType(UPDATED_SURVEY_TYPE)
            .assesmentData(UPDATED_ASSESMENT_DATA)
            .scoringResult(UPDATED_SCORING_RESULT)
            .createdTime(UPDATED_CREATED_TIME)
            .endTime(UPDATED_END_TIME)
            .additionalInfo(UPDATED_ADDITIONAL_INFO)
            .deviceId(DEFAULT_DEVICE_ID);

        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSurveyData)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.identifier").value(UPDATED_IDENTIFIER))
            .andExpect(jsonPath("$.surveyType").value(UPDATED_SURVEY_TYPE.name()))
            .andExpect(jsonPath("$.assesmentData").value(UPDATED_ASSESMENT_DATA))
            .andExpect(jsonPath("$.scoringResult").value(UPDATED_SCORING_RESULT.intValue()))
            .andExpect(jsonPath("$.createdTime").value(UPDATED_CREATED_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(UPDATED_END_TIME.toString()))
            .andExpect(jsonPath("$.additionalInfo").value(UPDATED_ADDITIONAL_INFO))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(UPDATED_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(UPDATED_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(UPDATED_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(UPDATED_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(UPDATED_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(UPDATED_ADDITIONAL_INFO);
        assertThat(testSurveyData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    private void updateSurveyDataRestCall_IsBadRequest(Long updateDeviceId) throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // Update the surveyData
        SurveyData updatedSurveyData = surveyDataRepository.findById(surveyData.getId()).get();
        // Disconnect from session so that the updates on updatedSurveyData are not directly saved in db
        em.detach(updatedSurveyData);
        updatedSurveyData
            .identifier(UPDATED_IDENTIFIER)
            .surveyType(UPDATED_SURVEY_TYPE)
            .assesmentData(UPDATED_ASSESMENT_DATA)
            .scoringResult(UPDATED_SCORING_RESULT)
            .createdTime(UPDATED_CREATED_TIME)
            .endTime(UPDATED_END_TIME)
            .additionalInfo(UPDATED_ADDITIONAL_INFO)
            .deviceId(updateDeviceId);

        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSurveyData)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(DEFAULT_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(DEFAULT_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(DEFAULT_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(DEFAULT_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(DEFAULT_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(DEFAULT_ADDITIONAL_INFO);
        assertThat(testSurveyData.getDeviceId()).isEqualTo(DEFAULT_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsPacient() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsPacient_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllSurveyData_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllSurveyData_OfPacient_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsFamily_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllSurveyData_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllSurveyData_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllSurveyData_OfPacient_AsOrganization_NoDevices() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyDataRestCall_EmptyList();
    }

    private void getAllSurveyDataRestCall_IsOk() throws Exception {
        // Get all the surveyDataList
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(surveyData.getId().intValue())))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].surveyType").value(hasItem(DEFAULT_SURVEY_TYPE.name())))
            .andExpect(jsonPath("$.[*].assesmentData").value(hasItem(DEFAULT_ASSESMENT_DATA)))
            .andExpect(jsonPath("$.[*].scoringResult").value(hasItem(DEFAULT_SCORING_RESULT.intValue())))
            .andExpect(jsonPath("$.[*].createdTime").value(hasItem(DEFAULT_CREATED_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].additionalInfo").value(hasItem(DEFAULT_ADDITIONAL_INFO)))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    private void getAllSurveyDataRestCall_EmptyList() throws Exception {
        // Get all the surveyDataList
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void getAllSurveyData_FiltersWithEquals_AsPacient() throws Exception {
        // setup
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyData_FiltersWithEquals_AsFamily() throws Exception {
        // setup
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyData_FiltersWithEquals_AsOrganization() throws Exception {
        // setup
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getAllSurveyData_DeviceIdPlusRestOfFilters(DEFAULT_DEVICE_ID, BAD_DEVICE_ID);
    }

    private void getAllSurveyData_DeviceIdPlusRestOfFilters(Long validDeviceId, Long invalidDeviceId) throws Exception {
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDeviceId.intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdEqualsFilter, invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDeviceId.intValue() + "," + invalidDeviceId.intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDeviceId.intValue();
        testingFilters(validDeviceIdInFilter, invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultSurveyDataShouldBeFound(validFilter,surveyData);
        defaultSurveyDataShouldNotBeFound(invalidFilter);

//        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"createdTime",DEFAULT_LABEL,UPDATED_LABEL);
//        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"endTime",DEFAULT_VALUES,UPDATED_VALUES);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"identifier",DEFAULT_IDENTIFIER,UPDATED_IDENTIFIER);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"scoringResult",DEFAULT_SCORING_RESULT,UPDATED_SCORING_RESULT);
        testValidAndInvalidFilters_ForEnumTypeValues(validFilter,invalidFilter,"surveyType",DEFAULT_SURVEY_TYPE,UPDATED_SURVEY_TYPE);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",surveyData.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultSurveyDataShouldBeFound(filter+validValue,surveyData);
        defaultSurveyDataShouldNotBeFound(filter+invalidValue);
        defaultSurveyDataShouldBeFound(validFilter+"&"+filter+validValue,surveyData);
        defaultSurveyDataShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultSurveyDataShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultSurveyDataShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSurveyDataShouldBeFound(String filter,SurveyData _surveyData) throws Exception {
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_surveyData.getId().intValue())))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].surveyType").value(hasItem(DEFAULT_SURVEY_TYPE.name())))
            .andExpect(jsonPath("$.[*].assesmentData").value(hasItem(DEFAULT_ASSESMENT_DATA)))
            .andExpect(jsonPath("$.[*].scoringResult").value(hasItem(DEFAULT_SCORING_RESULT.intValue())))
            .andExpect(jsonPath("$.[*].createdTime").value(hasItem(DEFAULT_CREATED_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].additionalInfo").value(hasItem(DEFAULT_ADDITIONAL_INFO)))
            .andExpect(jsonPath("$.[*].deviceId").value(hasItem(DEFAULT_DEVICE_ID.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSurveyDataShouldNotBeFound(String filter) throws Exception {
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

    }

    @Test
    @Transactional
    public void getSurveyDataById_AsPacient() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsPacient_NoDevice() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsPacient_BadDeviceId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setDeviceId(BAD_DEVICE_ID);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsPacient_BadId() throws Exception {
        userDTO.setAuthorities(PACIENT_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(-1L);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setDeviceId(BAD_DEVICE_ID);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(-1L);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsFamily() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsFamily_NoDevice() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsFamily_BadDeviceId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setDeviceId(BAD_DEVICE_ID);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsFamily_BadId() throws Exception {
        userDTO.setAuthorities(FAMILY_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(FAMILY_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(-1L);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setDeviceId(BAD_DEVICE_ID);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getSurveyDataById_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(PERSONAL_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(-1L);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsOrganization() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsOrganization_NoDevice() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
//        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_JSON_UTF8);
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsOrganization_BadDeviceId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setDeviceId(BAD_DEVICE_ID);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void getSurveyDataById_OfPacient_AsOrganization_BadId() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyData.setId(-1L);
        surveyDataRepository.saveAndFlush(surveyData);

        getSurveyDataByIdRestCall_IsBadRequest(MediaType.APPLICATION_PROBLEM_JSON);
    }

    private void getSurveyDataByIdRestCall_IsOk() throws Exception {
        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(surveyData.getId().intValue()))
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER))
            .andExpect(jsonPath("$.surveyType").value(DEFAULT_SURVEY_TYPE.name()))
            .andExpect(jsonPath("$.assesmentData").value(DEFAULT_ASSESMENT_DATA))
            .andExpect(jsonPath("$.scoringResult").value(DEFAULT_SCORING_RESULT.intValue()))
            .andExpect(jsonPath("$.createdTime").value(DEFAULT_CREATED_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.additionalInfo").value(DEFAULT_ADDITIONAL_INFO))
            .andExpect(jsonPath("$.deviceId").value(DEFAULT_DEVICE_ID.intValue()));
    }

    private void getSurveyDataByIdRestCall_IsUnauthorized() throws Exception {
        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getSurveyDataByIdRestCall_IsBadRequest(MediaType returnMediaType) throws Exception {
        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(returnMediaType));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SurveyData.class);
        SurveyData surveyData1 = new SurveyData();
        surveyData1.setId(1L);
        SurveyData surveyData2 = new SurveyData();
        surveyData2.setId(surveyData1.getId());
        assertThat(surveyData1).isEqualTo(surveyData2);
        surveyData2.setId(2L);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
        surveyData1.setId(null);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
    }

    @Test
    @Transactional
    public void deleteSurveyDataById_AsAdmin() throws Exception {
        userDTO.setAuthorities(ORGANIZATION_USER_DTO_AUTHORITIES);
        setupDeviceForUserDto(ORGANIZATION_DEVICES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        surveyDataRepository.saveAndFlush(surveyData);

        int numberOfEntitiesBeforeTest = surveyDataRepository.findAll().size();
        // Get the surveyData
        restSurveyDataMockMvc.perform(delete("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isOk());

        List<SurveyData> listInDb = surveyDataRepository.findAll();
        assertThat(listInDb.size()).isEqualTo(numberOfEntitiesBeforeTest-1);
    }
}
