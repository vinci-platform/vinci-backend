package com.vinci.io.server.web.rest;

import com.vinci.io.server.domain.enumeration.DeviceType;
import com.vinci.io.server.security.AuthoritiesConstants;
import com.vinci.io.server.service.dto.DeviceDTO;
import com.vinci.io.server.web.rest.beans.UserDTO;
import com.vinci.io.server.web.rest.util.UserDevicesEnum;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.*;

import static com.vinci.io.server.web.rest.util.UserDevicesEnum.*;

public abstract class SetupController {

    static protected final Long DEFAULT_USER_DTO_ID = 1L;
    static protected final Long DEFAULT_USER_DTO_USER_EXTRA_ID = 1L;
    static protected final String DEFAULT_USER_DTO_LOGIN = "user";
    static protected final String DEFAULT_USER_DTO_FIRSTNAME = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_GENDER = "MALE";
    static protected final String DEFAULT_USER_DTO_LASTNAME = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_ADDRESS = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_PHONE = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_EMAIL = "AAAAAAAAAAAAAAAA";
    static protected final boolean DEFAULT_USER_DTO_ACTIVATED = true;
    static protected final String DEFAULT_USER_DTO_LANG_KEY = "en";
    static protected final String DEFAULT_USER_DTO_CREATED_BY = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_LAST_MODIFIED_BY = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_ROLE = "ROLE_USER,ROLE_PATIENT";
    static protected final Set<String> DEFAULT_USER_DTO_AUTHORITIES = new HashSet<String>(){{
        add("ROLE_USER");
        add("ROLE_PACIENT");
    }};
    static protected final String DEFAULT_USER_DTO_UUID = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_MARITAL_STATUS = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_EDUCATION = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_FRIENDS = "AAAAAAAAAAAAAAAA";

    static protected final Set<String> PACIENT_USER_DTO_AUTHORITIES = new HashSet<String>(){{
        add("ROLE_USER");
        add("ROLE_PACIENT");
    }};
    static protected final Set<String> FAMILY_USER_DTO_AUTHORITIES = new HashSet<String>(){{
        add("ROLE_USER");
        add("ROLE_FAMILY");
    }};
    static protected final Set<String> ORGANIZATION_USER_DTO_AUTHORITIES = new HashSet<String>(){{
        add("ROLE_USER");
        add("ROLE_ORGANIZATION");
    }};

    protected static final Long DEFAULT_DEVICE_ID = 1L;
    protected static final Long UPDATED_DEVICE_ID = 2L;
    protected static final Long BAD_DEVICE_ID = -1L;
    protected static final String DEFAULT_DEVICE_NAME = "AAAAAAAAAAAAAAAA";
    protected static final String DEFAULT_DEVICE_DESCRIPTION = "AAAAAAAAAAAAAAAA";
    protected static final String DEFAULT_DEVICE_UUID = "AAAAAAAAAAAAAAAA";
//    protected static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.SURVEY;
    protected static final Boolean DEFAULT_DEVICE_ACTIVE = true;
    protected static final Long DEFAULT_USER_EXTRA_ID = 1L;

    @Autowired
    protected EntityManager em;


    protected UserDTO userDTO;

    protected DeviceDTO deviceDTO;

    /**
     * Sets up default values to userDTO. All device sets are empty hash sets.
     */
    protected void setupUserDto() {
        userDTO = new UserDTO();
        userDTO.setId(DEFAULT_USER_DTO_ID);
        userDTO.setUserExtraId(DEFAULT_USER_DTO_USER_EXTRA_ID);
        userDTO.setLogin(DEFAULT_USER_DTO_LOGIN);
        userDTO.setFirstName(DEFAULT_USER_DTO_FIRSTNAME);
        userDTO.setGender(DEFAULT_USER_DTO_GENDER);
        userDTO.setLastName(DEFAULT_USER_DTO_LASTNAME);
        userDTO.setAddress(DEFAULT_USER_DTO_ADDRESS);
        userDTO.setPhone(DEFAULT_USER_DTO_PHONE);
        userDTO.setEmail(DEFAULT_USER_DTO_EMAIL);
        userDTO.setActivated(DEFAULT_USER_DTO_ACTIVATED);
        userDTO.setLangKey(DEFAULT_USER_DTO_LANG_KEY);
        userDTO.setCreatedBy(DEFAULT_USER_DTO_CREATED_BY);
        userDTO.setLastModifiedBy(DEFAULT_USER_DTO_LAST_MODIFIED_BY);
        userDTO.setRole(DEFAULT_USER_DTO_ROLE);
        userDTO.setUuid(DEFAULT_USER_DTO_UUID);
        userDTO.setMaritalStatus(DEFAULT_USER_DTO_MARITAL_STATUS);
        userDTO.setEducation(DEFAULT_USER_DTO_EDUCATION);
        userDTO.setFriends(DEFAULT_USER_DTO_FRIENDS);

        userDTO.setAuthorities(DEFAULT_USER_DTO_AUTHORITIES);
        userDTO.setPersonalDevices(new HashSet<>());
        userDTO.setPatientsOfFamily(new HashSet<>());
        userDTO.setPatientsOfOrganization(new HashSet<>());
    }

    protected void setupUserDto(Set<DeviceDTO> personalDevices,Set<DeviceDTO> patientOfFamily,Set<DeviceDTO> patientOfOrganization) {
        setupUserDto();

        userDTO.setPersonalDevices((personalDevices != null) ? personalDevices : new HashSet<>());
        userDTO.setPatientsOfFamily((personalDevices != null) ? patientOfFamily : new HashSet<>());
        userDTO.setPatientsOfOrganization((personalDevices != null) ? patientOfOrganization : new HashSet<>());
    }

    protected Optional<UserDTO> getUserDTOOptional() {
        return Optional.of(userDTO);
    }

    protected void setupDeviceDto(DeviceType deviceType) {
        deviceDTO = new DeviceDTO();
        deviceDTO.setId(DEFAULT_DEVICE_ID);
        deviceDTO.setName(DEFAULT_DEVICE_NAME);
        deviceDTO.setDescription(DEFAULT_DEVICE_DESCRIPTION);
        deviceDTO.setUuid(DEFAULT_DEVICE_UUID);
        deviceDTO.setDeviceType(deviceType);
        deviceDTO.setActive(DEFAULT_DEVICE_ACTIVE);
        deviceDTO.setUserExtraId(DEFAULT_USER_EXTRA_ID);
    }

    protected void setupDeviceForUserDto(UserDevicesEnum userDevicesEnum){
        Set<DeviceDTO> set = new HashSet<>();
        set.add(deviceDTO);
        switch (userDevicesEnum) {
            case PERSONAL_DEVICES: {
                userDTO.setPersonalDevices(set);
                break;
            }
            case FAMILY_DEVICES: {
                userDTO.setPatientsOfFamily(set);
                break;
            }
            case ORGANIZATION_DEVICES: {
                userDTO.setPatientsOfOrganization(set);
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + userDevicesEnum);
        }
    }

    protected List<DeviceDTO> getDeviceDtoList() {
        return new ArrayList<DeviceDTO>(){{add(deviceDTO);}};
    }
}
