package com.vinci.io.server.domain;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.vinci.io.server.web.rest.TestUtil;

public class FitbitWatchIntradayDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchIntradayData.class);
        FitbitWatchIntradayData fitbitWatchIntradayData1 = new FitbitWatchIntradayData();
        fitbitWatchIntradayData1.setId(1L);
        FitbitWatchIntradayData fitbitWatchIntradayData2 = new FitbitWatchIntradayData();
        fitbitWatchIntradayData2.setId(fitbitWatchIntradayData1.getId());
        assertThat(fitbitWatchIntradayData1).isEqualTo(fitbitWatchIntradayData2);
        fitbitWatchIntradayData2.setId(2L);
        assertThat(fitbitWatchIntradayData1).isNotEqualTo(fitbitWatchIntradayData2);
        fitbitWatchIntradayData1.setId(null);
        assertThat(fitbitWatchIntradayData1).isNotEqualTo(fitbitWatchIntradayData2);
    }
}
