package com.vinci.io.server.service.dto;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.vinci.io.server.web.rest.TestUtil;

public class FitbitWatchIntradayDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FitbitWatchIntradayDataDTO.class);
        FitbitWatchIntradayDataDTO fitbitWatchIntradayDataDTO1 = new FitbitWatchIntradayDataDTO();
        fitbitWatchIntradayDataDTO1.setId(1L);
        FitbitWatchIntradayDataDTO fitbitWatchIntradayDataDTO2 = new FitbitWatchIntradayDataDTO();
        assertThat(fitbitWatchIntradayDataDTO1).isNotEqualTo(fitbitWatchIntradayDataDTO2);
        fitbitWatchIntradayDataDTO2.setId(fitbitWatchIntradayDataDTO1.getId());
        assertThat(fitbitWatchIntradayDataDTO1).isEqualTo(fitbitWatchIntradayDataDTO2);
        fitbitWatchIntradayDataDTO2.setId(2L);
        assertThat(fitbitWatchIntradayDataDTO1).isNotEqualTo(fitbitWatchIntradayDataDTO2);
        fitbitWatchIntradayDataDTO1.setId(null);
        assertThat(fitbitWatchIntradayDataDTO1).isNotEqualTo(fitbitWatchIntradayDataDTO2);
    }
}
