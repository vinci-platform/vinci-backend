# ioserver

This application was generated using JHipster 5.8.2, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.8.2](https://www.jhipster.tech/documentation-archive/v5.8.2).

All of the endpoints are available in `gateway -> administration -> api -> ioserver in dropdown`. You can try them 
out directly in swagger.

You can then verify that the data is available in `entities -> select your entity from dropdown`.
