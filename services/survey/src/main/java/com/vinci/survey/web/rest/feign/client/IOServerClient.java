package com.vinci.survey.web.rest.feign.client;

import com.vinci.survey.web.rest.beans.SurveyData;
import com.vinci.survey.web.rest.feign.handler.ClientExceptionHandler;
import com.vinci.survey.web.rest.feign.handler.HandleFeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ioserver")
public interface IOServerClient {

    @HandleFeignException(ClientExceptionHandler.class)
    @PostMapping(value = "/api/survey-data/import")
    String saveSurveyData(@RequestBody SurveyData surveyData);

    @HandleFeignException(ClientExceptionHandler.class)
    @PostMapping(value = "/api/v1.0.0/survey-data/import")
    String saveSurveyDataV_1_0_0(@RequestBody SurveyData surveyData);
}

