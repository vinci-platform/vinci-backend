package com.vinci.survey.domain.enumeration;

/**
 * The EducationType enumeration.
 */
public enum EducationType {
    NONE, PRIMARY_SCHOOL, SECONDARY_SCHOOL, TERTIARY
}
