package com.vinci.survey.web.rest.beans;


import com.vinci.survey.domain.enumeration.SurveyType;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A SurveyData.
 */

public class SurveyData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String identifier;

    private SurveyType surveyType;

    private String assesmentData;

    private Long scoringResult;

    private Instant createdTime;

    private Instant endTime;

    private String additionalInfo;

    private Long deviceId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SurveyData id(Long id){
        this.id = id;
        return this;
    }

    public String getIdentifier() {
        return identifier;
    }

    public SurveyData identifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public SurveyType getSurveyType() {
        return surveyType;
    }

    public SurveyData surveyType(SurveyType surveyType) {
        this.surveyType = surveyType;
        return this;
    }

    public void setSurveyType(SurveyType surveyType) {
        this.surveyType = surveyType;
    }

    public String getAssesmentData() {
        return assesmentData;
    }

    public SurveyData assesmentData(String assesmentData) {
        this.assesmentData = assesmentData;
        return this;
    }

    public void setAssesmentData(String assesmentData) {
        this.assesmentData = assesmentData;
    }

    public Long getScoringResult() {
        return scoringResult;
    }

    public SurveyData scoringResult(Long scoringResult) {
        this.scoringResult = scoringResult;
        return this;
    }

    public void setScoringResult(Long scoringResult) {
        this.scoringResult = scoringResult;
    }

    public Instant getCreatedTime() {
        return createdTime;
    }

    public SurveyData createdTime(Instant createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public void setCreatedTime(Instant createdTime) {
        this.createdTime = createdTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public SurveyData endTime(Instant endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public SurveyData additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public SurveyData deviceId(Long deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SurveyData surveyData = (SurveyData) o;
        if (surveyData.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyData.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyData{" +
            "id=" + getId() +
            ", identifier='" + getIdentifier() + "'" +
            ", surveyType='" + getSurveyType() + "'" +
            ", assesmentData='" + getAssesmentData() + "'" +
            ", scoringResult=" + getScoringResult() +
            ", createdTime='" + getCreatedTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", additionalInfo='" + getAdditionalInfo() + "'" +
            ", deviceId=" + getDeviceId() +
            "}";
    }
}
