package com.vinci.survey.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.vinci.survey.domain.enumeration.Gender;

import com.vinci.survey.domain.enumeration.EducationType;

/**
 * A UserExtra.
 */
@Entity
@Table(name = "user_extra")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserExtra implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "phone")
    private String phone;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "address")
    private String address;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "education")
    private EducationType education;

    @Column(name = "marital_status")
    private String maritalStatus;

    @NotNull
    @Column(name = "user_identifier", nullable = false)
    private String userIdentifier;

    @NotNull
    @Column(name = "organization_identifier", nullable = false)
    private String organizationIdentifier;

    @NotNull
    @Column(name = "family_identifier", nullable = false)
    private String familyIdentifier;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public UserExtra phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public UserExtra name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public UserExtra surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public UserExtra address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public UserExtra birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    public UserExtra gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public EducationType getEducation() {
        return education;
    }

    public UserExtra education(EducationType education) {
        this.education = education;
        return this;
    }

    public void setEducation(EducationType education) {
        this.education = education;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public UserExtra maritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public UserExtra userIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
        return this;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public String getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    public UserExtra organizationIdentifier(String organizationIdentifier) {
        this.organizationIdentifier = organizationIdentifier;
        return this;
    }

    public void setOrganizationIdentifier(String organizationIdentifier) {
        this.organizationIdentifier = organizationIdentifier;
    }

    public String getFamilyIdentifier() {
        return familyIdentifier;
    }

    public UserExtra familyIdentifier(String familyIdentifier) {
        this.familyIdentifier = familyIdentifier;
        return this;
    }

    public void setFamilyIdentifier(String familyIdentifier) {
        this.familyIdentifier = familyIdentifier;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserExtra userExtra = (UserExtra) o;
        if (userExtra.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtra.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtra{" +
            "id=" + getId() +
            ", phone='" + getPhone() + "'" +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", address='" + getAddress() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", gender='" + getGender() + "'" +
            ", education='" + getEducation() + "'" +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", userIdentifier='" + getUserIdentifier() + "'" +
            ", organizationIdentifier='" + getOrganizationIdentifier() + "'" +
            ", familyIdentifier='" + getFamilyIdentifier() + "'" +
            "}";
    }
}
