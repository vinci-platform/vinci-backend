package com.vinci.survey.web.rest;

import com.vinci.survey.service.SurveyDataQueryService;
import com.vinci.survey.service.SurveyDataService;
import com.vinci.survey.service.UserExtraService;
import com.vinci.survey.service.dto.SurveyDataCriteria;
import com.vinci.survey.service.dto.SurveyDataDTO;
import com.vinci.survey.service.dto.UserExtraDTO;
import com.vinci.survey.web.rest.beans.SurveyData;
import com.vinci.survey.web.rest.errors.BadRequestAlertException;
import com.vinci.survey.web.rest.feign.client.GatewayClient;
import com.vinci.survey.web.rest.feign.client.IOServerClient;
import com.vinci.survey.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.vinci.survey.domain.SurveyData}.
 */
@RestController
@RequestMapping("/api")
public class SurveyDataResource {

    private final Logger log = LoggerFactory.getLogger(SurveyDataResource.class);

    private static final String ENTITY_NAME = "surveySurveyData";

    private final SurveyDataService surveyDataService;

    private final UserExtraService userExtraService;

    private final GatewayClient gatewayClient;

    private final IOServerClient ioServerClient;

    private final SurveyDataQueryService surveyDataQueryService;

    public SurveyDataResource(SurveyDataService surveyDataService, SurveyDataQueryService surveyDataQueryService, UserExtraService userExtraService, GatewayClient gatewayClient, IOServerClient ioServerClient) {
        this.surveyDataService = surveyDataService;
        this.userExtraService = userExtraService;
        this.gatewayClient = gatewayClient;
        this.ioServerClient = ioServerClient;
        this.surveyDataQueryService = surveyDataQueryService;
    }


    /**
     * {@code POST  /survey-data} : Create a new surveyData.
     *
     * @param surveyDataDTO the surveyDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new surveyDataDTO, or with status {@code 400 (Bad Request)} if the surveyData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/survey-data")
    @Deprecated
    public ResponseEntity<SurveyDataDTO> createSurveyData(@Valid @RequestBody SurveyDataDTO surveyDataDTO,
                                                          @RequestParam String userIdentifier) throws Exception {
        log.debug("REST request to save SurveyData : {}", surveyDataDTO);
        if (surveyDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new surveyData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (userIdentifier == null) {
            throw new BadRequestAlertException("Parameter needs to exist", ENTITY_NAME, "userIdentifierDoesntExits");
        }
        Long userId = 0L;
        try{
            userId = Long.valueOf(userIdentifier);
        }catch (NumberFormatException ex){
            log.error("Parameter userIdentifier needs to be integer or long value in string");
            throw new BadRequestAlertException("Parameter userIdentifier needs to be integer or long value in string", ENTITY_NAME, "userIdentifierNotInt");
        }

        Long deviceId = gatewayClient.getSurveyDevice(userId);
        if (deviceId == 0l){ // GatewayClientFallback value if there is error on gateway service, deviceId should always exist for user
            throw new Exception("Error on gateway client");
        }

        SurveyData surveyData = new SurveyData();
        surveyData.setIdentifier(surveyDataDTO.getIdentifier());
        surveyData.setSurveyType(surveyDataDTO.getSurveyType());
        surveyData.setAssesmentData(surveyDataDTO.getAssesmentData());
        surveyData.setScoringResult(surveyDataDTO.getScoringResult());
        surveyData.setCreatedTime(surveyDataDTO.getCreatedTime());
        surveyData.setEndTime(surveyDataDTO.getEndTime());
        surveyData.setAdditionalInfo(surveyDataDTO.getAdditionalInfo());
        surveyData.setDeviceId(deviceId);

        String response=ioServerClient.saveSurveyData(surveyData);

	    SurveyDataDTO result = surveyDataService.save(surveyDataDTO);
        return ResponseEntity.created(new URI("/api/survey-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /survey-data} : Create a new surveyData.
     *
     * @param surveyDataDTO the surveyDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new surveyDataDTO, or with status {@code 400 (Bad Request)} if the surveyData has already an ID.
     */
    @PostMapping("/v1.0.0/survey-data")
    public ResponseEntity<String> createSurveyDataV_1_0_0(@Valid @RequestBody SurveyDataDTO surveyDataDTO,
                                                          @RequestParam String userIdentifier) {
        log.debug("REST request to save SurveyData : {}", surveyDataDTO);
        if (surveyDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new surveyData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (userIdentifier == null) {
            throw new BadRequestAlertException("Parameter needs to exist", ENTITY_NAME, "userIdentifierDoesntExits");
        }
        Long userId;
        try{
            userId = Long.valueOf(userIdentifier);
        }catch (NumberFormatException ex){
            log.debug("Parameter userIdentifier needs to be integer or long value in string");
            throw new BadRequestAlertException("Parameter userIdentifier needs to be integer or long value in string", ENTITY_NAME, "userIdentifierNotInt");
        }
        String surveyIdentifier = surveyDataService.saveV_1_0_0(surveyDataDTO,userId);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert("ioserverSurveyData", surveyIdentifier))
            .body(surveyIdentifier);
    }

    /**
     * {@code PUT  /survey-data} : Updates an existing surveyData.
     *
     * @param surveyDataDTO the surveyDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated surveyDataDTO,
     */
    @PutMapping("/survey-data")
    @Deprecated
    public ResponseEntity<SurveyDataDTO> updateSurveyData(@Valid @RequestBody SurveyDataDTO surveyDataDTO) {
        log.debug("REST request to update SurveyData : {}", surveyDataDTO);
        if (surveyDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SurveyDataDTO result = surveyDataService.save(surveyDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, surveyDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /survey-data} : get all the surveyData.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of surveyData in body.
     */
    /**
     * GET  /survey-data : get all the surveyData.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @GetMapping("/survey-data")
    @Deprecated
    public ResponseEntity<List<SurveyDataDTO>> getAllSurveyData(SurveyDataCriteria criteria) {
        log.debug("REST request to get SurveyData by criteria: {}", criteria);
        List<SurveyDataDTO> entityList = surveyDataQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /survey-data/count : count all the surveyData.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count in body
     */
    @GetMapping("/survey-data/count")
    @Deprecated
    public ResponseEntity<Long> countSurveyData(SurveyDataCriteria criteria) {
        log.debug("REST request to count SurveyData by criteria: {}", criteria);
        return ResponseEntity.ok().body(surveyDataQueryService.countByCriteria(criteria));
    }


    /**
     * {@code GET  /survey-data/:id} : get the "id" surveyData.
     *
     * @param id the id of the surveyDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the surveyDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/survey-data/{id}")
    @Deprecated
    public ResponseEntity<SurveyDataDTO> getSurveyData(@PathVariable Long id) {
        log.debug("REST request to get SurveyData : {}", id);
        Optional<SurveyDataDTO> surveyDataDTO = surveyDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(surveyDataDTO);
    }

    /**
     * {@code DELETE  /survey-data/:id} : delete the "id" surveyData.
     *
     * @param id the id of the surveyDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/survey-data/{id}")
    @Deprecated
    public ResponseEntity<Void> deleteSurveyData(@PathVariable Long id) {
        log.debug("REST request to delete SurveyData : {}", id);
        surveyDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert( ENTITY_NAME, id.toString())).build();
    }


    /**
     * GET  /survey-data : get all the surveyData for userId.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @GetMapping("/survey-data/user/{userId}")
    @Deprecated
    public List<SurveyDataDTO> getAllSurveyDataForUser(@PathVariable Long userId) {
        log.debug("REST request to get all SurveyData for UserExtra with id: {}", userId);
	Optional<UserExtraDTO> userExtra = userExtraService.findByUserIdentifier(userId.toString());
        Long userExtraId;
	if (userExtra.isPresent()){
	    userExtraId=userExtra.get().getId();
        }
	else {
	    throw new BadRequestAlertException("Survey not found for the user", ENTITY_NAME, "idnull");
	}

    return surveyDataService.findAllByUserExtra(userExtraId);
    }


    /**
     * GET  /survey-data : get all the surveyData for userExtraId.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @Deprecated
    @GetMapping("/survey-data/userExtra/{userExtraId}")
    public List<SurveyDataDTO> getAllSurveyData(@PathVariable Long userExtraId) {
        log.debug("REST request to get all SurveyData for UserExtra with id: {}", userExtraId);
        return surveyDataService.findAllByUserExtra(userExtraId);
    }

    /**
     * GET  /survey-data : get all the surveyData for surveyId.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @Deprecated
    @GetMapping("/survey-data/userExtra/surveyId/{surveyId}")
    public List<SurveyDataDTO> getAllSurveyDataFor(@PathVariable Long surveyId) {
        log.debug("REST request to get all SurveyData for surveyId: {}", surveyId);
        return surveyDataService.findAllBySurveyId(surveyId);
    }

    /**
     * GET  /survey-data : get all the surveyData for medicalId.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of surveyData in body
     */
    @Deprecated
    @GetMapping("/survey-data/userExtra/medicalId/{medicalId}")
    public List<SurveyDataDTO> getAllSurveyDataOn(@PathVariable Long medicalId) {
        log.debug("REST request to get all SurveyData for medicalId: {}", medicalId);
        return surveyDataService.findAllByMedicalId(medicalId);
    }
}
