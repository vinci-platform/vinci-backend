package com.vinci.survey.web.rest.feign.config;

import com.vinci.survey.web.rest.feign.handler.FeignHttpExceptionHandler;
import com.vinci.survey.web.rest.feign.handler.HandleFeignException;
import feign.Feign;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class HandlingFeignErrorDecoder implements ErrorDecoder {

    private final Map<String, FeignHttpExceptionHandler> exceptionHandlerMap = new HashMap<>();
    private final Default defaultErrorDecoder = new Default();
    private final ApplicationContext applicationContext;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Map<String, Object> feignClients = applicationContext.getBeansWithAnnotation(FeignClient.class);
        List<Method> clientMethods = feignClients.values().stream()
            .map(Object::getClass)
            .map(aClass -> aClass.getInterfaces()[0])
            .map(ReflectionUtils::getAllDeclaredMethods)
            .flatMap(Arrays::stream)
            .collect(Collectors.toList());
        for (Method m : clientMethods) {
            String configKey = Feign.configKey(m.getDeclaringClass(), m);
            log.info("HandlingFeignErrorDecoder#onApplicationEvent configKey: {}", configKey);
            HandleFeignException annotation = getHandleFeignExceptionAnnotation(m);
            if (annotation != null) {
                FeignHttpExceptionHandler handlerBean = applicationContext.getBean(annotation.value());
                exceptionHandlerMap.put(configKey, handlerBean);
            }
        }
    }

    private HandleFeignException getHandleFeignExceptionAnnotation(Method m) {
        HandleFeignException resault = m.getAnnotation(HandleFeignException.class);
        if (resault == null) {
            resault = m.getDeclaringClass().getAnnotation(HandleFeignException.class);
        }
        return resault;
    }

    @Override
    public Exception decode(String methodKey, Response response) {
        FeignHttpExceptionHandler handler = exceptionHandlerMap.get(methodKey);
        if (handler != null) {
            Exception exception = handler.handle(response);
            if (exception == null) {
                exception = defaultErrorDecoder.decode(methodKey, response);
            }
            return exception;
        }
        return defaultErrorDecoder.decode(methodKey, response);
    }
}
