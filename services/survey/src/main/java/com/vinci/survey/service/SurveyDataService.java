package com.vinci.survey.service;

import com.vinci.survey.service.dto.SurveyDataDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SurveyData.
 */
public interface SurveyDataService {

    /**
     * Save a surveyData.
     *
     * @param surveyDataDTO the entity to save
     * @return the persisted entity
     */
    SurveyDataDTO save(SurveyDataDTO surveyDataDTO);
    String saveV_1_0_0(SurveyDataDTO surveyDataDTO,Long userId);

    /**
     * Get all the surveyData.
     *
     * @return the list of entities
     */
    List<SurveyDataDTO> findAll();

    /**
     * Get all the surveyData for userExtra
     *
     * @return the list of entities
     */
    List<SurveyDataDTO> findAllByUserExtra(Long userExtraId);


    /**
     * Get the "id" surveyData.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SurveyDataDTO> findOne(Long id);

    /**
     * Delete the "id" surveyData.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Get all the surveyData for surveyId
     *
     * @return the list of entities
     */
    List<SurveyDataDTO> findAllBySurveyId(Long surveyId);

    /**
     * Get all the surveyData for medicalId
     *
     * @return the list of entities
     */
    List<SurveyDataDTO> findAllByMedicalId(Long medicalId);
}
