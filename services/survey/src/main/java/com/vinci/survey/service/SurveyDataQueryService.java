package com.vinci.survey.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.vinci.survey.domain.SurveyData;
import com.vinci.survey.domain.*; // for static metamodels
import com.vinci.survey.repository.SurveyDataRepository;
import com.vinci.survey.service.dto.SurveyDataCriteria;
import com.vinci.survey.service.dto.SurveyDataDTO;
import com.vinci.survey.service.mapper.SurveyDataMapper;

/**
 * Service for executing complex queries for SurveyData entities in the database.
 * The main input is a {@link SurveyDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SurveyDataDTO} or a {@link Page} of {@link SurveyDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SurveyDataQueryService extends QueryService<SurveyData> {

    private final Logger log = LoggerFactory.getLogger(SurveyDataQueryService.class);

    private final SurveyDataRepository surveyDataRepository;

    private final SurveyDataMapper surveyDataMapper;

    public SurveyDataQueryService(SurveyDataRepository surveyDataRepository, SurveyDataMapper surveyDataMapper) {
        this.surveyDataRepository = surveyDataRepository;
        this.surveyDataMapper = surveyDataMapper;
    }

    /**
     * Return a {@link List} of {@link SurveyDataDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SurveyDataDTO> findByCriteria(SurveyDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SurveyData> specification = createSpecification(criteria);
        return surveyDataMapper.toDto(surveyDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SurveyDataDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SurveyDataDTO> findByCriteria(SurveyDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SurveyData> specification = createSpecification(criteria);
        return surveyDataRepository.findAll(specification, page)
            .map(surveyDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SurveyDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SurveyData> specification = createSpecification(criteria);
        return surveyDataRepository.count(specification);
    }

    /**
     * Function to convert SurveyDataCriteria to a {@link Specification}
     */
    private Specification<SurveyData> createSpecification(SurveyDataCriteria criteria) {
        Specification<SurveyData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SurveyData_.id));
            }
            if (criteria.getIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIdentifier(), SurveyData_.identifier));
            }
            if (criteria.getSurveyType() != null) {
                specification = specification.and(buildSpecification(criteria.getSurveyType(), SurveyData_.surveyType));
            }
            if (criteria.getScoringResult() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScoringResult(), SurveyData_.scoringResult));
            }
            if (criteria.getCreatedTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedTime(), SurveyData_.createdTime));
            }
            if (criteria.getEndTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndTime(), SurveyData_.endTime));
            }
            if (criteria.getSurveyId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSurveyId(), SurveyData_.surveyId));
            }
            if (criteria.getMedicalId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMedicalId(), SurveyData_.medicalId));
            }
            if (criteria.getUserExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserExtraId(),
                    root -> root.join(SurveyData_.userExtra, JoinType.LEFT).get(UserExtra_.id)));
            }
        }
        return specification;
    }
}
