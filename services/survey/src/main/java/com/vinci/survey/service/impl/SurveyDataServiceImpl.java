package com.vinci.survey.service.impl;

import com.vinci.survey.domain.SurveyData;
import com.vinci.survey.repository.SurveyDataRepository;
import com.vinci.survey.service.SurveyDataService;
import com.vinci.survey.service.dto.SurveyDataDTO;
import com.vinci.survey.service.mapper.SurveyDataMapper;
import com.vinci.survey.web.rest.feign.client.GatewayClient;
import com.vinci.survey.web.rest.feign.client.IOServerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SurveyData.
 */
@Service
@Transactional
public class SurveyDataServiceImpl implements SurveyDataService {

    private static final String ENTITY_NAME = "surveySurveyData";

    private final Logger log = LoggerFactory.getLogger(SurveyDataServiceImpl.class);

    private final SurveyDataRepository surveyDataRepository;

    private final SurveyDataMapper surveyDataMapper;

    private final GatewayClient gatewayClient;

    private final IOServerClient ioServerClient;

    private final ObjectMapper objectMapper;

    public SurveyDataServiceImpl(SurveyDataRepository surveyDataRepository, SurveyDataMapper surveyDataMapper, GatewayClient gatewayClient,
                                 IOServerClient ioServerClient) {
        this.surveyDataRepository = surveyDataRepository;
        this.surveyDataMapper = surveyDataMapper;
        this.gatewayClient = gatewayClient;
        this.ioServerClient = ioServerClient;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Save a surveyData.
     *
     * @param surveyDataDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SurveyDataDTO save(SurveyDataDTO surveyDataDTO) {
        log.debug("Request to save SurveyData : {}", surveyDataDTO);
        SurveyData surveyData = surveyDataMapper.toEntity(surveyDataDTO);
        surveyData = surveyDataRepository.save(surveyData);
        return surveyDataMapper.toDto(surveyData);
    }

    /**
     * Save a surveyData.
     *
     * @param surveyDataDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public String saveV_1_0_0(SurveyDataDTO surveyDataDTO,Long userId) {
        log.debug("Request to save SurveyData : {}", surveyDataDTO);

        //todo: better is to make new api call for CURRENT user and not to use this api
        Long surveyDeviceId = gatewayClient.getSurveyDeviceV_1_0_0(userId);//gateway client will return surveyDevice for user with proper authorization from token

        com.vinci.survey.web.rest.beans.SurveyData surveyData = new com.vinci.survey.web.rest.beans.SurveyData();
        surveyData.setIdentifier(surveyDataDTO.getIdentifier());
        surveyData.setSurveyType(surveyDataDTO.getSurveyType());
        surveyData.setAssesmentData(surveyDataDTO.getAssesmentData());
        surveyData.setScoringResult(surveyDataDTO.getScoringResult());
        surveyData.setCreatedTime(surveyDataDTO.getCreatedTime());
        surveyData.setEndTime(surveyDataDTO.getEndTime());
        surveyData.setAdditionalInfo(surveyDataDTO.getAdditionalInfo());
        surveyData.setDeviceId(surveyDeviceId);

        return ioServerClient.saveSurveyDataV_1_0_0(surveyData);
    }

    /**
     * Get all the surveyData.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SurveyDataDTO> findAll() {
        log.debug("Request to get all SurveyData");
        return surveyDataRepository.findAll().stream()
            .map(surveyDataMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the surveyData for userExtra
     *
     * @return the list of entities
     */
    @Override
    public List<SurveyDataDTO> findAllByUserExtra(Long userExtraId) {
        log.debug("Request to get all SurveyData for UserExtra: {}", userExtraId);
        return surveyDataRepository.findAllByUserExtraId(userExtraId).stream()
            .map(surveyDataMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the surveyData for surveyId
     *
     * @return the list of entities
     */
    @Override
    public List<SurveyDataDTO> findAllBySurveyId(Long surveyId) {
        log.debug("Request to get all SurveyData for surveyId: {}", surveyId);
        return surveyDataRepository.findAllBySurveyId(surveyId).stream()
            .map(surveyDataMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the surveyData for medicalId
     *
     * @return the list of entities
     */
    @Override
    public List<SurveyDataDTO> findAllByMedicalId(Long medicalId) {
        log.debug("Request to get all SurveyData for surveyId: {}", medicalId);
        return surveyDataRepository.findAllByMedicalId(medicalId).stream()
            .map(surveyDataMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one surveyData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SurveyDataDTO> findOne(Long id) {
        log.debug("Request to get SurveyData : {}", id);
        return surveyDataRepository.findById(id)
            .map(surveyDataMapper::toDto);
    }

    /**
     * Delete the surveyData by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SurveyData : {}", id);
        surveyDataRepository.deleteById(id);
    }
}
