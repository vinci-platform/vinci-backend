package com.vinci.survey.error;

import com.vinci.survey.error.response.ErrorResponse;
import com.vinci.survey.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class AuthorizationException extends AbstractThrowableProblem {

    private static final String TITLE = "AUTHORIZATION_EXCEPTION";
    private static final Status STATUS_TYPE = Status.UNAUTHORIZED;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "There has been an authorization exception when performing an action!";

    public AuthorizationException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public AuthorizationException(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, details); }
    public AuthorizationException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "AuthorizationException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
