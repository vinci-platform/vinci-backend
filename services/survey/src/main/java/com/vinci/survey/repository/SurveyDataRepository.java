package com.vinci.survey.repository;

import com.vinci.survey.domain.SurveyData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the SurveyData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SurveyDataRepository extends JpaRepository<SurveyData, Long>,JpaSpecificationExecutor<SurveyData> {

    List<SurveyData> findAllByUserExtraId(Long userExtraId);

    List<SurveyData> findAllBySurveyId(Long surveyId);

    List<SurveyData> findAllByMedicalId(Long medicalId);

}
