package com.vinci.survey.web.rest.feign.client;

import com.vinci.survey.web.rest.feign.handler.ClientExceptionHandler;
import com.vinci.survey.web.rest.feign.handler.HandleFeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "gateway")
public interface GatewayClient {

    Long MAX_SIZE_VALUE = 0x7fff_ffff_ffff_ffffL;

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/validate")
    Long validate(@RequestParam(name = "userId") Long userId);


    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/devices/getSurveyDevice/userId")
    Long getSurveyDevice(@RequestParam(name = "userId") Long userId);

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/v1.0.0/devices/getSurveyDevice/userId")
    Long getSurveyDeviceV_1_0_0(@RequestParam(name = "userId") Long userId);

}
