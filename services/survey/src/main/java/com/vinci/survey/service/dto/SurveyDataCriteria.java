package com.vinci.survey.service.dto;

import java.io.Serializable;
import java.util.Objects;
import com.vinci.survey.domain.enumeration.SurveyType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the SurveyData entity. This class is used in SurveyDataResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /survey-data?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SurveyDataCriteria implements Serializable {
    /**
     * Class for filtering SurveyType
     */
    public static class SurveyTypeFilter extends Filter<SurveyType> {
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter identifier;

    private SurveyTypeFilter surveyType;

    private LongFilter scoringResult;

    private InstantFilter createdTime;

    private InstantFilter endTime;

    private LongFilter surveyId;

    private LongFilter medicalId;

    private LongFilter userExtraId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getIdentifier() {
        return identifier;
    }

    public void setIdentifier(StringFilter identifier) {
        this.identifier = identifier;
    }

    public SurveyTypeFilter getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(SurveyTypeFilter surveyType) {
        this.surveyType = surveyType;
    }

    public LongFilter getScoringResult() {
        return scoringResult;
    }

    public void setScoringResult(LongFilter scoringResult) {
        this.scoringResult = scoringResult;
    }

    public InstantFilter getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(InstantFilter createdTime) {
        this.createdTime = createdTime;
    }

    public InstantFilter getEndTime() {
        return endTime;
    }

    public void setEndTime(InstantFilter endTime) {
        this.endTime = endTime;
    }

    public LongFilter getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(LongFilter surveyId) {
        this.surveyId = surveyId;
    }

    public LongFilter getMedicalId() {
        return medicalId;
    }

    public void setMedicalId(LongFilter medicalId) {
        this.medicalId = medicalId;
    }

    public LongFilter getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(LongFilter userExtraId) {
        this.userExtraId = userExtraId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SurveyDataCriteria that = (SurveyDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(identifier, that.identifier) &&
            Objects.equals(surveyType, that.surveyType) &&
            Objects.equals(scoringResult, that.scoringResult) &&
            Objects.equals(createdTime, that.createdTime) &&
            Objects.equals(endTime, that.endTime) &&
            Objects.equals(surveyId, that.surveyId) &&
            Objects.equals(medicalId, that.medicalId) &&
            Objects.equals(userExtraId, that.userExtraId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        identifier,
        surveyType,
        scoringResult,
        createdTime,
        endTime,
        surveyId,
        medicalId,
        userExtraId
        );
    }

    @Override
    public String toString() {
        return "SurveyDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (identifier != null ? "identifier=" + identifier + ", " : "") +
                (surveyType != null ? "surveyType=" + surveyType + ", " : "") +
                (scoringResult != null ? "scoringResult=" + scoringResult + ", " : "") +
                (createdTime != null ? "createdTime=" + createdTime + ", " : "") +
                (endTime != null ? "endTime=" + endTime + ", " : "") +
                (surveyId != null ? "surveyId=" + surveyId + ", " : "") +
                (medicalId != null ? "medicalId=" + medicalId + ", " : "") +
                (userExtraId != null ? "userExtraId=" + userExtraId + ", " : "") +
            "}";
    }

}
