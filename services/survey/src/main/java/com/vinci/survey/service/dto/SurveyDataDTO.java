package com.vinci.survey.service.dto;
import com.vinci.survey.domain.enumeration.SurveyType;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the SurveyData entity.
 */
public class SurveyDataDTO implements Serializable {

    private Long id;

    @NotNull
    private String identifier;

    @NotNull
    private SurveyType surveyType;


    @Lob
    private String assesmentData;

    @NotNull
    private Long scoringResult;

    @NotNull
    private Instant createdTime;

    private Instant endTime;

    @Lob
    private String additionalInfo;

    private Long surveyId;

    private Long medicalId;


    private Long userExtraId;

    public SurveyDataDTO(Long id, String identifier, SurveyType surveyType, String assesmentData, Long scoringResult, Instant createdTime, Instant endTime, String additionalInfo, Long surveyId, Long medicalId, Long userExtraId) {
        this.id = id;
        this.identifier = identifier;
        this.surveyType = surveyType;
        this.assesmentData = assesmentData;
        this.scoringResult = scoringResult;
        this.createdTime = createdTime;
        this.endTime = endTime;
        this.additionalInfo = additionalInfo;
        this.surveyId = surveyId;
        this.medicalId = medicalId;
        this.userExtraId = userExtraId;
    }

    public SurveyDataDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public SurveyType getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(SurveyType surveyType) {
        this.surveyType = surveyType;
    }

    public String getAssesmentData() {
        return assesmentData;
    }

    public void setAssesmentData(String assesmentData) {
        this.assesmentData = assesmentData;
    }

    public Long getScoringResult() {
        return scoringResult;
    }

    public void setScoringResult(Long scoringResult) {
        this.scoringResult = scoringResult;
    }

    public Instant getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Instant createdTime) {
        this.createdTime = createdTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public Long getMedicalId() {
        return medicalId;
    }

    public void setMedicalId(Long medicalId) {
        this.medicalId = medicalId;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SurveyDataDTO surveyDataDTO = (SurveyDataDTO) o;
        if (surveyDataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyDataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyDataDTO{" +
            "id=" + getId() +
            ", identifier='" + getIdentifier() + "'" +
            ", surveyType='" + getSurveyType() + "'" +
            ", assesmentData='" + getAssesmentData() + "'" +
            ", scoringResult=" + getScoringResult() +
            ", createdTime='" + getCreatedTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", additionalInfo='" + getAdditionalInfo() + "'" +
            ", surveyId=" + getSurveyId() +
            ", medicalId=" + getMedicalId() +
            ", userExtra=" + getUserExtraId() +
            "}";
    }
}
