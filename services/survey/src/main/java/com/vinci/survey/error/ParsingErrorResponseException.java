package com.vinci.survey.error;

import com.vinci.survey.error.response.ErrorResponse;
import com.vinci.survey.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ParsingErrorResponseException extends AbstractThrowableProblem {

    private static final String TITLE = "PARSING_ERROR_EXCEPTION";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "ErrorResponse could not be parsed!";

    public ParsingErrorResponseException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public ParsingErrorResponseException(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format("%s\nError message: %s",DEFAULT_DETAILS, details)); }
    public ParsingErrorResponseException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "ParsingErrorResponseException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
