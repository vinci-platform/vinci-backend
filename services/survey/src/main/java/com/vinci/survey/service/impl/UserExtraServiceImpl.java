package com.vinci.survey.service.impl;

import com.vinci.survey.domain.UserExtra;
import com.vinci.survey.repository.UserExtraRepository;
import com.vinci.survey.service.UserExtraService;
import com.vinci.survey.service.dto.UserExtraDTO;
import com.vinci.survey.service.mapper.UserExtraMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing UserExtra.
 */
@Service
@Transactional
public class UserExtraServiceImpl implements UserExtraService {

    private final Logger log = LoggerFactory.getLogger(UserExtraServiceImpl.class);

    private final UserExtraRepository userExtraRepository;

    private final UserExtraMapper userExtraMapper;

    public UserExtraServiceImpl(UserExtraRepository userExtraRepository, UserExtraMapper userExtraMapper) {
        this.userExtraRepository = userExtraRepository;
        this.userExtraMapper = userExtraMapper;
    }

    /**
     * Save a userExtra.
     *
     * @param userExtraDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserExtraDTO save(UserExtraDTO userExtraDTO) {
        log.debug("Request to save UserExtra : {}", userExtraDTO);
        UserExtra userExtra = userExtraMapper.toEntity(userExtraDTO);
        userExtra = userExtraRepository.save(userExtra);
        return userExtraMapper.toDto(userExtra);
    }

    /**
     * Get all the userExtras.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserExtraDTO> findAll() {
        log.debug("Request to get all UserExtras");
        return userExtraRepository.findAll().stream()
            .map(userExtraMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one userExtra by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserExtraDTO> findOne(Long id) {
        log.debug("Request to get UserExtra : {}", id);
        return userExtraRepository.findById(id)
            .map(userExtraMapper::toDto);
    }

    @Override
    public Optional<UserExtraDTO> findByUserIdentifier(String userIdentifier) {
        log.debug("Request to get UserExtra by user Identifier : {}", userIdentifier);
        return userExtraRepository.findByUserIdentifier(userIdentifier).map(userExtraMapper::toDto);
    }

    @Override
    public List<UserExtraDTO> findAllByOrganizationIdentifier(String organizationIdentifier) {
        log.debug("Request to get all UserExtras by organization: {}", organizationIdentifier);
        return userExtraRepository.findAllByOrganizationIdentifier(organizationIdentifier).stream()
            .map(userExtraMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<UserExtraDTO> findAllByFamilyIdentifier(String familyIdentifier) {
        log.debug("Request to get all UserExtras by organization: {}", familyIdentifier);
        return userExtraRepository.findAllByFamilyIdentifier(familyIdentifier).stream()
            .map(userExtraMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Delete the userExtra by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserExtra : {}", id);        userExtraRepository.deleteById(id);
    }
}
