/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.survey.web.rest.vm;
