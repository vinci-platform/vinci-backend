package com.vinci.survey.web.rest.feign.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GatewayClientFallback implements GatewayClient {

    private final Logger log = LoggerFactory.getLogger(GatewayClientFallback.class);

    @Override
    public Long validate(Long userId) {
        log.error("Encountered an error. Defaulting to empty");
        return 0L;
    }

    @Override
    public Long getSurveyDevice(Long userId) {
        log.error("Encountered an error. Defaulting to empty");
        return 0L;
    }

    @Override
    public Long getSurveyDeviceV_1_0_0(Long userId) {
        return 0L;
    }
}
