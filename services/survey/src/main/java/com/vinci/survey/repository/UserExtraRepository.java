package com.vinci.survey.repository;

import com.vinci.survey.domain.UserExtra;
import org.springframework.data.jpa.repository.*;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the UserExtra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserExtraRepository extends JpaRepository<UserExtra, Long> {

    Optional<UserExtra> findByUserIdentifier(String userIdentifier);

    List<UserExtra> findAllByOrganizationIdentifier(String organizationIdentifier);

    List<UserExtra> findAllByFamilyIdentifier(String familyIdentifier);
}
