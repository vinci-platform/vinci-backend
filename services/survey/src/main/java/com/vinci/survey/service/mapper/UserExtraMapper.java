package com.vinci.survey.service.mapper;

import com.vinci.survey.domain.*;
import com.vinci.survey.service.dto.UserExtraDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserExtra and its DTO UserExtraDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserExtraMapper extends EntityMapper<UserExtraDTO, UserExtra> {



    default UserExtra fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtra userExtra = new UserExtra();
        userExtra.setId(id);
        return userExtra;
    }
}
