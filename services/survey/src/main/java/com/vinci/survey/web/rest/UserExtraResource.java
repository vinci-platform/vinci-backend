package com.vinci.survey.web.rest;

import com.vinci.survey.service.UserExtraService;
import com.vinci.survey.service.dto.UserExtraDTO;
import com.vinci.survey.web.rest.errors.BadRequestAlertException;
import com.vinci.survey.web.rest.feign.client.GatewayClient;
import com.vinci.survey.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserExtra.
 */
@RestController
@RequestMapping("/api")
public class UserExtraResource {

    private final Logger log = LoggerFactory.getLogger(UserExtraResource.class);

    private static final String ENTITY_NAME = "surveyUserExtra";

    private final UserExtraService userExtraService;

    private final GatewayClient gatewayClient;

    public UserExtraResource(UserExtraService userExtraService, GatewayClient gatewayClient) {
        this.userExtraService = userExtraService;
	this.gatewayClient = gatewayClient;
    }

    /**
     * POST  /user-extras : Create a new userExtra.
     *
     * @param userExtraDTO the userExtraDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userExtraDTO, or with status 400 (Bad Request) if the userExtra has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-extras")
    public ResponseEntity<UserExtraDTO> createUserExtra(@Valid @RequestBody UserExtraDTO userExtraDTO) throws URISyntaxException {
        log.debug("REST request to save UserExtra : {}", userExtraDTO);
        if (userExtraDTO.getId() != null) {
            throw new BadRequestAlertException("A new userExtra cannot already have an ID", ENTITY_NAME, "idexists");
        }
       	String userIdentifier=userExtraDTO.getUserIdentifier();
       	Long userId;

	try {
	    userId = Long.valueOf(userIdentifier).longValue();
	}
        catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException: " + nfe.getMessage());
            throw new BadRequestAlertException("Bad format of userIdentifier",userIdentifier,"not exists");
	}
            Long res=gatewayClient.validate(userId);

	if(res==0L){
         throw new BadRequestAlertException("The user with the identifier is not registered",userIdentifier,"not exists");
	}
     	UserExtraDTO result = userExtraService.save(userExtraDTO);

        return ResponseEntity.created(new URI("/api/user-extras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-extras : Updates an existing userExtra.
     *
     * @param userExtraDTO the userExtraDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userExtraDTO,
     * or with status 400 (Bad Request) if the userExtraDTO is not valid,
     * or with status 500 (Internal Server Error) if the userExtraDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-extras")
    public ResponseEntity<UserExtraDTO> updateUserExtra(@Valid @RequestBody UserExtraDTO userExtraDTO) throws URISyntaxException {
        log.debug("REST request to update UserExtra : {}", userExtraDTO);
        if (userExtraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserExtraDTO result = userExtraService.save(userExtraDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userExtraDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-extras : get all the userExtras.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userExtras in body
     */
    @GetMapping("/user-extras")
    public List<UserExtraDTO> getAllUserExtras() {
        log.debug("REST request to get all UserExtras");
        return userExtraService.findAll();
    }

    /**
     * GET  /user-extras/:id : get the "id" userExtra.
     *
     * @param id the id of the userExtraDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userExtraDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-extras/{id}")
    public ResponseEntity<UserExtraDTO> getUserExtra(@PathVariable Long id) {
        log.debug("REST request to get UserExtra : {}", id);
        Optional<UserExtraDTO> userExtraDTO = userExtraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userExtraDTO);
    }

    /**
     * GET userExtra for user
     *
     * @param userIdentifier the userIdentifier of the userExtraDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userExtraDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-extras/user/{userIdentifier}")
    public ResponseEntity<UserExtraDTO> getUserExtraFor(@PathVariable String userIdentifier) {
        log.debug("REST request to get UserExtra : {}", userIdentifier);
        Optional<UserExtraDTO> userExtraDTO = userExtraService.findByUserIdentifier(userIdentifier);
        return ResponseUtil.wrapOrNotFound(userExtraDTO);
    }

    /**
     * GET get all the userExtras by family .
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userExtras in body
     */
    @GetMapping("/user-extras/user/family/{familyIdentifier}")
    public List<UserExtraDTO> getAllUserExtrasByFamily(@PathVariable String familyIdentifier) {
        log.debug("REST request to get all UserExtras");
        return userExtraService.findAllByFamilyIdentifier(familyIdentifier);
    }


    /**
     * GET get all the userExtras by family .
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userExtras in body
     */
    @GetMapping("/user-extras/user/organization/{organizationIdentifier}")
    public List<UserExtraDTO> getAllUserExtrasByOrganization(@PathVariable String organizationIdentifier) {
        log.debug("REST request to get all UserExtras");
        return userExtraService.findAllByOrganizationIdentifier(organizationIdentifier);
    }

    /**
     * DELETE  /user-extras/:id : delete the "id" userExtra.
     *
     * @param id the id of the userExtraDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-extras/{id}")
    public ResponseEntity<Void> deleteUserExtra(@PathVariable Long id) {
        log.debug("REST request to delete UserExtra : {}", id);
        userExtraService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
