package com.vinci.survey.service;

import com.vinci.survey.service.dto.UserExtraDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing UserExtra.
 */
public interface UserExtraService {

    /**
     * Save a userExtra.
     *
     * @param userExtraDTO the entity to save
     * @return the persisted entity
     */
    UserExtraDTO save(UserExtraDTO userExtraDTO);

    /**
     * Get all the userExtras.
     *
     * @return the list of entities
     */
    List<UserExtraDTO> findAll();


    /**
     * Get the "id" userExtra.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<UserExtraDTO> findOne(Long id);

    /**
     * Get UserExtra by userIdentifier
     * @param userIdentifier the user Identifier
     * @return the entity
     */
    Optional<UserExtraDTO> findByUserIdentifier(String userIdentifier);

    /**
     * Get all UserExtra by organizationIdentifier
     * @param organizationIdentifier the organization Identifier
     * @return the entity
     */
    List<UserExtraDTO> findAllByOrganizationIdentifier(String organizationIdentifier);

    /**
     * Get all UserExtra by familyIdentifier
     * @param familyIdentifier the organization Identifier
     * @return the list of entities
     */
    List<UserExtraDTO> findAllByFamilyIdentifier(String familyIdentifier);

    /**
     * Delete the "id" userExtra.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
