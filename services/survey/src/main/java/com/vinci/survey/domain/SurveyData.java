package com.vinci.survey.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vinci.survey.domain.enumeration.SurveyType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A SurveyData.
 */
@Entity
@Table(name = "survey_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "identifier", nullable = false)
    private String identifier;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "survey_type", nullable = false)
    private SurveyType surveyType;


    @Lob
    @Column(name = "assesment_data", nullable = false)
    private String assesmentData;

    @NotNull
    @Column(name = "scoring_result", nullable = false)
    private Long scoringResult;

    @NotNull
    @Column(name = "created_time", nullable = false)
    private Instant createdTime;

    @Column(name = "end_time")
    private Instant endTime;

    @Lob
    @Column(name = "additional_info")
    private String additionalInfo;

    @Column(name = "survey_id")
    private Long surveyId;

    @Column(name = "medical_id")
    private Long medicalId;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("surveyData")
    private UserExtra userExtra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public SurveyData identifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public SurveyType getSurveyType() {
        return surveyType;
    }

    public SurveyData surveyType(SurveyType surveyType) {
        this.surveyType = surveyType;
        return this;
    }

    public void setSurveyType(SurveyType surveyType) {
        this.surveyType = surveyType;
    }

    public String getAssesmentData() {
        return assesmentData;
    }

    public SurveyData assesmentData(String assesmentData) {
        this.assesmentData = assesmentData;
        return this;
    }

    public void setAssesmentData(String assesmentData) {
        this.assesmentData = assesmentData;
    }

    public Long getScoringResult() {
        return scoringResult;
    }

    public SurveyData scoringResult(Long scoringResult) {
        this.scoringResult = scoringResult;
        return this;
    }

    public void setScoringResult(Long scoringResult) {
        this.scoringResult = scoringResult;
    }

    public Instant getCreatedTime() {
        return createdTime;
    }

    public SurveyData createdTime(Instant createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public void setCreatedTime(Instant createdTime) {
        this.createdTime = createdTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public SurveyData endTime(Instant endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public SurveyData additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public SurveyData surveyId(Long surveyId) {
        this.surveyId = surveyId;
        return this;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public Long getMedicalId() {
        return medicalId;
    }

    public SurveyData medicalId(Long medicalId) {
        this.medicalId = medicalId;
        return this;
    }

    public void setMedicalId(Long medicalId) {
        this.medicalId = medicalId;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public SurveyData userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SurveyData surveyData = (SurveyData) o;
        if (surveyData.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyData.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyData{" +
            "id=" + getId() +
            ", identifier='" + getIdentifier() + "'" +
            ", surveyType='" + getSurveyType() + "'" +
            ", assesmentData='" + getAssesmentData() + "'" +
            ", scoringResult=" + getScoringResult() +
            ", createdTime='" + getCreatedTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", additionalInfo='" + getAdditionalInfo() + "'" +
            ", surveyId=" + getSurveyId() +
            ", medicalId=" + getMedicalId() +
            "}";
    }
}
