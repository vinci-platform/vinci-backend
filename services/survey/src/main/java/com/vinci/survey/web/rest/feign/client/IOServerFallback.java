package com.vinci.survey.web.rest.feign.client;

import com.vinci.survey.web.rest.beans.SurveyData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class IOServerFallback implements IOServerClient {

    @Override
    public String saveSurveyData(SurveyData surveyData) {
        log.error("Encountered an error when saving survey data to IO server.");
        return ("Encountered an error when saving survey data to IO server.");
    }

    @Override
    public String saveSurveyDataV_1_0_0(SurveyData surveyData) {
        log.error("Encountered an error when saving survey data to IO server.");
        return ("Encountered an error when saving survey data to IO server.");
    }
}
