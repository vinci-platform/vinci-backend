package com.vinci.survey.service.mapper;

import com.vinci.survey.domain.*;
import com.vinci.survey.service.dto.SurveyDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SurveyData and its DTO SurveyDataDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class})
public interface SurveyDataMapper extends EntityMapper<SurveyDataDTO, SurveyData> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    SurveyDataDTO toDto(SurveyData surveyData);

    @Mapping(source = "userExtraId", target = "userExtra")
    SurveyData toEntity(SurveyDataDTO surveyDataDTO);

    default SurveyData fromId(Long id) {
        if (id == null) {
            return null;
        }
        SurveyData surveyData = new SurveyData();
        surveyData.setId(id);
        return surveyData;
    }
}
