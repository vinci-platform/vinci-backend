package com.vinci.survey.web.rest.feign;

import com.vinci.survey.web.rest.beans.SurveyData;
import com.vinci.survey.web.rest.feign.client.IOServerClient;

public class IOServerClientForTesting implements IOServerClient {
    @Override
    public String saveSurveyData(SurveyData surveyData) {
        return getSurveyData();
    }

    @Override
    public String saveSurveyDataV_1_0_0(SurveyData surveyData) {
        //empty for now, because response is not used, it should throw error if response is used, probably json parsing error
        return "";
    }

    private String getSurveyData() {
        //empty for now, because response is not used, it should throw error if response is used, probably json parsing error
        return "";
    }
}
