package com.vinci.survey.web.rest;

import com.vinci.survey.SurveyApp;
import com.vinci.survey.domain.SurveyData;
import com.vinci.survey.domain.UserExtra;
import com.vinci.survey.domain.enumeration.SurveyType;
import com.vinci.survey.repository.SurveyDataRepository;
import com.vinci.survey.service.SurveyDataQueryService;
import com.vinci.survey.service.SurveyDataService;
import com.vinci.survey.service.UserExtraService;
import com.vinci.survey.service.dto.SurveyDataDTO;
import com.vinci.survey.service.mapper.SurveyDataMapper;
import com.vinci.survey.web.rest.errors.ExceptionTranslator;
import com.vinci.survey.web.rest.feign.client.GatewayClient;
import com.vinci.survey.web.rest.feign.client.IOServerClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.vinci.survey.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Integration tests for the {@Link SurveyDataResource} REST controller.
 */
@SpringBootTest(classes = SurveyApp.class)
public class SurveyDataResourceIT {

    private static final String DEFAULT_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIER = "BBBBBBBBBB";

    private static final SurveyType DEFAULT_SURVEY_TYPE = SurveyType.WHOQOL_BREF;
    private static final SurveyType UPDATED_SURVEY_TYPE = SurveyType.IPAQ;

    private static final String DEFAULT_ASSESMENT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_ASSESMENT_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_SCORING_RESULT = 1L;
    private static final Long UPDATED_SCORING_RESULT = 2L;

    private static final Instant DEFAULT_CREATED_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ADDITIONAL_INFO = "AAAAAAAAAA";
    private static final String UPDATED_ADDITIONAL_INFO = "BBBBBBBBBB";

    private static final Long DEFAULT_SURVEY_ID = 1L;
    private static final Long UPDATED_SURVEY_ID = 2L;

    private static final Long DEFAULT_MEDICAL_ID = 1L;
    private static final Long UPDATED_MEDICAL_ID = 2L;

    @Autowired
    private SurveyDataRepository surveyDataRepository;

    @Autowired
    private SurveyDataMapper surveyDataMapper;

    @Autowired
    private SurveyDataService surveyDataService;

    @Autowired
    private SurveyDataQueryService surveyDataQueryService;

    @Autowired
    private GatewayClient gatewayClient;

    @Autowired
    private IOServerClient ioServerClient;

    @Autowired
    private UserExtraService userExtraService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSurveyDataMockMvc;

    private SurveyData surveyData;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SurveyDataResource surveyDataResource = new SurveyDataResource(surveyDataService,surveyDataQueryService,userExtraService,gatewayClient,ioServerClient);
        this.restSurveyDataMockMvc = MockMvcBuilders.standaloneSetup(surveyDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SurveyData createEntity(EntityManager em) {
        SurveyData surveyData = new SurveyData()
            .identifier(DEFAULT_IDENTIFIER)
            .surveyType(DEFAULT_SURVEY_TYPE)
            .assesmentData(DEFAULT_ASSESMENT_DATA)
            .scoringResult(DEFAULT_SCORING_RESULT)
            .createdTime(DEFAULT_CREATED_TIME)
            .endTime(DEFAULT_END_TIME)
            .additionalInfo(DEFAULT_ADDITIONAL_INFO)
            .surveyId(DEFAULT_SURVEY_ID)
            .medicalId(DEFAULT_MEDICAL_ID);
        // Add required entity
        UserExtra userExtra = UserExtraResourceIntTest.createEntity(em);
        em.persist(userExtra);
        em.flush();
        surveyData.setUserExtra(userExtra);
        return surveyData;
    }

    @BeforeEach
    public void initTest() {
        surveyData = createEntity(em);
    }

    @Test
    @Transactional
    public void createSurveyData() throws Exception {
        int databaseSizeBeforeCreate = surveyDataRepository.findAll().size();

        // Create the SurveyData
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isCreated());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeCreate + 1);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(DEFAULT_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(DEFAULT_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(DEFAULT_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(DEFAULT_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(DEFAULT_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(DEFAULT_ADDITIONAL_INFO);
        assertThat(testSurveyData.getSurveyId()).isEqualTo(DEFAULT_SURVEY_ID);
        assertThat(testSurveyData.getMedicalId()).isEqualTo(DEFAULT_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void createSurveyDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = surveyDataRepository.findAll().size();

        // Create the SurveyData with an existing ID
        surveyData.setId(1L);
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdentifierIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setIdentifier(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSurveyTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setSurveyType(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoringResultIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setScoringResult(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setCreatedTime(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(surveyData.getId().intValue())))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].surveyType").value(hasItem(DEFAULT_SURVEY_TYPE.toString())))
            .andExpect(jsonPath("$.[*].assesmentData").value(hasItem(DEFAULT_ASSESMENT_DATA.toString())))
            .andExpect(jsonPath("$.[*].scoringResult").value(hasItem(DEFAULT_SCORING_RESULT.intValue())))
            .andExpect(jsonPath("$.[*].createdTime").value(hasItem(DEFAULT_CREATED_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].additionalInfo").value(hasItem(DEFAULT_ADDITIONAL_INFO.toString())))
            .andExpect(jsonPath("$.[*].surveyId").value(hasItem(DEFAULT_SURVEY_ID.intValue())))
            .andExpect(jsonPath("$.[*].medicalId").value(hasItem(DEFAULT_MEDICAL_ID.intValue())));
    }

    @Test
    @Transactional
    public void getSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(surveyData.getId().intValue()))
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.surveyType").value(DEFAULT_SURVEY_TYPE.toString()))
            .andExpect(jsonPath("$.assesmentData").value(DEFAULT_ASSESMENT_DATA.toString()))
            .andExpect(jsonPath("$.scoringResult").value(DEFAULT_SCORING_RESULT.intValue()))
            .andExpect(jsonPath("$.createdTime").value(DEFAULT_CREATED_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.additionalInfo").value(DEFAULT_ADDITIONAL_INFO.toString()))
            .andExpect(jsonPath("$.surveyId").value(DEFAULT_SURVEY_ID.intValue()))
            .andExpect(jsonPath("$.medicalId").value(DEFAULT_MEDICAL_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSurveyData() throws Exception {
        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        int databaseSizeBeforeUpdate = surveyDataRepository.findAll().size();

        // Update the surveyData
        SurveyData updatedSurveyData = surveyDataRepository.findById(surveyData.getId()).get();
        // Disconnect from session so that the updates on updatedSurveyData are not directly saved in db
        em.detach(updatedSurveyData);
        updatedSurveyData
            .identifier(UPDATED_IDENTIFIER)
            .surveyType(UPDATED_SURVEY_TYPE)
            .assesmentData(UPDATED_ASSESMENT_DATA)
            .scoringResult(UPDATED_SCORING_RESULT)
            .createdTime(UPDATED_CREATED_TIME)
            .endTime(UPDATED_END_TIME)
            .additionalInfo(UPDATED_ADDITIONAL_INFO)
            .surveyId(UPDATED_SURVEY_ID)
            .medicalId(UPDATED_MEDICAL_ID);
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(updatedSurveyData);

        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isOk());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeUpdate);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(UPDATED_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(UPDATED_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(UPDATED_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(UPDATED_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(UPDATED_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(UPDATED_ADDITIONAL_INFO);
        assertThat(testSurveyData.getSurveyId()).isEqualTo(UPDATED_SURVEY_ID);
        assertThat(testSurveyData.getMedicalId()).isEqualTo(UPDATED_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingSurveyData() throws Exception {
        int databaseSizeBeforeUpdate = surveyDataRepository.findAll().size();

        // Create the SurveyData
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        int databaseSizeBeforeDelete = surveyDataRepository.findAll().size();

        // Delete the surveyData
        restSurveyDataMockMvc.perform(delete("/api/survey-data/{id}", surveyData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SurveyData.class);
        SurveyData surveyData1 = new SurveyData();
        surveyData1.setId(1L);
        SurveyData surveyData2 = new SurveyData();
        surveyData2.setId(surveyData1.getId());
        assertThat(surveyData1).isEqualTo(surveyData2);
        surveyData2.setId(2L);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
        surveyData1.setId(null);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SurveyDataDTO.class);
        SurveyDataDTO surveyDataDTO1 = new SurveyDataDTO();
        surveyDataDTO1.setId(1L);
        SurveyDataDTO surveyDataDTO2 = new SurveyDataDTO();
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
        surveyDataDTO2.setId(surveyDataDTO1.getId());
        assertThat(surveyDataDTO1).isEqualTo(surveyDataDTO2);
        surveyDataDTO2.setId(2L);
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
        surveyDataDTO1.setId(null);
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(surveyDataMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(surveyDataMapper.fromId(null)).isNull();
    }
}
