package com.vinci.survey.web.rest;

import com.vinci.survey.SurveyApp;
import com.vinci.survey.domain.SurveyData;
import com.vinci.survey.domain.UserExtra;
import com.vinci.survey.domain.enumeration.SurveyType;
import com.vinci.survey.repository.SurveyDataRepository;
import com.vinci.survey.service.SurveyDataQueryService;
import com.vinci.survey.service.SurveyDataService;
import com.vinci.survey.service.UserExtraService;
import com.vinci.survey.service.dto.SurveyDataDTO;
import com.vinci.survey.service.impl.SurveyDataServiceImpl;
import com.vinci.survey.service.mapper.SurveyDataMapper;
import com.vinci.survey.web.rest.errors.ExceptionTranslator;
import com.vinci.survey.web.rest.feign.client.GatewayClient;
import com.vinci.survey.web.rest.feign.client.IOServerClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.vinci.survey.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the SurveyDataResource REST controller.
 *
 * @see SurveyDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SurveyApp.class)
public class SurveyDataResourceIntTest {

    private static final String DEFAULT_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIER = "BBBBBBBBBB";

    private static final SurveyType DEFAULT_SURVEY_TYPE = SurveyType.WHOQOL_BREF;
    private static final SurveyType UPDATED_SURVEY_TYPE = SurveyType.IPAQ;

    private static final String DEFAULT_ASSESMENT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_ASSESMENT_DATA = "BBBBBBBBBB";

    private static final Long DEFAULT_SCORING_RESULT = 1L;
    private static final Long UPDATED_SCORING_RESULT = 2L;

    private static final Instant DEFAULT_CREATED_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ADDITIONAL_INFO = "AAAAAAAAAA";
    private static final String UPDATED_ADDITIONAL_INFO = "BBBBBBBBBB";

    private static final Long DEFAULT_SURVEY_ID = 1L;
    private static final Long UPDATED_SURVEY_ID = 2L;

    private static final Long DEFAULT_MEDICAL_ID = 1L;
    private static final Long UPDATED_MEDICAL_ID = 2L;

    private static final String DEFAULT_USER_IDENTIFIER = "3";

    @Autowired
    private SurveyDataRepository surveyDataRepository;

    @Autowired
    private SurveyDataMapper surveyDataMapper;

//    @Autowired
    private SurveyDataService surveyDataService;

    @Autowired
    private SurveyDataQueryService surveyDataQueryService;

    @Mock
    private GatewayClient gatewayClientMock;

    @Mock
    private IOServerClient ioServerClientMock;

    @Autowired
    private UserExtraService userExtraService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSurveyDataMockMvc;

    private SurveyData surveyData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        surveyDataService = new SurveyDataServiceImpl(surveyDataRepository,surveyDataMapper,gatewayClientMock,ioServerClientMock);
        final SurveyDataResource surveyDataResource = new SurveyDataResource(surveyDataService,surveyDataQueryService,userExtraService,gatewayClientMock,ioServerClientMock);
        this.restSurveyDataMockMvc = MockMvcBuilders.standaloneSetup(surveyDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SurveyData createEntity(EntityManager em) {
        SurveyData surveyData = new SurveyData()
            .identifier(DEFAULT_IDENTIFIER)
            .surveyType(DEFAULT_SURVEY_TYPE)
            .assesmentData(DEFAULT_ASSESMENT_DATA)
            .scoringResult(DEFAULT_SCORING_RESULT)
            .createdTime(DEFAULT_CREATED_TIME)
            .endTime(DEFAULT_END_TIME)
            .additionalInfo(DEFAULT_ADDITIONAL_INFO)
            .surveyId(DEFAULT_SURVEY_ID)
            .medicalId(DEFAULT_MEDICAL_ID);
        // Add required entity
        UserExtra userExtra = UserExtraResourceIntTest.createEntity(em);
        em.persist(userExtra);
        em.flush();
        surveyData.setUserExtra(userExtra);
        return surveyData;
    }

    @Before
    public void initTest() {
        surveyData = createEntity(em);
    }

    @Test
    @Transactional
    public void createSurveyData() throws Exception {
        int databaseSizeBeforeCreate = surveyDataRepository.findAll().size();

        when(gatewayClientMock.getSurveyDevice(any())).thenReturn(3L);
        when(gatewayClientMock.validate(any())).thenReturn(3L);
        when(ioServerClientMock.saveSurveyData(any())).thenReturn("");

        // Create the SurveyData
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .param("userIdentifier",DEFAULT_USER_IDENTIFIER)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isCreated());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeCreate + 1);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(DEFAULT_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(DEFAULT_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(DEFAULT_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(DEFAULT_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(DEFAULT_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(DEFAULT_ADDITIONAL_INFO);
        assertThat(testSurveyData.getSurveyId()).isEqualTo(DEFAULT_SURVEY_ID);
        assertThat(testSurveyData.getMedicalId()).isEqualTo(DEFAULT_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void createSurveyDataV_1_0_0() throws Exception {
        // Create the SurveyData
        surveyData.setIdentifier("3");
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);
        when(gatewayClientMock.getSurveyDeviceV_1_0_0(any())).thenReturn(3L);
        when(gatewayClientMock.validate(any())).thenReturn(3L);
        when(ioServerClientMock.saveSurveyDataV_1_0_0(any())).thenReturn(surveyData.getIdentifier());
        MvcResult mvcResult = restSurveyDataMockMvc.perform(post("/api/v1.0.0/survey-data")
            .param("userIdentifier", DEFAULT_USER_IDENTIFIER)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andDo(mvcResult1 -> System.out.println(mvcResult1.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andReturn();

        String returnString = mvcResult.getResponse().getContentAsString();
        assertThat(returnString).isEqualTo("\"" + surveyDataDTO.getIdentifier() + "\"");
    }

    private com.vinci.survey.web.rest.beans.SurveyData getSurveyDataBean(SurveyData surveyData) {
            return new com.vinci.survey.web.rest.beans.SurveyData()
                .id(surveyData.getId())
                .identifier(surveyData.getIdentifier())
                .surveyType(surveyData.getSurveyType())
                .assesmentData(surveyData.getAssesmentData())
                .scoringResult(surveyData.getScoringResult())
                .createdTime(surveyData.getCreatedTime())
                .endTime(surveyData.getEndTime())
                .additionalInfo(surveyData.getAdditionalInfo())
                .deviceId(3L);
    }

    @Test
    @Transactional
    public void createSurveyDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = surveyDataRepository.findAll().size();

        // Create the SurveyData with an existing ID
        surveyData.setId(1L);
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIdentifierIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setIdentifier(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSurveyTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setSurveyType(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoringResultIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setScoringResult(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = surveyDataRepository.findAll().size();
        // set the field null
        surveyData.setCreatedTime(null);

        // Create the SurveyData, which fails.
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        restSurveyDataMockMvc.perform(post("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(surveyData.getId().intValue())))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].surveyType").value(hasItem(DEFAULT_SURVEY_TYPE.toString())))
            .andExpect(jsonPath("$.[*].assesmentData").value(hasItem(DEFAULT_ASSESMENT_DATA.toString())))
            .andExpect(jsonPath("$.[*].scoringResult").value(hasItem(DEFAULT_SCORING_RESULT.intValue())))
            .andExpect(jsonPath("$.[*].createdTime").value(hasItem(DEFAULT_CREATED_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].additionalInfo").value(hasItem(DEFAULT_ADDITIONAL_INFO.toString())))
            .andExpect(jsonPath("$.[*].surveyId").value(hasItem(DEFAULT_SURVEY_ID.intValue())))
            .andExpect(jsonPath("$.[*].medicalId").value(hasItem(DEFAULT_MEDICAL_ID.intValue())));
    }

    @Test
    @Transactional
    public void getSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", surveyData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(surveyData.getId().intValue()))
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.surveyType").value(DEFAULT_SURVEY_TYPE.toString()))
            .andExpect(jsonPath("$.assesmentData").value(DEFAULT_ASSESMENT_DATA.toString()))
            .andExpect(jsonPath("$.scoringResult").value(DEFAULT_SCORING_RESULT.intValue()))
            .andExpect(jsonPath("$.createdTime").value(DEFAULT_CREATED_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.additionalInfo").value(DEFAULT_ADDITIONAL_INFO.toString()))
            .andExpect(jsonPath("$.surveyId").value(DEFAULT_SURVEY_ID.intValue()))
            .andExpect(jsonPath("$.medicalId").value(DEFAULT_MEDICAL_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllSurveyDataByIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where identifier equals to DEFAULT_IDENTIFIER
        defaultSurveyDataShouldBeFound("identifier.equals=" + DEFAULT_IDENTIFIER);

        // Get all the surveyDataList where identifier equals to UPDATED_IDENTIFIER
        defaultSurveyDataShouldNotBeFound("identifier.equals=" + UPDATED_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where identifier in DEFAULT_IDENTIFIER or UPDATED_IDENTIFIER
        defaultSurveyDataShouldBeFound("identifier.in=" + DEFAULT_IDENTIFIER + "," + UPDATED_IDENTIFIER);

        // Get all the surveyDataList where identifier equals to UPDATED_IDENTIFIER
        defaultSurveyDataShouldNotBeFound("identifier.in=" + UPDATED_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where identifier is not null
        defaultSurveyDataShouldBeFound("identifier.specified=true");

        // Get all the surveyDataList where identifier is null
        defaultSurveyDataShouldNotBeFound("identifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyType equals to DEFAULT_SURVEY_TYPE
        defaultSurveyDataShouldBeFound("surveyType.equals=" + DEFAULT_SURVEY_TYPE);

        // Get all the surveyDataList where surveyType equals to UPDATED_SURVEY_TYPE
        defaultSurveyDataShouldNotBeFound("surveyType.equals=" + UPDATED_SURVEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyTypeIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyType in DEFAULT_SURVEY_TYPE or UPDATED_SURVEY_TYPE
        defaultSurveyDataShouldBeFound("surveyType.in=" + DEFAULT_SURVEY_TYPE + "," + UPDATED_SURVEY_TYPE);

        // Get all the surveyDataList where surveyType equals to UPDATED_SURVEY_TYPE
        defaultSurveyDataShouldNotBeFound("surveyType.in=" + UPDATED_SURVEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyType is not null
        defaultSurveyDataShouldBeFound("surveyType.specified=true");

        // Get all the surveyDataList where surveyType is null
        defaultSurveyDataShouldNotBeFound("surveyType.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataByScoringResultIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where scoringResult equals to DEFAULT_SCORING_RESULT
        defaultSurveyDataShouldBeFound("scoringResult.equals=" + DEFAULT_SCORING_RESULT);

        // Get all the surveyDataList where scoringResult equals to UPDATED_SCORING_RESULT
        defaultSurveyDataShouldNotBeFound("scoringResult.equals=" + UPDATED_SCORING_RESULT);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByScoringResultIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where scoringResult in DEFAULT_SCORING_RESULT or UPDATED_SCORING_RESULT
        defaultSurveyDataShouldBeFound("scoringResult.in=" + DEFAULT_SCORING_RESULT + "," + UPDATED_SCORING_RESULT);

        // Get all the surveyDataList where scoringResult equals to UPDATED_SCORING_RESULT
        defaultSurveyDataShouldNotBeFound("scoringResult.in=" + UPDATED_SCORING_RESULT);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByScoringResultIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where scoringResult is not null
        defaultSurveyDataShouldBeFound("scoringResult.specified=true");

        // Get all the surveyDataList where scoringResult is null
        defaultSurveyDataShouldNotBeFound("scoringResult.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataByScoringResultIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where scoringResult greater than or equals to DEFAULT_SCORING_RESULT
        defaultSurveyDataShouldBeFound("scoringResult.greaterOrEqualThan=" + DEFAULT_SCORING_RESULT);

        // Get all the surveyDataList where scoringResult greater than or equals to UPDATED_SCORING_RESULT
        defaultSurveyDataShouldNotBeFound("scoringResult.greaterOrEqualThan=" + UPDATED_SCORING_RESULT);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByScoringResultIsLessThanSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where scoringResult less than or equals to DEFAULT_SCORING_RESULT
        defaultSurveyDataShouldNotBeFound("scoringResult.lessThan=" + DEFAULT_SCORING_RESULT);

        // Get all the surveyDataList where scoringResult less than or equals to UPDATED_SCORING_RESULT
        defaultSurveyDataShouldBeFound("scoringResult.lessThan=" + UPDATED_SCORING_RESULT);
    }


    @Test
    @Transactional
    public void getAllSurveyDataByCreatedTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where createdTime equals to DEFAULT_CREATED_TIME
        defaultSurveyDataShouldBeFound("createdTime.equals=" + DEFAULT_CREATED_TIME);

        // Get all the surveyDataList where createdTime equals to UPDATED_CREATED_TIME
        defaultSurveyDataShouldNotBeFound("createdTime.equals=" + UPDATED_CREATED_TIME);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByCreatedTimeIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where createdTime in DEFAULT_CREATED_TIME or UPDATED_CREATED_TIME
        defaultSurveyDataShouldBeFound("createdTime.in=" + DEFAULT_CREATED_TIME + "," + UPDATED_CREATED_TIME);

        // Get all the surveyDataList where createdTime equals to UPDATED_CREATED_TIME
        defaultSurveyDataShouldNotBeFound("createdTime.in=" + UPDATED_CREATED_TIME);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByCreatedTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where createdTime is not null
        defaultSurveyDataShouldBeFound("createdTime.specified=true");

        // Get all the surveyDataList where createdTime is null
        defaultSurveyDataShouldNotBeFound("createdTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataByEndTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where endTime equals to DEFAULT_END_TIME
        defaultSurveyDataShouldBeFound("endTime.equals=" + DEFAULT_END_TIME);

        // Get all the surveyDataList where endTime equals to UPDATED_END_TIME
        defaultSurveyDataShouldNotBeFound("endTime.equals=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByEndTimeIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where endTime in DEFAULT_END_TIME or UPDATED_END_TIME
        defaultSurveyDataShouldBeFound("endTime.in=" + DEFAULT_END_TIME + "," + UPDATED_END_TIME);

        // Get all the surveyDataList where endTime equals to UPDATED_END_TIME
        defaultSurveyDataShouldNotBeFound("endTime.in=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByEndTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where endTime is not null
        defaultSurveyDataShouldBeFound("endTime.specified=true");

        // Get all the surveyDataList where endTime is null
        defaultSurveyDataShouldNotBeFound("endTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyIdIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyId equals to DEFAULT_SURVEY_ID
        defaultSurveyDataShouldBeFound("surveyId.equals=" + DEFAULT_SURVEY_ID);

        // Get all the surveyDataList where surveyId equals to UPDATED_SURVEY_ID
        defaultSurveyDataShouldNotBeFound("surveyId.equals=" + UPDATED_SURVEY_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyIdIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyId in DEFAULT_SURVEY_ID or UPDATED_SURVEY_ID
        defaultSurveyDataShouldBeFound("surveyId.in=" + DEFAULT_SURVEY_ID + "," + UPDATED_SURVEY_ID);

        // Get all the surveyDataList where surveyId equals to UPDATED_SURVEY_ID
        defaultSurveyDataShouldNotBeFound("surveyId.in=" + UPDATED_SURVEY_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyId is not null
        defaultSurveyDataShouldBeFound("surveyId.specified=true");

        // Get all the surveyDataList where surveyId is null
        defaultSurveyDataShouldNotBeFound("surveyId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyId greater than or equals to DEFAULT_SURVEY_ID
        defaultSurveyDataShouldBeFound("surveyId.greaterOrEqualThan=" + DEFAULT_SURVEY_ID);

        // Get all the surveyDataList where surveyId greater than or equals to UPDATED_SURVEY_ID
        defaultSurveyDataShouldNotBeFound("surveyId.greaterOrEqualThan=" + UPDATED_SURVEY_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataBySurveyIdIsLessThanSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where surveyId less than or equals to DEFAULT_SURVEY_ID
        defaultSurveyDataShouldNotBeFound("surveyId.lessThan=" + DEFAULT_SURVEY_ID);

        // Get all the surveyDataList where surveyId less than or equals to UPDATED_SURVEY_ID
        defaultSurveyDataShouldBeFound("surveyId.lessThan=" + UPDATED_SURVEY_ID);
    }


    @Test
    @Transactional
    public void getAllSurveyDataByMedicalIdIsEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where medicalId equals to DEFAULT_MEDICAL_ID
        defaultSurveyDataShouldBeFound("medicalId.equals=" + DEFAULT_MEDICAL_ID);

        // Get all the surveyDataList where medicalId equals to UPDATED_MEDICAL_ID
        defaultSurveyDataShouldNotBeFound("medicalId.equals=" + UPDATED_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByMedicalIdIsInShouldWork() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where medicalId in DEFAULT_MEDICAL_ID or UPDATED_MEDICAL_ID
        defaultSurveyDataShouldBeFound("medicalId.in=" + DEFAULT_MEDICAL_ID + "," + UPDATED_MEDICAL_ID);

        // Get all the surveyDataList where medicalId equals to UPDATED_MEDICAL_ID
        defaultSurveyDataShouldNotBeFound("medicalId.in=" + UPDATED_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByMedicalIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where medicalId is not null
        defaultSurveyDataShouldBeFound("medicalId.specified=true");

        // Get all the surveyDataList where medicalId is null
        defaultSurveyDataShouldNotBeFound("medicalId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSurveyDataByMedicalIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where medicalId greater than or equals to DEFAULT_MEDICAL_ID
        defaultSurveyDataShouldBeFound("medicalId.greaterOrEqualThan=" + DEFAULT_MEDICAL_ID);

        // Get all the surveyDataList where medicalId greater than or equals to UPDATED_MEDICAL_ID
        defaultSurveyDataShouldNotBeFound("medicalId.greaterOrEqualThan=" + UPDATED_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void getAllSurveyDataByMedicalIdIsLessThanSomething() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        // Get all the surveyDataList where medicalId less than or equals to DEFAULT_MEDICAL_ID
        defaultSurveyDataShouldNotBeFound("medicalId.lessThan=" + DEFAULT_MEDICAL_ID);

        // Get all the surveyDataList where medicalId less than or equals to UPDATED_MEDICAL_ID
        defaultSurveyDataShouldBeFound("medicalId.lessThan=" + UPDATED_MEDICAL_ID);
    }


    @Test
    @Transactional
    public void getAllSurveyDataByUserExtraIsEqualToSomething() throws Exception {
        // Initialize the database
        UserExtra userExtra = UserExtraResourceIntTest.createEntity(em);
        em.persist(userExtra);
        em.flush();
        surveyData.setUserExtra(userExtra);
        surveyDataRepository.saveAndFlush(surveyData);
        Long userExtraId = userExtra.getId();

        // Get all the surveyDataList where userExtra equals to userExtraId
        defaultSurveyDataShouldBeFound("userExtraId.equals=" + userExtraId);

        // Get all the surveyDataList where userExtra equals to userExtraId + 1
        defaultSurveyDataShouldNotBeFound("userExtraId.equals=" + (userExtraId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSurveyDataShouldBeFound(String filter) throws Exception {
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(surveyData.getId().intValue())))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].surveyType").value(hasItem(DEFAULT_SURVEY_TYPE.toString())))
            .andExpect(jsonPath("$.[*].assesmentData").value(hasItem(DEFAULT_ASSESMENT_DATA.toString())))
            .andExpect(jsonPath("$.[*].scoringResult").value(hasItem(DEFAULT_SCORING_RESULT.intValue())))
            .andExpect(jsonPath("$.[*].createdTime").value(hasItem(DEFAULT_CREATED_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].additionalInfo").value(hasItem(DEFAULT_ADDITIONAL_INFO.toString())))
            .andExpect(jsonPath("$.[*].surveyId").value(hasItem(DEFAULT_SURVEY_ID.intValue())))
            .andExpect(jsonPath("$.[*].medicalId").value(hasItem(DEFAULT_MEDICAL_ID.intValue())));

        // Check, that the count call also returns 1
        restSurveyDataMockMvc.perform(get("/api/survey-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSurveyDataShouldNotBeFound(String filter) throws Exception {
        restSurveyDataMockMvc.perform(get("/api/survey-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSurveyDataMockMvc.perform(get("/api/survey-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSurveyData() throws Exception {
        // Get the surveyData
        restSurveyDataMockMvc.perform(get("/api/survey-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        int databaseSizeBeforeUpdate = surveyDataRepository.findAll().size();

        // Update the surveyData
        SurveyData updatedSurveyData = surveyDataRepository.findById(surveyData.getId()).get();
        // Disconnect from session so that the updates on updatedSurveyData are not directly saved in db
        em.detach(updatedSurveyData);
        updatedSurveyData
            .identifier(UPDATED_IDENTIFIER)
            .surveyType(UPDATED_SURVEY_TYPE)
            .assesmentData(UPDATED_ASSESMENT_DATA)
            .scoringResult(UPDATED_SCORING_RESULT)
            .createdTime(UPDATED_CREATED_TIME)
            .endTime(UPDATED_END_TIME)
            .additionalInfo(UPDATED_ADDITIONAL_INFO)
            .surveyId(UPDATED_SURVEY_ID)
            .medicalId(UPDATED_MEDICAL_ID);
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(updatedSurveyData);

        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isOk());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeUpdate);
        SurveyData testSurveyData = surveyDataList.get(surveyDataList.size() - 1);
        assertThat(testSurveyData.getIdentifier()).isEqualTo(UPDATED_IDENTIFIER);
        assertThat(testSurveyData.getSurveyType()).isEqualTo(UPDATED_SURVEY_TYPE);
        assertThat(testSurveyData.getAssesmentData()).isEqualTo(UPDATED_ASSESMENT_DATA);
        assertThat(testSurveyData.getScoringResult()).isEqualTo(UPDATED_SCORING_RESULT);
        assertThat(testSurveyData.getCreatedTime()).isEqualTo(UPDATED_CREATED_TIME);
        assertThat(testSurveyData.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testSurveyData.getAdditionalInfo()).isEqualTo(UPDATED_ADDITIONAL_INFO);
        assertThat(testSurveyData.getSurveyId()).isEqualTo(UPDATED_SURVEY_ID);
        assertThat(testSurveyData.getMedicalId()).isEqualTo(UPDATED_MEDICAL_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingSurveyData() throws Exception {
        int databaseSizeBeforeUpdate = surveyDataRepository.findAll().size();

        // Create the SurveyData
        SurveyDataDTO surveyDataDTO = surveyDataMapper.toDto(surveyData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSurveyDataMockMvc.perform(put("/api/survey-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(surveyDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SurveyData in the database
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSurveyData() throws Exception {
        // Initialize the database
        surveyDataRepository.saveAndFlush(surveyData);

        int databaseSizeBeforeDelete = surveyDataRepository.findAll().size();

        // Delete the surveyData
        restSurveyDataMockMvc.perform(delete("/api/survey-data/{id}", surveyData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SurveyData> surveyDataList = surveyDataRepository.findAll();
        assertThat(surveyDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SurveyData.class);
        SurveyData surveyData1 = new SurveyData();
        surveyData1.setId(1L);
        SurveyData surveyData2 = new SurveyData();
        surveyData2.setId(surveyData1.getId());
        assertThat(surveyData1).isEqualTo(surveyData2);
        surveyData2.setId(2L);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
        surveyData1.setId(null);
        assertThat(surveyData1).isNotEqualTo(surveyData2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SurveyDataDTO.class);
        SurveyDataDTO surveyDataDTO1 = new SurveyDataDTO();
        surveyDataDTO1.setId(1L);
        SurveyDataDTO surveyDataDTO2 = new SurveyDataDTO();
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
        surveyDataDTO2.setId(surveyDataDTO1.getId());
        assertThat(surveyDataDTO1).isEqualTo(surveyDataDTO2);
        surveyDataDTO2.setId(2L);
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
        surveyDataDTO1.setId(null);
        assertThat(surveyDataDTO1).isNotEqualTo(surveyDataDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(surveyDataMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(surveyDataMapper.fromId(null)).isNull();
    }
}
