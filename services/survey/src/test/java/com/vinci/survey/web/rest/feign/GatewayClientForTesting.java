package com.vinci.survey.web.rest.feign;

import com.vinci.survey.web.rest.feign.client.GatewayClient;

public class GatewayClientForTesting implements GatewayClient {
    @Override
    public Long validate(Long userId) {
        return 3l;
    }

    @Override
    public Long getSurveyDevice(Long userId) {
        return 3l;
    }

    @Override
    public Long getSurveyDeviceV_1_0_0(Long userId) {
        return 3l;
    }
}
