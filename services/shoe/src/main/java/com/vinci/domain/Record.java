package com.vinci.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Record.
 */
@Entity
@Table(name = "record")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "device_uuid", nullable = false)
    private String deviceUUID;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private Instant timestamp;

    @Column(name = "payload", nullable = false)
    private String payload;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public Record deviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
        return this;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Record timestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getPayload() {
        return payload;
    }

    public Record payload(String payload) {
        this.payload = payload;
        return this;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Record record = (Record) o;
        if (record.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), record.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Record{" +
            "id=" + getId() +
            ", deviceId='" + getDeviceUUID() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", payload='" + getPayload() + "'" +
            "}";
    }


    public static final class RecordBuilder {
        private String deviceUUID;
        private Instant timestamp;
        private String payload;

        private RecordBuilder() {
        }

        public static RecordBuilder aRecord() {
            return new RecordBuilder();
        }

        public RecordBuilder withDeviceUUID(String deviceUUID) {
            this.deviceUUID = deviceUUID;
            return this;
        }

        public RecordBuilder withTimestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public RecordBuilder withPayload(String payload) {
            this.payload = payload;
            return this;
        }

        public Record build() {
            Record record = new Record();
            record.setDeviceUUID(deviceUUID);
            record.setTimestamp(timestamp);
            record.setPayload(payload);
            return record;
        }
    }
}
