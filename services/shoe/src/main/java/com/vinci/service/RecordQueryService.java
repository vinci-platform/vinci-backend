package com.vinci.service;

import com.vinci.domain.Record;
import com.vinci.domain.Record_;
import com.vinci.repository.RecordRepository;
import com.vinci.service.dto.RecordCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for Record entities in the database.
 * The main input is a {@link RecordCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Record} or a {@link Page} of {@link Record} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecordQueryService extends QueryService<Record> {

    private final Logger log = LoggerFactory.getLogger(RecordQueryService.class);

    private final RecordRepository recordRepository;

    public RecordQueryService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    /**
     * Return a {@link List} of {@link Record} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Record> findByCriteria(RecordCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Record} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Record> findByCriteria(RecordCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecordCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.count(specification);
    }

    /**
     * Function to convert RecordCriteria to a {@link Specification}
     */
    private Specification<Record> createSpecification(RecordCriteria criteria) {
        Specification<Record> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Record_.id));
            }
            if (criteria.getDeviceUUID() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeviceUUID(), Record_.deviceUUID));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildSpecification(criteria.getTimestamp(), Record_.timestamp));
            }
        }
        return specification;
    }
}
