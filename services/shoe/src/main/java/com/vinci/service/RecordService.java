package com.vinci.service;

import com.vinci.domain.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Record.
 */
public interface RecordService {

    /**
     * Save a record.
     *
     * @param record the entity to save
     * @return the persisted entity
     */
    Record save(Record record);


    /**
     * Save a list of records.
     *
     * @param records the list of entities to save
     * @return the list of persisted entities
     */
    List<Record> saveAll(List<Record> records);

    /**
     * Get all the records.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Record> findAll(Pageable pageable);


    /**
     * Get the "id" record.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Record> findOne(Long id);

    /**
     * Delete the "id" record.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
