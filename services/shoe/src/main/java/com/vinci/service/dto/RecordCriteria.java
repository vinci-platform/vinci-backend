package com.vinci.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the Record entity. This class is used in RecordResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /records?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RecordCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter deviceUUID;

    private InstantFilter timestamp;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(StringFilter deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    public InstantFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(InstantFilter timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RecordCriteria that = (RecordCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(deviceUUID, that.deviceUUID) &&
            Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        deviceUUID,
        timestamp
        );
    }

    @Override
    public String toString() {
        return "RecordCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (deviceUUID != null ? "deviceUUID=" + deviceUUID + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
            "}";
    }

}
