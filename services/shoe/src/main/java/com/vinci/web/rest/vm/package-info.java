/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.web.rest.vm;
