package com.vinci.web.rest.util;

import com.google.common.collect.Lists;
import com.vinci.domain.Record;
import com.vinci.web.rest.beans.ImportRecordBean;

import java.util.List;

public class RecordResourceUtil {

    public static List<Record> convertToRecordsList(List<ImportRecordBean> payload) {
        List<Record> records = Lists.newLinkedList();
        payload.forEach(record -> records.add(
            Record.RecordBuilder.aRecord()
                .withDeviceUUID(record.getDeviceUUID())
                .withTimestamp(record.getTimestamp())
                .withPayload(record.getPayload())
                .build()
        ));
        return records;
    }
}
