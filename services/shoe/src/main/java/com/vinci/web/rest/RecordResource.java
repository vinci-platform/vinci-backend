package com.vinci.web.rest;

import com.vinci.service.RecordQueryService;
import com.vinci.service.RecordService;
import com.vinci.domain.Record;
import com.vinci.service.RecordQueryService;
import com.vinci.service.RecordService;
import com.vinci.service.dto.RecordCriteria;
import com.vinci.web.rest.beans.ImportRecordBean;
import com.vinci.web.rest.errors.BadRequestAlertException;
import com.vinci.web.rest.errors.InternalServerErrorException;
import com.vinci.web.rest.util.HeaderUtil;
import com.vinci.web.rest.util.PaginationUtil;
import com.vinci.web.rest.util.RecordResourceUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Record.
 */
@RestController
@RequestMapping("/api")
public class RecordResource {

    private final Logger log = LoggerFactory.getLogger(RecordResource.class);

    private static final String ENTITY_NAME = "shoeRecord";

    private final RecordService recordService;

    private final RecordQueryService recordQueryService;

    public RecordResource(RecordService recordService, RecordQueryService recordQueryService) {
        this.recordService = recordService;
        this.recordQueryService = recordQueryService;
    }

    /**
     * POST  /records : Create a new record.
     *
     * @param record the record to create
     * @return the ResponseEntity with status 201 (Created) and with body the new record, or with status 400 (Bad Request) if the record has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/records")
    public ResponseEntity<Record> createRecord(@Valid @RequestBody Record record) throws URISyntaxException {
        log.debug("REST request to save Record : {}", record);
        if (record.getId() != null) {
            throw new BadRequestAlertException("A new record cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Record result = recordService.save(record);
        return ResponseEntity.created(new URI("/api/records/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /import : Import a list of records.
     *
     * @param records the list of records to import
     * @return the ResponseEntity with status 201 (Created) and with body the success message, or with status 400 (Bad Request) if any errors were encountered
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/import")
    public ResponseEntity<String> createRecord(@Valid @RequestBody List<ImportRecordBean> records) throws URISyntaxException {
        log.debug("REST request to import a list of Records : {}", records);
        List<Record> result = recordService.saveAll(RecordResourceUtil.convertToRecordsList(records));
        if (result == null || result.size() != records.size()) {
            throw new InternalServerErrorException("We have encountered an error while importing the list of entities.");
        }
        return ResponseEntity.created(new URI("/api/import/"))
            .headers(HeaderUtil.createEntityArrayImportAlert(ENTITY_NAME, records.size()))
            .body("Successfully imported " + records.size() + " entities");
    }

    /**
     * PUT  /records : Updates an existing record.
     *
     * @param record the record to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated record,
     * or with status 400 (Bad Request) if the record is not valid,
     * or with status 500 (Internal Server Error) if the record couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/records")
    public ResponseEntity<Record> updateRecord(@Valid @RequestBody Record record) throws URISyntaxException {
        log.debug("REST request to update Record : {}", record);
        if (record.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Record result = recordService.save(record);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, record.getId().toString()))
            .body(result);
    }

    /**
     * GET  /records : get all the records.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of records in body
     */
    @GetMapping("/records")
    public ResponseEntity<List<Record>> getAllRecords(RecordCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Records by criteria: {}", criteria);
        Page<Record> page = recordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/records");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /records/count : count all the records.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/records/count")
    public ResponseEntity<Long> countRecords(RecordCriteria criteria) {
        log.debug("REST request to count Records by criteria: {}", criteria);
        return ResponseEntity.ok().body(recordQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /records/:id : get the "id" record.
     *
     * @param id the id of the record to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the record, or with status 404 (Not Found)
     */
    @GetMapping("/records/{id}")
    public ResponseEntity<Record> getRecord(@PathVariable Long id) {
        log.debug("REST request to get Record : {}", id);
        Optional<Record> record = recordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(record);
    }

    /**
     * DELETE  /records/:id : delete the "id" record.
     *
     * @param id the id of the record to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/records/{id}")
    public ResponseEntity<Void> deleteRecord(@PathVariable Long id) {
        log.debug("REST request to delete Record : {}", id);
        recordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
