package com.vinci.web.rest.beans;

import java.time.Instant;

public class ImportRecordBean {
    private String deviceUUID;
    private String payload;
    private String timestamp;

    public ImportRecordBean(String deviceUUID, String payload, String timestamp) {
        this.deviceUUID = deviceUUID;
        this.payload = payload;
        this.timestamp = timestamp;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Instant getTimestamp() {
        return Instant.parse(timestamp);
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
