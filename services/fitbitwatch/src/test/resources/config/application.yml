# ===================================================================
# Spring Boot configuration.
#
# This configuration is used for unit/integration tests.
#
# More information on profiles: https://www.jhipster.tech/profiles/
# More information on configuration properties: https://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

eureka:
  client:
    enabled: false
  instance:
    appname: fitbitwatch
    instanceId: fitbitwatch:${spring.application.instance-id:${random.value}}

spring:
  profiles:
    # Uncomment the following line to enable tests against production database type rather than H2, using Testcontainers
    #active: testcontainers
  application:
    name: fitbitwatch
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:h2:mem:fitbitwatch;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    name:
    username:
    password:
    hikari:
      auto-commit: false
  jackson:
    serialization:
      write-durations-as-timestamps: false
  jpa:
    database-platform: io.github.jhipster.domain.util.FixedH2Dialect
    open-in-view: false
    show-sql: false
    hibernate:
      ddl-auto: none
      naming:
        physical-strategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
        implicit-strategy: org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy
    properties:
      hibernate.id.new_generator_mappings: true
      hibernate.connection.provider_disables_autocommit: true
      hibernate.cache.use_second_level_cache: false
      hibernate.cache.use_query_cache: false
      hibernate.generate_statistics: false
      hibernate.hbm2ddl.auto: validate
      hibernate.jdbc.time_zone: UTC
  liquibase:
    contexts: test
  mail:
    host: localhost
  main:
    allow-bean-definition-overriding: true
  messages:
    basename: i18n/messages
  task:
    execution:
      thread-name-prefix: fitbitwatch-task-
      pool:
        core-size: 1
        max-size: 50
        queue-capacity: 10000
    scheduling:
      thread-name-prefix: fitbitwatch-scheduling-
      pool:
        size: 1
  thymeleaf:
    mode: HTML

server:
  port: 10344
  address: localhost

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: https://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
  clientApp:
    name: 'fitbitwatchApp'
  logging:
    # To test json console appender
    use-json-format: false
    logstash:
      enabled: false
      host: localhost
      port: 5000
      queue-size: 512
  security:
    authentication:
      jwt:
        # This token must be encoded using Base64 (you can type `echo 'secret-key'|base64` on your command line)
        base64-secret: YWNiNDMwMGM1ODU4MjU5Y2RlYTBkN2U1NmY0MmYxYzk5ZjIwZjAzNGJhZGRiZmZkYTNjMTcwMTAyNWRmZDA4NWQzMDc1YjRiMGQwYjVlMmMwZmU3YzM1MmRhNDNmMGYyNmJmODgyOGVmN2E3MWQxYmE2MDBkYWI2MzZmN2E4NzQ=
        # Token is valid 24 hours
        token-validity-in-seconds: 86400
  metrics:
    logs: # Reports metrics in the logs
      enabled: true
      report-frequency: 60 # in seconds
kafka:
  bootstrap-servers: localhost:9092
  group-id: vinci-application
  topic:
    fitbit-watch-data:
      num-partitions: 3
      replicas: 1
    fitbit-watch-intraday-data:
      num-partitions: 3
      replicas: 1
    fitbit-device-update:
      num-partitions: 3
      replicas: 1

# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:
fitbit:
  api:
    resource:
      sleep: https://api.fitbit.com/1.2/user/
      activities: https://api.fitbit.com/1/user/
      token: https://api.fitbit.com/oauth2/token
      auth: https://www.fitbit.com/oauth2/authorize
      subscribe: https://api.fitbit.com/1/user/
    notification-verification-code: only-works-for-HTTPS-so-add-it-with-configuration
    redirect-url: http://localhost:8080/api/fitbit-auth/code
    client-id: <add-fitbit-client-id>
    secret-id: <add-fitbit-secret-id>
