package com.upb.vinci.web.rest;

import com.upb.vinci.web.rest.beans.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.*;

public abstract class SetupController {

    static protected final Long DEFAULT_USER_DTO_ID = 1L;
    static protected final Long DEFAULT_USER_DTO_USER_EXTRA_ID = 1L;
    static protected final String DEFAULT_USER_DTO_LOGIN = "user";
    static protected final String DEFAULT_USER_DTO_FIRSTNAME = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_GENDER = "MALE";
    static protected final String DEFAULT_USER_DTO_LASTNAME = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_ADDRESS = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_PHONE = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_EMAIL = "AAAAAAAAAAAAAAAA";
    static protected final boolean DEFAULT_USER_DTO_ACTIVATED = true;
    static protected final String DEFAULT_USER_DTO_LANG_KEY = "en";
    static protected final String DEFAULT_USER_DTO_CREATED_BY = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_LAST_MODIFIED_BY = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_ROLE = "ROLE_USER,ROLE_PATIENT";
    static protected final Set<String> DEFAULT_USER_DTO_AUTHORITIES = new HashSet<String>() {{
        add("ROLE_USER");
        add("ROLE_PACIENT");
    }};
    static protected final String DEFAULT_USER_DTO_UUID = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_MARITAL_STATUS = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_EDUCATION = "AAAAAAAAAAAAAAAA";
    static protected final String DEFAULT_USER_DTO_FRIENDS = "AAAAAAAAAAAAAAAA";

    static protected final Set<String> PACIENT_USER_DTO_AUTHORITIES = new HashSet<String>() {{
        add("ROLE_USER");
        add("ROLE_PACIENT");
    }};
    static protected final Set<String> FAMILY_USER_DTO_AUTHORITIES = new HashSet<String>() {{
        add("ROLE_USER");
        add("ROLE_FAMILY");
    }};
    static protected final Set<String> ORGANIZATION_USER_DTO_AUTHORITIES = new HashSet<String>() {{
        add("ROLE_USER");
        add("ROLE_ORGANIZATION");
    }};
    static protected final Set<String> ADMIN_USER_DTO_AUTHORITIES = new HashSet<String>() {{
        add("ROLE_USER");
        add("ROLE_ADMIN");
    }};

    @Autowired
    protected EntityManager em;


    protected UserDTO userDTO;

    /**
     * Sets up default values to userDTO. All device sets are empty hash sets.
     */
    protected void setupUserDto() {
        userDTO = new UserDTO();
        userDTO.setId(DEFAULT_USER_DTO_ID);
        userDTO.setUserExtraId(DEFAULT_USER_DTO_USER_EXTRA_ID);
        userDTO.setLogin(DEFAULT_USER_DTO_LOGIN);
        userDTO.setFirstName(DEFAULT_USER_DTO_FIRSTNAME);
        userDTO.setGender(DEFAULT_USER_DTO_GENDER);
        userDTO.setLastName(DEFAULT_USER_DTO_LASTNAME);
        userDTO.setAddress(DEFAULT_USER_DTO_ADDRESS);
        userDTO.setPhone(DEFAULT_USER_DTO_PHONE);
        userDTO.setEmail(DEFAULT_USER_DTO_EMAIL);
        userDTO.setActivated(DEFAULT_USER_DTO_ACTIVATED);
        userDTO.setLangKey(DEFAULT_USER_DTO_LANG_KEY);
        userDTO.setCreatedBy(DEFAULT_USER_DTO_CREATED_BY);
        userDTO.setLastModifiedBy(DEFAULT_USER_DTO_LAST_MODIFIED_BY);
        userDTO.setRole(DEFAULT_USER_DTO_ROLE);
        userDTO.setUuid(DEFAULT_USER_DTO_UUID);
        userDTO.setMaritalStatus(DEFAULT_USER_DTO_MARITAL_STATUS);
        userDTO.setEducation(DEFAULT_USER_DTO_EDUCATION);
        userDTO.setFriends(DEFAULT_USER_DTO_FRIENDS);

        userDTO.setAuthorities(DEFAULT_USER_DTO_AUTHORITIES);
        userDTO.setPersonalDevices(new HashSet<>());
        userDTO.setPatientsOfFamily(new HashSet<>());
        userDTO.setPatientsOfOrganization(new HashSet<>());
    }

    protected Optional<UserDTO> getUserDTOOptional() {
        return Optional.of(userDTO);
    }

}
