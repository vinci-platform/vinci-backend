package com.upb.vinci.web.rest;

import com.upb.vinci.FitbitwatchApp;
import com.upb.vinci.domain.Tokens;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.repository.TokensRepository;
import com.upb.vinci.service.DeviceService;
import com.upb.vinci.service.webclient.FitbitWebClient;
import com.upb.vinci.service.TokensService;
import com.upb.vinci.service.UserService;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.mapper.TokensMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import com.upb.vinci.web.rest.feign.client.GatewayClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TokensResource} REST controller.
 */
@SpringBootTest(classes = FitbitwatchApp.class)
@AutoConfigureMockMvc
@WithMockUser
@RunWith(SpringRunner.class)
public class TokensResourceIT extends SetupController {

    private static final String DEFAULT_ACCESS_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_ACCESS_TOKEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_EXPIRES_IN = 1;
    private static final Integer UPDATED_EXPIRES_IN = 2;

    private static final String DEFAULT_REFRESH_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_REFRESH_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_SCOPE = "AAAAAAAAAA";
    private static final String UPDATED_SCOPE = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_CREATED = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_CREATED = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDateTime DEFAULT_LAST_SYNC = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_LAST_SYNC = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDateTime DEFAULT_LAST_SYNC_INTRADAY = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_LAST_SYNC_INTRADAY = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_USER_EXTRA_ID = 1l;
    private static final Long UPDATED_USER_EXTRA_ID = 2l;

    @Autowired
    private TokensRepository tokensRepository;

    @Autowired
    private TokensMapper tokensMapper;


//    @Autowired
    private TokensService tokensService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private MockMvc restTokensMockMvc;

    @Autowired
    private FitbitWebClient fitbitWebClient;

    @Autowired
    private FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;

    @Mock
    private DeviceService deviceServiceMock;

    @Mock
    private GatewayClient gatewayClientMock;

    private UserService userService;

    private Tokens token;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        userService = new UserService(gatewayClientMock);
        tokensService = new TokensService(fitbitWebClient, tokensRepository, tokensMapper, deviceServiceMock, userService, fitbitWebClientExceptionHandler);
        final TokensResource tokensResource = new TokensResource(tokensService);
        this.restTokensMockMvc = MockMvcBuilders.standaloneSetup(tokensResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create a FormattingConversionService which use ISO date format, instead of the localized one.
     * @return the FormattingConversionService
     */
    public static FormattingConversionService createFormattingConversionService() {
        DefaultFormattingConversionService dfcs = new DefaultFormattingConversionService ();
        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
        registrar.setUseIsoFormat(true);
        registrar.registerFormatters(dfcs);
        return dfcs;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tokens createEntity(EntityManager em) {
        return new Tokens()
            .access_token(DEFAULT_ACCESS_TOKEN)
            .expires_in(DEFAULT_EXPIRES_IN)
            .refresh_token(DEFAULT_REFRESH_TOKEN)
            .scope(DEFAULT_SCOPE)
            .token_type(DEFAULT_TOKEN_TYPE)
            .user_id(DEFAULT_USER_ID)
            .created(DEFAULT_CREATED)
            .last_sync(DEFAULT_LAST_SYNC)
            .last_sync_intraday(DEFAULT_LAST_SYNC_INTRADAY)
            .userExtraId(DEFAULT_USER_EXTRA_ID);
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tokens createUpdatedEntity(EntityManager em) {
        return new Tokens()
            .access_token(UPDATED_ACCESS_TOKEN)
            .expires_in(UPDATED_EXPIRES_IN)
            .refresh_token(UPDATED_REFRESH_TOKEN)
            .scope(UPDATED_SCOPE)
            .token_type(UPDATED_TOKEN_TYPE)
            .user_id(UPDATED_USER_ID)
            .created(UPDATED_CREATED)
            .last_sync(UPDATED_LAST_SYNC)
            .last_sync_intraday(UPDATED_LAST_SYNC_INTRADAY)
            .userExtraId(UPDATED_USER_EXTRA_ID);
    }

    private void initTest() {
        setupUserDto();
        token = createEntity(em);
    }

    @Test
    @Transactional
    public void createTokens() throws Exception {
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        createTokensRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createTokens_ForSomeoneElse_AsAdmin() throws Exception {
        userDTO.setAuthorities(ADMIN_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        token.setUserExtraId(-1L);

        createTokensRestCall_IsCreated();
    }

    @Test
    @Transactional
    public void createTokens_SomeoneElsesUserExtraId() throws Exception {
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        token.setUserExtraId(-1L);

        createTokensRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    public void createTokens_WithExistingId() throws Exception {
        // Create the Tokens with an existing ID
        token.setId(1L);

        createTokensRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createTokens_UserIdNull() throws Exception {
        // Create the Tokens with an existing ID
        token.setUser_id(null);

        createTokensRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createTokens_WithExistingUserExtraId() throws Exception {
        // Create the Tokens with an existing ID
        Tokens savedToken = tokensRepository.save(new Tokens().userExtraId(token.getUserExtraId()));

        createTokensRestCall_IsBadRequest();
    }

    private void createTokensRestCall_IsCreated() throws Exception {
        // Create the Tokens
        TokensDTO tokensDTO = tokensMapper.toDto(token);
        int databaseSizeBeforeTest = tokensRepository.findAll().size();
        restTokensMockMvc.perform(post("/api/tokens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tokensDTO)))
            .andExpect(status().isCreated());

        // Validate the Tokens in the database
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeTest + 1);
        Tokens testTokens = tokensList.get(tokensList.size() - 1);
        assertThat(testTokens.getAccess_token()).isEqualTo(DEFAULT_ACCESS_TOKEN);
        assertThat(testTokens.getExpires_in()).isEqualTo(DEFAULT_EXPIRES_IN);
        assertThat(testTokens.getRefresh_token()).isEqualTo(DEFAULT_REFRESH_TOKEN);
        assertThat(testTokens.getScope()).isEqualTo(DEFAULT_SCOPE);
        assertThat(testTokens.getToken_type()).isEqualTo(DEFAULT_TOKEN_TYPE);
        assertThat(testTokens.getUser_id()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testTokens.getUserExtraId()).isEqualTo(token.getUserExtraId());
    }

    private void createTokensRestCall_IsBadRequest() throws Exception {
        TokensDTO tokensDTO = tokensMapper.toDto(token);
        int databaseSizeBeforeTest = tokensRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restTokensMockMvc.perform(post("/api/tokens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tokensDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tokens in the database
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeTest);
    }

    private void createTokensRestCall_IsUnauthorized() throws Exception {
        TokensDTO tokensDTO = tokensMapper.toDto(token);
        int databaseSizeBeforeTest = tokensRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restTokensMockMvc.perform(post("/api/tokens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tokensDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the Tokens in the database
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateTokens() throws Exception {
        // Initialize the database
        tokensRepository.saveAndFlush(token);

        updateTokensRestCall_IsOk();
    }

    @Test
    @Transactional
    public void updateTokens_NonExistingTokenInDb() throws Exception {
        updateTokensRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void updateTokens_UserExtraIdNull() throws Exception {
        token.setUserExtraId(null);

        updateTokensRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void updateTokens_UserIdNull() throws Exception {
        token.setUser_id(null);

        updateTokensRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void updateTokens_IdNull() throws Exception {
        token.setId(null);

        updateTokensRestCall_IsBadRequest();
    }

    private void updateTokensRestCall_IsOk() throws Exception {
        // Update the tokens
        Tokens updatedTokens = tokensRepository.findById(token.getId()).get();
        // Disconnect from session so that the updates on updatedTokens are not directly saved in db
        em.detach(updatedTokens);
        setUpdateFields(updatedTokens);
        TokensDTO tokensDTO = tokensMapper.toDto(updatedTokens);

        int databaseSizeBeforeUpdate = tokensRepository.findAll().size();
        restTokensMockMvc.perform(put("/api/tokens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tokensDTO)))
            .andExpect(status().isOk());

        // Validate the Tokens in the database
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeUpdate);
        Tokens testTokens = tokensList.get(tokensList.size() - 1);
        assertThat(testTokens.getAccess_token()).isEqualTo(UPDATED_ACCESS_TOKEN);
        assertThat(testTokens.getExpires_in()).isEqualTo(UPDATED_EXPIRES_IN);
        assertThat(testTokens.getRefresh_token()).isEqualTo(UPDATED_REFRESH_TOKEN);
        assertThat(testTokens.getScope()).isEqualTo(UPDATED_SCOPE);
        assertThat(testTokens.getToken_type()).isEqualTo(UPDATED_TOKEN_TYPE);
        assertThat(testTokens.getUser_id()).isEqualTo(UPDATED_USER_ID);
        assertThat(testTokens.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testTokens.getLast_sync()).isEqualTo(UPDATED_LAST_SYNC);
        assertThat(testTokens.getLast_sync_intraday()).isEqualTo(UPDATED_LAST_SYNC_INTRADAY);
        assertThat(testTokens.getUserExtraId()).isEqualTo(UPDATED_USER_EXTRA_ID);
    }

    private void updateTokensRestCall_IsBadRequest() throws Exception {
        // Create the Tokens
        TokensDTO tokensDTO = tokensMapper.toDto(token);
        int databaseSizeBeforeUpdate = tokensRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTokensMockMvc.perform(put("/api/tokens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tokensDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tokens in the database
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeUpdate);
    }

    private void setUpdateFields(Tokens updatedTokens) {
        updatedTokens
            .access_token(UPDATED_ACCESS_TOKEN)
            .expires_in(UPDATED_EXPIRES_IN)
            .refresh_token(UPDATED_REFRESH_TOKEN)
            .scope(UPDATED_SCOPE)
            .token_type(UPDATED_TOKEN_TYPE)
            .user_id(UPDATED_USER_ID)
            .created(UPDATED_CREATED)
            .last_sync(UPDATED_LAST_SYNC)
            .last_sync_intraday(UPDATED_LAST_SYNC_INTRADAY)
            .userExtraId(UPDATED_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    public void getAllTokens() throws Exception {
        // Initialize the database
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        tokensRepository.saveAndFlush(token);

        getAllTokensRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllTokens_AsAdmin() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ADMIN_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        token.setUserExtraId(10000L);
        tokensRepository.saveAndFlush(token);

        getAllTokensRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getAllTokens_NoItems() throws Exception {
        // Initialize the database
//        tokensRepository.saveAndFlush(token);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        getAllTokensRestCall_EmptyList();
    }

    @Test
    @Transactional
    public void getAllTokens_DifferentUserExtraId() throws Exception {
        // Initialize the database
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        token.setUserExtraId(10000L);
        tokensRepository.saveAndFlush(token);

        getAllTokensRestCall_EmptyList();
    }

    private void getAllTokensRestCall_EmptyList() throws Exception {
        // Get all the tokensList
        restTokensMockMvc.perform(get("/api/tokens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    private void getAllTokensRestCall_IsOk() throws Exception {
        // Get all the tokensList
        restTokensMockMvc.perform(get("/api/tokens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(token.getId().intValue())))
            .andExpect(jsonPath("$.[*].access_token").value(hasItem(DEFAULT_ACCESS_TOKEN)))
            .andExpect(jsonPath("$.[*].expires_in").value(hasItem(DEFAULT_EXPIRES_IN)))
            .andExpect(jsonPath("$.[*].refresh_token").value(hasItem(DEFAULT_REFRESH_TOKEN)))
            .andExpect(jsonPath("$.[*].scope").value(hasItem(DEFAULT_SCOPE)))
            .andExpect(jsonPath("$.[*].token_type").value(hasItem(DEFAULT_TOKEN_TYPE)))
            .andExpect(jsonPath("$.[*].user_id").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(token.getUserExtraId().intValue())));
    }

    @Test
    @Transactional
    public void getTokensById() throws Exception {
        // Initialize the database
        tokensRepository.saveAndFlush(token);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());

        getTokenByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getTokensById_AsAdmin() throws Exception {
        // Initialize the database
        userDTO.setAuthorities(ADMIN_USER_DTO_AUTHORITIES);
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        tokensRepository.saveAndFlush(token);
        token.setUserExtraId(1000L);

        getTokenByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    public void getTokensById_BadUserExtraId() throws Exception {
        // Initialize the database
        when(gatewayClientMock.getUserWithAuthorities()).thenReturn(getUserDTOOptional());
        tokensRepository.saveAndFlush(token);
        token.setUserExtraId(10000L);

        getTokenByIdRestCall_IsUnauthorized();
    }

    private void getTokenByIdRestCall_IsOk() throws Exception {
        // Get the tokens
        restTokensMockMvc.perform(get("/api/tokens/{id}", token.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(token.getId().intValue()))
            .andExpect(jsonPath("$.access_token").value(DEFAULT_ACCESS_TOKEN))
            .andExpect(jsonPath("$.expires_in").value(DEFAULT_EXPIRES_IN))
            .andExpect(jsonPath("$.refresh_token").value(DEFAULT_REFRESH_TOKEN))
            .andExpect(jsonPath("$.scope").value(DEFAULT_SCOPE))
            .andExpect(jsonPath("$.token_type").value(DEFAULT_TOKEN_TYPE))
            .andExpect(jsonPath("$.user_id").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.userExtraId").value(token.getUserExtraId().intValue()));
    }

    private void getTokenByIdRestCall_IsUnauthorized() throws Exception {
        // Get the tokens
        restTokensMockMvc.perform(get("/api/tokens/{id}", token.getId()))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void deleteTokens() throws Exception {
        // Initialize the database
        tokensRepository.saveAndFlush(token);

        int databaseSizeBeforeDelete = tokensRepository.findAll().size();

        // Delete the tokens
        restTokensMockMvc.perform(delete("/api/tokens/{id}", token.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tokens> tokensList = tokensRepository.findAll();
        assertThat(tokensList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
