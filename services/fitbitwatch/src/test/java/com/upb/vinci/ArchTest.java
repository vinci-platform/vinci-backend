package com.upb.vinci;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    //todo: check why this test fails
//    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.upb.vinci");

        noClasses()
            .that()
                .resideInAnyPackage("com.upb.vinci.service..")
            .or()
                .resideInAnyPackage("com.upb.vinci.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.upb.vinci.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
