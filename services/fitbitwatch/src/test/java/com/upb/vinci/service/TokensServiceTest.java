package com.upb.vinci.service;

import com.upb.vinci.domain.Tokens;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.repository.TokensRepository;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.mapper.TokensMapper;
import com.upb.vinci.service.webclient.FitbitWebClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest()
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
class TokensServiceTest {

    private static final String DEFAULT_ACCESS_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_ACCESS_TOKEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_EXPIRES_IN = 1;
    private static final Integer UPDATED_EXPIRES_IN = 2;

    private static final String DEFAULT_REFRESH_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_REFRESH_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_SCOPE = "AAAAAAAAAA";
    private static final String UPDATED_SCOPE = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_CREATED = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_CREATED = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDateTime DEFAULT_LAST_SYNC = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_LAST_SYNC = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDateTime DEFAULT_LAST_SYNC_INTRADAY = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_LAST_SYNC_INTRADAY = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_USER_EXTRA_ID = 1l;
    private static final Long UPDATED_USER_EXTRA_ID = 2l;

    @Mock
    DeviceService deviceServiceMock;

    @Mock
    FitbitWebClient fitbitWebClientMock;

    @Mock
    UserService userServiceMock;

    TokensService tokensService;

    @Autowired
    FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;

    @Autowired
    TokensRepository tokensRepository;

    @Autowired
    TokensMapper tokensMapper;

    @Autowired
    private EntityManager em;

    Tokens token;
    Tokens differentToken;

    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
        tokensService = new TokensService(fitbitWebClientMock,tokensRepository,tokensMapper,deviceServiceMock,userServiceMock,fitbitWebClientExceptionHandler);

        initTest();
    }

    void initTest(){
        token = setupDefaultToken();
        differentToken = setupUpdateToken();
    }

    private Tokens setupUpdateToken() {
        return new Tokens()
            .access_token(UPDATED_ACCESS_TOKEN)
            .expires_in(UPDATED_EXPIRES_IN)
            .refresh_token(UPDATED_REFRESH_TOKEN)
            .scope(UPDATED_SCOPE)
            .token_type(UPDATED_TOKEN_TYPE)
            .user_id(UPDATED_USER_ID)
            .created(UPDATED_CREATED)
            .last_sync(UPDATED_LAST_SYNC)
            .last_sync_intraday(UPDATED_LAST_SYNC_INTRADAY)
            .userExtraId(null);
    }

    private Tokens setupDefaultToken() {
        return new Tokens()
            .access_token(DEFAULT_ACCESS_TOKEN)
            .expires_in(DEFAULT_EXPIRES_IN)
            .refresh_token(DEFAULT_REFRESH_TOKEN)
            .scope(DEFAULT_SCOPE)
            .token_type(DEFAULT_TOKEN_TYPE)
            .user_id(DEFAULT_USER_ID)
            .created(DEFAULT_CREATED)
            .last_sync(DEFAULT_LAST_SYNC)
            .last_sync_intraday(DEFAULT_LAST_SYNC_INTRADAY)
            .userExtraId(null);
    }

    @Test
    @Transactional
    void authorizeToken_noTokensInDatabase() {
        //prepare
        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest+1);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    void authorizeToken_TokenAlreadyInDatabase() {
        //prepare
        token.setUserExtraId(DEFAULT_USER_EXTRA_ID);
        tokensRepository.saveAndFlush(token);
        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    void authorizeToken_SomeoneElseAlreadyHasUser_Id() {
        //prepare
        differentToken.setUserExtraId(UPDATED_USER_EXTRA_ID);
        differentToken.setUser_id(DEFAULT_USER_ID);
        Tokens tokenInDatabase = tokensRepository.saveAndFlush(differentToken);
        Long tokenInDatabaseId = tokenInDatabase.getId();
        token.setUserExtraId(null);

        doNothing().when(deviceServiceMock).deactivateFitbitDeviceForUserExtraId(anyLong(),anyString());
        em.clear();

        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest+1);
        assertThat(tokensRepository.findAllByUser_id(DEFAULT_USER_ID).size()).isEqualTo(1);
        validateToken(tokensMapper.toDto(tokensRepository.findById(tokenInDatabase.getId()).get()),null,UPDATED_USER_EXTRA_ID);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    void authorizeToken_TokenWithUserExtraIdExistsInDatabase() {
        //prepare
        differentToken.setUserExtraId(DEFAULT_USER_EXTRA_ID);
        differentToken.setUser_id(DEFAULT_USER_ID);
        Tokens tokenInDatabase = tokensRepository.saveAndFlush(differentToken);

        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    void authorizeToken_HasDifferentUser_id() {
        //prepare
        differentToken.setUserExtraId(DEFAULT_USER_EXTRA_ID);
        differentToken.setUser_id(UPDATED_USER_ID);
        Tokens tokenInDatabase = tokensRepository.saveAndFlush(differentToken);

        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    @Test
    @Transactional
    void authorizeToken_SomeoneElseHasSameUser_id() {
        //prepare
        differentToken.setUserExtraId(DEFAULT_USER_EXTRA_ID);
        differentToken.setUser_id(UPDATED_USER_ID);
        Tokens tokenInDatabase = tokensRepository.saveAndFlush(differentToken);

        int numberInDatabaseBeforeTest = tokensRepository.findAll().size();
        //execute
        TokensDTO authorizedToken = tokensService.authorizeToken(tokensMapper.toDto(token), DEFAULT_USER_EXTRA_ID);
        //validate
        assertThat(tokensRepository.findAll().size()).isEqualTo(numberInDatabaseBeforeTest);
        validateToken(authorizedToken,DEFAULT_USER_ID,DEFAULT_USER_EXTRA_ID);
    }

    private void validateToken(TokensDTO authorizedToken,String user_Id, Long userExtraId) {
        Optional<Tokens> tokenInDatabase = tokensRepository.findById(authorizedToken.getId());
        assertThat(tokenInDatabase.isPresent()).isTrue();

        assertThat(tokenInDatabase.get().getUser_id()).isEqualTo(user_Id);
        assertThat(tokenInDatabase.get().getUserExtraId()).isEqualTo(userExtraId);
    }
}
