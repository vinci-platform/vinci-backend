package com.upb.vinci.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TokensMapperTest {

    private TokensMapper tokensMapper;

    @BeforeEach
    public void setUp() {
        tokensMapper = new TokensMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tokensMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tokensMapper.fromId(null)).isNull();
    }
}
