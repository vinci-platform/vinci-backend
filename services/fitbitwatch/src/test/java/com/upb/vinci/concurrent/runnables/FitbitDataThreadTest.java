package com.upb.vinci.concurrent.runnables;

import com.upb.vinci.FitbitwatchApp;
import com.upb.vinci.service.DataCollectionService;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.web.rest.beans.FitbitNotification;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootTest(classes = FitbitwatchApp.class)
@AutoConfigureMockMvc
@WithMockUser
@RunWith(SpringRunner.class)
@Slf4j
class FitbitDataThreadTest {

    @Autowired
    private DataCollectionService dataCollectionService;

    private List<FitbitNotification> notificationList;
    private ExecutorService executor;
    private TokensDTO tokensDTO;

    @BeforeEach
    void setUp() {
        notificationList = new ArrayList<>();
        notificationList.add(new FitbitNotification("collectionType","ownerId","ownerType","subscriberId","subscriptionId","date"));
        executor = Executors.newFixedThreadPool(3);
        tokensDTO = new TokensDTO();
        tokensDTO.setUser_id("123123");
    }

    @Test
    public void testingThread() {
        log.info("<------start ------>");
        Runnable fitbitDataThread = new FitbitDataThread(notificationList, dataCollectionService);
        log.info("notificationList: {}",notificationList);
        executor.execute(fitbitDataThread);

    }

    @Test
    public void testingThread2() {
        log.info("<------start ------>");
        Runnable collectDataForTokenThread = new CollectDataForTokenThread(dataCollectionService, tokensDTO);
        log.info("tokensDTO: {}",tokensDTO);
        executor.execute(collectDataForTokenThread);

    }
}
