package com.upb.vinci.domain;

import com.upb.vinci.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TokensTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tokens.class);
        Tokens tokens1 = new Tokens();
        tokens1.setId(1L);
        Tokens tokens2 = new Tokens();
        tokens2.setId(tokens1.getId());
        assertThat(tokens1).isEqualTo(tokens2);
        tokens2.setId(2L);
        assertThat(tokens1).isNotEqualTo(tokens2);
        tokens1.setId(null);
        assertThat(tokens1).isNotEqualTo(tokens2);
    }
}
