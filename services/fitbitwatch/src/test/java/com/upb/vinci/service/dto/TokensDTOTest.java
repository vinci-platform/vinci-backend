package com.upb.vinci.service.dto;

import com.upb.vinci.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TokensDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TokensDTO.class);
        TokensDTO tokensDTO1 = new TokensDTO();
        tokensDTO1.setId(1L);
        TokensDTO tokensDTO2 = new TokensDTO();
        assertThat(tokensDTO1).isNotEqualTo(tokensDTO2);
        tokensDTO2.setId(tokensDTO1.getId());
        assertThat(tokensDTO1).isEqualTo(tokensDTO2);
        tokensDTO2.setId(2L);
        assertThat(tokensDTO1).isNotEqualTo(tokensDTO2);
        tokensDTO1.setId(null);
        assertThat(tokensDTO1).isNotEqualTo(tokensDTO2);
    }
}
