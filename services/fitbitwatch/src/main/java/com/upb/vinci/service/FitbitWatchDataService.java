package com.upb.vinci.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upb.vinci.domain.*;
import com.upb.vinci.error.fitbit.RefreshTokenException;
import com.upb.vinci.error.fitbit.ToManyRequestsToFitbitServerException;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.kafka.KafkaProducer;
import com.upb.vinci.service.mapper.TokensMapper;
import com.upb.vinci.service.webclient.FitbitWebClient;
import com.upb.vinci.web.rest.beans.DateDetails;
import com.upb.vinci.service.dto.DeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static com.upb.vinci.service.DateConstants.DATE_HOUR_FORMAT;

@Service
@Slf4j
public class FitbitWatchDataService {

    private final TokensService tokensService;
    private final TokensMapper tokensMapper;
    private final DeviceService deviceService;
    private final FitbitWebClient fitbitWebClient;
    private final FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;
    private final DateFormattingUtility dateFormattingUtility;
    private final KafkaProducer kafkaProducer;
    private final ObjectMapper objectMapper;

    public FitbitWatchDataService(@Lazy TokensService tokensService, TokensMapper tokensMapper, @Lazy DeviceService deviceService,
                                   FitbitWebClient fitbitWebClient, FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler,
                                  DateFormattingUtility dateFormattingUtility, KafkaProducer kafkaProducer) {
        this.tokensService = tokensService;
        this.tokensMapper = tokensMapper;
        this.deviceService = deviceService;
        this.fitbitWebClient = fitbitWebClient;
        this.fitbitWebClientExceptionHandler = fitbitWebClientExceptionHandler;
        this.dateFormattingUtility = dateFormattingUtility;
        this.kafkaProducer = kafkaProducer;
        objectMapper = new ObjectMapper();
    }

    public void retrieveFitbitWatchDataForToken(Tokens token)  {
        retrieveFitbitWatchDataForTokenDto(tokensMapper.toDto(token));
    }

    public void retrieveFitbitWatchDataForTokenDto(TokensDTO tokensDTO) {
        List<FitbitDevice> deviceFitbit;
        try {
            deviceFitbit = deviceService.getDevicesFromFitbitServer(tokensDTO);
        } catch (ToManyRequestsToFitbitServerException ignored) {
            log.info("There has been to many requests for fitbit user_id: {}",tokensDTO.getUser_id());
            return;
        }
        if (deviceFitbit.isEmpty()) {
            log.info("Could not find a fitbit device connected to user_id: {}. Ignoring record...", tokensDTO.getUser_id());
            return;
        }
        Optional<DeviceDTO> device = deviceService.getActiveFitbitDeviceForToken(tokensDTO);
        if (!device.isPresent()) {
            return;
        }

        for (DateDetails d: dateFormattingUtility.getDatesForFitbitDeviceAndTokenDto(deviceFitbit.get(0), tokensDTO)) {
            FitbitWatchData fitbitWatchData;
            try {
                tokensService.refreshToken(tokensDTO);
                fitbitWatchData = retrieveFitbitData(d.getDate(), device.get().getId(), tokensDTO);
            } catch (RefreshTokenException e){
                setLastSyncInToken(tokensDTO, d);
                log.info("Refresh token exception caught for date {};", d.getDate());
                break;
            } catch (Exception e){
                setLastSyncInToken(tokensDTO, d);
                log.info("Exception caught when retrieveFitbitData().Exception: {};",e.getMessage());
                e.printStackTrace();
                break;
            }
            persistNewFitbitWatchData(device.get(), fitbitWatchData);
        }
        tokensService.justSave(tokensDTO);
    }

    private void persistNewFitbitWatchData(DeviceDTO device, FitbitWatchData fitbitWatchData) {
        if (fitbitWatchData != null) {
            log.debug("Received FitbitWatchData.timestamp: {}", fitbitWatchData.getTimestamp());
            fitbitWatchData.setDeviceId(device.getId());
            kafkaProducer.emitFitbitWatchData(fitbitWatchData);
        } else {
            log.debug("There are no new updates to be saved. Data wont be sent to kafka");
        }
    }

    private FitbitWatchData retrieveFitbitData(String date, Long deviceId, TokensDTO tokensDTO) {
        SleepData sleepData;
        ActivityData activityData;
        try {
            sleepData = fitbitWebClient.getSleepData(tokensDTO, date);
            activityData = fitbitWebClient.getActivityData(tokensDTO, date);
        }catch (WebClientResponseException e) {
            log.error("Exception when getting FitbitWatchData from fitbit severs for user_id: {}", tokensDTO.getUser_id());
            throw fitbitWebClientExceptionHandler.handleError(e);//this throws exception always
        }

        return buildFitbitWatchData(date, deviceId, sleepData, activityData);
    }

    private FitbitWatchData buildFitbitWatchData(String date, Long deviceId, SleepData sleepData, ActivityData activityData){
        try {
            return FitbitWatchData.builder()
                .sleepData(objectMapper.writeValueAsString(sleepData))
                .activityData(objectMapper.writeValueAsString(activityData.getSummary()))
                .timestamp(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli())
                .deviceId(deviceId)
                .build();
        } catch (JsonProcessingException e) {
            log.error("Encountered an error while building a FitbitWatchData bean for saving to database.");
            throw new IllegalArgumentException("Encountered an error while building a FitbitWatchData bean for saving to database.");
        }
    }

    private void setLastSyncInToken(TokensDTO tokensDTO, DateDetails dateDetails) {
        //setup new last_sync_time in token, because there is no more requests available to send to fitbit servers.
        if (dateDetails.getStartTime().equals("") || dateDetails.getStartTime() == null) dateDetails.setStartTime("00:00");
        String timeTimeString = dateDetails.getDate() + " " + dateDetails.getStartTime() + ":00";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_HOUR_FORMAT);
        tokensDTO.setLast_sync_intraday(LocalDateTime.parse(timeTimeString, dateFormatter));
    }
}
