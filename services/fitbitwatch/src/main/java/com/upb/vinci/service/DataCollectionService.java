package com.upb.vinci.service;

import com.upb.vinci.concurrent.runnables.CollectDataForTokenThread;
import com.upb.vinci.error.InternalServerErrorException;
import com.upb.vinci.service.dto.TokensDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class DataCollectionService {

    private final int DATA_COLLECTION_TIMER_IN_MINUTES = 5;
    private final int DATA_COLLECTION_TIMER_IN_SECONDS = DATA_COLLECTION_TIMER_IN_MINUTES*60;
    private final int DATA_COLLECTION_TIMER_IN_MILLISECONDS = DATA_COLLECTION_TIMER_IN_SECONDS*1000;//old
    private final String DATA_COLLECTION_CRON_BEGINNING_OF_EVERY_HOUR = "1 1 * * * ?";

    private final ExecutorService executor;

    private final FitbitWatchDataService fitbitWatchDataService;
    private final FitbitWatchIntraDayDataService fitbitWatchIntraDayDataService;
    private final TokensService tokensService;

    public DataCollectionService(FitbitWatchDataService fitbitWatchDataService, FitbitWatchIntraDayDataService fitbitWatchIntraDayDataService,
                                 TokensService tokensService) {
        this.fitbitWatchDataService = fitbitWatchDataService;
        this.fitbitWatchIntraDayDataService = fitbitWatchIntraDayDataService;
        this.tokensService = tokensService;

        executor = Executors.newFixedThreadPool(3);
    }

    @Scheduled(cron = DATA_COLLECTION_CRON_BEGINNING_OF_EVERY_HOUR)
    public void collectAllDataForTokensInDatabase(){
        log.debug("Collecting all data for all tokens! Done at the beginning of every hour!");
        tokensService.justFindAll().stream().filter(this::isTokenValidForDataCollection)
            .forEach(tokensDTO -> {
                retrieveFitbitWatchDataForTokenDto(tokensDTO);
                retrieveFitbitWatchIntraDatDataForTokenDto(tokensDTO);
        });
    }

    public void collectFitbitWatchDataForTokensInDatabase() {
        log.debug("Collecting FitbitWatchDataData for all tokens!");
        for (TokensDTO tokensDTO : tokensService.justFindAll()) {
            retrieveFitbitWatchDataForTokenDto(tokensDTO);
        }
    }

    public void collectFitbitWatchIntraDayDataForTokensInDatabase() {
        log.debug("Collecting FitbitWatchIntraDayData for all tokens!");
        for (TokensDTO tokensDTO : tokensService.justFindAll()) {
            retrieveFitbitWatchIntraDatDataForTokenDto(tokensDTO);
        }
    }

    public void collectDataForUser_Id(String user_id) {
        log.debug("Collecting data for user_Id: {}", user_id);
        TokensDTO tokensDTO = tokensService.findByUserID(user_id)
            .orElseThrow(() -> new InternalServerErrorException("There is no token for user_Id: " + user_id + " and there should be!"));

        retrieveFitbitWatchDataForTokenDto(tokensDTO);
        retrieveFitbitWatchIntraDatDataForTokenDto(tokensDTO);
    }

    public void collectDataForTokenInDifferentThread(TokensDTO tokenDto) {
        Runnable collectDataForTokenThread = new CollectDataForTokenThread(this, tokenDto);
        executor.execute(collectDataForTokenThread);
    }

    public void retrieveFitbitWatchIntraDatDataForTokenDto(TokensDTO tokensDTO) {
        try {
            fitbitWatchIntraDayDataService.retrieveFitbitWatchIntraDayDataForTokenDto(tokensDTO);
        } catch (Exception e) {
            log.info("There has been an exception when collecting data for token with id: {}!!!", tokensDTO.getId());
            log.debug("Error: {}", e.getMessage());
        }
    }

    public void retrieveFitbitWatchDataForTokenDto(TokensDTO tokensDTO) {
        try {
            fitbitWatchDataService.retrieveFitbitWatchDataForTokenDto(tokensDTO);
        } catch (Exception e) {
            log.info("There has been an exception when collecting data for token with id: {}!!!", tokensDTO.getId());
            log.debug("Error: {}", e.getMessage());
        }
    }

    private boolean isTokenValidForDataCollection(TokensDTO tokensDTO) {
        return tokensDTO.getRefresh_token()!=null;
    }
}
