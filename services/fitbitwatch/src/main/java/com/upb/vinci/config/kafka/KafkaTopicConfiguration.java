package com.upb.vinci.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfiguration {

    //TOPIC_FITBIT_WATCH_DATA
    @Value("${kafka.topic.fitbit-watch-data.num-partitions}")
    int fitbitWatchData_numPartitions;
    @Value("${kafka.topic.fitbit-watch-data.replicas}")
    short fitbitWatchData_replicas;

    //TOPIC_FITBIT_WATCH_INTRADAY_DATA
    @Value("${kafka.topic.fitbit-watch-intraday-data.num-partitions}")
    int fitbitWatchIntradayData_numPartitions;
    @Value("${kafka.topic.fitbit-watch-intraday-data.replicas}")
    short fitbitWatchIntradayData_replicas;

    //TOPIC_FITBIT_DEVICE_UPDATE
    @Value("${kafka.topic.fitbit-device-update.num-partitions}")
    int fitbitDeviceUpdate_numPartitions;
    @Value("${kafka.topic.fitbit-device-update.replicas}")
    short fitbitDeviceUpdate_replicas;

    @Bean
    public NewTopic fitbitWatchDataTopic() {
        return new NewTopic(TopicConstants.TOPIC_FITBIT_WATCH_DATA,fitbitWatchData_numPartitions, fitbitWatchData_replicas);
    }

    @Bean
    public NewTopic fitbitWatchIntradayDataTopic() {
        return new NewTopic(TopicConstants.TOPIC_FITBIT_WATCH_INTRADAY_DATA,fitbitWatchIntradayData_numPartitions, fitbitWatchIntradayData_replicas);
    }

    @Bean
    public NewTopic fitbitDeviceUpdateTopic() {
        return new NewTopic(TopicConstants.TOPIC_FITBIT_DEVICE_UPDATE,fitbitDeviceUpdate_numPartitions, fitbitDeviceUpdate_replicas);
    }

}
