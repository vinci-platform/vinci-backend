package com.upb.vinci.error.fitbit;

import com.upb.vinci.error.response.ErrorResponse;
import com.upb.vinci.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ToManyRequestsToFitbitServerException extends AbstractThrowableProblem {
    private static final String TITLE = "TO_MANY_REQUESTS_TO_FITBIT_SERVER";
    private static final Status STATUS_TYPE = Status.TOO_MANY_REQUESTS;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_MESSAGE = "Too many requests to fitbit server for user!";

    public ToManyRequestsToFitbitServerException() { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, DEFAULT_MESSAGE); }
    public ToManyRequestsToFitbitServerException(String message) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, message); }
    public ToManyRequestsToFitbitServerException(ErrorResponse errorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, errorResponse.getDetail()); }

    @Override
    public String toString() {
        return "ToManyRequestsToFitbitServerException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
