package com.upb.vinci.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * A Tokens.
 */
@Entity
@Table(name = "tokens")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Tokens implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "access_token")
    private String access_token;

    @Column(name = "expires_in")
    private Integer expires_in;

    @Column(name = "refresh_token")
    private String refresh_token;

    @Column(name = "scope")
    private String scope;

    @Column(name = "token_type")
    private String token_type;

    @Column(name = "user_id")
    private String user_id;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "last_sync")
    private LocalDateTime last_sync;

    @Column(name = "last_sync_intraday")
    private LocalDateTime last_sync_intraday;

    @Column(name = "user_extra_id")
    private Long userExtraId;

    @Column(name = "subscription_id")
    private String subscription_id;

    @PrePersist
    protected void onPersist() {
        this.created = LocalDateTime.now();
        this.last_sync = LocalDateTime.now().minus(3, ChronoUnit.DAYS);
        this.last_sync_intraday = LocalDateTime.now().minus(1, ChronoUnit.DAYS);
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public Tokens access_token(String access_token) {
        this.access_token = access_token;
        return this;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public Tokens expires_in(Integer expires_in) {
        this.expires_in = expires_in;
        return this;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public Tokens refresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
        return this;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public Tokens scope(String scope) {
        this.scope = scope;
        return this;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getToken_type() {
        return token_type;
    }

    public Tokens token_type(String token_type) {
        this.token_type = token_type;
        return this;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public Tokens user_id(String user_id) {
        this.user_id = user_id;
        return this;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public Tokens created(LocalDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLast_sync() {
        return last_sync;
    }

    public Tokens last_sync(LocalDateTime last_sync) {
        this.last_sync = last_sync;
        return this;
    }

    public void setLast_sync(LocalDateTime last_sync) {
        this.last_sync = last_sync;
    }

    public LocalDateTime getLast_sync_intraday() {
        return last_sync_intraday;
    }

    public Tokens last_sync_intraday(LocalDateTime last_sync_intraday) {
        this.last_sync_intraday = last_sync_intraday;
        return this;
    }

    public void setLast_sync_intraday(LocalDateTime last_sync_intraday) {
        this.last_sync_intraday = last_sync_intraday;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public Tokens userExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
        return this;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tokens)) {
            return false;
        }
        return id != null && id.equals(((Tokens) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
    // prettier-ignore

    @Override
    public String toString() {
        return "Tokens{" +
            "id=" + getId() +
            ", access_token='" + getAccess_token() + "'" +
            ", expires_in=" + getExpires_in() +
            ", refresh_token='" + getRefresh_token() + "'" +
            ", scope='" + getScope() + "'" +
            ", token_type='" + getToken_type() + "'" +
            ", user_id='" + getUser_id() + "'" +
            ", created='" + getCreated() + "'" +
            ", last_sync='" + getLast_sync() + "'" +
            ", last_sync_intraday='" + getLast_sync_intraday() + "'" +
            ", userExtraId=" + getUserExtraId() +
            "}";
    }
}
