package com.upb.vinci.web.rest.beans;


import lombok.Data;

@Data
public class DateDetails {
    String date;
    String startTime;
    String endTime;
}
