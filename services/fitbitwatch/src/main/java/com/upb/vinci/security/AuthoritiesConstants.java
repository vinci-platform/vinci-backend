package com.upb.vinci.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String FAMILY = "ROLE_FAMILY";

    public static final String PACIENT = "ROLE_PACIENT";

    public static final String ORGANIZIATION="ROLE_ORGANIZATION";

    private AuthoritiesConstants() {
    }
}
