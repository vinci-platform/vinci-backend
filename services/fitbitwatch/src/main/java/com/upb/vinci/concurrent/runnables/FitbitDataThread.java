package com.upb.vinci.concurrent.runnables;

import com.upb.vinci.service.DataCollectionService;
import com.upb.vinci.web.rest.beans.FitbitNotification;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class FitbitDataThread implements Runnable {

    private final List<FitbitNotification> fitbitNotifications;

    private final DataCollectionService dataCollectionService;

    public FitbitDataThread(List<FitbitNotification> fitbitNotifications, DataCollectionService dataCollectionService) {
        this.fitbitNotifications = fitbitNotifications;
        this.dataCollectionService = dataCollectionService;
    }

    @Override
    public void run() {
        log.debug(" --- IN FitbitDataThread.run() ---");
        fitbitNotifications.forEach(fitbitNotification -> log.info(fitbitNotification.toString()));

        Set<String> uniqueUser_Ids = fitbitNotifications.stream().map(FitbitNotification::getOwnerId).collect(Collectors.toSet());
        log.debug("uniqueUser_Ids: {}",uniqueUser_Ids);

        for (String user_id : uniqueUser_Ids){
            dataCollectionService.collectDataForUser_Id(user_id);
        }
    }
}


