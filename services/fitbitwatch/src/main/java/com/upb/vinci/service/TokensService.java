package com.upb.vinci.service;

import com.upb.vinci.domain.Tokens;
import com.upb.vinci.error.*;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.repository.TokensRepository;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.mapper.TokensMapper;
import com.upb.vinci.service.webclient.FitbitWebClient;
import com.upb.vinci.service.webclient.beans.GetSubscriptionsResponse;
import com.upb.vinci.web.rest.beans.UserDTO;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Tokens}.
 */
@Service
@Transactional
public class TokensService {

    private static final String ENTITY_NAME = "fitbitwatchTokens";

    private final Logger log = LoggerFactory.getLogger(TokensService.class);

    private final FitbitWebClient fitbitWebClient;

    private final TokensRepository tokensRepository;

    private final TokensMapper tokensMapper;

    private final DeviceService deviceService;

    private final UserService userService;

    private final FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;

    public TokensService(FitbitWebClient fitbitWebClient, TokensRepository tokensRepository, TokensMapper tokensMapper,
                         @Lazy DeviceService deviceService, UserService userService, FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler) {
        this.fitbitWebClient = fitbitWebClient;
        this.tokensRepository = tokensRepository;
        this.tokensMapper = tokensMapper;
        this.deviceService = deviceService;
        this.userService = userService;
        this.fitbitWebClientExceptionHandler = fitbitWebClientExceptionHandler;
    }

    /**
     * Just saves tokenDto.
     *
     * @param tokensDTO the entity to save.
     * @return the persisted entity.
     */
    public TokensDTO justSave(TokensDTO tokensDTO) {
        log.debug("Request to save Tokens : {}", tokensDTO);
        Tokens tokens = tokensMapper.toEntity(tokensDTO);
        tokens = tokensRepository.save(tokens);
        return tokensMapper.toDto(tokens);
    }

    /**
     * Save a tokens.
     *
     * @param tokensDTO the entity to save.
     * @return the persisted entity.
     */
    public TokensDTO save(TokensDTO tokensDTO) {
        log.info("Request to save Tokens : {}", tokensDTO);
        validateTokensDtoForCreation(tokensDTO);

        UserDTO currentUserDto = userService.getCurrentUserDto()
            .orElseThrow(NoUserForTokenException::new);

        if (currentUserDto.getAuthorities().contains(AuthoritiesConstants.ADMIN) || tokensDTO.getUserExtraId().equals(currentUserDto.getUserExtraId())) {
            Tokens tokens = tokensMapper.toEntity(tokensDTO);
            tokens = tokensRepository.save(tokens);
            return tokensMapper.toDto(tokens);
        } else {
            log.info("Trying to save token for different user.");
            throw new AuthorizationException("You can only save your fitbit token. Use token with same userExtraId as userExtraId from user from token!");
        }
    }

    /**
     * Just gets all the tokens.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TokensDTO> justFindAll() {
        log.debug("Request to get all Tokens");
        return tokensRepository.findAll().stream()
            .map(tokensMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the tokens.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TokensDTO> findAll() {
        log.debug("Request to get all Tokens");
        UserDTO currentUserDto = userService.getCurrentUserDto()
            .orElseThrow(NoUserForTokenException::new);

        if (currentUserDto.getAuthorities().contains(AuthoritiesConstants.ADMIN)){
            return tokensRepository.findAll().stream()
                .map(tokensMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        }
        List<TokensDTO> byUserExtraIdList = tokensRepository
            .findByUserExtraId(currentUserDto.getUserExtraId())
            .stream()
            .map(tokensMapper::toDto)
            .collect(Collectors.toList());
        if (byUserExtraIdList.isEmpty()) {
            log.info("There is no {} for userExtraId: {}", ENTITY_NAME, currentUserDto.getUserExtraId());
            return Collections.emptyList();
        }
        return byUserExtraIdList;
    }

    /**
     * Get one tokens by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public TokensDTO findOne(Long id) {
        log.debug("Request to get Tokens by id: {}", id);
        UserDTO currentUserDto = userService.getCurrentUserDto()
            .orElseThrow(NoUserForTokenException::new);

        TokensDTO tokenDTO = tokensRepository.findById(id)
            .map(tokensMapper::toDto).orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));
        if (currentUserDto.getAuthorities().contains(AuthoritiesConstants.ADMIN) || currentUserDto.getUserExtraId().equals(tokenDTO.getUserExtraId())){
            return tokenDTO;
        } else {
            log.info("Token with id {} does not belong to the current user with userId {} and userExtraId {}",id,currentUserDto.getId(),currentUserDto.getUserExtraId());
            throw new AuthorizationException(String.format("Token with id %s does not belong to the current user with userId %s and userExtraId %s",id,currentUserDto.getId(),currentUserDto.getUserExtraId()));
        }
    }

    /**
     * Just delete the tokens by id.
     *
     * @param id the id of the entity.
     */
    public void justDelete(Long id) {
        log.debug("Request to delete Tokens : {}", id);
        tokensRepository.deleteById(id);
    }

    /**
     *
     * @param userId
     * @return Optional of token if it exists, or empty optional if it doesn't
     */
    @Transactional(readOnly = true)
    public Optional<TokensDTO> findByUserID(String userId) {
        return tokensRepository.findByUserId(userId)
            .map(tokensMapper::toDto);
    }

    public TokensDTO justUpdate(TokensDTO tokensDTO) {
        validateTokensDtoForUpdate(tokensDTO);
        return justSave(tokensDTO);
    }

    public TokensDTO refreshToken(TokensDTO existingTokensDTO) {
        if (!hasTokenExpired(existingTokensDTO)) {
            return existingTokensDTO;
        }
        TokensDTO tokenResponse;
        try {
            tokenResponse = fitbitWebClient.refreshToken(existingTokensDTO);
        }catch (WebClientResponseException e) {
            log.debug("Exception when Refreshing token, message: {}", e.getMessage());
            existingTokensDTO.setRefresh_token(null);
            unsubscribeToken(existingTokensDTO);
            deviceService.deactivateFitbitDeviceForUserExtraId(existingTokensDTO.getUserExtraId(),"Refresh token not valid!");
            throw fitbitWebClientExceptionHandler.handleError(e);
        }
        updateTokenWithDataFromDatabase(tokenResponse);
        return justSave(tokenResponse);
    }

    public Tokens unsubscribeToken(TokensDTO existingTokensDTO) {
        GetSubscriptionsResponse subscriptions = fitbitWebClient.getSubscriptions(existingTokensDTO);
        subscriptions.getApiSubscriptions()
            .stream()
            .filter(apiSubscription -> apiSubscription.getCollectionType().equals("activities"))
            .findFirst()
            .ifPresent(apiSubscription -> fitbitWebClient.unsubscribe(existingTokensDTO, apiSubscription.getSubscriptionId()));

        existingTokensDTO.setSubscription_id(null);
        return tokensRepository.save(tokensMapper.toEntity(existingTokensDTO));
    }

    public TokensDTO processToken(String code, Long userExtraId) {
        TokensDTO tokenResponse = getFitbitTokenFromFitbitServers(code);
        TokensDTO savedTokenDto = authorizeToken(tokenResponse, userExtraId);
        subscribeToken(savedTokenDto);
        return savedTokenDto;
    }

    protected TokensDTO authorizeToken(TokensDTO tokenToAuthorizeDto,Long userExtraId) {
        Optional<Tokens> existingTokenForUserExtraIdOptional = tokensRepository.findByUserExtraId(userExtraId).stream().findAny();

        if (existingTokenForUserExtraIdOptional.isPresent()) {
            tokenToAuthorizeDto.setId(existingTokenForUserExtraIdOptional.get().getId());
            tokenToAuthorizeDto.setCreated(LocalDateTime.now());
            tokenToAuthorizeDto.setLast_sync(existingTokenForUserExtraIdOptional.get().getLast_sync());
            tokenToAuthorizeDto.setLast_sync_intraday(existingTokenForUserExtraIdOptional.get().getLast_sync_intraday());
        }
        tokenToAuthorizeDto.setUserExtraId(userExtraId);
        List<Long> tokenIdsThatHaveUser_IdFromNewToken = getTokenIdsForUser_Id(tokenToAuthorizeDto);

        setSubscriptionId(tokenToAuthorizeDto);
        TokensDTO savedTokenDto = justSave(tokenToAuthorizeDto);
        log.info("Saved token.user_id: {}; token.userExtraId: {}", tokenToAuthorizeDto.getUser_id(),tokenToAuthorizeDto.getUserExtraId());

        deactivateTokensWithUser_Id(tokenIdsThatHaveUser_IdFromNewToken);

        return savedTokenDto;
    }

    private void setSubscriptionId(TokensDTO tokenToAuthorizeDto) {
        if (tokenToAuthorizeDto.getSubscription_id() == null || tokenToAuthorizeDto.getSubscription_id().equals(""))
        tokenToAuthorizeDto.setSubscription_id(UUID.randomUUID().toString());
    }

    private List<Long> getTokenIdsForUser_Id(TokensDTO tokensDTO) {
        return tokensRepository.findAllByUser_id(tokensDTO.getUser_id())
            .stream()
            .map(Tokens::getId)
            //so token that is going to be saved is not deactivated later
            .filter(tokenId -> !tokenId.equals(tokensDTO.getId()))
            .collect(Collectors.toList());
    }

    /**
     * This method goes last because it sends requests to gateway service that cant be rolled back
     * @param tokenIdList
     */
    private void deactivateTokensWithUser_Id(List<Long> tokenIdList) {
        tokenIdList.forEach(tokenId -> {
            Optional<Tokens> tokenToUpdate = tokensRepository.findById(tokenId);
            tokenToUpdate.get().setUser_id(null);
            tokenToUpdate.get().setSubscription_id(null);
            tokensRepository.save(tokenToUpdate.get());
            deviceService.deactivateFitbitDeviceForUserExtraId(tokenToUpdate.get().getUserExtraId(),
                "Some other vinci user has connected with yours fitbit account. You need to connect again in order to receive data!");
        });
    }

    private TokensDTO getFitbitTokenFromFitbitServers(String code) {
        try {
            return fitbitWebClient.getFitbitTokenFromFitbitServers(code);
        }catch (WebClientResponseException e) {
            log.info("Exception when trying to get token from fitbit server with code. Exception message: {}", e.getMessage());
            throw fitbitWebClientExceptionHandler.handleError(e);
        }
    }

    private void subscribeToken(TokensDTO tokenDto) {
        fitbitWebClient.subscribe(tokenDto).subscribe(clientResponse -> {
            log.info("Response code from subscription request: {}", clientResponse.statusCode());
            if (clientResponse.statusCode().equals(HttpStatus.CONFLICT)) {
                unsubscribeToken(tokenDto);
                deviceService.deactivateFitbitDeviceForUserExtraId(tokenDto.getUserExtraId(),"Some other server or service is already subscribed to the data stream on fitbit servers. You must delete that subscription by disconnecting.");
                log.error("A single user may only be subscribed to the same collection once. Subscription already exists so corrective action must be taken.");
            }
        });
    }

    private void validateTokensDtoForCreation(TokensDTO tokensDTO) {
        if (tokensDTO.getId() != null) {
            throw new BadRequestAlertException("A new tokens cannot already have an ID", ENTITY_NAME, "idexists");
        }
        List<Tokens> tokensForUserExtraIdFromToken = tokensRepository.findByUserExtraId(tokensDTO.getUserExtraId());
        if (!tokensForUserExtraIdFromToken.isEmpty()){
            throw new BadRequestAlertException("A token with userExtraId already exist", ENTITY_NAME, "userExtraIdExist");
        }
    }

    private boolean hasTokenExpired(TokensDTO existingTokensDTO) {
        return existingTokensDTO.getCreated().plusSeconds(existingTokensDTO.getExpires_in()).isBefore(LocalDateTime.now());
    }

    private void updateTokenWithDataFromDatabase(TokensDTO tokenResponse) {
        findByUserID(tokenResponse.getUser_id()).ifPresent(tokensDTO -> {
            tokenResponse.setId(tokensDTO.getId());
            tokenResponse.setCreated(LocalDateTime.now());
            tokenResponse.setLast_sync(tokensDTO.getLast_sync());
            tokenResponse.setLast_sync_intraday(tokensDTO.getLast_sync_intraday());
            tokenResponse.setUserExtraId(tokensDTO.getUserExtraId()); });
    }

    private void validateTokensDtoForUpdate(TokensDTO tokensDTO) {
        if (tokensDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }
}
