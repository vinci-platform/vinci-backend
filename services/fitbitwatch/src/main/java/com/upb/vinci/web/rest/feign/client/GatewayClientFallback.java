package com.upb.vinci.web.rest.feign.client;

import com.upb.vinci.web.rest.beans.UserDTO;
import com.upb.vinci.service.dto.DeviceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class GatewayClientFallback implements GatewayClient {

    private final Logger log = LoggerFactory.getLogger(GatewayClientFallback.class);

    @Override
    public List<DeviceDTO> getFitbitDeviceForUserExtraId(Long userExtraId) {
        log.error("Encountered an error when retrieving watch devices for userExtraId:{}. Defaulting to an empty list.",userExtraId);
        return Collections.emptyList();
    }

    @Override
    public DeviceDTO updateDevice(DeviceDTO deviceDTO) {
        log.error("Encountered an error when updating device:{}. Defaulting to an empty object.",deviceDTO);
        return new DeviceDTO();
    }

    @Override
    public DeviceDTO createDevice(DeviceDTO deviceDTO) {
        log.error("Encountered an error when creating device:{}. Defaulting to an empty object.",deviceDTO);
        return new DeviceDTO();
    }

    @Override
    public Optional<UserDTO> getUserWithAuthorities() {
        log.info("==> getUserWithAuthorities()");
        //todo: throw exception
        return Optional.empty();
    }
}
