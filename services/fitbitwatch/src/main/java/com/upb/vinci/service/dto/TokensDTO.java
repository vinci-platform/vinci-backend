package com.upb.vinci.service.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link com.upb.vinci.domain.Tokens} entity.
 */
public class TokensDTO implements Serializable {

    private Long id;

    private String access_token;

    private Integer expires_in;

    private String refresh_token;

    private String scope;

    private String token_type;

    @NotNull
    private String user_id;

    private LocalDateTime created;

    private LocalDateTime last_sync;

    private LocalDateTime last_sync_intraday;

    private String subscription_id;

    @NotNull
    private Long userExtraId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(LocalDateTime last_sync) {
        this.last_sync = last_sync;
    }

    public LocalDateTime getLast_sync_intraday() {
        return last_sync_intraday;
    }

    public void setLast_sync_intraday(LocalDateTime last_sync_intraday) {
        this.last_sync_intraday = last_sync_intraday;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TokensDTO)) {
            return false;
        }

        return id != null && id.equals(((TokensDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TokensDTO{" +
            "id=" + getId() +
            ", access_token='" + getAccess_token() + "'" +
            ", expires_in=" + getExpires_in() +
            ", refresh_token='" + getRefresh_token() + "'" +
            ", scope='" + getScope() + "'" +
            ", token_type='" + getToken_type() + "'" +
            ", user_id='" + getUser_id() + "'" +
            ", created='" + getCreated() + "'" +
            ", last_sync='" + getLast_sync() + "'" +
            ", last_sync_intraday='" + getLast_sync_intraday() + "'" +
            ", userExtraId=" + getUserExtraId() +
            "}";
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }
}
