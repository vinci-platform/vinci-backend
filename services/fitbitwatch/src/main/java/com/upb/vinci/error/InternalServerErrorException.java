package com.upb.vinci.error;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.error.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

/**
 * Simple exception with a message, that returns an Internal Server Error code.
 */
public class InternalServerErrorException extends AbstractThrowableProblem {

    private static final String TITLE = "INTERNAL_SERVER_ERROR";
    private static final String DEFAULT_DETAILS = "Exception not covered! Internal server error exception is thrown!";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final long serialVersionUID = 1L;

    public InternalServerErrorException(String message) {
        super(ErrorConstants.DEFAULT_TYPE, TITLE, Status.INTERNAL_SERVER_ERROR,message);
    }
    public InternalServerErrorException() {
        super(ErrorConstants.DEFAULT_TYPE, TITLE, Status.INTERNAL_SERVER_ERROR,DEFAULT_DETAILS);
    }
    public InternalServerErrorException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }
    @Override
    public String toString() {
        return "IncorrectDeviceTypeException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
