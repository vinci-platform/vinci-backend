package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.FitbitWatchIntradayData;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface FitbitWatchIntradayDataMapper {
    FitbitWatchIntradayDataDTO toDto(FitbitWatchIntradayData fitbitWatchIntradayData);

    @InheritInverseConfiguration
    FitbitWatchIntradayData toEntity(FitbitWatchIntradayDataDTO fitbitWatchIntradayDataDTO);
}
