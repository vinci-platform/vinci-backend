package com.upb.vinci.service.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private String uuid;

    @NotNull
    private DeviceType deviceType;

    @NotNull
    private Boolean active;

    private Instant startTimestamp;

    private Long userExtraId;
}
