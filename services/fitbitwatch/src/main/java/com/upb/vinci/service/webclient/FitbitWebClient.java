package com.upb.vinci.service.webclient;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.webclient.beans.GetSubscriptionsResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class FitbitWebClient {

    @Value("${fitbit.api.redirect-url}")
    String redirectUrl;

    @Value("${fitbit.api.resource.token}")
    String tokenUrl;

    @Value("${fitbit.api.client-id}")
    String defaultclientId;

    @Value("${fitbit.api.secret-id}")
    String defaultsecretId;

    @Value("${fitbit.api.resource.sleep}")
    String fitbitSleepUri;

    @Value("${fitbit.api.resource.activities}")
    String activitiesUrl;

    @Value("${fitbit.api.resource.activities}")
    String fitbitActivitiesUri;

    @Value("${fitbit.api.resource.subscribe}")
    String fitbitSubscribeUrl;

    private final WebClient webClient;

    public FitbitWebClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public GetSubscriptionsResponse getSubscriptions(TokensDTO tokensDTO) {
        String endpoint = fitbitSubscribeUrl + tokensDTO.getUser_id() + "/activities/apiSubscriptions.json";
        log.debug("get subscriptions endpoint: {}", endpoint);
        return webClient.get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(GetSubscriptionsResponse.class).block();
    }

    public Mono<ClientResponse> subscribe(TokensDTO tokenToSubscribe) {
        String endpoint = fitbitSubscribeUrl + tokenToSubscribe.getUser_id() + "/activities/apiSubscriptions/" + tokenToSubscribe.getSubscription_id() + ".json";
        log.debug("subscribe endpoint: {}", endpoint);
        return webClient.post()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokenToSubscribe.getAccess_token()))
            .exchange();
    }

    public Mono<ClientResponse> unsubscribe(TokensDTO tokenToSubscribe, String subscription_Id) {
        String endpoint = fitbitSubscribeUrl + tokenToSubscribe.getUser_id() + "/activities/apiSubscriptions/" + subscription_Id + ".json";
        log.debug("unsubscribe endpoint: {}", endpoint);
        return webClient.delete()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokenToSubscribe.getAccess_token()))
            .exchange();
    }

    //Tokens api
    public TokensDTO getFitbitTokenFromFitbitServers(String code) {
        String endpoint = tokenUrl + "?code=" + code + "&client_id=" + defaultclientId + "&grant_type=authorization_code&redirect_uri=" + redirectUrl;
        log.debug("endpoint: {}", endpoint);
        return webClient.post()
            .uri(endpoint)
            .header("Authorization", "Basic " + getEncodedClientData())
            .header("Content-Type", "application/x-www-form-urlencoded")
            .retrieve()
            .bodyToMono(TokensDTO.class).block();
    }

    public TokensDTO refreshToken(TokensDTO tokenToRefresh) {
        String endpoint = tokenUrl + "?grant_type=refresh_token&refresh_token=" + tokenToRefresh.getRefresh_token();
        log.debug("endpoint: {}", endpoint);
        return webClient.post()
            .uri(endpoint)
            .header("Authorization", "Basic " + getEncodedClientData())
            .header("Content-Type", "application/x-www-form-urlencoded")
            .retrieve()
            .bodyToMono(TokensDTO.class).block();
    }

    //Device api
    public List<FitbitDevice> getFitbitDevicesFromFitbitServer(TokensDTO tokensDTO) {
        String endpoint = activitiesUrl + tokensDTO.getUser_id() + "/devices.json";
        log.debug("endpoint: {}", endpoint);
        return Arrays.asList(webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(FitbitDevice[].class).block());
    }

    //DataApi
    public SleepData getSleepData(TokensDTO tokensDTO, String date) {
        String endpoint = fitbitSleepUri + tokensDTO.getUser_id() + "/sleep/date/" + date + ".json";
        log.debug("endpoint: {}", endpoint);
        return webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(SleepData.class).block();
    }

    public ActivityData getActivityData(TokensDTO tokensDTO, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/date/" + date + ".json";
        log.debug("endpoint: {}", endpoint);
        return  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivityData.class).block();
    }
    //IntraDay Data Api
    public List<ActivitiesIntraday.DatasetItem> getHeartData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/heart/date/" + date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesHeart response;
        response = webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesHeart.class).block();
        return response != null && response.getActivitiesHeartIntraday() != null &&
            response.getActivitiesHeartIntraday().getDataset() != null
            ? response.getActivitiesHeartIntraday().getDataset() : Lists.newArrayList();
    }

    public List<ActivitiesIntraday.DatasetItem> getStepsData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/steps/date/"+ date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesSteps response =  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesSteps.class).block();
        return response != null ? response.getActivitiesStepsIntraday().getDataset() : Lists.newArrayList();
    }

    public List<ActivitiesIntraday.DatasetItem> getCaloriesData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/calories/date/" + date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesCalories response =  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesCalories.class).block();
        return response != null ? response.getActivitiesCaloriesIntraday().getDataset() : Lists.newArrayList();
    }

    public List<ActivitiesIntraday.DatasetItem> getDistanceData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/distance/date/" + date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesDistance response =  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesDistance.class).block();
        return response != null ? response.getActivitiesDistanceIntraday().getDataset() : Lists.newArrayList();
    }

    public List<ActivitiesIntraday.DatasetItem> getFloorsData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/floors/date/" + date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesFloors response =  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesFloors.class).block();
        return response != null ? response.getActivitiesFloorsIntraday().getDataset() : Lists.newArrayList();
    }

    public List<ActivitiesIntraday.DatasetItem> getElevationData(TokensDTO tokensDTO, String startTime, String endTime, String date) {
        String endpoint = fitbitActivitiesUri + tokensDTO.getUser_id() + "/activities/elevation/date/" + date + "/1d/time/" + startTime + "/" + endTime + ".json";
        log.debug("endpoint: {}", endpoint);
        ActivitiesElevation response =  webClient
            .get()
            .uri(endpoint)
            .headers(h -> h.setBearerAuth(tokensDTO.getAccess_token()))
            .retrieve()
            .bodyToMono(ActivitiesElevation.class).block();
        return response != null ? response.getActivitiesElevationIntraday().getDataset() : Lists.newArrayList();
    }

    private String getEncodedClientData() {
        return Base64Utils.encodeToString((defaultclientId + ":" + defaultsecretId).getBytes());
    }
}
