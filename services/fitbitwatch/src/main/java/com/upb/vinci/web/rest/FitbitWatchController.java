package com.upb.vinci.web.rest;

import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.DataCollectionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/api/fitbit-data")
public class FitbitWatchController {

    private final DataCollectionService dataCollectionService;

    public FitbitWatchController(DataCollectionService dataCollectionService) {
        this.dataCollectionService = dataCollectionService;
    }

    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @GetMapping("manual-start")
    protected void retrieveSleepActivityData() {
        dataCollectionService.collectFitbitWatchDataForTokensInDatabase();
    }
}
