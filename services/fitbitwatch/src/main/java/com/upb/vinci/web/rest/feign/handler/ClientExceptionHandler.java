package com.upb.vinci.web.rest.feign.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upb.vinci.error.*;
import com.upb.vinci.error.fitbit.FitbitWatchDataCanNotBeParsedFromStringException;
import com.upb.vinci.error.fitbit.RefreshTokenException;
import com.upb.vinci.error.fitbit.ToManyRequestsToFitbitServerException;
import com.upb.vinci.error.response.ErrorResponse;
import feign.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.zalando.problem.AbstractThrowableProblem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ClientExceptionHandler implements FeignHttpExceptionHandler {
    private final String ENTITY_NAME = "ClientExceptionHandler";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Exception handle(Response response) {
        String responseBody = getResponseBody(response);
        final int responseStatus = response.status();
        ErrorResponse errorResponse = getErrorResponseFromString(responseBody);
        switch (responseStatus) {
            case 400: return handleBadRequestException(errorResponse);
            case 401: return handleUnauthorizedException(errorResponse);
            case 429: return handleToManyRequestsException(errorResponse);
            case 500: return handleInternalServerErrorException(errorResponse);
            default:
                return new InternalServerErrorException("Exception not covered in "+ENTITY_NAME+"! Error response: " + errorResponse);
        }
    }

    private AbstractThrowableProblem handleBadRequestException(ErrorResponse errorResponse) {
        final String title = errorResponse.getTitle();
        switch (title) {
            case "INCORRECT_DEVICE_TYPE_EXCEPTION": return new IncorrectDeviceTypeException(errorResponse);
            case "NO_ENTITY_EXCEPTION": return new NoEntityException(errorResponse);
            case "NO_USER_BY_LOGIN_EXCEPTION": return new NoUserByLoginException(errorResponse);
            case "NO_USER_EXTRA_FOR_ID": return new NoUserExtraForId(errorResponse);
            case "NO_USER_EXTRA_FOR_USER_ID": return new NoUserExtraForUserIdException(errorResponse);
            case "NO_DEVICE_FOUND_EXCEPTION": return new NoDeviceFoundException(errorResponse);
            default:
                throw new InternalServerErrorException("Unexpected value for Title when handling exception from gateway service: " + title+"! Error response:" + errorResponse);
        }
    }

    private AbstractThrowableProblem handleUnauthorizedException(ErrorResponse errorResponse) {
        final String title = errorResponse.getTitle();
        switch (title) {
            case "AUTHORIZATION_EXCEPTION": return new AuthorizationException(errorResponse);
            case "NO_USER_FOR_TOKEN_EXCEPTION": return new NoUserForTokenException(errorResponse);
            case "REFRESH_TOKEN_EXCEPTION": return new RefreshTokenException(errorResponse);
            default:
                throw new InternalServerErrorException("Unexpected value for Title when handling exception from gateway service: " + title +"! Error response:" + errorResponse);
        }
    }

    private Exception handleToManyRequestsException(ErrorResponse errorResponse) {
        final String title = errorResponse.getTitle();
        switch (title) {
            case "TO_MANY_REQUESTS_TO_FITBIT_SERVER": return new ToManyRequestsToFitbitServerException(errorResponse);
            default:
                throw new InternalServerErrorException("Unexpected value for Title when handling exception from gateway service: " + title +"! Error response:" + errorResponse);
        }
    }

    private AbstractThrowableProblem handleInternalServerErrorException(ErrorResponse errorResponse) {
        final String title = errorResponse.getTitle();
        switch (title) {
            case "FITBIT_WATCH_DATA_CANT_BE_PARSED_FROM_STRING": return new FitbitWatchDataCanNotBeParsedFromStringException(errorResponse);
            default:
                return new InternalServerErrorException("Unexpected value for Title when handling exception from gateway service: " + title+"! Error response:" + errorResponse);
        }
    }

    private String getResponseBody(Response response) {
        try {
            String responseBody = new BufferedReader(
                new InputStreamReader(response.body().asInputStream(), StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
            log.debug("ResponseBody string: {}", responseBody);
            return responseBody;
        } catch (IOException exception) {
            log.error("exception.message: {}", exception.getMessage());
            throw new ParsingErrorResponseException("Error while deserializing the response body!\n Error message: \n" + exception.getMessage());
        }
    }

    private ErrorResponse getErrorResponseFromString(String responseBody) {
        try {
            return objectMapper.readValue(responseBody, ErrorResponse.class);
        } catch (IOException exception) {
            log.error("ErrorResponse could not be parsed!");
            throw new ParsingErrorResponseException(exception.getMessage());
        }
    }
}
