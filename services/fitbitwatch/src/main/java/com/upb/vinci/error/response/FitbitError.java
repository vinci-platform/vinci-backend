package com.upb.vinci.error.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FitbitError {
    private String errorType;//"expired_token","request"
    private String fieldName;//"500"
    private String message;
}
