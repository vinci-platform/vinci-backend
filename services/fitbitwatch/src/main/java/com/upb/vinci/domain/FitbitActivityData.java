package com.upb.vinci.domain;

import lombok.Data;

@Data
public class FitbitActivityData {
    private String time;
    private Long timestamp;
    private Integer heart;
    private Integer steps;
    private Integer calories;
    private Integer floors;
    private Integer elevation;
    private Integer distance;
}


