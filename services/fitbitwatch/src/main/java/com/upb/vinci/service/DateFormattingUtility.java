package com.upb.vinci.service;

import com.upb.vinci.domain.FitbitDevice;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.web.rest.beans.DateDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.upb.vinci.service.DateConstants.*;

@Component
@Slf4j
public class DateFormattingUtility {

    public List<DateDetails> getDatesForFitbitDeviceAndTokenDto(FitbitDevice deviceFitbit, TokensDTO tokensDTO) {
        LocalDateTime updatedLastSync = LocalDateTime.parse(deviceFitbit.getLastSyncTime());
        LocalDateTime lastSync = (tokensDTO.getLast_sync() == null ||
            tokensDTO.getLast_sync().isAfter(updatedLastSync))
            ? updatedLastSync : tokensDTO.getLast_sync();

        tokensDTO.setLast_sync(updatedLastSync);//side effect !!!!!!!!

        return getDatesBetween(lastSync, updatedLastSync);
    }

    public List<DateDetails> getDateDetailsForFitbitDeviceAndTokenDto(FitbitDevice deviceFitbit, TokensDTO tokensDTO) {
        LocalDateTime updatedLastSync = LocalDateTime.parse(deviceFitbit.getLastSyncTime());
        LocalDateTime lastSync = (tokensDTO.getLast_sync_intraday() == null ||
            tokensDTO.getLast_sync_intraday().isAfter(updatedLastSync))
            ? updatedLastSync : tokensDTO.getLast_sync_intraday();

        tokensDTO.setLast_sync_intraday(updatedLastSync);//side effect !!!!!!!!

        return getDateDetailsBetween(lastSync, updatedLastSync);
    }

    private List<DateDetails> getDatesBetween(LocalDateTime startDate, LocalDateTime endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate.toLocalDate(), endDate.toLocalDate()) + 1;
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        return IntStream.iterate(0, i -> i + 1)
            .limit(numOfDaysBetween)
            .mapToObj(i -> {
                DateDetails dateDetails = new DateDetails();
                dateDetails.setDate(startDate.plusDays(i).format(dateFormatter));
                return dateDetails;
            }).collect(Collectors.toList());
    }

    private List<DateDetails> getDateDetailsBetween(LocalDateTime startDate, LocalDateTime endDate) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(HOUR_FORMAT);
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate.toLocalDate(), endDate.toLocalDate()) + 1;

        return IntStream.iterate(0, i -> i + 1)
            .limit(numOfDaysBetween)
            .mapToObj(i -> {
                DateDetails dateDetails = new DateDetails();
                dateDetails.setDate(startDate.plusDays(i).format(dateFormatter));
                dateDetails.setEndTime("23:59");
                dateDetails.setStartTime("00:00");
                if (i == 0)
                    dateDetails.setStartTime(startDate.format(timeFormatter));
                if ( i == numOfDaysBetween - 1)
                    dateDetails.setEndTime(endDate.format(timeFormatter));
                return dateDetails;
            })
            .collect(Collectors.toList());
    }
}
