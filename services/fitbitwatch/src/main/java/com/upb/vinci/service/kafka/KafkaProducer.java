package com.upb.vinci.service.kafka;

import com.upb.vinci.config.kafka.TopicConstants;
import com.upb.vinci.domain.FitbitWatchData;
import com.upb.vinci.domain.FitbitWatchIntradayData;
import com.upb.vinci.service.kafka.util.DeviceDtoListenableFutureCallback;
import com.upb.vinci.service.kafka.util.FitbitWatchDataListenableFutureCallback;
import com.upb.vinci.service.kafka.util.FitbitWatchIntradayDataListenableFutureCallback;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.mapper.FitbitWatchDataMapper;
import com.upb.vinci.service.mapper.FitbitWatchIntradayDataMapper;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import com.vinci.io.server.service.dto.FitbitWatchIntradayDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

@Component
@Slf4j
public class KafkaProducer {
    @Qualifier("kafkaTemplate")
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Qualifier("fitbitWatchDataDTOKafkaTemplate")
    private final KafkaTemplate<String, FitbitWatchDataDTO> fitbitWatchDataDTOKafkaTemplate;
    @Qualifier("fitbitWatchIntradayDataDTOKafkaTemplate")
    private final KafkaTemplate<String, FitbitWatchIntradayDataDTO> fitbitWatchIntradayDataDTOKafkaTemplate;
    @Qualifier("deviceDTOKafkaTemplate")
    private final KafkaTemplate<String, DeviceDTO> deviceDTOKafkaTemplate;

    private final FitbitWatchDataMapper fitbitWatchDataMapper;
    private final FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper;

    public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate, KafkaTemplate<String, FitbitWatchDataDTO> fitbitWatchDataDTOKafkaTemplate,
                         KafkaTemplate<String, FitbitWatchIntradayDataDTO> fitbitWatchIntradayDataDTOKafkaTemplate, KafkaTemplate<String, DeviceDTO> deviceDTOKafkaTemplate,
                         FitbitWatchDataMapper fitbitWatchDataMapper, FitbitWatchIntradayDataMapper fitbitWatchIntradayDataMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.fitbitWatchDataDTOKafkaTemplate = fitbitWatchDataDTOKafkaTemplate;
        this.fitbitWatchIntradayDataDTOKafkaTemplate = fitbitWatchIntradayDataDTOKafkaTemplate;
        this.deviceDTOKafkaTemplate = deviceDTOKafkaTemplate;
        this.fitbitWatchDataMapper = fitbitWatchDataMapper;
        this.fitbitWatchIntradayDataMapper = fitbitWatchIntradayDataMapper;
    }

    public ListenableFuture<SendResult<String, String>> emit(final String topic, final String message){
        return kafkaTemplate.send(topic, message);
    }

    public ListenableFuture<SendResult<String, FitbitWatchDataDTO>> emitFitbitWatchData(FitbitWatchData fitbitWatchData){
        log.debug(String.format("#### -> Producing message (FitbitWatchData) -> %s", fitbitWatchData.toString()));

        ProducerRecord<String, FitbitWatchDataDTO> producerRecord = new ProducerRecord<>(TopicConstants.TOPIC_FITBIT_WATCH_DATA, fitbitWatchDataMapper.toDto(fitbitWatchData));

        ListenableFuture<SendResult<String, FitbitWatchDataDTO>> listenableFuture = fitbitWatchDataDTOKafkaTemplate.send(producerRecord);
        listenableFuture.addCallback(new FitbitWatchDataListenableFutureCallback());

        return listenableFuture;
    }

    public ListenableFuture<SendResult<String, FitbitWatchIntradayDataDTO>> emitFitbitWatchIntradayData(FitbitWatchIntradayData fitbitWatchIntradayData){
        log.debug(String.format("#### -> Producing message (FitbitWatchIntradayDataDTO) -> %s", fitbitWatchIntradayData.toString()));

        ProducerRecord<String, FitbitWatchIntradayDataDTO> producerRecord = new ProducerRecord<>(TopicConstants.TOPIC_FITBIT_WATCH_INTRADAY_DATA,fitbitWatchIntradayDataMapper.toDto(fitbitWatchIntradayData) );

        ListenableFuture<SendResult<String, FitbitWatchIntradayDataDTO>> listenableFuture = fitbitWatchIntradayDataDTOKafkaTemplate.send(producerRecord);
        listenableFuture.addCallback(new FitbitWatchIntradayDataListenableFutureCallback());

        return listenableFuture;
    }

    public ListenableFuture<SendResult<String, DeviceDTO>> emitDeviceDto(DeviceDTO deviceDTO) {
        log.debug(String.format("#### -> Producing message (DeviceDTO) -> %s", deviceDTO.toString()));

        ProducerRecord<String, DeviceDTO> producerRecord = new ProducerRecord<>(TopicConstants.TOPIC_FITBIT_DEVICE_UPDATE, deviceDTO);

        ListenableFuture<SendResult<String, DeviceDTO>> listenableFuture = deviceDTOKafkaTemplate.send(producerRecord);
        listenableFuture.addCallback(new DeviceDtoListenableFutureCallback());

        return listenableFuture;
    }
}
