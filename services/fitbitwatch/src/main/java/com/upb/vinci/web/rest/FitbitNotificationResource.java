package com.upb.vinci.web.rest;

import com.upb.vinci.concurrent.runnables.FitbitDataThread;
import com.upb.vinci.service.DataCollectionService;
import com.upb.vinci.web.rest.beans.FitbitNotification;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Context;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("api/notifications")
@Slf4j
public class FitbitNotificationResource {

    @Value("${fitbit.api.notification-verification-code}")
    private String fitBitEndPointVerificationCode;

    private final DataCollectionService dataCollectionService;

    private final ExecutorService executor;

    public FitbitNotificationResource(DataCollectionService dataCollectionService) {
        this.dataCollectionService = dataCollectionService;
        executor = Executors.newFixedThreadPool(3);
    }

    @GetMapping
    public ResponseEntity getFitBitNotification(@PathParam("version") String version, @RequestParam("verify") String verify) {

        log.info("<------ inside fitbit notification verify ------>");
        if(verify.equals(fitBitEndPointVerificationCode))
        {
            log.info("<------ fitbit subscription verify success------>");
            return ResponseEntity.noContent().build();
        }
        log.info("<------ fitbit subscription verify not applied------>");
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity getFitBitNotificationData(@Context HttpServletRequest request, @PathParam("version") String version, @RequestBody List<FitbitNotification> notificationList) {
        log.info("<------inside fitbit notification data ------>");
        Runnable fitbitDataThread = new FitbitDataThread(notificationList, dataCollectionService);
        log.info("notificationList: {}",notificationList);
        executor.execute(fitbitDataThread);
        return ResponseEntity.noContent().build();
    }

}
