package com.upb.vinci.domain;

import lombok.*;

@Data
@Getter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FitbitWatchIntradayData {
    private Long id;
    private String data;
    private Long timestamp;
    private Long deviceId;
}
