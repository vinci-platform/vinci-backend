package com.upb.vinci.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FitbitWatchData {
    private Long id;
    private String sleepData;
    private String activityData;
    private Long timestamp;
    private Long deviceId;
}
