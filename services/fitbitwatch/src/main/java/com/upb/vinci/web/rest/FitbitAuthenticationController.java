package com.upb.vinci.web.rest;

import com.upb.vinci.service.*;
import com.upb.vinci.service.dto.TokensDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/api/fitbit-auth")
public class FitbitAuthenticationController {

    private final TokensService tokensService;
    private final DeviceService deviceService;

    private final DataCollectionService dataCollectionService;

    public FitbitAuthenticationController(TokensService tokensService, DeviceService deviceService, DataCollectionService dataCollectionService) {
        this.tokensService = tokensService;
        this.deviceService = deviceService;
        this.dataCollectionService = dataCollectionService;
    }

    /**
     *
     * @param code authorization code needed to get AccessToken from fitbit servers
     * @param userExtraId userExtraId that will be connected to the Token
     * @return String message
     */
    @GetMapping("/code-mobile")
    public ResponseEntity<String> getCodeMobile(@RequestParam(value = "code") String code,
                                          @RequestParam(value = "userExtraId") Long userExtraId) {
        TokensDTO savedTokenDto = tokensService.processToken(code, userExtraId);

        if (!deviceService.registerFitbitWatchDeviceOnGateway(savedTokenDto)){
            return ResponseEntity.status(HttpStatus.CONFLICT)
                .body("Successful Fitbit Authentication, BUT there is no fitbit device for user so vinci fitbit device has been deactivated!");
        }

        dataCollectionService.collectDataForTokenInDifferentThread(savedTokenDto);

        return ResponseEntity.ok().body("Successful Fitbit Authentication.");
    }

}
