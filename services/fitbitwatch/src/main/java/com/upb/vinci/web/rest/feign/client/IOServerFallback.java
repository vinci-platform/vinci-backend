package com.upb.vinci.web.rest.feign.client;

import com.upb.vinci.domain.FitbitWatchData;
import com.upb.vinci.domain.FitbitWatchIntradayData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class IOServerFallback implements IOServerClient {
}
