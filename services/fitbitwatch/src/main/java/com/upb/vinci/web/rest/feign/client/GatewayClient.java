package com.upb.vinci.web.rest.feign.client;

import com.upb.vinci.web.rest.beans.UserDTO;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.web.rest.feign.handler.ClientExceptionHandler;
import com.upb.vinci.web.rest.feign.handler.HandleFeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@FeignClient(value = "gateway")
public interface GatewayClient {

    Long MAX_SIZE_VALUE = 0x7fff_ffff_ffff_ffffL;

    @HandleFeignException(ClientExceptionHandler.class)//this endpoint can only be reached if the request is from the same host
    @RequestMapping(method = RequestMethod.GET,value = "/api/devices/getFitbitDevice/userExtraId")
    List<DeviceDTO> getFitbitDeviceForUserExtraId(@RequestParam(value = "userExtraId") Long userExtraId);

    @HandleFeignException(ClientExceptionHandler.class)//this endpoint can only be reached if the request is from the same host
    @RequestMapping(method = RequestMethod.PUT,value = "/api/devices/updateFitbitDevice")
    DeviceDTO updateDevice(@Valid @RequestBody DeviceDTO deviceDTO);

    @HandleFeignException(ClientExceptionHandler.class)//this endpoint can only be reached if the request is from the same host
    @RequestMapping(method = RequestMethod.POST,value = "/api/devices/createFitbitDevice")
    DeviceDTO createDevice(@Valid @RequestBody DeviceDTO deviceDTO);

    @HandleFeignException(ClientExceptionHandler.class)
    @RequestMapping(method = RequestMethod.GET, value = "/api/users/getUserWithAuthorities")
    Optional<UserDTO> getUserWithAuthorities();
}
