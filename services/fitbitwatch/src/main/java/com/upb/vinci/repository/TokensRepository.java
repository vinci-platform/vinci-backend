package com.upb.vinci.repository;

import com.upb.vinci.domain.Tokens;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Tokens entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TokensRepository extends JpaRepository<Tokens, Long> {

    @Query("SELECT t FROM Tokens t WHERE t.user_id=:user_id")
    Optional<Tokens> findByUserId(@Param("user_id")String user_id);

    @Query("SELECT t FROM Tokens t WHERE t.userExtraId=:userExtraId")
    List<Tokens> findByUserExtraId(@Param("userExtraId")Long userExtraId);

    @Query("SELECT t FROM Tokens t WHERE t.user_id=:user_id")
    List<Tokens> findAllByUser_id(@Param("user_id")String user_id);
}
