package com.upb.vinci.service.impl;

import com.upb.vinci.domain.FitbitDevice;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.service.DeviceService;
import com.upb.vinci.service.webclient.FitbitWebClient;
import com.upb.vinci.service.TokensService;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.kafka.KafkaProducer;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.dto.DeviceType;
import com.upb.vinci.web.rest.feign.client.GatewayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DeviceServiceImpl implements DeviceService {

    private final GatewayClient gatewayClient;

    private final TokensService tokensService;

    private final FitbitWebClient fitbitWebClient;

    private final FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;

    private final KafkaProducer kafkaProducer;

    private final ObjectMapper objectMapper;

    public DeviceServiceImpl(GatewayClient gatewayClient, TokensService tokensService, FitbitWebClient fitbitWebClient,
                             FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler, KafkaProducer kafkaProducer) {
        this.gatewayClient = gatewayClient;
        this.tokensService = tokensService;
        this.fitbitWebClient = fitbitWebClient;
        this.fitbitWebClientExceptionHandler = fitbitWebClientExceptionHandler;
        this.kafkaProducer = kafkaProducer;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public Optional<DeviceDTO> getFitbitDeviceForUserExtraId(Long userExtraId) {
        List<DeviceDTO> fitbitDeviceForUserExtraId = gatewayClient.getFitbitDeviceForUserExtraId(userExtraId);
        createIfVinciFitbitDeviceDoesntExist(userExtraId, fitbitDeviceForUserExtraId);
        return Optional.of(fitbitDeviceForUserExtraId.stream().findFirst().get());
    }

    /**
     * returns empty optional if device is no active or when device uuid is not equal to device id of fitbit device on fitbit servers
     * returns vinci device for user that has valid uuid
     * @param tokensDTO
     * @return empty optional if device is no active or when device uuid is not equal to device id of fitbit device on fitbit servers
     */
    @Override
    public Optional<DeviceDTO> getActiveFitbitDeviceForToken(TokensDTO tokensDTO) {
        Long userExtraId = tokensDTO.getUserExtraId();
        Optional<DeviceDTO> fitbitDeviceForUserExtraId = getFitbitDeviceForUserExtraId(userExtraId);
        if (!checkIfThereIsUserIdInToken(tokensDTO) ||
            !checkIfDeviceActive(fitbitDeviceForUserExtraId.get()) ||
            !checkValidUUID(fitbitDeviceForUserExtraId.get(),tokensDTO)){
            return Optional.empty();
        }
        return fitbitDeviceForUserExtraId;
    }

    @Override
    public void deactivateFitbitDeviceForUserExtraId(Long userExtraId,String reason) {
        log.info("Deactivating fitbit device for userExtraId: {}; Reason: {}",userExtraId,reason);
        deactivateDevice(gatewayClient.getFitbitDeviceForUserExtraId(userExtraId).stream().findFirst().get(),reason);
    }

    /**
     *
     * @param tokensDTO
     * @return list of FitbitDevice for certain user_id
     */
    @Override
    public List<FitbitDevice> getDevicesFromFitbitServer(TokensDTO tokensDTO) {
        tokensService.refreshToken(tokensDTO);
        List<FitbitDevice> fitbitDevicesFromFitbitServer;
        try {
            fitbitDevicesFromFitbitServer = fitbitWebClient.getFitbitDevicesFromFitbitServer(tokensDTO);
            return fitbitDevicesFromFitbitServer;
        }catch (WebClientResponseException e) {
            log.error("Exception when getting fitbit device from fitbit severs for user_id: {}", tokensDTO.getUser_id());
            throw fitbitWebClientExceptionHandler.handleError(e);
        }
    }

    /**
     * creates Fitbit device on gateway if it doesn't exist, and returns list with newly created device
     * @param userExtraId
     * @param fitbitDeviceForUserExtraId
     */
    private void createIfVinciFitbitDeviceDoesntExist(Long userExtraId, List<DeviceDTO> fitbitDeviceForUserExtraId) {
        if (fitbitDeviceForUserExtraId.size() == 0){
            log.error("There is no vinci fitbit device. ");
            DeviceDTO createdFitbitDevice = gatewayClient.createDevice(getDefaultFitbitWatchDevice(userExtraId));
            fitbitDeviceForUserExtraId.add(createdFitbitDevice);
            log.info("Created fitbit device: {}",createdFitbitDevice);
        }
    }

    @Override
    public boolean registerFitbitWatchDeviceOnGateway(TokensDTO tokenResponse) {
        DeviceDTO vinciFitbitWatchDevice = gatewayClient.getFitbitDeviceForUserExtraId(tokenResponse.getUserExtraId()).stream().findFirst().get();
        List<FitbitDevice> listFitbitDevices = getDevicesFromFitbitServer(tokenResponse);
        if (listFitbitDevices.size() == 0) {
            vinciFitbitWatchDevice.setActive(false);
            vinciFitbitWatchDevice.setDescription("Not Connected to fitbit watch! There is no fitbit device on fitbit servers!");
            vinciFitbitWatchDevice.setUuid("n/a");
//            gatewayClient.updateDevice(vinciFitbitWatchDevice);
            updateDeviceWithKafka(vinciFitbitWatchDevice);
            log.info("Updated fitbit device with id: {}",vinciFitbitWatchDevice.getId());
            return false;
        } else {
            //there is device on fitbit servers
            vinciFitbitWatchDevice.setActive(true);
            vinciFitbitWatchDevice.setDescription("Connected to fitbit watch!");
            //here is connection with fitbit device id and vinci fitbit uuid
            vinciFitbitWatchDevice.setUuid(listFitbitDevices.stream().findFirst().get().getId());
//            gatewayClient.updateDevice(vinciFitbitWatchDevice);
            updateDeviceWithKafka(vinciFitbitWatchDevice);
            log.info("Updated fitbit device with id: {}",vinciFitbitWatchDevice.getId());
            return true;
        }
    }

    /**
     * first checks if there is device on fitbit server, if not then returns false,
     * checks if vinciDeviceDTO has uuid that is equal to id of fitbit device for that user,
     * true if they are equal, if not device on gateway is updated with new uuid
     * @param vinciDeviceDTO
     * @return true if there is, false if there is not
     */
    private boolean checkValidUUID(DeviceDTO vinciDeviceDTO,TokensDTO tokensDTO) {
        List<FitbitDevice> fitbitDeviceFromFitbitServer = getDevicesFromFitbitServer(tokensDTO);
        if (fitbitDeviceFromFitbitServer.isEmpty()) {
            log.error("Could not find a fitbit device on fitbit server connected to user id: {}.Vinci fitbit device will be deactivated!", tokensDTO.getUser_id());
            deactivateDevice(vinciDeviceDTO," Could not find a fitbit device on fitbit server!");
            return false;
        }
        if (!vinciDeviceDTO.getUuid().equals(fitbitDeviceFromFitbitServer.stream().findFirst().get().getId())){
            log.info("Vinci UUID is not equal to it of fitbit device on fitbit servers: {}.Vinci fitbit device will be updated with new uuid!",
                fitbitDeviceFromFitbitServer.stream().findFirst().get().getId());
            DeviceDTO updatedDeviceDto = updateDeviceWithNewUUID(vinciDeviceDTO, fitbitDeviceFromFitbitServer.stream().findFirst().get().getId());
            vinciDeviceDTO.setUuid(updatedDeviceDto.getUuid());
        }
        return true;
    }

    private DeviceDTO updateDeviceWithNewUUID(DeviceDTO vinciDeviceDTO, String uuid) {
        vinciDeviceDTO.setUuid(uuid);
        return gatewayClient.updateDevice(vinciDeviceDTO);
    }

    private void deactivateDevice(DeviceDTO vinciDeviceDTO,String comment) {
        DeviceDTO deviceDTO = getDefaultFitbitWatchDevice(vinciDeviceDTO.getUserExtraId());
        deviceDTO.setId(vinciDeviceDTO.getId());
        deviceDTO.setDescription(deviceDTO.getDescription()+comment);
//        gatewayClient.updateDevice(deviceDTO);
        updateDeviceWithKafka(deviceDTO);
        log.info("Vinci fitbit device with id updated: {}",deviceDTO.getId());
    }

    private boolean checkIfThereIsUserIdInToken(TokensDTO tokensDTO) {
        if (tokensDTO.getUserExtraId() <= 0L) {
            log.error("There is no userExtraId for user, he should connect again to the fitbit server!");
        }
        return tokensDTO.getUserExtraId() > 0L;
    }

    private boolean checkIfDeviceActive(DeviceDTO fitbitDeviceForUserExtraId) {
        if (!fitbitDeviceForUserExtraId.getActive()) {
            log.info("Fitbit device for user is not activated! Connect again to fitbit on dashboard on mobile app!");
        }
        return fitbitDeviceForUserExtraId.getActive();
    }

    private DeviceDTO getDefaultFitbitWatchDevice(Long userExtraId) {
        return DeviceDTO.builder()
            .deviceType(DeviceType.FITBIT_WATCH)
            .active(false)
            .description("Not connected to fitbit watch!")
            .name("Fitbit device")
            .startTimestamp(Instant.now())
            .uuid("n/a")
            .userExtraId(userExtraId)
            .build();

    }

    private void updateDeviceWithKafka(DeviceDTO vinciFitbitWatchDevice) {
            kafkaProducer.emitDeviceDto(vinciFitbitWatchDevice);
    }
}
