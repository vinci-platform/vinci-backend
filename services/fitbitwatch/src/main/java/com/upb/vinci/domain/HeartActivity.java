package com.upb.vinci.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HeartActivity {
    private String dateTime;

    private String value;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HeartValue {

        private List<String> customHeartRateZones;
        private List<HeartRateZones> heartRateZones;
        private Integer restingHeartRate;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HeartRateZones {

        private Integer caloriesOut;
        private Integer max;
        private Integer min;
        private Integer minutes;
        private String name;
    }
}
