package com.upb.vinci.error.fitbit;

import com.upb.vinci.error.response.ErrorResponse;
import com.upb.vinci.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class RefreshTokenException extends AbstractThrowableProblem {
    private static final String TITLE = "REFRESH_TOKEN_EXCEPTION";
    private static final Status STATUS_TYPE = Status.UNAUTHORIZED;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;

    public RefreshTokenException(String message) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message); }
    public RefreshTokenException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "RefreshTokenException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
