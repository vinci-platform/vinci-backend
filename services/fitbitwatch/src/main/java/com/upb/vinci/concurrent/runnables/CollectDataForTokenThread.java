package com.upb.vinci.concurrent.runnables;

import com.upb.vinci.service.DataCollectionService;
import com.upb.vinci.service.dto.TokensDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CollectDataForTokenThread implements Runnable {

    private final DataCollectionService dataCollectionService;

    private final TokensDTO tokenDTO;

    public CollectDataForTokenThread(DataCollectionService dataCollectionService, TokensDTO tokenDto) {
        this.dataCollectionService = dataCollectionService;
        this.tokenDTO = tokenDto;
    }

    @Override
    public void run() {
        dataCollectionService.retrieveFitbitWatchDataForTokenDto(tokenDTO);
        dataCollectionService.retrieveFitbitWatchIntraDatDataForTokenDto(tokenDTO);
    }
}
