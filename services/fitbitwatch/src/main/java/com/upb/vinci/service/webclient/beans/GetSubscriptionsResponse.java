package com.upb.vinci.service.webclient.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetSubscriptionsResponse {

    private List<ApiSubscription> apiSubscriptions;

}
