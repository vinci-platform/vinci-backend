package com.upb.vinci.error;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.error.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoEntityException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_ENTITY_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "No Entity found!";
    private static final String DETAILS_NO_ID = "No %s Entity found for id: %s!";
    private static final String DETAILS_NO_UUID = "No %s Entity found for uuid: %s!";

    public NoEntityException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public NoEntityException(Long id,String entityName) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_NO_ID,entityName,id)); }
    public NoEntityException(String uuid,String entityName) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_NO_UUID,entityName,uuid)); }
    public NoEntityException(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, details); }
    public NoEntityException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoEntityException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
