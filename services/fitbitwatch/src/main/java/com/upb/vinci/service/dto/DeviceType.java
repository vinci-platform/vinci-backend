package com.upb.vinci.service.dto;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    WATCH, SHOE, CAMERA_FITNESS, CAMERA_MOVEMENT, FITBIT_WATCH
}

