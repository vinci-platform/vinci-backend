package com.upb.vinci.error;

import com.upb.vinci.error.response.ErrorResponse;
import com.upb.vinci.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class FitbitNotificationException extends AbstractThrowableProblem {

    private static final String TITLE = "FITBIT_NOTIFICATION_EXCEPTION";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "There has been an fitbit notification exception!";

    public FitbitNotificationException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public FitbitNotificationException(String details) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, details); }
    public FitbitNotificationException(ErrorResponse errorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,errorResponse.getDetail()); }

    @Override
    public String toString() {
        return "AuthorizationException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
