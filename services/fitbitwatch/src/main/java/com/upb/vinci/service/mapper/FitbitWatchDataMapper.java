package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.FitbitWatchData;
import com.vinci.io.server.service.dto.FitbitWatchDataDTO;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface FitbitWatchDataMapper {

    FitbitWatchDataDTO toDto(FitbitWatchData fitbitWatchData);

    @InheritInverseConfiguration
    FitbitWatchData toEntity(FitbitWatchDataDTO fitbitWatchDataDTO);
}
