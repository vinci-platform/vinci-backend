package com.upb.vinci.service.webclient.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApiSubscription {
    private String collectionType;
    private String ownerId;
    private String ownerType;
    private String subscriberId;
    private String subscriptionId;
}
