package com.upb.vinci.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upb.vinci.domain.*;
import com.upb.vinci.error.fitbit.RefreshTokenException;
import com.upb.vinci.error.fitbit.ToManyRequestsToFitbitServerException;
import com.upb.vinci.error.response.FitbitWebClientExceptionHandler;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.kafka.KafkaProducer;
import com.upb.vinci.service.mapper.TokensMapper;
import com.upb.vinci.service.webclient.FitbitWebClient;
import com.upb.vinci.web.rest.beans.DateDetails;
import com.upb.vinci.service.dto.DeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.upb.vinci.service.DateConstants.DATE_HOUR_FORMAT;

@Service
@Slf4j
public class FitbitWatchIntraDayDataService {


    private final TokensMapper tokensMapper;

    private final DeviceService deviceService;

    private final TokensService tokensService;

    private final ObjectMapper objectMapper;

    private final FitbitWebClient fitbitWebClient;

    private final FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler;

    private final DateFormattingUtility dateFormattingUtility;

    private final KafkaProducer kafkaProducer;

    public FitbitWatchIntraDayDataService(TokensMapper tokensMapper, DeviceService deviceService,
                                          TokensService tokensService, FitbitWebClient fitbitWebClient, FitbitWebClientExceptionHandler fitbitWebClientExceptionHandler,
                                          DateFormattingUtility dateFormattingUtility, KafkaProducer kafkaProducer) {
        this.tokensMapper = tokensMapper;
        this.deviceService = deviceService;

        this.tokensService = tokensService;
        this.fitbitWebClient = fitbitWebClient;
        this.fitbitWebClientExceptionHandler = fitbitWebClientExceptionHandler;
        this.dateFormattingUtility = dateFormattingUtility;
        this.kafkaProducer = kafkaProducer;
        this.objectMapper = new ObjectMapper();
    }

    public void retrieveFitbitWatchIntraDayDataForTokenDto(TokensDTO tokensDTO) {
        List<FitbitDevice> deviceFitbit;
        try {
            deviceFitbit = deviceService.getDevicesFromFitbitServer(tokensDTO);
        } catch (ToManyRequestsToFitbitServerException ignored) {
            log.info("There has been to many requests for fitbit user_id: {}",tokensDTO.getUser_id());
            return;
        }
        if (deviceFitbit.isEmpty()) {
            log.info("Could not find a fitbit device connected to user_id: {}. Ignoring record...", tokensDTO.getUser_id());
            return;
        }
        Optional<DeviceDTO> device = deviceService.getActiveFitbitDeviceForToken(tokensDTO);
        if (!device.isPresent()) {
            return;
        }

        for (DateDetails d: dateFormattingUtility.getDateDetailsForFitbitDeviceAndTokenDto(deviceFitbit.get(0), tokensDTO)) {
            List<FitbitActivityData> fitbitWatchIntraDayData;
            try {
                tokensService.refreshToken(tokensDTO);
                fitbitWatchIntraDayData = retrieveFitbitIntraDayData(d.getDate(), d.getStartTime(), d.getEndTime(), tokensDTO);
            }catch (RefreshTokenException refreshTokenException){
                //setup new last_sync_time in token, because there is no more requests available to send to fitbit servers.
                setLastSyncInToken(tokensDTO, d);
                log.info("No more request available for date {};",d.getDate());
                break;
            } catch (Exception e){
                //setup new last_sync_time in token, because there is no more requests available to send to fitbit servers.
                setLastSyncInToken(tokensDTO, d);//todo: can be json parse exception, maybe cover that case
                log.error("Error message: {}",e.getMessage());
                break;
            }
            persistNewFitbitWatchIntraDayData(tokensDTO, device, d, fitbitWatchIntraDayData);
        }
        tokensService.justSave(tokensDTO);
    }

    private void persistNewFitbitWatchIntraDayData(TokensDTO tokensDTO, Optional<DeviceDTO> device, DateDetails d, List<FitbitActivityData> fitbitWatchIntradayData) {
        if (!fitbitWatchIntradayData.isEmpty()) {
            log.info("Received FitbitWatchIntraDayData for token with userExtraId: {}", tokensDTO.getUserExtraId());
            FitbitWatchIntradayData dataToBeSaved = buildFitbitIntraDayWatchData(fitbitWatchIntradayData, d.getDate(),  device.get().getId());
            kafkaProducer.emitFitbitWatchIntradayData(dataToBeSaved);
        } else {
            log.debug("There are no new updates to be saved. Skipping rest call to ioserver.");
        }
    }

    public void retrieveFitbitWatchIntraDayDataForToken(Tokens token) {
        retrieveFitbitWatchIntraDayDataForTokenDto(tokensMapper.toDto(token));
    }

    private  List<FitbitActivityData> retrieveFitbitIntraDayData(String date, String startTime, String endTime, TokensDTO tokensDTO) throws Exception {
        String userId = tokensDTO.getUser_id();
        try {
            List<ActivitiesIntraday.DatasetItem> heart;
            List<ActivitiesIntraday.DatasetItem> steps;
            List<ActivitiesIntraday.DatasetItem> calories;
            List<ActivitiesIntraday.DatasetItem> floors;
            List<ActivitiesIntraday.DatasetItem> elevation;
            List<ActivitiesIntraday.DatasetItem> distance;
            try {
                heart = fitbitWebClient.getHeartData(tokensDTO, startTime, endTime, date);
                steps = fitbitWebClient.getStepsData(tokensDTO, startTime, endTime, date);
                calories = fitbitWebClient.getCaloriesData(tokensDTO, startTime, endTime, date);
                floors = fitbitWebClient.getFloorsData(tokensDTO, startTime, endTime, date);
                elevation = fitbitWebClient.getElevationData(tokensDTO, startTime, endTime, date);
                distance = fitbitWebClient.getDistanceData(tokensDTO,startTime, endTime, date);
            }catch (WebClientResponseException e) {
                log.error("Exception when getting FitbitWatchIntraDayData from fitbit severs for user_id: {}", tokensDTO.getUser_id());
                throw fitbitWebClientExceptionHandler.handleError(e);//this throws exception always
            }

            Stream<FitbitActivityData> fitbitActivityDataStreamWithOnlySteps = getFitbitActivityDataStreamWithOnlySteps(date, steps);
            return aggretgateAllDataInOneList(heart, calories, floors, elevation, distance, fitbitActivityDataStreamWithOnlySteps);
        } catch (Exception exception) {
            log.error("Encountered an error when retrieving new intraday data from Fitbit for date:{} and start time: {} and end time: {} and user_id: {}",
                date, startTime, endTime, userId);
            exception.printStackTrace();
            throw new Exception(exception.getMessage());
        }
    }

    private Stream<FitbitActivityData> getFitbitActivityDataStreamWithOnlySteps(String date, List<ActivitiesIntraday.DatasetItem> steps) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_HOUR_FORMAT);
        return steps.stream().map(datasetItem -> {
            FitbitActivityData fitbitActivityData = new FitbitActivityData();
            fitbitActivityData.setTime(datasetItem.getTime());
            fitbitActivityData.setSteps(datasetItem.getValue());
            fitbitActivityData.setTimestamp(LocalDateTime.parse(date + " " + fitbitActivityData.getTime(), dateFormatter)
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
            return fitbitActivityData;
        });
    }

    private List<FitbitActivityData> aggretgateAllDataInOneList(List<ActivitiesIntraday.DatasetItem> heart,
                                                                List<ActivitiesIntraday.DatasetItem> calories,
                                                                List<ActivitiesIntraday.DatasetItem> floors,
                                                                List<ActivitiesIntraday.DatasetItem> elevation,
                                                                List<ActivitiesIntraday.DatasetItem> distance,
                                                                Stream<FitbitActivityData> fitbitActivityDataStreamWithOnlySteps) {
        return fitbitActivityDataStreamWithOnlySteps.map(data -> {
            heart.stream().filter(item -> item.getTime().equals(data.getTime())).findAny()
                .ifPresent(datasetItem -> data.setHeart(datasetItem.getValue()));
            calories.stream().filter(item -> item.getTime().equals(data.getTime())).findAny()
                .ifPresent(datasetItem -> data.setCalories(datasetItem.getValue()));
            distance.stream().filter(item -> item.getTime().equals(data.getTime())).findAny()
                .ifPresent(datasetItem -> data.setDistance(datasetItem.getValue()));
            floors.stream().filter(item -> item.getTime().equals(data.getTime())).findAny()
                .ifPresent(datasetItem -> data.setFloors(datasetItem.getValue()));
            elevation.stream().filter(item -> item.getTime().equals(data.getTime())).findAny()
                .ifPresent(datasetItem -> data.setElevation(datasetItem.getValue()));
            return data;
        }).collect(Collectors.toList());
    }

    private FitbitWatchIntradayData buildFitbitIntraDayWatchData(List<FitbitActivityData> fitbitActivityData, String date, Long deviceId) {
        try {
            return FitbitWatchIntradayData.builder()
                .data(objectMapper.writeValueAsString(fitbitActivityData))
                .timestamp(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli())
                .deviceId(deviceId)
                .build();
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Encountered an error while building a FitbitWatchData bean for saving to database.");
        }
    }

    private void setLastSyncInToken(TokensDTO tokensDTO, DateDetails dateDetails) {
        //setup new last_sync_time in token, because there is no more requests available to send to fitbit servers.
        if (dateDetails.getStartTime().equals("") || dateDetails.getStartTime() == null) dateDetails.setStartTime("00:00");
        String timeTimeString = dateDetails.getDate() + " " + dateDetails.getStartTime() + ":00";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_HOUR_FORMAT);
        tokensDTO.setLast_sync_intraday(LocalDateTime.parse(timeTimeString, dateFormatter));
    }
}
