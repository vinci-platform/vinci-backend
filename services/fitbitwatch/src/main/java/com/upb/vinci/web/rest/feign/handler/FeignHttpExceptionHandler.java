package com.upb.vinci.web.rest.feign.handler;

import feign.Response;

public interface FeignHttpExceptionHandler {
    Exception handle(Response response);
}
