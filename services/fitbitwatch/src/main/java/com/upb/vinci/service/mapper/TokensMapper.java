package com.upb.vinci.service.mapper;


import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.TokensDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tokens} and its DTO {@link TokensDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TokensMapper extends EntityMapper<TokensDTO, Tokens> {



    default Tokens fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tokens tokens = new Tokens();
        tokens.setId(id);
        return tokens;
    }
}
