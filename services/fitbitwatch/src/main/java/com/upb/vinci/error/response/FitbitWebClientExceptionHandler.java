package com.upb.vinci.error.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upb.vinci.error.InternalServerErrorException;
import com.upb.vinci.error.fitbit.FitbitGeneralException;
import com.upb.vinci.error.fitbit.RefreshTokenException;
import com.upb.vinci.error.fitbit.ToManyRequestsToFitbitServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.zalando.problem.AbstractThrowableProblem;

@Component
@Slf4j
public class FitbitWebClientExceptionHandler {

    private final ObjectMapper objectMapper;

    public FitbitWebClientExceptionHandler() {
        this.objectMapper = new ObjectMapper();
    }

    public AbstractThrowableProblem handleError(WebClientResponseException exception) {
        HttpStatus statusCode = exception.getStatusCode();
        log.info("status code: {}",statusCode);
        if (statusCode.is4xxClientError()) return handleBadRequest(exception);
        if (statusCode.is5xxServerError()) return handleInternalServerError(exception);

        return new FitbitGeneralException(exception.getResponseBodyAsString());
    }

    private AbstractThrowableProblem handleBadRequest(WebClientResponseException exception) {
        log.debug("Bad request error from fitbit servers: {}",exception.getResponseBodyAsString());
        if (exception.getStatusCode().equals(HttpStatus.TOO_MANY_REQUESTS)) return new ToManyRequestsToFitbitServerException();
        if (isRefreshTokenException(exception)) return new RefreshTokenException("Refresh token is invalid! Error response from fitbit server: " + exception.getResponseBodyAsString());
        return new FitbitGeneralException(exception.getResponseBodyAsString());
    }

    private AbstractThrowableProblem handleInternalServerError(WebClientResponseException exception) {
        log.debug("Internal server error from fitbit servers: {}",exception.getResponseBodyAsString());
        return new FitbitGeneralException(exception.getResponseBodyAsString());
    }

    private FitbitErrorResponse getResponseBody(String responseBodyAsString) {
        FitbitErrorResponse fitbitErrorResponse;
        try {
            fitbitErrorResponse = objectMapper.readValue(responseBodyAsString, FitbitErrorResponse.class);
        } catch (JsonProcessingException e) {
            log.error("Error parsing fitbit error response: " + responseBodyAsString);
            e.printStackTrace();
            throw new InternalServerErrorException("Error when parsing FitbitErrorResponse from fitbit server. Response: " + responseBodyAsString);
        }
        return fitbitErrorResponse;
    }

    private boolean isRefreshTokenException(WebClientResponseException exception) {
        FitbitErrorResponse responseBody = getResponseBody(exception.getResponseBodyAsString());
        return responseBody.getErrors().stream()
            .anyMatch(fitbitError -> fitbitError.getMessage().contains("Refresh token invalid") &&
            fitbitError.getErrorType().equals("invalid_grant"));
    }
}
