package com.upb.vinci.web.rest;

import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.TokensService;
import com.upb.vinci.service.dto.TokensDTO;

import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing {@link com.upb.vinci.domain.Tokens}.
 */
@RestController
@RequestMapping("/api")
public class TokensResource {

    private final Logger log = LoggerFactory.getLogger(TokensResource.class);

    private static final String ENTITY_NAME = "fitbitwatchTokens";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TokensService tokensService;

    public TokensResource(TokensService tokensService) {
        this.tokensService = tokensService;
    }

    /**
     * {@code POST  /tokens} : Create a new tokens.
     *
     * @param tokensDTO the tokensDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tokensDTO, or with status {@code 400 (Bad Request)} if the tokens has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tokens")
    public ResponseEntity<TokensDTO> createTokens(@RequestBody @Valid TokensDTO tokensDTO) throws URISyntaxException {
        log.debug("REST request to save Tokens : {}", tokensDTO);
        TokensDTO result = tokensService.save(tokensDTO);
        return ResponseEntity.created(new URI("/api/tokens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tokens} : Updates an existing tokens.
     *
     * @param tokensDTO the tokensDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tokensDTO,
     * or with status {@code 400 (Bad Request)} if the tokensDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tokensDTO couldn't be updated.
     */
    @PutMapping("/tokens")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TokensDTO> updateTokens(@RequestBody @Valid TokensDTO tokensDTO) {
        log.debug("REST request to update Tokens : {}", tokensDTO);
        TokensDTO result = tokensService.justUpdate(tokensDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tokensDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tokens} : You can get all tokens only if you are admin user, else you will get only token from current user
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tokens in body.
     */
    @GetMapping("/tokens")
    public List<TokensDTO> getAllTokens() {
        log.debug("REST request to get all Tokens");
        return tokensService.findAll();
    }

    /**
     * {@code GET  /tokens/:id} : get the "id" tokens.
     *
     * @param id the id of the tokensDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tokensDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tokens/{id}")
    public ResponseEntity<TokensDTO> getTokens(@PathVariable Long id) {
        log.debug("REST request to get Tokens by id: {}", id);
        return ResponseEntity.ok(tokensService.findOne(id));
    }

    /**
     * {@code DELETE  /tokens/:id} : delete the "id" tokens.
     *
     * @param id the id of the tokensDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tokens/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteTokens(@PathVariable Long id) {
        log.debug("REST request to delete Tokens : {}", id);
        tokensService.justDelete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
