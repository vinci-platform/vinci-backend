package com.upb.vinci.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActivityData {
    ActivitySummary summary;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ActivitySummary {
        Integer activityCalories;
        Integer caloriesBMR;
        Integer caloriesOut;
        Integer elevation;
        Integer fairlyActiveMinutes;
        Integer floors;
        Integer lightlyActiveMinutes;
        Integer marginalCalories;
        Integer restingHeartRate;
        Integer sedentaryMinutes;
        Integer steps;
        Integer veryActiveMinutes;
        List<HeartActivity.HeartRateZones> heartRateZones;
        List<Distance> distances;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Distance {
        String activity;
        String distance;
    }
}

