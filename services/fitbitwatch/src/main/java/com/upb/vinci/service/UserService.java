package com.upb.vinci.service;

import com.upb.vinci.error.NoUserForTokenException;
import com.upb.vinci.web.rest.beans.UserDTO;
import com.upb.vinci.web.rest.feign.client.GatewayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private final GatewayClient gatewayClient;

    public UserService(GatewayClient gatewayClient) {
        this.gatewayClient = gatewayClient;
    }

    public Optional<UserDTO> getCurrentUserDto(){
        UserDTO currentUser = gatewayClient.getUserWithAuthorities()
            .orElseThrow(NoUserForTokenException::new);

        return Optional.of(currentUser);
    }
}
