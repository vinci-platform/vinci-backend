package com.upb.vinci.service;

public class DateConstants {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String HOUR_FORMAT = "HH:mm";
    public static final String DATE_HOUR_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
