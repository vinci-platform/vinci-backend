package com.upb.vinci.error;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.error.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserForTokenException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_FOR_TOKEN_EXCEPTION";
    private static final Status STATUS_TYPE = Status.UNAUTHORIZED;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "No user found for token!";

    public NoUserForTokenException(){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS);
    }
    public NoUserForTokenException(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }
    public NoUserForTokenException(ErrorResponse feignErrorResponse) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail());
    }

    @Override
    public String toString() {
        return "NoUserForTokenException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}

