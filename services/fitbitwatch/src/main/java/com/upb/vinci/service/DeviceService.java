package com.upb.vinci.service;

import com.upb.vinci.domain.FitbitDevice;
import com.upb.vinci.service.dto.TokensDTO;
import com.upb.vinci.service.dto.DeviceDTO;

import java.util.List;
import java.util.Optional;

public interface DeviceService {

    Optional<DeviceDTO> getFitbitDeviceForUserExtraId(Long userExtraId);

    Optional<DeviceDTO> getActiveFitbitDeviceForToken(TokensDTO tokensDTO);

    void deactivateFitbitDeviceForUserExtraId(Long userExtraId,String reason);

    List<FitbitDevice> getDevicesFromFitbitServer(TokensDTO tokensDTO);

    boolean registerFitbitWatchDeviceOnGateway(TokensDTO tokenResponse);
}
