package com.upb.vinci.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActivitiesSteps {

    @JsonProperty("activities-steps")
    List<Activity> activitiesSteps;

    @JsonProperty("activities-steps-intraday")
    ActivitiesIntraday activitiesStepsIntraday;
}
