package com.upb.vinci.web.rest.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FitbitNotification {

    private String collectionType;
    private String ownerId;
    private String ownerType;
    private String subscriberId;
    private String subscriptionId;
    private String date;

}
