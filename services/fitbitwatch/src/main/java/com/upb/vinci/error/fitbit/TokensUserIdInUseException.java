package com.upb.vinci.error.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.error.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class TokensUserIdInUseException extends AbstractThrowableProblem {
    private static final String TITLE = "TOKEN_USER_ID_IN_USE_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "User_id from received token is used by another user!\nThis means another user is using that same fitbit account which is not supported!";
    private static final String DETAILS_WITH_USER_ID = "User_id %s from received token is used by another user!\nThis means another user is using that same fitbit account which is not supported!";

    public TokensUserIdInUseException() {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE, DEFAULT_DETAILS);
    }
    public TokensUserIdInUseException(String user_id) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_USER_ID,user_id)); }

    public TokensUserIdInUseException(ErrorResponse errorResponse) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE, errorResponse.getDetail());
    }

    @Override
    public String toString() {
        return "TokensUserIdInUseException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
