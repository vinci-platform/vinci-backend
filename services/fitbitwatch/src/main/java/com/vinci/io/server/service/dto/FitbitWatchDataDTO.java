package com.vinci.io.server.service.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FitbitWatchDataDTO implements Serializable {
    private Long id;
    private String sleepData;
    private String activityData;
    private Long timestamp;
    private Long deviceId;
}
