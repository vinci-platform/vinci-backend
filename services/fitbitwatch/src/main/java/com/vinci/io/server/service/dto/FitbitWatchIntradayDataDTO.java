package com.vinci.io.server.service.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Getter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FitbitWatchIntradayDataDTO implements Serializable {
    private Long id;
    private String data;
    private Long timestamp;
    private Long deviceId;
}
