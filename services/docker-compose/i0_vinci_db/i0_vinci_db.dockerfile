FROM ubuntu:20.04

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV TZ=Europe/Minsk
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get install -y \
    software-properties-common \
    postgresql

# Create app directory
WORKDIR /app

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get dist-upgrade -y
RUN apt-get install -y tzdata
RUN apt-get install -y sudo
RUN apt-get install curl \
htop \
vim \
nano \
lsof \
wget \
net-tools \
-y

# switch user
USER postgres

# create user & databases
RUN /etc/init.d/postgresql start && \
    psql --command "CREATE USER gateway WITH SUPERUSER PASSWORD null;" && \
    createdb -O gateway "gateway" && \
    psql --command "CREATE USER aggregator WITH SUPERUSER PASSWORD null;" && \
    createdb -O aggregator "aggregator" && \
    psql --command "CREATE USER ioserver WITH SUPERUSER PASSWORD null;" && \
    createdb -O ioserver "ioserver" && \
    psql --command "CREATE USER watch WITH SUPERUSER PASSWORD null;" && \
    createdb -O watch "watch" && \ 
    psql --command "CREATE USER survey WITH SUPERUSER PASSWORD null;" && \
    createdb -O survey "survey" && \ 
    psql --command "CREATE USER shoe WITH SUPERUSER PASSWORD null;" && \
    createdb -O shoe "shoe" && \
    psql --command "CREATE USER devices WITH SUPERUSER PASSWORD null;" && \
    createdb -O devices "devices" && \
    psql --command "CREATE USER cameramovement WITH SUPERUSER PASSWORD null;" && \
    createdb -O cameramovement "cameramovement" && \
    psql --command "CREATE USER \"cameraFitness\" WITH SUPERUSER PASSWORD null;" && \
    createdb -O cameraFitness "cameraFitness" && \
    psql --command "CREATE USER fitbitwatch WITH SUPERUSER PASSWORD null;" && \
    createdb -O fitbitwatch "fitbitwatch"


ENV PGPASSWORD=postgres
RUN /etc/init.d/postgresql start

# update PostgreSQL configuration - allow connections
RUN echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/12/main/pg_hba.conf
RUN echo "host all  all    ::/0       trust" >> /etc/postgresql/12/main/pg_hba.conf

# and add ``listen_addresses`` to ``/etc/postgresql/12/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/12/main/postgresql.conf

#setting up commands for the ability to backup and restore database
RUN mkdir /var/lib/postgresql/pg_log_archive


# expose the PostgreSQL port
EXPOSE 5432

# add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# set the default command to run when starting the container
CMD ["/usr/lib/postgresql/12/bin/postgres", "-D", "/var/lib/postgresql/12/main", "-c", "config_file=/etc/postgresql/12/main/postgresql.conf"]

