download-new-images-development()
{
VinciProjects=(
    gateway
    io-server
    watch
    shoe
    fitbitwatch
    survey
)

length=${#VinciProjects[@]}
gitlabProjectsGroup=vinci-aal
imageTag=development
for (( i = 0; i < length; i++ )); do
  echo "$(date +"%d.%m.%Y %T") [vINCI] ($i/$length) Downlaoding image registry.gitlab.com/${gitlabProjectsGroup}/${VinciProjects[i]}:${imageTag}"
  docker pull registry.gitlab.com/"${gitlabProjectsGroup}"/"${VinciProjects[i]}":"${imageTag}"
done
}
(download-new-images-development)
