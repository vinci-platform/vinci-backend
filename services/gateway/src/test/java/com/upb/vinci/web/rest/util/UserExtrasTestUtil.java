package com.upb.vinci.web.rest.util;

import com.upb.vinci.domain.*;
import com.upb.vinci.domain.enumeration.EducationType;
import com.upb.vinci.security.AuthoritiesConstants;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;

@Component
public class UserExtrasTestUtil {
    public static final String PACIENT_UUID = UUID.randomUUID().toString();
    public static final String PACIENT_DESCRIPTION = "pacient-description";
    public static final String PACIENT_PHONE = "pacient-phone";
    public static final String PACIENT_ADDRESS = "pacient-address";
    public static final String PACIENT_GENDER = "MALE";
    public static final EducationType PACIENT_EDUCATION_TYPE = EducationType.PRIMARY_SCHOOL;
    public static final String PACIENT_MARITAL_STATUS = "AAAA";
    public static final String PACIENT_FRIENDS = "friends";

    public static final String FAMILY_UUID = UUID.randomUUID().toString();
    public static final String FAMILY_DESCRIPTION = "family-description";
    public static final String FAMILY_PHONE = "family-phone";
    public static final String FAMILY_ADDRESS = "family-address";
    public static final String FAMILY_GENDER = "MALE";
    public static final EducationType FAMILY_EDUCATION_TYPE = EducationType.PRIMARY_SCHOOL;
    public static final String FAMILY_MARITAL_STATUS = "AAAA";
    public static final String FAMILY_FRIENDS = "friends";

    public static final String ORGANIZATION_UUID = UUID.randomUUID().toString();
    public static final String ORGANIZATION_DESCRIPTION = "organization-description";
    public static final String ORGANIZATION_PHONE = "organization-phone";
    public static final String ORGANIZATION_ADDRESS = "organization-address";
    public static final String ORGANIZATION_GENDER = "MALE";
    public static final EducationType ORGANIZATION_EDUCATION_TYPE = EducationType.PRIMARY_SCHOOL;
    public static final String ORGANIZATION_MARITAL_STATUS = "AAAA";
    public static final String ORGANIZATION_FRIENDS = "friends";

    public static final String ADMIN_UUID = UUID.randomUUID().toString();
    public static final String ADMIN_DESCRIPTION = "admin-description";
    public static final String ADMIN_PHONE = "admin-phone";
    public static final String ADMIN_ADDRESS = "admin-address";
    public static final String ADMIN_GENDER = "MALE";
    public static final EducationType ADMIN_EDUCATION_TYPE = EducationType.PRIMARY_SCHOOL;
    public static final String ADMIN_MARITAL_STATUS = "AAAA";
    public static final String ADMIN_FRIENDS = "friends";

    /**
     * Returns entity with fields AND with NO id. Entities are not persisted or injected with liquibase.
     * UUID,Desc,phone,address,gender,education,mar status,med id,sur id,friends ARE SET
     * Id,User,Family,Organization,Devices,Images,Alerts ARE SET FROM METHOD PARAMETERS
     * @param role from AuthoritiesConstants
     * @return UserExtra.java with no id
     */
    public UserExtra createUserExtraEntity(String role, User user, User family, User organization, Set<Device> devices, Set<UserImage> images, Set<UserAlert> alerts) {
        UserExtra userExtra = createUserExtraEntity(role);
        userExtra.setUser(user);
        userExtra.setFamily(family);
        userExtra.setOrganization(organization);
        userExtra.setDevices(devices);
        userExtra.setImages(images);
        userExtra.setAlerts(alerts);
        return userExtra;
    }

    /**
     * Returns entity with fields() AND with no id. Entities are not persisted or injected with liquibase.
     * UUID,Desc,phone,address,gender,education,mar status,med id,sur id,friends ARE SET
     * Id,User,Family,Organization,Devices,Images,Alerts ARE NOT SET
     * @param role from AuthoritiesConstants
     * @return UserExtra.java with fields: UUID,Desc,phone,address,gender,education,mar status,med id,sur id,friends
     */
    public UserExtra createUserExtraEntity(String role) {
        UserExtra userExtra = new UserExtra();
        switch (role) {
            case AuthoritiesConstants.PACIENT:
                userExtra.setUuid(PACIENT_UUID);
                userExtra.setDescription(PACIENT_DESCRIPTION);
                userExtra.setPhone(PACIENT_PHONE);
                userExtra.setAddress(PACIENT_ADDRESS);
                userExtra.setGender(PACIENT_GENDER);
                userExtra.setEducation(PACIENT_EDUCATION_TYPE);
                userExtra.setMaritalStatus(PACIENT_MARITAL_STATUS);
                userExtra.setFriends(PACIENT_FRIENDS);
                break;
            case AuthoritiesConstants.FAMILY:
                userExtra.setUuid(FAMILY_UUID);
                userExtra.setDescription(FAMILY_DESCRIPTION);
                userExtra.setPhone(FAMILY_PHONE);
                userExtra.setAddress(FAMILY_ADDRESS);
                userExtra.setGender(FAMILY_GENDER);
                userExtra.setEducation(FAMILY_EDUCATION_TYPE);
                userExtra.setMaritalStatus(FAMILY_MARITAL_STATUS);
                userExtra.setFriends(FAMILY_FRIENDS);
                break;
            case AuthoritiesConstants.ORGANIZIATION:
                userExtra.setUuid(ORGANIZATION_UUID);
                userExtra.setDescription(ORGANIZATION_DESCRIPTION);
                userExtra.setPhone(ORGANIZATION_PHONE);
                userExtra.setAddress(ORGANIZATION_ADDRESS);
                userExtra.setGender(ORGANIZATION_GENDER);
                userExtra.setEducation(ORGANIZATION_EDUCATION_TYPE);
                userExtra.setMaritalStatus(ORGANIZATION_MARITAL_STATUS);
                userExtra.setFriends(ORGANIZATION_FRIENDS);
                break;
            case AuthoritiesConstants.ADMIN:
                userExtra.setUuid(ADMIN_UUID);
                userExtra.setDescription(ADMIN_DESCRIPTION);
                userExtra.setPhone(ADMIN_PHONE);
                userExtra.setAddress(ADMIN_ADDRESS);
                userExtra.setGender(ADMIN_GENDER);
                userExtra.setEducation(ADMIN_EDUCATION_TYPE);
                userExtra.setMaritalStatus(ADMIN_MARITAL_STATUS);
                userExtra.setFriends(ADMIN_FRIENDS);
                break;
        }
        return userExtra;
    }

}
