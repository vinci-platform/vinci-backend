package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.MobileAppSettings;
import com.upb.vinci.domain.User;
import com.upb.vinci.repository.MobileAppSettingsRepository;
import com.upb.vinci.service.MobileAppSettingsQueryService;
import com.upb.vinci.service.MobileAppSettingsService;
import com.upb.vinci.service.dto.MobileAppSettingsDTO;
import com.upb.vinci.service.mapper.MobileAppSettingsMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MobileAppSettingsResource REST controller.
 *
 * @see MobileAppSettingsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class MobileAppSettingsResourceIntTest extends UsersSetupController{

    private static final String API_VERSION = "/api";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 4L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Long USER_ID_FOR_GENERAL_SETTINGS = 0L;
    private static final String VALUE_FOR_GENERAL_SETTINGS_VALUE = "global-value";
    private static final String NAME_FOR_GENERAL_SETTINGS = "global-name";

    @Autowired
    private MobileAppSettingsRepository mobileAppSettingsRepository;

    @Autowired
    private MobileAppSettingsMapper mobileAppSettingsMapper;

    @Autowired
    private MobileAppSettingsService mobileAppSettingsService;

    @Autowired
    private MobileAppSettingsQueryService mobileAppSettingsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMobileAppSettingsMockMvc;

    private MobileAppSettings mobileAppSettings;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MobileAppSettingsResource mobileAppSettingsResource = new MobileAppSettingsResource(mobileAppSettingsService, mobileAppSettingsQueryService);
        this.restMobileAppSettingsMockMvc = MockMvcBuilders.standaloneSetup(mobileAppSettingsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MobileAppSettings createEntity(EntityManager em) {
        return new MobileAppSettings()
            .name(DEFAULT_NAME)
            .value(DEFAULT_VALUE)
            .userId(DEFAULT_USER_ID);
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public void persistGlobalSettings() {
        mobileAppSettingsRepository.saveAndFlush(new MobileAppSettings()
            .name(NAME_FOR_GENERAL_SETTINGS)
            .value(VALUE_FOR_GENERAL_SETTINGS_VALUE)
            .userId(USER_ID_FOR_GENERAL_SETTINGS));
    }

    private void initTest() {
        initUsers();

        mobileAppSettings = createEntity(em);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_WithExistingId() throws Exception {
        // Create the MobileAppSettings with an existing ID
        mobileAppSettings.setId(1L);

        createMobileAppSettingsRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_NameIsNull() throws Exception {
        // set the field null
        mobileAppSettings.setName(null);

        // Create the MobileAppSettings, which fails.
        createMobileAppSettingsRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsPacient() throws Exception {
        createMobileAppSettingsRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsPacient_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);

        createMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsFamily() throws Exception {
        mobileAppSettings.setUserId(FAMILY_USER_ID);

        createMobileAppSettingsRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsFamily_DifferentUserId() throws Exception {
        Long differentUsersUserId = userOrganization.getId();
        mobileAppSettings.setUserId(differentUsersUserId);

        createMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_OfPacient_AsFamily_NotAssociated() throws Exception {
//        associatePacientWithFamily(userExtraPacient,userFamily);

        createMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsOrganization() throws Exception {
        mobileAppSettings.setUserId(ORGANIZATION_USER_ID);

        createMobileAppSettingsRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AsOrganization_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);

        createMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_OfPacient_AsOrganization_NotAssoicated() throws Exception {
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createMobileAppSettings_AlredyExists_NoDuplicateSettings() throws Exception {
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        em.clear();
        mobileAppSettings.setId(null);
        mobileAppSettings.setValue(UPDATED_VALUE);

        createMobileAppSettingsRestCall_IsUpdatedValue_IfUserIdAndNameAreTheSame();
    }

    private void createMobileAppSettingsRestCall_IsUpdatedValue_IfUserIdAndNameAreTheSame() throws Exception {
        // Create the MobileAppSettings
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeCreate = mobileAppSettingsRepository.findAll().size();
        restMobileAppSettingsMockMvc.perform(post(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.value").value(UPDATED_VALUE))
            .andExpect(jsonPath("$.userId").value(mobileAppSettings.getUserId().intValue()));

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeCreate);
        MobileAppSettings testMobileAppSettings = mobileAppSettingsList.get(mobileAppSettingsList.size() - 1);
        assertThat(testMobileAppSettings.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMobileAppSettings.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testMobileAppSettings.getUserId()).isEqualTo(mobileAppSettings.getUserId());
    }

    private void createMobileAppSettingsRestCall_IsCreated() throws Exception {
        // Create the MobileAppSettings
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeCreate = mobileAppSettingsRepository.findAll().size();
        restMobileAppSettingsMockMvc.perform(post(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.userId").value(mobileAppSettings.getUserId().intValue()));

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeCreate + 1);
        MobileAppSettings testMobileAppSettings = mobileAppSettingsList.get(mobileAppSettingsList.size() - 1);
        assertThat(testMobileAppSettings.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMobileAppSettings.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testMobileAppSettings.getUserId()).isEqualTo(mobileAppSettings.getUserId());
    }

    private void createMobileAppSettingsRestCall_IsBadRequest() throws Exception {
        // Create the MobileAppSettings, which fails.
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        restMobileAppSettingsMockMvc.perform(post(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isBadRequest());

        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
    }

    private void createMobileAppSettingsRestCall_IsUnauthorized() throws Exception {
        // Create the MobileAppSettings, which fails.
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        restMobileAppSettingsMockMvc.perform(post(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isUnauthorized());

        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_NoEntityInDB() throws Exception {
        // Create the MobileAppSettings
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMobileAppSettingsMockMvc.perform(put(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_NameIsNull() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        em.clear();
        mobileAppSettings.setName(null);

        updateMobileAppSettingsRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsPacient() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsOk(mobileAppSettings.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsFamily() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(FAMILY_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsOk(mobileAppSettings.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userOrganization.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_OfPacient_AsFamily_NotAssocianted() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsOrganization() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(ORGANIZATION_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsOk(mobileAppSettings.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateMobileAppSettings_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        updateMobileAppSettingsRestCall_IsUnauthorized();
    }

    private void updateMobileAppSettingsRestCall_IsOk(Long updatedUserId) throws Exception {
        // Update the mobileAppSettings
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        MobileAppSettings updatedMobileAppSettings = mobileAppSettingsRepository.findById(mobileAppSettings.getId()).get();
        // Disconnect from session so that the updates on updatedMobileAppSettings are not directly saved in db
        em.detach(updatedMobileAppSettings);
        updatedMobileAppSettings
            .name(UPDATED_NAME)
            .value(UPDATED_VALUE)
            .userId(updatedUserId);
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(updatedMobileAppSettings);

        restMobileAppSettingsMockMvc.perform(put(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(UPDATED_NAME))
            .andExpect(jsonPath("$.value").value(UPDATED_VALUE))
            .andExpect(jsonPath("$.userId").value(updatedUserId.intValue()));

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        mobileAppSettingsList.forEach(System.out::println);
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
        MobileAppSettings testMobileAppSettings = mobileAppSettingsList.get(mobileAppSettingsList.size() - 1);
        assertThat(testMobileAppSettings.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMobileAppSettings.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testMobileAppSettings.getUserId()).isEqualTo(updatedUserId);
    }

    private void updateMobileAppSettingsRestCall_IsBadRequest() throws Exception {
        // Create the MobileAppSettings
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMobileAppSettingsMockMvc.perform(put(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
        MobileAppSettings testMobileAppSettings = mobileAppSettingsList.get(mobileAppSettingsList.size() - 1);
        assertThat(testMobileAppSettings.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMobileAppSettings.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testMobileAppSettings.getUserId()).isEqualTo(mobileAppSettings.getUserId());
    }

    private void updateMobileAppSettingsRestCall_IsUnauthorized() throws Exception {
        // Create the MobileAppSettings
        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsMapper.toDto(mobileAppSettings);
        int databaseSizeBeforeTest = mobileAppSettingsRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMobileAppSettingsMockMvc.perform(put(API_VERSION+"/mobile-app-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mobileAppSettingsDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the MobileAppSettings in the database
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeTest);
        MobileAppSettings testMobileAppSettings = mobileAppSettingsList.get(mobileAppSettingsList.size() - 1);
        assertThat(testMobileAppSettings.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMobileAppSettings.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testMobileAppSettings.getUserId()).isEqualTo(mobileAppSettings.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsPacient() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsPacient_WithGeneralSettings() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        persistGlobalSettings();

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_WithGeneralSettings_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsFamily() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(FAMILY_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userOrganization.getId();
        mobileAppSettings.setUserId(differentUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        // Get all the mobileAppSettingsList
        getAllMobileAppSettingsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsOrganization() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(ORGANIZATION_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getAllMobileAppSettingsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getAllMobileAppSettingsRestCall_EmptyList();
    }


    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getAllMobileAppSettingsRestCall_EmptyList();
    }

    private void getAllMobileAppSettingsRestCall_IsOk() throws Exception {
        // Get all the mobileAppSettingsList
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mobileAppSettings.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(mobileAppSettings.getUserId().intValue())));
    }

    private void getAllMobileAppSettingsRestCall_WithGeneralSettings_IsOk() throws Exception {
        // Get all the mobileAppSettingsList
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(2)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mobileAppSettings.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(mobileAppSettings.getUserId().intValue())))

            .andExpect(jsonPath("$.[*].name").value(hasItem(NAME_FOR_GENERAL_SETTINGS)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(VALUE_FOR_GENERAL_SETTINGS_VALUE)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(USER_ID_FOR_GENERAL_SETTINGS.intValue())));
    }

    private void getAllMobileAppSettingsRestCall_EmptyList() throws Exception {
        // Get all the mobileAppSettingsList
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        User validUser = userPacient;
        User differentUser = userOrganization;
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        User validUser = userFamily;
        User differentUser = userOrganization;
        mobileAppSettings.setUserId(validUser.getId());
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        associatePacientWithFamily(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllMobileAppSettings_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        User validUser = userOrganization;
        User differentUser = userFamily;
        mobileAppSettings.setUserId(validUser.getId());
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);
        associatePacientWithOrganization(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultMobileAppSettingsShouldBeFound(validFilter,mobileAppSettings);
        defaultMobileAppSettingsShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"name",DEFAULT_NAME,UPDATED_NAME);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"value",DEFAULT_NAME,UPDATED_NAME);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",mobileAppSettings.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultMobileAppSettingsShouldBeFound(filter+validValue,mobileAppSettings);
        defaultMobileAppSettingsShouldNotBeFound(filter+invalidValue);
        defaultMobileAppSettingsShouldBeFound(validFilter+"&"+filter+validValue,mobileAppSettings);
        defaultMobileAppSettingsShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultMobileAppSettingsShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultMobileAppSettingsShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }


    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMobileAppSettingsShouldBeFound(String filter, MobileAppSettings _mobileAppSettings) throws Exception {
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_mobileAppSettings.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(_mobileAppSettings.getUserId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMobileAppSettingsShouldNotBeFound(String filter) throws Exception {
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getMobileAppSettingsById_NoUserForToken() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsPacient() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsFamily() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(FAMILY_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userOrganization.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }


    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_OfPacient_AsFamily_NotAssocaited() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsOrganization() throws Exception {
        // Initialize the database
        mobileAppSettings.setUserId(ORGANIZATION_USER_ID);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        mobileAppSettings.setUserId(differentUsersUserId);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }



    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getMobileAppSettingsById_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        getMobileAppSettingsByIdRestCall_IsUnauthorized();
    }

    private void getMobileAppSettingsByIdRestCall_IsUnauthorized() throws Exception {
        // Get the mobileAppSettings
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings/{id}", mobileAppSettings.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getMobileAppSettingsByIdRestCall_IsOk() throws Exception {
        // Get the mobileAppSettings
        restMobileAppSettingsMockMvc.perform(get(API_VERSION+"/mobile-app-settings/{id}", mobileAppSettings.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mobileAppSettings.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.userId").value(mobileAppSettings.getUserId().intValue()));
    }

    @Test
    @Transactional
    public void deleteMobileAppSettings() throws Exception {
        // Initialize the database
        mobileAppSettingsRepository.saveAndFlush(mobileAppSettings);

        int databaseSizeBeforeDelete = mobileAppSettingsRepository.findAll().size();

        // Delete the mobileAppSettings
        restMobileAppSettingsMockMvc.perform(delete(API_VERSION+"/mobile-app-settings/{id}", mobileAppSettings.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MobileAppSettings> mobileAppSettingsList = mobileAppSettingsRepository.findAll();
        assertThat(mobileAppSettingsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MobileAppSettings.class);
        MobileAppSettings mobileAppSettings1 = new MobileAppSettings();
        mobileAppSettings1.setId(1L);
        MobileAppSettings mobileAppSettings2 = new MobileAppSettings();
        mobileAppSettings2.setId(mobileAppSettings1.getId());
        assertThat(mobileAppSettings1).isEqualTo(mobileAppSettings2);
        mobileAppSettings2.setId(2L);
        assertThat(mobileAppSettings1).isNotEqualTo(mobileAppSettings2);
        mobileAppSettings1.setId(null);
        assertThat(mobileAppSettings1).isNotEqualTo(mobileAppSettings2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MobileAppSettingsDTO.class);
        MobileAppSettingsDTO mobileAppSettingsDTO1 = new MobileAppSettingsDTO();
        mobileAppSettingsDTO1.setId(1L);
        MobileAppSettingsDTO mobileAppSettingsDTO2 = new MobileAppSettingsDTO();
        assertThat(mobileAppSettingsDTO1).isNotEqualTo(mobileAppSettingsDTO2);
        mobileAppSettingsDTO2.setId(mobileAppSettingsDTO1.getId());
        assertThat(mobileAppSettingsDTO1).isEqualTo(mobileAppSettingsDTO2);
        mobileAppSettingsDTO2.setId(2L);
        assertThat(mobileAppSettingsDTO1).isNotEqualTo(mobileAppSettingsDTO2);
        mobileAppSettingsDTO1.setId(null);
        assertThat(mobileAppSettingsDTO1).isNotEqualTo(mobileAppSettingsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mobileAppSettingsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mobileAppSettingsMapper.fromId(null)).isNull();
    }
}
