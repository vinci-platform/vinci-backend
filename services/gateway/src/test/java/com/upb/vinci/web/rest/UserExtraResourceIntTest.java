package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.EducationType;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.security.jwt.JWTFilter;
import com.upb.vinci.security.jwt.TokenProvider;
import com.upb.vinci.service.UserExtraService;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.UserExtraMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import com.upb.vinci.web.rest.util.UserExtrasTestUtil;
import com.upb.vinci.web.rest.util.UsersTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.zalando.problem.Status;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserExtraResource REST controller.
 *
 * @see UserExtraResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class UserExtraResourceIntTest extends UsersSetupController{

    private static final String DEFAULT_UUID = UUID.randomUUID().toString();
    private static final String UPDATED_UUID = UUID.randomUUID().toString();

    private static final String DEFAULT_DESCRIPTION = "pacient-description";
    private static final String UPDATED_DESCRIPTION = "pacient-description-updated";

    private static final String DEFAULT_PHONE = "pacient-phone";
    private static final String UPDATED_PHONE = "pacient-phone-updated";

    private static final String DEFAULT_ADDRESS = "pacient-address";
    private static final String UPDATED_ADDRESS = "pacient-address-updated";

    private static final String DEFAULT_GENDER = "MALE";
    private static final String UPDATED_GENDER = "FEMALE";

    private static final EducationType DEFAULT_EDUCATION_TYPE = EducationType.PRIMARY_SCHOOL;
    private static final EducationType UPDATED_EDUCATION_TYPE = EducationType.SECONDARY_SCHOOL;

    private static final String DEFAULT_MARITAL_STATUS = "AAAA";
    private static final String UPDATED_MARITAL_STATUS = "BBBB";

    private static final String DEFAULT_FRIENDS = "friends";
    private static final String UPDATED_FRIENDS = "friends-updated";

    private static final String USER_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String USER_LOGIN = "user";

    @Autowired
    private UserExtraMapper userExtraMapper;

    @Autowired
    private UserExtraService userExtraService;

    @Autowired
    private UserExtraRepository userExtraRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private UsersTestUtil usersTestUtil;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private TokenProvider tokenProvider;

    private MockMvc restUserExtraMockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserExtraResource userExtraResource = new UserExtraResource(userExtraService);
        this.restUserExtraMockMvc = MockMvcBuilders.standaloneSetup(userExtraResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .addFilters(new JWTFilter(tokenProvider))
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtra createEntity(EntityManager em) {
        return UserExtra.builder()
            .uuid(DEFAULT_UUID)
            .description(DEFAULT_DESCRIPTION)
            .gender(DEFAULT_GENDER)
            .education(DEFAULT_EDUCATION_TYPE)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .phone(DEFAULT_PHONE)
            .address(DEFAULT_ADDRESS)
            .friends(DEFAULT_FRIENDS)
            .user(null)
            .build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtra createEntity(EntityManager em, User user) {
        return createEntity(em).user(user);
    }

    private void initTest() {
        this.initUsers();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserExtra() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Create the UserExtra
        User newUser = usersTestUtil.createUserEntity(AuthoritiesConstants.PACIENT);
        newUser.setLogin("new-login");
        newUser.setEmail("new-email@gmail.com");
        newUser.setId(null);
        userRepository.saveAndFlush(newUser);
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(createEntity(em, newUser));
        userExtraDTO.setId(null);
        int databaseSizeBeforeCreate = userExtraRepository.findAll().size();
        restUserExtraMockMvc.perform(post("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.uuid").value(userExtraDTO.getUuid()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS));

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        userExtraList.forEach(System.out::println);
        assertThat(userExtraList).hasSize(databaseSizeBeforeCreate + 1);
        UserExtra testUserExtra = userExtraList.get(userExtraList.size() - 1);
        assertThat(testUserExtra.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testUserExtra.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testUserExtra.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testUserExtra.getEducation()).isEqualTo(DEFAULT_EDUCATION_TYPE);
        assertThat(testUserExtra.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testUserExtra.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testUserExtra.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testUserExtra.getFriends()).isEqualTo(DEFAULT_FRIENDS);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserExtraWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userExtraRepository.findAll().size();

        // Create the UserExtra with an existing ID
//        userExtra.setId(1L);
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(userExtraPacient);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserExtraMockMvc.perform(post("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserExtra() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        int databaseSizeBeforeUpdate = userExtraRepository.findAll().size();

        // Update the userExtra
        UserExtra updatedUserExtra = userExtraRepository.findById(userExtraPacient.getId()).get();
        // Disconnect from session so that the updates on updatedUserExtra are not directly saved in db
        em.detach(updatedUserExtra);
        updatedUserExtra
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .education(UPDATED_EDUCATION_TYPE)
            .friends(UPDATED_FRIENDS)
            .gender(UPDATED_GENDER);
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(updatedUserExtra);

        restUserExtraMockMvc.perform(put("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(updatedUserExtra.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(UPDATED_PHONE))
            .andExpect(jsonPath("$.address").value(UPDATED_ADDRESS))
            .andExpect(jsonPath("$.education").value(UPDATED_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(UPDATED_FRIENDS))
            .andExpect(jsonPath("$.gender").value(UPDATED_GENDER));

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeUpdate);
        UserExtra testUserExtra = userExtraList.get(userExtraList.size() - 1);
        assertThat(testUserExtra.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testUserExtra.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testUserExtra.getEducation()).isEqualTo(UPDATED_EDUCATION_TYPE);
        assertThat(testUserExtra.getFriends()).isEqualTo(UPDATED_FRIENDS);
        assertThat(testUserExtra.getGender()).isEqualTo(UPDATED_GENDER);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserExtra_AsDifferentUser() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        int databaseSizeBeforeUpdate = userExtraRepository.findAll().size();

        // Update the userExtra
        UserExtra updatedUserExtra = userExtraRepository.findById(userExtraPacient.getId()).get();
        // Disconnect from session so that the updates on updatedUserExtra are not directly saved in db
        em.detach(updatedUserExtra);
        updatedUserExtra
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .education(UPDATED_EDUCATION_TYPE)
            .friends(UPDATED_FRIENDS)
            .gender(UPDATED_GENDER);
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(updatedUserExtra);

        restUserExtraMockMvc.perform(put("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeUpdate);
        UserExtra testUserExtra = userExtraList.get(userExtraList.size() - 1);
        assertThat(testUserExtra.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testUserExtra.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testUserExtra.getEducation()).isEqualTo(DEFAULT_EDUCATION_TYPE);
        assertThat(testUserExtra.getFriends()).isEqualTo(DEFAULT_FRIENDS);
        assertThat(testUserExtra.getGender()).isEqualTo(DEFAULT_GENDER);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserExtra_AsAdmin() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        int databaseSizeBeforeUpdate = userExtraRepository.findAll().size();

        // Update the userExtra
        UserExtra updatedUserExtra = userExtraRepository.findById(userExtraPacient.getId()).get();
        // Disconnect from session so that the updates on updatedUserExtra are not directly saved in db
        em.detach(updatedUserExtra);
        updatedUserExtra
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .education(UPDATED_EDUCATION_TYPE)
            .friends(UPDATED_FRIENDS)
            .gender(UPDATED_GENDER);
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(updatedUserExtra);

        restUserExtraMockMvc.perform(put("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andExpect(status().isOk())
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(jsonPath("$.id").value(updatedUserExtra.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(UPDATED_PHONE))
            .andExpect(jsonPath("$.address").value(UPDATED_ADDRESS))
            .andExpect(jsonPath("$.education").value(UPDATED_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(UPDATED_FRIENDS))
            .andExpect(jsonPath("$.gender").value(UPDATED_GENDER));

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeUpdate);
        UserExtra testUserExtra = userExtraList.get(userExtraList.size() - 1);
        assertThat(testUserExtra.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testUserExtra.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testUserExtra.getEducation()).isEqualTo(UPDATED_EDUCATION_TYPE);
        assertThat(testUserExtra.getFriends()).isEqualTo(UPDATED_FRIENDS);
        assertThat(testUserExtra.getGender()).isEqualTo(UPDATED_GENDER);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateNonExistingUserExtra() throws Exception {
        int databaseSizeBeforeUpdate = userExtraRepository.findAll().size();


        UserExtra nonExistingUserExtra = new UserExtra();
        // Create the UserExtra
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(nonExistingUserExtra);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserExtraMockMvc.perform(put("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateUserExtra_NoUserForToken() throws Exception {
        int databaseSizeBeforeUpdate = userExtraRepository.findAll().size();

        // Create the UserExtra
        UserExtraDTO userExtraDTO = userExtraMapper.toDto(userExtraPacient);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserExtraMockMvc.perform(put("/api/user-extras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtraDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the UserExtra in the database
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void getUserExtraById_NoUserForToken() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_AsPacient() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getNonExistingUserExtraById() throws Exception {
        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", Long.MAX_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title").value("NO_ENTITY_EXCEPTION"))
            .andExpect(jsonPath("$.status").value(String.valueOf(Status.BAD_REQUEST.getStatusCode())))
            .andExpect(jsonPath("$.detail").value("No userExtraEntity Entity found for id: " + Long.MAX_VALUE + "!"));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraFamily.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraFamily.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(userExtraFamily.getPhone()))
            .andExpect(jsonPath("$.address").value(userExtraFamily.getAddress()))
            .andExpect(jsonPath("$.education").value(userExtraFamily.getEducation().name()))
            .andExpect(jsonPath("$.friends").value(userExtraFamily.getFriends()))
            .andExpect(jsonPath("$.gender").value(userExtraFamily.getGender()));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        associatePacientWithFamily(userExtraPacient,userFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_OfPacient_AsFamily_OfNonAssociatedUser() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
//        associatePacientWithFamily(userExtraPacient,userFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraOrganization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraOrganization.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(userExtraOrganization.getPhone()))
            .andExpect(jsonPath("$.address").value(userExtraOrganization.getAddress()))
            .andExpect(jsonPath("$.education").value(userExtraOrganization.getEducation().name()))
            .andExpect(jsonPath("$.friends").value(userExtraOrganization.getFriends()))
            .andExpect(jsonPath("$.gender").value(userExtraOrganization.getGender()));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraById_OfPacient_AsOrganization_OfNonAssociatedUser() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/{id}", userExtraPacient.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    //--

    @Test
    @Transactional
    public void getUserExtraByUserId_NoUserForToken() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_AsPacient() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getNonExistingUserExtraByUserId() throws Exception {
        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", Long.MAX_VALUE))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraFamily.getUser().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraFamily.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(userExtraFamily.getPhone()))
            .andExpect(jsonPath("$.address").value(userExtraFamily.getAddress()))
            .andExpect(jsonPath("$.education").value(userExtraFamily.getEducation().name()))
            .andExpect(jsonPath("$.friends").value(userExtraFamily.getFriends()))
            .andExpect(jsonPath("$.gender").value(userExtraFamily.getGender()));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        associatePacientWithFamily(userExtraPacient,userFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_OfPacient_AsFamily_OfNonAssociatedUser() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
//        associatePacientWithFamily(userExtraPacient,userFamily);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraOrganization.getUser().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraOrganization.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(userExtraOrganization.getPhone()))
            .andExpect(jsonPath("$.address").value(userExtraOrganization.getAddress()))
            .andExpect(jsonPath("$.education").value(userExtraOrganization.getEducation().name()))
            .andExpect(jsonPath("$.friends").value(userExtraOrganization.getFriends()))
            .andExpect(jsonPath("$.gender").value(userExtraOrganization.getGender()));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtraPacient.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION_TYPE.name()))
            .andExpect(jsonPath("$.friends").value(DEFAULT_FRIENDS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserExtraByUserId_OfPacient_AsOrganization_OfNonAssociatedUser() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Get the userExtra
        restUserExtraMockMvc.perform(get("/api/user-extras/by-userId/{id}", userExtraPacient.getUser().getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_AsAdmin() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //pacient
            .andExpect(jsonPath("$.*",hasSize(3)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraPacient.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION_TYPE.name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(DEFAULT_FRIENDS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            //family
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraFamily.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(userExtraFamily.getPhone())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(userExtraFamily.getAddress())))
            .andExpect(jsonPath("$.[*].education").value(hasItem(userExtraFamily.getEducation().name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(userExtraFamily.getFriends())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(userExtraFamily.getGender())))
            //organization
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraOrganization.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(userExtraOrganization.getPhone())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(userExtraOrganization.getAddress())))
            .andExpect(jsonPath("$.[*].education").value(hasItem(userExtraOrganization.getEducation().name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(userExtraOrganization.getFriends())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(userExtraOrganization.getGender())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_AsPacient() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraPacient.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION_TYPE.name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(DEFAULT_FRIENDS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);
        //associate user
        associatePacientWithFamily(userExtraPacient,userFamily);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraPacient.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION_TYPE.name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(DEFAULT_FRIENDS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(userExtraPacient.getUser().getId().intValue())))
            .andExpect(jsonPath("$.[*].familyId").value(hasItem(userExtraPacient.getFamily().getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserExtras_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);
        userExtraRepository.saveAndFlush(userExtraFamily);
        userExtraRepository.saveAndFlush(userExtraOrganization);
        //associate user
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        // Get all the userExtraList
        restUserExtraMockMvc.perform(get("/api/user-extras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtraPacient.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION_TYPE.name())))
            .andExpect(jsonPath("$.[*].friends").value(hasItem(DEFAULT_FRIENDS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(userExtraPacient.getUser().getId().intValue())))
            .andExpect(jsonPath("$.[*].organizationId").value(hasItem(userExtraPacient.getOrganization().getId().intValue())));
    }

    @Test
    @Transactional
    public void deleteUserExtra() throws Exception {
        // Initialize the database
        userExtraRepository.saveAndFlush(userExtraPacient);

        int databaseSizeBeforeDelete = userExtraRepository.findAll().size();

        // Delete the userExtra
        restUserExtraMockMvc.perform(delete("/api/user-extras/{id}", userExtraPacient.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserExtra> userExtraList = userExtraRepository.findAll();
        assertThat(userExtraList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserExtra.class);
        UserExtra userExtra1 = new UserExtra();
        userExtra1.setId(1L);
        UserExtra userExtra2 = new UserExtra();
        userExtra2.setId(userExtra1.getId());
        assertThat(userExtra1).isEqualTo(userExtra2);
        userExtra2.setId(2L);
        assertThat(userExtra1).isNotEqualTo(userExtra2);
        userExtra1.setId(null);
        assertThat(userExtra1).isNotEqualTo(userExtra2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserExtraDTO.class);
        UserExtraDTO userExtraDTO1 = new UserExtraDTO();
        userExtraDTO1.setId(1L);
        UserExtraDTO userExtraDTO2 = new UserExtraDTO();
        assertThat(userExtraDTO1).isNotEqualTo(userExtraDTO2);
        userExtraDTO2.setId(userExtraDTO1.getId());
        assertThat(userExtraDTO1).isEqualTo(userExtraDTO2);
        userExtraDTO2.setId(2L);
        assertThat(userExtraDTO1).isNotEqualTo(userExtraDTO2);
        userExtraDTO1.setId(null);
        assertThat(userExtraDTO1).isNotEqualTo(userExtraDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userExtraMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userExtraMapper.fromId(null)).isNull();
    }
}
