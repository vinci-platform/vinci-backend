package com.upb.vinci.web.rest.util;

import com.upb.vinci.domain.Authority;
import com.upb.vinci.domain.User;
import com.upb.vinci.security.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Component
@Slf4j
public class UsersTestUtil {

    private static final String PACIENT_LOGIN = "user";
    private static final Long PACIENT_ID = 4L;
    private static final String PACIENT_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String PACIENT_EMAIL = "user@localhost";
    private static final String PACIENT_FIRSTNAME = "User";
    private static final String PACIENT_LASTNAME = "User";
    private static final String PACIENT_IMAGEURL = "http://placehold.it/50x50";
    private static final String PACIENT_LANGKEY = "en";
    private static final Boolean PACIENT_ACTIVATED = true;
    private static final Instant PACIENT_CREATED_DATE = Instant.parse("2020-10-10T10:10:10.00Z");

    private static final String FAMILY_LOGIN = "family_login";
    private static final Long FAMILY_ID = 5L;
    private static final String FAMILY_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String FAMILY_EMAIL = "family@localhost";
    private static final String FAMILY_FIRSTNAME = "family-firstname";
    private static final String FAMILY_LASTNAME = "family-lastname";
    private static final String FAMILY_IMAGEURL = "http://placehold.it/50x50";
    private static final String FAMILY_LANGKEY = "en";
    private static final Boolean FAMILY_ACTIVATED = true;
    private static final Instant FAMILY_CREATED_DATE = Instant.parse("2020-10-10T10:10:10.00Z");

    private static final String ORGANIZATION_LOGIN = "organization_login";
    private static final Long ORGANIZATION_ID = 6L;
    private static final String ORGANIZATION_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String ORGANIZATION_EMAIL = "organization@localhost";
    private static final String ORGANIZATION_FIRSTNAME = "organization-firstname";
    private static final String ORGANIZATION_LASTNAME = "organization-lastname";
    private static final String ORGANIZATION_IMAGEURL = "http://placehold.it/50x50";
    private static final String ORGANIZATION_LANGKEY = "en";
    private static final Boolean ORGANIZATION_ACTIVATED = true;
    private static final Instant ORGANIZATION_CREATED_DATE = Instant.parse("2020-10-10T10:10:10.00Z");

    private static final String ADMIN_LOGIN = "admin";
    private static final Long ADMIN_ID = 3L;
    private static final String ADMIN_EMAIL = "organization@localhost";
    private static final String ADMIN_FIRSTNAME = "Administrator-firstname";
    private static final String ADMIN_LASTNAME = "Administrator-lastname";
    private static final String ADMIN_IMAGEURL = "http://placehold.it/50x50";
    private static final String ADMIN_LANGKEY = "en";
    private static final Boolean ADMIN_ACTIVATED = true;
    private static final Instant ADMIN_CREATED_DATE = Instant.parse("2020-10-10T10:10:10.00Z");

    @Autowired
    public PasswordEncoder passwordEncoder;

    /**
     *
     * @param role from AuthoritiesConstants
     * @return Set with AuthoritiesConstants.USER and AuthoritiesConstants.role
     */
    public Set<Authority> getAuthorities(String role) {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(AuthoritiesConstants.USER));
        switch (role) {
            case AuthoritiesConstants.PACIENT:
                authorities.add(new Authority(AuthoritiesConstants.PACIENT));
                break;
            case AuthoritiesConstants.FAMILY:
                authorities.add(new Authority(AuthoritiesConstants.FAMILY));
                break;
            case AuthoritiesConstants.ORGANIZIATION:
                authorities.add(new Authority(AuthoritiesConstants.ORGANIZIATION));
                break;
            case AuthoritiesConstants.ADMIN:
                authorities.add(new Authority(AuthoritiesConstants.ADMIN));
                break;
        }
        return authorities;
    }

    /**
     * Returns entity with all fields AND id. All entities that are returned are injected in database with liquibase
     *
     * @param role from AuthoritiesConstants
     * @return User.java with all fields including id
     */
    public User createUserEntity(String role) {
        User user = new User();
        switch (role) {
            case AuthoritiesConstants.PACIENT:
                user.setId(PACIENT_ID);
                user.setLogin(PACIENT_LOGIN);
                user.setFirstName(PACIENT_FIRSTNAME);
                user.setLastName(PACIENT_LASTNAME);
                user.setEmail(PACIENT_EMAIL);
                user.setPassword(passwordEncoder.encode(PACIENT_PASSWORD));
                user.setImageUrl(PACIENT_IMAGEURL);
                user.setActivated(PACIENT_ACTIVATED);
                user.setCreatedDate(PACIENT_CREATED_DATE);
                user.setLangKey(PACIENT_LANGKEY);
                user.setAuthorities(getAuthorities(AuthoritiesConstants.PACIENT));
                break;
            case AuthoritiesConstants.FAMILY:
                user.setId(FAMILY_ID);
                user.setLogin(FAMILY_LOGIN);
                user.setFirstName(FAMILY_FIRSTNAME);
                user.setLastName(FAMILY_LASTNAME);
                user.setEmail(FAMILY_EMAIL);
                user.setPassword(passwordEncoder.encode(FAMILY_PASSWORD));
                user.setImageUrl(FAMILY_IMAGEURL);
                user.setActivated(FAMILY_ACTIVATED);
                user.setCreatedDate(FAMILY_CREATED_DATE);
                user.setLangKey(FAMILY_LANGKEY);
                user.setAuthorities(getAuthorities(AuthoritiesConstants.FAMILY));
                break;
            case AuthoritiesConstants.ORGANIZIATION:
                user.setId(ORGANIZATION_ID);
                user.setLogin(ORGANIZATION_LOGIN);
                user.setFirstName(ORGANIZATION_FIRSTNAME);
                user.setLastName(ORGANIZATION_LASTNAME);
                user.setEmail(ORGANIZATION_EMAIL);
                user.setPassword(passwordEncoder.encode(ORGANIZATION_PASSWORD));
                user.setImageUrl(ORGANIZATION_IMAGEURL);
                user.setActivated(ORGANIZATION_ACTIVATED);
                user.setCreatedDate(ORGANIZATION_CREATED_DATE);
                user.setLangKey(ORGANIZATION_LANGKEY);
                user.setAuthorities(getAuthorities(AuthoritiesConstants.ORGANIZIATION));
                break;
            case AuthoritiesConstants.ADMIN:
                user.setId(ADMIN_ID);
                user.setLogin(ADMIN_LOGIN);
                user.setFirstName(ADMIN_FIRSTNAME);
                user.setLastName(ADMIN_LASTNAME);
                user.setEmail(ADMIN_EMAIL);
                user.setImageUrl(ADMIN_IMAGEURL);
                user.setActivated(ADMIN_ACTIVATED);
                user.setCreatedDate(ADMIN_CREATED_DATE);
                user.setLangKey(ADMIN_LANGKEY);
                user.setAuthorities(getAuthorities(AuthoritiesConstants.ADMIN));
                break;
        }
        return user;
    }
}
