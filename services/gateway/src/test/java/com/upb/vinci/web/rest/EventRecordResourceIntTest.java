package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.EventRecord;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.repository.EventRecordRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.service.EventRecordQueryService;
import com.upb.vinci.service.EventRecordService;
import com.upb.vinci.service.dto.EventRecordDTO;
import com.upb.vinci.service.mapper.EventRecordMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EventRecordResource REST controller.
 *
 * @see EventRecordResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class EventRecordResourceIntTest extends UsersSetupController{

    private static final Long DEFAULT_APP_ID = 1L;
    private static final Long UPDATED_APP_ID = 2L;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Long DEFAULT_CREATED = 1L;
    private static final Long UPDATED_CREATED = 2L;

    private static final Long DEFAULT_NOTIFY = 1L;
    private static final Long UPDATED_NOTIFY = 2L;

    private static final String DEFAULT_REPEAT = "AAAAAAAAAA";
    private static final String UPDATED_REPEAT = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_DATA = "BBBBBBBBBB";

    @Autowired
    private EventRecordRepository eventRecordRepository;

    @Autowired
    private EventRecordMapper eventRecordMapper;

    @Autowired
    private EventRecordService eventRecordService;

    @Autowired
    private EventRecordQueryService eventRecordQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEventRecordMockMvc;

    private EventRecord eventRecord;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EventRecordResource eventRecordResource = new EventRecordResource(eventRecordService, eventRecordQueryService);
        this.restEventRecordMockMvc = MockMvcBuilders.standaloneSetup(eventRecordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test. NOTE: userId is of pacient user
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventRecord createEntity(EntityManager em) {
        return new EventRecord()
            .appId(DEFAULT_APP_ID)
            .userId(PACIENT_USER_ID)
            .timestamp(DEFAULT_TIMESTAMP)
            .created(DEFAULT_CREATED)
            .notify(DEFAULT_NOTIFY)
            .repeat(DEFAULT_REPEAT)
            .title(DEFAULT_TITLE)
            .type(DEFAULT_TYPE)
            .text(DEFAULT_TEXT)
            .data(DEFAULT_DATA);
    }

    private void initTest() {
        initUsers();
        eventRecord = createEntity(em);
    }

    @Test
    @Transactional
    public void createEventRecord_NoUserInToken() throws Exception {
        // Create the EventRecord with an existing ID
        createEventRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_WithExistingId() throws Exception {
        eventRecord.setId(1L);

        // Create the EventRecord with an existing ID
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restEventRecordMockMvc.perform(post("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_UserIdIsNull() throws Exception {
        eventRecord.setUserId(null);

        // Create the EventRecord with an existing ID
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restEventRecordMockMvc.perform(post("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsPacient() throws Exception {
        // Create the EventRecord
        createEventRecordRestCall_IsCreated(PACIENT_USER_ID);
    }
    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsPacient_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        eventRecord.setUserId(differentUsersUserId);
        // Create the EventRecord
        createEventRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsFamily() throws Exception {
        eventRecord.setUserId(FAMILY_USER_ID);
        // Create the EventRecord
        createEventRecordRestCall_IsCreated(FAMILY_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsFamily_DifferentUserId() throws Exception {
        Long differentUsersUserId = userOrganization.getId();
        eventRecord.setUserId(differentUsersUserId);
        // Create the EventRecord
        createEventRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_OfPacient_AsFamily() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);
        // Create the EventRecord
        createEventRecordRestCall_IsCreated(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_OfPacient_AsFamily_NotAssociated() throws Exception {
//        associatePacientWithFamily(userExtraPacient,userFamily);
        // Create the EventRecord
        createEventRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsOrganization() throws Exception {
        eventRecord.setUserId(ORGANIZATION_USER_ID);
        createEventRecordRestCall_IsCreated(ORGANIZATION_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_AsOrganization_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        eventRecord.setUserId(differentUsersUserId);
        // Create the EventRecord
        createEventRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_OfPacient_AsOrganization() throws Exception {
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        createEventRecordRestCall_IsCreated(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createEventRecord_OfPacient_AsOrganization_NotAssociated() throws Exception {
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        createEventRecordRestCall_IsUnauthorized();
    }

    private void createEventRecordRestCall_IsCreated(Long userId) throws Exception {
        // Create the EventRecord
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        restEventRecordMockMvc.perform(post("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.appId").value(DEFAULT_APP_ID.intValue()))
            .andExpect(jsonPath("$.userId").value(userId.intValue()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.intValue()))
            .andExpect(jsonPath("$.notify").value(DEFAULT_NOTIFY.intValue()))
            .andExpect(jsonPath("$.repeat").value(DEFAULT_REPEAT))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA));

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest + 1);
        EventRecord testEventRecord = eventRecordList.get(eventRecordList.size() - 1);
        assertThat(testEventRecord.getAppId()).isEqualTo(DEFAULT_APP_ID);
        assertThat(testEventRecord.getUserId()).isEqualTo(userId);
        assertThat(testEventRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testEventRecord.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testEventRecord.getNotify()).isEqualTo(DEFAULT_NOTIFY);
        assertThat(testEventRecord.getRepeat()).isEqualTo(DEFAULT_REPEAT);
        assertThat(testEventRecord.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testEventRecord.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testEventRecord.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testEventRecord.getData()).isEqualTo(DEFAULT_DATA);
    }

    private void createEventRecordRestCall_IsUnauthorized() throws Exception {
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restEventRecordMockMvc.perform(post("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateEventRecord_NoUserForToken() throws Exception {
        updateEventRecordRestCall_IsUnauthorized(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateNonExistingEventRecord() throws Exception {
        // Create the EventRecord
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventRecordMockMvc.perform(put("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_UserIdIsNull() throws Exception {
        eventRecordRepository.saveAndFlush(eventRecord);
        em.clear();
        eventRecord.setUserId(null);

        updateEventRecordRestCall_BadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsPacient() throws Exception {
        // Initialize the database
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        eventRecord.setUserId(differentUsersUserId);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsUnauthorized(eventRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsFamily() throws Exception {
        // Initialize the database
        eventRecord.setUserId(FAMILY_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(FAMILY_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userOrganization.getId();
        eventRecord.setUserId(differentUsersUserId);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(FAMILY_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient, userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient, userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsUnauthorized(eventRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsOrganization() throws Exception {
        // Initialize the database
        eventRecord.setUserId(ORGANIZATION_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(ORGANIZATION_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        eventRecord.setUserId(differentUsersUserId);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsUnauthorized(eventRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient, userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsOk(PACIENT_USER_ID);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateEventRecord_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient, userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        updateEventRecordRestCall_IsUnauthorized(eventRecord.getUserId());
    }

    private void updateEventRecordRestCall_IsOk(Long userId) throws Exception {
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // Update the eventRecord
        EventRecord updatedEventRecord = eventRecordRepository.findById(eventRecord.getId()).get();
        // Disconnect from session so that the updates on updatedEventRecord are not directly saved in db
        em.detach(updatedEventRecord);
        updatedEventRecord
            .appId(UPDATED_APP_ID)
            .userId(userId)
            .timestamp(UPDATED_TIMESTAMP)
            .created(UPDATED_CREATED)
            .notify(UPDATED_NOTIFY)
            .repeat(UPDATED_REPEAT)
            .title(UPDATED_TITLE)
            .type(UPDATED_TYPE)
            .text(UPDATED_TEXT)
            .data(UPDATED_DATA);
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(updatedEventRecord);

        restEventRecordMockMvc.perform(put("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isOk());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        eventRecordList.forEach(System.out::println);
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
        EventRecord testEventRecord = eventRecordList.get(eventRecordList.size() - 1);
        assertThat(testEventRecord.getAppId()).isEqualTo(UPDATED_APP_ID);
        assertThat(testEventRecord.getUserId()).isEqualTo(userId);
        assertThat(testEventRecord.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testEventRecord.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testEventRecord.getNotify()).isEqualTo(UPDATED_NOTIFY);
        assertThat(testEventRecord.getRepeat()).isEqualTo(UPDATED_REPEAT);
        assertThat(testEventRecord.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testEventRecord.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testEventRecord.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testEventRecord.getData()).isEqualTo(UPDATED_DATA);
    }

    private void updateEventRecordRestCall_IsUnauthorized(Long userIdInDatabase) throws Exception {
        eventRecordRepository.saveAndFlush(eventRecord);
        // Create the EventRecord
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        restEventRecordMockMvc.perform(put("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
        EventRecord unchangedEventRecord = eventRecordList.get(eventRecordList.size() - 1);
        assertThat(unchangedEventRecord.getAppId()).isEqualTo(DEFAULT_APP_ID);
        assertThat(unchangedEventRecord.getUserId()).isEqualTo(userIdInDatabase);
        assertThat(unchangedEventRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(unchangedEventRecord.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(unchangedEventRecord.getNotify()).isEqualTo(DEFAULT_NOTIFY);
        assertThat(unchangedEventRecord.getRepeat()).isEqualTo(DEFAULT_REPEAT);
        assertThat(unchangedEventRecord.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(unchangedEventRecord.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(unchangedEventRecord.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(unchangedEventRecord.getData()).isEqualTo(DEFAULT_DATA);
    }

    private void updateEventRecordRestCall_BadRequest() throws Exception {
        // Create the EventRecord
        EventRecordDTO eventRecordDTO = eventRecordMapper.toDto(eventRecord);
        int databaseSizeBeforeTest = eventRecordRepository.findAll().size();
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventRecordMockMvc.perform(put("/api/event-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventRecord in the database
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeTest);
        EventRecord unchangedEventRecord = eventRecordList.get(eventRecordList.size() - 1);
        assertThat(unchangedEventRecord.getAppId()).isEqualTo(DEFAULT_APP_ID);
        assertThat(unchangedEventRecord.getUserId()).isEqualTo(PACIENT_USER_ID);
        assertThat(unchangedEventRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(unchangedEventRecord.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(unchangedEventRecord.getNotify()).isEqualTo(DEFAULT_NOTIFY);
        assertThat(unchangedEventRecord.getRepeat()).isEqualTo(DEFAULT_REPEAT);
        assertThat(unchangedEventRecord.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(unchangedEventRecord.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(unchangedEventRecord.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(unchangedEventRecord.getData()).isEqualTo(DEFAULT_DATA);
    }

    @Test
    @Transactional
    public void getAllUserRecords_NoUserInToken() throws Exception {
        // Get all the eventRecordList
        restEventRecordMockMvc.perform(get("/api/event-records?sort=id,desc"))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsPacient() throws Exception {
        // Initialize the database
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsPacient_NoItems() throws Exception {
        // Initialize the database
//        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsFamily() throws Exception {
        // Initialize the database
        eventRecord.setUserId(FAMILY_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(FAMILY_USER_ID,1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsFamily_NoItems() throws Exception {
        // Initialize the database
//        eventRecord.setUserId(FAMILY_USER_ID);
//        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(FAMILY_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsFamily_NoItemsForPacient() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
//        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(FAMILY_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsOrganization() throws Exception {
        // Initialize the database
        eventRecord.setUserId(ORGANIZATION_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(ORGANIZATION_USER_ID,1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_AsOrganization_NoItems() throws Exception {
        // Initialize the database
//        eventRecord.setUserId(ORGANIZATION_USER_ID);
//        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(ORGANIZATION_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsOrganization_NoItemsForPacient() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
//        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(ORGANIZATION_USER_ID,0);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        getAllEventRecordsRestCall_IsOk(PACIENT_USER_ID,0);
    }

    private void getAllEventRecordsRestCall_IsOk(Long userId,int sizeOfReturnList) throws Exception {
        if (sizeOfReturnList == 0) {
            // Get all the eventRecordList
            restEventRecordMockMvc.perform(get("/api/event-records?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.*", hasSize(sizeOfReturnList)));
        } else {
            // Get all the eventRecordList
            restEventRecordMockMvc.perform(get("/api/event-records?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.*", hasSize(sizeOfReturnList)))
                .andExpect(jsonPath("$.[*].id").value(hasItem(eventRecord.getId().intValue())))
                .andExpect(jsonPath("$.[*].appId").value(hasItem(DEFAULT_APP_ID.intValue())))
                .andExpect(jsonPath("$.[*].userId").value(hasItem(userId.intValue())))
                .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED.intValue())))
                .andExpect(jsonPath("$.[*].notify").value(hasItem(DEFAULT_NOTIFY.intValue())))
                .andExpect(jsonPath("$.[*].repeat").value(hasItem(DEFAULT_REPEAT)))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
                .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
                .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)));
        }
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        User validUser = userPacient;
        User differentUser = userOrganization;
        eventRecordRepository.saveAndFlush(eventRecord);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        User validUser = userFamily;
        User differentUser = userOrganization;
        eventRecord.setUserId(validUser.getId());
        eventRecordRepository.saveAndFlush(eventRecord);
        associatePacientWithFamily(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllEventRecords_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        User validUser = userOrganization;
        User differentUser = userFamily;
        eventRecord.setUserId(validUser.getId());
        eventRecordRepository.saveAndFlush(eventRecord);
        associatePacientWithOrganization(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultEventRecordsShouldBeFound(validFilter,eventRecord);
        defaultEventRecordsShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"appId",DEFAULT_APP_ID,UPDATED_APP_ID);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"created",DEFAULT_CREATED,UPDATED_CREATED);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"data",DEFAULT_DATA,UPDATED_DATA);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"notify",DEFAULT_NOTIFY,UPDATED_NOTIFY);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"repeat",DEFAULT_REPEAT,UPDATED_REPEAT);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"text",DEFAULT_TEXT,UPDATED_TEXT);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"title",DEFAULT_TITLE,UPDATED_TITLE);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"type",DEFAULT_TYPE,UPDATED_TYPE);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",eventRecord.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultEventRecordsShouldBeFound(filter+validValue,eventRecord);
        defaultEventRecordsShouldNotBeFound(filter+invalidValue);
        defaultEventRecordsShouldBeFound(validFilter+"&"+filter+validValue,eventRecord);
        defaultEventRecordsShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultEventRecordsShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultEventRecordsShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEventRecordsShouldBeFound(String filter, EventRecord _eventRecord) throws Exception {
        restEventRecordMockMvc.perform(get("/api/event-records?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_eventRecord.getId().intValue())))
            .andExpect(jsonPath("$.[*].appId").value(hasItem(DEFAULT_APP_ID.intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(_eventRecord.getUserId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED.intValue())))
            .andExpect(jsonPath("$.[*].notify").value(hasItem(DEFAULT_NOTIFY.intValue())))
            .andExpect(jsonPath("$.[*].repeat").value(hasItem(DEFAULT_REPEAT)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEventRecordsShouldNotBeFound(String filter) throws Exception {
        restEventRecordMockMvc.perform(get("/api/event-records?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getEventRecordById_NoUserInToken() throws Exception {
        getEventRecordById_IsUnauthorized(Long.MAX_VALUE);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getNonExistingEventRecord() throws Exception {
        // Get the eventRecord
        restEventRecordMockMvc.perform(get("/api/event-records/{id}", Long.MAX_VALUE))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_AsPacient() throws Exception {
        // Initialize the database
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_AsFamily() throws Exception {
        // Initialize the database
        eventRecord.setUserId(FAMILY_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordById_IsUnauthorized(eventRecord.getId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_AsOrganization() throws Exception {
        // Initialize the database
        eventRecord.setUserId(ORGANIZATION_USER_ID);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordByIdRestCall_IsOk();

    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getEventRecordById_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        eventRecordRepository.saveAndFlush(eventRecord);

        getEventRecordById_IsUnauthorized(eventRecord.getId());
    }

    private void getEventRecordByIdRestCall_IsOk() throws Exception {
        // Get the eventRecord
        restEventRecordMockMvc.perform(get("/api/event-records/{id}", eventRecord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eventRecord.getId().intValue()))
            .andExpect(jsonPath("$.appId").value(DEFAULT_APP_ID.intValue()))
            .andExpect(jsonPath("$.userId").value(eventRecord.getUserId().intValue()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.intValue()))
            .andExpect(jsonPath("$.notify").value(DEFAULT_NOTIFY.intValue()))
            .andExpect(jsonPath("$.repeat").value(DEFAULT_REPEAT))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA));
    }

    private void getEventRecordById_IsUnauthorized(Long id) throws Exception {
        // Get the eventRecord
        restEventRecordMockMvc.perform(get("/api/event-records/{id}", id))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void deleteEventRecord() throws Exception {
        // Initialize the database
        eventRecordRepository.saveAndFlush(eventRecord);

        int databaseSizeBeforeDelete = eventRecordRepository.findAll().size();

        // Delete the eventRecord
        restEventRecordMockMvc.perform(delete("/api/event-records/{id}", eventRecord.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void deleteEventRecord_WithDifferentUser() throws Exception {
        // Initialize the database
        eventRecordRepository.saveAndFlush(eventRecord);

        int databaseSizeBeforeDelete = eventRecordRepository.findAll().size();

        // Delete the eventRecord
        restEventRecordMockMvc.perform(delete("/api/event-records/{id}", eventRecord.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isUnauthorized());

        // Validate the database is empty
        List<EventRecord> eventRecordList = eventRecordRepository.findAll();
        assertThat(eventRecordList).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventRecord.class);
        EventRecord eventRecord1 = new EventRecord();
        eventRecord1.setId(1L);
        EventRecord eventRecord2 = new EventRecord();
        eventRecord2.setId(eventRecord1.getId());
        assertThat(eventRecord1).isEqualTo(eventRecord2);
        eventRecord2.setId(2L);
        assertThat(eventRecord1).isNotEqualTo(eventRecord2);
        eventRecord1.setId(null);
        assertThat(eventRecord1).isNotEqualTo(eventRecord2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventRecordDTO.class);
        EventRecordDTO eventRecordDTO1 = new EventRecordDTO();
        eventRecordDTO1.setId(1L);
        EventRecordDTO eventRecordDTO2 = new EventRecordDTO();
        assertThat(eventRecordDTO1).isNotEqualTo(eventRecordDTO2);
        eventRecordDTO2.setId(eventRecordDTO1.getId());
        assertThat(eventRecordDTO1).isEqualTo(eventRecordDTO2);
        eventRecordDTO2.setId(2L);
        assertThat(eventRecordDTO1).isNotEqualTo(eventRecordDTO2);
        eventRecordDTO1.setId(null);
        assertThat(eventRecordDTO1).isNotEqualTo(eventRecordDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(eventRecordMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(eventRecordMapper.fromId(null)).isNull();
    }
}
