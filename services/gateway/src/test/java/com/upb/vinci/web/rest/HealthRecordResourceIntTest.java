package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.HealthRecord;
import com.upb.vinci.domain.User;
import com.upb.vinci.repository.HealthRecordRepository;
import com.upb.vinci.service.HealthRecordQueryService;
import com.upb.vinci.service.HealthRecordService;
import com.upb.vinci.service.dto.HealthRecordDTO;
import com.upb.vinci.service.mapper.HealthRecordMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HealthRecordResource REST controller.
 *
 * @see HealthRecordResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class HealthRecordResourceIntTest extends UsersSetupController{

    private static final Long DEFAULT_USER_ID = 4L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Double DEFAULT_SCORE = 1D;
    private static final Double UPDATED_SCORE = 2D;

    private static final Double DEFAULT_AVAILABLE_SCORE = 1D;
    private static final Double UPDATED_AVAILABLE_SCORE = 2D;

    private static final Double DEFAULT_STEPS_SCORE = 1D;
    private static final Double UPDATED_STEPS_SCORE = 2D;

    private static final Double DEFAULT_IPAQ_SCORE = 1D;
    private static final Double UPDATED_IPAQ_SCORE = 2D;

    private static final Double DEFAULT_WHOQOL_SCORE = 1D;
    private static final Double UPDATED_WHOQOL_SCORE = 2D;

    private static final Double DEFAULT_FEELINGS_SCORE = 1D;
    private static final Double UPDATED_FEELINGS_SCORE = 2D;

    @Autowired
    private HealthRecordRepository healthRecordRepository;

    @Autowired
    private HealthRecordMapper healthRecordMapper;

    @Autowired
    private HealthRecordService healthRecordService;

    @Autowired
    private HealthRecordQueryService healthRecordQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHealthRecordMockMvc;

    private HealthRecord healthRecord;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HealthRecordResource healthRecordResource = new HealthRecordResource(healthRecordService, healthRecordQueryService);
        this.restHealthRecordMockMvc = MockMvcBuilders.standaloneSetup(healthRecordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HealthRecord createEntity(EntityManager em) {
        return new HealthRecord()
            .userId(DEFAULT_USER_ID)
            .timestamp(DEFAULT_TIMESTAMP)
            .score(DEFAULT_SCORE)
            .availableScore(DEFAULT_AVAILABLE_SCORE)
            .stepsScore(DEFAULT_STEPS_SCORE)
            .ipaqScore(DEFAULT_IPAQ_SCORE)
            .whoqolScore(DEFAULT_WHOQOL_SCORE)
            .feelingsScore(DEFAULT_FEELINGS_SCORE);
    }

    private void initTest() {
        initUsers();

        healthRecord = createEntity(em);
    }

    @Test
    @Transactional
    public void createHealthRecord_NoUserInToken() throws Exception {
        createHealthRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_WithExistingId() throws Exception {
        // Create the HealthRecord with an existing ID
        healthRecord.setId(1L);

        createHealthRecordRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_UserIdIsNull() throws Exception {
        // Create the HealthRecord with an existing ID
        healthRecord.setUserId(null);

        createHealthRecordRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsPacient() throws Exception {
        createHealthRecordRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsPacient_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        healthRecord.setUserId(differentUsersUserId);

        createHealthRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsFamily() throws Exception {
        healthRecord.setUserId(FAMILY_USER_ID);

        createHealthRecordRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsFamily_DifferentUserId() throws Exception {
        Long differentUsersUserId = userOrganization.getId();
        healthRecord.setUserId(differentUsersUserId);

        createHealthRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_OfPacient_AsFamily() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);

        createHealthRecordRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_OfPacient_AsFamily_NotAssociated() throws Exception {
//        associatePacientWithFamily(userExtraPacient,userFamily);

        createHealthRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsOrganization() throws Exception {
        healthRecord.setUserId(ORGANIZATION_USER_ID);

        createHealthRecordRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_AsOrganization_DifferentUserId() throws Exception {
        Long differentUsersUserId = userFamily.getId();
        healthRecord.setUserId(differentUsersUserId);

        createHealthRecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_OfPacient_AsOrganization() throws Exception {
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createHealthRecordRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createHealthRecord_OfPacient_AsOrganization_NotAssocaited() throws Exception {
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createHealthRecordRestCall_IsUnauthorized();
    }

    private void createHealthRecordRestCall_IsCreated() throws Exception {
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // Create the HealthRecord
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        restHealthRecordMockMvc.perform(post("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.userId").value(healthRecord.getUserId().intValue()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
            .andExpect(jsonPath("$.availableScore").value(DEFAULT_AVAILABLE_SCORE))
            .andExpect(jsonPath("$.stepsScore").value(DEFAULT_STEPS_SCORE))
            .andExpect(jsonPath("$.ipaqScore").value(DEFAULT_IPAQ_SCORE))
            .andExpect(jsonPath("$.whoqolScore").value(DEFAULT_WHOQOL_SCORE))
            .andExpect(jsonPath("$.feelingsScore").value(DEFAULT_FEELINGS_SCORE));

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest + 1);
        HealthRecord testHealthRecord = healthRecordList.get(healthRecordList.size() - 1);
        assertThat(testHealthRecord.getUserId()).isEqualTo(healthRecordDTO.getUserId());
        assertThat(testHealthRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testHealthRecord.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testHealthRecord.getAvailableScore()).isEqualTo(DEFAULT_AVAILABLE_SCORE);
        assertThat(testHealthRecord.getStepsScore()).isEqualTo(DEFAULT_STEPS_SCORE);
        assertThat(testHealthRecord.getIpaqScore()).isEqualTo(DEFAULT_IPAQ_SCORE);
        assertThat(testHealthRecord.getWhoqolScore()).isEqualTo(DEFAULT_WHOQOL_SCORE);
        assertThat(testHealthRecord.getFeelingsScore()).isEqualTo(DEFAULT_FEELINGS_SCORE);
    }

    private void createHealthRecordRestCall_IsBadRequest() throws Exception {
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restHealthRecordMockMvc.perform(post("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
    }

    private void createHealthRecordRestCall_IsUnauthorized() throws Exception {
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restHealthRecordMockMvc.perform(post("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateHealthRecord_NoUserInToken() throws Exception {
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_UserIdNull() throws Exception {
        healthRecordRepository.saveAndFlush(healthRecord);

        healthRecord.setUserId(null);

        updateHealthRecordRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_NonExistingEntity() throws Exception {
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // Create the HealthRecord
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHealthRecordMockMvc.perform(put("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsPacient() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthRecordRestCall_IsOk(healthRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsFamily() throws Exception {
        // Initialize the database
        healthRecord.setUserId(FAMILY_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthRecordRestCall_IsOk(healthRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userOrganization.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthRecordRestCall_IsOk(healthRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsOrganization() throws Exception {
        // Initialize the database
        healthRecord.setUserId(ORGANIZATION_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthRecordRestCall_IsOk(healthRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthRecordRestCall_IsOk(healthRecord.getUserId());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateHealthRecord_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        healthRecordRepository.saveAndFlush(healthRecord);

        updateHealthrecordRestCall_IsUnauthorized();
    }

    private void updateHealthRecordRestCall_IsOk(Long updateUserId) throws Exception {
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // Update the healthRecord
        HealthRecord updatedHealthRecord = healthRecordRepository.findById(healthRecord.getId()).get();
        // Disconnect from session so that the updates on updatedHealthRecord are not directly saved in db
        em.detach(updatedHealthRecord);
        updatedHealthRecord
            .userId(updateUserId)
            .timestamp(UPDATED_TIMESTAMP)
            .score(UPDATED_SCORE)
            .availableScore(UPDATED_AVAILABLE_SCORE)
            .stepsScore(UPDATED_STEPS_SCORE)
            .ipaqScore(UPDATED_IPAQ_SCORE)
            .whoqolScore(UPDATED_WHOQOL_SCORE)
            .feelingsScore(UPDATED_FEELINGS_SCORE);
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(updatedHealthRecord);

        restHealthRecordMockMvc.perform(put("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(healthRecord.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(updateUserId.intValue()))
            .andExpect(jsonPath("$.timestamp").value(UPDATED_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.score").value(UPDATED_SCORE))
            .andExpect(jsonPath("$.availableScore").value(UPDATED_AVAILABLE_SCORE))
            .andExpect(jsonPath("$.stepsScore").value(UPDATED_STEPS_SCORE))
            .andExpect(jsonPath("$.ipaqScore").value(UPDATED_IPAQ_SCORE))
            .andExpect(jsonPath("$.whoqolScore").value(UPDATED_WHOQOL_SCORE))
            .andExpect(jsonPath("$.feelingsScore").value(UPDATED_FEELINGS_SCORE));

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
        HealthRecord testHealthRecord = healthRecordList.get(healthRecordList.size() - 1);
        assertThat(testHealthRecord.getUserId()).isEqualTo(updateUserId);
        assertThat(testHealthRecord.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testHealthRecord.getScore()).isEqualTo(UPDATED_SCORE);
        assertThat(testHealthRecord.getAvailableScore()).isEqualTo(UPDATED_AVAILABLE_SCORE);
        assertThat(testHealthRecord.getStepsScore()).isEqualTo(UPDATED_STEPS_SCORE);
        assertThat(testHealthRecord.getIpaqScore()).isEqualTo(UPDATED_IPAQ_SCORE);
        assertThat(testHealthRecord.getWhoqolScore()).isEqualTo(UPDATED_WHOQOL_SCORE);
        assertThat(testHealthRecord.getFeelingsScore()).isEqualTo(UPDATED_FEELINGS_SCORE);
    }

    private void updateHealthRecordRestCall_IsBadRequest() throws Exception {
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // Create the HealthRecord
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHealthRecordMockMvc.perform(put("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
        HealthRecord testHealthRecord = healthRecordList.get(healthRecordList.size() - 1);
        assertThat(testHealthRecord.getUserId()).isEqualTo(healthRecordDTO.getUserId());
        assertThat(testHealthRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testHealthRecord.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testHealthRecord.getAvailableScore()).isEqualTo(DEFAULT_AVAILABLE_SCORE);
        assertThat(testHealthRecord.getStepsScore()).isEqualTo(DEFAULT_STEPS_SCORE);
        assertThat(testHealthRecord.getIpaqScore()).isEqualTo(DEFAULT_IPAQ_SCORE);
        assertThat(testHealthRecord.getWhoqolScore()).isEqualTo(DEFAULT_WHOQOL_SCORE);
        assertThat(testHealthRecord.getFeelingsScore()).isEqualTo(DEFAULT_FEELINGS_SCORE);
    }

    private void updateHealthrecordRestCall_IsUnauthorized() throws Exception {
        int databaseSizeBeforeTest = healthRecordRepository.findAll().size();
        // Create the HealthRecord
        HealthRecordDTO healthRecordDTO = healthRecordMapper.toDto(healthRecord);
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHealthRecordMockMvc.perform(put("/api/health-records")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(healthRecordDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the HealthRecord in the database
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsPacient() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsPacient_NoEntity() throws Exception {
        // Initialize the database
//        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        healthRecord.setUserId(differentUsersUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsFamily() throws Exception {
        // Initialize the database
        healthRecord.setUserId(FAMILY_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsFamily_NoEntity() throws Exception {
        // Initialize the database
//        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userOrganization.getId();
        healthRecord.setUserId(differentUsersUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsOrganization() throws Exception {
        // Initialize the database
        healthRecord.setUserId(ORGANIZATION_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsOrganization_NoEntity() throws Exception {
        // Initialize the database
//        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUsersUserId = userFamily.getId();
        healthRecord.setUserId(differentUsersUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        getAllHealthRecordsRestCall_EmptyList();
    }

    private void getAllHealthRecordsRestCall_IsOk() throws Exception {
        // Get all the healthRecordList
        restHealthRecordMockMvc.perform(get("/api/health-records?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(healthRecord.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(healthRecord.getUserId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].availableScore").value(hasItem(DEFAULT_AVAILABLE_SCORE)))
            .andExpect(jsonPath("$.[*].stepsScore").value(hasItem(DEFAULT_STEPS_SCORE)))
            .andExpect(jsonPath("$.[*].ipaqScore").value(hasItem(DEFAULT_IPAQ_SCORE)))
            .andExpect(jsonPath("$.[*].whoqolScore").value(hasItem(DEFAULT_WHOQOL_SCORE)))
            .andExpect(jsonPath("$.[*].feelingsScore").value(hasItem(DEFAULT_FEELINGS_SCORE)));
    }

    private void getAllHealthRecordsRestCall_EmptyList() throws Exception {
        // Get all the healthRecordList
        restHealthRecordMockMvc.perform(get("/api/health-records?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        User validUser = userPacient;
        User differentUser = userOrganization;
        healthRecordRepository.saveAndFlush(healthRecord);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        User validUser = userFamily;
        User differentUser = userOrganization;
        healthRecord.setUserId(validUser.getId());
        healthRecordRepository.saveAndFlush(healthRecord);
        associatePacientWithFamily(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllHealthRecords_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        User validUser = userOrganization;
        User differentUser = userFamily;
        healthRecord.setUserId(validUser.getId());
        healthRecordRepository.saveAndFlush(healthRecord);
        associatePacientWithOrganization(userExtraPacient,userFamily);
        //setup
        String validUserIdEqualsFilter = "userId.equals=" + validUser.getId().intValue();
        String invalidUserIdEqualsFilter = "userId.equals=" + differentUser.getId().intValue();
        testingFilters(validUserIdEqualsFilter,invalidUserIdEqualsFilter);
        //setup
        String validUserIdInFilter = "userId.in=" + validUser.getId().intValue() + "," + differentUser.getId().intValue();
        String invalidUserIdInFilter = "userId.in=" + differentUser.getId().intValue();
        testingFilters(validUserIdInFilter,invalidUserIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultHealthRecordsShouldBeFound(validFilter,healthRecord);
        defaultHealthRecordsShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"availableScore",DEFAULT_AVAILABLE_SCORE,UPDATED_AVAILABLE_SCORE);
        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"feelingsScore",DEFAULT_FEELINGS_SCORE,UPDATED_FEELINGS_SCORE);
        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"ipaqScore",DEFAULT_IPAQ_SCORE,UPDATED_IPAQ_SCORE);
        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"score",DEFAULT_SCORE,UPDATED_SCORE);
        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"stepsScore",DEFAULT_STEPS_SCORE,UPDATED_STEPS_SCORE);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"timestamp",DEFAULT_TIMESTAMP,UPDATED_TIMESTAMP);
        testValidAndInvalidFilters_ForDoubleValues(validFilter,invalidFilter,"whoqolScore",DEFAULT_WHOQOL_SCORE,UPDATED_WHOQOL_SCORE);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",healthRecord.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultHealthRecordsShouldBeFound(filter+validValue,healthRecord);
        defaultHealthRecordsShouldNotBeFound(filter+invalidValue);
        defaultHealthRecordsShouldBeFound(validFilter+"&"+filter+validValue,healthRecord);
        defaultHealthRecordsShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultHealthRecordsShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultHealthRecordsShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultHealthRecordsShouldBeFound(String filter, HealthRecord _healthRecord) throws Exception {
        restHealthRecordMockMvc.perform(get("/api/health-records?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_healthRecord.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(_healthRecord.getUserId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].availableScore").value(hasItem(DEFAULT_AVAILABLE_SCORE)))
            .andExpect(jsonPath("$.[*].stepsScore").value(hasItem(DEFAULT_STEPS_SCORE)))
            .andExpect(jsonPath("$.[*].ipaqScore").value(hasItem(DEFAULT_IPAQ_SCORE)))
            .andExpect(jsonPath("$.[*].whoqolScore").value(hasItem(DEFAULT_WHOQOL_SCORE)))
            .andExpect(jsonPath("$.[*].feelingsScore").value(hasItem(DEFAULT_FEELINGS_SCORE)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultHealthRecordsShouldNotBeFound(String filter) throws Exception {
        restHealthRecordMockMvc.perform(get("/api/health-records?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getHealthRecordById_NoUserInToken() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);
        getHealthRecordByIdRestCall_IsUnauthorized();

    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_NoEntityForId() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);

        // Get the healthRecord
        restHealthRecordMockMvc.perform(get("/api/health-records/{id}", Long.MAX_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsPacient() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsPacient_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsFamily() throws Exception {
        // Initialize the database
        healthRecord.setUserId(FAMILY_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsFamily_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userOrganization.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsOrganization() throws Exception {
        // Initialize the database
        healthRecord.setUserId(ORGANIZATION_USER_ID);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_AsOrganization_DifferentUserId() throws Exception {
        // Initialize the database
        Long differentUserId = userFamily.getId();
        healthRecord.setUserId(differentUserId);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getHealthRecordById_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        healthRecordRepository.saveAndFlush(healthRecord);

        getHealthRecordByIdRestCall_IsUnauthorized();
    }

    private void getHealthRecordByIdRestCall_IsOk() throws Exception {
        // Get the healthRecord
        restHealthRecordMockMvc.perform(get("/api/health-records/{id}", healthRecord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(healthRecord.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(healthRecord.getUserId().intValue()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
            .andExpect(jsonPath("$.availableScore").value(DEFAULT_AVAILABLE_SCORE))
            .andExpect(jsonPath("$.stepsScore").value(DEFAULT_STEPS_SCORE))
            .andExpect(jsonPath("$.ipaqScore").value(DEFAULT_IPAQ_SCORE))
            .andExpect(jsonPath("$.whoqolScore").value(DEFAULT_WHOQOL_SCORE))
            .andExpect(jsonPath("$.feelingsScore").value(DEFAULT_FEELINGS_SCORE));
    }

    private void getHealthRecordByIdRestCall_IsUnauthorized() throws Exception {
        // Get the healthRecord
        restHealthRecordMockMvc.perform(get("/api/health-records/{id}", healthRecord.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void deleteHealthRecord() throws Exception {
        // Initialize the database
        healthRecordRepository.saveAndFlush(healthRecord);

        int databaseSizeBeforeDelete = healthRecordRepository.findAll().size();

        // Delete the healthRecord
        restHealthRecordMockMvc.perform(delete("/api/health-records/{id}", healthRecord.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<HealthRecord> healthRecordList = healthRecordRepository.findAll();
        assertThat(healthRecordList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HealthRecord.class);
        HealthRecord healthRecord1 = new HealthRecord();
        healthRecord1.setId(1L);
        HealthRecord healthRecord2 = new HealthRecord();
        healthRecord2.setId(healthRecord1.getId());
        assertThat(healthRecord1).isEqualTo(healthRecord2);
        healthRecord2.setId(2L);
        assertThat(healthRecord1).isNotEqualTo(healthRecord2);
        healthRecord1.setId(null);
        assertThat(healthRecord1).isNotEqualTo(healthRecord2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HealthRecordDTO.class);
        HealthRecordDTO healthRecordDTO1 = new HealthRecordDTO();
        healthRecordDTO1.setId(1L);
        HealthRecordDTO healthRecordDTO2 = new HealthRecordDTO();
        assertThat(healthRecordDTO1).isNotEqualTo(healthRecordDTO2);
        healthRecordDTO2.setId(healthRecordDTO1.getId());
        assertThat(healthRecordDTO1).isEqualTo(healthRecordDTO2);
        healthRecordDTO2.setId(2L);
        assertThat(healthRecordDTO1).isNotEqualTo(healthRecordDTO2);
        healthRecordDTO1.setId(null);
        assertThat(healthRecordDTO1).isNotEqualTo(healthRecordDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(healthRecordMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(healthRecordMapper.fromId(null)).isNull();
    }
}
