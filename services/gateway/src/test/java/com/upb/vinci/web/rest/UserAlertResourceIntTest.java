package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.UserAlert;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.AlertType;
import com.upb.vinci.repository.UserAlertRepository;
import com.upb.vinci.service.UserAlertQueryService;
import com.upb.vinci.service.UserAlertService;
import com.upb.vinci.service.dto.UserAlertDTO;
import com.upb.vinci.service.mapper.UserAlertMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserAlertResource REST controller.
 *
 * @see UserAlertResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class UserAlertResourceIntTest extends UsersSetupController{

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_VALUES = "AAAAAAAAAA";
    private static final String UPDATED_VALUES = "BBBBBBBBBB";

    private static final AlertType DEFAULT_ALERT_TYPE = AlertType.SUCCESS;
    private static final AlertType UPDATED_ALERT_TYPE = AlertType.INFO;

    private static final Boolean DEFAULT_USER_READ = false;
    private static final Boolean UPDATED_USER_READ = true;

    private static final Boolean DEFAULT_FAMILY_READ = false;
    private static final Boolean UPDATED_FAMILY_READ = true;

    private static final Boolean DEFAULT_ORGANIZATION_READ = false;
    private static final Boolean UPDATED_ORGANIZATION_READ = true;

    @Autowired
    private UserAlertRepository userAlertRepository;

    @Autowired
    private UserAlertMapper userAlertMapper;

    @Autowired
    private UserAlertService userAlertService;

    @Autowired
    private UserAlertQueryService userAlertQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserAlertMockMvc;

    private UserAlert userAlert;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserAlertResource userAlertResource = new UserAlertResource(userAlertService, userAlertQueryService);
        this.restUserAlertMockMvc = MockMvcBuilders.standaloneSetup(userAlertResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAlert createUserAlertEntity(UserExtra userExtra) {
        return new UserAlert()
            .label(DEFAULT_LABEL)
            .values(DEFAULT_VALUES)
            .alertType(DEFAULT_ALERT_TYPE)
            .userRead(DEFAULT_USER_READ)
            .familyRead(DEFAULT_FAMILY_READ)
            .organizationRead(DEFAULT_ORGANIZATION_READ)
            .userExtra(userExtra);
    }

    private void initTest() {
        initUsers();
        userAlert = createUserAlertEntity(userExtraPacient);
    }

    @Test
    @Transactional
    public void createUserAlert_NoUserInToken() throws Exception {
        createUserAlert_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_WithExistingId() throws Exception {
        // Create the UserAlert with an existing ID
        userAlert.setId(1L);

        createUserAlert_BadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_checkLabelIsRequired() throws Exception {
        // set the field null
        userAlert.setLabel(null);

        createUserAlert_BadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_checkAlertTypeIsRequired() throws Exception {
        // set the field null
        userAlert.setAlertType(null);

        createUserAlert_BadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_checkUserExtraIdIsRequired() throws Exception {
        userAlert.setUserExtra(null);

        createUserAlert_BadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsPacient_BadUserExtraId() throws Exception {
        userAlert.setUserExtra(userExtraOrganization);

        createUserAlert_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsPacient() throws Exception {
        createUserAlert_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsFamily_IsBadRequest() throws Exception {
        userAlert.setUserExtra(userExtraOrganization);

        createUserAlert_IsUnauthorized();

    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsFamily() throws Exception {
        userAlert.setUserExtra(userExtraFamily);

        createUserAlert_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_OfPacient_AsFamily() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);

        createUserAlert_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_OfPacient_AsFamily_NotAssociated() throws Exception {
//        associatePacientWithFamily(userExtraPacient,userFamily);

        createUserAlert_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsOrganization_IsBadUserExtraId() throws Exception {
        userAlert.setUserExtra(userExtraFamily);

        createUserAlert_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_AsOrganization() throws Exception {
        userAlert.setUserExtra(userExtraOrganization);

        createUserAlert_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_OfPacient_AsOrganization() throws Exception {
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createUserAlert_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createUserAlert_OfPacient_AsOrganization_NotAssociated() throws Exception {
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createUserAlert_IsUnauthorized();
    }

    private void createUserAlert_BadRequest() throws Exception {
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(userAlert);
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restUserAlertMockMvc.perform(post("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
    }

    private void createUserAlert_IsUnauthorized() throws Exception {
        // Create the UserAlert
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(userAlert);
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();
        // An entity with an existing ID cannot be created, so this API call must fail
        restUserAlertMockMvc.perform(post("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
    }
    private void createUserAlert_IsCreated() throws Exception {
        // Create the UserAlert
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(userAlert);
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();
        restUserAlertMockMvc.perform(post("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.values").value(DEFAULT_VALUES))
            .andExpect(jsonPath("$.alertType").value(DEFAULT_ALERT_TYPE.name()))
            .andExpect(jsonPath("$.userRead").value(DEFAULT_USER_READ))
            .andExpect(jsonPath("$.familyRead").value(DEFAULT_FAMILY_READ))
            .andExpect(jsonPath("$.organizationRead").value(DEFAULT_ORGANIZATION_READ));

        // Validate the UserAlert in the database
        validateUserAlertInDatabase(databaseSizeBeforeTest + 1,true);
    }

    @Test
    @Transactional
    public void updateNonExistingUserAlert() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();
        // Create the UserAlert
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(userAlert);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserAlertMockMvc.perform(put("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_AsPacient() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_AsFamily() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsUnatuhorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_AsOrganization() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlert_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlert_IsUnatuhorized();
    }

    private void updateUserAlert_IsUnatuhorized() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();

        // Update the userAlert
        UserAlert updatedUserAlert = userAlertRepository.findById(userAlert.getId()).get();
        // Disconnect from session so that the updates on updatedUserAlert are not directly saved in db
        em.detach(updatedUserAlert);
        updatedUserAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(updatedUserAlert);

        restUserAlertMockMvc.perform(put("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andExpect(status().isUnauthorized());
        validateUserAlertInDatabase(databaseSizeBeforeTest,true);

    }

    private void validateUserAlertInDatabase(int databaseSizeBeforeTest,boolean isDefault) {
        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
        UserAlert testUserAlert = userAlertList.get(userAlertList.size() - 1);
        assertThat(testUserAlert.getLabel()).isEqualTo(isDefault ? DEFAULT_LABEL:UPDATED_LABEL);
        assertThat(testUserAlert.getValues()).isEqualTo(isDefault ? DEFAULT_VALUES:UPDATED_VALUES);
        assertThat(testUserAlert.getAlertType()).isEqualTo(isDefault ? DEFAULT_ALERT_TYPE:UPDATED_ALERT_TYPE);
        assertThat(testUserAlert.isUserRead()).isEqualTo(isDefault ? DEFAULT_USER_READ:UPDATED_USER_READ);
        assertThat(testUserAlert.isFamilyRead()).isEqualTo(isDefault ? DEFAULT_FAMILY_READ:UPDATED_FAMILY_READ);
        assertThat(testUserAlert.isOrganizationRead()).isEqualTo(isDefault ? DEFAULT_ORGANIZATION_READ:UPDATED_ORGANIZATION_READ);
    }

    private void updateUserAlert_IsOk() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();

        // Update the userAlert
        UserAlert updatedUserAlert = userAlertRepository.findById(userAlert.getId()).get();
        // Disconnect from session so that the updates on updatedUserAlert are not directly saved in db
        em.detach(updatedUserAlert);
        updatedUserAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(updatedUserAlert);

        restUserAlertMockMvc.perform(put("/api/user-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTO)))
            .andExpect(status().isOk());

        validateUserAlertInDatabase(databaseSizeBeforeTest,false);
    }

    @Test
    @Transactional
    public void updateEntitiesNonExistingUserAlert() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();
//        userAlertRepository.saveAndFlush(userAlert);
        // Create the UserAlert
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(userAlert);
        List<UserAlertDTO> userAlertDTOS = new ArrayList<UserAlertDTO>(){{add(userAlertDTO);}};
        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserAlertMockMvc.perform(put("/api/user-alerts/entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTOS)))
            .andExpect(status().isBadRequest());

        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_AsPacient() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_AsFamily() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsUnatuhorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_AsOrganization() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateUserAlertEntities_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        updateUserAlertList_IsUnatuhorized();
    }

    private void updateUserAlertList_IsUnatuhorized() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();

        // Update the userAlert
        UserAlert updatedUserAlert = userAlertRepository.findById(userAlert.getId()).get();
        // Disconnect from session so that the updates on updatedUserAlert are not directly saved in db
        em.detach(updatedUserAlert);
        updatedUserAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(updatedUserAlert);
        List<UserAlertDTO> userAlertDTOS = new ArrayList<UserAlertDTO>(){{add(userAlertDTO);}};
        restUserAlertMockMvc.perform(put("/api/user-alerts/entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTOS)))
            .andExpect(status().isUnauthorized());

        // Validate the UserAlert in the database
        validateUserAlertInDatabase(databaseSizeBeforeTest,true);
    }

    private void updateUserAlertList_IsOk() throws Exception {
        int databaseSizeBeforeTest = userAlertRepository.findAll().size();

        // Update the userAlert
        UserAlert updatedUserAlert = userAlertRepository.findById(userAlert.getId()).get();
        // Disconnect from session so that the updates on updatedUserAlert are not directly saved in db
        em.detach(updatedUserAlert);
        updatedUserAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        UserAlertDTO userAlertDTO = userAlertMapper.toDto(updatedUserAlert);
        List<UserAlertDTO> userAlertDTOS = new ArrayList<UserAlertDTO>(){{add(userAlertDTO);}};
        restUserAlertMockMvc.perform(put("/api/user-alerts/entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userAlertDTOS)))
            .andExpect(status().isOk());

        // Validate the UserAlert in the database
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest);
        UserAlert testUserAlert = userAlertList.get(userAlertList.size() - 1);
        assertThat(testUserAlert.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testUserAlert.getValues()).isEqualTo(UPDATED_VALUES);
        assertThat(testUserAlert.getAlertType()).isEqualTo(UPDATED_ALERT_TYPE);
        assertThat(testUserAlert.isUserRead()).isEqualTo(UPDATED_USER_READ);
        assertThat(testUserAlert.isFamilyRead()).isEqualTo(UPDATED_FAMILY_READ);
        assertThat(testUserAlert.isOrganizationRead()).isEqualTo(UPDATED_ORGANIZATION_READ);
    }

    @Test
    @Transactional
    public void getAllUserAlerts_NoUserInToken() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        // Get all the userAlertList
        restUserAlertMockMvc.perform(get("/api/user-alerts?sort=id,desc"))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsPacient() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsPacient_DifferentUsersAlert() throws Exception {
        // Initialize the database
        UserExtra differentUser = userExtraFamily;
        userAlert.setUserExtra(differentUser);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsFamily() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsFamily_DifferentUsersAlert() throws Exception {
        // Initialize the database
        UserExtra differentUser = userExtraOrganization;
        userAlert.setUserExtra(differentUser);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsOrganization() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_IsOk();

    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_AsOrganization_DifferentUsersAlert() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraFamily;
        userAlert.setUserExtra(differentUserExtra);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlerts_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getAllUserAlertsRestCall_EmptyList();

    }

    private void getAllUserAlertsRestCall_EmptyList() throws Exception {
        // Get all the userAlertList
        restUserAlertMockMvc.perform(get("/api/user-alerts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getAllUserAlertsRestCall_IsOk() throws Exception {
        // Get all the userAlertList
        restUserAlertMockMvc.perform(get("/api/user-alerts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAlert.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].values").value(hasItem(DEFAULT_VALUES)))
            .andExpect(jsonPath("$.[*].alertType").value(hasItem(DEFAULT_ALERT_TYPE.name())))
            .andExpect(jsonPath("$.[*].userRead").value(hasItem(DEFAULT_USER_READ)))
            .andExpect(jsonPath("$.[*].familyRead").value(hasItem(DEFAULT_FAMILY_READ)))
            .andExpect(jsonPath("$.[*].organizationRead").value(hasItem(DEFAULT_ORGANIZATION_READ)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlertsAlerts_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraPacient;
        UserExtra differentUserExtra = userExtraOrganization;
        userAlertRepository.saveAndFlush(userAlert);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlertsAlerts_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraFamily;
        UserExtra differentUserExtra = userExtraOrganization;
        userAlert.setUserExtra(validUserExtra);
        userAlertRepository.saveAndFlush(userAlert);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllUserAlertsAlerts_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraOrganization;
        UserExtra differentUserExtra = userExtraFamily;
        userAlert.setUserExtra(validUserExtra);
        userAlertRepository.saveAndFlush(userAlert);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultUserAlertShouldBeFound(validFilter,userAlert);
        defaultUserAlertShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"label",DEFAULT_LABEL,UPDATED_LABEL);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"values",DEFAULT_VALUES,UPDATED_VALUES);
        testValidAndInvalidFilters_ForEnumTypeValues(validFilter,invalidFilter,"alertType",DEFAULT_ALERT_TYPE,UPDATED_ALERT_TYPE);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"userRead",DEFAULT_USER_READ,UPDATED_USER_READ);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"familyRead",DEFAULT_FAMILY_READ,UPDATED_FAMILY_READ);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"organizationRead",DEFAULT_ORGANIZATION_READ,UPDATED_ORGANIZATION_READ);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",userAlert.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultUserAlertShouldBeFound(filter+validValue,userAlert);
        defaultUserAlertShouldNotBeFound(filter+invalidValue);
        defaultUserAlertShouldBeFound(validFilter+"&"+filter+validValue,userAlert);
        defaultUserAlertShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultUserAlertShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultUserAlertShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultUserAlertShouldBeFound(String filter, UserAlert _userAlert) throws Exception {
        restUserAlertMockMvc.perform(get("/api/user-alerts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_userAlert.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].values").value(hasItem(DEFAULT_VALUES)))
            .andExpect(jsonPath("$.[*].alertType").value(hasItem(DEFAULT_ALERT_TYPE.name())))
            .andExpect(jsonPath("$.[*].userRead").value(hasItem(DEFAULT_USER_READ)))
            .andExpect(jsonPath("$.[*].familyRead").value(hasItem(DEFAULT_FAMILY_READ)))
            .andExpect(jsonPath("$.[*].organizationRead").value(hasItem(DEFAULT_ORGANIZATION_READ)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultUserAlertShouldNotBeFound(String filter) throws Exception {
        restUserAlertMockMvc.perform(get("/api/user-alerts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getUserAlertById_NoUserInToken() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_NoEntity() throws Exception {
        // Initialize the database
//        userAlertRepository.saveAndFlush(userAlert);

        Long badId = 123123123L;
        // Get the userAlert
        restUserAlertMockMvc.perform(get("/api/user-alerts/{id}", badId))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsPacient() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsPacient_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraFamily;
        userAlert.setUserExtra(differentUserExtra);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsFamily() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsFamily_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraOrganization;
        userAlert.setUserExtra(differentUserExtra);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsOrganization() throws Exception {
        // Initialize the database
        userAlert.setUserExtra(userExtraOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_AsOrganization_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraFamily;
        userAlert.setUserExtra(differentUserExtra);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUserAlertById_OfPacient_AsOrganization_IsNotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        userAlertRepository.saveAndFlush(userAlert);

        getUserAlertById_IsUnauthorized();
    }

    private void getUserAlertById_IsUnauthorized() throws Exception {
        // Get the userAlert
        restUserAlertMockMvc.perform(get("/api/user-alerts/{id}", userAlert.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getUserAlertById_IsOk() throws Exception {
        // Get the userAlert
        restUserAlertMockMvc.perform(get("/api/user-alerts/{id}", userAlert.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userAlert.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.values").value(DEFAULT_VALUES))
            .andExpect(jsonPath("$.alertType").value(DEFAULT_ALERT_TYPE.name()))
            .andExpect(jsonPath("$.userRead").value(DEFAULT_USER_READ))
            .andExpect(jsonPath("$.familyRead").value(DEFAULT_FAMILY_READ))
            .andExpect(jsonPath("$.organizationRead").value(DEFAULT_ORGANIZATION_READ));
    }

    @Test
    @Transactional
    public void deleteUserAlert() throws Exception {
        // Initialize the database
        userAlertRepository.saveAndFlush(userAlert);

        int databaseSizeBeforeTest = userAlertRepository.findAll().size();

        // Delete the userAlert
        restUserAlertMockMvc.perform(delete("/api/user-alerts/{id}", userAlert.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserAlert> userAlertList = userAlertRepository.findAll();
        assertThat(userAlertList).hasSize(databaseSizeBeforeTest - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAlert.class);
        UserAlert userAlert1 = new UserAlert();
        userAlert1.setId(1L);
        UserAlert userAlert2 = new UserAlert();
        userAlert2.setId(userAlert1.getId());
        assertThat(userAlert1).isEqualTo(userAlert2);
        userAlert2.setId(2L);
        assertThat(userAlert1).isNotEqualTo(userAlert2);
        userAlert1.setId(null);
        assertThat(userAlert1).isNotEqualTo(userAlert2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAlertDTO.class);
        UserAlertDTO userAlertDTO1 = new UserAlertDTO();
        userAlertDTO1.setId(1L);
        UserAlertDTO userAlertDTO2 = new UserAlertDTO();
        assertThat(userAlertDTO1).isNotEqualTo(userAlertDTO2);
        userAlertDTO2.setId(userAlertDTO1.getId());
        assertThat(userAlertDTO1).isEqualTo(userAlertDTO2);
        userAlertDTO2.setId(2L);
        assertThat(userAlertDTO1).isNotEqualTo(userAlertDTO2);
        userAlertDTO1.setId(null);
        assertThat(userAlertDTO1).isNotEqualTo(userAlertDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userAlertMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userAlertMapper.fromId(null)).isNull();
    }
}
