package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.*;
import com.upb.vinci.domain.enumeration.AlertType;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.repository.*;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.UserService;
import com.upb.vinci.service.dto.UserAggregateDTO;
import com.upb.vinci.service.dto.UserDTO;
import com.upb.vinci.service.mapper.UserMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import com.upb.vinci.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.zalando.problem.Status;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class UserResourceIntTest {

    private static final String DEFAULT_LOGIN = "user";
    private static final String UPDATED_LOGIN = "jhipster";

    private static final Long DEFAULT_ID = 4L;

    private static final String DEFAULT_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String UPDATED_PASSWORD = RandomStringUtils.random(60);

    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String UPDATED_EMAIL = "jhipster@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String UPDATED_FIRSTNAME = "jhipsterFirstName";

    private static final String DEFAULT_LASTNAME = "doe";
    private static final String UPDATED_LASTNAME = "jhipsterLastName";

    private static final String DEFAULT_IMAGEURL = "http://placehold.it/50x50";
    private static final String UPDATED_IMAGEURL = "http://placehold.it/40x40";

    private static final String DEFAULT_LANGKEY = "en";
    private static final String UPDATED_LANGKEY = "fr";

    private static final Long USER_ID = 4L;
    private static final String USER_LOGIN = "user";
    private static final String USER_PASSWORD = "123456789012345678901234567890123456789012345678901234567890";
    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String DEFAULT_UUID = UUID.randomUUID().toString();

    private static final Long FAMILY_ID = 5L;
    private static final String FAMILY_LOGIN = "family_login";
    private static final String FAMILY_EMAIL = "family@localhost";
    private static final String FAMILY_FIRSTNAME = "family-firstname";
    private static final String FAMILY_LASTNAME = "family-lastname";

    private static final Long ORGANIZATION_ID = 6L;
    private static final String ORGANIZATION_LOGIN = "organization_login";
    private static final String ORGANIZATION_EMAIL = "organization@localhost";
    private static final String ORGANIZATION_FIRSTNAME = "organization-firstname";
    private static final String ORGANIZATION_LASTNAME = "organization-lastname";

    private static final String ADMIN_LOGIN = "admin";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserExtraRepository userExtraRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserAlertRepository userAlertRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    private MockMvc restUserMockMvc;

    private User user;

    private User familyUser;

    private User organizationUser;

    private UserExtra userExtra;

    @Before
    public void setup() {
        cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).clear();
        cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).clear();
        UserResource userResource = new UserResource(userService);

        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter)
            .build();
        initTest();
    }

    /**
     * Create a User.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User entity.
     */
    public static User createEntity(EntityManager em) {
        User user = new User();
        user.setId(DEFAULT_ID);
        user.setLogin(USER_LOGIN);
        user.setPassword(USER_PASSWORD);
        user.setActivated(true);
        user.setEmail(DEFAULT_EMAIL);
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setLastName(DEFAULT_LASTNAME);
        user.setImageUrl(DEFAULT_IMAGEURL);
        user.setLangKey(DEFAULT_LANGKEY);
        return user;
    }

    public static UserAlert createUserAlert(EntityManager em,UserExtra userExtra) {
        UserAlert userAlert = new UserAlert();
        userAlert.setLabel("LABEL");
        userAlert.setValues("VALUE");
        userAlert.setAlertType(AlertType.INFO);
        userAlert.setUserRead(false);
        userAlert.setUserExtra(userExtra);
        userAlert.setOrganizationRead(false);
        userAlert.setFamilyRead(false);
        userAlert.setCreatedDate(Instant.now());
        return userAlert;
    }

    public static User createFamilyUser(EntityManager em) {
        User user = new User();
        user.setLogin(FAMILY_LOGIN);
        user.setPassword(USER_PASSWORD);
        user.setActivated(true);
        user.setEmail(FAMILY_EMAIL);
        user.setFirstName(FAMILY_FIRSTNAME);
        user.setLastName(FAMILY_LASTNAME);
        user.setLangKey(DEFAULT_LANGKEY);
        return user;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtra createUserExtraEntity(EntityManager em) {
        UserExtra userExtra = new UserExtra()
            .phone(DEFAULT_PHONE)
            .address(DEFAULT_ADDRESS);
        return userExtra;
    }

    private void initTest() {
        System.out.println(userRepository.findAll());
        Set<Authority> patientAuthorities = new HashSet<Authority>() {{
            add(new Authority(AuthoritiesConstants.USER));
            add(new Authority(AuthoritiesConstants.PACIENT));
        }};
        Set<Authority> familyAuthorities = new HashSet<Authority>() {{
            add(new Authority(AuthoritiesConstants.USER));
            add(new Authority(AuthoritiesConstants.FAMILY));
        }};
        Set<Authority> organizationAuthorities = new HashSet<Authority>() {{
            add(new Authority(AuthoritiesConstants.USER));
            add(new Authority(AuthoritiesConstants.ORGANIZIATION));
        }};

        user = createEntity(em);
        user.setLogin(USER_LOGIN);
        user.setPassword(passwordEncoder.encode(USER_PASSWORD));
        user.setAuthorities(patientAuthorities);
        userRepository.saveAndFlush(user);

        familyUser = userRepository.getOne(FAMILY_ID);
        organizationUser = userRepository.getOne(ORGANIZATION_ID);
    }

    @Test
    @Transactional
    public void createUser() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        // Create the User
        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin("new_"+DEFAULT_LOGIN);
        managedUserVM.setPassword("new_"+DEFAULT_PASSWORD);
        managedUserVM.setFirstName("new_"+DEFAULT_FIRSTNAME);
        managedUserVM.setLastName("new_"+DEFAULT_LASTNAME);
        managedUserVM.setEmail("new_"+DEFAULT_EMAIL);
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("new_"+DEFAULT_LOGIN);
        userDTO.setFirstName("new_"+DEFAULT_FIRSTNAME);
        userDTO.setLastName("new_"+DEFAULT_LASTNAME);
        userDTO.setEmail("new_"+DEFAULT_EMAIL);
        userDTO.setActivated(true);
        userDTO.setImageUrl(DEFAULT_IMAGEURL);
        userDTO.setLangKey(DEFAULT_LANGKEY);
        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        restUserMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andExpect(status().isCreated());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeCreate + 1);
        User testUser = userList.get(userList.size() - 1);
        assertThat(testUser.getLogin()).isEqualTo("new_"+DEFAULT_LOGIN);
        assertThat(testUser.getFirstName()).isEqualTo("new_"+DEFAULT_FIRSTNAME);
        assertThat(testUser.getLastName()).isEqualTo("new_"+DEFAULT_LASTNAME);
        assertThat(testUser.getEmail()).isEqualTo("new_"+DEFAULT_EMAIL);
        assertThat(testUser.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
        assertThat(testUser.getLangKey()).isEqualTo(DEFAULT_LANGKEY);

        //todo: test if mail was sent
    }


    @Test
    @Transactional
    public void createUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setId(1L);
        managedUserVM.setLogin(DEFAULT_LOGIN);
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createUserWithExistingLogin() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin(DEFAULT_LOGIN);// this login should already be used
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail("anothermail@localhost");
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        // Create the User
        restUserMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createUserWithExistingEmail() throws Exception {
        // Initialize the database
//        userRepository.saveAndFlush(user);
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setLogin("anotherlogin");
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);// this email should already be used
        managedUserVM.setActivated(true);
        managedUserVM.setImageUrl(DEFAULT_IMAGEURL);
        managedUserVM.setLangKey(DEFAULT_LANGKEY);
        managedUserVM.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        // Create the User
        restUserMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(managedUserVM)))
            .andExpect(status().isBadRequest());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void testRegisterPacientAsOrganization_WithNoEmailPresent() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("new-userName");
        userDTO.setLastName("new-lastName");
        userDTO.setPhone("0645747555");

        String returnLogin = (userDTO.getFirstName().trim() + "." + userDTO.getLastName().replace(" ", "")).toLowerCase();
        String returnMail = returnLogin + "@anonymous.com";

        Long usersBeforeRequest = userRepository.count();
        Long userExtrasBeforeRequest = userExtraRepository.count();
        restUserMockMvc.perform(
            post("/api/users/organisation")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andDo(result -> {
                System.out.println(result.getResponse().getContentAsString());
                userExtraRepository.findAll().forEach(System.out::println);
            })
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.email").value(returnMail))
            .andExpect(jsonPath("$.login").value(returnLogin))
            .andExpect(jsonPath("$.activated").value(true));

        //testing if there was proper association
        List<UserExtra> pacientUser = userExtraRepository.findByOrganization(userRepository.findOneByLogin(ORGANIZATION_LOGIN).get());
        assertThat(pacientUser.size()).isEqualTo(1);
        assertThat(pacientUser.get(0).getOrganization().getLogin()).isEqualTo(ORGANIZATION_LOGIN);
        assertThat(pacientUser.get(0).getUser().getLogin()).isEqualTo(returnLogin);
        assertThat(pacientUser.get(0).getUser().getEmail()).isEqualTo(returnMail);
        assertThat(pacientUser.get(0).getUser().getActivated()).isEqualTo(true);
        assertThat(userRepository.count()).isEqualTo(usersBeforeRequest+1);
        assertThat(userExtraRepository.count()).isEqualTo(userExtrasBeforeRequest+1);

        //todo: test if mail was sent
    }
    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void testRegisterPacientAsOrganization_WithEmailPresent() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("email@localhost");
        userDTO.setFirstName("new-userName");
        userDTO.setLastName("new-lastName");
        userDTO.setPhone("0645747555");

        String returnLogin = (userDTO.getFirstName().trim() + "." + userDTO.getLastName().replace(" ", "")).toLowerCase();
        String returnMail = returnLogin + "@anonymous.com";

        Long usersBeforeRequest = userRepository.count();
        Long userExtrasBeforeRequest = userExtraRepository.count();
        restUserMockMvc.perform(
            post("/api/users/organisation")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andDo(result -> {
                System.out.println(result.getResponse().getContentAsString());
                userExtraRepository.findAll().forEach(System.out::println);
            })
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.email").value(userDTO.getEmail()))
            .andExpect(jsonPath("$.login").value(returnLogin))
            .andExpect(jsonPath("$.activated").value(false));

        //testing if there was proper association
        List<UserExtra> pacientUser = userExtraRepository.findByOrganization(userRepository.findOneByLogin(ORGANIZATION_LOGIN).get());
        assertThat(pacientUser.size()).isEqualTo(1);
        assertThat(pacientUser.get(0).getOrganization().getLogin()).isEqualTo(ORGANIZATION_LOGIN);
        assertThat(pacientUser.get(0).getUser().getLogin()).isEqualTo(returnLogin);
        assertThat(pacientUser.get(0).getUser().getEmail()).isEqualTo(userDTO.getEmail());
        assertThat(pacientUser.get(0).getUser().getActivated()).isEqualTo(false);
        assertThat(userRepository.count()).isEqualTo(usersBeforeRequest+1);
        assertThat(userExtraRepository.count()).isEqualTo(userExtrasBeforeRequest+1);

        //todo: test if mail was sent
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void testRegisterPacientAsOrganization_UserIdPresent() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(4L);
        userDTO.setFirstName("new-userName");
        userDTO.setLastName("new-lastName");
        userDTO.setPhone("0645747555");

        Long usersBeforeRequest = userRepository.count();
        Long userExtrasBeforeRequest = userExtraRepository.count();
        restUserMockMvc.perform(
            post("/api/users/organisation")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isBadRequest());
        assertThat(userRepository.count()).isEqualTo(usersBeforeRequest);
        assertThat(userExtraRepository.count()).isEqualTo(userExtrasBeforeRequest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void testRegisterPacientAsOrganization_LoginExists() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("new-userName");
        userDTO.setLastName("new-lastName");
        userDTO.setPhone("0645747555");

        String returnLogin = (userDTO.getFirstName().trim() + "." + userDTO.getLastName().replace(" ", "")).toLowerCase();
        String returnMail = returnLogin + "@anonymous.com";

        userDTO.setLogin(returnLogin);
        userDTO.setEmail(returnMail);
        User user = userMapper.userDTOToUser(userDTO);
        user.setPassword(passwordEncoder.encode(USER_PASSWORD));
        userRepository.save(user);

        Long usersBeforeRequest = userRepository.count();
        Long userExtrasBeforeRequest = userExtraRepository.count();
        restUserMockMvc.perform(
            post("/api/users/organisation")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isBadRequest());
        assertThat(userRepository.count()).isEqualTo(usersBeforeRequest);
        assertThat(userExtraRepository.count()).isEqualTo(userExtrasBeforeRequest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void testRegisterPacientAsOrganization_EmailExists() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("new-userName");
        userDTO.setLastName("new-lastName");
        userDTO.setPhone("0645747555");


        String returnLogin = (userDTO.getFirstName().trim() + "." + userDTO.getLastName().replace(" ", "")).toLowerCase();
        String returnMail = returnLogin + "@anonymous.com";

        userDTO.setLogin(returnLogin);
        userDTO.setEmail(returnMail);
        User user = userMapper.userDTOToUser(userDTO);
        user.setPassword(passwordEncoder.encode(USER_PASSWORD));
        userRepository.save(user);

        Long usersBeforeRequest = userRepository.count();
        Long userExtrasBeforeRequest = userExtraRepository.count();
        restUserMockMvc.perform(
            post("/api/users/organisation")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isBadRequest());
        assertThat(userRepository.count()).isEqualTo(usersBeforeRequest);
        assertThat(userExtraRepository.count()).isEqualTo(userExtrasBeforeRequest);
    }

    @Test
    @Transactional
    public void updateUser() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeUpdate = userRepository.findAll().size();

        // Update the user
        User updatedUser = userRepository.findById(user.getId()).get();

        updatedUser.setPassword(UPDATED_PASSWORD);
        updatedUser.setFirstName(UPDATED_FIRSTNAME);
        updatedUser.setLastName(UPDATED_LASTNAME);
        updatedUser.setEmail(UPDATED_EMAIL);
        updatedUser.setImageUrl(UPDATED_IMAGEURL);
        updatedUser.setLangKey(UPDATED_LANGKEY);

        UserDTO payload = userMapper.userToUserDTO(updatedUser);

        restUserMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payload)))
            .andExpect(status().isOk());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeUpdate);
        User testUser = userRepository.getOne(user.getId());
        assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testUser.getLastName()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUser.getImageUrl()).isEqualTo(UPDATED_IMAGEURL);
        assertThat(testUser.getLangKey()).isEqualTo(UPDATED_LANGKEY);
    }

    @Test
    @Transactional
    public void updateUserLogin() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeUpdate = userRepository.findAll().size();

        // Update the user
        User updatedUser = userRepository.findById(user.getId()).get();

        updatedUser.setLogin(UPDATED_LOGIN);
        updatedUser.setPassword(UPDATED_PASSWORD);
        updatedUser.setFirstName(UPDATED_FIRSTNAME);
        updatedUser.setLastName(UPDATED_LASTNAME);
        updatedUser.setEmail(UPDATED_EMAIL);
        updatedUser.setImageUrl(UPDATED_IMAGEURL);
        updatedUser.setLangKey(UPDATED_LANGKEY);

        UserDTO payload = userMapper.userToUserDTO(updatedUser);

        restUserMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payload)))
            .andExpect(status().isOk());

        // Validate the User in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeUpdate);
        User testUser =  userRepository.getOne(user.getId());
        assertThat(testUser.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testUser.getLastName()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUser.getImageUrl()).isEqualTo(UPDATED_IMAGEURL);
        assertThat(testUser.getLangKey()).isEqualTo(UPDATED_LANGKEY);
    }

    @Test
    @Transactional
    public void updateUser_ExistingEmail() throws Exception {
        // Initialize the database with 2 users
        userRepository.saveAndFlush(user);

        User anotherUser = new User();
        anotherUser.setLogin("jhipster");
        anotherUser.setPassword(RandomStringUtils.random(60));
        anotherUser.setActivated(true);
        anotherUser.setEmail("jhipster@localhost");
        anotherUser.setFirstName("java");
        anotherUser.setLastName("hipster");
        anotherUser.setImageUrl("");
        anotherUser.setLangKey("en");
        userRepository.saveAndFlush(anotherUser);

        // Update the user
        User updatedUser = userRepository.findById(user.getId()).get();

        updatedUser.setEmail("jhipster@localhost");// this email should already be used by anotherUser
        em.clear();
        restUserMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUser)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateUser_ExistingLogin() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);

        User anotherUser = new User();
        anotherUser.setLogin("jhipster");
        anotherUser.setPassword(RandomStringUtils.random(60));
        anotherUser.setActivated(true);
        anotherUser.setEmail("jhipster@localhost");
        anotherUser.setFirstName("java");
        anotherUser.setLastName("hipster");
        anotherUser.setImageUrl("");
        anotherUser.setLangKey("en");
        userRepository.saveAndFlush(anotherUser);

        // Update the user
        User updatedUser = userRepository.findById(user.getId()).get();

        updatedUser.setLogin("jhipster");// this login should already be used by anotherUser
        em.clear();
        restUserMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUser)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = USER_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUsers_AsPacient() throws Exception {

        // Get all the users
        restUserMockMvc.perform(get("/api/users?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGEURL)))
            .andExpect(jsonPath("$.[*].langKey").value(hasItem(DEFAULT_LANGKEY)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUsers_AsOrganization() throws Exception {
        UserExtra organizationUserExtra = createUserExtraEntity(em);
        organizationUserExtra.setUser(organizationUser);
        userExtraRepository.saveAndFlush(organizationUserExtra);

        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setOrganization(organizationUser);
        userExtraRepository.saveAndFlush(userExtra);

        // Get all the users
        restUserMockMvc.perform(get("/api/users?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGEURL)))
            .andExpect(jsonPath("$.[*].langKey").value(hasItem(DEFAULT_LANGKEY)));
    }
    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUsers_AsOrganization_NoUsers() throws Exception {

        // Get all the users
        restUserMockMvc.perform(get("/api/users?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUsers_AsFamily() throws Exception {
        UserExtra familyUserExtra = createUserExtraEntity(em);
        familyUserExtra.setUser(familyUser);
        userExtraRepository.saveAndFlush(familyUserExtra);

        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(familyUser);
        userExtraRepository.saveAndFlush(userExtra);

        // Get all the users
        restUserMockMvc.perform(get("/api/users?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGEURL)))
            .andExpect(jsonPath("$.[*].langKey").value(hasItem(DEFAULT_LANGKEY)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllUsers_AsFamily_NoUsers() throws Exception {

        // Get all the users
        restUserMockMvc.perform(get("/api/users?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    @Transactional
    public void deleteUser() throws Exception {
        // Initialize the database
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(familyUser);
        userExtraRepository.saveAndFlush(userExtra);

        UserAlert userAlert = createUserAlert(em,userExtra);
        userAlertRepository.saveAndFlush(userAlert);

        long numOfUsersBeforeRequest = userRepository.count();
        long numOfUserExtrasBeforeRequest = userExtraRepository.count();

        em.clear();
        // Delete the user
        restUserMockMvc.perform(delete("/api/users/{login}", user.getLogin())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        assertThat(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNull();

        // Validate the database is empty
        assertThat(userRepository.count()).isEqualTo(numOfUsersBeforeRequest - 1);
        assertThat(userExtraRepository.count()).isEqualTo(numOfUserExtrasBeforeRequest - 1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = USER_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUser() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);

        assertThat(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNull();

        // Get the user
        restUserMockMvc.perform(get("/api/users/{login}", user.getLogin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.login").value(user.getLogin()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGEURL))
            .andExpect(jsonPath("$.langKey").value(DEFAULT_LANGKEY));

        assertThat(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).get(user.getLogin())).isNotNull();
    }

    @Test
    @Transactional
    @WithUserDetails(value = USER_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getNonExistingUser() throws Exception {
        restUserMockMvc.perform(get("/api/users/unknown"))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.title").value("NO_USER_BY_LOGIN_EXCEPTION"))
            .andExpect(jsonPath("$.status").value(String.valueOf(Status.BAD_REQUEST.getStatusCode())))
            .andExpect(jsonPath("$.detail").value("There is no user with login: unknown"));
    }

    @Test
    @Transactional
    public void getAllAgregatedUsers_NoUserInToken() throws Exception {
        // Get all the users
        restUserMockMvc.perform(get("/api/users/aggregated")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllAgregatedUsers_AsFamily() throws Exception {
        //setup user and associate him to family
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(familyUser);
        userExtraRepository.saveAndFlush(userExtra);

        List<UserAggregateDTO> returnValue = new ArrayList<>();
        // Get all the users
        restUserMockMvc.perform(get("/api/users/aggregated")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].userInfo.id").value(hasItem(userExtra.getId().intValue())))
            .andExpect(jsonPath("$.[*].userInfo.userId").value(hasItem(user.getId().intValue())))
            .andExpect(jsonPath("$.[*].pulse").value(hasItem("pulse test")))
            .andExpect(jsonPath("$.[*].heartRate").value(hasItem("HR test")))
            .andExpect(jsonPath("$.[*].mood").value(hasItem("mood test")))
            .andExpect(jsonPath("$.[*].fitness").value(hasItem("fitnesss test")));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllAgregatedUsers_AsOrganization() throws Exception {
        //setup user and associate him to family
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setOrganization(organizationUser);
        userExtraRepository.saveAndFlush(userExtra);

        List<UserAggregateDTO> returnValue = new ArrayList<>();
        // Get all the users
        restUserMockMvc.perform(get("/api/users/aggregated")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].userInfo.id").value(hasItem(userExtra.getId().intValue())))
            .andExpect(jsonPath("$.[*].userInfo.userId").value(hasItem(user.getId().intValue())))
            .andExpect(jsonPath("$.[*].pulse").value(hasItem("pulse test")))
            .andExpect(jsonPath("$.[*].heartRate").value(hasItem("HR test")))
            .andExpect(jsonPath("$.[*].mood").value(hasItem("mood test")))
            .andExpect(jsonPath("$.[*].fitness").value(hasItem("fitnesss test")));
    }

    @Test
    @Transactional
    public void getUsersWithAuthorities_NoUserInToken() throws Exception {
        // Get all the users
        restUserMockMvc.perform(get("/api/users/getUserWithAuthorities")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUsersWithAuthorities_AsAdmin() throws Exception {
        setUpForRequestToGetUsersWithAuthorities();

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getUserWithAuthorities")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //checking devices
            .andExpect(jsonPath("$.patientsOfFamily",hasSize(0)))
            .andExpect(jsonPath("$.patientsOfOrganization",hasSize(0)))
            .andExpect(jsonPath("$.personalDevices.[*].name").value(hasItem("survey-name")))
            .andExpect(jsonPath("$.personalDevices.[*].description").value(hasItem("survey-description")))
            .andExpect(jsonPath("$.personalDevices.[*].uuid").value(hasItem("survey-uuid")))
            .andExpect(jsonPath("$.personalDevices.[*].deviceType").value(hasItem(DeviceType.SURVEY.name())))
            .andExpect(jsonPath("$.personalDevices.[*].active").value(hasItem(true)))
            .andExpect(jsonPath("$.personalDevices.[*].userExtraId").value(hasItem(userExtra.getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = USER_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUsersWithAuthorities_AsPacient() throws Exception {
        setUpForRequestToGetUsersWithAuthorities();

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getUserWithAuthorities")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.userExtraId").value(userExtra.getId().intValue()))
            .andExpect(jsonPath("$.id").value(user.getId().intValue()))
            .andExpect(jsonPath("$.login").value(user.getLogin()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.langKey").value(DEFAULT_LANGKEY))
            //checking devices
            .andExpect(jsonPath("$.patientsOfFamily",hasSize(0)))
            .andExpect(jsonPath("$.patientsOfOrganization",hasSize(0)))
            .andExpect(jsonPath("$.personalDevices.[*].name").value(hasItem("survey-name")))
            .andExpect(jsonPath("$.personalDevices.[*].description").value(hasItem("survey-description")))
            .andExpect(jsonPath("$.personalDevices.[*].uuid").value(hasItem("survey-uuid")))
            .andExpect(jsonPath("$.personalDevices.[*].deviceType").value(hasItem(DeviceType.SURVEY.name())))
            .andExpect(jsonPath("$.personalDevices.[*].active").value(hasItem(true)))
            .andExpect(jsonPath("$.personalDevices.[*].userExtraId").value(hasItem(userExtra.getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUsersWithAuthorities_AsFamily() throws Exception {
        setUpForRequestToGetUsersWithAuthorities();

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getUserWithAuthorities")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(familyUser.getId().intValue()))
            .andExpect(jsonPath("$.login").value(familyUser.getLogin()))
            .andExpect(jsonPath("$.firstName").value(familyUser.getFirstName()))
            .andExpect(jsonPath("$.lastName").value(familyUser.getLastName()))
            .andExpect(jsonPath("$.email").value(familyUser.getEmail()))
            .andExpect(jsonPath("$.langKey").value(familyUser.getLangKey()))
            //checking devices
            .andExpect(jsonPath("$.personalDevices",hasSize(0)))
            .andExpect(jsonPath("$.patientsOfOrganization",hasSize(0)))
            .andExpect(jsonPath("$.patientsOfFamily.[*].name").value(hasItem("survey-name")))
            .andExpect(jsonPath("$.patientsOfFamily.[*].description").value(hasItem("survey-description")))
            .andExpect(jsonPath("$.patientsOfFamily.[*].uuid").value(hasItem("survey-uuid")))
            .andExpect(jsonPath("$.patientsOfFamily.[*].deviceType").value(hasItem(DeviceType.SURVEY.name())))
            .andExpect(jsonPath("$.patientsOfFamily.[*].active").value(hasItem(true)))
            .andExpect(jsonPath("$.patientsOfFamily.[*].userExtraId").value(hasItem(userExtra.getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getUsersWithAuthorities_AsOrganization() throws Exception {
        setUpForRequestToGetUsersWithAuthorities();

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getUserWithAuthorities")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organizationUser.getId()))
            .andExpect(jsonPath("$.login").value(organizationUser.getLogin()))
            .andExpect(jsonPath("$.firstName").value(organizationUser.getFirstName()))
            .andExpect(jsonPath("$.lastName").value(organizationUser.getLastName()))
            .andExpect(jsonPath("$.email").value(organizationUser.getEmail()))
            .andExpect(jsonPath("$.langKey").value(organizationUser.getLangKey()))
            //checking devices
            .andExpect(jsonPath("$.patientsOfFamily",hasSize(0)))
            .andExpect(jsonPath("$.personalDevices",hasSize(0)))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].name").value(hasItem("survey-name")))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].description").value(hasItem("survey-description")))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].uuid").value(hasItem("survey-uuid")))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].deviceType").value(hasItem(DeviceType.SURVEY.name())))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].active").value(hasItem(true)))
            .andExpect(jsonPath("$.patientsOfOrganization.[*].userExtraId").value(hasItem(userExtra.getId().intValue())));
    }

    private void setUpForRequestToGetUsersWithAuthorities() {
        //setup user associations
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setOrganization(organizationUser);
        userExtra.setFamily(familyUser);
        userExtraRepository.saveAndFlush(userExtra);

        UserExtra userExtraFamily = createUserExtraEntity(em);
        userExtraFamily.setUser(familyUser);
        userExtraRepository.saveAndFlush(userExtraFamily);

        UserExtra userExtraOrganization = createUserExtraEntity(em);
        userExtraOrganization.setUser(organizationUser);
        userExtraRepository.saveAndFlush(userExtraOrganization);

        //setup devices
        Device surveyDevice = new Device()
            .name("survey-name")
            .description("survey-description")
            .uuid("survey-uuid")
            .deviceType(DeviceType.SURVEY)
            .active(true)
            .userExtra(userExtra)
            .startTimestamp(Instant.now());
        deviceRepository.saveAndFlush(surveyDevice);
        em.clear();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_AsFamily() throws Exception {
        // Initialize the database
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(user);
        userExtra.setOrganization(user);
        userExtra.setUuid(DEFAULT_UUID);
        userExtraRepository.saveAndFlush(userExtra);

        User user2 = createEntity(em);
        user2.setAuthorities(new HashSet<Authority>(){{add(new Authority(AuthoritiesConstants.FAMILY));}});
        user2.setLogin("testing-login");
        user2.setEmail("testing-mail@gmail.com");
        user2.setFirstName("should not bw shown");
        user2.setId(null);
        user2.setAuthorities(new HashSet<Authority>() {{
            add(new Authority(AuthoritiesConstants.USER));
            add(new Authority(AuthoritiesConstants.PACIENT));
        }});
        userRepository.saveAndFlush(user2);

        UserExtra userExtra2 = createUserExtraEntity(em);
        userExtra2.setUser(user2);
        userExtra2.setFamily(user2);
        userExtra2.setOrganization(user2);
        userExtra2.setUuid(UUID.randomUUID().toString());
        userExtraRepository.saveAndFlush(userExtra2);

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(2)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //first user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(4)))
            //second user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(userExtra2.getUuid())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(user2.getFirstName())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(user2.getLastName())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(user2.getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_AsFamily_NobodyAssociatedToFamily() throws Exception {
        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(0)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_OnlyOneItemReturned_AsFamily() throws Exception {
        // Initialize the database
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(user);
        userExtra.setOrganization(user);
        userExtra.setUuid(DEFAULT_UUID);
        userExtraRepository.saveAndFlush(userExtra);

        User user2 = createEntity(em);
        user2.setAuthorities(new HashSet<Authority>(){{add(new Authority(AuthoritiesConstants.FAMILY));}});
        user2.setLogin("testing-login");
        user2.setEmail("testing-mail@gmail.com");
        user2.setFirstName("should not bw shown");
        user2.setId(null);
        UserExtra userExtra2 = createUserExtraEntity(em);
//        userExtra2.setUser(user2);
//        userExtra2.setFamily(user2);
        userExtra2.setOrganization(user2);
        userExtra2.setUuid(UUID.randomUUID().toString());
        userRepository.save(user2);
        userExtraRepository.saveAndFlush(userExtra2);

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //first user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(4)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_AsOrganization() throws Exception {
        // Initialize the database
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(user);
        userExtra.setOrganization(user);
        userExtra.setUuid(DEFAULT_UUID);
        userExtraRepository.saveAndFlush(userExtra);

        User user2 = createEntity(em);
        user2.setAuthorities(new HashSet<Authority>(){{add(new Authority(AuthoritiesConstants.FAMILY));}});
        user2.setLogin("testing-login");
        user2.setEmail("testing-mail@gmail.com");
        user2.setFirstName("should not bw shown");
        user2.setId(null);
        user2.setAuthorities(new HashSet<Authority>() {{
            add(new Authority(AuthoritiesConstants.USER));
            add(new Authority(AuthoritiesConstants.PACIENT));
        }});
        userRepository.saveAndFlush(user2);

        UserExtra userExtra2 = createUserExtraEntity(em);
        userExtra2.setUser(user2);
        userExtra2.setFamily(user2);
        userExtra2.setOrganization(user2);
        userExtra2.setUuid(UUID.randomUUID().toString());
        userExtraRepository.saveAndFlush(userExtra2);

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(2)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //first user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(4)))
            //second user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(userExtra2.getUuid())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(user2.getFirstName())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(user2.getLastName())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(user2.getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_AsOrganization_NobodyAsociatedToOrganization() throws Exception {
        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(0)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFirstAndLastNameWithUuid_OnlyOneItemReturned_AsOrganization() throws Exception {
        // Initialize the database
        userExtra = createUserExtraEntity(em);
        userExtra.setUser(user);
        userExtra.setFamily(user);
        userExtra.setOrganization(user);
        userExtra.setUuid(DEFAULT_UUID);
        userExtraRepository.saveAndFlush(userExtra);

        User user2 = createEntity(em);
        user2.setAuthorities(new HashSet<Authority>(){{add(new Authority(AuthoritiesConstants.FAMILY));}});
        user2.setLogin("testing-login");
        user2.setEmail("testing-mail@gmail.com");
        user2.setFirstName("should not bw shown");
        user2.setId(null);
        UserExtra userExtra2 = createUserExtraEntity(em);
//        userExtra2.setUser(user2);
//        userExtra2.setFamily(user2);
        userExtra2.setOrganization(user2);
        userExtra2.setUuid(UUID.randomUUID().toString());
        userRepository.save(user2);
        userExtraRepository.saveAndFlush(userExtra2);

        // Get all the users
        restUserMockMvc.perform(get("/api/users/getFirstAndLastNameWithUuid")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //first user
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(4)));
    }

    @Test
    @Transactional
    public void getAllAuthorities() throws Exception {
        restUserMockMvc.perform(get("/api/users/authorities")
            .accept(TestUtil.APPLICATION_JSON_UTF8)
            .contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").value(hasItems(AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN, AuthoritiesConstants.FAMILY, AuthoritiesConstants.ORGANIZIATION)));
    }

    @Test
    @Transactional
    public void testUserEquals() throws Exception {
        TestUtil.equalsVerifier(User.class);
        User user1 = new User();
        user1.setId(1L);
        User user2 = new User();
        user2.setId(user1.getId());
        assertThat(user1).isEqualTo(user2);
        user2.setId(2L);
        assertThat(user1).isNotEqualTo(user2);
        user1.setId(null);
        assertThat(user1).isNotEqualTo(user2);
    }

    @Test
    public void testUserDTOtoUser() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(DEFAULT_ID);
        userDTO.setLogin(DEFAULT_LOGIN);
        userDTO.setFirstName(DEFAULT_FIRSTNAME);
        userDTO.setLastName(DEFAULT_LASTNAME);
        userDTO.setEmail(DEFAULT_EMAIL);
        userDTO.setActivated(true);
        userDTO.setImageUrl(DEFAULT_IMAGEURL);
        userDTO.setLangKey(DEFAULT_LANGKEY);
        userDTO.setCreatedBy(DEFAULT_LOGIN);
        userDTO.setLastModifiedBy(DEFAULT_LOGIN);
        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        User user = userMapper.userDTOToUser(userDTO);
        assertThat(user.getId()).isEqualTo(DEFAULT_ID);
        assertThat(user.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(user.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(user.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(user.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(user.getActivated()).isEqualTo(true);
        assertThat(user.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
        assertThat(user.getLangKey()).isEqualTo(DEFAULT_LANGKEY);
        assertThat(user.getCreatedBy()).isNull();
        assertThat(user.getCreatedDate()).isNotNull();
        assertThat(user.getLastModifiedBy()).isNull();
        assertThat(user.getLastModifiedDate()).isNotNull();
        assertThat(user.getAuthorities()).extracting("name").containsExactly(AuthoritiesConstants.USER);
    }

    @Test
    public void testUserToUserDTO() {
        user.setId(DEFAULT_ID);
        user.setCreatedBy(DEFAULT_LOGIN);
        user.setCreatedDate(Instant.now());
        user.setLastModifiedBy(DEFAULT_LOGIN);
        user.setLastModifiedDate(Instant.now());
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.USER);
        authorities.add(authority);
        user.setAuthorities(authorities);

        UserDTO userDTO = userMapper.userToUserDTO(user);

        assertThat(userDTO.getId()).isEqualTo(DEFAULT_ID);
        assertThat(userDTO.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(userDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(userDTO.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(userDTO.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(userDTO.isActivated()).isEqualTo(true);
        assertThat(userDTO.getImageUrl()).isEqualTo(DEFAULT_IMAGEURL);
        assertThat(userDTO.getLangKey()).isEqualTo(DEFAULT_LANGKEY);
        assertThat(userDTO.getCreatedBy()).isEqualTo(DEFAULT_LOGIN);
        assertThat(userDTO.getCreatedDate()).isEqualTo(user.getCreatedDate());
        assertThat(userDTO.getLastModifiedBy()).isEqualTo(DEFAULT_LOGIN);
        assertThat(userDTO.getLastModifiedDate()).isEqualTo(user.getLastModifiedDate());
        assertThat(userDTO.getAuthorities()).containsExactly(AuthoritiesConstants.USER);
        assertThat(userDTO.toString()).isNotNull();
    }

    @Test
    public void testAuthorityEquals() {
        Authority authorityA = new Authority();
        assertThat(authorityA).isEqualTo(authorityA);
        assertThat(authorityA).isNotEqualTo(null);
        assertThat(authorityA).isNotEqualTo(new Object());
        assertThat(authorityA.hashCode()).isEqualTo(0);
        assertThat(authorityA.toString()).isNotNull();

        Authority authorityB = new Authority();
        assertThat(authorityA).isEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.ADMIN);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityA.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isEqualTo(authorityB);
        assertThat(authorityA.hashCode()).isEqualTo(authorityB.hashCode());
    }

}
