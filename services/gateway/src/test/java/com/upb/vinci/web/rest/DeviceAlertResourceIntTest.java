package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.*;
import com.upb.vinci.domain.enumeration.AlertType;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.repository.DeviceAlertRepository;
import com.upb.vinci.repository.DeviceRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.DeviceAlertQueryService;
import com.upb.vinci.service.DeviceAlertService;
import com.upb.vinci.service.dto.DeviceAlertDTO;
import com.upb.vinci.service.mapper.DeviceAlertMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the DeviceAlertResource REST controller.
 *
 * @see DeviceAlertResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class DeviceAlertResourceIntTest extends UsersSetupController {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_VALUES = "AAAAAAAAAA";
    private static final String UPDATED_VALUES = "BBBBBBBBBB";

    private static final AlertType DEFAULT_ALERT_TYPE = AlertType.SUCCESS;
    private static final AlertType UPDATED_ALERT_TYPE = AlertType.INFO;

    private static final Boolean DEFAULT_USER_READ = false;
    private static final Boolean UPDATED_USER_READ = true;

    private static final Boolean DEFAULT_FAMILY_READ = false;
    private static final Boolean UPDATED_FAMILY_READ = true;

    private static final Boolean DEFAULT_ORGANIZATION_READ = false;
    private static final Boolean UPDATED_ORGANIZATION_READ = true;

    //device constants
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_UUID = "AAAAAAAAAA";
    private static final String UPDATED_UUID = "BBBBBBBBBB";

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.WATCH;
    private static final DeviceType UPDATED_DEVICE_TYPE = DeviceType.SHOE;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Instant DEFAULT_START_TIMESTAMP = Instant.now();
    private static final Instant UPDATED_START_TIMESTAMP = Instant.parse("2019-11-04T10:30:31.00Z");

    @Autowired
    private UserExtraRepository userExtraRepository;

    @Autowired
    private DeviceAlertRepository deviceAlertRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceAlertMapper deviceAlertMapper;

    @Autowired
    private DeviceAlertService deviceAlertService;

    @Autowired
    private DeviceAlertQueryService deviceAlertQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDeviceAlertMockMvc;

    private DeviceAlert deviceAlert;

    private Device device;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeviceAlertResource deviceAlertResource = new DeviceAlertResource(deviceAlertService, deviceAlertQueryService);
        this.restDeviceAlertMockMvc = MockMvcBuilders.standaloneSetup(deviceAlertResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeviceAlert createEntity(Device device, EntityManager em) {
        return new DeviceAlert()
            .label(DEFAULT_LABEL)
            .values(DEFAULT_VALUES)
            .alertType(DEFAULT_ALERT_TYPE)
            .userRead(DEFAULT_USER_READ)
            .familyRead(DEFAULT_FAMILY_READ)
            .organizationRead(DEFAULT_ORGANIZATION_READ)
            .device(device);
    }

    public Device createDeviceEntity(UserExtra userExtra, EntityManager em) {
        return new Device()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .uuid(DEFAULT_UUID)
            .deviceType(DEFAULT_DEVICE_TYPE)
            .active(DEFAULT_ACTIVE)
            .userExtra(userExtra)
            .startTimestamp(DEFAULT_START_TIMESTAMP);
    }

    private void initTest() {
        this.initUsers();

        device = createDeviceEntity(userExtraPacient,em);
        deviceRepository.saveAndFlush(device);
        userExtraPacient.setDevices(new HashSet<Device>(){{add(device);}});

        deviceAlert = createEntity(device,em);
    }

    @Test
    @Transactional
    public void createDeviceAlert_NoUserInToken() throws Exception {
        createDeviceAlertRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlertWithExistingId() throws Exception {
        deviceAlert.setId(1L);
        createDeviceAlertRestCall_IsBadRequest();

    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_LabelIsRequired() throws Exception {
        // set the field null
        deviceAlert.setLabel(null);

        createDeviceAlertRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_AlertTypeIsRequired() throws Exception {
        // set the field null
        deviceAlert.setAlertType(null);

        createDeviceAlertRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_DeviceIdRequired() throws Exception {
        // set the field null
        deviceAlert.setDevice(null);

        createDeviceAlertRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_AsPacient() throws Exception {
        createDeviceAlertRestCall_IsCreated(deviceAlert);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_AsFamily() throws Exception {
        Device deviceOfFamily = createDeviceEntity(userExtraFamily, em);
        deviceRepository.saveAndFlush(deviceOfFamily);
        DeviceAlert deviceAlertOfFamily = createEntity(deviceOfFamily, em);

        createDeviceAlertRestCall_IsCreated(deviceAlertOfFamily);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_OfPacient_AsFamily() throws Exception {
        deviceRepository.saveAndFlush(device);
        associatePacientWithFamily(userExtraPacient, userFamily);

        createDeviceAlertRestCall_IsCreated(deviceAlert);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_OfPacient_AsFamily_NotAssociated() throws Exception {
        createDeviceAlertRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_AsOrganization() throws Exception {
        Device deviceOfOrganization = createDeviceEntity(userExtraOrganization, em);
        deviceRepository.saveAndFlush(deviceOfOrganization);
        DeviceAlert deviceAlertOfOrganization = createEntity(deviceOfOrganization, em);

        createDeviceAlertRestCall_IsCreated(deviceAlertOfOrganization);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_OfPacient_AsOrganization() throws Exception {
        userExtraRepository.save(userExtraOrganization);
        deviceRepository.saveAndFlush(device);
        associatePacientWithOrganization(userExtraPacient, userOrganization);

        createDeviceAlertRestCall_IsCreated(deviceAlert);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceAlert_OfPacient_AsOrganization_NotAssociated() throws Exception {
        createDeviceAlertRestCall_IsUnauthorized();
    }

    private void createDeviceAlertRestCall_IsCreated(DeviceAlert deviceAlert) throws Exception {
        // Create the DeviceAlert
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(deviceAlert);
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        em.clear();
        restDeviceAlertMockMvc.perform(post("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.values").value(DEFAULT_VALUES))
            .andExpect(jsonPath("$.alertType").value(DEFAULT_ALERT_TYPE.name()))
            .andExpect(jsonPath("$.userRead").value(DEFAULT_USER_READ))
            .andExpect(jsonPath("$.familyRead").value(DEFAULT_FAMILY_READ))
            .andExpect(jsonPath("$.organizationRead").value(DEFAULT_ORGANIZATION_READ));

        // Validate the DeviceAlert in the database
        validateDeviceAlertInDatabase(databaseSizeBeforeTest + 1,true);
    }

    private void createDeviceAlertRestCall_IsUnauthorized() throws Exception {
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(deviceAlert);
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        em.clear();
        // Create the DeviceAlert
        restDeviceAlertMockMvc.perform(post("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the DeviceAlert in the database
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeTest);
    }

    private void createDeviceAlertRestCall_IsBadRequest() throws Exception {
        // Create the DeviceAlert
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(deviceAlert);
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        restDeviceAlertMockMvc.perform(post("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DeviceAlert in the database
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateNonExistingDeviceAlert() throws Exception {
        // Create the DeviceAlert
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(deviceAlert);
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        restDeviceAlertMockMvc.perform(put("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DeviceAlert in the database
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void updateDeviceAlert_NoUserInToken() throws Exception {
        deviceAlertRepository.saveAndFlush(deviceAlert);
        em.clear();
        // Create the DeviceAlert
        updateDeviceAlertRestCall_IsUnautgorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_LabelIsRequired() throws Exception {
        deviceAlertRepository.saveAndFlush(deviceAlert);
        em.clear();
        deviceAlert.setLabel(null);

        updateDeviceAlertRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_AlertTypeIsRequired() throws Exception {
        deviceAlertRepository.saveAndFlush(deviceAlert);
        em.clear();
        deviceAlert.setAlertType(null);

        updateDeviceAlertRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_DeviceIdRequired() throws Exception {
        deviceAlertRepository.saveAndFlush(deviceAlert);
        em.clear();
        deviceAlert.setDevice(null);
        updateDeviceAlertRestCall_IsBadRequest();

    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_AsPacient() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        updateDeviceAlertRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_AsFamily() throws Exception {
        // Initialize the database
        Device familyDevice = createDeviceEntity(userExtraFamily,em);
        deviceRepository.saveAndFlush(familyDevice);
        deviceAlert.setDevice(familyDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);

        updateDeviceAlertRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithFamily(userExtraPacient,userFamily);

        updateDeviceAlertRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
//        associatePacientWithFamily(userExtraPacient,userFamily);

        updateDeviceAlertRestCall_IsUnautgorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_AsOrganization() throws Exception {
        // Initialize the database
        Device organizationDevice = createDeviceEntity(userExtraOrganization,em);
        deviceRepository.saveAndFlush(organizationDevice);
        deviceAlert.setDevice(organizationDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);

        updateDeviceAlertRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        updateDeviceAlertRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDeviceAlert_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        updateDeviceAlertRestCall_IsUnautgorized();
    }

    private void updateDeviceAlertRestCall_IsOk() throws Exception {
        // Update the deviceAlert
        DeviceAlert updatedDeviceAlert = deviceAlertRepository.findById(deviceAlert.getId()).get();
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        // Disconnect from session so that the updates on updatedDeviceAlert are not directly saved in db
        em.detach(updatedDeviceAlert);
        updatedDeviceAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(updatedDeviceAlert);

        em.clear();
        restDeviceAlertMockMvc.perform(put("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isOk());

        // Validate the DeviceAlert in the database
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeTest);
        DeviceAlert testDeviceAlert = deviceAlertList.get(deviceAlertList.size() - 1);
        assertThat(testDeviceAlert.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testDeviceAlert.getValues()).isEqualTo(UPDATED_VALUES);
        assertThat(testDeviceAlert.getAlertType()).isEqualTo(UPDATED_ALERT_TYPE);
        assertThat(testDeviceAlert.isUserRead()).isEqualTo(UPDATED_USER_READ);
        assertThat(testDeviceAlert.isFamilyRead()).isEqualTo(UPDATED_FAMILY_READ);
        assertThat(testDeviceAlert.isOrganizationRead()).isEqualTo(UPDATED_ORGANIZATION_READ);
    }

    private void updateDeviceAlertRestCall_IsBadRequest() throws Exception {
        // Create the DeviceAlert
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(deviceAlert);
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        restDeviceAlertMockMvc.perform(put("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isBadRequest());

        validateDeviceAlertInDatabase(databaseSizeBeforeTest,true);
    }

    private void validateDeviceAlertInDatabase(int databaseSizeBeforeTest, boolean isDefault) {
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeTest);
        DeviceAlert testDeviceAlert = deviceAlertList.get(deviceAlertList.size() - 1);
        assertThat(testDeviceAlert.getLabel()).isEqualTo(isDefault ? DEFAULT_LABEL:UPDATED_LABEL);
        assertThat(testDeviceAlert.getValues()).isEqualTo(isDefault ? DEFAULT_VALUES:UPDATED_VALUES);
        assertThat(testDeviceAlert.getAlertType()).isEqualTo(isDefault ? DEFAULT_ALERT_TYPE:UPDATED_ALERT_TYPE);
        assertThat(testDeviceAlert.isUserRead()).isEqualTo(isDefault ? DEFAULT_USER_READ:UPDATED_USER_READ);
        assertThat(testDeviceAlert.isFamilyRead()).isEqualTo(isDefault ? DEFAULT_FAMILY_READ:UPDATED_FAMILY_READ);
        assertThat(testDeviceAlert.isOrganizationRead()).isEqualTo(isDefault ? DEFAULT_ORGANIZATION_READ:UPDATED_ORGANIZATION_READ);
    }

    private void updateDeviceAlertRestCall_IsUnautgorized() throws Exception {
        // Update the deviceAlert
        DeviceAlert updatedDeviceAlert = deviceAlertRepository.findById(deviceAlert.getId()).get();
        int databaseSizeBeforeTest = deviceAlertRepository.findAll().size();
        // Disconnect from session so that the updates on updatedDeviceAlert are not directly saved in db
        em.detach(updatedDeviceAlert);
        updatedDeviceAlert
            .label(UPDATED_LABEL)
            .values(UPDATED_VALUES)
            .alertType(UPDATED_ALERT_TYPE)
            .userRead(UPDATED_USER_READ)
            .familyRead(UPDATED_FAMILY_READ)
            .organizationRead(UPDATED_ORGANIZATION_READ);
        DeviceAlertDTO deviceAlertDTO = deviceAlertMapper.toDto(updatedDeviceAlert);

        em.clear();
        restDeviceAlertMockMvc.perform(put("/api/device-alerts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceAlertDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the DeviceAlert in the database
        validateDeviceAlertInDatabase(databaseSizeBeforeTest,true);
    }

    @Test
    @Transactional
    public void getAllDeviceAlerts_NoUserInToken() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        // Get all the deviceAlertList
        restDeviceAlertMockMvc.perform(get("/api/device-alerts?sort=id,desc"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_AsPacient() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        getAllDeviceAlertsRestCall_IsOk();

    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_AsFamily() throws Exception {
        // Initialize the database
        Device familyDevice = createDeviceEntity(userExtraFamily, em);
        deviceRepository.saveAndFlush(familyDevice);
        deviceAlert.setDevice(familyDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getAllDeviceAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithFamily(userExtraPacient,userFamily);

        getAllDeviceAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_OfPacient_AsFamily_NotAssociatedUser() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
//        associatePacientWithFamily(userExtraPacient,userFamily);

        getAllDeviceAlertsRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_AsOrganization() throws Exception {
        // Initialize the database
        Device organizationDevice = createDeviceEntity(userExtraOrganization, em);
        deviceRepository.saveAndFlush(organizationDevice);
        deviceAlert.setDevice(organizationDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getAllDeviceAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        getAllDeviceAlertsRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_OfPacient_AsOrganization_NotAssociatedUser() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        getAllDeviceAlertsRestCall_EmptyList();
    }

    private void getAllDeviceAlertsRestCall_EmptyList() throws Exception {
        // Get all the deviceAlertList
        restDeviceAlertMockMvc.perform(get("/api/device-alerts?sort=id,desc"))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    private void getAllDeviceAlertsRestCall_IsOk() throws Exception {
        // Get all the deviceAlertList
        restDeviceAlertMockMvc.perform(get("/api/device-alerts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deviceAlert.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].values").value(hasItem(DEFAULT_VALUES)))
            .andExpect(jsonPath("$.[*].alertType").value(hasItem(DEFAULT_ALERT_TYPE.name())))
            .andExpect(jsonPath("$.[*].userRead").value(hasItem(DEFAULT_USER_READ)))
            .andExpect(jsonPath("$.[*].familyRead").value(hasItem(DEFAULT_FAMILY_READ)))
            .andExpect(jsonPath("$.[*].organizationRead").value(hasItem(DEFAULT_ORGANIZATION_READ)));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        Device validDevice = device;
        UserExtra differentUser = userExtraOrganization;
        Device invalidDevice = createDeviceEntity(differentUser,em);
        deviceRepository.saveAndFlush(invalidDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDevice.getId().intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdEqualsFilter,invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDevice.getId().intValue() + "," + invalidDevice.getId().intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdInFilter,invalidDeviceIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        Device validDevice = createDeviceEntity(userExtraFamily,em);
        deviceRepository.saveAndFlush(validDevice);
        UserExtra differentUser = userExtraOrganization;
        Device invalidDevice = createDeviceEntity(differentUser,em);
        deviceRepository.saveAndFlush(invalidDevice);
        deviceAlert.setDevice(validDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithFamily(userExtraPacient,userFamily);
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDevice.getId().intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdEqualsFilter,invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDevice.getId().intValue() + "," + invalidDevice.getId().intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdInFilter,invalidDeviceIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDeviceAlerts_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        Device validDevice = createDeviceEntity(userExtraOrganization,em);
        deviceRepository.saveAndFlush(validDevice);
        UserExtra differentUser = userExtraFamily;
        Device invalidDevice = createDeviceEntity(differentUser,em);
        deviceRepository.saveAndFlush(invalidDevice);
        deviceAlert.setDevice(validDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithFamily(userExtraPacient,userOrganization);
        //setup
        String validDeviceIdEqualsFilter = "deviceId.equals=" + validDevice.getId().intValue();
        String invalidDeviceIdEqualsFilter = "deviceId.equals=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdEqualsFilter,invalidDeviceIdEqualsFilter);
        //setup
        String validDeviceIdInFilter = "deviceId.in=" + validDevice.getId().intValue() + "," + invalidDevice.getId().intValue();
        String invalidDeviceIdInFilter = "deviceId.in=" + invalidDevice.getId().intValue();
        testingFilters(validDeviceIdInFilter,invalidDeviceIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultDeviceAlertShouldBeFound(validFilter,deviceAlert);
        defaultDeviceAlertShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"label",DEFAULT_LABEL,UPDATED_LABEL);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"values",DEFAULT_VALUES,UPDATED_VALUES);
        testValidAndInvalidFilters_ForEnumTypeValues(validFilter,invalidFilter,"alertType",DEFAULT_ALERT_TYPE,UPDATED_ALERT_TYPE);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"userRead",DEFAULT_USER_READ,UPDATED_USER_READ);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"familyRead",DEFAULT_FAMILY_READ,UPDATED_FAMILY_READ);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"organizationRead",DEFAULT_ORGANIZATION_READ,UPDATED_ORGANIZATION_READ);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",deviceAlert.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultDeviceAlertShouldBeFound(filter+validValue,deviceAlert);
        defaultDeviceAlertShouldNotBeFound(filter+invalidValue);
        defaultDeviceAlertShouldBeFound(validFilter+"&"+filter+validValue,deviceAlert);
        defaultDeviceAlertShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultDeviceAlertShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultDeviceAlertShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDeviceAlertShouldBeFound(String filter,DeviceAlert _deviceAlert) throws Exception {
        restDeviceAlertMockMvc.perform(get("/api/device-alerts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(_deviceAlert.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].values").value(hasItem(DEFAULT_VALUES)))
            .andExpect(jsonPath("$.[*].alertType").value(hasItem(DEFAULT_ALERT_TYPE.name())))
            .andExpect(jsonPath("$.[*].userRead").value(hasItem(DEFAULT_USER_READ)))
            .andExpect(jsonPath("$.[*].familyRead").value(hasItem(DEFAULT_FAMILY_READ)))
            .andExpect(jsonPath("$.[*].organizationRead").value(hasItem(DEFAULT_ORGANIZATION_READ)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDeviceAlertShouldNotBeFound(String filter) throws Exception {
        restDeviceAlertMockMvc.perform(get("/api/device-alerts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

    }

    @Test
    @Transactional
    public void getDeviceAlertById_NoTokenException() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getDeviceAlertByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ADMIN_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsAdmin() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getDeviceAlertByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsPacient() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getDeviceAlertByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsFamily() throws Exception {
        // Initialize the database
        Device familyDevice = createDeviceEntity(userExtraFamily, em);
        deviceRepository.saveAndFlush(familyDevice);
        deviceAlert.setDevice(familyDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);

        getDeviceAlertByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
        associatePacientWithFamily(userExtraPacient,userFamily);

        getDeviceAlertByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);
//        associatePacientWithFamily(userExtraPacient,userFamily);

        getDeviceAlertByIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsOrganization() throws Exception {
        // Initialize the database
        Device organizationDevice = createDeviceEntity(userExtraOrganization, em);
        deviceRepository.saveAndFlush(organizationDevice);
        deviceAlert.setDevice(organizationDevice);
        deviceAlertRepository.saveAndFlush(deviceAlert);;

        getDeviceAlertByIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsPacient_AsOrganization() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);;
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        getDeviceAlertByIdRestCall_IsOk();

    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceAlertById_AsPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);;
//        associatePacientWithOrganization(userExtraPacient,userOrganization);

        getDeviceAlertByIdRestCall_IsUnauthorized();
    }

    private void getDeviceAlertByIdRestCall_IsOk() throws Exception {
        // Get the deviceAlert
        restDeviceAlertMockMvc.perform(get("/api/device-alerts/{id}", deviceAlert.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(deviceAlert.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.values").value(DEFAULT_VALUES))
            .andExpect(jsonPath("$.alertType").value(DEFAULT_ALERT_TYPE.name()))
            .andExpect(jsonPath("$.userRead").value(DEFAULT_USER_READ))
            .andExpect(jsonPath("$.familyRead").value(DEFAULT_FAMILY_READ))
            .andExpect(jsonPath("$.organizationRead").value(DEFAULT_ORGANIZATION_READ));
    }

    private void getDeviceAlertByIdRestCall_IsUnauthorized() throws Exception {
        // Get the deviceAlert
        restDeviceAlertMockMvc.perform(get("/api/device-alerts/{id}", deviceAlert.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    public void deleteDeviceAlert() throws Exception {
        // Initialize the database
        deviceAlertRepository.saveAndFlush(deviceAlert);

        int databaseSizeBeforeDelete = deviceAlertRepository.findAll().size();

        // Delete the deviceAlert
        restDeviceAlertMockMvc.perform(delete("/api/device-alerts/{id}", deviceAlert.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DeviceAlert> deviceAlertList = deviceAlertRepository.findAll();
        assertThat(deviceAlertList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeviceAlert.class);
        DeviceAlert deviceAlert1 = new DeviceAlert();
        deviceAlert1.setId(1L);
        DeviceAlert deviceAlert2 = new DeviceAlert();
        deviceAlert2.setId(deviceAlert1.getId());
        assertThat(deviceAlert1).isEqualTo(deviceAlert2);
        deviceAlert2.setId(2L);
        assertThat(deviceAlert1).isNotEqualTo(deviceAlert2);
        deviceAlert1.setId(null);
        assertThat(deviceAlert1).isNotEqualTo(deviceAlert2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeviceAlertDTO.class);
        DeviceAlertDTO deviceAlertDTO1 = new DeviceAlertDTO();
        deviceAlertDTO1.setId(1L);
        DeviceAlertDTO deviceAlertDTO2 = new DeviceAlertDTO();
        assertThat(deviceAlertDTO1).isNotEqualTo(deviceAlertDTO2);
        deviceAlertDTO2.setId(deviceAlertDTO1.getId());
        assertThat(deviceAlertDTO1).isEqualTo(deviceAlertDTO2);
        deviceAlertDTO2.setId(2L);
        assertThat(deviceAlertDTO1).isNotEqualTo(deviceAlertDTO2);
        deviceAlertDTO1.setId(null);
        assertThat(deviceAlertDTO1).isNotEqualTo(deviceAlertDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(deviceAlertMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(deviceAlertMapper.fromId(null)).isNull();
    }
}
