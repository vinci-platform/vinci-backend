package com.upb.vinci.web.rest;

import com.upb.vinci.GatewayApp;
import com.upb.vinci.domain.Device;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.repository.DeviceRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.DeviceQueryService;
import com.upb.vinci.service.DeviceService;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.mapper.DeviceMapper;
import com.upb.vinci.web.rest.errors.ExceptionTranslator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.upb.vinci.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DeviceResource REST controller.
 *
 * @see DeviceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
@Slf4j
public class DeviceResourceIntTest extends UsersSetupController{

    private static final String DEFAULT_NAME = "DEFAULT_NAME";
    private static final String UPDATED_NAME = "UPDATED_NAME";

    private static final String DEFAULT_DESCRIPTION = "DEFAULT_DESCRIPTION";
    private static final String UPDATED_DESCRIPTION = "UPDATED_DESCRIPTION";

    private static final String DEFAULT_UUID = "DEFAULT_UUID";
    private static final String UPDATED_UUID = "UPDATED_UUID";
    private static final String SURVEY_DEVICE_UUID = "SURVEY_DEVICE_UUID";
    private static final String WATCH_DEVICE_UUID = "WATCH_DEVICE_UUID";
    private static final String SHOE_DEVICE_UUID = "SHOE_DEVICE_UUID";
    private static final String FITBIT_DEVICE_UUID = "FITBIT_DEVICE_UUID";

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.WATCH;
    private static final DeviceType UPDATED_DEVICE_TYPE = DeviceType.SHOE;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Instant DEFAULT_START_TIMESTAMP = Instant.now();
    private static final Instant UPDATED_START_TIMESTAMP = Instant.parse("2019-11-04T10:30:31.00Z");

    private static final String VALID_UUID = "VALID_UUID";

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceQueryService deviceQueryService;

    @Autowired
    private UserExtraRepository userExtraRepository;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDeviceMockMvc;

    private Device device;
    private Device fitbitDevice;
    private Device watchDevice;
    private Device shoeDevice;
    private Device surveyDevice;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeviceResource deviceResource = new DeviceResource(deviceService, deviceQueryService);
        this.restDeviceMockMvc = MockMvcBuilders.standaloneSetup(deviceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
        initTest();
    }

    public static Device createEntity(EntityManager em, UserExtra userExtra, DeviceType deviceType, String uuid) {
        Device device = createEntity(em);
        device.setUserExtra(userExtra);
        device.setDeviceType(deviceType);
        device.setUuid(uuid);
        return device;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Device createEntity(EntityManager em) {
        return new Device()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .uuid(DEFAULT_UUID)
            .deviceType(DEFAULT_DEVICE_TYPE)
            .active(DEFAULT_ACTIVE)
            .userExtra(null)
            .startTimestamp(DEFAULT_START_TIMESTAMP);
    }

    private void initTest() {
        this.initUsers();

        setupDevicesForUserExtra(userExtraPacient);
    }

    /**
     * Does not persist any device or user extra. UserExtra is added in every device and devices are added to userExtra devices set
     * @param userExtra
     */
    private void setupDevicesForUserExtra(UserExtra userExtra) {
        device = createEntity(em);
        device.setUserExtra(userExtra);

        surveyDevice = createEntity(em, userExtra,DeviceType.SURVEY,SURVEY_DEVICE_UUID);
        surveyDevice.setUserExtra(userExtra);

        watchDevice = createEntity(em, userExtra,DeviceType.WATCH,WATCH_DEVICE_UUID);
        watchDevice.setUserExtra(userExtra);

        shoeDevice = createEntity(em, userExtra,DeviceType.SHOE,SHOE_DEVICE_UUID);
        shoeDevice.setUserExtra(userExtra);

        fitbitDevice = createEntity(em, userExtra,DeviceType.FITBIT_WATCH,FITBIT_DEVICE_UUID);
        fitbitDevice.setUserExtra(userExtra);

        userExtra.setDevices(new HashSet<Device>(){{add(device);add(surveyDevice);add(watchDevice);add(shoeDevice);add(fitbitDevice);}});
    }

    private void setUpdateFields(UserExtra userExtra, Device updatedDevice) {
        updatedDevice
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .uuid(UPDATED_UUID)
            .deviceType(UPDATED_DEVICE_TYPE)
            .active(UPDATED_ACTIVE)
            .startTimestamp(UPDATED_START_TIMESTAMP)
            .userExtra(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_WithExistingId() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        device.setId(1L);

        createDeviceRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_NameIsNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // set the field null
        device.setName(null);

        createDeviceRestCall_IsBadRequest();
    }
    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_UuidIsNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // set the field null
        device.setUuid(null);

        createDeviceRestCall_IsBadRequest();
    }
    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_DeviceTypeIsNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // set the field null
        device.setDeviceType(null);

        createDeviceRestCall_IsBadRequest();
    }
    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_ActiveIsNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // set the field null
        device.setActive(null);

        createDeviceRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDevice_BadRequest_UserExtraIdIsNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // set the field null
        device.setUserExtra(null);

        createDeviceRestCall_IsBadRequest();
    }

    @Test
    @Transactional
    public void createDevice_BadRequest_NoUserForToken() throws Exception {
        createDeviceRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsPatient() throws Exception {
        createDeviceRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsPatient_BadUserExtraId() throws Exception {
        UserExtra badUserExtra = userExtraFamily;
        setupDevicesForUserExtra(userExtraPacient);
        device.setUserExtra(badUserExtra);

        createDeviceRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsFamily() throws Exception {
        setupDevicesForUserExtra(userExtraFamily);

        createDeviceRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsFamily_BadUserExtraId() throws Exception {
        UserExtra badUserExtra = userExtraOrganization;
        setupDevicesForUserExtra(userExtraFamily);
        device.setUserExtra(badUserExtra);

        createDeviceRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_ForPacient_AsFamily() throws Exception {
        //Connect pacient user to current family user
        associatePacientWithFamily(userExtraPacient,userFamily);

        createDeviceRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_ForPacient_AsFamily_NotAssociated() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);

        createDeviceRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsOrganization() throws Exception {
        setupDevicesForUserExtra(userExtraOrganization);

        createDeviceRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_AsOrganization_BadUserExtraId() throws Exception {
        UserExtra badUserExtra = userExtraFamily;
        setupDevicesForUserExtra(userExtraOrganization);
        device.setUserExtra(badUserExtra);

        createDeviceRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_ForPacient_AsOrganization() throws Exception {
        //Connect pacient user to current organization user
        associatePacientWithOrganization(userExtraPacient,userOrganization);

        createDeviceRestCall_IsCreated();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void createDeviceDevice_ForPacient_AsOrganization_NotAssociated() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);

        createDeviceRestCall_IsUnauthorized();
    }

    private void createDeviceRestCall_IsCreated() throws Exception {
        // Create the Device
        DeviceDTO deviceDTO = deviceMapper.toDto(device);
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        restDeviceMockMvc.perform(post("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isCreated());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest + 1);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDevice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDevice.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testDevice.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testDevice.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    private void createDeviceRestCall_IsUnauthorized() throws Exception {
        // Create the Device
        DeviceDTO deviceDTO = deviceMapper.toDto(device);
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        restDeviceMockMvc.perform(post("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isUnauthorized());

        // Validate database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
    }

    private void createDeviceRestCall_IsBadRequest() throws Exception {
        // Create the Device
        DeviceDTO deviceDTO = deviceMapper.toDto(device);
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        restDeviceMockMvc.perform(post("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest());

        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_IdNull() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);
        // Create the Device
        DeviceDTO deviceDTO = deviceMapper.toDto(device);
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        restDeviceMockMvc.perform(put("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest());

        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_UserExtraIdIsNull() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = device.getUserExtra();

        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(device);

        setUpdateFields(userExtra, device);
        device.setUserExtra(null);

        updateDeviceRestCall_IsBadRequest(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_NameIsNull() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = device.getUserExtra();

        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(device);

        setUpdateFields(userExtra, device);
        device.setName(null);

        updateDeviceRestCall_IsBadRequest(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_UuidIsNull() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = device.getUserExtra();

        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(device);

        setUpdateFields(userExtra, device);
        device.setUuid(null);

        updateDeviceRestCall_IsBadRequest(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_DeviceTypeIsNull() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = device.getUserExtra();

        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(device);

        setUpdateFields(userExtra, device);
        device.setDeviceType(null);

        updateDeviceRestCall_IsBadRequest(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_BadRequest_ActiveIsNull() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = device.getUserExtra();

        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(device);

        setUpdateFields(userExtra, device);
        device.setActive(null);

        updateDeviceRestCall_IsBadRequest(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsPacient() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = userExtraPacient;

        updateDeviceRestCall_IsOk(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsPacient_BadUserExtraId() throws Exception {
        // Initialize the database
        Device someoneElsesDevice = createEntity(em);
        UserExtra someOtherUserExtra = userExtraFamily;
        UserExtra userExtra = someOtherUserExtra;
        someoneElsesDevice.setUserExtra(someOtherUserExtra);
        someoneElsesDevice = deviceRepository.saveAndFlush(someoneElsesDevice);

        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        // Update the device
        Device updatedDevice = deviceRepository.findById(someoneElsesDevice.getId()).get();
        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(updatedDevice);
        setUpdateFields(userExtra, updatedDevice);
        DeviceDTO deviceDTO = deviceMapper.toDto(updatedDevice);

        restDeviceMockMvc.perform(put("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isUnauthorized());
        // Validate the Device in the database has not changed
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDevice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDevice.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testDevice.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testDevice.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testDevice.getUserExtra()).isEqualTo(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = userExtraFamily;

        updateDeviceRestCall_IsOk(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsFamily_BadUserExtraId() throws Exception {
        UserExtra invalidUserExtra = userExtraFamily;
        UserExtra validUser = userExtraOrganization;
        setupDevicesForUserExtra(validUser);
        deviceRepository.saveAndFlush(device);

        updateDeviceRestCall_IsUnauthorized(invalidUserExtra, validUser);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_ForPacient_AsFamily() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        associatePacientWithFamily(userExtraPacient,userFamily);
        UserExtra userExtra = userExtraPacient;

        updateDeviceRestCall_IsOk(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_ForPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = userExtraPacient;

        updateDeviceRestCall_IsUnauthorized(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = userExtraOrganization;

        updateDeviceRestCall_IsOk(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_AsOrganization_BadUserExtraId() throws Exception {
        // Initialize the database
        UserExtra invalidUserExtra = userExtraOrganization;
        UserExtra validUser = userExtraFamily;
        setupDevicesForUserExtra(validUser);
        deviceRepository.saveAndFlush(device);

        updateDeviceRestCall_IsUnauthorized(invalidUserExtra, validUser);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_ForPacient_AsOrganization() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        UserExtra userExtra = userExtraPacient;

        updateDeviceRestCall_IsOk(userExtra);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void updateDevice_ForPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        UserExtra userExtra = userExtraPacient;

        updateDeviceRestCall_IsUnauthorized(userExtra);
    }

    private void updateDeviceRestCall_IsUnauthorized(UserExtra userExtra) throws Exception {
        updateDeviceRestCall_IsUnauthorized(userExtra, userExtra);
    }

    private void updateDeviceRestCall_IsUnauthorized(UserExtra invalidUserExtra, UserExtra validUser) throws Exception {
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        // Update the device
        Device updatedDevice = deviceRepository.findById(device.getId()).get();
        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(updatedDevice);
        setUpdateFields(invalidUserExtra, updatedDevice);
        DeviceDTO deviceDTO = deviceMapper.toDto(updatedDevice);
        restDeviceMockMvc.perform(put("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isUnauthorized());
        // Validate the Device in the database has not changed
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDevice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDevice.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testDevice.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testDevice.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testDevice.getUserExtra()).isEqualTo(validUser);
    }

    private void updateDeviceRestCall_IsOk(UserExtra userExtra) throws Exception {
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        // Update the device
        Device updatedDevice = deviceRepository.findById(device.getId()).get();
        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(updatedDevice);
        setUpdateFields(userExtra, updatedDevice);
        DeviceDTO deviceDTO = deviceMapper.toDto(updatedDevice);

        restDeviceMockMvc.perform(put("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isOk());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDevice.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDevice.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testDevice.getDeviceType()).isEqualTo(UPDATED_DEVICE_TYPE);
        assertThat(testDevice.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testDevice.getUserExtra()).isEqualTo(userExtra);
    }

    private void updateDeviceRestCall_IsBadRequest(UserExtra userExtra) throws Exception {
        DeviceDTO deviceDTO = deviceMapper.toDto(device);
        int databaseSizeBeforeTest = deviceRepository.findAll().size();
        restDeviceMockMvc.perform(put("/api/devices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest());

        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeTest);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDevice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDevice.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testDevice.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testDevice.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testDevice.getUserExtra()).isEqualTo(userExtra);
    }

    @Test
    @Transactional
    public void getAllDevices_NoUserInToken() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        // Get the device
        restDeviceMockMvc.perform(get("/api/devices"))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsPacient() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsPacient_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraFamily;
        setupDevicesForUserExtra(differentUserExtra);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsFamily_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraOrganization;
        setupDevicesForUserExtra(differentUserExtra);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_OfPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithFamily(userExtraPacient,userFamily);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_AsOrganization_DifferentUserExtraId() throws Exception {
        // Initialize the database
        UserExtra differentUserExtra = userExtraFamily;
        setupDevicesForUserExtra(differentUserExtra);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_EmptyList();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_OfPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
//        associatePacientWithOrganization(userExtraPacient,userOrganization);
        deviceRepository.saveAndFlush(device);

        getAllDevicesRestCall_EmptyList();
    }

    private void getAllDevicesRestCall_IsOk() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(device.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(DEFAULT_DEVICE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE)))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(device.getUserExtra().getId().intValue())));
    }

    private void getAllDevicesRestCall_EmptyList() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_FiltersWithEquals_AsPacient() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraPacient;
        UserExtra differentUserExtra = userExtraOrganization;
        deviceRepository.saveAndFlush(device);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_FiltersWithEquals_AsFamily() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraFamily;
        UserExtra differentUserExtra = userExtraOrganization;
        device.setUserExtra(validUserExtra);
        deviceRepository.saveAndFlush(device);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getAllDevices_FiltersWithEquals_AsOrganization() throws Exception {
        // Initialize the database
        UserExtra validUserExtra = userExtraOrganization;
        UserExtra differentUserExtra = userExtraFamily;
        device.setUserExtra(validUserExtra);
        deviceRepository.saveAndFlush(device);
        //setup
        String validUserExtraIdEqualsFilter = "userExtraId.equals=" + validUserExtra.getId().intValue();
        String invalidUserExtraIdEqualsFilter = "userExtraId.equals=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdEqualsFilter,invalidUserExtraIdEqualsFilter);
        //setup
        String validUserExtraIdInFilter = "userExtraId.in=" + validUserExtra.getId().intValue() + "," + differentUserExtra.getId().intValue();
        String invalidUserExtraIdInFilter = "userExtraId.in=" + differentUserExtra.getId().intValue();
        testingFilters(validUserExtraIdInFilter,invalidUserExtraIdInFilter);
    }

    private void testingFilters(String validFilter, String invalidFilter) throws Exception {
        defaultDeviceShouldBeFound(validFilter,device);
        defaultDeviceShouldNotBeFound(invalidFilter);

        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"description",DEFAULT_DESCRIPTION,UPDATED_DESCRIPTION);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"name",DEFAULT_NAME,UPDATED_NAME);
        testValidAndInvalidFilters_ForStringValues(validFilter,invalidFilter,"uuid",DEFAULT_UUID,UPDATED_UUID);
        testValidAndInvalidFilters_ForEnumTypeValues(validFilter,invalidFilter,"deviceType",DEFAULT_DEVICE_TYPE,UPDATED_DEVICE_TYPE);
        testValidAndInvalidFilters_ForBooleanValues(validFilter,invalidFilter,"active",DEFAULT_ACTIVE,UPDATED_ACTIVE);
        testValidAndInvalidFilters_ForLongValues(validFilter,invalidFilter,"id",device.getId(),1L);
    }

    private void testValidAndInvalidFilters(String validFilter, String invalidFilter, String filter, String validValue, String invalidValue) throws Exception {
        defaultDeviceShouldBeFound(filter+validValue,device);
        defaultDeviceShouldNotBeFound(filter+invalidValue);
        defaultDeviceShouldBeFound(validFilter+"&"+filter+validValue,device);
        defaultDeviceShouldNotBeFound(invalidFilter+"&"+filter+validValue);
        defaultDeviceShouldNotBeFound(validFilter+"&"+filter+invalidValue);
        defaultDeviceShouldNotBeFound(invalidFilter+"&"+filter+invalidValue);
    }

    private void testValidAndInvalidFilters_ForLongValues(String validFilter, String invalidFilter, String variableName, Long validValue, Long invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.intValue()),String.valueOf(invalidValue.intValue()));
    }

    private void testValidAndInvalidFilters_ForDoubleValues(String validFilter, String invalidFilter, String variableName, Double validValue, Double invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",String.valueOf(validValue.doubleValue()),String.valueOf(invalidValue.doubleValue()));
    }

    private void testValidAndInvalidFilters_ForBooleanValues(String validFilter, String invalidFilter, String variableName, Boolean validValue, Boolean invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.toString(),invalidValue.toString());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.toString(),invalidValue.toString());
    }

    private void testValidAndInvalidFilters_ForEnumTypeValues(String validFilter, String invalidFilter, String variableName, Enum validValue, Enum invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue.name(),invalidValue.name());
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue.name(),invalidValue.name());
    }

    private void testValidAndInvalidFilters_ForStringValues(String validFilter, String invalidFilter, String variableName, String validValue, String invalidValue) throws Exception {
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".equals=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".in=",validValue,invalidValue);
        testValidAndInvalidFilters(validFilter,invalidFilter,variableName+".contains=",validValue,invalidValue);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDeviceShouldBeFound(String filter, Device device) throws Exception {
        restDeviceMockMvc.perform(get("/api/devices?sort=id,desc&" + filter))
            .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(device.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(DEFAULT_DEVICE_TYPE.name())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE)))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(device.getUserExtra().getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDeviceShouldNotBeFound(String filter) throws Exception {
        restDeviceMockMvc.perform(get("/api/devices?sort=id,desc&" + filter))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_IncorrectDeviceTypeException() throws Exception {
        final String BAD_TYPE = "BAD_TYPE";

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeviceMockMvc.perform(get("/api/devices/type")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .param("type",BAD_TYPE))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsPacient() throws Exception {
        setupDevicesForUserExtra(userExtraPacient);

        saveDevices();

        getDeviceByTypeTestingAllTypes(false);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsPacient_NoDevicesForUser() throws Exception {
        UserExtra differentUser = userExtraOrganization;
        setupDevicesForUserExtra(differentUser);

        saveDevices();

        getDeviceByTypeTestingAllTypes(true);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsFamily() throws Exception {
        setupDevicesForUserExtra(userExtraFamily);

        saveDevices();

        getDeviceByTypeTestingAllTypes(false);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsFamily_NoDevicesForUser() throws Exception {
        UserExtra differentUser = userExtraOrganization;
        setupDevicesForUserExtra(differentUser);

        saveDevices();

        getDeviceByTypeTestingAllTypes(true);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_FromPacient_AsFamily() throws Exception {
        //associate pacient
        associatePacientWithFamily(userExtraPacient,userFamily);

        setupDevicesForUserExtra(userExtraPacient);

        saveDevices();

        getDeviceByTypeTestingAllTypes(false);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsOrganization() throws Exception {
        setupDevicesForUserExtra(userExtraOrganization);

        saveDevices();

        getDeviceByTypeTestingAllTypes(false);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_AsOrganization_NoDevicesForUser() throws Exception {
        UserExtra differentUser = userExtraFamily;
        setupDevicesForUserExtra(differentUser);

        saveDevices();

        getDeviceByTypeTestingAllTypes(true);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceByType_FromPacient_AsOrganization() throws Exception {
        //associate pacient
        userExtraPacient.setOrganization(userOrganization);
        userExtraRepository.saveAndFlush(userExtraPacient);

        setupDevicesForUserExtra(userExtraPacient);

        saveDevices();

        getDeviceByTypeTestingAllTypes(false);
    }

    private void getDeviceByTypeTestingAllTypes(boolean shoulReturnListBeEmpty) throws Exception {
        getDeviceByTypeRequestForDevice(surveyDevice, shoulReturnListBeEmpty);
        getDeviceByTypeRequestForDevice(fitbitDevice, shoulReturnListBeEmpty);
        getDeviceByTypeRequestForDevice(watchDevice, shoulReturnListBeEmpty);
        getDeviceByTypeRequestForDevice(shoeDevice, shoulReturnListBeEmpty);
    }

    private void saveDevices() {
        deviceRepository.saveAndFlush(surveyDevice);
        deviceRepository.saveAndFlush(fitbitDevice);
        deviceRepository.saveAndFlush(watchDevice);
        deviceRepository.saveAndFlush(shoeDevice);
    }

    private void getDeviceByTypeRequestForDevice(Device device,Boolean shoulReturnListBeEmpty) throws Exception {
        if (shoulReturnListBeEmpty){
            restDeviceMockMvc.perform(get("/api/devices/type")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .param("deviceType",device.getDeviceType().name()))
                .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").isEmpty());
        } else {
            restDeviceMockMvc.perform(get("/api/devices/type")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .param("deviceType",device.getDeviceType().name()))
                .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.*",hasSize(1)))
                .andExpect(jsonPath("$.[*].id").value(hasItem(device.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(device.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(device.getDescription())))
                .andExpect(jsonPath("$.[*].uuid").value(hasItem(device.getUuid())))
                .andExpect(jsonPath("$.[*].deviceType").value(hasItem(device.getDeviceType().name())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(device.isActive())))
                .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(device.getUserExtra().getId().intValue())));
        }
    }

    @Test
    @Transactional
    public void getDevicesByLogin_AsPacient_NoUserForLoginException() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        String BAD_LOGIN = "BAD_LOGIN";

        // Get the device
        restDeviceMockMvc.perform(get("/api/user-devices/{login}",BAD_LOGIN))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsPacient() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsPacient_SomeoneElsesLogin() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        String someoneElsesLogin = userFamily.getLogin();

        getDevicesByLoginRestCall_EmptyList(someoneElsesLogin);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsFamily_SomeoneElsesLogin() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);
        String someoneElsesLogin = userPacient.getLogin();

        getDevicesByLoginRestCall_EmptyList(someoneElsesLogin);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_ForPacient_AsFamily() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);
        // Initialize the database
        setupDevicesForUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_ForPacient_AsFamily_NotAssociated() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_EmptyList(device.getUserExtra().getUser().getLogin());
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_AsOrganization_SomeoneElsesLogin() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);
        String someoneElsesLogin = userPacient.getLogin();

        getDevicesByLoginRestCall_EmptyList(someoneElsesLogin);

    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_ForPacient_AsOrganization() throws Exception {
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        // Initialize the database
        setupDevicesForUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDevicesByLogin_ForPacient_AsOrganization_NotAssociated() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDevicesByLoginRestCall_EmptyList(device.getUserExtra().getUser().getLogin());
    }

    private void getDevicesByLoginRestCall_IsOk() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/user-devices/{login}", device.getUserExtra().getUser().getLogin()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(device.getId().intValue()))
            .andExpect(jsonPath("$.[*].name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.[*].description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.[*].uuid").value(DEFAULT_UUID))
            .andExpect(jsonPath("$.[*].deviceType").value(DEFAULT_DEVICE_TYPE.name()))
            .andExpect(jsonPath("$.[*].active").value(DEFAULT_ACTIVE));
    }

    private void getDevicesByLoginRestCall_EmptyList(String unAuthorizedLogin) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/user-devices/{login}",unAuthorizedLogin))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsPacient() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        getDeviceWithIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsPacient_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraFamily;
        device.setUserExtra(someoneElse);
        deviceRepository.saveAndFlush(device);

        getDeviceWithIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsPacient_NoEntityException() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);
        Long badDeviceId = 666L;

        restDeviceMockMvc.perform(get("/api/devices/{id}", badDeviceId))
            .andExpect(status().isBadRequest());
        restDeviceMockMvc.perform(get("/api/devices/id").param("id",String.valueOf(badDeviceId)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsFamily() throws Exception {
        // Initialize the database
        device.setUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);

        getDeviceWithIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraPacient;
        device.setUserExtra(someoneElse);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsUnauthorized();

    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_FromPacient_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        device.setUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_FromPacient_AsFamily() throws Exception {
        userExtraPacient.setFamily(userFamily);
        userExtraRepository.saveAndFlush(userExtraPacient);
        // Initialize the database
        device.setUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsOrganization() throws Exception {
        // Initialize the database
        device.setUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_AsOrganization_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraPacient;
        device.setUserExtra(someoneElse);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_FromPacient_AsOrganization_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        device.setUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        // Get the device
        getDeviceWithIdRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithId_FromPacient_AsOrganization() throws Exception {
        userExtraPacient.setOrganization(userOrganization);
        userExtraRepository.saveAndFlush(userExtraPacient);
        // Initialize the database
        device.setUserExtra(userExtraPacient);
        deviceRepository.saveAndFlush(device);

        getDeviceWithIdRestCall_IsOk();
    }

    private void getDeviceWithIdRestCall_IsOk() throws Exception {
        getDeviceWithPathIdRestCall_IsOk(device);
        getDeviceWithParamIdRestCall_IsOk(device);
    }

    private void getDeviceWithParamIdRestCall_IsOk(Device device) throws Exception {
        restDeviceMockMvc.perform(get("/api/devices/id").param("id", String.valueOf(this.device.getId())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(device.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID))
            .andExpect(jsonPath("$.deviceType").value(DEFAULT_DEVICE_TYPE.name()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE));
    }

    private void getDeviceWithPathIdRestCall_IsOk(Device device) throws Exception {
        restDeviceMockMvc.perform(get("/api/devices/{id}", this.device.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(device.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID))
            .andExpect(jsonPath("$.deviceType").value(DEFAULT_DEVICE_TYPE.name()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE));
    }

    private void getDeviceWithIdRestCall_IsUnauthorized() throws Exception {
        getDeviceWithPathIdRestCall_IsUnauthorized();
        getDeviceWithParamIdRestCall_IsUnauthorized();
    }

    private void getDeviceWithParamIdRestCall_IsUnauthorized() throws Exception {
        restDeviceMockMvc.perform(get("/api/devices/id").param("id",String.valueOf(device.getId())))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    private void getDeviceWithPathIdRestCall_IsUnauthorized() throws Exception {
        restDeviceMockMvc.perform(get("/api/devices/{id}", device.getId()))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsPacient_NoEntityException() throws Exception {
        // Initialize the database
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);
        String INVALID_UUID = "INVALID_UUID";

        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/uuid").param("uuid", INVALID_UUID))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsPacient() throws Exception {
        // Initialize the database
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsPacient_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraFamily;
        setupDevicesForUserExtra(someoneElse);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsOk();
    }


    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraPacient;
        setupDevicesForUserExtra(someoneElse);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_ForPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_ForPacient_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_AsOrganization_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        UserExtra someoneElse = userExtraPacient;
        setupDevicesForUserExtra(someoneElse);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsUnauthorized();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_ForPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsOk();
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getDeviceWithUuidParam_ForPacient_AsOrganization_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        device.setUuid(VALID_UUID);
        deviceRepository.saveAndFlush(device);

        getDeviceWithUuidParamRestCall_IsUnauthorized();
    }

    private void getDeviceWithUuidParamRestCall_IsUnauthorized() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/uuid").param("uuid", VALID_UUID))
            .andExpect(status().isUnauthorized());
    }

    private void getDeviceWithUuidParamRestCall_IsOk() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/uuid").param("uuid", VALID_UUID))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(device.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.uuid").value(VALID_UUID))
            .andExpect(jsonPath("$.deviceType").value(DEFAULT_DEVICE_TYPE.name()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsPacient() throws Exception {
        // Initialize the database
        saveDevices();
        Long userId = userPacient.getId();

        getSurveyDeviceRestCall_IsOk(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsPacient_SurveyNotExisting() throws Exception {
        Long userId = userPacient.getId();
        Long userExtraId = userExtraPacient.getId();

        getSurveyDeviceRestCall_SurveyNotExisting_IsOk(userId, userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsPacient_NotAuthorizedUserId() throws Exception {
        setupDevicesForUserExtra(userExtraFamily);
        saveDevices();
        Long userId = userFamily.getId();

        getSurveyDeviceRestCall_IsUnauthorized(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        saveDevices();
        Long userId = userFamily.getId();

        getSurveyDeviceRestCall_IsOk(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsFamily_SurveyNotExisting() throws Exception {
        Long userId = userFamily.getId();
        Long userExtraId = userExtraFamily.getId();

        getSurveyDeviceRestCall_SurveyNotExisting_IsOk(userId, userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsFamily_NotAuthorizedUserId() throws Exception {
        Long userId = userOrganization.getId();
        saveDevices();

        // Get the device
        getSurveyDeviceRestCall_IsUnauthorized(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_ForPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient,userFamily);

        saveDevices();
        Long userId = userPacient.getId();

        getSurveyDeviceRestCall_IsOk(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_ForPacient_AsFamily_SurveyNotExisting() throws Exception {
        associatePacientWithFamily(userExtraPacient,userFamily);
        Long userId = userPacient.getId();
        Long userExtraId = userExtraPacient.getId();

        getSurveyDeviceRestCall_SurveyNotExisting_IsOk(userId, userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_ForPacient_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        saveDevices();
        Long userId = userPacient.getId();

        getSurveyDeviceRestCall_IsUnauthorized(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        saveDevices();
        Long userId = userOrganization.getId();

        getSurveyDeviceRestCall_IsOk(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsOrganization_SurveyNotExisting() throws Exception {
        Long userId = userOrganization.getId();
        Long userExtraId = userExtraOrganization.getId();

        getSurveyDeviceRestCall_SurveyNotExisting_IsOk(userId, userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_AsOrganization_NotAuthorizedUserId() throws Exception {
        Long userId = userFamily.getId();
        saveDevices();

        getSurveyDeviceRestCall_IsUnauthorized(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_ForPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        saveDevices();
        Long userId = userPacient.getId();

        getSurveyDeviceRestCall_IsOk(userId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN,userDetailsServiceBeanName = "userDetailsService")
    public void getSurveyDevice_ForPacient_AsOrganization_SurveyNotExisting() throws Exception {
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        Long userId = userPacient.getId();
        Long userExtraId = userExtraPacient.getId();

        getSurveyDeviceRestCall_SurveyNotExisting_IsOk(userId, userExtraId);
    }

    private void getSurveyDeviceRestCall_SurveyNotExisting_IsOk(Long userId, Long userExtraId) throws Exception {
        Long numberOfDevicesBeforeTest = deviceRepository.count();

        AtomicReference<Long> idOfCreatedSurvey = new AtomicReference<>();
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getSurveyDevice/userId").param("userId", String.valueOf(userId)))
            .andExpect(status().isOk())
            .andDo(mvcResult -> idOfCreatedSurvey.set(Long.valueOf(mvcResult.getResponse().getContentAsString())))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().stream().findFirst().get();
        assertThat(newDeviceInDb.getId()).isEqualTo(idOfCreatedSurvey.get());
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);

        checkIfThereDuplicateSurveyDevicesInDatabase(userExtraId);
    }

    private void getSurveyDeviceRestCall_IsOk(Long userId) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getSurveyDevice/userId").param("userId", String.valueOf(userId)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string(String.valueOf(surveyDevice.getId())));

        checkIfThereDuplicateSurveyDevicesInDatabase(surveyDevice.getUserExtra().getId());
    }

    private void checkIfThereDuplicateSurveyDevicesInDatabase(Long userExtraId) {
        //check if there are duplicates of survey
        List<Device> surveyDevicesIdDatabase = deviceRepository.findAllByUserExtraId(userExtraId)
            .stream()
            .filter(dev -> dev.getDeviceType().equals(DeviceType.SURVEY))
            .collect(Collectors.toList());
        assertThat(surveyDevicesIdDatabase.size()).isEqualTo(1);
    }

    private void getSurveyDeviceRestCall_IsUnauthorized(Long userId) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getSurveyDevice/userId").param("userId", String.valueOf(userId)))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsPacient() throws Exception {
        // Initialize the database
        saveDevices();
        Long userExtraId = userExtraPacient.getId();

        getFitbitWatchDeviceRestCall_IsOk(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsPacient_DeviceNotExisting() throws Exception {
        Long userExtraId = userExtraPacient.getId();

        Long numberOfDevicesBeforeTest = deviceRepository.count();
        getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(userExtraId);

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().get(0);
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsPacient_NotAuthorized() throws Exception {
        // Initialize the database
        saveDevices();
        Long userExtraId = userExtraFamily.getId();

        getFitbitWatchDeviceRestCall_IsUnauthorized(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        saveDevices();
        Long userExtraId = userExtraFamily.getId();

        getFitbitWatchDeviceRestCall_IsOk(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsFamily_DeviceNotExisting() throws Exception {
        // Initialize the database
        Long userExtraId = userExtraFamily.getId();

        Long numberOfDevicesBeforeTest = deviceRepository.count();
        // Get the device
        getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(userExtraId);

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().stream().findFirst().get();
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsFamily_NotAuthorized() throws Exception {
        // Initialize the database
        Long userExtraId = userExtraOrganization.getId();

        getFitbitWatchDeviceRestCall_IsUnauthorized(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsFamily() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient, userFamily);
        setupDevicesForUserExtra(userExtraPacient);
        saveDevices();
        Long userExtraId = userExtraPacient.getId();

        restDeviceMockMvc.perform(get("/api/devices/getFitbitWatchDevice/userExtraId").param("userExtraId", String.valueOf(userExtraId)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitDevice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(FITBIT_DEVICE_UUID.toString())))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(fitbitDevice.getDeviceType().toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(fitbitDevice.getUserExtra().getId().intValue())));
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsFamily_DeviceNotExisting() throws Exception {
        // Initialize the database
        associatePacientWithFamily(userExtraPacient, userFamily);
        Long userExtraId = userExtraPacient.getId();

        Long numberOfDevicesBeforeTest = deviceRepository.count();
        getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(userExtraId);

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().stream().findFirst().get();
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsFamily_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        saveDevices();
        associatePacientWithFamily(userExtraPacient,userFamily);
        Long userExtraId = userExtraOrganization.getId();

        getFitbitWatchDeviceRestCall_IsUnauthorized(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        saveDevices();
        Long userExtraId = userExtraOrganization.getId();

        getFitbitWatchDeviceRestCall_IsOk(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsOrganization_DeviceNotExisting() throws Exception {
        // Initialize the database
        Long userExtraId = userExtraOrganization.getId();

        Long numberOfDevicesBeforeTest = deviceRepository.count();
        getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(userExtraId);

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().stream().findFirst().get();
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_AsOrganization_NotAuthorized() throws Exception {
        // Initialize the database
        saveDevices();
        Long userExtraId = userExtraFamily.getId();

        getFitbitWatchDeviceRestCall_IsUnauthorized(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsOrganization() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        setupDevicesForUserExtra(userExtraPacient);
        saveDevices();
        Long userExtraId = userExtraPacient.getId();

        getFitbitWatchDeviceRestCall_IsOk(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsOrganization_DeviceNotExisting() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        Long userExtraId = userExtraPacient.getId();

        Long numberOfDevicesBeforeTest = deviceRepository.count();

        getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(userExtraId);

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
        Device newDeviceInDb = deviceRepository.findAll().stream().findFirst().get();
        assertThat(newDeviceInDb.getUserExtra().getId()).isEqualTo(userExtraId);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void getFitbitWatchDevice_OfPacient_AsOrganization_SomeoneElsesDevice() throws Exception {
        // Initialize the database
        associatePacientWithOrganization(userExtraPacient,userOrganization);
        Long userExtraId = userExtraFamily.getId();

        getFitbitWatchDeviceRestCall_IsUnauthorized(userExtraId);
    }

    private void getFitbitWatchDeviceRestCall_IsOk(Long userExtraId) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getFitbitWatchDevice/userExtraId").param("userExtraId", String.valueOf(userExtraId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fitbitDevice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(FITBIT_DEVICE_UUID)))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(fitbitDevice.getDeviceType().name())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE)))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(fitbitDevice.getUserExtra().getId().intValue())));
    }

    private void getFitbitWatchDeviceRestCall_DeviceNotExisting_IsOk(Long userExtraId) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getFitbitWatchDevice/userExtraId").param("userExtraId", String.valueOf(userExtraId)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.*",hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getName())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getDescription())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getUuid())))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getDeviceType().name())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().isActive())))
            .andExpect(jsonPath("$.[*].userExtraId").value(hasItem(deviceRepository.findAllByUserExtraIdAndDeviceType(userExtraId,DeviceType.FITBIT_WATCH).get().getUserExtra().getId().intValue())));
    }

    private void getFitbitWatchDeviceRestCall_IsUnauthorized(Long userExtraId) throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/getFitbitWatchDevice/userExtraId").param("userExtraId", String.valueOf(userExtraId)))
            .andExpect(status().isUnauthorized())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @Transactional
    public void createFitbitDevice() throws Exception {
        Long numberOfDevicesBeforeTest = deviceRepository.count();
        // Initialize the database
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        // Get the device
        restDeviceMockMvc.perform(post("/api/devices/createFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(deviceRepository.findAll().get(0).getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.uuid").value(FITBIT_DEVICE_UUID.toString()))
            .andExpect(jsonPath("$.deviceType").value(fitbitDevice.getDeviceType().toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.userExtraId").value(deviceRepository.findAll().get(0).getUserExtra().getId().intValue()));

        assertThat(numberOfDevicesBeforeTest + 1)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    public void createFitbitDevice_BadRequest_IdIsPresent() throws Exception {
        Long numberOfDevicesBeforeTest = deviceRepository.count();
        // Initialize the database
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        deviceDTO.setId(1L);
        // Get the device
        restDeviceMockMvc.perform(post("/api/devices/createFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest())
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));

        assertThat(numberOfDevicesBeforeTest)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    public void createFitbitDevice_BadRequest_UserExtraIdMissing() throws Exception {
        Long numberOfDevicesBeforeTest = deviceRepository.count();
        // Initialize the database
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        deviceDTO.setUserExtraId(null);
        // Get the device
        restDeviceMockMvc.perform(post("/api/devices/createFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest())
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));

        assertThat(numberOfDevicesBeforeTest)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    public void updateFitbitDevice() throws Exception {
        // Initialize the database
        fitbitDevice = deviceRepository.saveAndFlush(fitbitDevice);
        Long numberOfDevicesBeforeTest = deviceRepository.count();
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        // Get the device
        restDeviceMockMvc.perform(put("/api/devices/updateFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isOk())
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fitbitDevice.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.uuid").value(FITBIT_DEVICE_UUID.toString()))
            .andExpect(jsonPath("$.deviceType").value(fitbitDevice.getDeviceType().toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.userExtraId").value(fitbitDevice.getUserExtra().getId().intValue()));

        assertThat(numberOfDevicesBeforeTest)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    public void updateFitbitDevice_BadRequest_IdIsNotPresent() throws Exception {
        // Initialize the database
        fitbitDevice = deviceRepository.saveAndFlush(fitbitDevice);
        Long numberOfDevicesBeforeAutomaticallyCreatingSurveyDevice = deviceRepository.count();
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        deviceDTO.setId(null);
        // Get the device
        restDeviceMockMvc.perform(put("/api/devices/updateFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest())
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));

        assertThat(numberOfDevicesBeforeAutomaticallyCreatingSurveyDevice)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    public void updateFitbitDevice_BadRequest_UserExtraIdMissing() throws Exception {
        Long numberOfDevicesBeforeAutomaticallyCreatingSurveyDevice = deviceRepository.count();
        // Initialize the database
        DeviceDTO deviceDTO = deviceMapper.toDto(fitbitDevice);
        deviceDTO.setUserExtraId(null);
        // Get the device
        restDeviceMockMvc.perform(put("/api/devices/updateFitbitDevice")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deviceDTO)))
            .andExpect(status().isBadRequest())
            .andDo(mvcResult -> System.out.println(mvcResult.getResponse().getContentAsString()))
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));

        assertThat(numberOfDevicesBeforeAutomaticallyCreatingSurveyDevice)
            .isEqualTo(deviceRepository.count());
    }

    @Test
    @Transactional
    @WithUserDetails(value = PACIENT_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void deleteDevice_AsPacient() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        int databaseSizeBeforeDelete = deviceRepository.findAll().size();

        // Delete the device
        restDeviceMockMvc.perform(delete("/api/devices/{id}", device.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = FAMILY_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void deleteDevice_AsFamily() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraFamily);
        deviceRepository.saveAndFlush(device);

        int databaseSizeBeforeDelete = deviceRepository.findAll().size();

        // Delete the device
        restDeviceMockMvc.perform(delete("/api/devices/{id}", device.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    @WithUserDetails(value = ORGANIZATION_LOGIN, userDetailsServiceBeanName = "userDetailsService")
    public void deleteDevice_AsOrganization() throws Exception {
        // Initialize the database
        setupDevicesForUserExtra(userExtraOrganization);
        deviceRepository.saveAndFlush(device);

        int databaseSizeBeforeDelete = deviceRepository.findAll().size();

        // Delete the device
        restDeviceMockMvc.perform(delete("/api/devices/{id}", device.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Device.class);
        Device device1 = new Device();
        device1.setId(1L);
        Device device2 = new Device();
        device2.setId(device1.getId());
        assertThat(device1).isEqualTo(device2);
        device2.setId(2L);
        assertThat(device1).isNotEqualTo(device2);
        device1.setId(null);
        assertThat(device1).isNotEqualTo(device2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeviceDTO.class);
        DeviceDTO deviceDTO1 = new DeviceDTO();
        deviceDTO1.setId(1L);
        DeviceDTO deviceDTO2 = new DeviceDTO();
        assertThat(deviceDTO1).isNotEqualTo(deviceDTO2);
        deviceDTO2.setId(deviceDTO1.getId());
        assertThat(deviceDTO1).isEqualTo(deviceDTO2);
        deviceDTO2.setId(2L);
        assertThat(deviceDTO1).isNotEqualTo(deviceDTO2);
        deviceDTO1.setId(null);
        assertThat(deviceDTO1).isNotEqualTo(deviceDTO2);
    }

}
