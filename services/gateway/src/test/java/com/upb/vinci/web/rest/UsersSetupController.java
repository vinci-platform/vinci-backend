package com.upb.vinci.web.rest;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.web.rest.util.UserExtrasTestUtil;
import com.upb.vinci.web.rest.util.UsersTestUtil;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class UsersSetupController {

    protected static final Long FAMILY_USER_EXTRA_ID = 1L;
    protected static final Long ORGANIZATION_USER_EXTRA_ID = 2L;
    protected static final Long PACIENT_USER_EXTRA_ID = 201L;

    protected static final Long PACIENT_USER_ID = 4L;
    protected static final Long FAMILY_USER_ID = 5L;
    protected static final Long ORGANIZATION_USER_ID = 6L;

    protected static final String ADMIN_LOGIN = "admin";
    protected static final String PACIENT_LOGIN = "user";
    protected static final String FAMILY_LOGIN = "family_login";
    protected static final String ORGANIZATION_LOGIN = "organization_login";

    @Autowired
    protected UsersTestUtil usersTestUtil;

    @Autowired
    protected UserExtrasTestUtil userExtrasTestUtil;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected UserExtraRepository userExtraRepository;

    protected User userPacient;
    protected User userFamily;
    protected User userOrganization;

    protected UserExtra userExtraPacient;
    protected UserExtra userExtraFamily;
    protected UserExtra userExtraOrganization;

    protected void initUsers() {
        userFamily = usersTestUtil.createUserEntity(AuthoritiesConstants.FAMILY);
        userFamily = userRepository.saveAndFlush(userFamily);
        userExtraFamily = userExtrasTestUtil.createUserExtraEntity(AuthoritiesConstants.FAMILY,userFamily,null,null,null,null,null);
        userExtraFamily.setId(FAMILY_USER_EXTRA_ID);
        userExtraFamily = userExtraRepository.saveAndFlush(userExtraFamily);

        userOrganization = usersTestUtil.createUserEntity(AuthoritiesConstants.ORGANIZIATION);
        userOrganization = userRepository.saveAndFlush(userOrganization);
        userExtraOrganization = userExtrasTestUtil.createUserExtraEntity(AuthoritiesConstants.ORGANIZIATION,userOrganization,null,null,null,null,null);
        userExtraOrganization.setId(ORGANIZATION_USER_EXTRA_ID);
        userExtraOrganization = userExtraRepository.saveAndFlush(userExtraOrganization);

        userPacient = usersTestUtil.createUserEntity(AuthoritiesConstants.PACIENT);
        userPacient = userRepository.saveAndFlush(userPacient);
        userExtraPacient = userExtrasTestUtil.createUserExtraEntity(AuthoritiesConstants.PACIENT, userPacient,null,null,null,null,null);
        userExtraPacient.setId(PACIENT_USER_EXTRA_ID);
        userExtraPacient = userExtraRepository.saveAndFlush(userExtraPacient);
    }

    protected void associatePacientWithFamily(UserExtra pacient, User family){
        pacient.setFamily(family);
        userExtraRepository.saveAndFlush(pacient);
    }
    protected void associatePacientWithOrganization(UserExtra pacient, User organization){
        pacient.setOrganization(organization);
        userExtraRepository.saveAndFlush(pacient);
    }

}
