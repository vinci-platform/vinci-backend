package com.upb.vinci.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.upb.vinci.domain.enumeration.EducationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A UserExtra.
 */
@Entity
@Table(name = "user_extra")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserExtra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "description")
    private String description;

    @Column(name = "gender")
    private String gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "education")
    private EducationType education;

    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "friends")
    private String friends;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userExtra")
    private Set<UserAlert> alerts = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userExtra")
    private Set<UserImage> images = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userExtra")
    private Set<Device> devices = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties("userExtras")
    private User organization;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties("userExtras")
    private User family;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public UserExtra phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public UserExtra address(String address) {
        this.address = address;
        return this;
    }

    public UserExtra gender(String gender) {
        this.gender = gender;
        return this;
    }

    public UserExtra friends(String friends) {
        this.friends = friends;
        return this;
    }

    public UserExtra education(EducationType education) {
        this.education = education;
        return this;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public UserExtra user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<UserAlert> getAlerts() {
        return alerts;
    }

    public UserExtra alerts(Set<UserAlert> userAlerts) {
        this.alerts = userAlerts;
        return this;
    }

    public UserExtra addAlert(UserAlert userAlert) {
        this.alerts.add(userAlert);
        userAlert.setUserExtra(this);
        return this;
    }

    public UserExtra removeAlert(UserAlert userAlert) {
        this.alerts.remove(userAlert);
        userAlert.setUserExtra(null);
        return this;
    }

    public void setAlerts(Set<UserAlert> userAlerts) {
        this.alerts = userAlerts;
    }

    public Set<UserImage> getImages() {
        return images;
    }

    public UserExtra images(Set<UserImage> userImages) {
        this.images = userImages;
        return this;
    }

    public UserExtra addImage(UserImage userImage) {
        this.images.add(userImage);
        userImage.setUserExtra(this);
        return this;
    }

    public UserExtra removeImage(UserImage userImage) {
        this.images.remove(userImage);
        userImage.setUserExtra(null);
        return this;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public UserExtra devices(Set<Device> devices) {
        this.devices = devices;
        return this;
    }

    public UserExtra addDevice(Device device) {
        this.devices.add(device);
        device.setUserExtra(this);
        return this;
    }

    public UserExtra removeDevice(Device device) {
        this.devices.remove(device);
        device.setUserExtra(null);
        return this;
    }

    public void setImages(Set<UserImage> userImages) {
        this.images = userImages;
    }

    public User getOrganization() {
        return organization;
    }

    public UserExtra organization(User user) {
        this.organization = user;
        return this;
    }

    public void setOrganization(User user) {
        this.organization = user;
    }

    public User getFamily() {
        return family;
    }

    public UserExtra family(User user) {
        this.family = user;
        return this;
    }

    public void setFamily(User user) {
        this.family = user;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public EducationType getEducation() { return education; }

    public void setEducation(EducationType education) { this.education = education; }

    public String getMaritalStatus() { return maritalStatus; }

    public void setMaritalStatus(String maritalStatus) { this.maritalStatus = maritalStatus; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserExtra userExtra = (UserExtra) o;
        if (userExtra.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtra.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtra{" +
            "id=" + getId() +
            ", phone='" + getPhone() + "'" +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
