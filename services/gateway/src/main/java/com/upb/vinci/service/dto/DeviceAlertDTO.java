package com.upb.vinci.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.upb.vinci.domain.DeviceAlert;
import com.upb.vinci.domain.enumeration.AlertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A DTO for the DeviceAlert entity.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class DeviceAlertDTO implements Serializable {

    private Long id;

    @NotNull
    private String label;

    private String values;

    @NotNull
    private AlertType alertType;

    private Boolean userRead;

    private Boolean familyRead;

    private Boolean organizationRead;

    private Instant createdDate;

    private Long deviceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Boolean isUserRead() {
        return userRead;
    }

    public void setUserRead(Boolean userRead) {
        this.userRead = userRead;
    }

    public Boolean isFamilyRead() {
        return familyRead;
    }

    public void setFamilyRead(Boolean familyRead) {
        this.familyRead = familyRead;
    }

    public Boolean isOrganizationRead() {
        return organizationRead;
    }

    public void setOrganizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getUserRead() {
        return userRead;
    }

    public Boolean getFamilyRead() {
        return familyRead;
    }

    public Boolean getOrganizationRead() {
        return organizationRead;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceAlertDTO deviceAlertDTO = (DeviceAlertDTO) o;
        if (deviceAlertDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), deviceAlertDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DeviceAlertDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", values='" + getValues() + "'" +
            ", alertType='" + getAlertType() + "'" +
            ", userRead='" + isUserRead() + "'" +
            ", familyRead='" + isFamilyRead() + "'" +
            ", organizationRead='" + isOrganizationRead() + "'" +
            ", device=" + getDeviceId() +
            "}";
    }
}
