package com.upb.vinci.service.dto;

import com.upb.vinci.domain.enumeration.AlertType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the UserAlert entity.
 */
public class UserImageDTO implements Serializable {

    private Long id;

    @NotNull
    private byte[] image;

    private Long userExtraId;

    private String userExtraFirstName;

    private String userExtraLastName;

    private String format;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public String getUserExtraFirstName() {
        return userExtraFirstName;
    }

    public void setUserExtraFirstName(String userExtraFirstName) {
        this.userExtraFirstName = userExtraFirstName;
    }

    public String getUserExtraLastName() {
        return userExtraLastName;
    }

    public void setUserExtraLastName(String userExtraLastName) {
        this.userExtraLastName = userExtraLastName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public UserImageDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserImageDTO userAlertDTO = (UserImageDTO) o;
        if (userAlertDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAlertDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAlertDTO{" +
                "id=" + getId() +
                ", image='" + getImage() + "'" +
                ", userExtra=" + getUserExtraId() +
                "}";
    }
}
