package com.upb.vinci.service;

import com.upb.vinci.domain.HealthRecord;
import com.upb.vinci.domain.HealthRecord_;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.repository.HealthRecordRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.dto.HealthRecordCriteria;
import com.upb.vinci.service.dto.HealthRecordDTO;
import com.upb.vinci.service.mapper.HealthRecordMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for HealthRecord entities in the database.
 * The main input is a {@link HealthRecordCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HealthRecordDTO} or a {@link Page} of {@link HealthRecordDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HealthRecordQueryService extends QueryService<HealthRecord> {

    private static final String ENTITY_NAME = "healthRecord";

    private final Logger log = LoggerFactory.getLogger(HealthRecordQueryService.class);

    private final HealthRecordRepository healthRecordRepository;

    private final HealthRecordMapper healthRecordMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public HealthRecordQueryService(HealthRecordRepository healthRecordRepository, HealthRecordMapper healthRecordMapper,
                                    UserExtraRepository userExtraRepository, AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.healthRecordRepository = healthRecordRepository;
        this.healthRecordMapper = healthRecordMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link Page} of {@link HealthRecordDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HealthRecordDTO> findByCriteria(HealthRecordCriteria criteria, Pageable page) {
        log.debug("find "+ENTITY_NAME+" by criteria : {}, page: {}", criteria, page);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        LongFilter userIdFilter = criteria.getUserId();
        List<Long> userIdList = new ArrayList<>();
        userIdList.add(currentUser.getId());

        switch (currentUsersAuthority) {
            case ADMIN: {
                final Specification<HealthRecord> specification = createSpecification(criteria);
                return healthRecordRepository.findAll(specification, page)
                    .map(healthRecordMapper::toDto);
            }
            case FAMILY:  addUserIdsFromUserList(userIdList, userExtraRepository.findByFamily(currentUser));
            case ORGANIZIATION: addUserIdsFromUserList(userIdList, userExtraRepository.findByOrganization(currentUser));
        }

        criteria.setUserId(longFilterUtil.seIdFilter(userIdList, userIdFilter));
        final Specification<HealthRecord> specification = createSpecification(criteria);
        return healthRecordRepository.findAll(specification, page)
            .map(healthRecordMapper::toDto);
    }

    private void addUserIdsFromUserList(List<Long> userIdList, List<UserExtra> userExtraList) {
        userExtraList
            .forEach(userExtra -> userIdList.add(userExtra.getUser().getId()));
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HealthRecordCriteria criteria) {
        log.debug("count "+ENTITY_NAME+" by criteria : {}", criteria);
        final Specification<HealthRecord> specification = createSpecification(criteria);
        return healthRecordRepository.count(specification);
    }

    /**
     * Function to convert HealthRecordCriteria to a {@link Specification}
     */
    private Specification<HealthRecord> createSpecification(HealthRecordCriteria criteria) {
        Specification<HealthRecord> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), HealthRecord_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserId(), HealthRecord_.userId));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), HealthRecord_.timestamp));
            }
            if (criteria.getScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScore(), HealthRecord_.score));
            }
            if (criteria.getAvailableScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAvailableScore(), HealthRecord_.availableScore));
            }
            if (criteria.getStepsScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStepsScore(), HealthRecord_.stepsScore));
            }
            if (criteria.getIpaqScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIpaqScore(), HealthRecord_.ipaqScore));
            }
            if (criteria.getWhoqolScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWhoqolScore(), HealthRecord_.whoqolScore));
            }
            if (criteria.getFeelingsScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFeelingsScore(), HealthRecord_.feelingsScore));
            }
        }
        return specification;
    }
}
