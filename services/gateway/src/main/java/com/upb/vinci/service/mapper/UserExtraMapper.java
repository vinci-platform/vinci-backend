package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.UserExtraDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserExtra and its DTO UserExtraDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserExtraMapper extends EntityMapper<UserExtraDTO, UserExtra> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "login")
    @Mapping(source = "organization.id", target = "organizationId")
    @Mapping(source = "family.id", target = "familyId")
    @Mapping(source = "family.firstName", target = "familyFirstName")
    @Mapping(source = "family.lastName", target = "familyLastName")
    @Mapping(source = "user.firstName", target = "userFirstName")
    @Mapping(source = "user.lastName", target = "userLastName")
    @Mapping(source = "organization.firstName", target = "organizationFirstName")
    @Mapping(source = "organization.lastName", target = "organizationLastName")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "gender", target = "gender")
    @Mapping(source = "devices", target = "devices")
    @Mapping(source = "friends", target = "friends")
    UserExtraDTO toDto(UserExtra userExtra);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "alerts", ignore = true)
    @Mapping(target = "images", ignore = true)
    @Mapping(target = "devices", ignore = true)
    @Mapping(source = "organizationId", target = "organization")
    @Mapping(source = "familyId", target = "family")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "gender", target = "gender")
    @Mapping(source = "friends", target = "friends")
    UserExtra toEntity(UserExtraDTO userExtraDTO);

    default UserExtra fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtra userExtra = new UserExtra();
        userExtra.setId(id);
        return userExtra;
    }
}
