package com.upb.vinci.bootstrap;

import com.upb.vinci.domain.UserBruteForceAttackData;
import com.upb.vinci.repository.UserBruteForceAttackDataRepository;
import com.upb.vinci.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class BootstrapData implements CommandLineRunner {

    private final UserBruteForceAttackDataRepository userBruteForceAttackDataRepository;
    private final UserRepository userRepository;

    public BootstrapData(UserBruteForceAttackDataRepository userBruteForceAttackDataRepository, UserRepository userRepository) {
        this.userBruteForceAttackDataRepository = userBruteForceAttackDataRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        userRepository.findAll().forEach(user -> {
            if (!userBruteForceAttackDataRepository.findByUser(user).isPresent()) {
                userBruteForceAttackDataRepository.saveAndFlush(UserBruteForceAttackData.builder()
                    .user(user)
                    .loginDisabled(false)
                    .loginEnabledFrom(Instant.now())
                    .failedLoginAttempts(0L)
                    .build());
            }
        });
    }
}
