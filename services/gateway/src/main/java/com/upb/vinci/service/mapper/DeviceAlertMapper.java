package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.DeviceAlertDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DeviceAlert and its DTO DeviceAlertDTO.
 */
@Mapper(componentModel = "spring", uses = {DeviceMapper.class})
public interface DeviceAlertMapper extends EntityMapper<DeviceAlertDTO, DeviceAlert> {

    @Mapping(source = "device.id", target = "deviceId")
    @Mapping(source = "createdDate", target = "createdDate")
    DeviceAlertDTO toDto(DeviceAlert deviceAlert);

    @Mapping(source = "deviceId", target = "device")
    @Mapping(source = "createdDate", target = "createdDate")
    DeviceAlert toEntity(DeviceAlertDTO deviceAlertDTO);

    default DeviceAlert fromId(Long id) {
        if (id == null) {
            return null;
        }
        DeviceAlert deviceAlert = new DeviceAlert();
        deviceAlert.setId(id);
        return deviceAlert;
    }
}
