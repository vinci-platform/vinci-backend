package com.upb.vinci.service;

import com.upb.vinci.service.dto.EventRecordDTO;

/**
 * Service Interface for managing EventRecord.
 */
public interface EventRecordService {

    /**
     * Save a eventRecord.
     *
     * @param eventRecordDTO the entity to save
     * @return the persisted entity
     */
    EventRecordDTO save(EventRecordDTO eventRecordDTO);

    /**
     * Get the "id" eventRecord.
     *
     * @param id the id of the entity
     * @return the entity
     */

    EventRecordDTO findOne(Long id);
    /**
     * Delete the "id" eventRecord.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    EventRecordDTO update(EventRecordDTO eventRecordDTO);
}
