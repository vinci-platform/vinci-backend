package com.upb.vinci.service.kafka;

import com.upb.vinci.config.kafka.TopicConstants;
import com.upb.vinci.service.DeviceService;
import com.upb.vinci.service.dto.DeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaConsumer {

    private final DeviceService deviceService;

    public KafkaConsumer(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @KafkaListener(topics = TopicConstants.TOPIC_FITBIT_DEVICE_UPDATE, containerFactory = "deviceDtoKafkaListenerContainerFactory")
    public void consumeFitbitWatchData(DeviceDTO deviceDTO) {
        log.debug("deviceDTO received: {} ; from topic: {}",deviceDTO, TopicConstants.TOPIC_FITBIT_DEVICE_UPDATE);
        deviceService.updateFitbitDevice(deviceDTO);
    }

}
