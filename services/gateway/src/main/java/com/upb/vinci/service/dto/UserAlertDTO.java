package com.upb.vinci.service.dto;
import com.upb.vinci.domain.enumeration.AlertType;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the UserAlert entity.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserAlertDTO implements Serializable {

    private Long id;

    @NotNull
    private String label;

    private String values;

    @NotNull
    private AlertType alertType;

    private Boolean userRead;

    private Boolean familyRead;

    private Boolean organizationRead;

    private Instant createdDate;

    @NotNull
    private Long userExtraId;

    private String userExtraFirstName;

    private String userExtraLastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Boolean isUserRead() {
        return userRead;
    }

    public void setUserRead(Boolean userRead) {
        this.userRead = userRead;
    }

    public Boolean isFamilyRead() {
        return familyRead;
    }

    public void setFamilyRead(Boolean familyRead) {
        this.familyRead = familyRead;
    }

    public Boolean isOrganizationRead() {
        return organizationRead;
    }

    public void setOrganizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Boolean getUserRead() {
        return userRead;
    }

    public Boolean getFamilyRead() {
        return familyRead;
    }

    public Boolean getOrganizationRead() {
        return organizationRead;
    }

    public String getUserExtraFirstName() {
        return userExtraFirstName;
    }

    public void setUserExtraFirstName(String userExtraFirstName) {
        this.userExtraFirstName = userExtraFirstName;
    }

    public String getUserExtraLastName() {
        return userExtraLastName;
    }

    public void setUserExtraLastName(String userExtraLastName) {
        this.userExtraLastName = userExtraLastName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public UserAlertDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserAlertDTO userAlertDTO = (UserAlertDTO) o;
        if (userAlertDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAlertDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAlertDTO{" +
                "id=" + getId() +
                ", label='" + getLabel() + "'" +
                ", values='" + getValues() + "'" +
                ", alertType='" + getAlertType() + "'" +
                ", userRead='" + isUserRead() + "'" +
                ", familyRead='" + isFamilyRead() + "'" +
                ", organizationRead='" + isOrganizationRead() + "'" +
                ", userExtra=" + getUserExtraId() +
                "}";
    }
}
