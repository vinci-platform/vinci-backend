package com.upb.vinci.web.rest.feign.handlers;

import feign.Response;

public interface FeignHttpExceptionHandler {
    Exception handle(Response response);
}
