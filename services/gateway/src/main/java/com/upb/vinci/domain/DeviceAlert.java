package com.upb.vinci.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.upb.vinci.domain.enumeration.AlertType;

/**
 * A DeviceAlert.
 */
@Entity
@Table(name = "device_alert")
public class DeviceAlert implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_label", nullable = false)
    private String label;

    @Column(name = "jhi_values")
    private String values;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "alert_type", nullable = false)
    private AlertType alertType;

    @Column(name = "user_read")
    private Boolean userRead;

    @Column(name = "family_read")
    private Boolean familyRead;

    @Column(name = "organization_read")
    private Boolean organizationRead;

    @ManyToOne
    @JsonIgnoreProperties("alerts")
    private Device device;

    @Column(name = "created_date", insertable = false)
    private Instant createdDate = Instant.now();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public DeviceAlert label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValues() {
        return values;
    }

    public DeviceAlert values(String values) {
        this.values = values;
        return this;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public DeviceAlert alertType(AlertType alertType) {
        this.alertType = alertType;
        return this;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Boolean isUserRead() {
        return userRead;
    }

    public DeviceAlert userRead(Boolean userRead) {
        this.userRead = userRead;
        return this;
    }

    public void setUserRead(Boolean userRead) {
        this.userRead = userRead;
    }

    public Boolean isFamilyRead() {
        return familyRead;
    }

    public DeviceAlert familyRead(Boolean familyRead) {
        this.familyRead = familyRead;
        return this;
    }

    public void setFamilyRead(Boolean familyRead) {
        this.familyRead = familyRead;
    }

    public Boolean isOrganizationRead() {
        return organizationRead;
    }

    public DeviceAlert organizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
        return this;
    }

    public void setOrganizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
    }

    public Device getDevice() {
        return device;
    }

    public DeviceAlert device(Device device) {
        this.device = device;
        return this;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getUserRead() {
        return userRead;
    }

    public Boolean getFamilyRead() {
        return familyRead;
    }

    public Boolean getOrganizationRead() {
        return organizationRead;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DeviceAlert deviceAlert = (DeviceAlert) o;
        if (deviceAlert.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), deviceAlert.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DeviceAlert{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", values='" + getValues() + "'" +
            ", alertType='" + getAlertType() + "'" +
            ", userRead='" + isUserRead() + "'" +
            ", familyRead='" + isFamilyRead() + "'" +
            ", organizationRead='" + isOrganizationRead() + "'" +
            "}";
    }
}
