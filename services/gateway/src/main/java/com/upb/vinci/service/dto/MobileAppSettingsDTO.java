package com.upb.vinci.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MobileAppSettings entity.
 */
public class MobileAppSettingsDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String value;

    private Long userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MobileAppSettingsDTO mobileAppSettingsDTO = (MobileAppSettingsDTO) o;
        if (mobileAppSettingsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mobileAppSettingsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MobileAppSettingsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", value='" + getValue() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
