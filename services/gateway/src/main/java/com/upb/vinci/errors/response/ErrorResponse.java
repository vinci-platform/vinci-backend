package com.upb.vinci.errors.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URI;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorResponse {
    private URI type;
    private String title;//unique
    private int status;//400,401,500,429
    private String detail;//vise info
    private URI instance;
    private Map<String, Object> parameters;
}
