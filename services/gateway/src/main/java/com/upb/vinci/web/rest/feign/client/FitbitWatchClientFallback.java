package com.upb.vinci.web.rest.feign.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FitbitWatchClientFallback implements FitbitWatchClient {

        @Override
        public ResponseEntity<String> getCodeMobile(String code,Long userExtraId) {
            return ResponseEntity.ok().body("Successful Fitbit Authentication.");
    }
}
