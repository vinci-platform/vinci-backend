package com.upb.vinci.config.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upb.vinci.service.dto.DeviceDTO;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfiguration {
    @Value("${kafka.bootstrap-servers}")
    private String BOOTSTRAP_SERVERS_CONFIG;

    @Value("${kafka.group-id}")
    private String GROUP_ID;

    @Autowired
    private ObjectMapper objectMapper;

    // 1. Consume string data from Kafka
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory(){
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerFactory());

        return factory;
    }

    // 2. Consume DeviceDto objects from Kafka
    public ConsumerFactory<String, DeviceDTO> deviceDtoConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.put(JsonDeserializer.VALUE_DEFAULT_TYPE, DeviceDTO.class);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, getTrustedPackagesForDeserialization());

        JsonDeserializer<DeviceDTO> deviceDTOJsonDeserializer = new JsonDeserializer<>(DeviceDTO.class, objectMapper);
        deviceDTOJsonDeserializer.configure(props,false);

        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), deviceDTOJsonDeserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, DeviceDTO> deviceDtoKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, DeviceDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(deviceDtoConsumerFactory());
        return factory;
    }

    private String getTrustedPackagesForDeserialization() {
        return "*";
    }
}
