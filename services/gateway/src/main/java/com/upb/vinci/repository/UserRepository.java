package com.upb.vinci.repository;

import com.upb.vinci.domain.User;
import com.upb.vinci.service.dto.FirstAndLastNameWithUuid;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    String USERS_BY_LOGIN_CACHE = "usersByLogin";

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneById(Long id);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_LOGIN_CACHE)
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
    Optional<User> findOneWithAuthoritiesByEmail(String email);

    Page<User> findAllByLoginNot(Pageable pageable, String login);

    @Query("SELECT user FROM User user" +
        " JOIN UserExtra user_extra ON(user.id = user_extra.user.id)" +
        " WHERE user_extra.family.id = :familyId")
    Page<User> findByFamilyId(@Param("familyId") Long familyId, Pageable pageable);

    @Query("SELECT user FROM User user" +
        " JOIN UserExtra user_extra ON(user.id = user_extra.user.id) " +
        " WHERE user_extra.organization.id = :organizationId")
    Page<User> findByOrganizationId(@Param("organizationId") Long organizationId, Pageable pageable);

    @Query("SELECT new com.upb.vinci.service.dto.FirstAndLastNameWithUuid(user.id,user.firstName,user.lastName,user_extra.uuid) FROM User user" +
        " JOIN user.authorities authority" +
        " JOIN UserExtra user_extra on(user.id = user_extra.user.id)" +
        " WHERE authority.name = :authName")
    List<FirstAndLastNameWithUuid> findAllFirstAndLastNamesWithUuid(@Param("authName") String authName);
}
