package com.upb.vinci.web.rest;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.MobileAppSettingsQueryService;
import com.upb.vinci.service.MobileAppSettingsService;
import com.upb.vinci.service.dto.MobileAppSettingsCriteria;
import com.upb.vinci.service.dto.MobileAppSettingsDTO;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing MobileAppSettings.
 */
@RestController
@RequestMapping("/api")
public class MobileAppSettingsResource {

    private final Logger log = LoggerFactory.getLogger(MobileAppSettingsResource.class);

    private static final String ENTITY_NAME = "mobileAppSettings";

    private final MobileAppSettingsService mobileAppSettingsService;

    private final MobileAppSettingsQueryService mobileAppSettingsQueryService;

    public MobileAppSettingsResource(MobileAppSettingsService mobileAppSettingsService, MobileAppSettingsQueryService mobileAppSettingsQueryService) {
        this.mobileAppSettingsService = mobileAppSettingsService;
        this.mobileAppSettingsQueryService = mobileAppSettingsQueryService;
    }

     /**
     * POST  /mobile-app-settings : Create a new mobileAppSettings.
     *
     * @param mobileAppSettingsDTO the mobileAppSettingsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mobileAppSettingsDTO, or with status 400 (Bad Request) if the mobileAppSettings has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/mobile-app-settings")
    public ResponseEntity<MobileAppSettingsDTO> createMobileAppSettings(@Valid @RequestBody MobileAppSettingsDTO mobileAppSettingsDTO) throws URISyntaxException {
        log.debug("REST request to save MobileAppSettings : {}", mobileAppSettingsDTO);
        MobileAppSettingsDTO result = mobileAppSettingsService.save(mobileAppSettingsDTO);
        return ResponseEntity.created(new URI("/api/mobile-app-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mobile-app-settings : Updates an existing mobileAppSettings.
     *
     * @param mobileAppSettingsDTO the mobileAppSettingsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mobileAppSettingsDTO,
     * or with status 400 (Bad Request) if the mobileAppSettingsDTO is not valid,
     * or with status 500 (Internal Server Error) if the mobileAppSettingsDTO couldn't be updated
     */
    @PutMapping("/mobile-app-settings")
    public ResponseEntity<MobileAppSettingsDTO> updateMobileAppSettings(@Valid @RequestBody MobileAppSettingsDTO mobileAppSettingsDTO) {
        log.debug("REST request to update MobileAppSettings : {}", mobileAppSettingsDTO);
        MobileAppSettingsDTO result = mobileAppSettingsService.update(mobileAppSettingsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, mobileAppSettingsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mobile-app-settings : get all the mobileAppSettings.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of mobileAppSettings in body
     */
    @GetMapping("/mobile-app-settings")
    public ResponseEntity<List<MobileAppSettingsDTO>> getAllMobileAppSettings(MobileAppSettingsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MobileAppSettings by criteria: {}", criteria);
        Page<MobileAppSettingsDTO> page = mobileAppSettingsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/mobile-app-settings");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /mobile-app-settings/count : count all the mobileAppSettings.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/mobile-app-settings/count")
    public ResponseEntity<Long> countMobileAppSettings(MobileAppSettingsCriteria criteria) {
        log.debug("REST request to count MobileAppSettings by criteria: {}", criteria);
        return ResponseEntity.ok().body(mobileAppSettingsQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /mobile-app-settings/:id : get the "id" mobileAppSettings.
     *
     * @param id the id of the mobileAppSettingsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mobileAppSettingsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/mobile-app-settings/{id}")
    public ResponseEntity<MobileAppSettingsDTO> getMobileAppSettings(@PathVariable Long id) {
        log.debug("REST request to get MobileAppSettings : {}", id);
        return ResponseEntity.ok(mobileAppSettingsService.findOne(id));
    }

    /**
     * DELETE  /mobile-app-settings/:id : delete the "id" mobileAppSettings.
     *
     * @param id the id of the mobileAppSettingsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/mobile-app-settings/{id}")
    public ResponseEntity<Void> deleteMobileAppSettings(@PathVariable Long id) {
        log.debug("REST request to delete MobileAppSettings : {}", id);
        mobileAppSettingsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
