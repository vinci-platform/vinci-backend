package com.upb.vinci.service.dto;

import java.io.Serializable;

public class FitbitRedirectUrl implements Serializable {
    public String url;

    public FitbitRedirectUrl(String url) {
        this.url = url;
    }
}
