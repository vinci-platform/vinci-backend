package com.upb.vinci.web.rest;


import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.UserImage;
import com.upb.vinci.errors.NoUserByLoginException;
import com.upb.vinci.errors.NoUserForTokenException;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.repository.UserImageRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.security.SecurityUtils;
import com.upb.vinci.service.MailService;
import com.upb.vinci.service.UserService;
import com.upb.vinci.service.dto.PasswordChangeDTO;
import com.upb.vinci.service.dto.UserDTO;
import com.upb.vinci.web.rest.errors.*;
import com.upb.vinci.web.rest.vm.KeyAndPasswordVM;
import com.upb.vinci.web.rest.vm.ManagedUserVM;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final UserExtraRepository userExtraRepository;

    private final UserImageRepository userImageRepository;

    private final MailService mailService;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService, UserImageRepository userImageRepository, UserExtraRepository userExtraRepository) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.userExtraRepository = userExtraRepository;
        this.userImageRepository = userImageRepository;
    }

    /**
     * POST  /register : register the user.
     *
     * @param managedUserVM the managed user View Model
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already used
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * GET  /activate : activate the registered user.
     *
     * @param key the activation key
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        userService.activateRegistration(key);

    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
   * GET  /validate : check if  the user exist by userId (for survey microservice).
     *
     * @return the current user
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
     */
    @GetMapping("/validate")
    @Deprecated
    public Long validate(@RequestParam("userId") Long userId) {
        Optional<User> user = userRepository.findOneById(userId);
        Long response;
        if (user.isPresent()) {
	        response=userId;
	    }
        else {
	        response=0L;
        }
      return response;
    }

    /**
     * GET  /account : get the current user.
     *
     * @return the current user
     * @throws com.upb.vinci.errors.NoUserForTokenException 401 (Unauthorized exception) if there is no user in token
     */
    @GetMapping("/account")
    public ResponseEntity<UserDTO> getAccount() {
        log.debug("REST requset to get current Account!");
        UserDTO currentUserDto = userService.getCurrentUserDto();
        return ResponseEntity.ok(currentUserDto);
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws RuntimeException 500 (Internal Server Error) if the user login wasn't found
     */
    @PostMapping("/account")
    @Transactional
    public void saveAccount(@RequestParam("account") UserDTO userDTO, @RequestParam("image1") Optional<MultipartFile> image1,
                            @RequestParam("image1Id") Optional<Long> image1Id, @RequestParam("image2") Optional<MultipartFile> image2,
                            @RequestParam("image2Id") Optional<Long> image2Id) {
        log.debug(userDTO.toString());
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(NoUserForTokenException::new);
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        User user = userRepository.findOneByLogin(userLogin)
            .orElseThrow(() -> new NoUserByLoginException(userLogin));

        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());

        UserExtra userExtra = userExtraRepository.findByUserId(user.getId());
        if(userExtra!= null) {
            userExtra.setGender(userDTO.getGender() != null ? userDTO.getGender() : "");
            userExtra.setAddress(userDTO.getAddress() != null ? userDTO.getAddress() : "");
            userExtra.setPhone(userDTO.getPhone() != null ? userDTO.getPhone() : "");
            userExtra.setFriends(userExtra.getFriends() != null && !userDTO.getFriends().equals("") ? userDTO.getFriends() : "");
            try {
                if (image1.isPresent()) {
                    if (image1Id.isPresent() && image1Id.get() != 0) {
                        Optional<UserImage> userImage = userImageRepository.findById(image1Id.get());
                        if (userImage.isPresent()) {
                            UserImage ui = userImage.get();
                            ui.setImage(image1.get().getBytes());
                            ui.setFormat(FilenameUtils.getExtension(image1.get().getOriginalFilename()));
                            userImageRepository.save(ui);
                            userImageRepository.flush();
                        } else {
                            throw new InternalServerErrorException("Image 1 could not be found");
                        }
                    } else {
                        UserImage img1 = new UserImage();
                        img1.setImage(image1.get().getBytes());
                        img1.setFormat(FilenameUtils.getExtension(image1.get().getOriginalFilename()));
                        img1.setUserExtra(userExtra);
                        userImageRepository.save(img1);
                        userImageRepository.flush();
                    }
                }
                if (image2.isPresent()) {
                    if (image2Id.isPresent() && image2Id.get() != 0) {
                        Optional<UserImage> userImage = userImageRepository.findById(image2Id.get());
                        if (userImage.isPresent()) {
                            UserImage ui = userImage.get();
                            ui.setImage(image2.get().getBytes());
                            ui.setFormat(FilenameUtils.getExtension(image2.get().getOriginalFilename()));
                            userImageRepository.save(ui);
                        } else {
                            throw new InternalServerErrorException("Image 2 could not be found");
                        }
                    } else {
                        UserImage img2 = new UserImage();
                        img2.setImage(image2.get().getBytes());
                        img2.setFormat(FilenameUtils.getExtension(image2.get().getOriginalFilename()));
                        img2.setUserExtra(userExtra);
                        userImageRepository.save(img2);
                    }
                }
            } catch (IOException e) {
                throw new InternalServerErrorException("Problem saving user images");
            }
            userExtraRepository.save(userExtra);
        }
    }

    /**
     * POST  /account/change-password : changes the current user's password
     *
     * @param passwordChangeDto current and new password
     * @throws InvalidPasswordException 400 (Bad Request) if the new password is incorrect
     */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody @Valid PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }

    /**
     * POST   /account/reset-password/init : Send an email to reset the password of the user
     *
     * @param mail the mail of the user
     * @throws EmailNotFoundException 400 (Bad Request) if the email address is not registered
     */
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
       mailService.sendPasswordResetMail(
           userService.requestPasswordReset(mail)
               .orElseThrow(EmailNotFoundException::new)
       );
    }

    /**
     * POST   /account/reset-password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws RuntimeException 500 (Internal Server Error) if the password could not be reset
     */
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody @Valid KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey(), keyAndPassword.getActivate())
            .orElseThrow(() -> new BadRequestAlertException("No user was found for reset key!","user","noUserForResetkey"));

    }

    /**
     * POST  /associate-patient : associate a patient
     *
     * @param body the target user uuid and the boolean isFamily (if false then associate to organization)
     * @throws NoUserForTokenException 401 (Unauthorized Request) if no user in auth token
     * @throws BadRequestAlertException 400 (Bad Request) if there is no user for uuid
     */
    @PostMapping("/associate-patient")
    @ResponseStatus(HttpStatus.CREATED)
    public void associatePatient(@Valid @RequestBody Map<String,Object> body) {
        log.debug("REST request to associate pacient, body: {}",body);
        userService.associatePacient(body);
    }

    /**
     * GET  /associate : associate the user
     *
     * @param userId the owner (family / organization) user id
     * @param targetUserId the patient user id
     * @param isFamily associate to family or organization
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be associated
     */
    @GetMapping("/associate")
    public void associateAccount(@RequestParam(value = "u") Long userId, @RequestParam(value = "tu") Long targetUserId,
                                  @RequestParam(value = "f") Boolean isFamily) {
        userService.associateUser(userId, targetUserId, isFamily, true);
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
}
