package com.upb.vinci.web.websocket.dto;


import com.upb.vinci.web.websocket.dto.skeleton.Skeleton;

public class CameraDataDTO {
    private Long userId; /*will be set after bracelet identification*/
    private String deviceId;
    private Skeleton skeleton;
    private byte[] depthImage;

    public Long getUserId() {
        return userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public byte[] getDepthImage() {
        return depthImage;
    }

    @Override
    public String toString() {
        return userId + " " + deviceId + " " + depthImage + " " + skeleton.toString();
    }
}
