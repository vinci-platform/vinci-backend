package com.upb.vinci.service.dto;

import com.upb.vinci.domain.enumeration.AlertType;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the DeviceAlert entity. This class is used in DeviceAlertResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /device-alerts?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DeviceAlertCriteria implements Serializable {
    /**
     * Class for filtering AlertType
     */
    public static class AlertTypeFilter extends Filter<AlertType> {
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private StringFilter values;

    private AlertTypeFilter alertType;

    private BooleanFilter userRead;

    private BooleanFilter familyRead;

    private BooleanFilter organizationRead;

    private LongFilter deviceId;

    private InstantFilter createdDate;

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public StringFilter getValues() {
        return values;
    }

    public void setValues(StringFilter values) {
        this.values = values;
    }

    public AlertTypeFilter getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertTypeFilter alertType) {
        this.alertType = alertType;
    }

    public BooleanFilter getUserRead() {
        return userRead;
    }

    public void setUserRead(BooleanFilter userRead) {
        this.userRead = userRead;
    }

    public BooleanFilter getFamilyRead() {
        return familyRead;
    }

    public void setFamilyRead(BooleanFilter familyRead) {
        this.familyRead = familyRead;
    }

    public BooleanFilter getOrganizationRead() {
        return organizationRead;
    }

    public void setOrganizationRead(BooleanFilter organizationRead) {
        this.organizationRead = organizationRead;
    }

    public LongFilter getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(LongFilter deviceId) {
        this.deviceId = deviceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DeviceAlertCriteria that = (DeviceAlertCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(values, that.values) &&
            Objects.equals(alertType, that.alertType) &&
            Objects.equals(userRead, that.userRead) &&
            Objects.equals(familyRead, that.familyRead) &&
            Objects.equals(organizationRead, that.organizationRead) &&
            Objects.equals(deviceId, that.deviceId) &&
            Objects.equals(createdDate, that.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        values,
        alertType,
        userRead,
        familyRead,
        organizationRead,
        deviceId, createdDate
        );
    }

    @Override
    public String toString() {
        return "DeviceAlertCriteria{" +
            "id=" + id +
            ", label=" + label +
            ", values=" + values +
            ", alertType=" + alertType +
            ", userRead=" + userRead +
            ", familyRead=" + familyRead +
            ", organizationRead=" + organizationRead +
            ", deviceId=" + deviceId +
            ", createdDate=" + createdDate +
            '}';
    }
}
