package com.upb.vinci.service.dto;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.upb.vinci.domain.enumeration.DeviceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Device entity.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private String uuid;

    @NotNull
    private DeviceType deviceType;

    @NotNull
    private Boolean active;

    @JsonSerialize(using = ToStringSerializer.class)
    private Instant startTimestamp;

    @NotNull
    private Long userExtraId;

    private String userExtraFirstName;

    private String userExtraLastName;

    private Long userFamilyId;

    private String userFamilyFirstName;

    private String userFamilyLastName;

    private Long userOrganisationId;

    private String userOrganisationFirstName;

    private String userOrganisationLastName;

    private List<DeviceAlertDTO> alerts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Instant getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Boolean getActive() {
        return active;
    }

    public String getUserExtraFirstName() {
        return userExtraFirstName;
    }

    public void setUserExtraFirstName(String userExtraFirstName) {
        this.userExtraFirstName = userExtraFirstName;
    }

    public String getUserExtraLastName() {
        return userExtraLastName;
    }

    public void setUserExtraLastName(String userExtraLastName) {
        this.userExtraLastName = userExtraLastName;
    }

    public Long getUserFamilyId() {
        return userFamilyId;
    }

    public void setUserFamilyId(Long userFamilyId) {
        this.userFamilyId = userFamilyId;
    }

    public String getUserFamilyFirstName() {
        return userFamilyFirstName;
    }

    public void setUserFamilyFirstName(String userFamilyFirstName) {
        this.userFamilyFirstName = userFamilyFirstName;
    }

    public String getUserFamilyLastName() {
        return userFamilyLastName;
    }

    public void setUserFamilyLastName(String userFamilyLastName) {
        this.userFamilyLastName = userFamilyLastName;
    }

    public Long getUserOrganisationId() {
        return userOrganisationId;
    }

    public void setUserOrganisationId(Long userOrganisationId) {
        this.userOrganisationId = userOrganisationId;
    }

    public String getUserOrganisationFirstName() {
        return userOrganisationFirstName;
    }

    public void setUserOrganisationFirstName(String userOrganisationFirstName) {
        this.userOrganisationFirstName = userOrganisationFirstName;
    }

    public String getUserOrganisationLastName() {
        return userOrganisationLastName;
    }

    public void setUserOrganisationLastName(String userOrganisationLastName) {
        this.userOrganisationLastName = userOrganisationLastName;
    }

    public List<DeviceAlertDTO> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<DeviceAlertDTO> alerts) {
        this.alerts = alerts;
    }

    public DeviceDTO id(Long id){
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceDTO deviceDTO = (DeviceDTO) o;
        if (deviceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), deviceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DeviceDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", uuid='" + getUuid() + "'" +
            ", deviceType='" + getDeviceType() + "'" +
            ", active='" + isActive() + "'" +
            ", startTimestamp=" + getStartTimestamp() +
            ", userExtra=" + getUserExtraId() +
            "}";
    }
}
