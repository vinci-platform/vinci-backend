package com.upb.vinci.repository;

import com.upb.vinci.domain.MobileAppSettings;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the MobileAppSettings entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MobileAppSettingsRepository extends JpaRepository<MobileAppSettings, Long>, JpaSpecificationExecutor<MobileAppSettings> {

    Optional<MobileAppSettings> findByNameAndUserId(String name, Long userId);
}
