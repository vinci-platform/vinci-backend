package com.upb.vinci.repository;

import com.upb.vinci.domain.HealthRecord;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HealthRecord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HealthRecordRepository extends JpaRepository<HealthRecord, Long>, JpaSpecificationExecutor<HealthRecord> {

}
