package com.upb.vinci.repository;

import com.upb.vinci.domain.DeviceAlert;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Spring Data  repository for the DeviceAlert entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeviceAlertRepository extends JpaRepository<DeviceAlert, Long>, JpaSpecificationExecutor<DeviceAlert> {

    Page<DeviceAlert> getDeviceAlertsBy(Pageable pageable);

}
