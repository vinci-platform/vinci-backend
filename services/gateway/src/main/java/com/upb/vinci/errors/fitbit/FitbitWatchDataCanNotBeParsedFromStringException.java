package com.upb.vinci.errors.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class FitbitWatchDataCanNotBeParsedFromStringException extends AbstractThrowableProblem {

    private static final String TITLE = "FITBIT_WATCH_DATA_CANT_BE_PARSED_FROM_STRING";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "FitbitWatchData cant be parsed from string!";
    private static final String DETAILS_WITH_DATA_STRING = "FitbitWatchData cant be parsed from string! Data: %s";

    public FitbitWatchDataCanNotBeParsedFromStringException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public FitbitWatchDataCanNotBeParsedFromStringException(String fitbitWatchDataString) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_DATA_STRING,fitbitWatchDataString)); }
    public FitbitWatchDataCanNotBeParsedFromStringException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "FitbitWatchDataCanNotBeParsedFromString{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
