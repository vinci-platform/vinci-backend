package com.upb.vinci.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A HealthRecord.
 */
@Entity
@Table(name = "health_record")
public class HealthRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "jhi_timestamp")
    private Long timestamp;

    @Column(name = "score")
    private Double score;

    @Column(name = "available_score")
    private Double availableScore;

    @Column(name = "steps_score")
    private Double stepsScore;

    @Column(name = "ipaq_score")
    private Double ipaqScore;

    @Column(name = "whoqol_score")
    private Double whoqolScore;

    @Column(name = "feelings_score")
    private Double feelingsScore;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public HealthRecord userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public HealthRecord timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getScore() {
        return score;
    }

    public HealthRecord score(Double score) {
        this.score = score;
        return this;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getAvailableScore() {
        return availableScore;
    }

    public HealthRecord availableScore(Double availableScore) {
        this.availableScore = availableScore;
        return this;
    }

    public void setAvailableScore(Double availableScore) {
        this.availableScore = availableScore;
    }

    public Double getStepsScore() {
        return stepsScore;
    }

    public HealthRecord stepsScore(Double stepsScore) {
        this.stepsScore = stepsScore;
        return this;
    }

    public void setStepsScore(Double stepsScore) {
        this.stepsScore = stepsScore;
    }

    public Double getIpaqScore() {
        return ipaqScore;
    }

    public HealthRecord ipaqScore(Double ipaqScore) {
        this.ipaqScore = ipaqScore;
        return this;
    }

    public void setIpaqScore(Double ipaqScore) {
        this.ipaqScore = ipaqScore;
    }

    public Double getWhoqolScore() {
        return whoqolScore;
    }

    public HealthRecord whoqolScore(Double whoqolScore) {
        this.whoqolScore = whoqolScore;
        return this;
    }

    public void setWhoqolScore(Double whoqolScore) {
        this.whoqolScore = whoqolScore;
    }

    public Double getFeelingsScore() {
        return feelingsScore;
    }

    public HealthRecord feelingsScore(Double feelingsScore) {
        this.feelingsScore = feelingsScore;
        return this;
    }

    public void setFeelingsScore(Double feelingsScore) {
        this.feelingsScore = feelingsScore;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HealthRecord healthRecord = (HealthRecord) o;
        if (healthRecord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), healthRecord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HealthRecord{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", timestamp=" + getTimestamp() +
            ", score=" + getScore() +
            ", availableScore=" + getAvailableScore() +
            ", stepsScore=" + getStepsScore() +
            ", ipaqScore=" + getIpaqScore() +
            ", whoqolScore=" + getWhoqolScore() +
            ", feelingsScore=" + getFeelingsScore() +
            "}";
    }
}
