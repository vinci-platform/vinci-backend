package com.upb.vinci.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A UserImage
 */
@Entity
@Table(name = "user_image")
public class UserImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    private byte[] image;

    @Column(name = "format")
    private String format;

    @ManyToOne
    @JoinColumn(name="user_extra_id")
    @JsonIgnoreProperties({"alerts", "images", "devices"})
    private UserExtra userExtra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public UserImage setImage(byte[] image) {
        this.image = image;
        return this;
    }

    public String getFormat() {
        return format;
    }

    public UserImage setFormat(String format) {
        this.format = format;
        return this;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public UserImage userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserImage userAlert = (UserImage) o;
        if (userAlert.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAlert.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserImage{" +
                "id=" + getId() +
                ", image='" + getImage() + "'" +
                ", format='" + getFormat() + "'" +
                "}";
    }
}
