package com.upb.vinci.service.dto;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EventRecord entity.
 */
public class EventRecordDTO implements Serializable {

    private Long id;

    private Long appId;

    @NotNull
    private Long userId;

    private Long timestamp;

    private Long created;

    private Long notify;

    private String repeat;

    private String title;

    private String type;

    private String text;

    private String data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getNotify() {
        return notify;
    }

    public void setNotify(Long notify) {
        this.notify = notify;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventRecordDTO eventRecordDTO = (EventRecordDTO) o;
        if (eventRecordDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventRecordDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventRecordDTO{" +
            "id=" + getId() +
            ", appId=" + getAppId() +
            ", userId=" + getUserId() +
            ", timestamp=" + getTimestamp() +
            ", created=" + getCreated() +
            ", notify=" + getNotify() +
            ", repeat='" + getRepeat() + "'" +
            ", title='" + getTitle() + "'" +
            ", type='" + getType() + "'" +
            ", text='" + getText() + "'" +
            ", data='" + getData() + "'" +
            "}";
    }
}
