package com.upb.vinci.service;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.service.dto.DeviceDTO;

import java.util.List;

/**
 * Service Interface for managing Device.
 */
public interface DeviceService {

    /**
     * Save a device.
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity
     */
    DeviceDTO justSave(DeviceDTO deviceDTO);
    DeviceDTO save(DeviceDTO deviceDTO);

    /**
     * Update a device.
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity
     */
    DeviceDTO justUpdate(DeviceDTO deviceDTO);
    DeviceDTO update(DeviceDTO deviceDTO);

    /**
     * Get the "id" device.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DeviceDTO findOne(Long id);

    /**
     * Get the "uuid" device.
     *
     * @param uuid the uuid of the entity
     * @return the entity
     */
    DeviceDTO findOneByUuid(String uuid);

    /**
     * Get all devices of a certain DeviceType
     *
     * @param deviceType the DeviceType of the entities
     * @return the list of entities
     */
    List<DeviceDTO> findAllByDeviceType(DeviceType deviceType);
    List<DeviceDTO> findAllByDeviceType(String deviceType);
    /**
     * Delete the "id" device.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<DeviceDTO> findAllByUserId(Long userId);
    List<DeviceDTO> findAllByUserExtraId(Long userExtraId);
    List<DeviceDTO> findAllByLogin(String userLogin);

    List<DeviceDTO> findAllByUserIdAndType(Long userId, DeviceType survey) throws Exception;
    List<DeviceDTO> findOneByUserIdAndType(Long userId, DeviceType survey) throws Exception;
    List<DeviceDTO> findAllByUserExtraIdAndDeviceType(Long userExtraId, DeviceType fitbitWatch) throws Exception;
    List<DeviceDTO> findOneByUserExtraIdAndDeviceType(Long userExtraId, DeviceType fitbitWatch) throws Exception;

    List<DeviceDTO> findAllByFamily(User user);

    List<DeviceDTO> findAllByOrganization(User user);

    void updateFitbitDevice(DeviceDTO deviceDTO);

    List<DeviceDTO> findOneSurveyDeviceByUserId(Long userId);
    List<DeviceDTO> findFitbitWatchDevicesByUserExtraId(Long userExtraId);

    List<DeviceDTO> getPersonalFitbitDeviceById(Long userExtraId);
}
