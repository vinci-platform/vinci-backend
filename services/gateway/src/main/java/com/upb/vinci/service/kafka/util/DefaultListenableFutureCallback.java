package com.upb.vinci.service.kafka.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
public class DefaultListenableFutureCallback implements ListenableFutureCallback<SendResult<String, Object>> {

    @Override
    public void onFailure(Throwable ex) {
        handleFailure(ex);
    }

    @Override
    public void onSuccess(SendResult<String, Object> result) {
        handleSuccess(result);
    }

    private void handleFailure(Throwable ex) {
        log.error("Error Sending the Message and the exception is {}",ex.getMessage());
        try {
            throw ex;
        } catch (Throwable throwable) {
            log.error("Error in OnFailure: {}", throwable.getMessage());
        }
    }

    private void handleSuccess(SendResult<String, Object> result) {
        log.debug("Message Sent Successfully for key : {} and value is {} , partitions is {},"
            ,result.getProducerRecord().key(),result.getProducerRecord().value(),result.getRecordMetadata().partition());
    }
}
