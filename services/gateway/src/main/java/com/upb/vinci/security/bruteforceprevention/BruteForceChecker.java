package com.upb.vinci.security.bruteforceprevention;

import com.upb.vinci.repository.UserBruteForceAttackDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@Slf4j
public class BruteForceChecker {

    private final int CHECKING_USER_LOGIN_STATUS_PERIOD = 60000;
    private final int REVERTING_USER_LOGIN_STATUS_TO_ZERO_PERIOD = 60000*10;

    private final UserBruteForceAttackDataRepository userBruteForceAttackDataRepository;

    public BruteForceChecker(UserBruteForceAttackDataRepository userBruteForceAttackDataRepository) {
        this.userBruteForceAttackDataRepository = userBruteForceAttackDataRepository;
    }

    @Scheduled(fixedDelay = CHECKING_USER_LOGIN_STATUS_PERIOD)
    public void enableLoginForUsers() {
        log.debug("Checking for users to enable login! Every {} milliseconds!", CHECKING_USER_LOGIN_STATUS_PERIOD);
        userBruteForceAttackDataRepository.findAllByLoginDisabledEquals(true).stream()
            .filter(ubfad -> ubfad.getLoginEnabledFrom().isBefore(Instant.now()))
            .forEach(ubfad -> {
                ubfad.setLoginDisabled(false);
                ubfad.setFailedLoginAttempts(0L);
                userBruteForceAttackDataRepository.save(ubfad);
            });
    }

    @Scheduled(fixedDelay = REVERTING_USER_LOGIN_STATUS_TO_ZERO_PERIOD)
    public void revertUserLoginAttemptsToZero() {
        log.debug("Checking for users to enable login! Every {} milliseconds!", CHECKING_USER_LOGIN_STATUS_PERIOD);
        userBruteForceAttackDataRepository.findAll()
            .forEach(ubfad -> {
                ubfad.setFailedLoginAttempts(0L);
                userBruteForceAttackDataRepository.save(ubfad);
            });
    }

}
