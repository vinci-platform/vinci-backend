package com.upb.vinci.domain.enumeration;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    WATCH, SHOE, CAMERA_FITNESS, CAMERA_MOVEMENT, SURVEY, FITBIT_WATCH
}
