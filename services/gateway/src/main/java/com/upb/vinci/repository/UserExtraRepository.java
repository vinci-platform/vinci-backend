package com.upb.vinci.repository;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UserExtra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserExtraRepository extends JpaRepository<UserExtra, Long> {

    @Query("select user_extra from UserExtra user_extra where user_extra.organization.login = ?#{principal.username}")
    List<UserExtra> findByOrganizationIsCurrentUser(Pageable pageable);

    @Query("select user_extra from UserExtra user_extra where user_extra.family.login = ?#{principal.username}")
    List<UserExtra> findByFamilyIsCurrentUser(Pageable pageable);

    Page<UserExtra> getUserExtraBy(Pageable pageable);

    UserExtra findByUserId(Long user_id);

    Optional<UserExtra> findOneByUuidIgnoreCase(String uuid);

    void deleteUserExtraByUserId(Long user_id);

    List<UserExtra> findByFamily(User user);

    List<UserExtra> findByOrganization(User user);

    Page<UserExtra> findByFamily(User family, Pageable pageable);

    Page<UserExtra> findByOrganization(User organization, Pageable pageable);
}
