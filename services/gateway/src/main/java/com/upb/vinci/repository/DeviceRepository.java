package com.upb.vinci.repository;

import com.upb.vinci.domain.Device;
import com.upb.vinci.domain.enumeration.DeviceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Device entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long>, JpaSpecificationExecutor<Device> {
    Page<Device> getDevicesBy(Pageable pageable);

    Optional<Device> findByUuid(String uuid);

    List<Device> findAllByDeviceTypeEquals(DeviceType deviceType);

    List<Device> findAllByUserExtraId(Long userExtraId);

    List<Device> findAllByUserExtraIdIn(List<Long> userExtraIdList);

    Optional<Device> findAllByUserExtraIdAndDeviceType(Long userExtraId, DeviceType type);
}
