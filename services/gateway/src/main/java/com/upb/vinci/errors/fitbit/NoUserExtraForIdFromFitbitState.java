package com.upb.vinci.errors.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserExtraForIdFromFitbitState extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_EXTRA_ID_FROM_FITBIT_STATE";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "There is no UserExtra for state from fitbit servers!";
    private static final String DETAILS_WITH_USER_EXTRA_ID = "There is no UserExtra for state from fitbit servers! userExtraId: %s";

    public NoUserExtraForIdFromFitbitState(){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS);
    }
    public NoUserExtraForIdFromFitbitState(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }
    public NoUserExtraForIdFromFitbitState(Long userExtraId) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,String.format(DETAILS_WITH_USER_EXTRA_ID,userExtraId)); }
    public NoUserExtraForIdFromFitbitState(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoUserExtraForIdFromFitbitState{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
