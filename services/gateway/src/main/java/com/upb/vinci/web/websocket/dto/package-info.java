/**
 * Data Access Objects used by WebSocket services.
 */
package com.upb.vinci.web.websocket.dto;
