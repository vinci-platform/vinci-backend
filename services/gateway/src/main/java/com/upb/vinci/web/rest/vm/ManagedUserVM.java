package com.upb.vinci.web.rest.vm;

import com.upb.vinci.service.dto.UserDTO;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    public static final String PASSWORD_REGEX = "(?=[A-Za-z0-9@#$%^&+!=-]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=-])(?=.{10,}).*$";
    public static final String PASSWORD_REGEX_MESSAGE = "Password needs to have at least 1 upper and lower case, number and special character!";

    @Pattern(regexp = PASSWORD_REGEX,message=PASSWORD_REGEX_MESSAGE)
    private String password;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ManagedUserVM{" +
            "} " + super.toString();
    }
}
