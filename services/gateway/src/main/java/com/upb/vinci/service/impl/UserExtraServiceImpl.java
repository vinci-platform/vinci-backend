package com.upb.vinci.service.impl;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.errors.NoUserExtraForUserIdException;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.AuthorizationService;
import com.upb.vinci.service.UserExtraService;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.UserExtraMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing UserExtra.
 */
@Service
@Transactional
public class UserExtraServiceImpl implements UserExtraService {

    private final String ENTITY_NAME = "userExtraEntity";

    private final Logger log = LoggerFactory.getLogger(UserExtraServiceImpl.class);

    private final UserExtraRepository userExtraRepository;

    private final UserExtraMapper userExtraMapper;

    private final AuthorizationService authorizationService;

    public UserExtraServiceImpl(UserExtraRepository userExtraRepository, UserExtraMapper userExtraMapper, @Lazy AuthorizationService authorizationService) {
        this.userExtraRepository = userExtraRepository;
        this.userExtraMapper = userExtraMapper;
        this.authorizationService = authorizationService;
    }

    /**
     * Just saves userExtraDto that is sent
     *
     * @param userExtraDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserExtraDTO justSave(UserExtraDTO userExtraDTO) {
        log.debug("Request to jsut save "+ENTITY_NAME+" : {}", userExtraDTO);
        UserExtra userExtra = userExtraMapper.toEntity(userExtraDTO);
        userExtra = userExtraRepository.saveAndFlush(userExtra);
        return userExtraMapper.toDto(userExtra);
    }

    /**
     * Just get optional of entity by id
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserExtraDTO> justFindOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" : {}", id);
        return userExtraRepository.findById(id)
            .map(userExtraMapper::toDto);
    }
    /**
     * Get one userExtra:
     * Admin can get any userExtra,
     * Organization and Family can get their userExtra and their pacients
     * Pacient can only get their userExtra
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserExtraDTO findOne(Long id) {
        log.debug("Request to findOne "+ENTITY_NAME+" by id: {}",id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserExtraDTO returnValueUserExtraDTO = userExtraRepository.findById(id)
            .map(userExtraMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return returnValueUserExtraDTO;
            case FAMILY: {
                if(returnValueUserExtraDTO.getUserId().equals(currentUser.getId()) ||
                    isCurrentUserAssociated(currentUser, returnValueUserExtraDTO.getFamilyId())) {
                    return returnValueUserExtraDTO;
                } else throw new AuthorizationException("You can only get yours or "+ENTITY_NAME+" of your associated users!");
            }
            case ORGANIZIATION: {
                if(returnValueUserExtraDTO.getUserId().equals(currentUser.getId()) ||
                    (isCurrentUserAssociated(currentUser, returnValueUserExtraDTO.getOrganizationId()))) {
                    return returnValueUserExtraDTO;
                } else throw new AuthorizationException("You can only get yours or "+ENTITY_NAME+" of your associated users!");
            }
            default: {
                if (returnValueUserExtraDTO.getUserId().equals(currentUser.getId())) return returnValueUserExtraDTO;
                else throw new AuthorizationException("You can only get yours "+ENTITY_NAME+"!");
            }
        }
    }

    private boolean isCurrentUserAssociated(User currentUser, Long associateUserId) {
        return associateUserId != null &&
            associateUserId.equals(currentUser.getId());
    }

    /**
     * Delete the userExtra by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);
        userExtraRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Page<UserExtraDTO> getAllManagedUserExtras(Pageable pageable) {
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return userExtraRepository.getUserExtraBy(pageable).map(userExtraMapper::toDto);
            case FAMILY: return userExtraRepository.findByFamily(currentUser, pageable).map(userExtraMapper::toDto);
            case ORGANIZIATION: return userExtraRepository.findByOrganization(currentUser, pageable).map(userExtraMapper::toDto);
            default: {
                List<UserExtraDTO> list = new ArrayList<UserExtraDTO>(){{
                    add(userExtraMapper.toDto(userExtraRepository.findByUserId(currentUser.getId())));
                }};
                return new PageImpl<>(list);
            }
        }
    }

    @Override
    public UserExtraDTO findByUserId(Long userId) {
        log.debug("Find "+ENTITY_NAME+" by userId: {}", userId);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserExtraDTO returnValueUserExtraDTO = userExtraMapper.toDto(userExtraRepository.findByUserId(userId));
        if (returnValueUserExtraDTO == null) {
            log.debug("No "+ENTITY_NAME+" found for userId: {}",userId);
            throw new NoUserExtraForUserIdException(userId);
        }

        switch (currentUsersAuthority) {
            case ADMIN: return returnValueUserExtraDTO;
            case FAMILY: {
                if(returnValueUserExtraDTO.getUserId().equals(currentUser.getId()) ||
                    (isCurrentUserAssociated(currentUser, returnValueUserExtraDTO.getFamilyId()))) return returnValueUserExtraDTO;
                throw new AuthorizationException("You can only get yours or "+ENTITY_NAME+" of your associated users!");
            }
            case ORGANIZIATION: {
                if (returnValueUserExtraDTO.getUserId().equals(currentUser.getId()) ||
                    (isCurrentUserAssociated(currentUser, returnValueUserExtraDTO.getOrganizationId()))) return returnValueUserExtraDTO;
                throw new AuthorizationException("You can only get yours or "+ENTITY_NAME+" of your associated users!");
            }
            default: {
                if (returnValueUserExtraDTO.getUserId().equals(currentUser.getId())) return returnValueUserExtraDTO;
                throw new AuthorizationException("You can only get yours "+ENTITY_NAME+"!");
            }
        }
    }

    /**
     * Admin can update anybody, Everybody else can update only their userExtra entity
     * @return UserExtraDto
     */
    @Override
    @Transactional
    public UserExtraDTO update(UserExtraDTO userExtraDtoToUpdate) {
        log.debug("Update "+ENTITY_NAME+": {}", userExtraDtoToUpdate);

        validateUserExtraDtoForUpdate(userExtraDtoToUpdate);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) ||
            currentUserExtraDto.getId().equals(userExtraDtoToUpdate.getId())){
                return userExtraMapper.toDto(userExtraRepository.save(userExtraMapper.toEntity(userExtraDtoToUpdate)));
        } else {
            throw new AuthorizationException(String.format("Only admin or user to whom the %s belongs can update the "+ENTITY_NAME+" entity. Use correct user in jwt token!",ENTITY_NAME));
        }
    }

    @Override
    public UserExtra createUserExtraForUser(User user) {
        UserExtra newUserExtra = new UserExtra();
        newUserExtra.setUser(user);
        newUserExtra.setUuid(UUID.randomUUID().toString());
        newUserExtra.setDescription("Automatically created "+ENTITY_NAME+"!");
        return newUserExtra;
    }

    private void validateUserExtraDtoForUpdate(UserExtraDTO userExtraDto) {
        if (userExtraDto.getId() == null) {
            throw new BadRequestAlertException("Id must not be null", ENTITY_NAME, "idnull");
        }
    }

}
