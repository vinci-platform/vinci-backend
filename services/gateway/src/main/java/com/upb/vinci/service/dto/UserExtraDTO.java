package com.upb.vinci.service.dto;

import com.upb.vinci.domain.enumeration.EducationType;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the UserExtra entity.
 */
public class UserExtraDTO implements Serializable {

    private Long id;

    private String uuid;

    private String phone;

    private String address;

    private Long userId;

    private Long organizationId;

    private Long familyId;

    private String userFirstName;

    private String userLastName;

    private String familyFirstName;

    private String familyLastName;

    private String organizationFirstName;

    private String organizationLastName;

    private String description;

    private String gender;

    private EducationType education;

    private String maritalStatus;

    private String login;

    private List<DeviceAlertDTO> alerts;

    private List<UserImageDTO> images;

    private List<DeviceDTO> devices;

    private String friends;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long userId) {
        this.organizationId = userId;
    }

    public Long getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Long userId) {
        this.familyId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getFamilyFirstName() {
        return familyFirstName;
    }

    public void setFamilyFirstName(String familyFirstName) {
        this.familyFirstName = familyFirstName;
    }

    public String getFamilyLastName() {
        return familyLastName;
    }

    public void setFamilyLastName(String familyLastName) {
        this.familyLastName = familyLastName;
    }

    public String getOrganizationFirstName() {
        return organizationFirstName;
    }

    public void setOrganizationFirstName(String organizationFirstName) {
        this.organizationFirstName = organizationFirstName;
    }

    public String getOrganizationLastName() {
        return organizationLastName;
    }

    public void setOrganizationLastName(String organizationLastName) {
        this.organizationLastName = organizationLastName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<DeviceAlertDTO> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<DeviceAlertDTO> alerts) {
        this.alerts = alerts;
    }

    public List<UserImageDTO> getImages() {
        return images;
    }

    public void setImages(List<UserImageDTO> images) {
        this.images = images;
    }

    public List<DeviceDTO> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceDTO> devices) {
        this.devices = devices;
    }

    public EducationType getEducation() { return education; }

    public void setEducation(EducationType education) { this.education = education; }

    public String getMaritalStatus() { return maritalStatus; }

    public void setMaritalStatus(String maritalStatus) { this.maritalStatus = maritalStatus; }

    public String getFriends() { return friends; }

    public void setFriends(String friends) {this.friends = friends; }

    public UserExtraDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtraDTO userExtraDTO = (UserExtraDTO) o;
        if (userExtraDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtraDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtraDTO{" +
            "id=" + getId() +
            ", phone='" + getPhone() + "'" +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
