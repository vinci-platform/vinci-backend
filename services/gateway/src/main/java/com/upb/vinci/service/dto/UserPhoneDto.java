package com.upb.vinci.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserPhoneDto implements Serializable {
    private Long userId;
    private String phone;
    private String firstName;
    private String lastName;

    public UserPhoneDto() {
    }

    public UserPhoneDto(Long userId, String phone, String firstName, String lastName) {
        this.userId = userId;
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPhoneDto that = (UserPhoneDto) o;
        return Objects.equals(userId, that.userId) && Objects.equals(phone, that.phone) && Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, phone, firstName, lastName);
    }

    @Override
    public String toString() {
        return "UserPhoneDto{" +
            "userId=" + userId +
            ", phone='" + phone + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }
}
