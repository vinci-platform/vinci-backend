package com.upb.vinci.service;

import com.upb.vinci.domain.EventRecord;
import com.upb.vinci.domain.EventRecord_;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.repository.EventRecordRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.dto.EventRecordCriteria;
import com.upb.vinci.service.dto.EventRecordDTO;
import com.upb.vinci.service.mapper.EventRecordMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for EventRecord entities in the database.
 * The main input is a {@link EventRecordCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EventRecordDTO} or a {@link Page} of {@link EventRecordDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EventRecordQueryService extends QueryService<EventRecord> {

    private static final String ENTITY_NAME = "eventRecord";

    private final Logger log = LoggerFactory.getLogger(EventRecordQueryService.class);

    private final EventRecordRepository eventRecordRepository;

    private final EventRecordMapper eventRecordMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public EventRecordQueryService(EventRecordRepository eventRecordRepository, EventRecordMapper eventRecordMapper, UserExtraRepository userExtraRepository,
                                   AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.eventRecordRepository = eventRecordRepository;
        this.eventRecordMapper = eventRecordMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link Page} of {@link EventRecordDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EventRecordDTO> findByCriteria(EventRecordCriteria criteria, Pageable page) {
        log.debug("find "+ENTITY_NAME+" by criteria : {}, page: {}", criteria, page);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        LongFilter userIdFilter = criteria.getUserId();
        List<Long> userIdList = new ArrayList<>();
        userIdList.add(currentUser.getId());//for getting user alerts for the user that is sending the request

        switch (currentUsersAuthority) {
            case ADMIN: {
                final Specification<EventRecord> specification = createSpecification(criteria);
                return eventRecordRepository.findAll(specification, page).map(eventRecordMapper::toDto);
            }
            case FAMILY: addUserIdsFromUsers(userIdList, userExtraRepository.findByFamily(currentUser));
            case ORGANIZIATION: addUserIdsFromUsers(userIdList, userExtraRepository.findByOrganization(currentUser));
        }
        criteria.setUserId(longFilterUtil.seIdFilter(userIdList, userIdFilter));
        final Specification<EventRecord> specification = createSpecification(criteria);
        return eventRecordRepository.findAll(specification, page)
            .map(eventRecordMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EventRecordCriteria criteria) {
        log.debug("count "+ENTITY_NAME+" by criteria : {}", criteria);
        final Specification<EventRecord> specification = createSpecification(criteria);
        return eventRecordRepository.count(specification);
    }

    /**
     * Function to convert EventRecordCriteria to a {@link Specification}
     */
    private Specification<EventRecord> createSpecification(EventRecordCriteria criteria) {
        Specification<EventRecord> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EventRecord_.id));
            }
            if (criteria.getAppId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAppId(), EventRecord_.appId));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserId(), EventRecord_.userId));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), EventRecord_.timestamp));
            }
            if (criteria.getCreated() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreated(), EventRecord_.created));
            }
            if (criteria.getNotify() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNotify(), EventRecord_.notify));
            }
            if (criteria.getRepeat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRepeat(), EventRecord_.repeat));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), EventRecord_.title));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getType(), EventRecord_.type));
            }
            if (criteria.getText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getText(), EventRecord_.text));
            }
            if (criteria.getData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getData(), EventRecord_.data));
            }
        }
        return specification;
    }

    private void addUserIdsFromUsers(List<Long> userIdList, List<UserExtra> userExtraList) {
        userExtraList
            .forEach(userExtra -> userIdList.add(userExtra.getUser().getId()));
    }
}
