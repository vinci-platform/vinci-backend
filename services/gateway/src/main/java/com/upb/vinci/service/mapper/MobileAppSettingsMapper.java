package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.MobileAppSettingsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MobileAppSettings and its DTO MobileAppSettingsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MobileAppSettingsMapper extends EntityMapper<MobileAppSettingsDTO, MobileAppSettings> {



    default MobileAppSettings fromId(Long id) {
        if (id == null) {
            return null;
        }
        MobileAppSettings mobileAppSettings = new MobileAppSettings();
        mobileAppSettings.setId(id);
        return mobileAppSettings;
    }
}
