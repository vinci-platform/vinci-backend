package com.upb.vinci.service.impl;

import com.google.common.base.Enums;
import com.upb.vinci.domain.Device;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.errors.*;
import com.upb.vinci.repository.DeviceRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.service.*;
import com.upb.vinci.service.dto.DeviceCriteria;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.DeviceMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import com.upb.vinci.web.rest.errors.InternalServerErrorException;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing Device.
 */
@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

    private static final String ENTITY_NAME = "device";

    private final Logger log = LoggerFactory.getLogger(DeviceServiceImpl.class);

    private final DeviceRepository deviceRepository;

    private final DeviceMapper deviceMapper;

    private final UserRepository userRepository;

    private final UserExtraRepository userExtraRepository;

    private final DeviceQueryService deviceQueryService;

    private final AuthorizationService authorizationService;

    public DeviceServiceImpl(DeviceRepository deviceRepository, DeviceMapper deviceMapper, UserRepository userRepository,
                             UserExtraRepository userExtraRepository, @Lazy DeviceQueryService deviceQueryService, AuthorizationService authorizationService) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
        this.userRepository = userRepository;
        this.userExtraRepository = userExtraRepository;
        this.deviceQueryService = deviceQueryService;
        this.authorizationService = authorizationService;
    }

    /**
     * Save a device.
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeviceDTO justSave(DeviceDTO deviceDTO) {
        log.debug("Request to save Device : {}", deviceDTO);
        Device device = deviceMapper.toEntity(deviceDTO);
        device = deviceRepository.save(device);
        return deviceMapper.toDto(device);
    }

    /**
     * Save a device.
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity or null if there is exception
     */
    @Override
    public DeviceDTO save(DeviceDTO deviceDTO) {
        log.debug("Request to save Device : {}", deviceDTO);
        validateSaveDevice(deviceDTO);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
            case FAMILY: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByFamily(currentUser))) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can save "+ENTITY_NAME+" entity only for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can save "+ENTITY_NAME+" entity only for your user or users associated to you!");
            }
            default: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId())) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can save "+ENTITY_NAME+" entity only for your user!");
            }
        }
    }

    /**
     * Update a device if userExtraId of deviceDto is equal userExtraId to the existing device in database
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeviceDTO justUpdate(DeviceDTO deviceDTO) {
        log.debug("Request to update Device : {}", deviceDTO);
        Optional<DeviceDTO> existingDevice = deviceRepository.findById(deviceDTO.getId()).map(deviceMapper::toDto);
        if (existingDevice.isPresent()) {
            // if device already exists
            if (!deviceDTO.getUserExtraId().equals(existingDevice.get().getUserExtraId())) {
                // if the device belonged to another user, reset the startTimestamp
                deviceDTO.setStartTimestamp(Instant.now());
            }
        }
        return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
    }

    /**
     * Update a device.
     *
     * @param deviceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeviceDTO update(DeviceDTO deviceDTO) {
        log.debug("Request to update Device : {}", deviceDTO);
        validateUpdateDevice(deviceDTO);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        Device deviceInDb = deviceRepository.findById(deviceDTO.getId())
            .orElseThrow(() -> new NoEntityException(deviceDTO.getId(),ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
            case FAMILY: {
                if (deviceInDb.getUserExtra().getId().equals(currentUserExtraDto.getId())
                    || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByFamily(currentUser))) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can update "+ENTITY_NAME+" entity only for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (deviceInDb.getUserExtra().getId().equals(currentUserExtraDto.getId())
                    || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can update "+ENTITY_NAME+" entity only for your user or users associated to you!");
            }
            default: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId())) {
                    return deviceMapper.toDto(deviceRepository.save(deviceMapper.toEntity(deviceDTO)));
                } throw new AuthorizationException("You can update "+ENTITY_NAME+" entity only for your user!");
            }
        }
    }

    /**
     * Get one device by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DeviceDTO findOne(Long id) {
        log.debug("Request to get Device by id: {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        DeviceDTO deviceDTO = deviceRepository.findById(id)
            .map(deviceMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return deviceDTO;
            case FAMILY: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId()) || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByFamily(currentUser)))
                    return deviceDTO;
            }
            case ORGANIZIATION: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId()) || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByOrganization(currentUser)))
                    return deviceDTO;
            }
            default: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId()))
                    return deviceDTO;
            }
        }
        throw new AuthorizationException("You dont have permission to get" + ENTITY_NAME);
    }

    /**
     * Get the "uuid" device.
     *
     * @param uuid the uuid of the entity
     * @return the entity
     */
    @Override
    public DeviceDTO findOneByUuid(String uuid) {
        log.debug("Request to get Device by uuid: {}", uuid);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        DeviceDTO deviceDTO = deviceRepository.findByUuid(uuid).map(deviceMapper::toDto)
            .orElseThrow(() -> new NoEntityException(uuid,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return deviceDTO;
            case FAMILY: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId()) || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByFamily(currentUser)))
                    return deviceDTO;
            }
            case ORGANIZIATION: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId()) || isDevceAssociatedToUsers(deviceDTO, userExtraRepository.findByOrganization(currentUser)))
                    return deviceDTO;
            }
            default: {
                if (deviceDTO.getUserExtraId().equals(currentUserExtraDto.getId())) return deviceDTO;
            }
        }
        throw new AuthorizationException("You dont have permission to get" + ENTITY_NAME);
    }

    /**
     * Get all devices of a certain DeviceType
     *
     * @param deviceType the DeviceType of the entities
     * @return the list of entities
     */
    @Override
    public List<DeviceDTO> findAllByDeviceType(DeviceType deviceType)  {
        log.debug("Request to get all Device of type: {}", deviceType);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: {
                return deviceRepository.findAllByDeviceTypeEquals(deviceType).stream()
                    .map(deviceMapper::toDto)
                    .collect(Collectors.toCollection(LinkedList::new));
            }
            case FAMILY: {
                List<Long> associatedUserExtraIds = getUserExtraIdsFromList(userExtraRepository.findByFamily(currentUser));
                associatedUserExtraIds.add(currentUserExtraDto.getId());//add current user
                return getDevicesUsingCriteria(deviceType, associatedUserExtraIds);
            }
            case ORGANIZIATION: {
                List<Long> associatedUserExtraIds = getUserExtraIdsFromList(userExtraRepository.findByOrganization(currentUser));
                associatedUserExtraIds.add(currentUserExtraDto.getId());//add current user
                return getDevicesUsingCriteria(deviceType, associatedUserExtraIds);
            }
            default: {
                List<Long> associatedUserExtraIds = Collections.singletonList(currentUserExtraDto.getId());
                return getDevicesUsingCriteria(deviceType, associatedUserExtraIds);
            }
        }
    }

    @Override
    public List<DeviceDTO> findAllByDeviceType(String deviceType) {
        DeviceType type = Enums.getIfPresent(DeviceType.class, deviceType.toUpperCase()).orNull();
        if (type == null) {
            log.debug("Incorrect device type: {} in request.", deviceType);
            throw new IncorrectDeviceTypeException(deviceType);
        }
        return findAllByDeviceType(type);
    }

    /**
     * Delete the device by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Device : {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());

        Device deviceById = deviceRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) ||
            deviceById.getUserExtra().getId().equals(currentUserExtraDto.getId())) {
            deviceRepository.deleteById(id);
        } else {
            throw new AuthorizationException(ENTITY_NAME + " can be deleted by user to whom it belongs to or by admin! Use correct users jwt token!");
        }
    }

    @Override
    public List<DeviceDTO> findAllByUserId(Long userId) {
        log.debug("Request to find all devices by user id {}", userId);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserExtra userExtra = userExtraRepository.findByUserId(userId);
        if (userExtra == null) {
            log.debug("No UserExtra by userId: " + userId);
            throw new NoEntityException("No UserExtra by userId: " + userId);
        }
        switch (currentUsersAuthority) {
            case ADMIN: return getDeviceDtosByUserExtraId(userExtra.getId());
            case FAMILY: {
                List<Long> associatedUserExtraIds = getUserExtraIdsFromList(userExtraRepository.findByFamily(currentUser));
                associatedUserExtraIds.add(currentUserExtraDto.getId());//add current user
                return getDevicesForAssociatedUserExtraIds(userExtra, associatedUserExtraIds);
            }
            case ORGANIZIATION: {
                List<Long> associatedUserExtraIds = getUserExtraIdsFromList(userExtraRepository.findByOrganization(currentUser));
                associatedUserExtraIds.add(currentUserExtraDto.getId());//add current user
                return getDevicesForAssociatedUserExtraIds(userExtra, associatedUserExtraIds);
            }
            default: {
                List<Long> associatedUserExtraIds = Collections.singletonList(userExtra.getId());
                return getDevicesForAssociatedUserExtraIds(userExtra, associatedUserExtraIds);
            }
        }
    }

    private List<DeviceDTO> getDevicesForAssociatedUserExtraIds(UserExtra userExtra, List<Long> associatedUserExtraIds) {
        return associatedUserExtraIds.contains(userExtra.getId()) ?
            getDeviceDtosByUserExtraId(userExtra.getId()) : Collections.emptyList();
    }

    @Override
    public List<DeviceDTO> findAllByUserExtraId(Long userExtraId) {
        log.debug("Request to find all devices by userExtraId {}", userExtraId);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        userExtraRepository.findById(userExtraId)
            .orElseThrow(() -> new NoEntityException(userExtraId,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return getDeviceDtosByUserExtraId(userExtraId);
            case FAMILY: {
                if (currentUserExtraDto.getId().equals(userExtraId)
                    || isUserExtraListAssociatedToUserExtraId(userExtraId, userExtraRepository.findByFamily(currentUser))) {
                    return getDeviceDtosByUserExtraId(userExtraId);
                }
            }
            case ORGANIZIATION: {
                if (currentUserExtraDto.getId().equals(userExtraId)
                    || isUserExtraListAssociatedToUserExtraId(userExtraId, userExtraRepository.findByOrganization(currentUser))) {
                    return getDeviceDtosByUserExtraId(userExtraId);
                }
            }
            case PACIENT: {
                if (currentUserExtraDto.getId().equals(userExtraId)) {
                    return getDeviceDtosByUserExtraId(userExtraId);
                }
            }
            default: return Collections.emptyList();
        }
    }

    @Override
    public List<DeviceDTO> findAllByLogin(String userLogin) {
        log.debug("Request to find all devices by userLogin {}", userLogin);

        UserExtra userExtraForLogin = getUserExtraForLogin(userLogin);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return getDeviceDtosByUserExtraId(userExtraForLogin.getId());
            case FAMILY: {
                List<Long> familyMembersIds = getUserExtraIdsFromList(userExtraRepository.findByFamily(currentUser));
                familyMembersIds.add(currentUserExtraDto.getId());//add current user
                if (familyMembersIds.contains(userExtraForLogin.getId())) {
                    return getDeviceDtosByUserExtraId(userExtraForLogin.getId());
                }
            }
            case ORGANIZIATION: {
                List<Long> organizationMembersIds = getUserExtraIdsFromList(userExtraRepository.findByOrganization(currentUser));
                organizationMembersIds.add(currentUserExtraDto.getId());//add current user
                if (organizationMembersIds.contains(userExtraForLogin.getId())) {
                    return getDeviceDtosByUserExtraId(userExtraForLogin.getId());
                }
            }
            case PACIENT: {
                if (currentUserExtraDto.getId().equals(userExtraForLogin.getId())) {
                    return getDeviceDtosByUserExtraId(userExtraForLogin.getId());
                }
            }
            default: return Collections.emptyList();
        }
    }

    private UserExtra getUserExtraForLogin(String userLogin) {
        User userFromLogin = userRepository.findOneByLogin(userLogin)
            .orElseThrow(() -> new NoUserForLoginException(userLogin));
        return Optional.ofNullable(userExtraRepository.findByUserId(userFromLogin.getId()))
            .orElseThrow(() -> new InternalServerErrorException("UserExtra doesn't exist for userId: " + userFromLogin.getId()));
    }

    @Override
    public List<DeviceDTO> findAllByUserIdAndType(Long userId, DeviceType type) {
        return findAllByUserId(userId)
            .stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(type))
            .collect(Collectors.toList());
    }

    @Override
    public List<DeviceDTO> findOneByUserIdAndType(Long userId, DeviceType type) {
        UserExtra userExtraByUserId = userExtraRepository.findByUserId(userId);
        if (userExtraByUserId == null) {
            log.debug("No UserExtra by userId: " + userId);
            throw new NoEntityException("No UserExtra by userId: " + userId);
        }
        return findOneByUserExtraIdAndDeviceType(userExtraByUserId.getId(),type);
    }

    @Override
    public List<DeviceDTO> findAllByUserExtraIdAndDeviceType(Long userExtraId, DeviceType type) {
        return findAllByUserExtraId(userExtraId)
            .stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(type))
            .collect(Collectors.toList());
    }

    @Override
    public List<DeviceDTO> findOneByUserExtraIdAndDeviceType(Long userExtraId, DeviceType type) {
        return findAllByUserExtraId(userExtraId)
            .stream()
            .filter(deviceDTO -> deviceDTO.getDeviceType().equals(type) && deviceDTO.getUserExtraId().equals(userExtraId))
            .collect(Collectors.toList());
    }

    @Override
    public List<DeviceDTO> findAllByFamily(User user) {
        return getDevicesFromUserExtraList(userExtraRepository.findByFamily(user));
    }

    @Override
    public List<DeviceDTO> findAllByOrganization(User user) {
        return getDevicesFromUserExtraList(userExtraRepository.findByOrganization(user));
    }

    @Override
    public void updateFitbitDevice(DeviceDTO deviceDTO) {
        if (!deviceDTO.getDeviceType().equals(DeviceType.FITBIT_WATCH)) {
            throw new IncorrectDeviceTypeException(deviceDTO.getDeviceType());
        }
        this.justSave(deviceDTO);
    }

    @Override
    public List<DeviceDTO> findOneSurveyDeviceByUserId(Long userId) {
        log.debug("Request to get Survey Devices by userId: {}", userId);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtra currentUserExtra = authorizationService.getCurrentUserExtra_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserExtra targetUserExtra = userExtraRepository.findByUserId(userId);
        if (targetUserExtra == null) throw new NoUserExtraForUserIdException(userId);

        List<DeviceDTO> returnValue = getDeviceByUserExtraAndType(targetUserExtra, DeviceType.SURVEY, this::createSurveyNewDeviceWithUserExtra);

        switch (currentUsersAuthority) {
            case ADMIN: return returnValue;
            case FAMILY: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())
                    || isUserExtraListAssociatedToUserExtraId(targetUserExtra.getId(), userExtraRepository.findByFamily(currentUser))) {
                    return returnValue;
                } throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())
                    || isUserExtraListAssociatedToUserExtraId(targetUserExtra.getId(), userExtraRepository.findByOrganization(currentUser))) {
                    return returnValue;
                } throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            default: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())) {
                    return returnValue;
                } throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user");
            }
        }
    }

    @Override
    public List<DeviceDTO> findFitbitWatchDevicesByUserExtraId(Long userExtraId) {
        log.debug("Request to get Fitbit Devices by userExtraId: {}", userExtraId);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtra currentUserExtra = authorizationService.getCurrentUserExtra_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserExtra targetUserExtra = userExtraRepository.findById(userExtraId)
            .orElseThrow(() -> new NoUserExtraForUserIdException(userExtraId));

        List<DeviceDTO> returnValue = getDeviceByUserExtraAndType(targetUserExtra, DeviceType.FITBIT_WATCH, this::createFitbitWatchNewDeviceWithUserExtra);

        switch (currentUsersAuthority) {
            case ADMIN: return returnValue;
            case FAMILY: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())
                    || isUserExtraListAssociatedToUserExtraId(targetUserExtra.getId(), userExtraRepository.findByFamily(currentUser))) {
                    return returnValue;
                }throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())
                    || isUserExtraListAssociatedToUserExtraId(targetUserExtra.getId(), userExtraRepository.findByOrganization(currentUser))) {
                    return returnValue;
                }throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            default: {
                if (currentUserExtra.getId().equals(targetUserExtra.getId())) {
                    return returnValue;
                } throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    private List<DeviceDTO> getDeviceByUserExtraAndType(UserExtra userExtra, DeviceType deviceType, Function<UserExtra, DeviceDTO> createAlternativeDevice) {
        Optional<Device> deviceDto = deviceRepository.findAllByUserExtraIdAndDeviceType(userExtra.getId(), deviceType);
        if (!deviceDto.isPresent()) {
            deviceDto = Optional.of(deviceMapper.toEntity(createAlternativeDevice.apply(userExtra)));
        }

        DeviceDTO fitbitDeviceDto = deviceDto.map(deviceMapper::toDto).get();
        List<DeviceDTO> returnValue = new ArrayList<>();
        returnValue.add(fitbitDeviceDto);
        return returnValue;
    }

    @Override
    public List<DeviceDTO> getPersonalFitbitDeviceById(Long userExtraId){
        return deviceRepository.findAllByUserExtraId(userExtraId)
            .stream()
            .filter(device -> device.getDeviceType().equals(DeviceType.FITBIT_WATCH))
            .map(deviceMapper::toDto)
            .collect(Collectors.toList());
    }

    private boolean isUserExtraListAssociatedToUserExtraId(Long userExtraId, List<UserExtra> userExtraList) {
        return userExtraList
            .stream()
            .map(UserExtra::getId)
            .anyMatch(UE_ID -> UE_ID.equals(userExtraId));
    }

    private List<DeviceDTO> getDevicesUsingCriteria(DeviceType deviceType, List<Long> associatedUserExtraIds) {
        DeviceCriteria criteria = new DeviceCriteria();
        LongFilter userExtraIdListFilter = new LongFilter();
        userExtraIdListFilter.setIn(associatedUserExtraIds);
        criteria.setUserExtraId(userExtraIdListFilter);
        DeviceCriteria.DeviceTypeFilter deviceTypeFilter = new DeviceCriteria.DeviceTypeFilter();
        deviceTypeFilter.setEquals(deviceType);
        criteria.setDeviceType(deviceTypeFilter);
        return deviceQueryService.findByCriteria(criteria);
    }

    private boolean isDevceAssociatedToUsers(DeviceDTO deviceDTO, List<UserExtra> byFamily) {
        return byFamily
            .stream()
            .anyMatch(userExtra -> userExtra.getId().equals(deviceDTO.getUserExtraId()));
    }

    private DeviceDTO createSurveyNewDeviceWithUserExtra(UserExtra userExtra) {
        Optional<Device> surveyInDatabaseForUserExtra = checkIfSurveyAlreadyExists(userExtra);
        if (surveyInDatabaseForUserExtra.isPresent()){
            log.error("There is already survey device for user when calling DeviceService#createSurveyNewDeviceWithUserExtra(UserExtra)");
            return deviceMapper.toDto(surveyInDatabaseForUserExtra.get());
        }
        Device surveyDevice = Device.builder()
            .deviceType(DeviceType.SURVEY)
            .active(true)
            .userExtra(userExtra)
            .name("Survey device")
            .description("Add description...")
            .uuid(UUID.randomUUID().toString())
            .startTimestamp(Instant.now())
            .alerts(new HashSet<>()).build();
        Device device = deviceRepository.saveAndFlush(surveyDevice);
        log.debug("Saved survey device: " + device);
        return deviceMapper.toDto(device);
    }

    private DeviceDTO createFitbitWatchNewDeviceWithUserExtra(UserExtra userExtra) {
        Device fitbitWatchDevice = Device.builder()
            .deviceType(DeviceType.FITBIT_WATCH)
            .active(false)
            .userExtra(userExtra)
            .name("Fitbit Watch Device")
            .description("Not Connected to fitbit watch!")
            .uuid("n/a")
            .startTimestamp(Instant.now())
            .alerts(new HashSet<>()).build();
        Device device = deviceRepository.save(fitbitWatchDevice);
        log.debug("Saved fitbit watch device: " + device);
        return deviceMapper.toDto(device);
    }

    private List<Long> getUserExtraIdsFromList(List<UserExtra> byFamily) {
        return byFamily
            .stream()
            .map(UserExtra::getId)
            .collect(Collectors.toList());
    }

    private List<DeviceDTO> getDevicesFromUserExtraList(List<UserExtra> userExtraList) {
        return userExtraList.stream()
            .map(UserExtra::getDevices)
            .flatMap(Collection::stream)
            .map(deviceMapper::toDto)
            .collect(Collectors.toList());
    }

    private Optional<Device> checkIfSurveyAlreadyExists(UserExtra userExtra) {
        return deviceRepository.findAllByUserExtraId(userExtra.getId())
            .stream()
            .filter(device -> device.getDeviceType().equals(DeviceType.SURVEY))
            .findFirst();
    }

    private void validateSaveDevice(DeviceDTO deviceDTO) {
        if (deviceDTO.getId() != null) {
            throw new BadRequestAlertException("A new device cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (deviceDTO.getUserExtraId() == null) {
            throw new BadRequestAlertException("UserExtraId must be present!", ENTITY_NAME, "userExtraIdIsNull");
        }
    }

    private void validateUpdateDevice(DeviceDTO deviceDTO) {
        if (deviceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (deviceDTO.getUserExtraId() == null) {
            throw new BadRequestAlertException("UserExtraId must be present!", ENTITY_NAME, "userExtraIdIsNull");
        }
    }

    private List<DeviceDTO> getDeviceDtosByUserExtraId(Long id) {
        return deviceRepository.findAllByUserExtraId(id).stream().map(deviceMapper::toDto).collect(Collectors.toList());
    }
}
