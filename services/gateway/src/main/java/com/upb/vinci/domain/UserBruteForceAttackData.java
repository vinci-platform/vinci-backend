package com.upb.vinci.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "user_brute_force_attack_data")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserBruteForceAttackData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "failed_login_attempts")
    private Long failedLoginAttempts;

    @Column(name = "login_disabled")
    private Boolean loginDisabled;

    @Column(name = "login_enabled_from")
    private Instant loginEnabledFrom;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;
}
