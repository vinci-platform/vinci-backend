package com.upb.vinci.web.rest;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.DeviceAlertQueryService;
import com.upb.vinci.service.DeviceAlertService;
import com.upb.vinci.service.dto.DeviceAlertCriteria;
import com.upb.vinci.service.dto.DeviceAlertDTO;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing DeviceAlert.
 */
@RestController
@RequestMapping("/api")
public class DeviceAlertResource {

    private final Logger log = LoggerFactory.getLogger(DeviceAlertResource.class);

    private static final String ENTITY_NAME = "deviceAlert";

    private final DeviceAlertService deviceAlertService;

    private final DeviceAlertQueryService deviceAlertQueryService;

    public DeviceAlertResource(DeviceAlertService deviceAlertService, DeviceAlertQueryService deviceAlertQueryService) {
        this.deviceAlertService = deviceAlertService;
        this.deviceAlertQueryService = deviceAlertQueryService;
    }

    /**
     * POST  /device-alerts : Create a new deviceAlert.
     *
     * @param deviceAlertDTO the deviceAlertDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deviceAlertDTO, or with status 400 (Bad Request) if the deviceAlert has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/device-alerts")
    public ResponseEntity<DeviceAlertDTO> createDeviceAlert(@Valid @RequestBody DeviceAlertDTO deviceAlertDTO) throws Exception {
        log.debug("REST request to save DeviceAlert : {}", deviceAlertDTO);
        DeviceAlertDTO result = deviceAlertService.save(deviceAlertDTO);
        return ResponseEntity.created(new URI("/api/device-alerts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /device-alerts : Updates an existing deviceAlert.
     *
     * @param deviceAlertDTO the deviceAlertDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deviceAlertDTO,
     * or with status 400 (Bad Request) if the deviceAlertDTO is not valid,
     * or with status 500 (Internal Server Error) if the deviceAlertDTO couldn't be updated
     */
    @PutMapping("/device-alerts")
    public ResponseEntity<DeviceAlertDTO> updateDeviceAlert(@Valid @RequestBody DeviceAlertDTO deviceAlertDTO) {
        log.debug("REST request to update DeviceAlert : {}", deviceAlertDTO);
        DeviceAlertDTO result = deviceAlertService.update(deviceAlertDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deviceAlertDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /device-alerts : get all the deviceAlerts.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of deviceAlerts in body
     */
    @GetMapping("/device-alerts")
    public ResponseEntity<List<DeviceAlertDTO>> getAllDeviceAlerts(DeviceAlertCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DeviceAlerts by criteria: {}", criteria);
        Page<DeviceAlertDTO> page = deviceAlertQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/device-alerts");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /device-alerts/:id : get the "id" deviceAlert.
     *
     * @param id the id of the deviceAlertDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deviceAlertDTO, or with status 404 (Not Found)
     */
    @GetMapping("/device-alerts/{id}")
    public ResponseEntity<DeviceAlertDTO> getDeviceAlert(@PathVariable Long id) {
        log.debug("REST request to get DeviceAlert by id: {}", id);
        return ResponseEntity.ok(deviceAlertService.findOne(id));
    }

    /**
     * DELETE  /device-alerts/:id : delete the "id" deviceAlert.
     *
     * @param id the id of the deviceAlertDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/device-alerts/{id}")
    public ResponseEntity<Void> deleteDeviceAlert(@PathVariable Long id) {
        log.debug("REST request to delete DeviceAlert : {}", id);
        deviceAlertService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /device-alerts/count : count all the deviceAlerts.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count in body
     */
    @GetMapping("/device-alerts/count")
    public ResponseEntity<Long> countDeviceAlerts(DeviceAlertCriteria criteria) {
        log.debug("REST request to count DeviceAlerts by criteria: {}", criteria);
        return ResponseEntity.ok().body(deviceAlertQueryService.countByCriteria(criteria));
    }
}
