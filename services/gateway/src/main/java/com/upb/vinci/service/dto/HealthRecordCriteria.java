package com.upb.vinci.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the HealthRecord entity. This class is used in HealthRecordResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /health-records?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class HealthRecordCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter userId;

    private LongFilter timestamp;

    private DoubleFilter score;

    private DoubleFilter availableScore;

    private DoubleFilter stepsScore;

    private DoubleFilter ipaqScore;

    private DoubleFilter whoqolScore;

    private DoubleFilter feelingsScore;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LongFilter timestamp) {
        this.timestamp = timestamp;
    }

    public DoubleFilter getScore() {
        return score;
    }

    public void setScore(DoubleFilter score) {
        this.score = score;
    }

    public DoubleFilter getAvailableScore() {
        return availableScore;
    }

    public void setAvailableScore(DoubleFilter availableScore) {
        this.availableScore = availableScore;
    }

    public DoubleFilter getStepsScore() {
        return stepsScore;
    }

    public void setStepsScore(DoubleFilter stepsScore) {
        this.stepsScore = stepsScore;
    }

    public DoubleFilter getIpaqScore() {
        return ipaqScore;
    }

    public void setIpaqScore(DoubleFilter ipaqScore) {
        this.ipaqScore = ipaqScore;
    }

    public DoubleFilter getWhoqolScore() {
        return whoqolScore;
    }

    public void setWhoqolScore(DoubleFilter whoqolScore) {
        this.whoqolScore = whoqolScore;
    }

    public DoubleFilter getFeelingsScore() {
        return feelingsScore;
    }

    public void setFeelingsScore(DoubleFilter feelingsScore) {
        this.feelingsScore = feelingsScore;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HealthRecordCriteria that = (HealthRecordCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(timestamp, that.timestamp) &&
            Objects.equals(score, that.score) &&
            Objects.equals(availableScore, that.availableScore) &&
            Objects.equals(stepsScore, that.stepsScore) &&
            Objects.equals(ipaqScore, that.ipaqScore) &&
            Objects.equals(whoqolScore, that.whoqolScore) &&
            Objects.equals(feelingsScore, that.feelingsScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        userId,
        timestamp,
        score,
        availableScore,
        stepsScore,
        ipaqScore,
        whoqolScore,
        feelingsScore
        );
    }

    @Override
    public String toString() {
        return "HealthRecordCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (score != null ? "score=" + score + ", " : "") +
                (availableScore != null ? "availableScore=" + availableScore + ", " : "") +
                (stepsScore != null ? "stepsScore=" + stepsScore + ", " : "") +
                (ipaqScore != null ? "ipaqScore=" + ipaqScore + ", " : "") +
                (whoqolScore != null ? "whoqolScore=" + whoqolScore + ", " : "") +
                (feelingsScore != null ? "feelingsScore=" + feelingsScore + ", " : "") +
            "}";
    }

}
