package com.upb.vinci.service;

import com.upb.vinci.domain.*;
import com.upb.vinci.repository.UserAlertRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.dto.UserAlertCriteria;
import com.upb.vinci.service.dto.UserAlertDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.UserAlertMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for UserAlert entities in the database.
 * The main input is a {@link UserAlertCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserAlertDTO} or a {@link Page} of {@link UserAlertDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserAlertQueryService extends QueryService<UserAlert> {

    private final Logger log = LoggerFactory.getLogger(UserAlertQueryService.class);

    private final UserAlertRepository userAlertRepository;

    private final UserAlertMapper userAlertMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public UserAlertQueryService(UserAlertRepository userAlertRepository, UserAlertMapper userAlertMapper,
                                 UserExtraRepository userExtraRepository, AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.userAlertRepository = userAlertRepository;
        this.userAlertMapper = userAlertMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link Page} of {@link UserAlertDTO} which matches the criteria from the database
     * Admin can get all data, patient only his, family and organization can get data from associated users
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserAlertDTO> findByCriteria(UserAlertCriteria criteria, Pageable page) {
        log.debug("find by criteria: {}, page: {}", criteria, page);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        LongFilter userExtraIdFilter = criteria.getUserExtraId();
        List<Long> userExtraIdList = new ArrayList<>();
        userExtraIdList.add(currentUserExtraDto.getId());//for getting user alerts for the user that is sending the request

        switch (currentUsersAuthority) {
            case ADMIN: {
                final Specification<UserAlert> specification = createSpecification(criteria);
                return userAlertRepository.findAll(specification, page)
                    .map(userAlertMapper::toDto);
            }
            case FAMILY: addUserIdsFromUsers(userExtraIdList, userExtraRepository.findByFamily(currentUser));
            case ORGANIZIATION:addUserIdsFromUsers(userExtraIdList, userExtraRepository.findByOrganization(currentUser));
        }
        criteria.setUserExtraId(longFilterUtil.seIdFilter(userExtraIdList, userExtraIdFilter));
        final Specification<UserAlert> specification = createSpecification(criteria);
        return userAlertRepository.findAll(specification, page)
            .map(userAlertMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserAlertCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserAlert> specification = createSpecification(criteria);
        return userAlertRepository.count(specification);
    }

    /**
     * Function to convert UserAlertCriteria to a {@link Specification}
     */
    private Specification<UserAlert> createSpecification(UserAlertCriteria criteria) {
        Specification<UserAlert> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserAlert_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), UserAlert_.label));
            }
            if (criteria.getValues() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValues(), UserAlert_.values));
            }
            if (criteria.getAlertType() != null) {
                specification = specification.and(buildSpecification(criteria.getAlertType(), UserAlert_.alertType));
            }
            if (criteria.getUserRead() != null) {
                specification = specification.and(buildSpecification(criteria.getUserRead(), UserAlert_.userRead));
            }
            if (criteria.getFamilyRead() != null) {
                specification = specification.and(buildSpecification(criteria.getFamilyRead(), UserAlert_.familyRead));
            }
            if (criteria.getOrganizationRead() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganizationRead(), UserAlert_.organizationRead));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),UserAlert_.createdDate));
            }
            if (criteria.getUserExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserExtraId(),
                    root -> root.join(UserAlert_.userExtra, JoinType.LEFT).get(UserExtra_.id)));
            }
        }
        return specification;
    }

    private void addUserIdsFromUsers(List<Long> userExtraIdList, List<UserExtra> userExtraList) {
        userExtraList
            .forEach(userExtra -> userExtraIdList.add(userExtra.getId()));
    }
}
