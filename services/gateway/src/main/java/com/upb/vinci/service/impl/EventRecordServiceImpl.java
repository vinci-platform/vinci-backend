package com.upb.vinci.service.impl;

import com.upb.vinci.domain.EventRecord;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.repository.EventRecordRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.AuthorizationService;
import com.upb.vinci.service.EventRecordService;
import com.upb.vinci.service.dto.EventRecordDTO;
import com.upb.vinci.service.mapper.EventRecordMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing EventRecord.
 */
@Service
@Transactional
public class EventRecordServiceImpl implements EventRecordService {

    private static final String ENTITY_NAME = "eventRecord";

    private final Logger log = LoggerFactory.getLogger(EventRecordServiceImpl.class);

    private final EventRecordRepository eventRecordRepository;

    private final EventRecordMapper eventRecordMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    public EventRecordServiceImpl(EventRecordRepository eventRecordRepository, EventRecordMapper eventRecordMapper, UserExtraRepository userExtraRepository,
                                  AuthorizationService authorizationService) {
        this.eventRecordRepository = eventRecordRepository;
        this.eventRecordMapper = eventRecordMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
    }

    /**
     * Save a eventRecord.
     *
     * @param eventRecordDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EventRecordDTO save(EventRecordDTO eventRecordDTO) {
        log.debug("Request to save EventRecord : {}", eventRecordDTO);
        validateEventRecordDtoForCreation(eventRecordDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
            case FAMILY: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByFamily(currentUser))) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                } throw new AuthorizationException("You can only create "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            case ORGANIZIATION: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                }throw new AuthorizationException("You can only create "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            default: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                }throw new AuthorizationException("You can only create "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    /**
     * Get one eventRecord by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EventRecordDTO findOne(Long id) {
        log.debug("Request to get EventRecord by id: {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        EventRecordDTO eventRecordDTO = eventRecordRepository.findById(id)
            .map(eventRecordMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return eventRecordDTO;
            case FAMILY: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByFamily(currentUser)))
                    return eventRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            case ORGANIZIATION: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByOrganization(currentUser)))
                    return eventRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            default: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())) return eventRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user only!");
            }
        }
    }

    /**
     * Delete the eventRecord by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EventRecord : {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY)){
            eventRecordRepository.deleteById(id);
        } else {
            EventRecord eventRecordToDelete = eventRecordRepository.getOne(id);
            if (eventRecordToDelete.getUserId().equals(currentUser.getId())) {
                eventRecordRepository.deleteById(id);
            } else {
                throw new AuthorizationException("You can only delete "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    /**
     * Update a eventRecord.
     *
     * @param eventRecordDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EventRecordDTO update(EventRecordDTO eventRecordDTO) {
        log.debug("Request to update EventRecord : {}", eventRecordDTO);
        validateEventRecordDtoForUpdate(eventRecordDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
            case FAMILY: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByFamily(currentUser))) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            case ORGANIZIATION: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(eventRecordDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" entity for your user or users that are associated with you!");
            }
            default: {
                if (eventRecordDTO.getUserId().equals(currentUser.getId())) {
                    return eventRecordMapper.toDto(eventRecordRepository.save(eventRecordMapper.toEntity(eventRecordDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    private boolean isRecordAssociatedToUsers(EventRecordDTO eventRecordDTO, List<UserExtra> userExtraList) {
        return userExtraList
            .stream()
            .anyMatch(userExtra -> userExtra.getUser().getId().equals(eventRecordDTO.getUserId()));
    }

    private void validateEventRecordDtoForUpdate(EventRecordDTO eventRecordDTO) {
        if (eventRecordDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    private void validateEventRecordDtoForCreation(EventRecordDTO eventRecordDTO) {
        if (eventRecordDTO.getId() != null) {
            throw new BadRequestAlertException("A new eventRecord cannot already have an ID", ENTITY_NAME, "idexists");
        }
    }
}
