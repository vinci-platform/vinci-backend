package com.upb.vinci.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EventRecord.
 */
@Entity
@Table(name = "event_record")
public class EventRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "app_id")
    private Long appId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "jhi_timestamp")
    private Long timestamp;

    @Column(name = "created")
    private Long created;

    @Column(name = "notify")
    private Long notify;

    @Column(name = "jhi_repeat")
    private String repeat;

    @Column(name = "title")
    private String title;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "text")
    private String text;

    @Column(name = "data")
    private String data;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppId() {
        return appId;
    }

    public EventRecord appId(Long appId) {
        this.appId = appId;
        return this;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getUserId() {
        return userId;
    }

    public EventRecord userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public EventRecord timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getCreated() {
        return created;
    }

    public EventRecord created(Long created) {
        this.created = created;
        return this;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getNotify() {
        return notify;
    }

    public EventRecord notify(Long notify) {
        this.notify = notify;
        return this;
    }

    public void setNotify(Long notify) {
        this.notify = notify;
    }

    public String getRepeat() {
        return repeat;
    }

    public EventRecord repeat(String repeat) {
        this.repeat = repeat;
        return this;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getTitle() {
        return title;
    }

    public EventRecord title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public EventRecord type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public EventRecord text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getData() {
        return data;
    }

    public EventRecord data(String data) {
        this.data = data;
        return this;
    }

    public void setData(String data) {
        this.data = data;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventRecord eventRecord = (EventRecord) o;
        if (eventRecord.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventRecord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventRecord{" +
            "id=" + getId() +
            ", appId=" + getAppId() +
            ", userId=" + getUserId() +
            ", timestamp=" + getTimestamp() +
            ", created=" + getCreated() +
            ", notify=" + getNotify() +
            ", repeat='" + getRepeat() + "'" +
            ", title='" + getTitle() + "'" +
            ", type='" + getType() + "'" +
            ", text='" + getText() + "'" +
            ", data='" + getData() + "'" +
            "}";
    }
}
