package com.upb.vinci.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal")
public class LegalResource {

    @GetMapping("/privacy-policy")
    public String getPrivacyPolicy(){
        return "legal/vInci_Privacy_Policy.html";
    }

    @GetMapping("/terms-and-conditions")
    public String getTermsAndConditions() {
        return "legal/vInci_Terms_And_Conditions.html";
    }
}
