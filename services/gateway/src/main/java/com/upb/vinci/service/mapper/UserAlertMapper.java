package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.UserAlertDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserAlert and its DTO UserAlertDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class})
public interface UserAlertMapper extends EntityMapper<UserAlertDTO, UserAlert> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    @Mapping(source = "userExtra.user.firstName", target = "userExtraFirstName")
    @Mapping(source = "userExtra.user.lastName", target = "userExtraLastName")
    @Mapping(source = "createdDate", target = "createdDate")
    UserAlertDTO toDto(UserAlert userAlert);

    @Mapping(source = "userExtraId", target = "userExtra")
    @Mapping(source = "createdDate", target = "createdDate")
    UserAlert toEntity(UserAlertDTO userAlertDTO);

    default UserAlert fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserAlert userAlert = new UserAlert();
        userAlert.setId(id);
        return userAlert;
    }
}
