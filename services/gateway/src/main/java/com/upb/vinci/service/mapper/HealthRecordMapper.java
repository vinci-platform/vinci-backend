package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.HealthRecordDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity HealthRecord and its DTO HealthRecordDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HealthRecordMapper extends EntityMapper<HealthRecordDTO, HealthRecord> {



    default HealthRecord fromId(Long id) {
        if (id == null) {
            return null;
        }
        HealthRecord healthRecord = new HealthRecord();
        healthRecord.setId(id);
        return healthRecord;
    }
}
