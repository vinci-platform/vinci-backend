package com.upb.vinci.service;

import com.upb.vinci.domain.Authority;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.errors.NoUserForTokenException;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.security.SecurityUtils;
import com.upb.vinci.service.dto.UserDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.UserExtraMapper;
import com.upb.vinci.service.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

@Service
@Slf4j
public class AuthorizationService {

    private final UserRepository userRepository;
    private final UserExtraService userExtraService;
    private final UserExtraRepository userExtraRepository;
    private final UserExtraMapper userExtraMapper;
    private final UserMapper userMapper;

    public AuthorizationService(UserRepository userRepository, UserExtraService userExtraService, UserExtraRepository userExtraRepository,
                                UserExtraMapper userExtraMapper, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userExtraService = userExtraService;
        this.userExtraRepository = userExtraRepository;
        this.userExtraMapper = userExtraMapper;
        this.userMapper = userMapper;
    }

    @Transactional(readOnly = true)
    public User getCurrentUser_WithAuthorities() {
        Optional<User> currentUser = SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneWithAuthoritiesByLogin);
        if (!currentUser.isPresent()) {
            log.debug("No user found for token!");
            throw new NoUserForTokenException();
        }
        return currentUser.get();
    }

    @Transactional(readOnly = true)
    public UserDTO getCurrentUserDto_WithAuthorities() {
        return userMapper.userToUserDTO(getCurrentUser_WithAuthorities());
    }

    @Transactional(readOnly = true)
    public UserExtraDTO getCurrentUserExtraDto_WithAuthorities() {
        User currentUser = getCurrentUser_WithAuthorities();
        return getCurrentUserExtraDto_ForUserId(currentUser.getId());
    }

    @Transactional(readOnly = true)
    public UserExtraDTO getCurrentUserExtraDto_ForUserId(Long userId) {
        UserExtra currentUserExtra = getCurrentUserExtra_ForUserId(userId);
        return userExtraMapper.toDto(currentUserExtra);
    }
    @Transactional(readOnly = true)
    public UserExtra getCurrentUserExtra_ForUserId(Long userId) {
        UserExtra currentUserExtra = userExtraRepository.findByUserId(userId);
        if (currentUserExtra == null) {
            log.error("No userExtra found for userId: {}", userId);
            currentUserExtra = userExtraService.createUserExtraForUser(getCurrentUser_WithAuthorities());
        }
        return currentUserExtra;
    }

    public String getUserAuthorityString(User currentUser) {
        List<String> authorities = currentUser.getAuthorities().stream().map(Authority::getName).collect(Collectors.toList());
        if (authorities.contains(ADMIN)) return ADMIN;
        if (authorities.contains(FAMILY)) return FAMILY;
        if (authorities.contains(ORGANIZIATION)) return ORGANIZIATION;
        if (authorities.contains(PACIENT)) return PACIENT;
        return USER;
    }
}
