package com.upb.vinci.errors.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ToManyRequestsToFitbitServerException extends AbstractThrowableProblem {
    private static final String TITLE = "TO_MANY_REQUESTS_TO_FITBIT_SERVER";
    private static final Status STATUS_TYPE = Status.TOO_MANY_REQUESTS;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    public ToManyRequestsToFitbitServerException(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }

    public ToManyRequestsToFitbitServerException(ErrorResponse errorResponse){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE, errorResponse.getDetail());
    }

    @Override
    public String toString() {
        return "ToManyRequestsToFitbitServerException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
