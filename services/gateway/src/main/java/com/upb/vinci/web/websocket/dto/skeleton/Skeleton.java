package com.upb.vinci.web.websocket.dto.skeleton;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Skeleton implements Serializable {
    @JsonProperty("skeleton")
    private Long index;
    @JsonProperty("joint_head")
    private Joint head;
    @JsonProperty("joint_neck")
    private Joint neck;
    @JsonProperty("joint_torso")
    private Joint torso;
    @JsonProperty("joint_waist")
    private Joint waist;

    private Map<BodyPart, Joint> left = new HashMap<>();
    private Map<BodyPart, Joint> right = new HashMap<>();

    @JsonAnySetter
    public void setBodyPart(String key, Joint value) {
        Map<BodyPart, Joint> side = key.contains("left") ? left :
                key.contains("right") ? right : null;
        if (side == null) return;
        addJoint(side, key, value);
    }

    private void addJoint(Map<BodyPart, Joint> side, String jsonKey, Joint joint) {
        String key = Arrays.stream(jsonKey.split("_"))
                .reduce((first, second) -> second)
                .orElse(null);
        side.put(BodyPart.getInstance(key), joint);

    }

    @Override
    public String toString() {
        return index + " " + head.toString() + " " + neck.toString() + " " + torso.toString() + " " + waist.toString();
    }
}
