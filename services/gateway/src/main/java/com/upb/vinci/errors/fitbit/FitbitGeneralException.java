package com.upb.vinci.errors.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class FitbitGeneralException extends AbstractThrowableProblem {

    private static final String TITLE = "FITBIT_GENERAL_EXCEPTION";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_MESSAGE = "Exception from fitbit servers!";
    private static final String MESSAGE_WITH_EXCEPTION = "Exception from fitbit servers: %s";

    public FitbitGeneralException() { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, DEFAULT_MESSAGE); }
    public FitbitGeneralException(String exceptionFromFitbitServers) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(MESSAGE_WITH_EXCEPTION,exceptionFromFitbitServers)); }
    public FitbitGeneralException(ErrorResponse errorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, errorResponse.getDetail()); }

    @Override
    public String toString() {
        return "FitbitGeneralException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }
}
