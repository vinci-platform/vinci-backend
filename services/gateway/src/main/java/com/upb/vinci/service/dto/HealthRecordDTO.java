package com.upb.vinci.service.dto;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the HealthRecord entity.
 */
public class HealthRecordDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userId;

    private Long timestamp;

    private Double score;

    private Double availableScore;

    private Double stepsScore;

    private Double ipaqScore;

    private Double whoqolScore;

    private Double feelingsScore;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getAvailableScore() {
        return availableScore;
    }

    public void setAvailableScore(Double availableScore) {
        this.availableScore = availableScore;
    }

    public Double getStepsScore() {
        return stepsScore;
    }

    public void setStepsScore(Double stepsScore) {
        this.stepsScore = stepsScore;
    }

    public Double getIpaqScore() {
        return ipaqScore;
    }

    public void setIpaqScore(Double ipaqScore) {
        this.ipaqScore = ipaqScore;
    }

    public Double getWhoqolScore() {
        return whoqolScore;
    }

    public void setWhoqolScore(Double whoqolScore) {
        this.whoqolScore = whoqolScore;
    }

    public Double getFeelingsScore() {
        return feelingsScore;
    }

    public void setFeelingsScore(Double feelingsScore) {
        this.feelingsScore = feelingsScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HealthRecordDTO healthRecordDTO = (HealthRecordDTO) o;
        if (healthRecordDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), healthRecordDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HealthRecordDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", timestamp=" + getTimestamp() +
            ", score=" + getScore() +
            ", availableScore=" + getAvailableScore() +
            ", stepsScore=" + getStepsScore() +
            ", ipaqScore=" + getIpaqScore() +
            ", whoqolScore=" + getWhoqolScore() +
            ", feelingsScore=" + getFeelingsScore() +
            "}";
    }
}
