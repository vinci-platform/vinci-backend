package com.upb.vinci.repository;

import com.upb.vinci.domain.UserAlert;
import com.upb.vinci.domain.UserImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserAlert entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserImageRepository extends JpaRepository<UserImage, Long> {

    Page<UserImage> getUserImageBy(Pageable pageable);
}
