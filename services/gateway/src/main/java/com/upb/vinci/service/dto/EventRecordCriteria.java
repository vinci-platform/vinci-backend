package com.upb.vinci.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the EventRecord entity. This class is used in EventRecordResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /event-records?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EventRecordCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter appId;

    private LongFilter userId;

    private LongFilter timestamp;

    private LongFilter created;

    private LongFilter notify;

    private StringFilter repeat;

    private StringFilter title;

    private StringFilter type;

    private StringFilter text;

    private StringFilter data;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAppId() {
        return appId;
    }

    public void setAppId(LongFilter appId) {
        this.appId = appId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LongFilter timestamp) {
        this.timestamp = timestamp;
    }

    public LongFilter getCreated() {
        return created;
    }

    public void setCreated(LongFilter created) {
        this.created = created;
    }

    public LongFilter getNotify() {
        return notify;
    }

    public void setNotify(LongFilter notify) {
        this.notify = notify;
    }

    public StringFilter getRepeat() {
        return repeat;
    }

    public void setRepeat(StringFilter repeat) {
        this.repeat = repeat;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getType() {
        return type;
    }

    public void setType(StringFilter type) {
        this.type = type;
    }

    public StringFilter getText() {
        return text;
    }

    public void setText(StringFilter text) {
        this.text = text;
    }

    public StringFilter getData() {
        return data;
    }

    public void setData(StringFilter data) {
        this.data = data;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EventRecordCriteria that = (EventRecordCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(appId, that.appId) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(timestamp, that.timestamp) &&
            Objects.equals(created, that.created) &&
            Objects.equals(notify, that.notify) &&
            Objects.equals(repeat, that.repeat) &&
            Objects.equals(title, that.title) &&
            Objects.equals(type, that.type) &&
            Objects.equals(text, that.text) &&
            Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        appId,
        userId,
        timestamp,
        created,
        notify,
        repeat,
        title,
        type,
        text,
        data
        );
    }

    @Override
    public String toString() {
        return "EventRecordCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (appId != null ? "appId=" + appId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (created != null ? "created=" + created + ", " : "") +
                (notify != null ? "notify=" + notify + ", " : "") +
                (repeat != null ? "repeat=" + repeat + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (text != null ? "text=" + text + ", " : "") +
                (data != null ? "data=" + data + ", " : "") +
            "}";
    }

}
