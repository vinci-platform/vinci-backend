package com.upb.vinci.service.impl;

import com.upb.vinci.domain.DeviceAlert;
import com.upb.vinci.domain.User;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.repository.DeviceAlertRepository;
import com.upb.vinci.service.*;
import com.upb.vinci.service.dto.DeviceAlertDTO;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.DeviceAlertMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing DeviceAlert.
 */
@Service
@Transactional
@Slf4j
public class DeviceAlertServiceImpl implements DeviceAlertService {

    private static final String ENTITY_NAME = "deviceAlert";

    private final DeviceAlertRepository deviceAlertRepository;

    private final DeviceAlertMapper deviceAlertMapper;

    private final DeviceService deviceService;

    private final AuthorizationService authorizationService;

    public DeviceAlertServiceImpl(DeviceAlertRepository deviceAlertRepository, DeviceAlertMapper deviceAlertMapper,
                                  DeviceService deviceService, AuthorizationService authorizationService) {
        this.deviceAlertRepository = deviceAlertRepository;
        this.deviceAlertMapper = deviceAlertMapper;
        this.deviceService = deviceService;
        this.authorizationService = authorizationService;
    }

    /**
     * Save a deviceAlert.
     *
     * @param deviceAlertDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeviceAlertDTO save(DeviceAlertDTO deviceAlertDTO) throws Exception {
        log.debug("Request to save "+ENTITY_NAME+" : {}", deviceAlertDTO);
        validateDeviceAlertForCreation(deviceAlertDTO);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
            case FAMILY: {
                if (currentUserExtraDto.getDevices().contains(new DeviceDTO().id(deviceAlertDTO.getDeviceId())) || isDeviceOfFamily(deviceAlertDTO, currentUser)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } throw new AuthorizationException("You can create "+ENTITY_NAME+" for your devices or devices of associated users only!");
            }
            case ORGANIZIATION: {
                if (currentUserExtraDto.getDevices().contains(new DeviceDTO().id(deviceAlertDTO.getDeviceId())) || isDeviceOfOrganization(deviceAlertDTO, currentUser)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } throw new AuthorizationException("You can create "+ENTITY_NAME+" for your devices or devices of associated users only!");
            }
            default: {
                if (isDeviceIdFromDeviceAlertInAnyOfDevicesForCurrentUser(deviceAlertDTO, currentUserExtraDto)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } else throw new AuthorizationException("You can create "+ENTITY_NAME+" for your devices only!");
            }
        }
    }

    /**
     * Get one deviceAlert by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DeviceAlertDTO findOne(Long id) {
        log.debug("Request to get "+ENTITY_NAME+" by id: {}", id);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        DeviceAlert deviceAlert = deviceAlertRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));
        DeviceAlertDTO returnValueDeviceAlertDto = deviceAlertMapper.toDto(deviceAlert);

        switch (currentUsersAuthority) {
            case ADMIN: return returnValueDeviceAlertDto;
            case ORGANIZIATION: {
                if (deviceAlert.getDevice().getUserExtra().getId().equals(currentUserExtraDto.getId()) || isDeviceOfOrganization(returnValueDeviceAlertDto,currentUser))
                    return returnValueDeviceAlertDto;
                throw new AuthorizationException("You can only get your "+ENTITY_NAME+" or "+ENTITY_NAME+" from devices of your associates!");
            }
            case FAMILY: {
                if (deviceAlert.getDevice().getUserExtra().getId().equals(currentUserExtraDto.getId()) || isDeviceOfFamily(returnValueDeviceAlertDto,currentUser))
                    return returnValueDeviceAlertDto;
                throw new AuthorizationException("You can only get your "+ENTITY_NAME+" or "+ENTITY_NAME+" from devices of your associates!");
            }
            default: {
                if (deviceAlert.getDevice().getUserExtra().getId().equals(currentUserExtraDto.getId()))
                    return returnValueDeviceAlertDto;
                throw new AuthorizationException("You can only get your "+ENTITY_NAME);
            }
        }
    }

    public DeviceAlertDTO update(DeviceAlertDTO deviceAlertDTO) {
        log.debug("Request to update "+ENTITY_NAME+" : {}", deviceAlertDTO);
        validateUpdateDeviceAlertDto(deviceAlertDTO);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
            case FAMILY: {
                if (currentUserExtraDto.getDevices().contains(new DeviceDTO().id(deviceAlertDTO.getDeviceId())) ||
                    isDeviceOfFamily(deviceAlertDTO, currentUser)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" from your devices or associated users devices!");
            }
            case ORGANIZIATION: {
                if (currentUserExtraDto.getDevices().contains(new DeviceDTO().id(deviceAlertDTO.getDeviceId())) ||
                    isDeviceOfOrganization(deviceAlertDTO, currentUser)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" from your devices or associated users devices!");
            }
            default: {
                if (isDeviceIdFromDeviceAlertInAnyOfDevicesForCurrentUser(deviceAlertDTO, currentUserExtraDto)) {
                    return deviceAlertMapper.toDto(deviceAlertRepository.save(deviceAlertMapper.toEntity(deviceAlertDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" from your devices");
            }
        }
    }

    /**
     * Delete the deviceAlert by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete "+ENTITY_NAME+" : {}", id);  deviceAlertRepository.deleteById(id);
    }

    private void validateUpdateDeviceAlertDto(DeviceAlertDTO deviceAlertDTO) {
        if (deviceAlertDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (deviceAlertDTO.getDeviceId() == null) {
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" must have a deviceID", ENTITY_NAME, "deviceIdNull");
        }
    }

    private void validateDeviceAlertForCreation(DeviceAlertDTO deviceAlertDTO) {
        if (deviceAlertDTO.getId() != null) {
            throw new BadRequestAlertException("A new deviceAlert cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (deviceAlertDTO.getDeviceId() == null) {
            throw new BadRequestAlertException("A new deviceAlert must have a deviceID", ENTITY_NAME, "deviceIdNull");
        }
    }

    private boolean isDeviceOfFamily(DeviceAlertDTO deviceAlertDTO, User currentUser) {
        return deviceService.findAllByFamily(currentUser)
            .stream()
            .anyMatch(deviceDTO -> deviceDTO.getId().equals(deviceAlertDTO.getDeviceId()));
    }

    private boolean isDeviceOfOrganization(DeviceAlertDTO deviceAlertDTO, User currentUser) {
        return deviceService.findAllByOrganization(currentUser)
            .stream()
            .anyMatch(deviceDTO -> deviceDTO.getId().equals(deviceAlertDTO.getDeviceId()));
    }

    private boolean isDeviceIdFromDeviceAlertInAnyOfDevicesForCurrentUser(DeviceAlertDTO deviceAlertDTO, UserExtraDTO currentUserExtraDto) {
        return currentUserExtraDto.getDevices()
            .stream()
            .map(DeviceDTO::getId)
            .anyMatch(deviceId -> deviceId.equals(deviceAlertDTO.getDeviceId()));
    }

}
