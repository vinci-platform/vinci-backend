package com.upb.vinci.repository;

import com.upb.vinci.domain.EventRecord;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EventRecord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventRecordRepository extends JpaRepository<EventRecord, Long>, JpaSpecificationExecutor<EventRecord> {

}
