package com.upb.vinci.service.dto;

import com.upb.vinci.domain.enumeration.DeviceType;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the Device entity. This class is used in DeviceResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /devices?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DeviceCriteria implements Serializable {
    /**
     * Class for filtering DeviceType
     */
    public static class DeviceTypeFilter extends Filter<DeviceType> {
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter description;

    private StringFilter uuid;

    private DeviceTypeFilter deviceType;

    private BooleanFilter active;

    private InstantFilter startTimestamp;

    private LongFilter alertId;

    private LongFilter userExtraId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getUuid() {
        return uuid;
    }

    public void setUuid(StringFilter uuid) {
        this.uuid = uuid;
    }

    public DeviceTypeFilter getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceTypeFilter deviceType) {
        this.deviceType = deviceType;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public InstantFilter getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(InstantFilter startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public LongFilter getAlertId() {
        return alertId;
    }

    public void setAlertId(LongFilter alertId) {
        this.alertId = alertId;
    }

    public LongFilter getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(LongFilter userExtraId) {
        this.userExtraId = userExtraId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DeviceCriteria that = (DeviceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(uuid, that.uuid) &&
            Objects.equals(deviceType, that.deviceType) &&
            Objects.equals(active, that.active) &&
            Objects.equals(startTimestamp, that.startTimestamp) &&
            Objects.equals(alertId, that.alertId) &&
            Objects.equals(userExtraId, that.userExtraId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        description,
        uuid,
        deviceType,
        active,
        startTimestamp,
        alertId,
        userExtraId
        );
    }

    @Override
    public String toString() {
        return "DeviceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (uuid != null ? "uuid=" + uuid + ", " : "") +
                (deviceType != null ? "deviceType=" + deviceType + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
                (startTimestamp != null ? "startTimestamp=" + startTimestamp + ", " : "") +
                (alertId != null ? "alertId=" + alertId + ", " : "") +
                (userExtraId != null ? "userExtraId=" + userExtraId + ", " : "") +
            "}";
    }

}
