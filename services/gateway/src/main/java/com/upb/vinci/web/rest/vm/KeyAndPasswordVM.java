package com.upb.vinci.web.rest.vm;

import javax.validation.constraints.Pattern;

/**
 * View Model object for storing the user's key and password.
 */
public class KeyAndPasswordVM {

    public static final String PASSWORD_REGEX = "(?=[A-Za-z0-9@#$%^&+!=-]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=-])(?=.{10,}).*$";
    public static final String PASSWORD_REGEX_MESSAGE = "Password needs to have at least 1 upper and lower case, number and special character!";

    private String key;

    @Pattern(regexp = PASSWORD_REGEX,message=PASSWORD_REGEX_MESSAGE)
    private String newPassword;

    private boolean activate;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public boolean getActivate() {
        return activate;
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }
}
