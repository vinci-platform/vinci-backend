package com.upb.vinci.service.dto;

import javax.validation.constraints.Pattern;

/**
 * A DTO representing a password change required data - current and new password.
 */
public class PasswordChangeDTO {
    public static final String PASSWORD_REGEX = "(?=[A-Za-z0-9@#$%^&+!=-]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=-])(?=.{10,}).*$";
    public static final String PASSWORD_REGEX_MESSAGE = "Password needs to have at least 1 upper and lower case, number and special character!";

    private String currentPassword;

    @Pattern(regexp = PASSWORD_REGEX, message = PASSWORD_REGEX_MESSAGE)
    private String newPassword;

    public PasswordChangeDTO() {
        // Empty constructor needed for Jackson.
    }

    public PasswordChangeDTO(String currentPassword, String newPassword) {
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    public String getCurrentPassword() {

        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
