package com.upb.vinci.config.kafka;

public interface TopicConstants {
    String TOPIC = "KAFKA_TOPIC_EXAMPLE";
    String TOPIC_FITBIT_DEVICE_UPDATE = "KAFKA_TOPIC_FITBIT_DEVICE_UPDATE";
}
