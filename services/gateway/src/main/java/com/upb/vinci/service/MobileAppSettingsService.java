package com.upb.vinci.service;

import com.upb.vinci.domain.User;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.repository.MobileAppSettingsRepository;
import com.upb.vinci.service.dto.MobileAppSettingsDTO;
import com.upb.vinci.service.mapper.MobileAppSettingsMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing MobileAppSettings.
 */
@Service
@Transactional
public class MobileAppSettingsService {

    private static final String ENTITY_NAME = "mobileAppSettings";

    private final Logger log = LoggerFactory.getLogger(MobileAppSettingsService.class);

    private final MobileAppSettingsRepository mobileAppSettingsRepository;

    private final MobileAppSettingsMapper mobileAppSettingsMapper;
    private final AuthorizationService authorizationService;

    public MobileAppSettingsService(MobileAppSettingsRepository mobileAppSettingsRepository, MobileAppSettingsMapper mobileAppSettingsMapper,
                                    AuthorizationService authorizationService) {
        this.mobileAppSettingsRepository = mobileAppSettingsRepository;
        this.mobileAppSettingsMapper = mobileAppSettingsMapper;
        this.authorizationService = authorizationService;
    }

  /**
     * Save a mobileAppSettings.
     * Admin can get all settings, other users can only get their personal settings and global settings
     *
     * @param mobileAppSettingsDTO the entity to save
     * @return the persisted entity or null if there is exception
     */
    public MobileAppSettingsDTO save(MobileAppSettingsDTO mobileAppSettingsDTO) {
        log.debug("Request to save MobileAppSettings : {}", mobileAppSettingsDTO);
        validateMobileAppSettingsForCreation(mobileAppSettingsDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        setIdIfSettingAlreadyExists(mobileAppSettingsDTO);

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) || mobileAppSettingsDTO.getUserId().equals(currentUser.getId())) {
            return mobileAppSettingsMapper.toDto(mobileAppSettingsRepository.save(mobileAppSettingsMapper.toEntity(mobileAppSettingsDTO)));
        }
        throw new AuthorizationException("You can only create "+ENTITY_NAME+" for your user!");
    }

    /**
     * Get one mobileAppSettings by id.
     * Admin can get all settings, other users can only get their personal settings and global settings
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MobileAppSettingsDTO findOne(Long id) {
        log.debug("Request to get MobileAppSettings : {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        MobileAppSettingsDTO mobileAppSettingsDTO = mobileAppSettingsRepository.findById(id)
            .map(mobileAppSettingsMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) ||
            mobileAppSettingsDTO.getUserId().equals(currentUser.getId()) ||
            mobileAppSettingsDTO.getUserId().equals(0L)) {
            return mobileAppSettingsDTO;
        }
        throw new AuthorizationException("You can only get "+ENTITY_NAME+" for your user!");
    }

    /**
     * Delete the mobileAppSettings by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MobileAppSettings : {}", id);
        mobileAppSettingsRepository.deleteById(id);
    }

    /**
     * Admin can update all settings, other users can only get their personal settings and global settings
     *
     * @param mobileAppSettingsDTO to update
     * @return MobileAppSettingsDTO
     */
    public MobileAppSettingsDTO update(MobileAppSettingsDTO mobileAppSettingsDTO) {
        log.debug("Request to update MobileAppSettings : {}", mobileAppSettingsDTO);
        validateMobileAppSettingsDtoForUpdate(mobileAppSettingsDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) || mobileAppSettingsDTO.getUserId().equals(currentUser.getId())){
            return mobileAppSettingsMapper.toDto(mobileAppSettingsRepository.save(mobileAppSettingsMapper.toEntity(mobileAppSettingsDTO)));
        }
        throw new AuthorizationException("You can only update "+ENTITY_NAME+" for your user!");
    }

    private void validateMobileAppSettingsDtoForUpdate(MobileAppSettingsDTO mobileAppSettingsDTO) {
        if (mobileAppSettingsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    private void validateMobileAppSettingsForCreation(MobileAppSettingsDTO mobileAppSettingsDTO) {
        if (mobileAppSettingsDTO.getId() != null) {
            throw new BadRequestAlertException("A new mobileAppSettings cannot already have an ID", ENTITY_NAME, "idexists");
        }
    }

    private MobileAppSettingsDTO setIdIfSettingAlreadyExists(MobileAppSettingsDTO mobileAppSettingsDTO) {
        mobileAppSettingsRepository.findByNameAndUserId(mobileAppSettingsDTO.getName(),mobileAppSettingsDTO.getUserId())
            .ifPresent(mobileAppSettings -> mobileAppSettingsDTO.setId(mobileAppSettings.getId()));
        return mobileAppSettingsDTO;
    }
}
