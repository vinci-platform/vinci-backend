package com.upb.vinci.security;

import com.upb.vinci.domain.Authority;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String FAMILY = "ROLE_FAMILY";

    public static final String PACIENT = "ROLE_PACIENT";

    public static final String ORGANIZIATION="ROLE_ORGANIZATION";

    public static final Authority ADMIN_AUTHORITY = new Authority("ROLE_ADMIN");

    public static final Authority USER_AUTHORITY = new Authority("ROLE_USER");

    public static final Authority ANONYMOUS_AUTHORITY = new Authority("ROLE_ANONYMOUS");

    public static final Authority FAMILY_AUTHORITY = new Authority("ROLE_FAMILY");

    public static final Authority PACIENT_AUTHORITY = new Authority("ROLE_PACIENT");

    public static final Authority ORGANIZIATION_AUTHORITY = new Authority("ROLE_ORGANIZATION");

    private AuthoritiesConstants() {
    }
}
