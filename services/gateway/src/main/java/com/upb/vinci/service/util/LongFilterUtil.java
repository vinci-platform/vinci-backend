package com.upb.vinci.service.util;

import io.github.jhipster.service.filter.LongFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LongFilterUtil {
    /**
     * This method is used for setting up Long filter for userExtraId, userId and deviceId because of conflicts with the criteria in requests
     * @param listOfIds
     * @param idFilter
     * @return returns modified idFilter that contains appropriate filter
     */
    public LongFilter seIdFilter(List<Long> listOfIds, LongFilter idFilter) {
        if (idFilter == null) {
            idFilter = new LongFilter();
            idFilter.setIn(listOfIds);
            return idFilter;
        }
        if (idFilter.getEquals() != null) {
            LongFilter finalIdFilter = idFilter;
            Optional<Long> equalsId = listOfIds.stream().filter(aLong -> aLong.equals(finalIdFilter.getEquals())).findFirst();
            if (equalsId.isPresent()) return idFilter;
        }
        if (idFilter.getIn() != null) {
            idFilter.setIn(idFilter
                .getIn()
                .stream()
                .filter(listOfIds::contains)
                .collect(Collectors.toList()));
            return idFilter;
        }
        idFilter.setIn(listOfIds);
        return idFilter;
    }

}
