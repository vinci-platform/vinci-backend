package com.upb.vinci.errors;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserByLoginException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_BY_LOGIN_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "There is no user with login!";
    private static final String DETAILS_WITH_LOGIN = "There is no user with login: %s";

    public NoUserByLoginException(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public NoUserByLoginException(String login) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE, String.format(DETAILS_WITH_LOGIN,login)); }
    public NoUserByLoginException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoUserByLoginException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
