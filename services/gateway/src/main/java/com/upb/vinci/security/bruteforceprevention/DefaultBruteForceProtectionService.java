package com.upb.vinci.security.bruteforceprevention;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserBruteForceAttackData;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoUserByLoginException;
import com.upb.vinci.repository.UserBruteForceAttackDataRepository;
import com.upb.vinci.repository.UserRepository;
import com.upb.vinci.service.UserAlertService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class DefaultBruteForceProtectionService implements BruteForceProtectionService {

    @Value("${jdj.security.failedlogin.count}")
    private int maxFailedLogins;

    @Value("${jdj.security.failedlogin.lockout-time}")
    private int lockoutTime;

    private final String TOO_MANY_LOGIN_ATTEMPTS = "You have spent 10 attempts trying to login. Wait 10 minutes!";

    private final UserRepository userRepository;
    private final UserAlertService userAlertService;
    private final UserBruteForceAttackDataRepository userBruteForceAttackDataRepository;

    public DefaultBruteForceProtectionService(UserRepository userRepository, UserAlertService userAlertService,
                                              UserBruteForceAttackDataRepository userBruteForceAttackDataRepository) {
        this.userRepository = userRepository;
        this.userAlertService = userAlertService;
        this.userBruteForceAttackDataRepository = userBruteForceAttackDataRepository;
    }

    @Override
    public void registerLoginFailure(String login) {
        UserBruteForceAttackData ubfad = getUserBruteForceAttackData(login);
        if (!ubfad.getLoginDisabled()) {
            Long failedCounter = ubfad.getFailedLoginAttempts();
            if (maxFailedLogins < failedCounter + 1) {
                ubfad.setLoginDisabled(true); //disabling the account
                ubfad.setLoginEnabledFrom(Instant.now().plus(lockoutTime, ChronoUnit.MINUTES));
                userBruteForceAttackDataRepository.save(ubfad);
//                userAlertService.bruteForceAttemptForUserExtra(ubfad.getUser());
            } else {
                ubfad.setFailedLoginAttempts(failedCounter + 1);
                userBruteForceAttackDataRepository.save(ubfad);
            }
        }
    }

    @Override
    public void resetBruteForceCounter(String login) {
        UserBruteForceAttackData ubfad = getUserBruteForceAttackData(login);
        if (ubfad != null) {
            ubfad.setFailedLoginAttempts(0L);
            ubfad.setLoginDisabled(false);
            userBruteForceAttackDataRepository.save(ubfad);
        }
    }

    @Override
    public void checkAttemptsForUser(User user) {
        UserBruteForceAttackData userBruteForceAttackData = userBruteForceAttackDataRepository
            .findByUser(user)
            .orElse(createUserBruteForceForUserLogin(user.getLogin()));
        if (userBruteForceAttackData.getLoginDisabled())
            throw new AuthorizationException(TOO_MANY_LOGIN_ATTEMPTS);
    }

    private UserBruteForceAttackData getUserBruteForceAttackData(final String login) {
        return userBruteForceAttackDataRepository
            .findByUser(getUser(login))
            .orElse(createUserBruteForceForUserLogin(login));
    }

    private User getUser(final String login) {
        return userRepository
            .findOneByLogin(login)
            .orElseThrow(NoUserByLoginException::new);
    }

    private UserBruteForceAttackData createUserBruteForceForUserLogin(String login) {
        Optional<UserBruteForceAttackData> byUserLogin = userBruteForceAttackDataRepository.findByUserLogin(login);
        return byUserLogin.orElseGet(() -> userBruteForceAttackDataRepository.saveAndFlush(UserBruteForceAttackData.builder()
            .user(getUser(login))
            .failedLoginAttempts(0L)
            .loginDisabled(false)
            .loginEnabledFrom(Instant.now())
            .build()));
    }
}
