package com.upb.vinci.web.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/")
public class RedirectToDashboardController {

    @Value("${dashboard.url}")
    private String dashboardUrl;

    /**
     * Redirects to where dashboard is placed
     */
    @GetMapping
    public void redirectToDashboard(HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location", dashboardUrl);
        httpServletResponse.setStatus(301);
    }
}
