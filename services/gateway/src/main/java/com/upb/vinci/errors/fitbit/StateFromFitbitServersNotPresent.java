package com.upb.vinci.errors.fitbit;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class StateFromFitbitServersNotPresent extends AbstractThrowableProblem {

    private static final String TITLE = "STATE_FROM_FITBIT_SERVER_NOT_PRESENT";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "State is not present when trying to get fitbit token!";

    public StateFromFitbitServersNotPresent(){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS);
    }
    public StateFromFitbitServersNotPresent(String message) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,message);
    }
    public StateFromFitbitServersNotPresent(ErrorResponse feignErrorResponse) {
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail());
    }

    @Override
    public String toString() {
        return "StateFromFitbitServersNotPresent{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
