package com.upb.vinci.service.impl;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserAlert;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.AlertType;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.repository.UserAlertRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.AuthorizationService;
import com.upb.vinci.service.UserAlertService;
import com.upb.vinci.service.dto.UserAlertDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.UserAlertMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing UserAlert.
 */
@Service
@Transactional
public class UserAlertServiceImpl implements UserAlertService {

    private static final String ENTITY_NAME = "userAlert";

    private final Logger log = LoggerFactory.getLogger(UserAlertServiceImpl.class);

    private final UserAlertRepository userAlertRepository;

    private final UserExtraRepository userExtraRepository;

    private final UserAlertMapper userAlertMapper;

    private final AuthorizationService authorizationService;

    public UserAlertServiceImpl(UserAlertRepository userAlertRepository, UserExtraRepository userExtraRepository, UserAlertMapper userAlertMapper,
                                AuthorizationService authorizationService) {
        this.userAlertRepository = userAlertRepository;
        this.userExtraRepository = userExtraRepository;
        this.userAlertMapper = userAlertMapper;
        this.authorizationService = authorizationService;
    }

    /**
     * just saves what is in userAlertDto.
     *
     * @param userAlertDTO the entity to save
     */
    @Override
    public void justSave(UserAlertDTO userAlertDTO) {
        log.debug("Request to justSave "+ENTITY_NAME+" : {}", userAlertDTO);
        UserAlert userAlert = userAlertMapper.toEntity(userAlertDTO);
        userAlert = userAlertRepository.save(userAlert);
        userAlertMapper.toDto(userAlert);
    }

    /**
     * Save a userAlert.
     *
     * @param userAlertDTO the entity to save
     * @return the persisted entity, or null if exception
     */
    @Override
    public UserAlertDTO save(UserAlertDTO userAlertDTO) {
        log.debug("Request to save UserAlert : {}", userAlertDTO);
        validateUserAlertDtoForCreation(userAlertDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
            case FAMILY: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByFamily(currentUser))) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                } throw new AuthorizationException("You can create "+ENTITY_NAME+" for your user or associated users only!");
            }
            case ORGANIZIATION: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                } throw new AuthorizationException("You can create "+ENTITY_NAME+" for your user or associated users only!");
            }
            default: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                }throw new AuthorizationException("You can create "+ENTITY_NAME+" for your user!");
            }
        }
    }

    private boolean isUserAlertAssociatedToUserList(UserAlertDTO userAlertDTO, List<UserExtra> byFamily) {
        return byFamily
            .stream()
            .map(UserExtra::getId)
            .anyMatch(userExtraId -> userExtraId.equals(userAlertDTO.getUserExtraId()));
    }


    /**
     * Get one userAlert by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserAlertDTO findOne(Long id) {
        log.debug("Request to get UserAlert : {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        UserAlertDTO userAlertDTO = userAlertRepository.findById(id)
            .map(userAlertMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return userAlertDTO;
            case FAMILY: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByFamily(currentUser))) return userAlertDTO;
                throw new AuthorizationException("You can get "+ENTITY_NAME+" for your user or associated users only!");
            }
            case ORGANIZIATION: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByOrganization(currentUser))) return userAlertDTO;
                throw new AuthorizationException("You can create "+ENTITY_NAME+" for your user or associated users only!");
            }
            default: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())) return userAlertDTO;
                throw new AuthorizationException("You can create "+ENTITY_NAME+" for your user only!");
            }
        }
    }

    /**
     * Delete the userAlert by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserAlert : {}", id);        userAlertRepository.deleteById(id);
    }

    /**
     * Update a userAlert.
     *
     * @param userAlertDTO the entity to save
     * @return the persisted entity, or null if exception
     */
    @Override
    public UserAlertDTO update(UserAlertDTO userAlertDTO) {
        log.debug("Request to update UserAlert : {}", userAlertDTO);
        validateUserAlertDtoForUpdate(userAlertDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
            case FAMILY: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByFamily(currentUser))) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                } throw new AuthorizationException("You can update "+ENTITY_NAME+" for your user or associated users only!");
            }
            case ORGANIZIATION: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())
                    || isUserAlertAssociatedToUserList(userAlertDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                } throw new AuthorizationException("You can update "+ENTITY_NAME+" for your user or associated users only!");
            }
            default: {
                if (userAlertDTO.getUserExtraId().equals(currentUserExtraDto.getId())) {
                    return userAlertMapper.toDto(userAlertRepository.save(userAlertMapper.toEntity(userAlertDTO)));
                }throw new AuthorizationException("You can update "+ENTITY_NAME+" for your user only!");
            }
        }
    }

    @Override
    public void updateList(List<UserAlertDTO> userAlertDTOs) {
        //todo: refactor this code so authorization service i called once here and rest is done in some update method
        for(UserAlertDTO alert : userAlertDTOs) {
            update(alert);
        }
    }

    @Override
    public void bruteForceAttemptForUserExtra(User user) {
        userAlertRepository.save(UserAlert.builder()
            .userExtra(userExtraRepository.findByUserId(user.getId()))
            .alertType(AlertType.DANGER)
            .familyRead(false)
            .organizationRead(false)
            .userRead(false)
            .label("bruteForceAttack")
            .values("Brute force attack was attempted! Someone tried to login to your account 10 times in a row")
            .build());
    }

    private void validateUserAlertDtoForCreation(UserAlertDTO userAlertDTO) {
        if (userAlertDTO.getId() != null) {
            throw new BadRequestAlertException("A new "+ENTITY_NAME+" cannot already have an ID", ENTITY_NAME, "idexists");
        }
    }

    private void validateUserAlertDtoForUpdate(UserAlertDTO userAlertDTO) {
        if (userAlertDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }
}
