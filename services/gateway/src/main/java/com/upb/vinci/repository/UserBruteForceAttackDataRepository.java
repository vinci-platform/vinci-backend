package com.upb.vinci.repository;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserBruteForceAttackData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserBruteForceAttackDataRepository extends JpaRepository <UserBruteForceAttackData,Integer> {

    List<UserBruteForceAttackData> findAllByLoginDisabledEquals(Boolean disable);
    Optional<UserBruteForceAttackData> findByUser(User user);
    @Query("select ubfad from UserBruteForceAttackData ubfad where ubfad.user.login = :login")
    Optional<UserBruteForceAttackData> findByUserLogin(@Param("login") String login);
}
