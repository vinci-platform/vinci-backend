package com.upb.vinci.web.rest;

import com.upb.vinci.service.FitbitService;
import com.upb.vinci.service.dto.FitbitRedirectUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;

/**
 * FitbitAuthResource controller
 */
@RestController
@RequestMapping("/api/fitbit-auth")
public class FitbitAuthResource {

    private final Logger log = LoggerFactory.getLogger(FitbitAuthResource.class);

    private final FitbitService fitbitService;

    public FitbitAuthResource(FitbitService fitbitService) {
        this.fitbitService = fitbitService;
    }

    /**
     * GET Redirect to Fitbit login page
     */
    @GetMapping("/authorize-mobile")
    public ResponseEntity<FitbitRedirectUrl> authorizeMobile(@RequestParam("userExtraId") Long userExtraId) {
        log.debug("REST request to get fitbit redirect url!");
        FitbitRedirectUrl fitFitbitRedirectUrl = fitbitService.getFitbitRedirectUrl(userExtraId);
        return ResponseEntity.ok().body(fitFitbitRedirectUrl);
    }

    /**
     * GET get Access Code from Fitbit and call the fitbitwatch microservices to generate tokens
     */
    @GetMapping("/code-mobile")
    public RedirectView getCodeMobile(@RequestParam(value = "code") String code, @RequestParam(value = "state") Optional<Long> state) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/#/");
        fitbitService.authorizeUserWithAuhorizationCode(code, state);
        return redirectView;
    }

}
