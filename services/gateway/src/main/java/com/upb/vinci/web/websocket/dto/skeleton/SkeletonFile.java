package com.upb.vinci.web.websocket.dto.skeleton;

import java.io.Serializable;
import java.util.List;

public class SkeletonFile implements Serializable {
    private Long number;
    private Long lastNumber;
    private List<Skeleton> skeletons;
}
