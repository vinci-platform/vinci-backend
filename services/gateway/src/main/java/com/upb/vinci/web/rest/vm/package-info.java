/**
 * View Models used by Spring MVC REST controllers.
 */
package com.upb.vinci.web.rest.vm;
