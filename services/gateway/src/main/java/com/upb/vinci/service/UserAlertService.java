package com.upb.vinci.service;

import com.upb.vinci.domain.User;
import com.upb.vinci.service.dto.UserAlertDTO;

import java.util.List;

/**
 * Service Interface for managing UserAlert.
 */
public interface UserAlertService {

    /**
     * Save a userAlert.
     *
     * @param userAlertDTO the entity to save
     */
    void justSave(UserAlertDTO userAlertDTO);
    UserAlertDTO save(UserAlertDTO userAlertDTO);

    /**
     * Get the "id" userAlert.
     *
     * @param id the id of the entity
     * @return the entity
     */
    UserAlertDTO findOne(Long id);

    /**
     * Delete the "id" userAlert.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    UserAlertDTO update(UserAlertDTO userAlertDTO);

    void updateList(List<UserAlertDTO> userAlertDTOs);

    void bruteForceAttemptForUserExtra(User user);
}
