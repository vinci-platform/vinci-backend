package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.DeviceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Device and its DTO DeviceDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class})
public interface DeviceMapper extends EntityMapper<DeviceDTO, Device> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    @Mapping(source = "userExtra.user.firstName", target = "userExtraFirstName")
    @Mapping(source = "userExtra.user.lastName", target = "userExtraLastName")
    @Mapping(source = "userExtra.family.id", target = "userFamilyId")
    @Mapping(source = "userExtra.family.firstName", target = "userFamilyFirstName")
    @Mapping(source = "userExtra.family.lastName", target = "userFamilyLastName")
    @Mapping(source = "userExtra.organization.id", target = "userOrganisationId")
    @Mapping(source = "userExtra.organization.firstName", target = "userOrganisationFirstName")
    @Mapping(source = "userExtra.organization.lastName", target = "userOrganisationLastName")
    DeviceDTO toDto(Device device);

    @Mapping(target = "alerts", ignore = true)
    @Mapping(source = "userExtraId", target = "userExtra")
    Device toEntity(DeviceDTO deviceDTO);

    default Device fromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
