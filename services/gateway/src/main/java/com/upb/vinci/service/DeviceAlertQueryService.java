package com.upb.vinci.service;

import com.upb.vinci.domain.*;
import com.upb.vinci.repository.DeviceAlertRepository;
import com.upb.vinci.repository.DeviceRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.dto.DeviceAlertCriteria;
import com.upb.vinci.service.dto.DeviceAlertDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.DeviceAlertMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for DeviceAlert entities in the database.
 * The main input is a {@link DeviceAlertCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DeviceAlertDTO} or a {@link Page} of {@link DeviceAlertDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DeviceAlertQueryService extends QueryService<DeviceAlert> {

    private static final String ENTITY_NAME = "deviceAlert";

    private final Logger log = LoggerFactory.getLogger(DeviceAlertQueryService.class);

    private final DeviceAlertRepository deviceAlertRepository;

    private final DeviceRepository deviceRepository;

    private final UserExtraRepository userExtraRepository;

    private final DeviceAlertMapper deviceAlertMapper;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public DeviceAlertQueryService(DeviceAlertRepository deviceAlertRepository, DeviceRepository deviceRepository, UserExtraRepository userExtraRepository,
                                   DeviceAlertMapper deviceAlertMapper, AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.deviceAlertRepository = deviceAlertRepository;
        this.deviceRepository = deviceRepository;
        this.userExtraRepository = userExtraRepository;
        this.deviceAlertMapper = deviceAlertMapper;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link Page} of {@link DeviceAlertDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DeviceAlertDTO> findByCriteria(DeviceAlertCriteria criteria, Pageable page) {
        log.debug("find "+ENTITY_NAME+" by criteria : {}, page: {}", criteria, page);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        LongFilter deviceIdFilter = criteria.getDeviceId();

        switch (currentUsersAuthority) {
            case ADMIN: break;
            case FAMILY: {
                List<Long> userExtraIdList = getUserExtraIdsFromListOfAssociatedUsers(userExtraRepository.findByFamily(currentUser));
                userExtraIdList.add(currentUserExtraDto.getId());//adding current users id

                List<Long> listOfDeviceIds = getListOfDeviceIdsForUserExtraIds(userExtraIdList);

                LongFilter longFilter = longFilterUtil.seIdFilter(listOfDeviceIds, deviceIdFilter);
                criteria.setDeviceId(longFilter);
                break;
            }
            case ORGANIZIATION: {
                List<Long> userExtraIdList = getUserExtraIdsFromListOfAssociatedUsers(userExtraRepository.findByOrganization(currentUser));
                userExtraIdList.add(currentUserExtraDto.getId());//adding current users id

                List<Long> listOfDeviceIds = getListOfDeviceIdsForUserExtraIds(userExtraIdList);

                LongFilter longFilter = longFilterUtil.seIdFilter(listOfDeviceIds, deviceIdFilter);
                criteria.setDeviceId(longFilter);
                break;
            }
            default: {
                List<Long> userExtraIdList = Collections.singletonList(currentUserExtraDto.getId());
                List<Long> listOfDeviceIds = getListOfDeviceIdsForUserExtraIds(userExtraIdList);

                longFilterUtil.seIdFilter(listOfDeviceIds, deviceIdFilter);
                criteria.setDeviceId(deviceIdFilter);
            }
        }
        final Specification<DeviceAlert> specification = createSpecification(criteria);
        return deviceAlertRepository.findAll(specification, page).map(deviceAlertMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DeviceAlertCriteria criteria) {
        log.debug("count "+ENTITY_NAME+" by criteria : {}", criteria);
        final Specification<DeviceAlert> specification = createSpecification(criteria);
        return deviceAlertRepository.count(specification);
    }

    /**
     * Function to convert DeviceAlertCriteria to a {@link Specification}
     */
    private Specification<DeviceAlert> createSpecification(DeviceAlertCriteria criteria) {
        Specification<DeviceAlert> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DeviceAlert_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), DeviceAlert_.label));
            }
            if (criteria.getValues() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValues(), DeviceAlert_.values));
            }
            if (criteria.getAlertType() != null) {
                specification = specification.and(buildSpecification(criteria.getAlertType(), DeviceAlert_.alertType));
            }
            if (criteria.getUserRead() != null) {
                specification = specification.and(buildSpecification(criteria.getUserRead(), DeviceAlert_.userRead));
            }
            if (criteria.getFamilyRead() != null) {
                specification = specification.and(buildSpecification(criteria.getFamilyRead(), DeviceAlert_.familyRead));
            }
            if (criteria.getOrganizationRead() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganizationRead(), DeviceAlert_.organizationRead));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),DeviceAlert_.createdDate));
            }
            if (criteria.getDeviceId() != null) {
                specification = specification.and(buildSpecification(criteria.getDeviceId(),
                    root -> root.join(DeviceAlert_.device, JoinType.LEFT).get(Device_.id)));
            }
        }
        return specification;
    }

    private List<Long> getListOfDeviceIdsForUserExtraIds(List<Long> userExtraIdList) {
        return deviceRepository.findAllByUserExtraIdIn(userExtraIdList)
            .stream()
            .map(Device::getId)
            .collect(Collectors.toList());
    }

    private List<Long> getUserExtraIdsFromListOfAssociatedUsers(List<UserExtra> byFamily) {
        return byFamily
            .stream()
            .map(UserExtra::getId)
            .collect(Collectors.toList());
    }
}
