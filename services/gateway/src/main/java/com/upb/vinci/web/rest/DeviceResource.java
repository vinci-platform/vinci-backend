package com.upb.vinci.web.rest;

import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.errors.NoUserForTokenException;
import com.upb.vinci.security.SecurityUtils;
import com.upb.vinci.service.DeviceQueryService;
import com.upb.vinci.service.DeviceService;
import com.upb.vinci.service.dto.DeviceCriteria;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Device.
 */
@RestController
@RequestMapping("/api")
public class DeviceResource {

    private final Logger log = LoggerFactory.getLogger(DeviceResource.class);

    private static final String ENTITY_NAME = "device";

    private final DeviceService deviceService;

    private final DeviceQueryService deviceQueryService;

    public DeviceResource(DeviceService deviceService, DeviceQueryService deviceQueryService) {
        this.deviceService = deviceService;
        this.deviceQueryService = deviceQueryService;
    }

    /**
     * POST  /devices : Create a new device.
     *
     * @param deviceDTO the deviceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deviceDTO, or with status 400 (Bad Request) if the device has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/devices")
    public ResponseEntity<DeviceDTO> createDevice(@Valid @RequestBody DeviceDTO deviceDTO) throws URISyntaxException {
        log.debug("REST request to save Device : {}", deviceDTO);
        DeviceDTO result = deviceService.save(deviceDTO);
        return ResponseEntity.created(new URI("/api/devices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /devices : Updates an existing device.
     *
     * @param deviceDTO the deviceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deviceDTO,
     * or with status 400 (Bad Request) if the deviceDTO is not valid,
     * or with status 500 (Internal Server Error) if the deviceDTO couldn't be updated
     */
    @PutMapping("/devices")
    public ResponseEntity<DeviceDTO> updateDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        log.debug("REST request to update Device : {}", deviceDTO);
        DeviceDTO result = deviceService.update(deviceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deviceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /devices : get all the devices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of devices in body
     */
    @GetMapping("/devices")
    public ResponseEntity<List<DeviceDTO>> getAllDevices(Pageable pageable, DeviceCriteria criteria) {
        log.debug("REST request to get all Devices");
        Page<DeviceDTO> page = deviceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/devices");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /devices/type : get all watch the devices.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of devices in body
     */
    @GetMapping("/devices/type")
    public ResponseEntity<List<DeviceDTO>> getAllDevicesByType(@RequestParam(value = "deviceType") String deviceType) {
        log.debug("REST request to get all Devices by type");
        final List<DeviceDTO> devices = deviceService.findAllByDeviceType(deviceType);
        return ResponseEntity.ok().body(devices);
    }

    /**
     * GET /my-devices : get a list of devices corresponding to the logged user
     *
     * @return the ResponseEntity with status 200 (OK) and the list of devices in the body
     */
    @GetMapping(value = {"/user-devices/{login}", "/user-devices/"})
    public ResponseEntity<List<DeviceDTO>> getMyDevices(@PathVariable Optional<String> login) {
        log.debug("REST request to get devices for login {}", login);
        String userLogin;
        if (login.isPresent()) {
            userLogin = login.get();
        } else {
            Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
            if (!currentUserLogin.isPresent()) {
                throw new NoUserForTokenException(String.format("You are not authenticated. Cannot retrieve list of associated devices %s not logged in",ENTITY_NAME));
            } else {
                userLogin = currentUserLogin.get();
            }
        }
        return ResponseEntity.ok()
            .body(deviceService.findAllByLogin(userLogin));
    }

    /**
     * GET  /devices/:id : get the "id" device.
     *
     * @param id the id of the deviceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deviceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/devices/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable Long id) {
        log.debug("REST request to get Device by id: {}", id);
        return ResponseEntity.ok(deviceService.findOne(id));
    }

    /**
     * GET  /devices/uuid/:uuid : get the "uuid" device (used by ioserver feign client).
     *
     * @param uuid the uuid of the deviceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deviceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/devices/uuid")
    public ResponseEntity<DeviceDTO> getDevice(@RequestParam(value = "uuid") String uuid) {
        log.debug("REST request to get Device by uuid: {}", uuid);
        return ResponseEntity.ok(deviceService.findOneByUuid(uuid));
    }

    /**
     * GET  /devices/:id : get the "id" device (used by ioserver feign client).
     *
     * @param id the id of the deviceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deviceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/devices/id")
    public ResponseEntity<DeviceDTO> getDeviceById(@RequestParam(value = "id") Long id) {
        log.debug("REST request to get Device by id: {}", id);
        return ResponseEntity.ok(deviceService.findOne(id));
    }

    /**
     * GET  /devices/getSurveyDevice : get the id of Survey device by userId (used by survey feign client).
     *
     * @param userId the id of the owner of Survey device
     * @return the ResponseEntity with status 200 (OK) and with body the deviceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/devices/getSurveyDevice/userId")
    public Long getSurveyDevice(@RequestParam(value = "userId") Long userId) {
        log.debug("REST request to get SurveyDevice : {}", userId);
        List<DeviceDTO> devices = deviceService.findOneSurveyDeviceByUserId(userId);
        return devices.get(0).getId();

    }

    /**
     * GET  /devices/getFitbitWatchDevice : get the FitbitWatch  device by userExtraId
     *
     * @param userExtraId the id of the owner of FitbitWatch device
     * @return the ResponseEntity with status 200 (OK) and with body list with DeviceDTO that is FITBIT_WATCH, or with status 404 (Not Found)
     */
    @GetMapping("/devices/getFitbitWatchDevice/userExtraId")
    public List<DeviceDTO> getFitbitWatchDeviceByUserExtraId(@RequestParam(value = "userExtraId") Long userExtraId) {
        log.debug("REST request to get FitbitWatchDevices with userExtraId: {}", userExtraId);
        return deviceService.findFitbitWatchDevicesByUserExtraId(userExtraId);
    }

    /**
     * GET  /devices/getFitbitWatchDevice : get the FitbitWatch  device by userExtraId (used by fitbit feign client).
     * This endpoint should only be used for fitbitwatch service because there is no auth token when sending request from fitbitwatch service
     * @param userExtraId the id of the owner of FitbitWatch device
     * @return the ResponseEntity with status 200 (OK) and with body list with DeviceDTO that is FITBIT_WATCH, or with status 404 (Not Found)
     */
    @GetMapping("/devices/getFitbitDevice/userExtraId")
    public List<DeviceDTO> getFitbitDeviceByUserExtraId(@RequestParam(value = "userExtraId") Long userExtraId) {
        log.debug("REST request to get personal FitbitWatchDevices with userExtraId: {}", userExtraId);
        return deviceService.getPersonalFitbitDeviceById(userExtraId);
    }

    /**
     * POST  /devices : Create a new device.
     *
     * This endpoint can only be reached if the request is from the same host, look in spring security configuration SecurityConfiguration.java
     *
     * @param deviceDTO the deviceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deviceDTO, or with status 400 (Bad Request) if the device has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/devices/createFitbitDevice")
    public ResponseEntity<DeviceDTO> createFitbitDevice(@Valid @RequestBody DeviceDTO deviceDTO) throws URISyntaxException {
        log.debug("REST request to save fitbitdevice Device : {}", deviceDTO);
        if (deviceDTO.getId() != null) {
            throw new BadRequestAlertException("A new device cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (deviceDTO.getUserExtraId() == null) {
            throw new BadRequestAlertException("UserExtraId must be set in order for creation of fitbit device!",ENTITY_NAME,"idnull");
        }
        DeviceDTO result = deviceService.justSave(deviceDTO);
        return ResponseEntity.created(new URI("/api/devices/createFitbitDevice" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /devices : Updates an existing device.
     *
     * This endpoint can only be reached if the request is from the same host, look in spring security configuration SecurityConfiguration.java
     *
     * @param deviceDTO the deviceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deviceDTO,
     * or with status 400 (Bad Request) if the deviceDTO is not valid,
     * or with status 500 (Internal Server Error) if the deviceDTO couldn't be updated
     */
    @PutMapping("/devices/updateFitbitDevice")
    public ResponseEntity<DeviceDTO> updateFitbitDevice(@Valid @RequestBody DeviceDTO deviceDTO)  {
        log.debug("REST request to update fitbit Device : {}", deviceDTO);
        if (deviceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (deviceDTO.getUserExtraId() == null) {
            throw new BadRequestAlertException("UserExtraId must be set in order for update of fitbit device!",ENTITY_NAME,"idnull");
        }
        DeviceDTO result = deviceService.justUpdate(deviceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deviceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /devices/count : count all the devices.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count in body
     */
    @GetMapping("/devices/count")
    public ResponseEntity<Long> countDevices(DeviceCriteria criteria) {
        log.debug("REST request to count Devices by criteria: {}", criteria);
        return ResponseEntity.ok().body(deviceQueryService.countByCriteria(criteria));
    }

    /**
     * DELETE  /devices/:id : delete the "id" device.
     *
     * @param id the id of the deviceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/devices/{id}")
    public ResponseEntity<Void> deleteDevice(@PathVariable Long id) {
        log.debug("REST request to delete Device : {}", id);
        deviceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /devices : get all the devices.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of devices in body
     */
    @GetMapping("/devices/types")
    public List<DeviceType> getDeviceTypes() {
        log.debug("REST request to get all device types");
        return Arrays.asList(DeviceType.values());
    }
}

