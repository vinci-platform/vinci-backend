package com.upb.vinci.domain.enumeration;

/**
 * The AlertType enumeration.
 */
public enum AlertType {
    SUCCESS, INFO, WARNING, DANGER
}
