package com.upb.vinci.service;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.service.dto.UserExtraDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing UserExtra.
 */
public interface UserExtraService {

    /**
     * Save a userExtra.
     *
     * @param userExtraDTO the entity to save
     * @return the persisted entity
     */
    UserExtraDTO justSave(UserExtraDTO userExtraDTO);

    /**
     * Get the "id" userExtra.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<UserExtraDTO> justFindOne(Long id);
    UserExtraDTO findOne(Long id);

    /**
     * Delete the "id" userExtra.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    Page<UserExtraDTO> getAllManagedUserExtras(Pageable pageable);

    UserExtraDTO findByUserId(Long userId);

    UserExtraDTO update(UserExtraDTO userExtraDTO);

    UserExtra createUserExtraForUser(User user);
}
