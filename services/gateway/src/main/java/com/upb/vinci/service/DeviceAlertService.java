package com.upb.vinci.service;

import com.upb.vinci.service.dto.DeviceAlertDTO;

/**
 * Service Interface for managing DeviceAlert.
 */
public interface DeviceAlertService {

    /**
     * Save a deviceAlert.
     *
     * @param deviceAlertDTO the entity to save
     * @return the persisted entity
     */
    DeviceAlertDTO save(DeviceAlertDTO deviceAlertDTO) throws Exception;

    /**
     * Get the "id" deviceAlert.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DeviceAlertDTO findOne(Long id);

    /**
     * Delete the "id" deviceAlert.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    DeviceAlertDTO update(DeviceAlertDTO deviceAlertDTO);
}
