package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.*;
import com.upb.vinci.service.dto.EventRecordDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EventRecord and its DTO EventRecordDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventRecordMapper extends EntityMapper<EventRecordDTO, EventRecord> {



    default EventRecord fromId(Long id) {
        if (id == null) {
            return null;
        }
        EventRecord eventRecord = new EventRecord();
        eventRecord.setId(id);
        return eventRecord;
    }
}
