package com.upb.vinci.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfiguration {

    //TOPIC_FITBIT_DEVICE_UPDATE
    @Value("${kafka.topic.fitbit-device-update.num-partitions}")
    int fitbitDeviceUpdate_numPartitions;
    @Value("${kafka.topic.fitbit-device-update.replicas}")
    short fitbitDeviceUpdate_replicas;

    @Bean
    public NewTopic fitbitDeviceUpdateTopic() {
        return new NewTopic(TopicConstants.TOPIC_FITBIT_DEVICE_UPDATE,fitbitDeviceUpdate_numPartitions, fitbitDeviceUpdate_replicas);
    }

}
