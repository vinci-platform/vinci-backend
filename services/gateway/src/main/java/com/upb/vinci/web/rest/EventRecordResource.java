package com.upb.vinci.web.rest;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.EventRecordQueryService;
import com.upb.vinci.service.EventRecordService;
import com.upb.vinci.service.dto.EventRecordCriteria;
import com.upb.vinci.service.dto.EventRecordDTO;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing EventRecord.
 */
@RestController
@RequestMapping("/api")
public class EventRecordResource {

    private final Logger log = LoggerFactory.getLogger(EventRecordResource.class);

    private static final String ENTITY_NAME = "eventRecord";

    private final EventRecordService eventRecordService;

    private final EventRecordQueryService eventRecordQueryService;

    public EventRecordResource(EventRecordService eventRecordService, EventRecordQueryService eventRecordQueryService) {
        this.eventRecordService = eventRecordService;
        this.eventRecordQueryService = eventRecordQueryService;
    }

    /**
     * POST  /event-records : Create a new eventRecord.
     *
     * @param eventRecordDTO the eventRecordDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eventRecordDTO, or with status 400 (Bad Request) if the eventRecord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/event-records")
    public ResponseEntity<EventRecordDTO> createEventRecord(@RequestBody @Valid EventRecordDTO eventRecordDTO) throws URISyntaxException {
        log.debug("REST request to save EventRecord : {}", eventRecordDTO);
        EventRecordDTO result = eventRecordService.save(eventRecordDTO);
        return ResponseEntity.created(new URI("/api/event-records/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /event-records : Updates an existing eventRecord.
     *
     * @param eventRecordDTO the eventRecordDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated eventRecordDTO,
     * or with status 400 (Bad Request) if the eventRecordDTO is not valid,
     * or with status 500 (Internal Server Error) if the eventRecordDTO couldn't be updated
     */
    @PutMapping("/event-records")
    public ResponseEntity<EventRecordDTO> updateEventRecord(@RequestBody @Valid EventRecordDTO eventRecordDTO) {
        log.debug("REST request to update EventRecord : {}", eventRecordDTO);
        EventRecordDTO result = eventRecordService.update(eventRecordDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, eventRecordDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /event-records : get all the eventRecords.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of eventRecords in body
     */
    @GetMapping("/event-records")
    public ResponseEntity<List<EventRecordDTO>> getAllEventRecords(EventRecordCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EventRecords by criteria: {}", criteria);
        Page<EventRecordDTO> page = eventRecordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/event-records");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /event-records/:id : get the "id" eventRecord.
     *
     * @param id the id of the eventRecordDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the eventRecordDTO, or with status 404 (Not Found)
     */
    @GetMapping("/event-records/{id}")
    public ResponseEntity<EventRecordDTO> getEventRecord(@PathVariable Long id) {
        log.debug("REST request to get EventRecord : {}", id);
        return ResponseEntity.ok(eventRecordService.findOne(id));
    }

    /**
     * DELETE  /event-records/:id : delete the "id" eventRecord.
     *
     * ONLY ADMIN CAN DELETE ENTITIES
     *
     * @param id the id of the eventRecordDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/event-records/{id}")
    public ResponseEntity<Void> deleteEventRecord(@PathVariable Long id) {
        log.debug("REST request to delete EventRecord : {}", id);
        eventRecordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
    * GET  /event-records/count : count all the eventRecords.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/event-records/count")
    public ResponseEntity<Long> countEventRecords(EventRecordCriteria criteria) {
        log.debug("REST request to count EventRecords by criteria: {}", criteria);
        return ResponseEntity.ok().body(eventRecordQueryService.countByCriteria(criteria));
    }
}
