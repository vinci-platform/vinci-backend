package com.upb.vinci.web.rest;

import com.upb.vinci.config.Constants;
import com.upb.vinci.domain.User;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.UserService;
import com.upb.vinci.service.dto.FirstAndLastNameWithUuid;
import com.upb.vinci.service.dto.UserAggregateDTO;
import com.upb.vinci.service.dto.UserDTO;
import com.upb.vinci.service.dto.UserPhoneDto;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import com.upb.vinci.web.rest.errors.EmailAlreadyUsedException;
import com.upb.vinci.web.rest.errors.LoginAlreadyUsedException;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    /**
     * POST  /users  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param userDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email is already in use
     */
    @PostMapping("/users")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("REST request to save User : {}", userDTO);

        userDTO.setActivated(true);
        User newUser = userService.createUser(userDTO);
        return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
            .headers(HeaderUtil.createAlert( "userManagement.created", newUser.getLogin()))
            .body(newUser);
    }

    /**
     * POST  /users/organisation  : Creates a new user from an organisation
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link if the organisation required creating a proper user account.
     * The user needs to be activated on creation only if the organisation required a proper user account.
     *
     * @param userDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email is already in use
     */
    @PostMapping("/users/organisation")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ORGANIZIATION + "\")")
    public ResponseEntity<User> createUserOrganisation(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("REST request to save User with organization account : {}", userDTO);
        User newUser = userService.registerUserForOrganisation(userDTO);
        return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
            .headers(HeaderUtil.createAlert("userManagement.created", newUser.getLogin()))
            .body(newUser);
    }

    /**
     * PUT /users : Updates an existing User.
     * Only admin can use this endpoint
     *
     * @param userDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already in use
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already in use
     */
    @PutMapping("/users")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);

        return ResponseUtil.wrapOrNotFound(userService.updateUser(userDTO),
            HeaderUtil.createAlert("userManagement.updated", userDTO.getLogin()));
    }

    /**
     * GET /users : get all users.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * @return a string list of the all of the roles
     */
    @GetMapping("/users/authorities")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }

    /**
     * DELETE /users/:login : delete the "login" User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "userManagement.deleted", login)).build();
    }

    /**
     * GET /users/:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login" user, or with status 404 (Not Found)
     */
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseEntity.ok(userService.getUserWithAuthoritiesByLogin(login));
    }

    /**
     * GET  /users/aggregated : get all the associated users' info plus their status for current user
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aggregated users in body
     */
    @GetMapping("/users/aggregated")
    public ResponseEntity<List<UserAggregateDTO>> getAssociatedUsers(Pageable pageable) {
        log.debug("REST request to get all associated users' info and status");
        return ResponseEntity.ok(userService.getAssociatedUsers(pageable));
    }

    //todo: fix method so it does not have data leeks
    @GetMapping("/users/getPhones")
    public ResponseEntity<List<UserPhoneDto>> getUserPhones() {
        log.debug("REST request to get all users and their phones, /users/getPhones");
        return ResponseEntity.ok().body(userService.getUserPhones());
    }

    @GetMapping("/users/getUserWithAuthorities")
    public Optional<com.upb.vinci.web.beans.UserDTO> getUserWithAuthorities(){
        log.debug("REST request to get user with authorities, /users/getUserWithAuthorities");
        return userService.getBeansUserDtoWithAuthorities();
    }

    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.FAMILY + ", " + AuthoritiesConstants.ORGANIZIATION + "\")")
    @GetMapping("/users/getFirstAndLastNameWithUuid")
    public ResponseEntity<List<FirstAndLastNameWithUuid>> getFirstAndLastNameWithUuid(){
        log.debug("REST request to get firstAndLastNameWithUuid, /users/getFirstAndLastNameWithUuid");
        return ResponseEntity.ok().body(userService.getFirstAndLastNameWithUuid());
    }

}
