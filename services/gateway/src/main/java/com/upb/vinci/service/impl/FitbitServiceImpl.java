package com.upb.vinci.service.impl;

import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoUserExtraForId;
import com.upb.vinci.errors.fitbit.NoUserExtraForIdFromFitbitState;
import com.upb.vinci.errors.fitbit.StateFromFitbitServersNotPresent;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.AuthorizationService;
import com.upb.vinci.service.FitbitService;
import com.upb.vinci.service.UserExtraService;
import com.upb.vinci.service.dto.FitbitRedirectUrl;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.web.rest.feign.client.FitbitWatchClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class FitbitServiceImpl implements FitbitService {

    @Value("${fitbit.api.redirect-url}")
    String redirectUrl;

    @Value("${fitbit.api.auth}")
    String authUrl;

    @Value("${fitbit.api.client-id}")
    String defaultclientId;

    private final FitbitWatchClient fitbitWatchClient;
    private final AuthorizationService authorizationService;
    private final UserExtraRepository userExtraRepository;
    private final UserExtraService userExtraService;

    public FitbitServiceImpl(FitbitWatchClient fitbitWatchClient, AuthorizationService authorizationService, UserExtraRepository userExtraRepository,
                             UserExtraService userExtraService) {
        this.fitbitWatchClient = fitbitWatchClient;
        this.authorizationService = authorizationService;
        this.userExtraRepository = userExtraRepository;
        this.userExtraService = userExtraService;
    }

    @Override
    public FitbitRedirectUrl getFitbitRedirectUrl(Long userExtraId) {
        log.debug("Request to get FitbitRedirectUrl for userExtraId: {}",userExtraId);

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());

        doesUserExtraExist(userExtraId);
        doesUserExtraIdBelongToCurrentUser(currentUserExtraDto,userExtraId);

        if (!currentUserExtraDto.getId().equals(userExtraId)) throw new AuthorizationException("UserExtraId in request must be of the user in jwt token!");

        return new FitbitRedirectUrl(authUrl + "?response_type=code&client_id=" + defaultclientId +
            "&redirect_uri=" + redirectUrl +
            "&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight" +
            "&state=" + userExtraId);
    }

    @Override
    public void authorizeUserWithAuhorizationCode(String code, Optional<Long> state) {
        Long userExtraId = state.orElseThrow(StateFromFitbitServersNotPresent::new);
        userExtraService.justFindOne(userExtraId).orElseThrow(() -> new NoUserExtraForIdFromFitbitState(userExtraId));

        ResponseEntity<String> responseEntity = fitbitWatchClient.getCodeMobile(code, userExtraId);
        if (responseEntity.getStatusCode().equals(HttpStatus.OK)) log.debug(responseEntity.getBody());
        if (responseEntity.getStatusCode().equals(HttpStatus.CONFLICT)) log.info(responseEntity.getBody());
    }

    private void doesUserExtraIdBelongToCurrentUser(UserExtraDTO currentUserExtraDto, Long userExtraId) {
        if (!currentUserExtraDto.getId().equals(userExtraId)) throw new AuthorizationException("UserExtraId in request must be of the user in jwt token!");
    }

    private UserExtra doesUserExtraExist(Long userExtraId) {
        return userExtraRepository.findById(userExtraId).orElseThrow(() -> new NoUserExtraForId(userExtraId));
    }
}
