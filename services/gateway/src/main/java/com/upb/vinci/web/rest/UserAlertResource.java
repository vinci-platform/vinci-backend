package com.upb.vinci.web.rest;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.UserAlertQueryService;
import com.upb.vinci.service.UserAlertService;
import com.upb.vinci.service.dto.UserAlertCriteria;
import com.upb.vinci.service.dto.UserAlertDTO;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing UserAlert.
 */
@RestController
@RequestMapping("/api")
public class UserAlertResource {

    private final Logger log = LoggerFactory.getLogger(UserAlertResource.class);

    private static final String ENTITY_NAME = "userAlert";

    private final UserAlertService userAlertService;

    private final UserAlertQueryService userAlertQueryService;

    public UserAlertResource(UserAlertService userAlertService, UserAlertQueryService userAlertQueryService) {
        this.userAlertService = userAlertService;
        this.userAlertQueryService = userAlertQueryService;
    }

    /**
     * POST  /user-alerts : Create a new userAlert.
     *
     * @param userAlertDTO the userAlertDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userAlertDTO, or with status 400 (Bad Request) if the userAlert has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-alerts")
    public ResponseEntity<UserAlertDTO> createUserAlert(@Valid @RequestBody UserAlertDTO userAlertDTO) throws URISyntaxException {
        log.debug("REST request to save UserAlert : {}", userAlertDTO);
        UserAlertDTO result = userAlertService.save(userAlertDTO);
        return ResponseEntity.created(new URI("/api/user-alerts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-alerts : Updates an existing userAlert.
     *
     * @param userAlertDTO the userAlertDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userAlertDTO,
     * or with status 400 (Bad Request) if the userAlertDTO is not valid,
     * or with status 500 (Internal Server Error) if the userAlertDTO couldn't be updated
     */
    @PutMapping("/user-alerts")
    public ResponseEntity<UserAlertDTO> updateUserAlert(@Valid @RequestBody UserAlertDTO userAlertDTO) {
        log.debug("REST request to update UserAlert : {}", userAlertDTO);
        UserAlertDTO result = userAlertService.update(userAlertDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userAlertDTO.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-alerts : Updates existing userAlerts.
     *
     * @param userAlertDTOs the userAlertDTOs to update
     * @return the ResponseEntity with status 200 (OK),
     * or with status 400 (Bad Request) if the userAlertDTOs are not valid,
     * or with status 500 (Internal Server Error) if the userAlertDTOs couldn't be updated
     */
    @PutMapping("/user-alerts/entities")
    public ResponseEntity<Void> updateUserAlerts(@Valid @RequestBody List<UserAlertDTO> userAlertDTOs) {
        log.debug("REST request to update UserAlerts : {}", userAlertDTOs);
        userAlertService.updateList(userAlertDTOs);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /user-alerts : get all the userAlerts.
     * Admin can get all data, patient only his, family and organization can get data from associated users
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of userAlerts in body
     */
    @GetMapping("/user-alerts")
    public ResponseEntity<List<UserAlertDTO>> getAllUserAlerts(UserAlertCriteria criteria, Pageable pageable) {
        log.debug("REST request to get UserAlerts by criteria: {}", criteria);
        Page<UserAlertDTO> page = userAlertQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-alerts");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /user-alerts/:id : get the "id" userAlert.
     *
     * @param id the id of the userAlertDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userAlertDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-alerts/{id}")
    public ResponseEntity<UserAlertDTO> getUserAlert(@PathVariable Long id) {
        log.debug("REST request to get UserAlert for id: {}", id);
        return ResponseEntity.ok(userAlertService.findOne(id));
    }

    /**
     * DELETE  /user-alerts/:id : delete the "id" userAlert.
     *
     * @param id the id of the userAlertDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/user-alerts/{id}")
    public ResponseEntity<Void> deleteUserAlert(@PathVariable Long id) {
        log.debug("REST request to delete UserAlert : {}", id);
        userAlertService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
    * GET  /user-alerts/count : count all the userAlerts.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/user-alerts/count")
    public ResponseEntity<Long> countUserAlerts(UserAlertCriteria criteria) {
        log.debug("REST request to count UserAlerts by criteria: {}", criteria);
        return ResponseEntity.ok().body(userAlertQueryService.countByCriteria(criteria));
    }
}
