package com.upb.vinci.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.upb.vinci.domain.enumeration.AlertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * A UserAlert.
 */
@Entity
@Table(name = "user_alert")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAlert implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_label", nullable = false)
    private String label;

    @Column(name = "jhi_values")
    private String values;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "alert_type", nullable = false)
    private AlertType alertType;

    @Column(name = "user_read")
    private Boolean userRead;

    @Column(name = "family_read")
    private Boolean familyRead;

    @Column(name = "organization_read")
    private Boolean organizationRead;

    @ManyToOne
    @JoinColumn(name="user_extra_id")
    @JsonIgnoreProperties({"alerts", "images", "devices"})
    private UserExtra userExtra;

    @Column(name = "created_date", insertable = false)
    private Instant createdDate = Instant.now();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public UserAlert label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValues() {
        return values;
    }

    public UserAlert values(String values) {
        this.values = values;
        return this;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public UserAlert alertType(AlertType alertType) {
        this.alertType = alertType;
        return this;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Boolean isUserRead() {
        return userRead;
    }

    public UserAlert userRead(Boolean userRead) {
        this.userRead = userRead;
        return this;
    }

    public void setUserRead(Boolean userRead) {
        this.userRead = userRead;
    }

    public Boolean isFamilyRead() {
        return familyRead;
    }

    public UserAlert familyRead(Boolean familyRead) {
        this.familyRead = familyRead;
        return this;
    }

    public void setFamilyRead(Boolean familyRead) {
        this.familyRead = familyRead;
    }

    public Boolean isOrganizationRead() {
        return organizationRead;
    }

    public UserAlert organizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
        return this;
    }

    public void setOrganizationRead(Boolean organizationRead) {
        this.organizationRead = organizationRead;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public UserAlert userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getUserRead() {
        return userRead;
    }

    public Boolean getFamilyRead() {
        return familyRead;
    }

    public Boolean getOrganizationRead() {
        return organizationRead;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserAlert userAlert = (UserAlert) o;
        if (userAlert.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAlert.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAlert{" +
                "id=" + getId() +
                ", label='" + getLabel() + "'" +
                ", values='" + getValues() + "'" +
                ", alertType='" + getAlertType() + "'" +
                ", userRead='" + isUserRead() + "'" +
                ", familyRead='" + isFamilyRead() + "'" +
                ", organizationRead='" + isOrganizationRead() + "'" +
                "}";
    }
}
