package com.upb.vinci.security.bruteforceprevention;

import com.upb.vinci.domain.User;

public interface BruteForceProtectionService {
    void registerLoginFailure(String username);
    void resetBruteForceCounter(String username);
    void checkAttemptsForUser(User user);
}
