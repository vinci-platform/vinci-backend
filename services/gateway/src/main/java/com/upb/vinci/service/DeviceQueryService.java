package com.upb.vinci.service;

import com.upb.vinci.domain.*;
import com.upb.vinci.repository.DeviceRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.dto.DeviceCriteria;
import com.upb.vinci.service.dto.DeviceDTO;
import com.upb.vinci.service.dto.UserExtraDTO;
import com.upb.vinci.service.mapper.DeviceMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.ArrayList;
import java.util.List;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for Device entities in the database.
 * The main input is a {@link DeviceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DeviceDTO} or a {@link Page} of {@link DeviceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DeviceQueryService extends QueryService<Device> {

    private final String ENTITY_NAME = "device";

    private final Logger log = LoggerFactory.getLogger(DeviceQueryService.class);

    private final DeviceRepository deviceRepository;

    private final DeviceMapper deviceMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public DeviceQueryService(DeviceRepository deviceRepository, DeviceMapper deviceMapper, UserExtraRepository userExtraRepository,
                              AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link List} of {@link DeviceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DeviceDTO> findByCriteria(DeviceCriteria criteria) {
        log.debug("find "+ENTITY_NAME+" by criteria : {}", criteria);
        final Specification<Device> specification = createSpecification(criteria);
        return deviceMapper.toDto(deviceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DeviceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param pageable The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DeviceDTO> findByCriteria(DeviceCriteria criteria, Pageable pageable) {
        log.debug("find "+ENTITY_NAME+" by criteria : {}, page: {}", criteria, pageable);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtraDTO currentUserExtraDto = authorizationService.getCurrentUserExtraDto_ForUserId(currentUser.getId());
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        List<Long> userExtraIdList = new ArrayList<>();
        userExtraIdList.add(currentUserExtraDto.getId());
        LongFilter userExtraIdFilter = criteria.getUserExtraId();
        if (userExtraIdFilter == null) userExtraIdFilter = new LongFilter();

        switch (currentUsersAuthority) {
            case ADMIN: {
                final Specification<Device> specification = createSpecification(criteria);
                return deviceRepository.findAll(specification, pageable).map(deviceMapper::toDto);
            }
            case FAMILY: addAssociatedIdsToList(userExtraIdList, userExtraRepository.findByFamily(currentUser));
            case ORGANIZIATION: addAssociatedIdsToList(userExtraIdList, userExtraRepository.findByOrganization(currentUser));
        }

        criteria.setUserExtraId(longFilterUtil.seIdFilter(userExtraIdList, userExtraIdFilter));
        final Specification<Device> specification = createSpecification(criteria);
        return deviceRepository.findAll(specification, pageable).map(deviceMapper::toDto);
    }

    private void addAssociatedIdsToList(List<Long> userExtraIdList, List<UserExtra> byOrganization) {
        byOrganization
            .forEach(userExtra -> userExtraIdList.add(userExtra.getId()));
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DeviceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Device> specification = createSpecification(criteria);
        return deviceRepository.count(specification);
    }

    /**
     * Function to convert DeviceCriteria to a {@link Specification}
     */
    private Specification<Device> createSpecification(DeviceCriteria criteria) {
        Specification<Device> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Device_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Device_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Device_.description));
            }
            if (criteria.getUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuid(), Device_.uuid));
            }
            if (criteria.getDeviceType() != null) {
                specification = specification.and(buildSpecification(criteria.getDeviceType(), Device_.deviceType));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), Device_.active));
            }
            if (criteria.getStartTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartTimestamp(), Device_.startTimestamp));
            }
            if (criteria.getAlertId() != null) {
                specification = specification.and(buildSpecification(criteria.getAlertId(),
                    root -> root.join(Device_.alerts, JoinType.LEFT).get(DeviceAlert_.id)));
            }
            if (criteria.getUserExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserExtraId(),
                    root -> root.join(Device_.userExtra, JoinType.LEFT).get(UserExtra_.id)));
            }
        }
        return specification;
    }
}
