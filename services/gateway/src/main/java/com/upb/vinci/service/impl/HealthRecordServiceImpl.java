package com.upb.vinci.service.impl;

import com.upb.vinci.domain.HealthRecord;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoEntityException;
import com.upb.vinci.repository.HealthRecordRepository;
import com.upb.vinci.repository.UserExtraRepository;
import com.upb.vinci.service.AuthorizationService;
import com.upb.vinci.service.HealthRecordService;
import com.upb.vinci.service.dto.HealthRecordDTO;
import com.upb.vinci.service.mapper.HealthRecordMapper;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service Implementation for managing HealthRecord.
 */
@Service
@Transactional
public class HealthRecordServiceImpl implements HealthRecordService {

    private static final String ENTITY_NAME = "healthRecord";

    private final Logger log = LoggerFactory.getLogger(HealthRecordServiceImpl.class);

    private final HealthRecordRepository healthRecordRepository;

    private final HealthRecordMapper healthRecordMapper;

    private final UserExtraRepository userExtraRepository;

    private final AuthorizationService authorizationService;

    public HealthRecordServiceImpl(HealthRecordRepository healthRecordRepository, HealthRecordMapper healthRecordMapper, UserExtraRepository userExtraRepository,
                                   AuthorizationService authorizationService) {
        this.healthRecordRepository = healthRecordRepository;
        this.healthRecordMapper = healthRecordMapper;
        this.userExtraRepository = userExtraRepository;
        this.authorizationService = authorizationService;
    }

    /**
     * Save a healthRecord.
     *
     * @param healthRecordDTO the entity to save
     * @return the persisted entity or null if there is exception
     */
    @Override
    public HealthRecordDTO save(HealthRecordDTO healthRecordDTO) {
        log.debug("Request to save HealthRecord : {}", healthRecordDTO);
        validateHealthRecordForCreation(healthRecordDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
            case FAMILY: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByFamily(currentUser))) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only create entity "+ENTITY_NAME+" for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only create entity "+ENTITY_NAME+" for your user or users associated to you!");
            }
            default: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only create "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    /**
     * Get one healthRecord by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public HealthRecordDTO findOne(Long id) {
        log.debug("Request to get HealthRecord  by id: {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        HealthRecordDTO healthRecordDTO = healthRecordRepository.findById(id)
            .map(healthRecordMapper::toDto)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        switch (currentUsersAuthority) {
            case ADMIN: return healthRecordDTO;
            case FAMILY: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByFamily(currentUser))) return healthRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByOrganization(currentUser))) return healthRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            default: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())) return healthRecordDTO;
                throw new AuthorizationException("You can only get "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    /**
     * Delete the healthRecord by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HealthRecord : {}", id);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        HealthRecord healthRecordById = healthRecordRepository.findById(id)
            .orElseThrow(() -> new NoEntityException(id,ENTITY_NAME));

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY) ||
            healthRecordById.getUserId().equals(currentUser.getId())) {
            healthRecordRepository.deleteById(id);
        } else {
            throw new AuthorizationException(ENTITY_NAME + " can be deleted by user to whom it belongs to or by admin! Use correct users jwt token!");
        }
    }

    /**
     * Update a healthRecord.
     *
     * @param healthRecordDTO the entity to save
     * @return the persisted entity or null if there is exception
     */
    @Override
    public HealthRecordDTO update(HealthRecordDTO healthRecordDTO) {
        log.debug("Request to update HealthRecord : {}", healthRecordDTO);
        validateHealthrecordDtoForUpdate(healthRecordDTO);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        String currentUsersAuthority = authorizationService.getUserAuthorityString(currentUser);

        switch (currentUsersAuthority) {
            case ADMIN: return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
            case FAMILY: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByFamily(currentUser))) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            case ORGANIZIATION: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())
                    || isRecordAssociatedToUsers(healthRecordDTO, userExtraRepository.findByOrganization(currentUser))) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only create "+ENTITY_NAME+" entity for your user or users associated to you!");
            }
            default: {
                if (healthRecordDTO.getUserId().equals(currentUser.getId())) {
                    return healthRecordMapper.toDto(healthRecordRepository.save(healthRecordMapper.toEntity(healthRecordDTO)));
                } throw new AuthorizationException("You can only update "+ENTITY_NAME+" entity for your user!");
            }
        }
    }

    private boolean isRecordAssociatedToUsers(HealthRecordDTO healthRecordDTO, List<UserExtra> userExtraList) {
        return userExtraList
            .stream()
            .map(userExtra -> userExtra.getUser().getId())
            .anyMatch(userId -> userId.equals(healthRecordDTO.getUserId()));
    }

    private void validateHealthRecordForCreation(HealthRecordDTO healthRecordDTO) {
        if (healthRecordDTO.getId() != null) {
            throw new BadRequestAlertException("A new healthRecord cannot already have an ID", ENTITY_NAME, "idexists");
        }
    }

    private void validateHealthrecordDtoForUpdate(HealthRecordDTO healthRecordDTO) {
        if (healthRecordDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

}
