package com.upb.vinci.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FirstAndLastNameWithUuid {

    private Long id;
    private String firstName;
    private String lastName;
    private String uuid;

}
