package com.upb.vinci.errors;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ShoeDataInDatabaseNotValid extends AbstractThrowableProblem {

    private static final String TITLE = "SHOE_DATA_IN_DATABASE_NOT_VALID_EXCEPTION";
    private static final Status STATUS_TYPE = Status.INTERNAL_SERVER_ERROR;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "Shoe data in database is not valid!";
    private static final String DETAILS_WITH_DATA = "Shoe data in database is not valid! ShoeData: %s";

    public ShoeDataInDatabaseNotValid(){ super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS); }
    public ShoeDataInDatabaseNotValid(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "ShoeDataInDatabaseNotValid{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
