package com.upb.vinci.service;

import com.upb.vinci.config.Constants;
import com.upb.vinci.domain.Authority;
import com.upb.vinci.domain.Device;
import com.upb.vinci.domain.User;
import com.upb.vinci.domain.UserExtra;
import com.upb.vinci.domain.enumeration.AlertType;
import com.upb.vinci.domain.enumeration.DeviceType;
import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.errors.NoUserByLoginException;
import com.upb.vinci.errors.NoUserExtraForUserIdException;
import com.upb.vinci.repository.*;
import com.upb.vinci.security.SecurityUtils;
import com.upb.vinci.service.dto.*;
import com.upb.vinci.service.mapper.DeviceMapper;
import com.upb.vinci.service.mapper.UserExtraMapper;
import com.upb.vinci.service.mapper.UserMapper;
import com.upb.vinci.service.util.RandomUtil;
import com.upb.vinci.web.rest.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    public static final String ENTITY_NAME = "user";

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final UserExtraRepository userExtraRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    private final MailService mailService;

    private final UserAlertService userAlertService;

    private final UserExtraMapper userExtraMapper;

    private final DeviceRepository deviceRepository;

    private final DeviceMapper deviceMapper;

    private final AuthorizationService authorizationService;

    private final UserAlertRepository userAlertRepository;

    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserExtraRepository userExtraRepository,
                       PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository,
                       CacheManager cacheManager, MailService mailService, @Lazy UserAlertService userAlertService,
                       UserExtraMapper userExtraMapper, DeviceRepository deviceRepository, DeviceMapper deviceMapper,
                       @Lazy AuthorizationService authorizationService, UserAlertRepository userAlertRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userExtraRepository = userExtraRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.mailService = mailService;
        this.userAlertService = userAlertService;
        this.userExtraMapper = userExtraMapper;
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
        this.authorizationService = authorizationService;
        this.userAlertRepository = userAlertRepository;
        this.userMapper = userMapper;
    }

    public void activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                return user;
            }).orElseThrow(() -> new BadRequestAlertException("There is no user for Activation key", ENTITY_NAME, "noUserForActivationKey"));
    }

    public Optional<User> completePasswordReset(String newPassword, String key, Boolean activate) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                validatePassword(user.getLogin(), newPassword);

                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                if (activate) {
                    user.setActivated(true);
                    UserExtra userExtra = userExtraRepository.findByUserId(user.getId());

                    UserAlertDTO initUserAlert = new UserAlertDTO();
                    initUserAlert.setUserExtraId(userExtra.getId());
                    initUserAlert.setValues("");
                    initUserAlert.setAlertType(AlertType.SUCCESS);
                    initUserAlert.setLabel("ok");
                    userAlertService.justSave(initUserAlert);

                    mailService.sendAssociationSuccessEmail(userExtra.getOrganization(), userExtra);
                }
                this.clearUserCaches(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        validateUserRegistration(userDTO, password);

        User newUser = getNewUser(userDTO, password);
        User savedUser = userRepository.save(newUser);

        UserExtra newUserExtra = createNewUserExtra(userDTO, newUser, savedUser);

        createSurveyDeviceForUserExtra(newUserExtra);
        createFitbitWatchDeviceForUserExtra(newUserExtra);

        return newUser;
    }

    @Transactional
    public User registerUserForOrganisation(UserDTO userDTO) {
        userDTO.setLogin(userDTO.getFirstName().trim() + "." + userDTO.getLastName().replace(" ", ""));

        validateUserDtoForCreation(userDTO);

        if(userDTO.getEmail() == null || userDTO.getEmail().length() == 0) {
            userDTO.setEmail(userDTO.getLogin() + "@anonymous.com");
            userDTO.setActivated(true);
        } else {
            userDTO.setActivated(false);
        }

        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        User newUser = createUser(userDTO);
        UserExtra newUserExtra = userExtraRepository.findByUserId(newUser.getId());

        createInitUserAlertForUserExtra(newUserExtra);

        if (newUser.getActivated())
            mailService.sendCreationEmailWithPassword(newUser, currentUser.getFirstName() + " " + currentUser.getLastName());

        associateUser(currentUser.getId(), newUser.getId(), false, newUser.getActivated());

        return newUser;
    }

    private void createInitUserAlertForUserExtra(UserExtra userExtra) {
        UserAlertDTO initUserAlert = new UserAlertDTO();
        initUserAlert.setUserExtraId(userExtra.getId());
        initUserAlert.setValues("");
        if (userExtra.getUser().getEmail() != null && userExtra.getUser().getEmail().length() > 0) {
            initUserAlert.setAlertType(AlertType.WARNING);
            initUserAlert.setLabel("pending");
        } else {
            initUserAlert.setAlertType(AlertType.SUCCESS);
            initUserAlert.setLabel("ok");
        }
        userAlertService.justSave(initUserAlert);
    }

    /**
     * Creates new User.java and new UserExtra.java in database
     * @return created User
     */
    public User createUser(UserDTO userDTO) {
        validateUserDtoForCreation(userDTO);

        User user = createUserFromDto(userDTO);
        User savedUser = userRepository.save(user);

        UserExtra newUserExtra = createDummyUserExtraWithSavedUser(savedUser);
        userExtraRepository.save(newUserExtra);

        this.clearUserCaches(user);
        mailService.sendCreationEmail(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email.toLowerCase());
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * First Validates login and mail
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        log.debug("updateUser(userDto), userDto: {}", userDTO);
        validateUserDtoForUpdate(userDTO);

        return userRepository.findById(userDTO.getId())
            .map(user -> {
                this.clearUserCaches(user);
                user.setLogin(userDTO.getLogin() != null ? userDTO.getLogin().toLowerCase() : user.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName() != null ? userDTO.getFirstName() : user.getFirstName());
                user.setLastName(userDTO.getLastName() != null ? userDTO.getLastName() : user.getLastName());
                user.setEmail(userDTO.getEmail() != null ? userDTO.getEmail().toLowerCase() : user.getEmail().toLowerCase());
                user.setImageUrl(userDTO.getImageUrl() != null ? userDTO.getImageUrl() : user.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey() != null ? userDTO.getLangKey() : user.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                if (userDTO.getAuthorities() != null) {
                    managedAuthorities.clear();
                    userDTO.getAuthorities().stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                }
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public Optional<UserDTO> updateUserByUser(UserDTO userDTO) {
        log.debug("updateUserByUser(userDto), userDto: {}", userDTO);
           UserDTO authUserDTO = this.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new InternalServerErrorException("User could not be found"));

	    UserExtra userExtra;
	    userExtra = userExtraRepository.findByUserId(authUserDTO.getId());
	    return Optional.of(userRepository
            .findById(authUserDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this.clearUserCaches(user);
                user.setLogin(userDTO.getLogin()!= null ? userDTO.getLogin().toLowerCase() : user.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName()!= null ? userDTO.getFirstName() : user.getFirstName());
		        user.setLastName(userDTO.getLastName()!= null ? userDTO.getLastName() : user.getLastName());
                user.setEmail(userDTO.getEmail()!= null ? userDTO.getEmail().toLowerCase() : user.getEmail().toLowerCase());
                user.setImageUrl(userDTO.getImageUrl()!= null ? userDTO.getImageUrl() : user.getImageUrl());
                user.setActivated(true);
                user.setLangKey(userDTO.getLangKey()!= null ? userDTO.getLangKey() : user.getLangKey());
                userExtra.setPhone(userDTO.getPhone()!= null ? userDTO.getPhone() : userExtra.getPhone());
		        userExtra.setAddress(userDTO.getAddress()!= null ? userDTO.getAddress() : userExtra.getAddress());
		        userExtra.setGender(userDTO.getGender()!= null ? userDTO.getGender() : userExtra.getGender());
		        userExtra.setUuid(userDTO.getUuid()!= null ? userDTO.getUuid() : userExtra.getUuid());
                userExtra.setMaritalStatus(userDTO.getMaritalStatus() != null ? userDTO.getMaritalStatus() : userExtra.getMaritalStatus());
                userExtra.setEducation(userDTO.getEducation() != null ? userDTO.getEducation() : userExtra.getEducation());
                userExtra.setFriends(userDTO.getFriends() != null ? userDTO.getFriends() : userExtra.getFriends());
		Set<Authority> managedAuthorities = user.getAuthorities();
                if (userDTO.getAuthorities() !=null)
		{
		managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
		}
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            deleteUserExtra(user);
            userRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }


    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                checkIfCurrentPasswordsMatch(currentClearTextPassword, user);
                validatePassword(user.getLogin(), newPassword);

                String newEncryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(newEncryptedPassword);

                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    private void checkIfCurrentPasswordsMatch(String currentClearTextPassword, User user) {
        String currentEncryptedPassword = user.getPassword();
        if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
            throw new InvalidPasswordException();
        }
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        Page<UserDTO> page = new PageImpl<>(new ArrayList<>());
        UserDTO userDTO = authorizationService.getCurrentUserDto_WithAuthorities();
        authorizationService.getCurrentUserExtraDto_ForUserId(userDTO.getId());
        if (userDTO.getAuthorities().contains(ADMIN)) {
            return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
        } else if (userDTO.getAuthorities().contains(FAMILY)) {
            return userRepository
                .findByFamilyId(userDTO.getId(),pageable)
                .map(UserDTO::new);
        } else if (userDTO.getAuthorities().contains(ORGANIZIATION)) {
            return userRepository
                .findByOrganizationId(userDTO.getId(),pageable)
                .map(UserDTO::new);
        } if (userDTO.getAuthorities().contains(PACIENT)) {
            List<UserDTO> list = new ArrayList<>();
            list.add(userDTO);
            page = new PageImpl<>(list);
            return page;
        }
        return page;
    }

    @Transactional(readOnly = true)
    public UserDTO getUserWithAuthoritiesByLogin(String login) {
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        if (currentUser.getLogin().equals(login)){
            return userMapper.userToUserDTO(currentUser);
        }
        User byLogin = userRepository.findOneWithAuthoritiesByLogin(login)
            .orElseThrow(() -> new NoUserByLoginException(login));
        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY)){
            return userMapper.userToUserDTO(byLogin);
        } else{
            throw new AuthorizationException("Only admin can get other peoples user data!");
        }
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired at the start of every month in 01:00 AM
     */
    @Scheduled(cron = "0 0 1 1 * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getLogin());
                userRepository.delete(user);
                this.clearUserCaches(user);
            });
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    public void associateUser(Long userId, Long targetUserId, Boolean isFamily, Boolean isActive) {
        User user = userRepository.findOneById(userId)
            .orElseThrow(() ->  new BadRequestAlertException("User user found for userId: "+userId,"user","noUserForId") );

        UserExtra targetUser = userExtraRepository.findByUserId(targetUserId);
        if (targetUser == null) {
            throw new NoUserExtraForUserIdException(targetUserId);
        }

        if (isFamily) {
            targetUser.setFamily(user);
        } else {
            targetUser.setOrganization(user);
        }

        this.clearUserCaches(targetUser.getUser());
        if (isActive) {
            mailService.sendAssociationSuccessEmail(user, targetUser);
        }
        log.debug("Changed Information for User: {}", targetUser);
    }

    public List<UserAggregateDTO> getAssociatedUsers(Pageable pageable) {
        //checks if there is user in token
        authorizationService.getCurrentUser_WithAuthorities();

        List<UserExtra> users = userExtraRepository.findByFamilyIsCurrentUser(pageable);
        users.addAll(userExtraRepository.findByOrganizationIsCurrentUser(pageable));
        return users.stream()
            .map(userExtraMapper::toDto)
            .map(userExtra -> {
                UserAggregateDTO userInfo = new UserAggregateDTO();
                userInfo.setUserInfo(userExtra);

                // TODO: add this info from the 'aggregator' MS or any other MSs that have it
                userInfo.setFitness("fitnesss test");
                userInfo.setHeartRate("HR test");
                userInfo.setMood("mood test");
                userInfo.setPulse("pulse test");
                return userInfo;
            })
            .collect(Collectors.toList());
    }

    public List<UserPhoneDto> getUserPhones() {
        ArrayList<UserPhoneDto> listWithNoPhones = userRepository.findAll().stream()
            .map(user -> new UserPhoneDto(user.getId(), "", user.getFirstName(), user.getLastName()))
            .collect(Collectors.toCollection(ArrayList::new));
        listWithNoPhones.forEach(userPhoneDto -> {
            UserExtra userExtra = userExtraRepository.findByUserId(userPhoneDto.getUserId());
            if (userExtra != null) {
                userPhoneDto.setPhone(userExtra.getPhone());
            } else {
                log.error("There is no userExtra for userId: {}",userPhoneDto.getUserId());
            }
        });
        return listWithNoPhones;
    }

    /**
     * returns com.upb.vinci.web.beans.UserDTO from beans package, this is used for authorization on microservices
     *
     * @return com.upb.vinci.web.beans.UserDTO
     */
    @Transactional
    public Optional<com.upb.vinci.web.beans.UserDTO> getBeansUserDtoWithAuthorities() {
        log.debug("Request to get current user bean with authorities and devices.");
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtra currentUserExtra = authorizationService.getCurrentUserExtra_ForUserId(currentUser.getId());

        if (currentUser.getAuthorities().contains(ADMIN_AUTHORITY)){
            //TODO: refactor so not all devices are sent, this wont work on big scale
            Set<DeviceDTO> allDevicesForUser = deviceRepository.findAll().stream().map(deviceMapper::toDto).collect(Collectors.toSet());
            return Optional.of(new com.upb.vinci.web.beans.UserDTO(currentUserExtra, allDevicesForUser));
        }

        Set<DeviceDTO> personalDevices = currentUserExtra.getDevices()
            .stream()
            .map(deviceMapper::toDto)
            .collect(Collectors.toSet());

        Set<DeviceDTO> patientsOfFamilyDevices = getPatientsOfFamilyDevices(currentUser);
        Set<DeviceDTO> patientsOfOrganizationDevices = getPatientsOfOrganizationDevices(currentUser);

        return Optional.of(new com.upb.vinci.web.beans.UserDTO(currentUserExtra, personalDevices,patientsOfFamilyDevices,patientsOfOrganizationDevices));
    }

    @Transactional
    public List<FirstAndLastNameWithUuid> getFirstAndLastNameWithUuid() {
        log.debug("Request to get all users first and last names with uuid");
        return userRepository.findAllFirstAndLastNamesWithUuid(PACIENT);
    }

    private void deleteUserExtra(User user) {
        UserExtra userExtraToDelete = userExtraRepository.findByUserId(user.getId());
        userExtraToDelete.getAlerts().forEach(userAlertRepository::delete);
        userExtraRepository.delete(userExtraToDelete);
    }

    private void validateUserDtoForUpdate(UserDTO userDTO) {
        Optional<User> existingUser;
        if (userDTO.getId() == null)
            throw new BadRequestAlertException("Id must be present", "userManagement", "idNull");

        if(userDTO.getEmail() != null) {
            existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
            if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
                throw new EmailAlreadyUsedException();
            }
        }
        if(userDTO.getLogin() != null){
            existingUser = userRepository.findOneByLogin(userDTO.getLogin().toLowerCase());
            if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
                throw new LoginAlreadyUsedException();
            }
        }
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }

    private void validateUserDtoForCreation(UserDTO userDTO) {
        if (userDTO.getId() != null)
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
        if (userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent())
            throw new LoginAlreadyUsedException();
        if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent())
            throw new EmailAlreadyUsedException();
    }

    private UserExtra createDummyUserExtraWithSavedUser(User savedUser) {
        UserExtra newUserExtra = new UserExtra();
        newUserExtra.setUser(savedUser);
        newUserExtra.setUuid(UUID.randomUUID().toString());
        newUserExtra.setDescription("");
        newUserExtra.setFriends("");
        return newUserExtra;
    }

    /**
     * Does not have side effects, just creates user entity from userDto and sets up password and authorities so they can be saved
     * If there are no authorities pacient is default.
     * All users will have USER authority by default.
     * Password will have 20 random characters(RandomUtil.generatePassword()) which will be encoded.
     * @return User
     */
    private User createUserFromDto(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName() != null ? userDTO.getFirstName() : "");
        user.setLastName(userDTO.getLastName() != null ? userDTO.getLastName() : "");
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(userDTO.isActivated());
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            if(!authorities.contains(USER_AUTHORITY)){
                Authority userAuthority = new Authority();
                userAuthority.setName(USER);
                authorities.add(userAuthority);
            }
            user.setAuthorities(authorities);
        } else {
            Set<Authority> patientAuthorities = new HashSet<>();
            authorityRepository.findById(USER).ifPresent(patientAuthorities::add);
            authorityRepository.findById(PACIENT).ifPresent(patientAuthorities::add);
            user.setAuthorities(patientAuthorities);
        }
        return user;
    }

    private Set<DeviceDTO> getPatientsOfFamilyDevices(User user) {
        if (user.getAuthorities().contains(FAMILY_AUTHORITY)){
            return userExtraRepository.findByFamily(user)
                .stream()
                .map(UserExtra::getDevices)
                .flatMap(Collection::stream)
                .map(deviceMapper::toDto)
                .collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }
    private Set<DeviceDTO> getPatientsOfOrganizationDevices(User user) {
        if (user.getAuthorities().contains(ORGANIZIATION_AUTHORITY)) {
            return userExtraRepository.findByOrganization(user)
                .stream()
                .map(UserExtra::getDevices)
                .flatMap(Collection::stream)
                .map(deviceMapper::toDto)
                .collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }

    private boolean removeNonActivatedUser(User existingUser){
        if (existingUser.getActivated()) {
            return false;
        }
        UserExtra userExtra = userExtraRepository.findByUserId(existingUser.getId());
        deviceRepository.findAllByUserExtraId(userExtra.getId())
            .forEach(deviceRepository::delete);
        deviceRepository.flush();
        userExtraRepository.deleteUserExtraByUserId(existingUser.getId());
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    private void validateUserRegistration(UserDTO userDTO, String password) {
        validateLogin(userDTO);
        validateEmail(userDTO);
        validatePassword(userDTO.getLogin(),password);
    }

    private UserExtra createNewUserExtra(UserDTO userDTO, User newUser, User savedUser) {
        UserExtra newUserExtra = new UserExtra();
        newUserExtra.setUser(savedUser);
        if(userDTO.getRole().equals(PACIENT)) {
            newUserExtra.setUuid(UUID.randomUUID().toString());
        } else {
            newUserExtra.setUuid("");
        }
        newUserExtra.setDescription("");
        newUserExtra.setFriends("");
        userExtraRepository.save(newUserExtra);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUserExtra;
    }

    private User getNewUser(UserDTO userDTO, String password) {
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName() != null ? userDTO.getFirstName() : "");
        newUser.setLastName(userDTO.getLastName() != null ? userDTO.getLastName() : "");
        newUser.setEmail(userDTO.getEmail().toLowerCase());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());

        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());

        Set<Authority> authorities = getAuthoritiesForUserDto(userDTO);
        newUser.setAuthorities(authorities);
        return newUser;
    }

    private Set<Authority> getAuthoritiesForUserDto(UserDTO userDTO) {
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(USER).ifPresent(authorities::add);
        switch (userDTO.getRole()) {
            case PACIENT:
                authorityRepository.findById(PACIENT).ifPresent(authorities::add);
                break;
            case FAMILY:
                authorityRepository.findById(FAMILY).ifPresent(authorities::add);
                break;
            case ORGANIZIATION:
                authorityRepository.findById(ORGANIZIATION).ifPresent(authorities::add);
                break;
        }
        return authorities;
    }

    private void validatePassword(String login, String password) {
        if (password.toLowerCase().contains(login.toLowerCase())) throw new BadRequestAlertException("Password can't contain login!", ENTITY_NAME, "loginInPassword");
    }

    private void validateEmail(UserDTO userDTO) {
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
    }

    private void validateLogin(UserDTO userDTO) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new LoginAlreadyUsedException();
            }
        });
    }

    private void createFitbitWatchDeviceForUserExtra(UserExtra userExtra) {
        Device fitbitDevice = Device.builder()
            .deviceType(DeviceType.FITBIT_WATCH)
            .active(false)//false means that it has not connected to the fitbit watch on fitbit servers
            .userExtra(userExtra)
            .name("Fitbit device")
            .description("Not connected to fitbit watch!")
            .uuid("n/a")// n/a means it is not connected, uuid will be id of fitbit watch on fitbit servers
            .startTimestamp(Instant.now())//should change when you connect to fitbit device
            .alerts(new HashSet<>()).build();
        checkIfFitbitDeviceExists(fitbitDevice);
        Device device = deviceRepository.save(fitbitDevice);
        log.debug("Saved or updated fitbit watch device: " + device);
    }

    private void checkIfFitbitDeviceExists(Device fitbitDevice) {
        List<Device> listOfFitbitWatchDevicesForUserWithUserExtraId = deviceRepository.findAllByDeviceTypeEquals(DeviceType.FITBIT_WATCH)
            .stream()
            .filter(device -> device.getUserExtra().getId().equals(fitbitDevice.getUserExtra().getId()))
            .collect(Collectors.toList());
        if (listOfFitbitWatchDevicesForUserWithUserExtraId.size() == 0){//if there are no fitbit watch devices id will be null and new one will be created
            log.debug("New device with type FITBIT_WATCH will be created for userExtraId: " + fitbitDevice.getUserExtra().getId());
            return;
        }
        if (listOfFitbitWatchDevicesForUserWithUserExtraId.size() == 1){//if there is one fitbit watch device it will be updated
            saveOldDeviceData(fitbitDevice,listOfFitbitWatchDevicesForUserWithUserExtraId);
            log.debug("Device with type FITBIT_WATCH will be updated for userExtraId: " + fitbitDevice.getUserExtra().getId());
            return;
        }
        if (listOfFitbitWatchDevicesForUserWithUserExtraId.size() > 1){//this is an error and will delete all elements in the list
            log.error("Multiple Devices with type FITBIT_WATCH for userExtraId: " + fitbitDevice.getUserExtra().getId() + ", which will be deleted");
            listOfFitbitWatchDevicesForUserWithUserExtraId
                .forEach(deviceRepository::delete);
            deviceRepository.flush();
        }
    }

    private void createSurveyDeviceForUserExtra(UserExtra userExtra) {
        Device surveyDevice = Device.builder()
            .deviceType(DeviceType.SURVEY)
            .active(true)
            .userExtra(userExtra)
            .name("Survey device")
            .description("Add description...")
            .uuid(UUID.randomUUID().toString())
            .startTimestamp(Instant.now())
            .alerts(new HashSet<>()).build();
        checkIfSurveyDeviceExists(surveyDevice);
        Device device = deviceRepository.save(surveyDevice);
        log.debug("Saved or updated survey device: " + device);
    }

    private void checkIfSurveyDeviceExists(Device surveyDevice) {
        List<Device> listOfSurveyDevicesForUserWithUserExtraId = deviceRepository.findAllByDeviceTypeEquals(DeviceType.SURVEY)
            .stream()
            .filter(device -> device.getUserExtra().getId().equals(surveyDevice.getUserExtra().getId()))
            .collect(Collectors.toList());
        if (listOfSurveyDevicesForUserWithUserExtraId.size() == 0){//if there are no survey devices id will be null and new one will be created
            log.debug("New device with type survey will be created for userExtraId: " + surveyDevice.getUserExtra().getId());
            return;
        }
        if (listOfSurveyDevicesForUserWithUserExtraId.size() == 1){//if there is one survey device it will be updated
            saveOldDeviceData(surveyDevice,listOfSurveyDevicesForUserWithUserExtraId);
            log.debug("Device with type survey will be updated for userExtraId: " + surveyDevice.getUserExtra().getId());
            return;
        }
        if (listOfSurveyDevicesForUserWithUserExtraId.size() > 1){//this is an error and will delete all elements in the list
            log.error("Multiple Devices with type Survey for userExtraId: " + surveyDevice.getUserExtra().getId() + ", which will be deleted");
            listOfSurveyDevicesForUserWithUserExtraId
                .forEach(deviceRepository::delete);
            deviceRepository.flush();
        }
    }

    private void saveOldDeviceData(Device newDevice, List<Device> listWithOldDevice) {
        Device oldDevice = listWithOldDevice.get(0);
        newDevice.setId(oldDevice.getId());
        newDevice.setAlerts(oldDevice.getAlerts());
        newDevice.setActive(oldDevice.isActive());
        newDevice.setDescription(oldDevice.getDescription());
        newDevice.setUuid(oldDevice.getUuid());
        newDevice.setName(oldDevice.getName());
        newDevice.deviceType(oldDevice.getDeviceType());
        newDevice.setStartTimestamp(oldDevice.getStartTimestamp());
    }

    public UserDTO getCurrentUserDto() {
        User user = authorizationService.getCurrentUser_WithAuthorities();
        UserDTO userDTO = userMapper.userToUserDTO(userRepository.getOne(user.getId()));
        UserExtra userExtra = authorizationService.getCurrentUserExtra_ForUserId(userDTO.getId());
        if(userExtra != null) {
            userDTO.setGender(userExtra.getGender() != null ? userExtra.getGender() : "");
            userDTO.setAddress(userExtra.getAddress() != null ? userExtra.getAddress() : "");
            userDTO.setPhone(userExtra.getPhone() != null ? userExtra.getPhone() : "");
            userDTO.setUuid(userExtra.getUuid() != null ? userExtra.getUuid() : "");
            userDTO.setAlerts(userExtra.getAlerts());
            userDTO.setImages(userExtra.getImages());
            userDTO.setFriends(userExtra.getFriends() != null ? userExtra.getFriends() : "");
            userDTO.setDevices(userExtra.getDevices());
        }
        return userDTO;
    }

    public void associatePacient(Map<String, Object> body) {
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();
        UserExtra targetUser = userExtraRepository.findOneByUuidIgnoreCase(body.get("uuid").toString())
            .orElseThrow(() -> new BadRequestAlertException("There is no userExtra for uuid in request!","userExtra","noUserExtraForUuid"));
        mailService.sendAssociationEmail(currentUser, targetUser, body.get("isFamily").toString(), body.get("isOrganization").toString());
    }
}
