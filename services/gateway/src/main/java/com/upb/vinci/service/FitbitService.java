package com.upb.vinci.service;

import com.upb.vinci.service.dto.FitbitRedirectUrl;

import java.util.Optional;

public interface FitbitService {
    FitbitRedirectUrl getFitbitRedirectUrl(Long userExtraId);

    void authorizeUserWithAuhorizationCode(String code, Optional<Long> state);
}
