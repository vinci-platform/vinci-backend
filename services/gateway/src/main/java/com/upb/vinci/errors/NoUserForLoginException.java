package com.upb.vinci.errors;

import com.upb.vinci.web.rest.errors.ErrorConstants;
import com.upb.vinci.errors.response.ErrorResponse;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class NoUserForLoginException extends AbstractThrowableProblem {

    private static final String TITLE = "NO_USER_FOR_LOGIN_EXCEPTION";
    private static final Status STATUS_TYPE = Status.BAD_REQUEST;
    private static final URI DEFAULT_TYPE = ErrorConstants.DEFAULT_TYPE;
    private static final String DEFAULT_DETAILS = "No user for login!";
    private static final String DETAILS_WITH_LOGIN = "No user for login %s!";

    public NoUserForLoginException(){
        super(DEFAULT_TYPE, TITLE, STATUS_TYPE,DEFAULT_DETAILS);
    }
    public NoUserForLoginException(String userLogin) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,String.format(DETAILS_WITH_LOGIN,userLogin)); }
    public NoUserForLoginException(ErrorResponse feignErrorResponse) { super(DEFAULT_TYPE, TITLE, STATUS_TYPE,feignErrorResponse.getDetail()); }

    @Override
    public String toString() {
        return "NoUserForLoginException{" +
            "title: " + getTitle() +
            "status: " + getStatus() +
            "detail: " + getDetail() +
            "type: " + getType() +
            "}";
    }

}
