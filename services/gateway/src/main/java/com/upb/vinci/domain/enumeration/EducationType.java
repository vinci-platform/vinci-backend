package com.upb.vinci.domain.enumeration;

public enum EducationType {
    NONE, PRIMARY_SCHOOL, SECONDARY_SCHOOL, TERTIARY
}
