package com.upb.vinci.service.dto;
import com.upb.vinci.domain.UserExtra;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the UserExtra entity.
 */
public class UserAggregateDTO implements Serializable {

    private UserExtraDTO userInfo;
    private String pulse;
    private String heartRate;
    private String mood;
    private String fitness;

    public UserExtraDTO getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserExtraDTO userInfo) {
        this.userInfo = userInfo;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getFitness() {
        return fitness;
    }

    public void setFitness(String fitness) {
        this.fitness = fitness;
    }

    public UserAggregateDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtraDTO userExtraDTO = ((UserAggregateDTO) o).getUserInfo();
        if (userExtraDTO.getId() == null || getUserInfo().getId() == null) {
            return false;
        }
        return Objects.equals(getUserInfo().getId(), userExtraDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUserInfo().getId());
    }

    @Override
    public String toString() {
        return "UserAggregateDTO{" +
            "userInfo=" + userInfo +
            ", pulse='" + pulse + '\'' +
            ", heartRate='" + heartRate + '\'' +
            ", mood='" + mood + '\'' +
            ", fitness='" + fitness + '\'' +
            '}';
    }
}
