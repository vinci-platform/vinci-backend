package com.upb.vinci.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.upb.vinci.domain.enumeration.DeviceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Device.
 */
@Entity
@Table(name = "device")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", nullable = false)
    private DeviceType deviceType;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @CreationTimestamp
    @Column(name = "start_timestamp")
    private Instant startTimestamp;

    @Builder.Default
    @OneToMany(mappedBy = "device")
    private Set<DeviceAlert> alerts = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties({"alerts", "images", "devices"})
    private UserExtra userExtra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Device name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Device description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public Device uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public Device deviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public Boolean isActive() {
        return active;
    }

    public Device active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Instant getStartTimestamp() {
        return startTimestamp;
    }

    public Device startTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
        return this;
    }

    public void setStartTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Set<DeviceAlert> getAlerts() {
        return alerts;
    }

    public Device alerts(Set<DeviceAlert> deviceAlerts) {
        this.alerts = deviceAlerts;
        return this;
    }

    public Device addAlert(DeviceAlert deviceAlert) {
        this.alerts.add(deviceAlert);
        deviceAlert.setDevice(this);
        return this;
    }

    public Device removeAlert(DeviceAlert deviceAlert) {
        this.alerts.remove(deviceAlert);
        deviceAlert.setDevice(null);
        return this;
    }

    public void setAlerts(Set<DeviceAlert> deviceAlerts) {
        this.alerts = deviceAlerts;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public Device userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Device device = (Device) o;
        if (device.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), device.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", uuid='" + getUuid() + "'" +
            ", deviceType='" + getDeviceType() + "'" +
            ", active='" + isActive() + "'" +
            ", startTimestamp=" + getStartTimestamp() +
            (userExtra != null ? ", userExtraId: " + userExtra.getId() : "") +
            "}";
    }
}
