package com.upb.vinci.service;

import com.upb.vinci.domain.MobileAppSettings;
import com.upb.vinci.domain.MobileAppSettings_;
import com.upb.vinci.domain.User;
import com.upb.vinci.repository.MobileAppSettingsRepository;
import com.upb.vinci.service.dto.MobileAppSettingsCriteria;
import com.upb.vinci.service.dto.MobileAppSettingsDTO;
import com.upb.vinci.service.mapper.MobileAppSettingsMapper;
import com.upb.vinci.service.util.LongFilterUtil;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.upb.vinci.security.AuthoritiesConstants.*;

/**
 * Service for executing complex queries for MobileAppSettings entities in the database.
 * The main input is a {@link MobileAppSettingsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MobileAppSettingsDTO} or a {@link Page} of {@link MobileAppSettingsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MobileAppSettingsQueryService extends QueryService<MobileAppSettings> {

    private final Long GLOBAL_SETTINGS_ID = 0L;

    private final Logger log = LoggerFactory.getLogger(MobileAppSettingsQueryService.class);

    private final MobileAppSettingsRepository mobileAppSettingsRepository;

    private final MobileAppSettingsMapper mobileAppSettingsMapper;

    private final AuthorizationService authorizationService;

    private final LongFilterUtil longFilterUtil;

    public MobileAppSettingsQueryService(MobileAppSettingsRepository mobileAppSettingsRepository, MobileAppSettingsMapper mobileAppSettingsMapper,
                                         AuthorizationService authorizationService, LongFilterUtil longFilterUtil) {
        this.mobileAppSettingsRepository = mobileAppSettingsRepository;
        this.mobileAppSettingsMapper = mobileAppSettingsMapper;
        this.authorizationService = authorizationService;
        this.longFilterUtil = longFilterUtil;
    }

    /**
     * Return a {@link Page} of {@link MobileAppSettingsDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MobileAppSettingsDTO> findByCriteria(MobileAppSettingsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        User currentUser = authorizationService.getCurrentUser_WithAuthorities();

        if (!currentUser.getAuthorities().contains(ADMIN_AUTHORITY)) {
            LongFilter userIdFilter = criteria.getUserId();
            List<Long> userIdList = new ArrayList<>();
            userIdList.add(currentUser.getId());
            userIdList.add(GLOBAL_SETTINGS_ID);
            criteria.setUserId(longFilterUtil.seIdFilter(userIdList, userIdFilter));
        }
        final Specification<MobileAppSettings> specification = createSpecification(criteria);
        return mobileAppSettingsRepository.findAll(specification, page)
            .map(mobileAppSettingsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MobileAppSettingsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MobileAppSettings> specification = createSpecification(criteria);
        return mobileAppSettingsRepository.count(specification);
    }

    /**
     * Function to convert MobileAppSettingsCriteria to a {@link Specification}
     */
    private Specification<MobileAppSettings> createSpecification(MobileAppSettingsCriteria criteria) {
        Specification<MobileAppSettings> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MobileAppSettings_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), MobileAppSettings_.name));
            }
            if (criteria.getValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue(), MobileAppSettings_.value));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserId(), MobileAppSettings_.userId));
            }
        }
        return specification;
    }
}
