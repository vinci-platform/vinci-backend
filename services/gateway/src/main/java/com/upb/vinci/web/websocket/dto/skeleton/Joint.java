package com.upb.vinci.web.websocket.dto.skeleton;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Joint implements Serializable {
    private Double confidence;
    @JsonProperty("real.x")
    private Double realX;
    @JsonProperty("real.y")
    private Double realY;
    @JsonProperty("real.z")
    private Double realZ;
    @JsonProperty("proj.x")
    private Double projectionX;
    @JsonProperty("proj.y")
    private Double projectionY;

    @Override
    public String toString() {
        return "Joint{" +
            "confidence=" + confidence +
            ", realX=" + realX +
            ", realY=" + realY +
            ", realZ=" + realZ +
            ", projectionX=" + projectionX +
            ", projectionY=" + projectionY +
            '}';
    }
}
