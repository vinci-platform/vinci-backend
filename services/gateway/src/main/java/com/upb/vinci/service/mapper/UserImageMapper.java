package com.upb.vinci.service.mapper;

import com.upb.vinci.domain.UserImage;
import com.upb.vinci.service.dto.UserImageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity UserImage and its DTO UserImageDTO.
 */
@Mapper(componentModel = "spring", uses = {UserExtraMapper.class})
public interface UserImageMapper extends EntityMapper<UserImageDTO, UserImage> {

    @Mapping(source = "userExtra.id", target = "userExtraId")
    @Mapping(source = "userExtra.user.firstName", target = "userExtraFirstName")
    @Mapping(source = "userExtra.user.lastName", target = "userExtraLastName")
    UserImageDTO toDto(UserImage userImage);

    @Mapping(source = "userExtraId", target = "userExtra")
    UserImage toEntity(UserImageDTO userImageDTO);

    default UserImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserImage userImage = new UserImage();
        userImage.setId(id);
        return userImage;
    }
}
