package com.upb.vinci.service.dto;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;

import java.beans.PropertyEditorSupport;
import java.io.IOException;

public class UserDTOEditor extends PropertyEditorSupport {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String getAsText() {
        UserDTO userDTO = (UserDTO) getValue();

        return userDTO == null ? "" : userDTO.toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isEmpty(text)) {
            setValue(null);
        } else {
            try {
                Module module = new JavaTimeModule();
                objectMapper.registerModule(module);
                UserDTO userDTO = objectMapper.readValue(text, UserDTO.class);
                setValue(userDTO);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
