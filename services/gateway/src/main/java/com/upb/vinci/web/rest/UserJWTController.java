package com.upb.vinci.web.rest;

import com.upb.vinci.errors.AuthorizationException;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.security.jwt.JWTFilter;
import com.upb.vinci.security.jwt.TokenProvider;
import com.upb.vinci.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        Authentication authentication = getAuthenticationFromLoginVM(loginVM);
        if (isAuthenticationInRole(authentication,AuthoritiesConstants.ADMIN)) {
            throw new AuthorizationException("Admin can't log in with this endpoint!");
        } else {
            return getJwtTokenResponseEntity(loginVM, authentication);
        }
    }

    @PostMapping("/authenticate-admin")
    public ResponseEntity<JWTToken> authorizeAdmin(@Valid @RequestBody LoginVM loginVM) {

        Authentication authentication = getAuthenticationFromLoginVM(loginVM);
        if (isAuthenticationInRole(authentication,AuthoritiesConstants.ADMIN)) {
            return getJwtTokenResponseEntity(loginVM, authentication);
        } else {
            throw new AuthorizationException("Only Admin can log in with this endpoint!");
        }
    }

    private Authentication getAuthenticationFromLoginVM(@RequestBody @Valid LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        return this.authenticationManager.authenticate(authenticationToken);
    }

    private boolean isAuthenticationInRole(Authentication authentication,String role) {
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(s -> s.equals(role));
    }

    private ResponseEntity<JWTToken> getJwtTokenResponseEntity(@RequestBody @Valid LoginVM loginVM, Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
