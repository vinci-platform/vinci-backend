package com.upb.vinci.repository;

import com.upb.vinci.domain.UserAlert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Spring Data  repository for the UserAlert entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserAlertRepository extends JpaRepository<UserAlert, Long>, JpaSpecificationExecutor<UserAlert> {

    Page<UserAlert> getUserAlertBy(Pageable pageable);
}
