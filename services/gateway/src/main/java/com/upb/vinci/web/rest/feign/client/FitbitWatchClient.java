package com.upb.vinci.web.rest.feign.client;

import com.upb.vinci.web.rest.feign.handlers.ClientExceptionHandler;
import com.upb.vinci.web.rest.feign.handlers.HandleFeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "fitbitwatch")
public interface FitbitWatchClient {

    @HandleFeignException(ClientExceptionHandler.class)
    @GetMapping("/api/fitbit-auth/code-mobile")
    ResponseEntity<String> getCodeMobile(@RequestParam(value = "code") String code,
                                   @RequestParam(value = "userExtraId") Long userExtraId);
}
