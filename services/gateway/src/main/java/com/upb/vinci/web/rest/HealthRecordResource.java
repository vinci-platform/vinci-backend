package com.upb.vinci.web.rest;
import com.upb.vinci.security.AuthoritiesConstants;
import com.upb.vinci.service.HealthRecordQueryService;
import com.upb.vinci.service.HealthRecordService;
import com.upb.vinci.service.dto.HealthRecordCriteria;
import com.upb.vinci.service.dto.HealthRecordDTO;
import com.upb.vinci.web.rest.errors.BadRequestAlertException;
import com.upb.vinci.web.rest.util.HeaderUtil;
import com.upb.vinci.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing HealthRecord.
 */
@RestController
@RequestMapping("/api")
public class HealthRecordResource {

    private final Logger log = LoggerFactory.getLogger(HealthRecordResource.class);

    private static final String ENTITY_NAME = "healthRecord";

    private final HealthRecordService healthRecordService;

    private final HealthRecordQueryService healthRecordQueryService;

    public HealthRecordResource(HealthRecordService healthRecordService, HealthRecordQueryService healthRecordQueryService) {
        this.healthRecordService = healthRecordService;
        this.healthRecordQueryService = healthRecordQueryService;
    }

    /**
     * POST  /health-records : Create a new healthRecord.
     *
     * @param healthRecordDTO the healthRecordDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new healthRecordDTO, or with status 400 (Bad Request) if the healthRecord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/health-records")
    public ResponseEntity<HealthRecordDTO> createHealthRecord(@RequestBody @Valid HealthRecordDTO healthRecordDTO) throws URISyntaxException {
        log.debug("REST request to save HealthRecord : {}", healthRecordDTO);
        HealthRecordDTO result = healthRecordService.save(healthRecordDTO);
        return ResponseEntity.created(new URI("/api/health-records/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /health-records : Updates an existing healthRecord.
     *
     * @param healthRecordDTO the healthRecordDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated healthRecordDTO,
     * or with status 400 (Bad Request) if the healthRecordDTO is not valid,
     * or with status 500 (Internal Server Error) if the healthRecordDTO couldn't be updated
     */
    @PutMapping("/health-records")
    public ResponseEntity<HealthRecordDTO> updateHealthRecord(@RequestBody @Valid HealthRecordDTO healthRecordDTO) {
        log.debug("REST request to update HealthRecord : {}", healthRecordDTO);
        HealthRecordDTO result = healthRecordService.update(healthRecordDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, healthRecordDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /health-records : get all the healthRecords.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of healthRecords in body
     */
    @GetMapping("/health-records")
    public ResponseEntity<List<HealthRecordDTO>> getAllHealthRecords(HealthRecordCriteria criteria, Pageable pageable) {
        log.debug("REST request to get HealthRecords by criteria: {}", criteria);
        Page<HealthRecordDTO> page = healthRecordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/health-records");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /health-records/count : count all the healthRecords.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/health-records/count")
    public ResponseEntity<Long> countHealthRecords(HealthRecordCriteria criteria) {
        log.debug("REST request to count HealthRecords by criteria: {}", criteria);
        return ResponseEntity.ok().body(healthRecordQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /health-records/:id : get the "id" healthRecord.
     *
     * @param id the id of the healthRecordDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the healthRecordDTO, or with status 404 (Not Found)
     */
    @GetMapping("/health-records/{id}")
    public ResponseEntity<HealthRecordDTO> getHealthRecord(@PathVariable Long id) {
        log.debug("REST request to get HealthRecord by id: {}", id);
        return ResponseEntity.ok(healthRecordService.findOne(id));
    }

    /**
     * DELETE  /health-records/:id : delete the "id" healthRecord.
     *
     * @param id the id of the healthRecordDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/health-records/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteHealthRecord(@PathVariable Long id) {
        log.debug("REST request to delete HealthRecord : {}", id);
        healthRecordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
