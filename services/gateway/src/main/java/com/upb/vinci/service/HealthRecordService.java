package com.upb.vinci.service;

import com.upb.vinci.service.dto.HealthRecordDTO;

/**
 * Service Interface for managing HealthRecord.
 */
public interface HealthRecordService {

    /**
     * Save a healthRecord.
     *
     * @param healthRecordDTO the entity to save
     * @return the persisted entity
     */
    HealthRecordDTO save(HealthRecordDTO healthRecordDTO);

    /**
     * Get the "id" healthRecord.
     *
     * @param id the id of the entity
     * @return the entity
     */
    HealthRecordDTO findOne(Long id);

    /**
     * Delete the "id" healthRecord.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    HealthRecordDTO update(HealthRecordDTO healthRecordDTO);
}
