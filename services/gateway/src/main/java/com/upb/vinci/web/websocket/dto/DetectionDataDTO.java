package com.upb.vinci.web.websocket.dto;

import java.util.List;

public class DetectionDataDTO {

    private String deviceId;
    private List<String> detectedPersons;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<String> getDetectedPersons() {
        return detectedPersons;
    }

    public void setDetectedPersons(List<String> detectedPersons) {
        this.detectedPersons = detectedPersons;
    }

    @Override
    public String toString() {
        return "DetectionDataDTO{" +
            "deviceId='" + deviceId + '\'' +
            ", detectedPersons=" + detectedPersons +
            '}';
    }
}
