apiVersion: apps/v1
kind: Deployment
metadata:
    name: gateway
    namespace: vinci-aal-namespace
spec:
    selector:
        matchLabels:
            app: gateway
            version: "v1"
    template:
        metadata:
            labels:
                app: gateway
                version: "v1"
        spec:
            initContainers:
                - name: init-ds
                  image: busybox:latest
                  command:
                      - '/bin/sh'
                      - '-c'
                      - |
                          while true
                          do
                            rt=$(nc -z -w 1 jhipster-registry 8761)
                            if [ $? -eq 0 ]; then
                              echo "Registry is up"
                              break
                            fi
                            echo "Registry is not yet up!"
                            sleep 10
                          done
            imagePullSecrets:
                - name: registry-credentials
            restartPolicy: Always
            containers:
                - name: gateway-app
                  image: registry.gitlab.com/vinci-aal/gateway:master
                  imagePullPolicy: Always
                  ports:
                      - name: http
                        containerPort: 8080
                  resources:
                      requests:
                          memory: "500Mi"
                          cpu: "400m"
                      limits:
                          memory: "700Mi"
                          cpu: "800m"
                  readinessProbe:
                      httpGet:
                          path: /management/health
                          port: http
                      initialDelaySeconds: 150
                      periodSeconds: 20
                      failureThreshold: 12
                  livenessProbe:
                      httpGet:
                          path: /management/health
                          port: http
                      periodSeconds: 20
                      failureThreshold: 12
                      initialDelaySeconds: 200
                  env:
                      - name: SPRING_PROFILES_ACTIVE
                        value: prod,swagger
                      - name: SPRING_CLOUD_CONFIG_URI
                        value: http://admin:${jhipster.registry.password}@jhipster-registry.vinci-aal-namespace.svc.cluster.local:8761/config
                      - name: JHIPSTER_REGISTRY_PASSWORD
                        valueFrom:
                            secretKeyRef:
                                name: registry-secret
                                key: registry-admin-password
                      - name: EUREKA_CLIENT_SERVICE_URL_DEFAULTZONE
                        value: http://admin:${jhipster.registry.password}@jhipster-registry.vinci-aal-namespace.svc.cluster.local:8761/eureka/
                      - name: SPRING_DATASOURCE_URL
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_DATASOURCE_URL
                      - name: SPRING_DATASOURCE_USERNAME
                        value: gateway
                      - name: SPRING_DATASOURCE_PASSWORD
                        valueFrom:
                            secretKeyRef:
                                name: db-secret
                                key: gateway-password
                      - name: JHIPSTER_METRICS_LOGS_ENABLED
                        value: 'true'
                      - name: JHIPSTER_LOGGING_LOGSTASH_ENABLED
                        value: 'true'
                      - name: JHIPSTER_LOGGING_LOGSTASH_HOST
                        value: jhipster-logstash
                      - name: SPRING_ZIPKIN_ENABLED
                        value: "true"
                      - name: SPRING_ZIPKIN_BASE_URL
                        value: http://jhipster-zipkin
                      - name: SPRING_SLEUTH_PROPAGATION_KEYS
                        value: "x-request-id,x-ot-span-context"
                      - name: JAVA_OPTS
                        value: " -Xmx256m -Xms256m"
                      - name: FITBIT_API_REDIRECT_URL
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: FITBIT_API_REDIRECT_URL
                      - name: JHIPSTER_MAIL_BASE_URL
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: JHIPSTER_MAIL_BASE_URL
                      - name: SPRING_MAIL_HOST
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_HOST
                      - name: SPRING_MAIL_PORT
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_PORT
                      - name: SPRING_MAIL_USERNAME
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_USERNAME
                      - name: SPRING_MAIL_PASSWORD
                        valueFrom:
                            secretKeyRef:
                                name: spring-mail-password
                                key: SPRING_MAIL_PASSWORD
                      - name: SPRING_MAIL_PROPERTIES_MAIL_SMTP_AUTH
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_PROPERTIES_MAIL_SMTP_AUTH
                      - name: SPRING_MAIL_PROPERTIES_MAIL_SMTP_STARTTLS_ENABLE
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_PROPERTIES_MAIL_SMTP_STARTTLS_ENABLE
                      - name: SPRING_MAIL_PROPERTIES_MAIL_SMTP_SSL_TRUST
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: SPRING_MAIL_PROPERTIES_MAIL_SMTP_SSL_TRUST
                      - name: JHIPSTER_MAIL_FROM
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: JHIPSTER_MAIL_FROM
                      - name: JHIPSTER_SWAGGER_HOST
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: JHIPSTER_SWAGGER_HOST
                      - name: FITBIT_API_CLIENT_ID
                        valueFrom:
                            secretKeyRef:
                                name: fitbit-auth-credentials
                                key: FITBIT_API_CLIENT_ID
                      - name: KAFKA_BOOTSTRAP_SERVERS
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: KAFKA_BOOTSTRAP_SERVERS
                      - name: KAFKA_GROUP_ID
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: KAFKA_GROUP_ID
                      - name: KAFKA_TOPIC_FITBIT_DEVICE_UPDATE_DATA_NUM_PARTITIONS
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: KAFKA_TOPIC_FITBIT_DEVICE_UPDATE_DATA_NUM_PARTITIONS
                      - name: KAFKA_TOPIC_FITBIT_DEVICE_UPDATE_DATA_REPLICAS
                        valueFrom:
                            configMapKeyRef:
                                name: vinci-aal-config
                                key: KAFKA_TOPIC_FITBIT_DEVICE_UPDATE_DATA_REPLICAS
