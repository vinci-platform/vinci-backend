package com.vinci.watch.data.collector.web.rest.beans;

import lombok.Data;

@Data
public class ImportRecordBean {
    private String _id;
    private String _xt;
    private String ei;
    private String si;
    private String dt;
    private String s;
    private String c;
    private String y;
    private String m;
    private String d;
    private String p;
}
