package com.vinci.watch.data.collector.web.rest.beans;

import com.vinci.watch.data.collector.web.rest.enums.DeviceType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Data
public class DeviceDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private String uuid;

    @NotNull
    private DeviceType deviceType;

    @NotNull
    private Boolean active;

    private Instant startTimestamp;

    private Long userExtraId;
}
