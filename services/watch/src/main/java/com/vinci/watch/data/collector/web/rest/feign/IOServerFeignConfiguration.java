package com.vinci.watch.data.collector.web.rest.feign;

import com.vinci.watch.data.collector.security.SecurityUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class IOServerFeignConfiguration implements RequestInterceptor {
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BEARER = "Bearer";

    @Value("${jhipster.security.authentication.jwt.base64-secret}")
    private String jwtToken;

    @Override
    public void apply(RequestTemplate template) {
        log.debug("Intercepting feign client call with token {}", SecurityUtils.getCurrentUserJWT());
        SecurityUtils.getCurrentUserJWT()
            .ifPresent(s -> {
                log.debug("Adding authentication header: {}", String.format("%s %s", BEARER, this.jwtToken));
                template.header(AUTHORIZATION_HEADER, String.format("%s %s", BEARER, this.jwtToken));
            });
    }
}
