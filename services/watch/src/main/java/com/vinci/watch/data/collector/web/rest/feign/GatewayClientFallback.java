package com.vinci.watch.data.collector.web.rest.feign;

import com.vinci.watch.data.collector.web.rest.beans.DeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class GatewayClientFallback implements GatewayClient {

    @Override
    public Optional<DeviceDTO> getDeviceByUid(String uuid) {
        log.error("Encountered an error. Defaulting to empty");
        return Optional.empty();
    }

    @Override
    public List<DeviceDTO> getDevices(String deviceType) {
        log.error("Encountered an error when retrieving all watch devices. Defaulting to an empty list.");
        return Collections.emptyList();
    }
}
