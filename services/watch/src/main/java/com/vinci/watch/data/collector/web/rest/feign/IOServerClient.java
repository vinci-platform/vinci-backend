package com.vinci.watch.data.collector.web.rest.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "ioserver", fallback = IOServerFallback.class)
public interface IOServerClient {

    @PostMapping(value = "/api/watch-data/import")
    ResponseEntity<String> saveWatchData(@RequestBody String payload);
}


