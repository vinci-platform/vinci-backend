package com.vinci.watch.data.collector.web.rest.enums;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    WATCH, SHOE, CAMERA_FITNESS, CAMERA_MOVEMENT
}
