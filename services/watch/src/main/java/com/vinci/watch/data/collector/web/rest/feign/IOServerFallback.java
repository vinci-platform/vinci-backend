package com.vinci.watch.data.collector.web.rest.feign;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class IOServerFallback implements IOServerClient {

    @Override
    public ResponseEntity<String> saveWatchData(String payload) {
        log.error("Encountered an error when saving watch data to IO server.");
        return ResponseEntity
            .ok("Encountered an error when saving watch data to IO server.");
    }
}
