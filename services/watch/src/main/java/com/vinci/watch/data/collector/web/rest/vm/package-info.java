/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.watch.data.collector.web.rest.vm;
