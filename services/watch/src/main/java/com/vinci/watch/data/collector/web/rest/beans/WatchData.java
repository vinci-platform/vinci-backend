package com.vinci.watch.data.collector.web.rest.beans;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
public class WatchData {
    private Long id;
    private String data;
    private Long timestamp;
    private Long deviceId;
}
