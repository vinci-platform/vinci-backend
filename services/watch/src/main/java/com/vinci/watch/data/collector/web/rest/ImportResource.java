package com.vinci.watch.data.collector.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.vinci.watch.data.collector.web.rest.beans.DeviceDTO;
import com.vinci.watch.data.collector.web.rest.feign.GatewayClient;
import com.vinci.watch.data.collector.web.rest.feign.IOServerClient;
import com.vinci.watch.data.collector.web.rest.beans.ImportRecordBean;
import com.vinci.watch.data.collector.web.rest.beans.WatchData;
import com.vinci.watch.data.collector.web.rest.util.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerErrorException;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

/**
 * REST controller for importing daa from Connected Medical.
 */
@Slf4j
@RestController
public class ImportResource {
    private static final Predicate<List<?>> NOT_EMPTY = x -> !x.isEmpty();
    private static final Predicate<Object> NOT_NULL = x -> !Objects.isNull(x);

    private static final String CONNECTED_MEDICAL_API_URL = "https://connected-solution.com:4200/getlog/getalerts";

    private static final String START_DATE_KEY = "sdate";
    private static final String END_DATE_KEY = "edate";
    private static final String ALERT_TYPE_KEY = "askfor";

    private static final ImmutableList<String> ALERT_TYPES =
        ImmutableList.of("H02", "H12");


    private final ObjectMapper objectMapper = new ObjectMapper();
    private static Supplier<List<DeviceDTO>> devices;

    private final IOServerClient ioServerClient;

    //todo: devices should use version v1.0.0 of get devices from client
    public ImportResource(IOServerClient ioServerClient,
                          GatewayClient gatewayClient) {
        this.ioServerClient = ioServerClient;
        devices = Suppliers.memoizeWithExpiration(() -> gatewayClient.getDevices("watch"), 10, TimeUnit.SECONDS);

    }

    @Scheduled(fixedRate = 30000)
    protected void constantlyRetrieveNewData() throws JsonProcessingException {
        LocalDateTime endDate = LocalDateTime.now();
        LocalDateTime startDate = endDate.minusSeconds(30);
        retrieveAndPersist(startDate, endDate);
    }

    //todo: needs to be updated to ether work with new v1.0.0 api or some other way(like jms???)
    public long retrieveAndPersist(LocalDateTime start, LocalDateTime end) throws JsonProcessingException {
        log.debug("Retrieve updated data from Connected Medical");

        List<WatchData> watchData;
        try {
            watchData = getLatestData(start, end);
        } catch (Exception ex) {
            throw new ServerErrorException("Encountered an error when retrieving the data from CMD.", ex);
        }

        if (!watchData.isEmpty()) {
            this.ioServerClient.saveWatchData(objectMapper.writeValueAsString(watchData));
        } else {
            log.debug("There are no new updates to be saved. Skipping rest call to ioserver.");
        }
        return watchData.size();
    }

    private List<WatchData> getLatestData(LocalDateTime start, LocalDateTime end) {
        List<ImportRecordBean> alertRecords = ALERT_TYPES.stream()
            .map(type -> getDataForAlertType(start, end, type))
            .filter(NOT_EMPTY)
            .flatMap(List::stream)
            .collect(toList());

        return alertRecords.stream()
            .map(this::buildWatchDataBean)
            .filter(NOT_NULL)
            .collect(toList());
    }

    private WatchData buildWatchDataBean(ImportRecordBean recordBean) {
        try {
            Optional<DeviceDTO> device = foundMatchingUUIDForDevice(recordBean.getEi());
            if (!device.isPresent()) {
                log.error("Could not find a device with uuid: {}. Ignoring record...", recordBean.getEi());
                return null;
            }

            return WatchData.builder()
                .data(objectMapper.writeValueAsString(recordBean))
                .deviceId(device.get().getId())
                .build();
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Encountered an error while building a WatchData bean for saving to database.");
        }
    }

    private List<ImportRecordBean> getDataForAlertType(LocalDateTime startDate,
                                                       LocalDateTime endDate,
                                                       String alertType) {
        log.debug("{} -> {} Retrieving new data", startDate, endDate);

        try {
            JSONObject parameters = new JSONObject();
            parameters.accumulate("key", "test");
            parameters.accumulate(START_DATE_KEY, startDate);
            parameters.accumulate(END_DATE_KEY, endDate);
            parameters.accumulate(ALERT_TYPE_KEY, alertType);

            String result = HttpRequestUtil.performPost(CONNECTED_MEDICAL_API_URL, parameters.toString());

            JSONObject jsonObject = new JSONObject(result);
            JSONArray recordsArray = (JSONArray) jsonObject.get("response");

            return Arrays.asList(objectMapper
                .readValue(recordsArray.toString(), ImportRecordBean[].class));
        } catch (Exception exception) {
            log.error("Encountered an error when retrieving new data for alert type {} from Connected Medical", alertType);
        }
        log.debug("{} -> {} Received data", startDate, endDate);

        return Lists.newLinkedList();
    }

    private Optional<DeviceDTO> foundMatchingUUIDForDevice(String uuid) {
        return devices.get().stream()
            .filter(device -> StringUtils.equals(device.getUuid(), uuid))
            .findAny();
    }
}
