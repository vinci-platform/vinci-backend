package com.vinci.watch.data.collector.web.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * REST controller for managing WatchData retrieval from CMD.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class WatchAppResource {

    private final ImportResource importClient;

    public WatchAppResource(ImportResource importClient) {
        this.importClient = importClient;
    }

    /**
     * POST  /import : Records import API.
     *
     * @param start the start date for the import
     * @param end   the end date for the import
     * @return the ResponseEntity with status 201 (Created) and with body the number of imported records
     * @throws JsonProcessingException if any of the records has invalid format
     */
    //todo: make this endpoint secure, now anybody can get to this endpoint and spam requests, it also uses old getDevices endpoint in gateway client
    @PostMapping("/import")
    public ResponseEntity<String> importRecords(@RequestParam LocalDateTime start,
                                                @RequestParam LocalDateTime end) throws JsonProcessingException {
        log.debug("REST request to trigger re-import of records for interval {} - {}", start, end);
        return ResponseEntity.ok(String.format("Successfully imported '%s' records",
            importClient.retrieveAndPersist(start, end)));
    }

    @Data
    private class ImportPayload {
        private LocalDateTime start;
        private LocalDateTime end;

        @JsonCreator
        public ImportPayload(LocalDateTime start, LocalDateTime end) {
            this.start = start;
            this.end = end;
        }
    }
}
