package com.vinci.watch.data.collector.web.rest.feign;

import com.vinci.watch.data.collector.web.rest.beans.DeviceDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@FeignClient(value = "gateway", fallback = GatewayClientFallback.class)
public interface GatewayClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/devices/uuid")
    Optional<DeviceDTO> getDeviceByUid(@RequestParam(name = "uuid") String uuid);

    //todo: use GET gateway/api/v1.0.0/devices/type , problems when it is working in scheduled task (with no authentication)
    @RequestMapping(method = RequestMethod.GET, value = "/api/devices/type")
    List<DeviceDTO> getDevices(@RequestParam(name = "deviceType") String deviceType);
}
