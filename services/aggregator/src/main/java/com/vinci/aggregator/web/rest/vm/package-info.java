/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinci.aggregator.web.rest.vm;
