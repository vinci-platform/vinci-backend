import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-alert.reducer';
import { IUserAlert } from 'app/shared/model/user-alert.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserAlertDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserAlertDetail extends React.Component<IUserAlertDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userAlertEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.userAlert.detail.title">UserAlert</Translate> [<b>{userAlertEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="label">
                <Translate contentKey="gatewayApp.userAlert.label">Label</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.label}</dd>
            <dt>
              <span id="values">
                <Translate contentKey="gatewayApp.userAlert.values">Values</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.values}</dd>
            <dt>
              <span id="alertType">
                <Translate contentKey="gatewayApp.userAlert.alertType">Alert Type</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.alertType}</dd>
            <dt>
              <span id="userRead">
                <Translate contentKey="gatewayApp.userAlert.userRead">User Read</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.userRead ? 'true' : 'false'}</dd>
            <dt>
              <span id="familyRead">
                <Translate contentKey="gatewayApp.userAlert.familyRead">Family Read</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.familyRead ? 'true' : 'false'}</dd>
            <dt>
              <span id="organizationRead">
                <Translate contentKey="gatewayApp.userAlert.organizationRead">Organization Read</Translate>
              </span>
            </dt>
            <dd>{userAlertEntity.organizationRead ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="gatewayApp.userAlert.user">User</Translate>
            </dt>
            <dd>
              {userAlertEntity.userExtraFirstName ? userAlertEntity.userExtraFirstName : ''}{' '}
              {userAlertEntity.userExtraLastName ? userAlertEntity.userExtraLastName : ''}
            </dd>
            <dt>
              <Translate contentKey="userManagement.createdDate">Created Date</Translate>
            </dt>
            <dd>
              <TextFormat value={userAlertEntity.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/user-alert" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/user-alert/${userAlertEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ userAlert }: IRootState) => ({
  userAlertEntity: userAlert.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAlertDetail);
