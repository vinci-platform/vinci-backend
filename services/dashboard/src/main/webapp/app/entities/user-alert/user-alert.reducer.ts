import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUserAlert, defaultValue } from 'app/shared/model/user-alert.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_USERALERT_LIST: 'userAlert/FETCH_USERALERT_LIST',
  FETCH_USERALERT: 'userAlert/FETCH_USERALERT',
  CREATE_USERALERT: 'userAlert/CREATE_USERALERT',
  UPDATE_USERALERT: 'userAlert/UPDATE_USERALERT',
  UPDATE_USERALERTS: 'userAlert/UPDATE_USERALERTS',
  DELETE_USERALERT: 'userAlert/DELETE_USERALERT',
  RESET: 'userAlert/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserAlert>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type UserAlertState = Readonly<typeof initialState>;

// Reducer

export default (state: UserAlertState = initialState, action): UserAlertState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USERALERT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USERALERT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_USERALERT):
    case REQUEST(ACTION_TYPES.UPDATE_USERALERT):
    case REQUEST(ACTION_TYPES.UPDATE_USERALERTS):
    case REQUEST(ACTION_TYPES.DELETE_USERALERT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_USERALERT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USERALERT):
    case FAILURE(ACTION_TYPES.CREATE_USERALERT):
    case FAILURE(ACTION_TYPES.UPDATE_USERALERT):
    case FAILURE(ACTION_TYPES.UPDATE_USERALERTS):
    case FAILURE(ACTION_TYPES.DELETE_USERALERT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERALERT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERALERT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_USERALERT):
    case SUCCESS(ACTION_TYPES.UPDATE_USERALERT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_USERALERT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
    case SUCCESS(ACTION_TYPES.UPDATE_USERALERTS):
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/user-alerts';
const apiUrlNew = 'api/user-alerts';

// Actions

export const getEntities: ICrudGetAll<IUserAlert> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_USERALERT_LIST,
    payload: axios.get<IUserAlert>(requestUrl)
  };
};

export const getEntity: ICrudGetAction<IUserAlert> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USERALERT,
    payload: axios.get<IUserAlert>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IUserAlert> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USERALERT,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserAlert> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERALERT,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntities: ICrudPutAction<IUserAlert[]> = entities => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERALERTS,
    payload: axios.put(`${apiUrlNew}/entities`, entities.map(entity => cleanEntity(entity)))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserAlert> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USERALERT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
