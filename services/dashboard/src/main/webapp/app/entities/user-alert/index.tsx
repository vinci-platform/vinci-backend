import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserAlert from './user-alert';
import UserAlertDetail from './user-alert-detail';
import UserAlertUpdate from './user-alert-update';
import UserAlertDeleteDialog from './user-alert-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserAlertUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserAlertUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserAlertDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserAlert} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={UserAlertDeleteDialog} />
  </>
);

export default Routes;
