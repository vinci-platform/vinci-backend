import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserExtra } from 'app/shared/model/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-alert.reducer';
import { IUserAlert } from 'app/shared/model/user-alert.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserAlertUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserAlertUpdateState {
  isNew: boolean;
  userExtraId: string;
}

export class UserAlertUpdate extends React.Component<IUserAlertUpdateProps, IUserAlertUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userExtraId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUserExtras();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { userAlertEntity } = this.props;
      const entity = {
        ...userAlertEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-alert');
  };

  render() {
    const { userAlertEntity, userExtras, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.userAlert.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.userAlert.home.createOrEditLabel">Create or edit a UserAlert</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userAlertEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-alert-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="labelLabel" for="label">
                    <Translate contentKey="gatewayApp.userAlert.label">Label</Translate>
                  </Label>
                  <AvField
                    id="user-alert-label"
                    type="text"
                    name="label"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="valuesLabel" for="values">
                    <Translate contentKey="gatewayApp.userAlert.values">Values</Translate>
                  </Label>
                  <AvField 
                    id="user-alert-values" 
                    type="text" 
                    name="values"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }} />
                </AvGroup>
                <AvGroup>
                  <Label id="alertTypeLabel">
                    <Translate contentKey="gatewayApp.userAlert.alertType">Alert Type</Translate>
                  </Label>
                  <AvInput
                    id="user-alert-alertType"
                    type="select"
                    className="form-control"
                    name="alertType"
                    value={(!isNew && userAlertEntity.alertType) || 'SUCCESS'}
                  >
                    <option value="SUCCESS">{translate('gatewayApp.AlertType.SUCCESS')}</option>
                    <option value="INFO">{translate('gatewayApp.AlertType.INFO')}</option>
                    <option value="WARNING">{translate('gatewayApp.AlertType.WARNING')}</option>
                    <option value="DANGER">{translate('gatewayApp.AlertType.DANGER')}</option>
                  </AvInput>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="userReadLabel" check>
                    <AvInput id="user-alert-userRead" type="checkbox" className="form-control" name="userRead" />
                    <Translate contentKey="gatewayApp.userAlert.userRead">User Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="familyReadLabel" check>
                    <AvInput id="user-alert-familyRead" type="checkbox" className="form-control" name="familyRead" />
                    <Translate contentKey="gatewayApp.userAlert.familyRead">Family Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="organizationReadLabel" check>
                    <AvInput id="user-alert-organizationRead" type="checkbox" className="form-control" name="organizationRead" />
                    <Translate contentKey="gatewayApp.userAlert.organizationRead">Organization Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="userExtra.id">
                    <Translate contentKey="gatewayApp.userAlert.userExtra">User</Translate>
                  </Label>
                  <AvInput id="user-alert-userExtra" type="select" className="form-control" name="userExtraId">
                    <option value="" key="0" />
                    {userExtras
                      ? userExtras.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.userFirstName} {otherEntity.userLastName}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="createdDate">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.createdDate">Created Date</Translate>
                  </Label>
                  <AvField id="device-created-date" type="text" name="createdDate" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-alert" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  userExtras: storeState.userExtra.entities,
  userAlertEntity: storeState.userAlert.entity,
  loading: storeState.userAlert.loading,
  updating: storeState.userAlert.updating,
  updateSuccess: storeState.userAlert.updateSuccess
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAlertUpdate);
