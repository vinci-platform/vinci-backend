import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table, Label, Input } from 'reactstrap';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import {
  Translate,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  JhiPagination,
  getPaginationItemsNumber,
  translate
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-alert.reducer';
import { APP_DATE_FORMAT } from 'app/config/constants';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';

export interface IUserAlertProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IUserAlertState extends IPaginationBaseState {
  filter: string;
  showFilters: boolean;
  alertTypeValue: string;
  userValue: string;
  dateFrom: string;
  dateTo: string;
}

export class UserAlert extends React.Component<IUserAlertProps, IUserAlertState> {
  state: IUserAlertState = {
    filter: '',
    showFilters: false,
    alertTypeValue: '',
    userValue: '',
    dateFrom: '',
    dateTo: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getUserAlerts();
    this.props.getUserExtras(null, MAX_SIZE);
  }

  setFilter = evt => {
    this.setState({
      filter: evt.target.value
    });
  };

  filterFn = l =>{
  return l.label
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  l.values
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  (l.userExtraFirstName && l.userExtraFirstName
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase())) ||
  (l.userExtraLastName && l.userExtraLastName
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()));
  }
  

  alertTypeOnChange = event => {
    this.setState(
      {
        alertTypeValue: event.target.value
      },
      () => this.getUserAlerts()
    );
  };

  usersOnChange = event => {
    this.setState(
      {
        userValue: event.target.value
      },
      () => this.getUserAlerts()
    );
  };

  dateFromOnChange = event => {
    this.setState(
      {
        dateFrom: event.target.value
      },
      () => this.getUserAlerts()
    );
  };

  dateToOnChange = event => {
    this.setState(
      {
        dateTo: event.target.value
      },
      () => this.getUserAlerts()
    );
  };

  handleRemoveFilters = () => {
    this.setState(
      {
        alertTypeValue: '',
        userValue: '',
        dateFrom: '',
        dateTo: ''
      },
      () => this.getUserAlerts()
    );
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortUserAlerts()
    );
  };

  sortUserAlerts = () => {
    this.getUserAlerts();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortUserAlerts());

  getUserAlerts = () => {
    const { activePage, itemsPerPage, sort, order, alertTypeValue, userValue, dateFrom, dateTo } = this.state;

    let filter =
      `${alertTypeValue !== '' ? `alertType.equals=${alertTypeValue}` : ''}` +
      `${alertTypeValue ? '&' : ''}` +
      `${userValue !== '' ? `userExtraId.equals=${userValue}` : ''}` +
      `${userValue ? '&' : ''}` +
      `${dateFrom !== '' ? `createdDate.greaterOrEqualThan=${new Date(dateFrom).toISOString()}` : ''}` +
      `${dateFrom ? '&' : ''}` +
      `${dateTo !== '' ? `createdDate.lessOrEqualThan=${new Date(dateTo).toISOString()}` : ''}`;

    if (filter.endsWith('&')) filter = filter.slice(0, -1);

    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`, filter);
  };

  render() {
    const { userAlertList, match, isFetching, totalItems, userExtras } = this.props;
    const { filter, showFilters, alertTypeValue, userValue, dateFrom, dateTo } = this.state;
    return (
      <div>
        <h2 id="user-alert-heading" className="d-flex flex-wrap justify-content-between" style={{ marginBottom: '50px' }}>
          <Translate contentKey="gatewayApp.userAlert.home.title">User Alerts</Translate>
          <div className="d-flex flex-wrap col-md-6 justify-content-between search-container">
            <div className="d-flex col-md-8">
              <div style={{ margin: 'auto' }}>
                <Translate contentKey="logs.search">Search</Translate>
              </div>
              <input
                type="search"
                value={filter}
                onChange={this.setFilter}
                disabled={isFetching}
                className="form-control"
                style={{ marginLeft: '10px', width: '100% !important' }}
              />
              <Button type="submit" color="primary">
                <FontAwesomeIcon icon="search" />
              </Button>
            </div>
            <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
              <FontAwesomeIcon icon="plus" />
              &nbsp;
              <Translate contentKey="gatewayApp.userAlert.home.createLabel">Create new User Alert</Translate>
            </Link>
          </div>
        </h2>
        <div className="pb-3 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label id="deviceTypeLabel">
                    <Translate contentKey="gatewayApp.deviceAlert.alertType">Alert Type</Translate>
                  </Label>
                  <AvInput
                    id="device-alert-alertType"
                    type="select"
                    className="form-control"
                    name="alertType"
                    value={alertTypeValue}
                    onChange={this.alertTypeOnChange}
                  >
                    <option value="" />
                    <option value="SUCCESS">{translate('gatewayApp.AlertType.SUCCESS')}</option>
                    <option value="INFO">{translate('gatewayApp.AlertType.INFO')}</option>
                    <option value="WARNING">{translate('gatewayApp.AlertType.WARNING')}</option>
                    <option value="DANGER">{translate('gatewayApp.AlertType.DANGER')}</option>
                  </AvInput>
                </AvGroup>
                <AvGroup className="pr-2">
                  <Label for="userExtra.id">
                    <Translate contentKey="gatewayApp.device.user">User</Translate>
                  </Label>
                  <AvInput
                    id="device-userExtra"
                    type="select"
                    className="form-control "
                    name="userExtraId"
                    value={userValue}
                    onChange={this.usersOnChange}
                  >
                    <option value="" key="0" />
                    {userExtras
                      ? userExtras.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <div className="pr-2">
                  <label>Date from</label>
                  <Input type="date" value={dateFrom} onChange={this.dateFromOnChange} />
                </div>
                <div className="pr-2">
                  <label>Date to</label>
                  <Input type="date" value={dateTo} onChange={this.dateToOnChange} />
                </div>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th onClick={this.sort('label')}>
                  <Translate contentKey="gatewayApp.userAlert.label">Label</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userAlert.values">Values</Translate>
                </th>
                <th onClick={this.sort('alertType')}>
                  <Translate contentKey="gatewayApp.userAlert.alertType">Alert Type</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userAlert.userRead">User Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userAlert.familyRead">Family Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userAlert.organizationRead">Organization Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userAlert.user">User</Translate>
                </th>
                <th className="hand" onClick={this.sort('createdDate')}>
                  <Translate contentKey="userManagement.createdDate">Created Date</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {userAlertList.filter(this.filterFn).map((userAlert, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${userAlert.id}`} color="link" size="sm">
                      {userAlert.id}
                    </Button>
                  </td>
                  <td>{userAlert.label}</td>
                  <td>{userAlert.values}</td>
                  <td>
                    <Translate contentKey={`gatewayApp.AlertType.${userAlert.alertType}`} />
                  </td>
                  <td>{userAlert.userRead ? 'true' : 'false'}</td>
                  <td>{userAlert.familyRead ? 'true' : 'false'}</td>
                  <td>{userAlert.organizationRead ? 'true' : 'false'}</td>
                  <td>
                    {userAlert.userExtraId ? (
                      <Link to={`user-extra/${userAlert.userExtraId}`}>
                        {userAlert.userExtraFirstName ? userAlert.userExtraFirstName : ''}{' '}
                        {userAlert.userExtraLastName ? userAlert.userExtraLastName : ''}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    <TextFormat value={userAlert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
                  </td>

                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${userAlert.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userAlert.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userAlert.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ userAlert, userExtra }: IRootState) => ({
  userAlertList: userAlert.entities,
  isFetching: userAlert.loading,
  totalItems: userAlert.totalItems,
  userExtras: userExtra.entities
});

const mapDispatchToProps = {
  getEntities,
  getUserExtras
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAlert);
