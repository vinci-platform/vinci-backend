import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import MobileAppSettings from './mobile-app-settings';
import MobileAppSettingsDetail from './mobile-app-settings-detail';
import MobileAppSettingsUpdate from './mobile-app-settings-update';
import MobileAppSettingsDeleteDialog from './mobile-app-settings-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={MobileAppSettingsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={MobileAppSettingsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={MobileAppSettingsDetail} />
      <ErrorBoundaryRoute path={match.url} component={MobileAppSettings} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={MobileAppSettingsDeleteDialog} />
  </>
);

export default Routes;
