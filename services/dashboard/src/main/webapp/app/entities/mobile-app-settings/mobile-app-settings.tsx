import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getPaginationItemsNumber, JhiPagination, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './mobile-app-settings.reducer';
import { IMobileAppSettings } from 'app/shared/model/mobile-app-settings.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IMobileAppSettingsProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IMobileAppSettingsState extends IPaginationBaseState {
  filter: string;
  showModal: boolean;
  deviceEntity: any;
}

export class MobileAppSettings extends React.Component<IMobileAppSettingsProps, IMobileAppSettingsState> {
  state: IMobileAppSettingsState = {
    filter: '',
    showModal: false,
    deviceEntity: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getMobileAppSettings();
  }

  sortMobileAppSettings = () => {
    this.getMobileAppSettings();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortMobileAppSettings());

  getMobileAppSettings = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { mobileAppSettingsList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="mobile-app-settings-heading">
          <Translate contentKey="gatewayApp.mobileAppSettings.home.title">Mobile App Settings</Translate>
          <Link
            to={`${match.url}/new`}
            className="btn btn-primary float-right jh-create-entity"
            id="jh-create-entity"
            style={{ fontSize: '0.9rem' }}
          >
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.mobileAppSettings.home.createLabel">Create new Mobile App Settings</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.mobileAppSettings.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.mobileAppSettings.value">Value</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.mobileAppSettings.userId">User Id</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {mobileAppSettingsList.map((mobileAppSettings, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${mobileAppSettings.id}`} color="link" size="sm">
                      {mobileAppSettings.id}
                    </Button>
                  </td>
                  <td>{mobileAppSettings.name}</td>
                  <td>{mobileAppSettings.value}</td>
                  <td>{mobileAppSettings.userId === 0 ? 'Global setting' : mobileAppSettings.userId}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${mobileAppSettings.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${mobileAppSettings.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${mobileAppSettings.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ mobileAppSettings }: IRootState) => ({
  mobileAppSettingsList: mobileAppSettings.entities,
  totalItems: mobileAppSettings.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileAppSettings);
