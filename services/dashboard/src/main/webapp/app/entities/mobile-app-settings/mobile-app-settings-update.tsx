import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';

import { getEntity, updateEntity, createEntity, reset } from './mobile-app-settings.reducer';
import { IMobileAppSettings } from 'app/shared/model/mobile-app-settings.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IMobileAppSettingsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IMobileAppSettingsUpdateState {
  isNew: boolean;
}

export class MobileAppSettingsUpdate extends React.Component<IMobileAppSettingsUpdateProps, IMobileAppSettingsUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUserExtras(null,1000,null);
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { mobileAppSettingsEntity } = this.props;
      const entity = {
        ...mobileAppSettingsEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/mobile-app-settings');
  };

  render() {
    const { mobileAppSettingsEntity, loading, updating, userExtras } = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.mobileAppSettings.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.mobileAppSettings.home.createOrEditLabel">Create or edit a MobileAppSettings</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : mobileAppSettingsEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="mobile-app-settings-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="name">
                    <Translate contentKey="gatewayApp.mobileAppSettings.name">Name</Translate>
                  </Label>
                  <AvField
                    id="mobile-app-settings-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="valueLabel" for="value">
                    <Translate contentKey="gatewayApp.mobileAppSettings.value">Value</Translate>
                  </Label>
                  <AvField 
                    id="mobile-app-settings-value" 
                    type="text" 
                    name="value"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }} />
                </AvGroup>
                <AvGroup>
                  <Label id="userIdLabel" for="userId">
                    <Translate contentKey="gatewayApp.mobileAppSettings.user">User</Translate>
                  </Label>
                  <AvInput id="userIdInput" type="select" className="form-control " name="userId">
                        <option value="0" key="0">Global settings</option>
                        {userExtras
                          ? userExtras.map(otherEntity => (
                              <option value={otherEntity.userId} key={otherEntity.userId} selected>
                                {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.userId}
                              </option>
                            ))
                          : null}
                      </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/mobile-app-settings" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  mobileAppSettingsEntity: storeState.mobileAppSettings.entity,
  loading: storeState.mobileAppSettings.loading,
  updating: storeState.mobileAppSettings.updating,
  updateSuccess: storeState.mobileAppSettings.updateSuccess,
  userExtras: storeState.userExtra.entities
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileAppSettingsUpdate);
