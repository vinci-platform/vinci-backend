import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './mobile-app-settings.reducer';
import { IMobileAppSettings } from 'app/shared/model/mobile-app-settings.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMobileAppSettingsDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class MobileAppSettingsDetail extends React.Component<IMobileAppSettingsDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { mobileAppSettingsEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.mobileAppSettings.detail.title">MobileAppSettings</Translate> [
            <b>{mobileAppSettingsEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="gatewayApp.mobileAppSettings.name">Name</Translate>
              </span>
            </dt>
            <dd>{mobileAppSettingsEntity.name}</dd>
            <dt>
              <span id="value">
                <Translate contentKey="gatewayApp.mobileAppSettings.value">Value</Translate>
              </span>
            </dt>
            <dd>{mobileAppSettingsEntity.value}</dd>
            <dt>
              <span id="userId">
                <Translate contentKey="gatewayApp.mobileAppSettings.userId">User Id</Translate>
              </span>
            </dt> 
            <dd>{mobileAppSettingsEntity.userId === 0 ? "Global Settings" : mobileAppSettingsEntity.userId}</dd>
          </dl>
          <Button tag={Link} to="/entity/mobile-app-settings" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/mobile-app-settings/${mobileAppSettingsEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ mobileAppSettings }: IRootState) => ({
  mobileAppSettingsEntity: mobileAppSettings.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileAppSettingsDetail);
