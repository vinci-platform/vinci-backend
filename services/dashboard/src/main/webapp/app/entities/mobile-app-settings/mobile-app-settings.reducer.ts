import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IMobileAppSettings, defaultValue } from 'app/shared/model/mobile-app-settings.model';

export const ACTION_TYPES = {
  FETCH_MOBILEAPPSETTINGS_LIST: 'mobileAppSettings/FETCH_MOBILEAPPSETTINGS_LIST',
  FETCH_MOBILEAPPSETTINGS: 'mobileAppSettings/FETCH_MOBILEAPPSETTINGS',
  CREATE_MOBILEAPPSETTINGS: 'mobileAppSettings/CREATE_MOBILEAPPSETTINGS',
  UPDATE_MOBILEAPPSETTINGS: 'mobileAppSettings/UPDATE_MOBILEAPPSETTINGS',
  DELETE_MOBILEAPPSETTINGS: 'mobileAppSettings/DELETE_MOBILEAPPSETTINGS',
  RESET: 'mobileAppSettings/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMobileAppSettings>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type MobileAppSettingsState = Readonly<typeof initialState>;

// Reducer

export default (state: MobileAppSettingsState = initialState, action): MobileAppSettingsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_MOBILEAPPSETTINGS):
    case REQUEST(ACTION_TYPES.UPDATE_MOBILEAPPSETTINGS):
    case REQUEST(ACTION_TYPES.DELETE_MOBILEAPPSETTINGS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS):
    case FAILURE(ACTION_TYPES.CREATE_MOBILEAPPSETTINGS):
    case FAILURE(ACTION_TYPES.UPDATE_MOBILEAPPSETTINGS):
    case FAILURE(ACTION_TYPES.DELETE_MOBILEAPPSETTINGS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_MOBILEAPPSETTINGS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_MOBILEAPPSETTINGS):
    case SUCCESS(ACTION_TYPES.UPDATE_MOBILEAPPSETTINGS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_MOBILEAPPSETTINGS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/mobile-app-settings';

// Actions
export const getEntities: ICrudGetAllAction<IMobileAppSettings> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_MOBILEAPPSETTINGS_LIST,
    payload: axios.get<IMobileAppSettings>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IMobileAppSettings> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_MOBILEAPPSETTINGS,
    payload: axios.get<IMobileAppSettings>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IMobileAppSettings> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_MOBILEAPPSETTINGS,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IMobileAppSettings> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_MOBILEAPPSETTINGS,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IMobileAppSettings> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_MOBILEAPPSETTINGS,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
