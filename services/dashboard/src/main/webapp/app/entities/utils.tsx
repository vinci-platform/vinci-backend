import { interval } from 'app/config/constants';

export const getAdditionToTimestamp = (historyInterval, fromDate: string, toDate: string) => {
  switch (historyInterval) {
    case interval.ONE_DAY:
      return 1 * 24 * 60 * 60 * 1000;
    case interval.ONE_WEEK:
      return 7 * 24 * 60 * 60 * 1000;
    case interval.TWO_WEEKS:
      return 14 * 24 * 60 * 60 * 1000;
    case interval.THREE_WEEKS:
      return 21 * 24 * 60 * 60 * 1000;
    case interval.ONE_MONTH:
      return 30 * 24 * 60 * 60 * 1000;
    case interval.THREE_MONTHS:
      return 90 * 24 * 60 * 60 * 1000;
    case interval.CUSTOM: {
      const [from] = fromDate.split('T');
      const [to] = toDate.split('T');
      return new Date(from);
    }
    default:
      return 90 * 24 * 60 * 60 * 1000;
  }
};
