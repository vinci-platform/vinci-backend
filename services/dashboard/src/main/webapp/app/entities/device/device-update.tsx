import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserExtra } from 'app/shared/model/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';
import { getEntity, updateEntity, createEntity, reset } from './device.reducer';
import { DeviceType, IDevice } from 'app/shared/model/device.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

import './../../app.scss';
import { IDeviceAlert } from 'app/shared/model/device-alert.model';

export interface IDeviceUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IDeviceUpdateState {
  isNew: boolean;
  userExtraId: string;
}

export class DeviceUpdate extends React.Component<IDeviceUpdateProps, IDeviceUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userExtraId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUserExtras(null,1000,null);
  }

  mapAlerts(values) {
    return values.map(alert => (alert !== '' ? (typeof alert === 'object' ? alert : JSON.parse(alert)) : {}));
  }

  saveEntity = (event, errors, values) => {
    values.alerts = this.mapAlerts(values.alerts);
    if (errors.length === 0) {
      const { deviceEntity } = this.props;
      const entity = {
        ...deviceEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/device');
  };

  render() {
    const { deviceEntity, userExtras, loading, updating, isPatient } = this.props;
    const { isNew } = this.state;
    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.device.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.device.home.createOrEditLabel">Create or edit a Device</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : deviceEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="device-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="name">
                    <Translate contentKey="gatewayApp.device.name">Name</Translate>
                  </Label>
                  <AvField
                    id="device-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="gatewayApp.device.description">Description</Translate>
                  </Label>
                  <AvField id="device-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="uuidLabel" for="uuid">
                    <Translate contentKey="gatewayApp.device.uuid">Uuid</Translate>
                  </Label>
                  <AvField
                    id="device-uuid"
                    type="text"
                    name="uuid"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="deviceTypeLabel">
                    <Translate contentKey="gatewayApp.device.deviceType">Device Type</Translate>
                  </Label>
                  <AvInput
                    id="device-deviceType"
                    type="select"
                    className="form-control"
                    name="deviceType"
                    value={(!isNew && deviceEntity.deviceType) || 'WATCH'}
                  >
                    <option value="WATCH">{translate('gatewayApp.DeviceType.WATCH')}</option>
                    <option value="SHOE">{translate('gatewayApp.DeviceType.SHOE')}</option>
                    <option value="CAMERA_FITNESS">{translate('gatewayApp.DeviceType.CAMERA_FITNESS')}</option>
                    <option value="CAMERA_MOVEMENT">{translate('gatewayApp.DeviceType.CAMERA_MOVEMENT')}</option>
                    <option value="SURVEY">{translate('gatewayApp.DeviceType.SURVEY')}</option>
                    <option value="FITBIT_WATCH">{translate('gatewayApp.DeviceType.FITBIT_WATCH')}</option>
                  </AvInput>
                </AvGroup>
                <AvGroup check>
                  <Label id="activeLabel" check>
                    <AvInput
                      id="device-active"
                      type="checkbox"
                      name="active"
                      disabled={deviceEntity.deviceType === DeviceType.FITBIT_WATCH || deviceEntity.deviceType === DeviceType.SURVEY}
                    />
                    <Translate contentKey="gatewayApp.device.active">Active</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="startTimeStampLabel" for="startTimeStamp">
                    <Translate contentKey="gatewayApp.device.startTimestamp">Start Timestamp</Translate>
                  </Label>
                  <AvField id="device-startTimestamp" type="text" name="startTimestamp" />
                </AvGroup>
                <AvGroup>
                  <Label id="alertsLabel" for="alerts">
                    <Translate contentKey="gatewayApp.device.alerts">Alerts</Translate>
                  </Label>
                  <AvInput id="device-alerts" type="select" className="form-control" name="alerts" multiple>
                    <option value="" key="0" />
                    {deviceEntity.alerts
                      ? deviceEntity.alerts.map(alert => (
                          <option value={JSON.stringify(alert)} key={alert.id}>
                            {alert.createdDate} {alert.alertType} {alert.values}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                {!isPatient && (
                  <Fragment>
                    <AvGroup>
                      <Label for="userExtra.id">
                        <Translate contentKey="gatewayApp.device.user">User</Translate>
                      </Label>
                      <AvInput id="device-userExtra" type="select" className="form-control " name="userExtraId">
                        <option value="Pick user for device" key="0" />
                        {userExtras
                          ? userExtras.map(otherEntity => (
                              <option value={otherEntity.id} key={otherEntity.id} selected>
                                {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.id}
                              </option>
                            ))
                          : null}
                      </AvInput>
                    </AvGroup>
                  </Fragment>
                )}
                <Button tag={Link} id="cancel-save" to="/entity/device" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  userExtras: storeState.userExtra.entities,
  deviceEntity: storeState.device.entity,
  loading: storeState.device.loading,
  updating: storeState.device.updating,
  updateSuccess: storeState.device.updateSuccess,
  isPatient: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.PACIENT])
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceUpdate);
