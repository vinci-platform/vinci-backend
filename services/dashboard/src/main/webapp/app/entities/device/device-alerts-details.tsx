import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Row, Col, Table, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';

import { IRootState } from 'app/shared/reducers';
import { APP_DATE_FORMAT } from 'app/config/constants';

export interface IDeviceAlertsDetailsProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
  showModal: boolean;
  handleClose: Function;
  deviceEntity: any;
}

export class DeviceAlertsDetails extends React.Component<IDeviceAlertsDetailsProps> {
  render() {
    const { deviceEntity, handleClose } = this.props;
    return (
      <Modal isOpen={this.props.showModal} toggle={handleClose} backdrop="static" id="login-page" autoFocus={false}>
        <ModalHeader id="login-title" toggle={handleClose}>
          <Translate contentKey="gatewayApp.device.alerts">Your recent alerts:</Translate>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col md="12">
              <Table responsive>
                {deviceEntity.alerts !== undefined &&
                  deviceEntity.alerts
                    .sort((a, b) => b.id - a.id)
                    .slice(0, 10)
                    .map(alert => (
                      <tbody>
                        <td>{alert.alertType}</td>
                        <td>{<TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />}</td>
                        <td>
                          {alert.label} {alert.values}
                        </td>
                      </tbody>
                    ))}
              </Table>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}

const mapStateToProps = ({ device }: IRootState) => ({
  deviceEntity: device.entity
});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceAlertsDetails);
