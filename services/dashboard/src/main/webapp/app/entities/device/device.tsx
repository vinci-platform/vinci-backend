import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table, Label, Input } from 'reactstrap';
import { Translate, getSortState, IPaginationBaseState, JhiPagination, getPaginationItemsNumber, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './device.reducer';
import { AUTHORITIES } from 'app/config/constants';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import { AlertType } from 'app/shared/model/user-alert.model';
import './../../app.scss';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import { DeviceAlertsDetails } from 'app/entities/device/device-alerts-details';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';

export interface IDeviceProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IDeviceState extends IPaginationBaseState {
  filter: string;
  showModal: boolean;
  deviceEntity: any;
  showFilters: boolean;
  deviceTypeValue: string;
  userValue: string;
  dateFrom: string;
  dateTo: string;
}

export class Device extends React.Component<IDeviceProps, IDeviceState> {
  state: IDeviceState = {
    filter: '',
    showModal: false,
    deviceEntity: '',
    showFilters: false,
    deviceTypeValue: '',
    userValue: '',
    dateFrom: '',
    dateTo: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getDevices();
    this.props.getUserExtras(null, MAX_SIZE);
  }

  deviceTypeOnChange = event => {
    this.setState(
      {
        deviceTypeValue: event.target.value
      },
      () => this.getDevices()
    );
  };

  usersOnChange = event => {
    this.setState(
      {
        userValue: event.target.value
      },
      () => this.getDevices()
    );
  };

  dateFromOnChange = event => {
    this.setState(
      {
        dateFrom: event.target.value
      },
      () => this.getDevices()
    );
  };

  dateToOnChange = event => {
    this.setState(
      {
        dateTo: event.target.value
      },
      () => this.getDevices()
    );
  };

  setFilter = evt => {
    this.setState({
      filter: evt.target.value
    });
  };

  filterFn = l =>
  l.userExtraFirstName
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  l.name
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  l.description
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  l.userExtraLastName
    .toString()
    .toUpperCase()
    .includes(this.state.filter.toUpperCase()) ||
  l.uuid.toString().includes(this.state.filter) ||
  (l.organizationFirstName &&
    l.organizationFirstName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase())) ||
  (l.organizationLastName &&
    l.organizationLastName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase())) ||
  (l.familyLastName &&
    l.familyLastName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase())) ||
  (l.familyFirstName &&
    l.familyFirstName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()));

  handleRemoveFilters = () => {
    this.setState(
      {
        deviceTypeValue: '',
        userValue: '',
        dateFrom: '',
        dateTo: ''
      },
      () => this.getDevices()
    );
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortDevices()
    );
  };

  sortDevices = () => {
    this.getDevices();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortDevices());

  getDevices = () => {
    const { activePage, itemsPerPage, sort, order, deviceTypeValue, userValue, dateFrom, dateTo } = this.state;
    const { isPatient, isAdmin } = this.props;

    let filter =
      `${deviceTypeValue !== '' ? `deviceType.equals=${deviceTypeValue}` : ''}` +
      `${deviceTypeValue ? '&' : ''}` +
      `${!isPatient && userValue !== '' ? `userExtraId.equals=${userValue}` : ''}` +
      `${!isPatient && userValue ? '&' : ''}` +
      `${dateFrom !== '' ? `startTimestamp.greaterOrEqualThan=${new Date(dateFrom).toISOString()}` : ''}` +
      `${dateFrom ? '&' : ''}` +
      `${dateTo !== '' ? `startTimestamp.lessOrEqualThan=${new Date(dateTo).toISOString()}` : ''}`;

    if (filter.endsWith('&')) filter = filter.slice(0, -1);

    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`, filter);
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  openModal = device => {
    this.setState({ showModal: true, deviceEntity: device });
  };

  render() {
    const RedTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ff0000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const GreenTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#008000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const OrangeTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ffc107',
        fontSize: '1em'
      }
    }))(Tooltip);

    const { deviceList, match, isPatient, isAdmin, isFetching, totalItems, userExtras, isOrganization, isFamily } = this.props;
    const { filter, showFilters, deviceTypeValue, userValue, dateFrom, dateTo } = this.state;
    return (
      <div>
        <h2 id="device-heading" className="d-flex flex-wrap justify-content-between" style={{ marginBottom: '50px' }}>
          <Translate contentKey="gatewayApp.device.home.title">Devices</Translate>
          <div className="d-flex flex-wrap col-md-6 justify-content-between search-container">
            <div className="d-flex col-md-8">
              <div style={{ margin: 'auto' }}>
                <Translate contentKey="logs.search">Search</Translate>
              </div>
              <input
                type="search"
                value={filter}
                onChange={this.setFilter}
                disabled={isFetching}
                className="form-control"
                style={{ marginLeft: '10px', width: '100% !important' }}
              />
              <Button type="submit" color="primary">
                <FontAwesomeIcon icon="search" />
              </Button>
            </div>
            { !isPatient &&
              <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
                <FontAwesomeIcon icon="plus" />
                &nbsp;
                <Translate contentKey="gatewayApp.device.home.createLabel">Create new Device</Translate>
              </Link>
            }
          </div>
        </h2>

        <div className="pb-3 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label id="deviceTypeLabel">
                    <Translate contentKey="gatewayApp.device.deviceType">Device Type</Translate>
                  </Label>
                  <AvInput
                    id="device-deviceType"
                    type="select"
                    className="form-control"
                    name="deviceType"
                    value={deviceTypeValue}
                    onChange={this.deviceTypeOnChange}
                  >
                    <option value="" />
                    <option value="WATCH">{translate('gatewayApp.DeviceType.WATCH')}</option>
                    <option value="SHOE">{translate('gatewayApp.DeviceType.SHOE')}</option>
                    <option value="CAMERA_FITNESS">{translate('gatewayApp.DeviceType.CAMERA_FITNESS')}</option>
                    <option value="CAMERA_MOVEMENT">{translate('gatewayApp.DeviceType.CAMERA_MOVEMENT')}</option>
                    <option value="SURVEY">{translate('gatewayApp.DeviceType.SURVEY')}</option>
                    <option value="FITBIT_WATCH">{translate('gatewayApp.DeviceType.FITBIT_WATCH')}</option>
                  </AvInput>
                </AvGroup>
                {!isPatient && (
                  <AvGroup className="pr-2">
                    <Label for="userExtra.id">
                      <Translate contentKey="gatewayApp.device.user">User</Translate>
                    </Label>
                    <AvInput
                      id="device-userExtra"
                      type="select"
                      className="form-control "
                      name="userExtraId"
                      value={userValue}
                      onChange={this.usersOnChange}
                    >
                      <option value="" key="0" />
                      {userExtras
                        ? userExtras.map(otherEntity => (
                            <option value={otherEntity.id} key={otherEntity.id}>
                              {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.id}
                            </option>
                          ))
                        : null}
                    </AvInput>
                  </AvGroup>
                )}
                <div className="pr-2">
                  <label>Date from</label>
                  <Input type="date" value={dateFrom} onChange={this.dateFromOnChange} />
                </div>
                <div className="pr-2">
                  <label>Date to</label>
                  <Input type="date" value={dateTo} onChange={this.dateToOnChange} />
                </div>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>

        <div className="table-responsive">
          <Table responsive className="table-fixed">
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th onClick={this.sort('name')}>
                  <Translate contentKey="gatewayApp.device.name">Name</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.description">Description</Translate>
                </th>
                <th onClick={this.sort('uuid')}>
                  <Translate contentKey="gatewayApp.device.uuid">Uuid</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.deviceType">Device Type</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.active">Active</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.startTimestamp">Start Timestamp</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.alerts">Alerts</Translate>
                </th>

                {!isPatient && (
                  <Fragment>
                    <th>
                      <Translate contentKey="gatewayApp.device.user">User Extra</Translate>
                    </th>
                    <th onClick={this.sort('userExtra.family.lastName')}>
                      <Translate contentKey="gatewayApp.device.family">Family</Translate>
                      <FontAwesomeIcon icon="sort" />
                    </th>
                      {!isOrganization &&
                      <th onClick={this.sort('userExtra.organization.firstName')}>
                        <Translate contentKey="gatewayApp.device.organisation">Organisation</Translate>
                        <FontAwesomeIcon icon="sort" />
                      </th>}
                  </Fragment>
                )}
                <th />
              </tr>
            </thead>
            <tbody>
              {deviceList.filter(this.filterFn).map((device, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${device.id}`} color="link" size="sm">
                      {device.id}
                    </Button>
                  </td>
                  <td>{device.name}</td>
                  <td className="truncate-text">{device.description}</td>
                  <td>{device.uuid}</td>
                  <td>
                    <Translate contentKey={`gatewayApp.DeviceType.${device.deviceType}`} />
                  </td>
                  <td>{device.active ? 'true' : 'false'}</td>
                  <td>{device.startTimestamp}</td>
                  <td>
                    <DeviceAlertsDetails
                      handleClose={this.handleClose}
                      showModal={this.state.showModal}
                      deviceEntity={this.state.deviceEntity}
                      history={`${this.props.location.pathname}/${device.id}/alerts` as any}
                      location={this.props.location}
                      match={match.url as any}
                    />
                    {device.alerts.length !== 0 &&
                      (device.alerts
                        .filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0]
                        .alertType.toString() === AlertType.DANGER ? (
                        <div>
                          <RedTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.deviceAlert.${
                                  device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0].alertType
                                }`}
                                interpolate={{
                                  value: device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0]
                                    .values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: 'red' }} onClick={this.openModal.bind(this, device)}>
                              <div style={{ color: 'red' }}>
                                <Translate contentKey="gatewayApp.device.errorStatus" />
                              </div>
                            </Button>
                          </RedTooltip>
                        </div>
                      ) : device.alerts
                        .filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0]
                        .alertType.toString() === AlertType.WARNING ? (
                        <div>
                          <OrangeTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.deviceAlert.${
                                  device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0].alertType
                                }`}
                                interpolate={{
                                  value: device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0]
                                    .values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: '#ffa500' }} onClick={this.openModal.bind(this, device)}>
                              <div style={{ color: 'orange' }}>
                                <Translate contentKey="gatewayApp.device.errorStatus" />
                              </div>
                            </Button>
                          </OrangeTooltip>
                        </div>
                      ) : (
                        <div>
                          <GreenTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.deviceAlert.${
                                  device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0].alertType
                                }`}
                                interpolate={{
                                  value: device.alerts.filter(dev => dev.id === Math.max.apply(Math, device.alerts.map(o => o.id)))[0]
                                    .values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: '#008000' }} onClick={this.openModal.bind(this, device)}>
                              <div style={{ color: 'green' }}>
                                <Translate contentKey="gatewayApp.device.okStatus" />
                              </div>
                            </Button>
                          </GreenTooltip>
                        </div>
                      ))}
                  </td>
                  {!isPatient && (
                    <Fragment>
                      <td>
                        {device.userExtraId ? (
                          <Link to={`user-extra/${device.userExtraId}`}>
                            {device.userExtraFirstName ? device.userExtraFirstName : ''}{' '}
                            {device.userExtraLastName ? device.userExtraLastName : ''}
                          </Link>
                        ) : (
                          ''
                        )}
                      </td>
                        {!isFamily && <td>
                        {device.userFamilyFirstName ? device.userFamilyFirstName : ''}{' '}
                        {device.userFamilyLastName ? device.userFamilyLastName : ''}
                      </td>}
                        {!isOrganization && <td>
                            {device.userOrganisationFirstName ? device.userOrganisationFirstName : ''}{' '}
                            {device.userOrganisationLastName ? device.userOrganisationLastName : ''}
                        </td>}
                    </Fragment>
                  )}
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${device.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      {!isPatient && <Button tag={Link} to={`${match.url}/${device.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>}
                      {isAdmin &&<Button tag={Link} to={`${match.url}/${device.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ device, authentication, deviceAlert, userExtra }: IRootState) => ({
  deviceList: device.entities,
  isPatient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT]),
  isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
  isOrganization: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ORGANIZATION]),
  isFamily: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.FAMILY]),
  isFetching: device.loading,
  totalItems: device.totalItems,
  userExtras: userExtra.entities
});

const mapDispatchToProps = {
  getEntities,
  getUserExtras
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Device);
