import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './device.reducer';
import { APP_DATE_FORMAT } from 'app/config/constants';

export interface IDeviceDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class DeviceDetail extends React.Component<IDeviceDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { deviceEntity, isPatient } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.device.detail.title">Device</Translate> [<b>{deviceEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="gatewayApp.device.name">Name</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.name}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="gatewayApp.device.description">Description</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.description}</dd>
            <dt>
              <span id="uuid">
                <Translate contentKey="gatewayApp.device.uuid">Uuid</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.uuid}</dd>
            <dt>
              <span id="deviceType">
                <Translate contentKey="gatewayApp.device.deviceType">Device Type</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.deviceType}</dd>
            <dt>
              <span id="active">
                <Translate contentKey="gatewayApp.device.active">Active</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.active ? 'true' : 'false'}</dd>
            <dt>
              <span id="user">
                <Translate contentKey="gatewayApp.device.user">User</Translate>
              </span>
            </dt>
            <dd>
              {deviceEntity.userExtraFirstName ? deviceEntity.userExtraFirstName : ''}{' '}
              {deviceEntity.userExtraLastName ? deviceEntity.userExtraLastName : ''}
            </dd>
            <dt>
              <span id="startTimestamp">
                <Translate contentKey="gatewayApp.device.startTimestamp">Start Timestamp</Translate>
              </span>
            </dt>
            <dd>{deviceEntity.startTimestamp}</dd>
            <dt>
              <span id="alerts">
                <Translate contentKey="gatewayApp.device.alerts">Alerts</Translate>
              </span>
            </dt>
            {deviceEntity.alerts &&
              deviceEntity.alerts.map((alert, index) => (
                <dd key={index}>
                  {alert.alertType} <TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />{' '}
                  {alert.values}
                </dd>
              ))}
            <dt>
              <span id="family">
                <Translate contentKey="gatewayApp.device.family">Family</Translate>
              </span>
            </dt>
            <dd>
              {deviceEntity.userFamilyFirstName ? deviceEntity.userFamilyFirstName : ''}{' '}
              {deviceEntity.userFamilyLastName ? deviceEntity.userFamilyLastName : ''}
            </dd>
            <dt>
              <span id="organisation">
                <Translate contentKey="gatewayApp.device.organisation">Organisation</Translate>
              </span>
            </dt>
            <dd>
              {deviceEntity.userOrganisationFirstName ? deviceEntity.userOrganisationFirstName : ''}{' '}
              {deviceEntity.userOrganisationLastName ? deviceEntity.userOrganisationLastName : ''}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/device" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          {!isPatient && <Button tag={Link} to={`/entity/device/${deviceEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>}
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ device, authentication }: IRootState) => ({
  deviceEntity: device.entity,
  isPatient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT]),
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceDetail);
