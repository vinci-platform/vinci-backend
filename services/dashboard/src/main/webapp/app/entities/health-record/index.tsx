import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import HealthRecord from './health-record';
import HealthRecordDetail from './health-record-detail';
import HealthRecordUpdate from './health-record-update';
import HealthRecordDeleteDialog from './health-record-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={HealthRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={HealthRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={HealthRecordDetail} />
      <ErrorBoundaryRoute path={match.url} component={HealthRecord} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={HealthRecordDeleteDialog} />
  </>
);

export default Routes;
