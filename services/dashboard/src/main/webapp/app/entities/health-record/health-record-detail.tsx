import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './health-record.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface IHealthRecordDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class HealthRecordDetail extends React.Component<IHealthRecordDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { healthRecordEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.healthRecord.detail.title">HealthRecord</Translate> [<b>{healthRecordEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="userId">
                <Translate contentKey="gatewayApp.healthRecord.userId">User Id</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.userId}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.healthRecord.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(healthRecordEntity.timestamp)}</dd>
            <dt>
              <span id="score">
                <Translate contentKey="gatewayApp.healthRecord.score">Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.score}</dd>
            <dt>
              <span id="availableScore">
                <Translate contentKey="gatewayApp.healthRecord.availableScore">Available Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.availableScore}</dd>
            <dt>
              <span id="stepsScore">
                <Translate contentKey="gatewayApp.healthRecord.stepsScore">Steps Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.stepsScore}</dd>
            <dt>
              <span id="ipaqScore">
                <Translate contentKey="gatewayApp.healthRecord.ipaqScore">Ipaq Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.ipaqScore}</dd>
            <dt>
              <span id="whoqolScore">
                <Translate contentKey="gatewayApp.healthRecord.whoqolScore">Whoqol Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.whoqolScore}</dd>
            <dt>
              <span id="feelingsScore">
                <Translate contentKey="gatewayApp.healthRecord.feelingsScore">Feelings Score</Translate>
              </span>
            </dt>
            <dd>{healthRecordEntity.feelingsScore}</dd>
          </dl>
          <Button tag={Link} to="/entity/health-record" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/health-record/${healthRecordEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ healthRecord }: IRootState) => ({
  healthRecordEntity: healthRecord.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HealthRecordDetail);
