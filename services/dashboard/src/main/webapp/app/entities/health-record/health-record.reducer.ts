import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IHealthRecord, defaultValue } from 'app/shared/model/health-record.model';

export const ACTION_TYPES = {
  FETCH_HEALTHRECORD_LIST: 'healthRecord/FETCH_HEALTHRECORD_LIST',
  FETCH_HEALTHRECORD: 'healthRecord/FETCH_HEALTHRECORD',
  CREATE_HEALTHRECORD: 'healthRecord/CREATE_HEALTHRECORD',
  UPDATE_HEALTHRECORD: 'healthRecord/UPDATE_HEALTHRECORD',
  DELETE_HEALTHRECORD: 'healthRecord/DELETE_HEALTHRECORD',
  RESET: 'healthRecord/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IHealthRecord>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type HealthRecordState = Readonly<typeof initialState>;

// Reducer

export default (state: HealthRecordState = initialState, action): HealthRecordState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_HEALTHRECORD_LIST):
    case REQUEST(ACTION_TYPES.FETCH_HEALTHRECORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_HEALTHRECORD):
    case REQUEST(ACTION_TYPES.UPDATE_HEALTHRECORD):
    case REQUEST(ACTION_TYPES.DELETE_HEALTHRECORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_HEALTHRECORD_LIST):
    case FAILURE(ACTION_TYPES.FETCH_HEALTHRECORD):
    case FAILURE(ACTION_TYPES.CREATE_HEALTHRECORD):
    case FAILURE(ACTION_TYPES.UPDATE_HEALTHRECORD):
    case FAILURE(ACTION_TYPES.DELETE_HEALTHRECORD):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_HEALTHRECORD_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_HEALTHRECORD):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_HEALTHRECORD):
    case SUCCESS(ACTION_TYPES.UPDATE_HEALTHRECORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_HEALTHRECORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/health-records';

// Actions

export const getEntities: ICrudGetAllAction<IHealthRecord> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_HEALTHRECORD_LIST,
    payload: axios.get<IHealthRecord>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IHealthRecord> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_HEALTHRECORD,
    payload: axios.get<IHealthRecord>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IHealthRecord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_HEALTHRECORD,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IHealthRecord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_HEALTHRECORD,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IHealthRecord> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_HEALTHRECORD,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
