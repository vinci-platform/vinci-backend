import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';

import { getEntity, updateEntity, createEntity, reset } from './health-record.reducer';
import { IHealthRecord } from 'app/shared/model/health-record.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IHealthRecordUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IHealthRecordUpdateState {
  isNew: boolean;
}

export class HealthRecordUpdate extends React.Component<IHealthRecordUpdateProps, IHealthRecordUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
    this.props.getUserExtras(null,1000,null);
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { healthRecordEntity } = this.props;
      const entity = {
        ...healthRecordEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/health-record');
  };

  render() {
    const { healthRecordEntity, loading, updating, userExtras } = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.healthRecord.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.healthRecord.home.createOrEditLabel">Create or edit a HealthRecord</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : healthRecordEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="health-record-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="userIdLabel" for="userId">
                    <Translate contentKey="gatewayApp.healthRecord.user">User</Translate>
                  </Label>
                  <AvInput id="userIdInput" type="select" className="form-control " name="userId" required>
                        {userExtras
                          ? userExtras.map(otherEntity => (
                              <option value={otherEntity.userId} key={otherEntity.userId} selected>
                                {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.userId}
                              </option>
                            ))
                          : null}
                      </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.healthRecord.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField id="health-record-timestamp" type="string" className="form-control" name="timestamp" />
                </AvGroup>
                <AvGroup>
                  <Label id="scoreLabel" for="score">
                    <Translate contentKey="gatewayApp.healthRecord.score">Score</Translate>
                  </Label>
                  <AvField id="health-record-score" type="string" className="form-control" name="score" />
                </AvGroup>
                <AvGroup>
                  <Label id="availableScoreLabel" for="availableScore">
                    <Translate contentKey="gatewayApp.healthRecord.availableScore">Available Score</Translate>
                  </Label>
                  <AvField id="health-record-availableScore" type="string" className="form-control" name="availableScore" />
                </AvGroup>
                <AvGroup>
                  <Label id="stepsScoreLabel" for="stepsScore">
                    <Translate contentKey="gatewayApp.healthRecord.stepsScore">Steps Score</Translate>
                  </Label>
                  <AvField id="health-record-stepsScore" type="string" className="form-control" name="stepsScore" />
                </AvGroup>
                <AvGroup>
                  <Label id="ipaqScoreLabel" for="ipaqScore">
                    <Translate contentKey="gatewayApp.healthRecord.ipaqScore">Ipaq Score</Translate>
                  </Label>
                  <AvField id="health-record-ipaqScore" type="string" className="form-control" name="ipaqScore" />
                </AvGroup>
                <AvGroup>
                  <Label id="whoqolScoreLabel" for="whoqolScore">
                    <Translate contentKey="gatewayApp.healthRecord.whoqolScore">Whoqol Score</Translate>
                  </Label>
                  <AvField id="health-record-whoqolScore" type="string" className="form-control" name="whoqolScore" />
                </AvGroup>
                <AvGroup>
                  <Label id="feelingsScoreLabel" for="feelingsScore">
                    <Translate contentKey="gatewayApp.healthRecord.feelingsScore">Feelings Score</Translate>
                  </Label>
                  <AvField id="health-record-feelingsScore" type="string" className="form-control" name="feelingsScore" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/health-record" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  healthRecordEntity: storeState.healthRecord.entity,
  loading: storeState.healthRecord.loading,
  updating: storeState.healthRecord.updating,
  updateSuccess: storeState.healthRecord.updateSuccess,
  userExtras: storeState.userExtra.entities
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HealthRecordUpdate);
