import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity, updateEntity } from './user-extra.reducer';
import { getAssociatedPatients } from 'app/shared/reducers/user-utils';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

export interface IUserExtraDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserExtraDeleteDialog extends React.Component<IUserExtraDeleteDialogProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  confirmDelete = event => {
    if (this.props.isOrganization) {
      const userExtra = this.props.userExtraEntity;
      userExtra.organizationId = null;
      this.props.updateEntity(userExtra, () => this.props.getAssociatedPatients(null));
    } else {
      this.props.deleteEntity(this.props.userExtraEntity.id);
    }

    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { userExtraEntity, isOrganization } = this.props;
    return (
      <Modal isOpen toggle={this.handleClose}>
        <ModalHeader toggle={this.handleClose}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody id="gatewayApp.userExtra.delete.question">
          {isOrganization && (
            <Translate
              contentKey="gatewayApp.userExtra.delete.associationQuestion"
              interpolate={{ name: `${userExtraEntity.userFirstName} ${userExtraEntity.userLastName}` }}
            >
              Are you sure you want to delete this User?
            </Translate>
          )}
          {!isOrganization && (
            <Translate
              contentKey="gatewayApp.userExtra.delete.question"
              interpolate={{ name: `${userExtraEntity.userFirstName} ${userExtraEntity.userLastName}` }}
            >
              Are you sure you want to delete this User?
            </Translate>
          )}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button id="jhi-confirm-delete-userExtra" color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />
            &nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ userExtra, authentication }: IRootState) => ({
  userExtraEntity: userExtra.entity,
  isOrganization: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ORGANIZATION])
});

const mapDispatchToProps = { getEntity, deleteEntity, updateEntity, getAssociatedPatients };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserExtraDeleteDialog);
