import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-extra.reducer';
import {APP_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import userExtra from 'app/modules/home/organization/user-extra';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IUserExtraDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserExtraDetail extends React.Component<IUserExtraDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userExtraEntity, isAdmin, isPacient } = this.props;
    return (
      <div className="detail-entity-container">
        {userExtraEntity.id && (
          <Row>
            <Col md="8">
              <h2>
                <Translate contentKey="gatewayApp.userExtra.detail.title">User</Translate> [<b>{userExtraEntity.id}</b>]
              </h2>
              <dl className="jh-entity-details">
                <dt>
                  <Translate contentKey="gatewayApp.userExtra.user">User</Translate>
                </dt>
                <dd>
                  {userExtraEntity.userFirstName} {userExtraEntity.userLastName}
                </dd>
                <dt>
                  <span id="uuid">
                    <Translate contentKey="gatewayApp.userExtra.uuid">Uuid</Translate>
                  </span>
                </dt>
                <dd>{userExtraEntity.uuid}</dd>
                <dt>
                  <span id="address">
                    <Translate contentKey="gatewayApp.userExtra.address">Address</Translate>
                  </span>
                </dt>
                <dd>{userExtraEntity.address}</dd>
                <dt>
                  <span id="phone">
                    <Translate contentKey="gatewayApp.userExtra.phone">Phone</Translate>
                  </span>
                </dt>
                <dd>{userExtraEntity.phone}</dd>
                <dt>
                  <Translate contentKey="gatewayApp.userExtra.organization">Organization</Translate>
                </dt>
                <dd>
                  {userExtraEntity.organizationFirstName} {userExtraEntity.organizationLastName}
                </dd>
                <dt>
                  <Translate contentKey="gatewayApp.userExtra.family">Family</Translate>
                </dt>
                <dd>
                  {userExtraEntity.familyFirstName} {userExtraEntity.familyLastName}
                </dd>
                <dt>
                  <span id="description">
                    <Translate contentKey="gatewayApp.userExtra.description">Description</Translate>
                  </span>
                </dt>
                <dd>{userExtraEntity.description}</dd>
                <dt>
                  <span id="nrDevices">
                    <Translate contentKey="gatewayApp.userExtra.nrDevices">No of Devices</Translate>
                  </span>
                </dt>
                <dd>{ userExtraEntity.devices == null ? "0" : userExtraEntity.devices.length }</dd>
                <dt>
                  <span id="gender">
                    <Translate contentKey="gatewayApp.userExtra.gender">Gender</Translate>
                  </span>
                </dt>
                <dd>{userExtraEntity.gender}</dd>
                <dt>
                  <span id="alerts">
                    <Translate contentKey="gatewayApp.device.alerts">Alerts</Translate>
                  </span>
                </dt>
                {userExtraEntity.alerts &&
                  userExtraEntity.alerts.map((alert, index) => (
                    <dd key={index}>
                      {alert.alertType} <TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />{' '}
                      {alert.values}
                    </dd>
                  ))}
              </dl>
              <Button tag={Link} to="/entity/user-extra" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />{' '}
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
                {(isAdmin || isPacient) && <Button tag={Link} to={`/entity/user-extra/${userExtraEntity.id}/edit`} replace color="primary">
                    <FontAwesomeIcon icon="pencil-alt"/>{' '}
                    <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.edit">Edit</Translate>
                </span>
                </Button>}
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ userExtra, authentication }: IRootState) => ({
  userExtraEntity: userExtra.entity,
    isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
    isPacient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT])
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserExtraDetail);
