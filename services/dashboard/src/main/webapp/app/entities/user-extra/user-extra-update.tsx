import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-extra.reducer';
import { IUserExtra } from 'app/shared/model/user-extra.model';
// tslint:disable-next-line:no-unused-variable

import { AUTHORITIES } from 'app/config/constants';
import { hasAnyAuthority } from 'app/shared/auth/private-route';

export interface IUserExtraUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserExtraUpdateState {
  isNew: boolean;
  userId: string;
  organizationId: string;
  familyId: string;
  description: string;
  gender: string;
  image1?: File;
  image2?: File;
  friends: string;
}

export class UserExtraUpdate extends React.Component<IUserExtraUpdateProps, IUserExtraUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userId: '0',
      organizationId: '0',
      familyId: '0',
      description: '',
      gender: '',
      image1: null,
      image2: null,
      isNew: !this.props.match.params || !this.props.match.params.id,
      friends: ''
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUsers();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { userExtraEntity } = this.props;
      const entity = {
        ...userExtraEntity,
        ...values
      };

      console.log(entity);

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    if (this.props.isOrganization) {
      this.props.history.push('/');
    } else {
      this.props.history.push('/entity/user-extra');
    }
  };

  render() {
    const { userExtraEntity, users, loading, updating, isOrganization } = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.userExtra.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.userExtra.home.createOrEditLabel">Create or edit a UserExtra</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userExtraEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-extra-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : (
                  ''
                )}
                <AvGroup>
                  <Label id="phoneLabel" for="phone">
                    <Translate contentKey="gatewayApp.userExtra.phone">Phone</Translate>
                  </Label>
                  <AvField id="user-extra-phone" type="text" name="phone" />
                </AvGroup>
                <AvGroup>
                  <Label id="uuidLabel" for="uuid">
                    <Translate contentKey="gatewayApp.userExtra.uuid">Uuid</Translate>
                  </Label>
                  <AvField id="user-extra-uuid" type="text" name="uuid" required errorMessage="Uuid must not be empty!"/>
                </AvGroup>
                <AvGroup>
                  <Label id="addressLabel" for="address">
                    <Translate contentKey="gatewayApp.userExtra.address">Address</Translate>
                  </Label>
                  <AvField id="user-extra-address" type="text" name="address" />
                </AvGroup>
                <AvGroup>
                  <Label for="user.id">
                    <Translate contentKey="gatewayApp.userExtra.userCreation">User</Translate>
                  </Label>
                  <AvInput id="user-extra-user" type="select" className="form-control" name="userId" required disabled={isOrganization}>
                    <option value="" key="0" />
                    {users
                      ? users.map(
                          otherEntity =>
                            !otherEntity.authorities.includes(AUTHORITIES.ADMIN) && (
                              <option value={otherEntity.id} key={otherEntity.id}>
                                {otherEntity.firstName} {otherEntity.lastName}
                              </option>
                            )
                        )
                      : ''}
                  </AvInput>
                </AvGroup>
                {/* <AvGroup>
                  <Label for="user-extra-organization-id">
                    <Translate contentKey="gatewayApp.userExtra.organizationId">Organization id</Translate>
                  </Label>
                  <AvInput id="user-extra-organization-id" type="text" name="organizationId" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-extra-organization-first-name">
                    <Translate contentKey="gatewayApp.userExtra.organizationFirstName">Organization first name</Translate>
                  </Label>
                  <AvInput id="user-extra-organization-first-name" type="text" name="organizationFirstName" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-extra-organization-last-name">
                    <Translate contentKey="gatewayApp.userExtra.organizationLastName">Organization last name</Translate>
                  </Label>
                  <AvInput id="user-extra-organization-last-name" type="text" name="organizationLastName" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-extra-family-id">
                    <Translate contentKey="gatewayApp.userExtra.familyId">Family id</Translate>
                  </Label>
                  <AvInput id="user-extra-family-id" type="text" name="familyId" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-extra-family-first-name">
                    <Translate contentKey="gatewayApp.userExtra.familyFirstName">Family first name</Translate>
                  </Label>
                  <AvInput id="user-extra-family-first-name" type="text" name="familyFirstName" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-extra-family-last-name">
                    <Translate contentKey="gatewayApp.userExtra.familyLastName">Family last name</Translate>
                  </Label>
                  <AvInput id="user-extra-family-last-name" type="text" name="familyLastName" />
                </AvGroup> */}
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="gatewayApp.userExtra.description">Description</Translate>
                  </Label>
                  <AvField id="user-extra-description" type="text" name="description" />
                </AvGroup>
                {/* <AvGroup>
                  <Label id="educationLabel" for="education">
                    <Translate contentKey="gatewayApp.userExtra.education">Education</Translate>
                  </Label>
                  <AvField id="user-extra-education" type="text" name="education" />
                </AvGroup>
                <AvGroup>
                  <Label id="maritalStatusLabel" for="maritalStatus">
                    <Translate contentKey="gatewayApp.userExtra.maritalStatus">Marital Status</Translate>
                  </Label>
                  <AvField id="user-extra-marital-status" type="text" name="maritalStatus" />
                </AvGroup> */}
                <AvGroup>
                  <Label id="userExtraGender">
                    <Translate contentKey="gatewayApp.userExtra.gender">Gender</Translate>
                  </Label>
                  <AvInput id="userExtra-gender" type="select" className="form-control" name="gender" htmlFor="gender" value="" key="" required errorMessage="You must choose a gender!">
                    <option value="" />
                    <option value="Male">{translate('gatewayApp.userExtra.MALE')}</option>
                    <option value="Female">{translate('gatewayApp.userExtra.FEMALE')}</option>
                  </AvInput>
                </AvGroup>
                {!isOrganization && <AvGroup>
                  <Label id="friendsLabel" for="friends">
                    <Translate contentKey="gatewayApp.userExtra.friends">Friends</Translate>
                  </Label>
                  <AvField id="user-extra-friends" type="text" name="friends"/>
                </AvGroup>}
                <AvGroup>
                  <Label id="alertsLabel" for="alerts">
                    <Translate contentKey="gatewayApp.userExtra.alerts">Alerts</Translate>
                  </Label>
                  <AvInput id="user-extra-alerts" type="select" className="form-control" name="alerts" multiple>
                    <option value="" key="0" />
                    {userExtraEntity.alerts
                      ? userExtraEntity.alerts.map(alert => (
                          <option value={alert ? JSON.stringify(alert) : ''} key={alert.id}>
                            {alert.createdDate} {alert.alertType} {alert.values}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to={isOrganization ? '/' : '/entity/user-extra'} replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  userExtraEntity: storeState.userExtra.entity,
  loading: storeState.userExtra.loading,
  updating: storeState.userExtra.updating,
  updateSuccess: storeState.userExtra.updateSuccess,
  isOrganization: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.ORGANIZATION])
});

const mapDispatchToProps = {
  getUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserExtraUpdate);
