import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table } from 'reactstrap';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { Translate, getSortState, IPaginationBaseState, JhiPagination, getPaginationItemsNumber } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Tooltip from '@material-ui/core/Tooltip';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-extra.reducer';
import { withStyles } from '@material-ui/core/styles';
import { UserAlertsDetails } from './user-alerts-details';
import { AlertType } from 'app/shared/model/user-alert.model';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

export interface IUserExtraProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IUserExtraState extends IPaginationBaseState {
  filter: string;
  showModal: boolean;
  userEntity: any;
}

export class UserExtra extends React.Component<IUserExtraProps, IUserExtraState> {
  state: IUserExtraState = {
    filter: '',
    showModal: false,
    userEntity: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getUserExtra();
  }

  setFilter = evt => {
    this.setState({
      filter: evt.target.value
    });
  };

  filterFn = l =>
    l.userFirstName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()) ||
    l.userLastName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()) ||
    l.uuid.toString().includes(this.state.filter) ||
    (l.organizationFirstName &&
      l.organizationFirstName
        .toString()
        .toUpperCase()
        .includes(this.state.filter.toUpperCase())) ||
    (l.organizationLastName &&
      l.organizationLastName
        .toString()
        .toUpperCase()
        .includes(this.state.filter.toUpperCase())) ||
    (l.familyLastName &&
      l.familyLastName
        .toString()
        .toUpperCase()
        .includes(this.state.filter.toUpperCase())) ||
    (l.familyFirstName &&
      l.familyFirstName
        .toString()
        .toUpperCase()
        .includes(this.state.filter.toUpperCase()));

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortUserExtra()
    );
  };

  sortUserExtra = () => {
    this.getUserExtra();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortUserExtra());

  getUserExtra = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  openModal = user => {
    this.setState({ showModal: true, userEntity: user });
  };

  render() {
    const RedTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ff0000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const GreenTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#008000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const OrangeTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ffc107',
        fontSize: '1em'
      }
    }))(Tooltip);

    const { userExtraList, match, isFetching, totalItems, isOrganization } = this.props;
    const { filter } = this.state;
    return (
      <div>
        {!isOrganization && (
          <Fragment>
            <h2 id="user-extra-heading" className="d-flex flex-wrap justify-content-between" style={{ marginBottom: '50px' }}>
              <Translate contentKey="gatewayApp.userExtra.home.title">Users</Translate>
              <div className="d-flex flex-wrap col-md-6 justify-content-between search-container">
                <div className="d-flex col-md-8">
                  <div style={{ margin: 'auto' }}>
                    <Translate contentKey="logs.search">Search</Translate>
                  </div>
                  <input
                    type="search"
                    value={filter}
                    onChange={this.setFilter}
                    disabled={isFetching}
                    className="form-control"
                    style={{ marginLeft: '10px', width: '100% !important' }}
                  />
                  <Button type="submit" color="primary">
                    <FontAwesomeIcon icon="search" />
                  </Button>
                </div>
                <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
                  <FontAwesomeIcon icon="plus" />
                  &nbsp;
                  <Translate contentKey="gatewayApp.userExtra.home.createLabel">Create new User Extra</Translate>
                </Link>
              </div>
            </h2>

            <div className="table-responsive">
              <Table responsive>
                <thead>
                  <tr>
                    <th>
                      <Translate contentKey="global.field.id">ID</Translate>
                    </th>
                    <th onClick={this.sort('userLastName')}>
                      <Translate contentKey="gatewayApp.userExtra.user">User</Translate>
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th onClick={this.sort('uuid')}>
                      <Translate contentKey="gatewayApp.userExtra.uuid">Uuid</Translate>
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th onClick={this.sort('organizationFirstName')}>
                      <Translate contentKey="gatewayApp.userExtra.organization">Organization</Translate>
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th onClick={this.sort('familyLastName')}>
                      <Translate contentKey="gatewayApp.userExtra.family">Family</Translate>
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th>
                      <Translate contentKey="gatewayApp.userExtra.description">Description</Translate>
                    </th>
                    <th>
                      <Translate contentKey="gatewayApp.userExtra.address">Address</Translate>
                    </th>
                    <th>
                      <Translate contentKey="gatewayApp.userExtra.phone">Phone</Translate>
                    </th>
                    <th>
                      <Translate contentKey="gatewayApp.userExtra.gender">Gender</Translate>
                    </th>
                    <th>
                      <Translate contentKey="gatewayApp.userExtra.alerts">Alerts</Translate>
                    </th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {userExtraList.filter(this.filterFn).map((userExtra, i) => (
                    <tr key={`entity-${i}`}>
                      <td>
                        <Button tag={Link} to={`${match.url}/${userExtra.id}`} color="link" size="sm">
                          {userExtra.id}
                        </Button>
                      </td>
                      <td>
                        {userExtra.userFirstName ? userExtra.userFirstName : ''} {userExtra.userLastName ? userExtra.userLastName : ''}
                      </td>
                      <td>{userExtra.uuid ? userExtra.uuid : ''}</td>
                      <td>
                        {userExtra.organizationFirstName ? userExtra.organizationFirstName : ''}{' '}
                        {userExtra.organizationLastName ? userExtra.organizationLastName : ''}
                      </td>
                      <td>
                        {userExtra.familyFirstName ? userExtra.familyFirstName : ''}{' '}
                        {userExtra.familyLastName ? userExtra.familyLastName : ''}
                      </td>
                      <td>{userExtra.description ? userExtra.description : ''}</td>
                      <td>{userExtra.address ? userExtra.address : ''}</td>
                      <td>{userExtra.phone ? userExtra.phone : ''}</td>
                      <td>{userExtra.gender ? userExtra.gender : ''}</td>
                      <td>
                        <UserAlertsDetails
                          handleClose={this.handleClose}
                          updateEntities={null}
                          userType={null}
                          showModal={this.state.showModal}
                          userEntity={this.state.userEntity}
                          history={`${this.props.location.pathname}/${userExtra.id}/alerts` as any}
                          location={this.props.location}
                          match={match.url as any}
                        />
                        {userExtra.alerts.length !== 0 &&
                          (userExtra.alerts
                            .filter(dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id)))[0]
                            .alertType.toString() === AlertType.DANGER ? (
                            <div>
                              <RedTooltip
                                title={
                                  <Translate
                                    contentKey={`gatewayApp.deviceAlert.${
                                      userExtra.alerts.filter(dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id)))[0]
                                        .label
                                    }`}
                                    interpolate={{
                                      value: userExtra.alerts.filter(
                                        dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id))
                                      )[0].values
                                    }}
                                  />
                                }
                              >
                                <Button color="link" style={{ color: 'red' }} onClick={this.openModal.bind(this, userExtra)}>
                                  <div style={{ color: 'red' }}>
                                    <Translate contentKey="gatewayApp.device.errorStatus" />
                                  </div>
                                </Button>
                              </RedTooltip>
                            </div>
                          ) : userExtra.alerts
                            .filter(dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id)))[0]
                            .alertType.toString() === AlertType.WARNING ? (
                            <div>
                              <OrangeTooltip
                                title={
                                  <Translate
                                    contentKey={`gatewayApp.deviceAlert.${
                                      userExtra.alerts.filter(dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id)))[0]
                                        .label
                                    }`}
                                    interpolate={{
                                      value: userExtra.alerts.filter(
                                        dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id))
                                      )[0].values
                                    }}
                                  />
                                }
                              >
                                <Button color="link" style={{ color: '#ffa500' }} onClick={this.openModal.bind(this, userExtra)}>
                                  <div style={{ color: 'orange' }}>
                                    <Translate contentKey="gatewayApp.device.errorStatus" />
                                  </div>
                                </Button>
                              </OrangeTooltip>
                            </div>
                          ) : (
                            <div>
                              <GreenTooltip
                                title={
                                  <Translate
                                    contentKey={`gatewayApp.deviceAlert.${
                                      userExtra.alerts.filter(dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id)))[0]
                                        .label
                                    }`}
                                    interpolate={{
                                      value: userExtra.alerts.filter(
                                        dev => dev.id === Math.max.apply(Math, userExtra.alerts.map(o => o.id))
                                      )[0].values
                                    }}
                                  />
                                }
                              >
                                <Button color="link" style={{ color: '#008000' }} onClick={this.openModal.bind(this, userExtra)}>
                                  <div style={{ color: 'green' }}>
                                    <Translate contentKey="gatewayApp.device.okStatus" />
                                  </div>
                                </Button>
                              </GreenTooltip>
                            </div>
                          ))}
                      </td>

                      <td className="text-right">
                        <div className="btn-group flex-btn-group-container">
                          <Button tag={Link} to={`${match.url}/${userExtra.id}`} color="info" size="sm">
                            <FontAwesomeIcon icon="eye" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.view">View</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${userExtra.id}/edit`} color="primary" size="sm">
                            <FontAwesomeIcon icon="pencil-alt" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.edit">Edit</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${userExtra.id}/delete`} color="danger" size="sm">
                            <FontAwesomeIcon icon="trash" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.delete">Delete</Translate>
                            </span>
                          </Button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
            <Row className="justify-content-center">
              <JhiPagination
                items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
                activePage={this.state.activePage}
                onSelect={this.handlePagination}
                maxButtons={5}
              />
            </Row>
          </Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ userExtra, authentication }: IRootState) => ({
  userExtraList: userExtra.entities,
  isFetching: userExtra.loading,
  totalItems: userExtra.totalItems,
  isOrganization: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ORGANIZATION])
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserExtra);
