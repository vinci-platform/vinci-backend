import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, TextFormat } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { APP_DATE_FORMAT } from 'app/config/constants';

export interface IUserAlertsDetailsProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
  showModal: boolean;
  handleClose: Function;
  userEntity: any;
  updateEntities: Function;
  userType: string;
}

export class UserAlertsDetails extends React.Component<IUserAlertsDetailsProps> {
  componentWillReceiveProps(nextProps: Readonly<IUserAlertsDetailsProps>, nextContext: any): void {
    if (nextProps.showModal && this.props.userType !== null) {
      const alerts = [];
      nextProps.userEntity.alerts.forEach(alert => {
        if (this.props.userType === 'patient' && !alert.userRead) {
          alert.userRead = true;
          alert.userExtraId = nextProps.userEntity.id;
          alerts.push(alert);
        } else if (this.props.userType === 'family' && !alert.familyRead) {
          alert.familyRead = true;
          alert.userExtraId = nextProps.userEntity.id;
          alerts.push(alert);
        } else if (this.props.userType === 'organization' && !alert.organizationRead) {
          alert.organizationRead = true;
          alert.userExtraId = nextProps.userEntity.id;
          alerts.push(alert);
        }
      });
      let alertsToSend = alerts.map((alert => {//quick fix so deviceId is not sent
        return {
          alertType: alert.alertType,
          createdDate: alert.createdDate,
          familyRead: alert.familyRead,
          id: alert.id,
          label: alert.label,
          organizationRead: alert.organizationRead,
          userExtraId: alert.userExtraId,
          userRead: alert.userRead,
          values: alert.values
        };
      }));
      this.props.updateEntities(alertsToSend);
    }
  }

  render() {
    const { userEntity, handleClose } = this.props;
    return (
      <Modal isOpen={this.props.showModal} toggle={handleClose} backdrop="static" id="login-page" autoFocus={false}>
        <ModalHeader id="login-title" toggle={handleClose}>
          <Translate contentKey="gatewayApp.device.alerts">Your recent alerts:</Translate>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col md="12">
              <Table responsive>
                {userEntity !== undefined &&
                  userEntity.alerts !== undefined &&
                  userEntity.alerts
                    .sort((a, b) => b.id - a.id)
                    .slice(0, 10)
                    .map(alert => (
                      <tbody>
                        <td>{alert.alertType}</td>
                        <td>{<TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />}</td>
                        <td>
                          {alert.label} {alert.values}
                        </td>
                      </tbody>
                    ))}
              </Table>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}

const mapStateToProps = ({ userExtra }: IRootState) => ({
  userEntity: userExtra.entity
});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAlertsDetails);
