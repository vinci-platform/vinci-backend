import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CameraMovementData from './camera-movement-data';
import CameraMovementDataDetail from './camera-movement-data-detail';
import CameraMovementDataUpdate from './camera-movement-data-update';
import CameraMovementDataDeleteDialog from './camera-movement-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CameraMovementDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CameraMovementDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CameraMovementDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={CameraMovementData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={CameraMovementDataDeleteDialog} />
  </>
);

export default Routes;
