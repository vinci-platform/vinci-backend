import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './camera-movement-data.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface ICameraMovementDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class CameraMovementDataDetail extends React.Component<ICameraMovementDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { cameraMovementDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioServerCameraMovementData.detail.title">CameraMovementData</Translate> [
            <b>{cameraMovementDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioServerCameraMovementData.data">Data</Translate>
              </span>
            </dt>
            <dd>{cameraMovementDataEntity.data}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioServerCameraMovementData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{cameraMovementDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(cameraMovementDataEntity.timestamp)}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerCameraMovementData.device">Device</Translate>
            </dt>
            <dd>{cameraMovementDataEntity.device ? cameraMovementDataEntity.device.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/camera-movement-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/camera-movement-data/${cameraMovementDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ cameraMovementData }: IRootState) => ({
  cameraMovementDataEntity: cameraMovementData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraMovementDataDetail);
