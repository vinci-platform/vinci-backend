import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, Label } from 'reactstrap';
import { Translate, IPaginationBaseState, getSortState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './fitbit-watch-intraday-data.reducer';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { DeviceType } from 'app/shared/model/device.model';
import { formatTime } from 'app/shared/util/date-utils';

export interface IFitbitWatchIntradayDataProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IFitbitWatchIntradayDataState extends IPaginationBaseState {
  showFilters: boolean;
  deviceIdValue: string;
}

export class FitbitWatchIntradayData extends React.Component<IFitbitWatchIntradayDataProps, IFitbitWatchIntradayDataState> {
  state: IFitbitWatchIntradayDataState = {
    showFilters: false,
    deviceIdValue: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getFitbitWatchIntradayData();
    this.props.getDevices(null, MAX_SIZE);
  }

  sortFitbitWatchIntradayData = () => {
    this.getFitbitWatchIntradayData();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortFitbitWatchIntradayData());

  getFitbitWatchIntradayData = () => {
    const { activePage, itemsPerPage, sort, order, deviceIdValue } = this.state;
    this.props.getEntities(
      activePage - 1,
      itemsPerPage,
      `${sort},${order}`,
      `${deviceIdValue !== '' ? `deviceId.equals=${deviceIdValue}` : ''}`
    );
  };

  deviceOnChange = event => {
    this.setState({
      deviceIdValue: event.target.value
    });
  };

  handleRemoveFilters = () => {
    this.setState({ deviceIdValue: '' });
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  filterFn = l => (this.state.deviceIdValue !== '' ? l.deviceId.toString() === this.state.deviceIdValue : true);

  render() {
    const { fitbitWatchIntradayDataList, match, devices, totalItems } = this.props;
    const { showFilters, deviceIdValue } = this.state;

    return (
      <div>
        <h2 id="fitbit-watch-intraday-data-heading">
          <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.home.title">Fitbit Watch Intraday Data</Translate>
          <Link
            to={`${match.url}/new`}
            className="btn btn-primary float-right jh-create-entity"
            id="jh-create-entity"
            style={{ fontSize: '0.9rem' }}
          >
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.home.createLabel">
              Create new Fitbit Watch Intraday Data
            </Translate>
          </Link>
        </h2>
        <div className="py-4 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label for="deviceUUID">
                    <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
                  </Label>
                  <AvInput
                    id="watch-data-device"
                    type="select"
                    className="form-control"
                    name="deviceId"
                    value={deviceIdValue}
                    onChange={this.deviceOnChange}
                  >
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive className="table-fixed">
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.data">Data</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.timestamp">Timestamp</Translate>
                </th>
                <th>
                  <Translate contentKey="sessions.table.date">Date</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.deviceId">Device Id</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {fitbitWatchIntradayDataList.filter(this.filterFn).map((fitbitWatchIntradayData, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${fitbitWatchIntradayData.id}`} color="link" size="sm">
                      {fitbitWatchIntradayData.id}
                    </Button>
                  </td>
                  <td className="truncate-text">{fitbitWatchIntradayData.data}</td>
                  <td>{fitbitWatchIntradayData.timestamp}</td>
                  <td>{formatTime(fitbitWatchIntradayData.timestamp)}</td>
                  <td>
                        {fitbitWatchIntradayData.deviceId ? (
                          <Link to={`device/${fitbitWatchIntradayData.deviceId}`}>
                            {fitbitWatchIntradayData.deviceId}
                          </Link>
                        ) : (
                          ''
                        )}
                      </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${fitbitWatchIntradayData.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${fitbitWatchIntradayData.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${fitbitWatchIntradayData.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ fitbitWatchIntradayData, device }: IRootState) => ({
  fitbitWatchIntradayDataList: fitbitWatchIntradayData.entities,
  devices: device.entities.filter(item => item.deviceType === DeviceType.FITBIT_WATCH),
  totalItems: fitbitWatchIntradayData.totalItems
});

const mapDispatchToProps = {
  getEntities,
  getDevices
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FitbitWatchIntradayData);
