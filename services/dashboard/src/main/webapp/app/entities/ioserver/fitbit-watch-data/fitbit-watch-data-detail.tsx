import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './fitbit-watch-data.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface IFitbitWatchDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class FitbitWatchDataDetail extends React.Component<IFitbitWatchDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { fitbitWatchDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioserverFitbitWatchData.detail.title">FitbitWatchData</Translate> [
            <b>{fitbitWatchDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sleepData">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchData.sleepData">Sleep Data</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchDataEntity.sleepData}</dd>
            <dt>
              <span id="activityData">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchData.activityData">Activity Data</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchDataEntity.activityData}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(fitbitWatchDataEntity.timestamp)}</dd>
            <dt>
              <span id="deviceId">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchData.deviceId">Device Id</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchDataEntity.deviceId}</dd>
          </dl>
          <Button tag={Link} to="/entity/fitbit-watch-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/fitbit-watch-data/${fitbitWatchDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ fitbitWatchData }: IRootState) => ({
  fitbitWatchDataEntity: fitbitWatchData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FitbitWatchDataDetail);
