import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FitbitWatchIntradayData from './fitbit-watch-intraday-data';
import FitbitWatchIntradayDataDetail from './fitbit-watch-intraday-data-detail';
import FitbitWatchIntradayDataUpdate from './fitbit-watch-intraday-data-update';
import FitbitWatchIntradayDataDeleteDialog from './fitbit-watch-intraday-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FitbitWatchIntradayDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FitbitWatchIntradayDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FitbitWatchIntradayDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={FitbitWatchIntradayData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FitbitWatchIntradayDataDeleteDialog} />
  </>
);

export default Routes;
