import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './camera-fitness-data.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface ICameraFitnessDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class CameraFitnessDataDetail extends React.Component<ICameraFitnessDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { cameraFitnessDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioServerCameraFitnessData.detail.title">CameraFitnessData</Translate> [
            <b>{cameraFitnessDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioServerCameraFitnessData.data">Data</Translate>
              </span>
            </dt>
            <dd>{cameraFitnessDataEntity.data}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioServerCameraFitnessData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{cameraFitnessDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(cameraFitnessDataEntity.timestamp)}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerCameraFitnessData.device">Device</Translate>
            </dt>
            <dd>{cameraFitnessDataEntity.device ? cameraFitnessDataEntity.device.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/camera-fitness-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/camera-fitness-data/${cameraFitnessDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ cameraFitnessData }: IRootState) => ({
  cameraFitnessDataEntity: cameraFitnessData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraFitnessDataDetail);
