import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table, Label } from 'reactstrap';
import { Translate, IPaginationBaseState, getSortState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './watch-data.reducer';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { DeviceType } from 'app/shared/model/device.model';
import { formatTime } from 'app/shared/util/date-utils';

export interface IWatchDataProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IWatchDataState extends IPaginationBaseState {
  showFilters: boolean;
  deviceValue: string;
}

export class WatchData extends React.Component<IWatchDataProps, IWatchDataState> {
  state: IWatchDataState = {
    showFilters: false,
    deviceValue: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getWatchData();
    this.props.getDevices(null, MAX_SIZE);
  }

  sortWatchData = () => {
    this.getWatchData();
    this.props.history.push(
      `${this.props.location.pathname}?deviceId.equals=${this.state.deviceValue}&page=${this.state.activePage}&sort=${this.state.sort},${
        this.state.order
      }`
    );
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortWatchData());

  getWatchData = () => {
    const { activePage, itemsPerPage, sort, order, deviceValue } = this.state;
    this.props.getEntities(
      activePage - 1,
      itemsPerPage,
      `${sort},${order}`,
      `${deviceValue !== '' ? `deviceId.equals=${deviceValue}` : ''}`
    );
  };

  deviceOnChange = event => {
    this.setState(
      {
        deviceValue: event.target.value
      },
      () => this.getWatchData()
    );
  };

  handleRemoveFilters = () => {
    this.setState({ deviceValue: '' }, () => this.getWatchData());
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  render() {
    const { watchDataList, match, devices, totalItems } = this.props;
    const { showFilters, deviceValue } = this.state;

    return (
      <div>
        <h2 id="watch-data-heading">
          <Translate contentKey="gatewayApp.ioServerWatchData.home.title">Watch Data</Translate>
          <Link
            to={`${match.url}/new`}
            className="btn btn-primary float-right jh-create-entity"
            id="jh-create-entity"
            style={{ fontSize: '0.9rem' }}
          >
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.ioServerWatchData.home.createLabel">Create new Watch Data</Translate>
          </Link>
        </h2>
        <div className="py-4 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label for="deviceUUID">
                    <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
                  </Label>
                  <AvInput
                    id="watch-data-device"
                    type="select"
                    className="form-control"
                    name="deviceId"
                    value={deviceValue}
                    onChange={this.deviceOnChange}
                  >
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName} 
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive className="table-fixed">
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerWatchData.data">Data</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerWatchData.timestamp">Timestamp</Translate>
                </th>
                <th>
                  <Translate contentKey="sessions.table.date">Date</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {watchDataList.map((watchData, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${watchData.id}`} color="link" size="sm">
                      {watchData.id}
                    </Button>
                  </td>
                  <td className="truncate-text">{watchData.data}</td>
                  <td>{watchData.timestamp}</td>
                  <td>{formatTime(watchData.timestamp)}</td>
                  <td>{watchData.deviceId ? <Link to={`device/${watchData.deviceId}`}>{watchData.deviceId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${watchData.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${watchData.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${watchData.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ watchData, device }: IRootState) => ({
  watchDataList: watchData.entities,
  devices: device.entities.filter(item => item.deviceType === DeviceType.WATCH),
  totalItems: watchData.totalItems
});

const mapDispatchToProps = {
  getEntities,
  getDevices
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WatchData);
