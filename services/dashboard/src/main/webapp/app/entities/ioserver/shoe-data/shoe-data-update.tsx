import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDevice } from 'app/shared/model/device.model';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './shoe-data.reducer';
import { IShoeData } from 'app/shared/model/ioserver/shoe-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IShoeDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IShoeDataUpdateState {
  isNew: boolean;
  deviceId: string;
}

export class ShoeDataUpdate extends React.Component<IShoeDataUpdateProps, IShoeDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      deviceId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getDevices(null, 1000, null, "deviceType.equals=SHOE");
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { shoeDataEntity } = this.props;
      const entity = {
        ...shoeDataEntity,
        ...values
      };
      entity.data = JSON.stringify(entity.data);
      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/shoe-data');
  };

  render() {
    const { shoeDataEntity, devices, loading, updating } = this.props;
    const { isNew } = this.state;

    const { data } = shoeDataEntity;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.ioServerShoeData.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.ioServerShoeData.home.createOrEditLabel">Create or edit a ShoeData</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : shoeDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="shoe-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="gatewayApp.ioServerShoeData.step-counter">Step counter</Translate>
                  </Label>
                  <AvInput id="shoe-data-data" type="input" name="data.step_counter" required errorMessage="Step counter is required!"/>
                </AvGroup>
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="gatewayApp.ioServerShoeData.step-activity">Step activity</Translate>
                  </Label>
                  <AvInput id="shoe-data-data" type="input" name="data.step_activity" required errorMessage="Step activity is required!"/>
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.ioServerShoeData.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField id="shoe-data-timestamp" type="string" className="form-control" name="timestamp" required errorMessage="You must enter some timestamp!"/>
                </AvGroup>
                <AvGroup>
                  <Label for="deviceUUID">
                    <Translate contentKey="gatewayApp.ioServerShoeData.device">Device</Translate>
                  </Label>
                  <AvInput id="shoe-data-device" type="select" className="form-control" name="deviceId" required errorMessage="You must select a device!">
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName}
                          </option>
                        ))
                      : ''}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/shoe-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  shoeDataEntity: storeState.shoeData.entity,
  loading: storeState.shoeData.loading,
  updating: storeState.shoeData.updating,
  updateSuccess: storeState.shoeData.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeDataUpdate);
