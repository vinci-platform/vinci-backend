import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IShoeData, defaultValue } from 'app/shared/model/ioserver/shoe-data.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_SHOEDATA_LIST: 'shoeData/FETCH_SHOEDATA_LIST',
  FETCH_SHOEDATA: 'shoeData/FETCH_SHOEDATA',
  CREATE_SHOEDATA: 'shoeData/CREATE_SHOEDATA',
  UPDATE_SHOEDATA: 'shoeData/UPDATE_SHOEDATA',
  DELETE_SHOEDATA: 'shoeData/DELETE_SHOEDATA',
  SET_BLOB: 'shoeData/SET_BLOB',
  RESET: 'shoeData/RESET'
};

interface IShoeDataEntities {
  id?: number;
  data?: string;
  timestamp?: number;
  deviceId?: number;
}

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IShoeDataEntities>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type ShoeDataState = Readonly<typeof initialState>;

// Reducer

export default (state: ShoeDataState = initialState, action): ShoeDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SHOEDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SHOEDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SHOEDATA):
    case REQUEST(ACTION_TYPES.UPDATE_SHOEDATA):
    case REQUEST(ACTION_TYPES.DELETE_SHOEDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SHOEDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SHOEDATA):
    case FAILURE(ACTION_TYPES.CREATE_SHOEDATA):
    case FAILURE(ACTION_TYPES.UPDATE_SHOEDATA):
    case FAILURE(ACTION_TYPES.DELETE_SHOEDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOEDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOEDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SHOEDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_SHOEDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SHOEDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/shoe-data';
const apiUrlNew = 'ioserver/api/shoe-data';

// Actions

export const getEntitiesFor: ICrudGetAllAction<IShoeData> = (deviceId, fromDate, toDate) => ({
  type: ACTION_TYPES.FETCH_SHOEDATA_LIST,
  payload: axios.get<IShoeData>(
    `${apiUrlNew}?cacheBuster=${new Date().getTime()}&deviceId.equals=${deviceId}&timestamp.greaterThan=${fromDate}`
  )
});

export const getEntities: ICrudGetAll<IShoeData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  console.log('get entites', axios.defaults.baseURL, requestUrl);
  return {
    type: ACTION_TYPES.FETCH_SHOEDATA_LIST,
    payload: axios.get<IShoeData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IShoeData> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SHOEDATA,
    payload: axios.get<IShoeData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IShoeData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SHOEDATA,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IShoeData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SHOEDATA,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IShoeData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SHOEDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
