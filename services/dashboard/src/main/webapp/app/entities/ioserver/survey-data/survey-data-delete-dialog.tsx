import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getEntity, deleteEntity } from './survey-data.reducer';

export interface ISurveyDataDeleteDialogProps extends DispatchProps, RouteComponentProps<{ id: string }> {}

export class SurveyDataDeleteDialog extends React.Component<ISurveyDataDeleteDialogProps> {
  confirmDelete = (event, id) => {
    this.props.deleteEntity(id);
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { id } = this.props.match.params;
    return (
      <Modal isOpen toggle={this.handleClose}>
        <ModalHeader toggle={this.handleClose}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody id="gatewayApp.ioServerSurveyData.delete.question">
          <Translate contentKey="gatewayApp.ioServerSurveyData.delete.question" interpolate={{ id }}>
            Are you sure you want to delete this SurveyData?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button id="jhi-confirm-delete-surveyData" color="danger" onClick={this.confirmDelete.bind(this, event, id)}>
            <FontAwesomeIcon icon="trash" />
            &nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapDispatchToProps = { getEntity, deleteEntity };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  () => {},
  mapDispatchToProps
)(SurveyDataDeleteDialog);
