import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction, IPayload } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IWatchData, defaultValue } from 'app/shared/model/ioserver/watch-data.model';
import { ICrudGetAll } from 'app/entities/types';

export declare type IGetAlertsForParameters<T> = (
  deviceId: string,
  fromDate: Date,
  toDate: Date
) => IPayload<T> | ((dispatch: any) => IPayload<T>);

export const ACTION_TYPES = {
  FETCH_WATCHDATA_FOR_PARAMETERS: 'watchData/FETCH_WATCHDATA_FOR_PARAMETERS',
  FETCH_WATCHDATA_LIST: 'watchData/FETCH_WATCHDATA_LIST',
  FETCH_WATCHDATA: 'watchData/FETCH_WATCHDATA',
  CREATE_WATCHDATA: 'watchData/CREATE_WATCHDATA',
  UPDATE_WATCHDATA: 'watchData/UPDATE_WATCHDATA',
  DELETE_WATCHDATA: 'watchData/DELETE_WATCHDATA',
  RESET: 'watchData/RESET'
};

interface IWatchDataEntities {
  id?: number;
  data?: string;
  timestamp?: number;
  deviceId?: number;
}

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IWatchDataEntities>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  locationAlerts: [],
  totalItems: 0
};

export type WatchDataState = Readonly<typeof initialState>;

// Reducer

export default (state: WatchDataState = initialState, action): WatchDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_WATCHDATA_FOR_PARAMETERS):
    case REQUEST(ACTION_TYPES.FETCH_WATCHDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_WATCHDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_WATCHDATA):
    case REQUEST(ACTION_TYPES.UPDATE_WATCHDATA):
    case REQUEST(ACTION_TYPES.DELETE_WATCHDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_WATCHDATA_FOR_PARAMETERS):
    case FAILURE(ACTION_TYPES.FETCH_WATCHDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_WATCHDATA):
    case FAILURE(ACTION_TYPES.CREATE_WATCHDATA):
    case FAILURE(ACTION_TYPES.UPDATE_WATCHDATA):
    case FAILURE(ACTION_TYPES.DELETE_WATCHDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_WATCHDATA_FOR_PARAMETERS): {
      return {
        ...state,
        loading: false,
        locationAlerts: action.payload.data['H02']
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_WATCHDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_WATCHDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_WATCHDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_WATCHDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_WATCHDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/watch-data';
const apiUrlNew = 'ioserver/api/watch-data';

// Actions

export const getEntitiesFor: IGetAlertsForParameters<IWatchData> = (deviceId, fromDate, toDate) => ({
  type: ACTION_TYPES.FETCH_WATCHDATA_FOR_PARAMETERS,
  payload: axios.post<IWatchData>(`${apiUrlNew}/orderedByType`, {
    deviceId,
    begin: fromDate,
    end: toDate
  })
});

export const getEntities: ICrudGetAll<IWatchData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_WATCHDATA_LIST,
    payload: axios.get<IWatchData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IWatchData> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_WATCHDATA,
    payload: axios.get<IWatchData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IWatchData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_WATCHDATA,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IWatchData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_WATCHDATA,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IWatchData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_WATCHDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
