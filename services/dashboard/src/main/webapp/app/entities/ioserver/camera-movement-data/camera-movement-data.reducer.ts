import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ICameraMovementData, defaultValue } from 'app/shared/model/ioserver/camera-movement-data.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_CAMERAMOVEMENTDATA_LIST: 'cameraMovementData/FETCH_CAMERAMOVEMENTDATA_LIST',
  FETCH_CAMERAMOVEMENTDATA: 'cameraMovementData/FETCH_CAMERAMOVEMENTDATA',
  FETCH_CAMERAMOVEMENTDATA_LAST_IMAGE: 'cameraMovementData/FETCH_CAMERAMOVEMENTDATA_LAST_IMAGE',
  CREATE_CAMERAMOVEMENTDATA: 'cameraMovementData/CREATE_CAMERAMOVEMENTDATA',
  UPDATE_CAMERAMOVEMENTDATA: 'cameraMovementData/UPDATE_CAMERAMOVEMENTDATA',
  DELETE_CAMERAMOVEMENTDATA: 'cameraMovementData/DELETE_CAMERAMOVEMENTDATA',
  SET_BLOB: 'cameraMovementData/SET_BLOB',
  RESET: 'cameraMovementData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICameraMovementData>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type CameraMovementDataState = Readonly<typeof initialState>;

// Reducer

export default (state: CameraMovementDataState = initialState, action): CameraMovementDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA):
    case REQUEST(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LAST_IMAGE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CAMERAMOVEMENTDATA):
    case REQUEST(ACTION_TYPES.UPDATE_CAMERAMOVEMENTDATA):
    case REQUEST(ACTION_TYPES.DELETE_CAMERAMOVEMENTDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA):
    case FAILURE(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LAST_IMAGE):
    case FAILURE(ACTION_TYPES.CREATE_CAMERAMOVEMENTDATA):
    case FAILURE(ACTION_TYPES.UPDATE_CAMERAMOVEMENTDATA):
    case FAILURE(ACTION_TYPES.DELETE_CAMERAMOVEMENTDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CAMERAMOVEMENTDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_CAMERAMOVEMENTDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CAMERAMOVEMENTDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/camera-movement-data';
const cameraLastImageUrl = 'cameramovement/images/lastimage';

// Actions

export const getEntities: ICrudGetAll<ICameraMovementData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrl}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LIST,
    payload: axios.get<ICameraMovementData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICameraMovementData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA,
    payload: axios.get<ICameraMovementData>(requestUrl)
  };
};

export const getLastEntity = () => {
  const requestUrl = `${cameraLastImageUrl}`;
  return {
    type: ACTION_TYPES.FETCH_CAMERAMOVEMENTDATA_LAST_IMAGE,
    payload: axios.get<void>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICameraMovementData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CAMERAMOVEMENTDATA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICameraMovementData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CAMERAMOVEMENTDATA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICameraMovementData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CAMERAMOVEMENTDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
