import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import WatchData from './watch-data';
import WatchDataDetail from './watch-data-detail';
import WatchDataUpdate from './watch-data-update';
import WatchDataDeleteDialog from './watch-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={WatchDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={WatchDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={WatchDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={WatchData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={WatchDataDeleteDialog} />
  </>
);

export default Routes;
