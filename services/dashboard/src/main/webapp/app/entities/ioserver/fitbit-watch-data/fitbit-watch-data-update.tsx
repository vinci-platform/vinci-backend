import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './fitbit-watch-data.reducer';
import { IFitbitWatchData } from 'app/shared/model/ioserver/fitbit-watch-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFitbitWatchDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IFitbitWatchDataUpdateState {
  isNew: boolean;
}

export class FitbitWatchDataUpdate extends React.Component<IFitbitWatchDataUpdateProps, IFitbitWatchDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
    this.props.getDevices(null, 1000, null, "deviceType.equals=FITBIT_WATCH");
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { fitbitWatchDataEntity } = this.props;
      const entity = {
        ...fitbitWatchDataEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/fitbit-watch-data');
  };

  render() {
    const { fitbitWatchDataEntity, devices, loading, updating } = this.props;
    const { isNew } = this.state;

    const { sleepData, activityData } = fitbitWatchDataEntity;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.ioserverFitbitWatchData.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.ioserverFitbitWatchData.home.createOrEditLabel">Create or edit a FitbitWatchData</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : fitbitWatchDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="fitbit-watch-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="sleepDataLabel" for="sleepData">
                    <Translate contentKey="gatewayApp.ioserverFitbitWatchData.sleepData">Sleep Data</Translate>
                  </Label>
                  <AvInput 
                    id="fitbit-watch-data-sleepData" 
                    type="textarea" 
                    name="sleepData" 
                    required
                    errorMessage="Sleep Data is required!"/>
                </AvGroup>
                <AvGroup>
                  <Label id="activityDataLabel" for="activityData">
                    <Translate contentKey="gatewayApp.ioserverFitbitWatchData.activityData">Activity Data</Translate>
                  </Label>
                  <AvInput 
                    id="fitbit-watch-data-activityData" 
                    type="textarea" 
                    name="activityData"
                    required
                    errorMessage="Activity Data is required!" />
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.ioserverFitbitWatchData.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField 
                    id="fitbit-watch-data-timestamp"
                    type="string" 
                    className="form-control" 
                    name="timestamp"
                    required
                    errorMessage="Timestamp is required!" />
                </AvGroup>
                <AvGroup>
                  <Label for="deviceId">
                    <Translate contentKey="gatewayApp.ioserverFitbitWatchData.device">Device</Translate>
                  </Label>
                  <AvInput id="device-alert-device" type="select" className="form-control" name="deviceId" required>
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName}
                          </option>
                        ))
                      : ''}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/fitbit-watch-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  fitbitWatchDataEntity: storeState.fitbitWatchData.entity,
  loading: storeState.fitbitWatchData.loading,
  updating: storeState.fitbitWatchData.updating,
  updateSuccess: storeState.fitbitWatchData.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FitbitWatchDataUpdate);
