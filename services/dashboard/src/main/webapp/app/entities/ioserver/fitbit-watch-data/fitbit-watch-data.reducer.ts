import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IFitbitWatchData, defaultValue } from 'app/shared/model/ioserver/fitbit-watch-data.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_FITBITWATCHDATA_LIST: 'fitbitWatchData/FETCH_FITBITWATCHDATA_LIST',
  FETCH_FITBITWATCHDATA: 'fitbitWatchData/FETCH_FITBITWATCHDATA',
  CREATE_FITBITWATCHDATA: 'fitbitWatchData/CREATE_FITBITWATCHDATA',
  UPDATE_FITBITWATCHDATA: 'fitbitWatchData/UPDATE_FITBITWATCHDATA',
  DELETE_FITBITWATCHDATA: 'fitbitWatchData/DELETE_FITBITWATCHDATA',
  SET_BLOB: 'fitbitWatchData/SET_BLOB',
  RESET: 'fitbitWatchData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFitbitWatchData>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type FitbitWatchDataState = Readonly<typeof initialState>;

// Reducer

export default (state: FitbitWatchDataState = initialState, action): FitbitWatchDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FITBITWATCHDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FITBITWATCHDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FITBITWATCHDATA):
    case REQUEST(ACTION_TYPES.UPDATE_FITBITWATCHDATA):
    case REQUEST(ACTION_TYPES.DELETE_FITBITWATCHDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FITBITWATCHDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FITBITWATCHDATA):
    case FAILURE(ACTION_TYPES.CREATE_FITBITWATCHDATA):
    case FAILURE(ACTION_TYPES.UPDATE_FITBITWATCHDATA):
    case FAILURE(ACTION_TYPES.DELETE_FITBITWATCHDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FITBITWATCHDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_FITBITWATCHDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FITBITWATCHDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_FITBITWATCHDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FITBITWATCHDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/fitbit-watch-data';
const apiUrlNew = 'ioserver/api/fitbit-watch-data';

// Actions

export const getEntitiesFor: ICrudGetAllAction<IFitbitWatchData> = (deviceId, fromDate, toDate) => ({
  type: ACTION_TYPES.FETCH_FITBITWATCHDATA_LIST,
  payload: axios.get<IFitbitWatchData>(
    `${apiUrlNew}/payload?cacheBuster=${new Date().getTime()}&deviceId.equals=${deviceId}&timestamp.greaterThan=${fromDate}`
  )
});

export const getEntities: ICrudGetAll<IFitbitWatchData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FITBITWATCHDATA_LIST,
    payload: axios.get<IFitbitWatchData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IFitbitWatchData> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FITBITWATCHDATA,
    payload: axios.get<IFitbitWatchData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IFitbitWatchData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FITBITWATCHDATA,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  console.log("createEntity");
  // dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFitbitWatchData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FITBITWATCHDATA,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  console.log("updateEntity");
  // dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFitbitWatchData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FITBITWATCHDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
