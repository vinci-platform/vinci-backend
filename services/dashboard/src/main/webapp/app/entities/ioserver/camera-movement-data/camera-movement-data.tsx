import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table, Label } from 'reactstrap';
import { Translate, IPaginationBaseState, getSortState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './camera-movement-data.reducer';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface ICameraMovementDataProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface ICameraMovementDataState extends IPaginationBaseState {
  showFilters: boolean;
  deviceIdValue: string;
}

export class CameraMovementData extends React.Component<ICameraMovementDataProps, ICameraMovementDataState> {
  state: ICameraMovementDataState = {
    showFilters: false,
    deviceIdValue: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getCameraMovementData();
    this.props.getDevices(null, MAX_SIZE);
  }

  sortCameraMovementData = () => {
    this.getCameraMovementData();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortCameraMovementData());

  getCameraMovementData = () => {
    const { activePage, itemsPerPage, sort, order, deviceIdValue } = this.state;
    this.props.getEntities(
      activePage - 1,
      itemsPerPage,
      `${sort},${order}`,
      `${deviceIdValue !== '' ? `deviceId.equals=${deviceIdValue}` : ''}`
    );
  };

  deviceOnChange = event => {
    this.setState(
      {
        deviceIdValue: event.target.value
      },
      () => this.getCameraMovementData()
    );
  };

  handleRemoveFilters = () => {
    this.setState({ deviceIdValue: '' }, () => this.getCameraMovementData());
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  render() {
    const { cameraMovementDataList, match, devices, totalItems } = this.props;
    const { showFilters, deviceIdValue } = this.state;

    return (
      <div>
        <h2 id="camera-movement-data-heading">
          <Translate contentKey="gatewayApp.ioServerCameraMovementData.home.title">Camera Movement Data</Translate>
          <Link
            to={`${match.url}/new`}
            className="btn btn-primary float-right jh-create-entity"
            id="jh-create-entity"
            style={{ fontSize: '0.9rem' }}
          >
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.ioServerCameraMovementData.home.createLabel">Create new Camera Movement Data</Translate>
          </Link>
        </h2>
        <div className="py-4 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label for="deviceUUID">
                    <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
                  </Label>
                  <AvInput
                    id="watch-data-device"
                    type="select"
                    className="form-control"
                    name="deviceId"
                    value={deviceIdValue}
                    onChange={this.deviceOnChange}
                  >
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerCameraMovementData.data">Data</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerCameraMovementData.timestamp">Timestamp</Translate>
                </th>
                <th>
                  <Translate contentKey="sessions.table.date">Date</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerCameraMovementData.device">Device</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {cameraMovementDataList.map((cameraMovementData, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${cameraMovementData.id}`} color="link" size="sm">
                      {cameraMovementData.id}
                    </Button>
                  </td>
                  <td>{cameraMovementData.data}</td>
                  <td>{cameraMovementData.timestamp}</td>
                  <td>{formatTime(cameraMovementData.timestamp)}</td>
                  <td>
                    {cameraMovementData.device ? (
                      <Link to={`device/${cameraMovementData.device.id}`}>{cameraMovementData.device.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${cameraMovementData.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${cameraMovementData.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${cameraMovementData.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ cameraMovementData, device }: IRootState) => ({
  cameraMovementDataList: cameraMovementData.entities,
  devices: device.entities,
  totalItems: cameraMovementData.totalItems
});

const mapDispatchToProps = {
  getEntities,
  getDevices
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraMovementData);
