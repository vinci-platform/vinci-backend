import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ShoeData from './shoe-data';
import ShoeDataDetail from './shoe-data-detail';
import ShoeDataUpdate from './shoe-data-update';
import ShoeDataDeleteDialog from './shoe-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ShoeDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ShoeDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ShoeDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={ShoeData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ShoeDataDeleteDialog} />
  </>
);

export default Routes;
