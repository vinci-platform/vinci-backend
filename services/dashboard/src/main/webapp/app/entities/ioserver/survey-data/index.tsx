import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SurveyData from './survey-data';
import SurveyDataDetail from './survey-data-detail';
import SurveyDataUpdate from './survey-data-update';
import SurveyDataDeleteDialog from './survey-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SurveyDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SurveyDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SurveyDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={SurveyData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SurveyDataDeleteDialog} />
  </>
);

export default Routes;
