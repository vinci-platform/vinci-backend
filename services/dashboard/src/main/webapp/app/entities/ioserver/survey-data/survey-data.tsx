import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table, Label, Input } from 'reactstrap';
import { Translate, translate, IPaginationBaseState, getSortState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './survey-data.reducer';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ITEMS_PER_PAGE, MAX_SIZE } from 'app/shared/util/pagination.constants';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { DeviceType } from 'app/shared/model/device.model';
import { SurveyType } from 'app/entities/types';

export interface ISurveyDataProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface ISurveyDataState extends IPaginationBaseState {
  showFilters: boolean;
  surveyTypeValue: SurveyType;
  deviceIdValue: string;
  dateFrom: string;
  dateTo: string;
}

export class SurveyData extends React.Component<ISurveyDataProps, ISurveyDataState> {
  state: ISurveyDataState = {
    showFilters: false,
    surveyTypeValue: null,
    deviceIdValue: '',
    dateFrom: '',
    dateTo: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getSurveyData();
    this.props.getDevices(null, MAX_SIZE);
  }

  sortSurveyData = () => {
    this.getSurveyData();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortSurveyData());

  getSurveyData = () => {
    const { activePage, itemsPerPage, sort, order, surveyTypeValue, deviceIdValue, dateFrom, dateTo } = this.state;

    let filter =
      `${surveyTypeValue !== null ? `surveyType.equals=${surveyTypeValue}` : ''}` +
      `${surveyTypeValue ? '&' : ''}` +
      `${deviceIdValue !== '' ? `deviceId.equals=${deviceIdValue}` : ''}` +
      `${deviceIdValue ? '&' : ''}` +
      `${dateFrom !== '' ? `createdTime.greaterOrEqualThan=${new Date(dateFrom).toISOString()}` : ''}` +
      `${dateFrom ? '&' : ''}` +
      `${dateTo !== '' ? `createdTime.lessOrEqualThan=${new Date(dateTo).toISOString()}` : ''}`;

    if (filter.endsWith('&')) filter = filter.slice(0, -1);

    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`, filter);
  };

  surveyTypeOnChange = event => {
    this.setState(
      {
        surveyTypeValue: event.target.value
      },
      () => this.getSurveyData()
    );
  };

  deviceOnChange = event => {
    this.setState(
      {
        deviceIdValue: event.target.value
      },
      () => this.getSurveyData()
    );
  };

  dateFromOnChange = event => {
    this.setState(
      {
        dateFrom: event.target.value
      },
      () => this.getSurveyData()
    );
  };

  dateToOnChange = event => {
    this.setState(
      {
        dateTo: event.target.value
      },
      () => this.getSurveyData()
    );
  };

  handleRemoveFilters = () => {
    this.setState(
      {
        surveyTypeValue: null,
        deviceIdValue: '',
        dateFrom: '',
        dateTo: ''
      },
      () => this.getSurveyData()
    );
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  render() {
    const { surveyDataList, match, devices, totalItems } = this.props;
    const { showFilters, surveyTypeValue, deviceIdValue, dateFrom, dateTo } = this.state;
    return (
      <div>
        <h2 id="survey-data-heading">
          <Translate contentKey="gatewayApp.ioServerSurveyData.home.title">Survey Data</Translate>
          <Link
            to={`${match.url}/new`}
            className="btn btn-primary float-right jh-create-entity"
            id="jh-create-entity"
            style={{ fontSize: '0.9rem' }}
          >
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.ioServerSurveyData.home.createLabel">Create new Survey Data</Translate>
          </Link>
        </h2>
        <div className="py-4 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label id="deviceTypeLabel">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.surveyType">Survey Type</Translate>
                  </Label>
                  <AvInput
                    id="device-alert-alertType"
                    type="select"
                    className="form-control"
                    name="alertType"
                    value={surveyTypeValue}
                    onChange={this.surveyTypeOnChange}
                  >
                    <option value="" />
                    <option value="WHOQOL_BREF">{translate('gatewayApp.SurveyType.WHOQOLBREF')}</option>
                    <option value="IPAQ">{translate('gatewayApp.SurveyType.IPAQ')}</option>
                    <option value="D_VAMS">{translate('gatewayApp.SurveyType.DVAMS')}</option>
                    <option value="ELDERLY">{translate('gatewayApp.SurveyType.ELDERLY')}</option>
                  </AvInput>
                </AvGroup>
                <AvGroup className="pr-2">
                  <Label for="deviceUUID">
                    <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
                  </Label>
                  <AvInput
                    id="watch-data-device"
                    type="select"
                    className="form-control"
                    name="deviceId"
                    value={deviceIdValue}
                    onChange={this.deviceOnChange}
                  >
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <div className="pr-2">
                  <label>Date from</label>
                  <Input type="date" value={dateFrom} onChange={this.dateFromOnChange} />
                </div>
                <div className="pr-2">
                  <label>Date to</label>
                  <Input type="date" value={dateTo} onChange={this.dateToOnChange} />
                </div>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive className="table-fixed">
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                {/* <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.identifier">Identifier</Translate>
                </th> */}
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.surveyType">Survey Type</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.scoringResult">Scoring Result</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.createdDate">Created Date</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.endDate">End Date</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.deviceId">Device ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.assesmentData">Assesment Data</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.ioServerSurveyData.additionalInfo">Additional Info</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {surveyDataList.map((surveyData, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${surveyData.id}`} color="link" size="sm">
                      {surveyData.id}
                    </Button>
                  </td>
                  {/* <td>{surveyData.identifier}</td> */}
                  <td>{surveyData.surveyType}</td>
                  <td>{surveyData.scoringResult}</td>
                  <td className="truncate-text">{surveyData.createdTime}</td>
                  <td className="truncate-text">{surveyData.endTime}</td>
                  <td>{surveyData.deviceId ? <Link to={`device/${surveyData.deviceId}`}>{surveyData.deviceId}</Link> : ''}</td>
                  <td className="truncate-text">{surveyData.assesmentData}</td>
                  <td>{surveyData.additionalInfo}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${surveyData.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${surveyData.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${surveyData.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ surveyData, device }: IRootState) => ({
  surveyDataList: surveyData.entities,
  devices: device.entities.filter(item => item.deviceType === DeviceType.SURVEY),
  totalItems: surveyData.totalItems
});

const mapDispatchToProps = {
  getEntities,
  getDevices
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyData);
