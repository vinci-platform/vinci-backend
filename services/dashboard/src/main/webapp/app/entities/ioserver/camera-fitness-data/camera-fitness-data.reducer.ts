import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ICameraFitnessData, defaultValue } from 'app/shared/model/ioserver/camera-fitness-data.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_CAMERAFITNESSDATA_LIST: 'cameraFitnessData/FETCH_CAMERAFITNESSDATA_LIST',
  FETCH_CAMERAFITNESSDATA: 'cameraFitnessData/FETCH_CAMERAFITNESSDATA',
  CREATE_CAMERAFITNESSDATA: 'cameraFitnessData/CREATE_CAMERAFITNESSDATA',
  UPDATE_CAMERAFITNESSDATA: 'cameraFitnessData/UPDATE_CAMERAFITNESSDATA',
  DELETE_CAMERAFITNESSDATA: 'cameraFitnessData/DELETE_CAMERAFITNESSDATA',
  SET_BLOB: 'cameraFitnessData/SET_BLOB',
  RESET: 'cameraFitnessData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICameraFitnessData>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type CameraFitnessDataState = Readonly<typeof initialState>;

// Reducer

export default (state: CameraFitnessDataState = initialState, action): CameraFitnessDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CAMERAFITNESSDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CAMERAFITNESSDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CAMERAFITNESSDATA):
    case REQUEST(ACTION_TYPES.UPDATE_CAMERAFITNESSDATA):
    case REQUEST(ACTION_TYPES.DELETE_CAMERAFITNESSDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CAMERAFITNESSDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CAMERAFITNESSDATA):
    case FAILURE(ACTION_TYPES.CREATE_CAMERAFITNESSDATA):
    case FAILURE(ACTION_TYPES.UPDATE_CAMERAFITNESSDATA):
    case FAILURE(ACTION_TYPES.DELETE_CAMERAFITNESSDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CAMERAFITNESSDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_CAMERAFITNESSDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CAMERAFITNESSDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_CAMERAFITNESSDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CAMERAFITNESSDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/camera-fitness-data';

// Actions

export const getEntities: ICrudGetAll<ICameraFitnessData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrl}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CAMERAFITNESSDATA_LIST,
    payload: axios.get<ICameraFitnessData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICameraFitnessData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CAMERAFITNESSDATA,
    payload: axios.get<ICameraFitnessData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICameraFitnessData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CAMERAFITNESSDATA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICameraFitnessData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CAMERAFITNESSDATA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICameraFitnessData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CAMERAFITNESSDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
