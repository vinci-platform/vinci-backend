import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';

import { IUserExtra } from 'app/shared/model/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './survey-data.reducer';
import { ISurveyData } from 'app/shared/model/ioserver/survey-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { SurveyType } from 'app/entities/types';

export interface ISurveyDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISurveyDataUpdateState {
  isNew: boolean;
  userExtraId: string;
}

export class SurveyDataUpdate extends React.Component<ISurveyDataUpdateProps, ISurveyDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userExtraId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUserExtras();
    this.props.getDevices(null, 1000, null, "deviceType.equals=SURVEY");
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { surveyDataEntity } = this.props;
      const entity = {
        ...surveyDataEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/survey-data');
  };

  render() {
    const { surveyDataEntity, userExtras, loading, updating } = this.props;
    const { isNew } = this.state;
    const userExtra = userExtras.find(user => user.id === surveyDataEntity.userExtraId);

    // const { data } = surveyDataEntity;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.ioServerSurveyData.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.ioServerSurveyData.home.createOrEditLabel">Create or edit a SurveyData</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : surveyDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="survey-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                {/* <AvGroup>
                  <Label for="identifier">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.identifier">Identifier</Translate>
                  </Label>
                  <AvInput id="survey-data-identifier" type="text" className="form-control" name="identifier" />
                </AvGroup> */}
                <AvGroup>
                  <Label for="surveyType">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.surveyType">Survey Type</Translate>
                  </Label>
                  <AvInput id="survey-data-survey-type" type="select" className="form-control" name="surveyType">
                  {/* <option value="Pick a device type" key="0" /> */}
                        { Object.values(SurveyType).map(surveyType => (
                              <option value={surveyType} key={surveyType} selected>
                                {surveyType}
                              </option>
                            ))}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="scoringResult">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.scoringResult">Scoring Result</Translate>
                  </Label>
                  <AvInput 
                    id="survey-data-scoring-result" 
                    type="number" 
                    className="form-control" 
                    name="scoringResult"
                    required
                    errorMessage="Scoring Result is required!" />
                </AvGroup>
                <AvGroup>
                  <Label for="createdDate">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput 
                    id="survey-data-created-date" 
                    type="text" 
                    className="form-control" 
                    name="createdTime" 
                    required
                    errorMessage="Created Date is required!"/>
                </AvGroup>
                <AvGroup>
                  <Label for="endDate">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.endDate">End Date</Translate>
                  </Label>
                  <AvInput 
                    id="survey-data-end-date" 
                    type="text" 
                    className="form-control" 
                    name="endTime" 
                    required
                    errorMessage="End date is required!"/>
                </AvGroup>
                <AvGroup>
                  <Label for="deviceId">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.device">Device</Translate>
                  </Label>
                  <AvInput id="device-alert-device" type="select" className="form-control" name="deviceId" required>
                    <option value="" key="0" />
                    {this.props.devices
                      ? this.props.devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name} | {otherEntity.userExtraFirstName} {otherEntity.userExtraLastName}
                          </option>
                        ))
                      : ''}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="assesmentDataLabel" for="assesmentData">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.assesmentData">Assessment Data</Translate>
                  </Label>
                  <AvInput 
                    id="survey-data-assesment-data" 
                    type="textarea" 
                    name="assesmentData" />
                </AvGroup>
                <AvGroup>
                  <Label id="additionalInfoLabel" for="additionalInfo">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.additionalInfo">Additional Info</Translate>
                  </Label>
                  <AvInput id="survey-data-additional-info" type="textarea" name="additionalInfo" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/survey-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  userExtras: storeState.userExtra.entities,
  surveyDataEntity: storeState.surveyData.entity,
  loading: storeState.surveyData.loading,
  updating: storeState.surveyData.updating,
  updateSuccess: storeState.surveyData.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getUserExtras,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyDataUpdate);
