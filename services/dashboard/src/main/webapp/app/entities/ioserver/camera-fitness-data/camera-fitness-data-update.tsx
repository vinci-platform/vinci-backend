import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDevice } from 'app/shared/model/device.model';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './camera-fitness-data.reducer';
import { ICameraFitnessData } from 'app/shared/model/ioserver/camera-fitness-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICameraFitnessDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ICameraFitnessDataUpdateState {
  isNew: boolean;
  deviceId: string;
}

export class CameraFitnessDataUpdate extends React.Component<ICameraFitnessDataUpdateProps, ICameraFitnessDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      deviceId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getDevices();
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { cameraFitnessDataEntity } = this.props;
      const entity = {
        ...cameraFitnessDataEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/camera-fitness-data');
  };

  render() {
    const { cameraFitnessDataEntity, devices, loading, updating } = this.props;
    const { isNew } = this.state;

    const { data } = cameraFitnessDataEntity;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.ioServerCameraFitnessData.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.ioServerCameraFitnessData.home.createOrEditLabel">
                Create or edit a CameraFitnessData
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : cameraFitnessDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="camera-fitness-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="gatewayApp.ioServerCameraFitnessData.data">Data</Translate>
                  </Label>
                  <AvInput id="camera-fitness-data-data" type="textarea" name="data" />
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.ioServerCameraFitnessData.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField id="camera-fitness-data-timestamp" type="string" className="form-control" name="timestamp" />
                </AvGroup>
                <AvGroup>
                  <Label for="device.id">
                    <Translate contentKey="gatewayApp.ioServerCameraFitnessData.device">Device</Translate>
                  </Label>
                  <AvInput id="camera-fitness-data-device" type="select" className="form-control" name="device.id">
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/camera-fitness-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  cameraFitnessDataEntity: storeState.cameraFitnessData.entity,
  loading: storeState.cameraFitnessData.loading,
  updating: storeState.cameraFitnessData.updating,
  updateSuccess: storeState.cameraFitnessData.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraFitnessDataUpdate);
