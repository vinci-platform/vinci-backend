import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CameraFitnessData from './camera-fitness-data';
import CameraFitnessDataDetail from './camera-fitness-data-detail';
import CameraFitnessDataUpdate from './camera-fitness-data-update';
import CameraFitnessDataDeleteDialog from './camera-fitness-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CameraFitnessDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CameraFitnessDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CameraFitnessDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={CameraFitnessData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={CameraFitnessDataDeleteDialog} />
  </>
);

export default Routes;
