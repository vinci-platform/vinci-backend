import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IFitbitWatchIntradayData, defaultValue } from 'app/shared/model/ioserver/fitbit-watch-intraday-data.model';
import { ICrudGetAll, ICrudGetAllFor } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_FITBITWATCHINTRADAYDATA_LIST: 'fitbitWatchIntradayData/FETCH_FITBITWATCHINTRADAYDATA_LIST',
  FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY: 'fitbitWatchIntradayData/FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY',
  FETCH_FITBITWATCHINTRADAYDATA: 'fitbitWatchIntradayData/FETCH_FITBITWATCHINTRADAYDATA',
  CREATE_FITBITWATCHINTRADAYDATA: 'fitbitWatchIntradayData/CREATE_FITBITWATCHINTRADAYDATA',
  UPDATE_FITBITWATCHINTRADAYDATA: 'fitbitWatchIntradayData/UPDATE_FITBITWATCHINTRADAYDATA',
  DELETE_FITBITWATCHINTRADAYDATA: 'fitbitWatchIntradayData/DELETE_FITBITWATCHINTRADAYDATA',
  SET_BLOB: 'fitbitWatchIntradayData/SET_BLOB',
  RESET: 'fitbitWatchIntradayData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFitbitWatchIntradayData>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type FitbitWatchIntradayDataState = Readonly<typeof initialState>;

// Reducer

export default (state: FitbitWatchIntradayDataState = initialState, action): FitbitWatchIntradayDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY):
    case REQUEST(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FITBITWATCHINTRADAYDATA):
    case REQUEST(ACTION_TYPES.UPDATE_FITBITWATCHINTRADAYDATA):
    case REQUEST(ACTION_TYPES.DELETE_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY):
    case FAILURE(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA):
    case FAILURE(ACTION_TYPES.CREATE_FITBITWATCHINTRADAYDATA):
    case FAILURE(ACTION_TYPES.UPDATE_FITBITWATCHINTRADAYDATA):
    case FAILURE(ACTION_TYPES.DELETE_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST):
    case SUCCESS(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FITBITWATCHINTRADAYDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FITBITWATCHINTRADAYDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/fitbit-watch-intraday-data';
const apiUrlNew = 'ioserver/api/fitbit-watch-intraday-data';

// Actions

export const getEntitiesFor: ICrudGetAllAction<IFitbitWatchIntradayData> = (deviceId, fromDate, toDate) => ({
  type: ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST,
  payload: axios.get<IFitbitWatchIntradayData>(
    `${apiUrlNew}/payload?cacheBuster=${new Date().getTime()}&deviceId.equals=${deviceId}&timestamp.equals=${fromDate}`
  )
});

export const getEntitiesForDeviceAndDay: ICrudGetAllFor<IFitbitWatchIntradayData> = (deviceId, timestampFrom, timestampTo, sort) => {
  const requestUrl = `${apiUrlNew}?deviceId.equals=${deviceId}&timestamp.greaterOrEqualThan=${timestampFrom}&timestamp.lessOrEqualThan=${timestampTo}&${
    sort ? `&sort=${sort}` : ''
  }`;
  return {
    type: ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST_FOR_DEVICE_AND_DAY,
    payload: axios.get<IFitbitWatchIntradayData>(requestUrl)
  };
};

export const getEntities: ICrudGetAll<IFitbitWatchIntradayData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA_LIST,
    payload: axios.get<IFitbitWatchIntradayData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IFitbitWatchIntradayData> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FITBITWATCHINTRADAYDATA,
    payload: axios.get<IFitbitWatchIntradayData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IFitbitWatchIntradayData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FITBITWATCHINTRADAYDATA,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFitbitWatchIntradayData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FITBITWATCHINTRADAYDATA,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFitbitWatchIntradayData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FITBITWATCHINTRADAYDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
