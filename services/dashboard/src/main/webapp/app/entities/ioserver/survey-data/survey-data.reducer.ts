import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ISurveyData, defaultValue } from 'app/shared/model/ioserver/survey-data.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_SURVEYDATA_LIST: 'surveyData/FETCH_SURVEYDATA_LIST',
  FETCH_SURVEYDATA: 'surveyData/FETCH_SURVEYDATA',
  CREATE_SURVEYDATA: 'surveyData/CREATE_SURVEYDATA',
  UPDATE_SURVEYDATA: 'surveyData/UPDATE_SURVEYDATA',
  DELETE_SURVEYDATA: 'surveyData/DELETE_SURVEYDATA',
  SET_BLOB: 'surveyData/SET_BLOB',
  RESET: 'surveyData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISurveyData>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type SurveyDataState = Readonly<typeof initialState>;

// Reducer

export default (state: SurveyDataState = initialState, action): SurveyDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SURVEYDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SURVEYDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SURVEYDATA):
    case REQUEST(ACTION_TYPES.UPDATE_SURVEYDATA):
    case REQUEST(ACTION_TYPES.DELETE_SURVEYDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SURVEYDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SURVEYDATA):
    case FAILURE(ACTION_TYPES.CREATE_SURVEYDATA):
    case FAILURE(ACTION_TYPES.UPDATE_SURVEYDATA):
    case FAILURE(ACTION_TYPES.DELETE_SURVEYDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SURVEYDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_SURVEYDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SURVEYDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_SURVEYDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SURVEYDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'ioserver/api/survey-data';
const apiUrlNew = 'ioserver/api/survey-data';

// Actions

export const getEntitiesFor: ICrudGetAllAction<ISurveyData> = (surveyId, fromDate, toDate) => ({
  type: ACTION_TYPES.FETCH_SURVEYDATA_LIST,
  payload: axios.get<ISurveyData>(`${apiUrlNew}?deviceId.equals=${surveyId}&surveyType.in=WHOQOL_BREF,IPAQ&size=100`)
});

export const getEntities: ICrudGetAll<ISurveyData> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SURVEYDATA_LIST,
    payload: axios.get<ISurveyData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ISurveyData> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SURVEYDATA,
    payload: axios.get<ISurveyData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISurveyData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SURVEYDATA,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISurveyData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SURVEYDATA,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISurveyData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SURVEYDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
