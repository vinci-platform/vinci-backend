import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './fitbit-watch-intraday-data.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface IFitbitWatchIntradayDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class FitbitWatchIntradayDataDetail extends React.Component<IFitbitWatchIntradayDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { fitbitWatchIntradayDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.detail.title">FitbitWatchIntradayData</Translate> [
            <b>{fitbitWatchIntradayDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.data">Data</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchIntradayDataEntity.data}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchIntradayDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(fitbitWatchIntradayDataEntity.timestamp)}</dd>
            <dt>
              <span id="deviceId">
                <Translate contentKey="gatewayApp.ioserverFitbitWatchIntradayData.deviceId">Device Id</Translate>
              </span>
            </dt>
            <dd>{fitbitWatchIntradayDataEntity.deviceId}</dd>
          </dl>
          <Button tag={Link} to="/entity/fitbit-watch-intraday-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/fitbit-watch-intraday-data/${fitbitWatchIntradayDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ fitbitWatchIntradayData }: IRootState) => ({
  fitbitWatchIntradayDataEntity: fitbitWatchIntradayData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FitbitWatchIntradayDataDetail);
