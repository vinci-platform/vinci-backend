import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './shoe-data.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface IShoeDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ShoeDataDetail extends React.Component<IShoeDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { shoeDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioServerShoeData.detail.title">ShoeData</Translate> [<b>{shoeDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioServerShoeData.data">Data</Translate>
              </span>
            </dt>
            <dd>{JSON.stringify(shoeDataEntity.data)}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioServerShoeData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{shoeDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(shoeDataEntity.timestamp)}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerShoeData.device">Device</Translate>
            </dt>
            <dd>{shoeDataEntity.device ? shoeDataEntity.device.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/shoe-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/shoe-data/${shoeDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ shoeData }: IRootState) => ({
  shoeDataEntity: shoeData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeDataDetail);
