import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './watch-data.reducer';
import { APP_DATE_FORMAT } from 'app/config/constants';
import { formatTime } from 'app/shared/util/date-utils';

export interface IWatchDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class WatchDataDetail extends React.Component<IWatchDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { watchDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioServerWatchData.detail.title">WatchData</Translate> [<b>{watchDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioServerWatchData.data">Data</Translate>
              </span>
            </dt>
            <dd>{watchDataEntity.data}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.ioServerWatchData.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{watchDataEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(watchDataEntity.timestamp)}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerWatchData.device">Device</Translate>
            </dt>
            <dd>{watchDataEntity.deviceId ? <Link to={`device/${watchDataEntity.deviceId}`}>{watchDataEntity.deviceId}</Link> : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/watch-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/watch-data/${watchDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ watchData }: IRootState) => ({
  watchDataEntity: watchData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WatchDataDetail);
