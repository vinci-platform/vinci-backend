import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntity } from './survey-data.reducer';

export interface ISurveyDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SurveyDataDetail extends React.Component<ISurveyDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { surveyDataEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.ioServerSurveyData.detail.title">SurveyData</Translate> [<b>{surveyDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.ioServerSurveyData.data">Data</Translate>
              </span>
            </dt>
            <dt>
              <span id="surveyType">
                <Translate contentKey="gatewayApp.ioServerSurveyData.surveyType">Survey Type</Translate>
              </span>
            </dt>
            <dd>{surveyDataEntity.surveyType}</dd>
            <dt>
              <span id="scoringResult">
                <Translate contentKey="gatewayApp.ioServerSurveyData.scoringResult">Scoring Result</Translate>
              </span>
            </dt>
            <dd>{surveyDataEntity.scoringResult}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="gatewayApp.ioServerSurveyData.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>{surveyDataEntity.createdTime}</dd>
            <dt>
              <span id="endDate">
                <Translate contentKey="gatewayApp.ioServerSurveyData.endDate">End Date</Translate>
              </span>
            </dt>
            <dd>{surveyDataEntity.endTime}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerSurveyData.userExtra">User Extra</Translate>
            </dt>
            <dd>{surveyDataEntity.userExtraId ? surveyDataEntity.userExtraId : ''}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerSurveyData.deviceId">Device ID</Translate>
            </dt>
            <dd>{surveyDataEntity.deviceId ? surveyDataEntity.deviceId : ''}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerSurveyData.assesmentData">Assesment Data</Translate>
            </dt>
            <dd>{surveyDataEntity.assesmentData}</dd>
            <dt>
              <Translate contentKey="gatewayApp.ioServerSurveyData.additionalInfo">Additional Info</Translate>
            </dt>
            <dd>{surveyDataEntity.additionalInfo}</dd>
          </dl>
          <Button tag={Link} to="/entity/survey-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/survey-data/${surveyDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ surveyData }: IRootState) => ({
  surveyDataEntity: surveyData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyDataDetail);
