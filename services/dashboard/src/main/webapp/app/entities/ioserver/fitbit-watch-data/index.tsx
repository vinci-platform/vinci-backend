import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FitbitWatchData from './fitbit-watch-data';
import FitbitWatchDataDetail from './fitbit-watch-data-detail';
import FitbitWatchDataUpdate from './fitbit-watch-data-update';
import FitbitWatchDataDeleteDialog from './fitbit-watch-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FitbitWatchDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FitbitWatchDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FitbitWatchDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={FitbitWatchData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FitbitWatchDataDeleteDialog} />
  </>
);

export default Routes;
