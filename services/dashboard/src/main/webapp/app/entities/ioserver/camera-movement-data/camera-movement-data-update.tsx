import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDevice } from 'app/shared/model/device.model';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './camera-movement-data.reducer';
import { ICameraMovementData } from 'app/shared/model/ioserver/camera-movement-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICameraMovementDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ICameraMovementDataUpdateState {
  isNew: boolean;
  deviceId: string;
}

export class CameraMovementDataUpdate extends React.Component<ICameraMovementDataUpdateProps, ICameraMovementDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      deviceId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getDevices();
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { cameraMovementDataEntity } = this.props;
      const entity = {
        ...cameraMovementDataEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/camera-movement-data');
  };

  render() {
    const { cameraMovementDataEntity, devices, loading, updating } = this.props;
    const { isNew } = this.state;

    const { data } = cameraMovementDataEntity;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.ioServerCameraMovementData.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.ioServerCameraMovementData.home.createOrEditLabel">
                Create or edit a CameraMovementData
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : cameraMovementDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="camera-movement-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="gatewayApp.ioServerCameraMovementData.data">Data</Translate>
                  </Label>
                  <AvInput id="camera-movement-data-data" type="textarea" name="data" />
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.ioServerCameraMovementData.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField id="camera-movement-data-timestamp" type="string" className="form-control" name="timestamp" />
                </AvGroup>
                <AvGroup>
                  <Label for="device.id">
                    <Translate contentKey="gatewayApp.ioServerCameraMovementData.device">Device</Translate>
                  </Label>
                  <AvInput id="camera-movement-data-device" type="select" className="form-control" name="device.id">
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/camera-movement-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  cameraMovementDataEntity: storeState.cameraMovementData.entity,
  loading: storeState.cameraMovementData.loading,
  updating: storeState.cameraMovementData.updating,
  updateSuccess: storeState.cameraMovementData.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraMovementDataUpdate);
