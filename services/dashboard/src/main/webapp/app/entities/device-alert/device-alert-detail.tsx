import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './device-alert.reducer';
import { IDeviceAlert } from 'app/shared/model/device-alert.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDeviceAlertDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class DeviceAlertDetail extends React.Component<IDeviceAlertDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { deviceAlertEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.deviceAlert.detail.title">DeviceAlert</Translate> [<b>{deviceAlertEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="label">
                <Translate contentKey="gatewayApp.deviceAlert.label">Label</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.label}</dd>
            <dt>
              <span id="values">
                <Translate contentKey="gatewayApp.deviceAlert.values">Values</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.values}</dd>
            <dt>
              <span id="alertType">
                <Translate contentKey="gatewayApp.deviceAlert.alertType">Alert Type</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.alertType}</dd>
            <dt>
              <span id="userRead">
                <Translate contentKey="gatewayApp.deviceAlert.userRead">User Read</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.userRead ? 'true' : 'false'}</dd>
            <dt>
              <span id="familyRead">
                <Translate contentKey="gatewayApp.deviceAlert.familyRead">Family Read</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.familyRead ? 'true' : 'false'}</dd>
            <dt>
              <span id="organizationRead">
                <Translate contentKey="gatewayApp.deviceAlert.organizationRead">Organization Read</Translate>
              </span>
            </dt>
            <dd>{deviceAlertEntity.organizationRead ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="gatewayApp.deviceAlert.device">Device</Translate>
            </dt>
            <dd>{deviceAlertEntity.deviceId ? deviceAlertEntity.deviceId : ''}</dd>
            <dt>
              <Translate contentKey="userManagement.createdDate">Created Date</Translate>
            </dt>
            <dd>
              <TextFormat value={deviceAlertEntity.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/device-alert" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/device-alert/${deviceAlertEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ deviceAlert }: IRootState) => ({
  deviceAlertEntity: deviceAlert.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceAlertDetail);
