import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, Label, Input } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  JhiPagination,
  getPaginationItemsNumber,
  translate
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './device-alert.reducer';
import { IDeviceAlert } from 'app/shared/model/device-alert.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';

export interface IDeviceAlertProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IDeviceAlertState extends IPaginationBaseState {
  filter: string;
  showFilters: boolean;
  alertTypeValue: string;
  dateFrom: string;
  dateTo: string;
}

export class DeviceAlert extends React.Component<IDeviceAlertProps, IDeviceAlertState> {
  state: IDeviceAlertState = {
    filter: '',
    showFilters: false,
    alertTypeValue: '',
    dateFrom: '',
    dateTo: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getDeviceAlerts();
  }

  setFilter = evt => {
    this.setState({
      filter: evt.target.value
    });
  };

  filterFn = l =>
    l.label
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()) ||
    l.values
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase());

  alertTypeOnChange = event => {
    this.setState(
      {
        alertTypeValue: event.target.value
      },
      () => this.getDeviceAlerts()
    );
  };

  dateFromOnChange = event => {
    this.setState(
      {
        dateFrom: event.target.value
      },
      () => this.getDeviceAlerts()
    );
  };

  dateToOnChange = event => {
    this.setState(
      {
        dateTo: event.target.value
      },
      () => this.getDeviceAlerts()
    );
  };

  handleRemoveFilters = () => {
    this.setState({ alertTypeValue: '', dateFrom: '', dateTo: '' }, () => this.getDeviceAlerts());
  };

  handleShowFilters = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortDeviceAlerts()
    );
  };

  sortDeviceAlerts = () => {
    this.getDeviceAlerts();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortDeviceAlerts());

  getDeviceAlerts = () => {
    const { activePage, itemsPerPage, sort, order, alertTypeValue, dateFrom, dateTo } = this.state;

    let filter =
      `${alertTypeValue !== '' ? `alertType.equals=${alertTypeValue}` : ''}` +
      `${alertTypeValue ? '&' : ''}` +
      `${dateFrom !== '' ? `createdDate.greaterOrEqualThan=${new Date(dateFrom).toISOString()}` : ''}` +
      `${dateFrom ? '&' : ''}` +
      `${dateTo !== '' ? `createdDate.lessOrEqualThan=${new Date(dateTo).toISOString()}` : ''}`;

    if (filter.endsWith('&')) filter = filter.slice(0, -1);

    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`, filter);
  };

  render() {
    const { deviceAlertList, match, isFetching, totalItems } = this.props;
    const { filter, showFilters, alertTypeValue, dateFrom, dateTo } = this.state;
    return (
      <div>
        <h2 id="device-alert-heading" className="d-flex flex-wrap justify-content-between" style={{ marginBottom: '50px' }}>
          <Translate contentKey="gatewayApp.deviceAlert.home.title">Device Alerts</Translate>
          <div className="d-flex flex-wrap col-md-6 justify-content-between search-container">
            <div className="d-flex col-md-8">
              <div style={{ margin: 'auto' }}>
                <Translate contentKey="logs.search">Search</Translate>
              </div>
              <input
                type="search"
                value={filter}
                onChange={this.setFilter}
                disabled={isFetching}
                className="form-control"
                style={{ marginLeft: '10px', width: '100% !important' }}
              />
              <Button type="submit" color="primary">
                <FontAwesomeIcon icon="search" />
              </Button>
            </div>
            <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
              <FontAwesomeIcon icon="plus" />
              &nbsp;
              <Translate contentKey="gatewayApp.deviceAlert.home.createLabel">Create new Device Alert</Translate>
            </Link>
          </div>
        </h2>
        <div className="pb-3 filters-container">
          <Button color="primary" onClick={this.handleShowFilters}>
            {showFilters ? 'Hide filters' : 'Show filters'}
          </Button>
          {showFilters && (
            <AvForm>
              <div style={{ display: 'flex', width: '70%' }} className="flex-row pt-3">
                <AvGroup className="pr-2">
                  <Label id="deviceTypeLabel">
                    <Translate contentKey="gatewayApp.deviceAlert.alertType">Alert Type</Translate>
                  </Label>
                  <AvInput
                    id="device-alert-alertType"
                    type="select"
                    className="form-control"
                    name="alertType"
                    value={alertTypeValue}
                    onChange={this.alertTypeOnChange}
                  >
                    <option value="" />
                    <option value="SUCCESS">{translate('gatewayApp.AlertType.SUCCESS')}</option>
                    <option value="INFO">{translate('gatewayApp.AlertType.INFO')}</option>
                    <option value="WARNING">{translate('gatewayApp.AlertType.WARNING')}</option>
                    <option value="DANGER">{translate('gatewayApp.AlertType.DANGER')}</option>
                  </AvInput>
                </AvGroup>
                <div className="pr-2">
                  <label>Date from</label>
                  <Input type="date" value={dateFrom} onChange={this.dateFromOnChange} />
                </div>
                <div className="pr-2">
                  <label>Date to</label>
                  <Input type="date" value={dateTo} onChange={this.dateToOnChange} />
                </div>
                <div className="align-self-center pt-3">
                  <Button color="primary" onClick={this.handleRemoveFilters}>
                    Remove filters
                  </Button>
                </div>
              </div>
            </AvForm>
          )}
        </div>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th onClick={this.sort('label')}>
                  <Translate contentKey="gatewayApp.deviceAlert.label">Label</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.deviceAlert.values">Values</Translate>
                </th>
                <th onClick={this.sort('alertType')}>
                  <Translate contentKey="gatewayApp.deviceAlert.alertType">Alert Type</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.deviceAlert.userRead">User Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.deviceAlert.familyRead">Family Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.deviceAlert.organizationRead">Organization Read</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.deviceAlert.device">Device</Translate>
                </th>
                <th className="hand" onClick={this.sort('createdDate')}>
                  <Translate contentKey="userManagement.createdDate">Created Date</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {deviceAlertList.filter(this.filterFn).map((deviceAlert, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${deviceAlert.id}`} color="link" size="sm">
                      {deviceAlert.id}
                    </Button>
                  </td>
                  <td>{deviceAlert.label}</td>
                  <td>{deviceAlert.values}</td>
                  <td>
                    <Translate contentKey={`gatewayApp.AlertType.${deviceAlert.alertType}`} />
                  </td>
                  <td>{deviceAlert.userRead ? 'true' : 'false'}</td>
                  <td>{deviceAlert.familyRead ? 'true' : 'false'}</td>
                  <td>{deviceAlert.organizationRead ? 'true' : 'false'}</td>
                  <td>{deviceAlert.deviceId ? <Link to={`device/${deviceAlert.deviceId}`}>{deviceAlert.deviceId}</Link> : ''}</td>
                  <td>
                    <TextFormat value={deviceAlert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${deviceAlert.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${deviceAlert.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${deviceAlert.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ deviceAlert }: IRootState) => ({
  deviceAlertList: deviceAlert.entities,
  isFetching: deviceAlert.loading,
  totalItems: deviceAlert.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceAlert);
