import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDeviceAlert, defaultValue } from 'app/shared/model/device-alert.model';
import { ICrudGetAll } from 'app/entities/types';

export const ACTION_TYPES = {
  FETCH_DEVICEALERT_LIST: 'deviceAlert/FETCH_DEVICEALERT_LIST',
  FETCH_DEVICEALERT: 'deviceAlert/FETCH_DEVICEALERT',
  CREATE_DEVICEALERT: 'deviceAlert/CREATE_DEVICEALERT',
  UPDATE_DEVICEALERT: 'deviceAlert/UPDATE_DEVICEALERT',
  DELETE_DEVICEALERT: 'deviceAlert/DELETE_DEVICEALERT',
  RESET: 'deviceAlert/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDeviceAlert>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type DeviceAlertState = Readonly<typeof initialState>;

// Reducer

export default (state: DeviceAlertState = initialState, action): DeviceAlertState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DEVICEALERT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DEVICEALERT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_DEVICEALERT):
    case REQUEST(ACTION_TYPES.UPDATE_DEVICEALERT):
    case REQUEST(ACTION_TYPES.DELETE_DEVICEALERT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_DEVICEALERT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DEVICEALERT):
    case FAILURE(ACTION_TYPES.CREATE_DEVICEALERT):
    case FAILURE(ACTION_TYPES.UPDATE_DEVICEALERT):
    case FAILURE(ACTION_TYPES.DELETE_DEVICEALERT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_DEVICEALERT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    case SUCCESS(ACTION_TYPES.FETCH_DEVICEALERT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_DEVICEALERT):
    case SUCCESS(ACTION_TYPES.UPDATE_DEVICEALERT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_DEVICEALERT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/device-alerts';
const apiUrlNew = 'api/device-alerts';

// Actions

export const getEntities: ICrudGetAll<IDeviceAlert> = (page, size, sort, filter) => {
  const requestUrl = `${apiUrlNew}${filter ? `?${filter}&` : '?'}${sort ? `page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DEVICEALERT_LIST,
    payload: axios.get<IDeviceAlert>(requestUrl)
  };
};

export const getEntity: ICrudGetAction<IDeviceAlert> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DEVICEALERT,
    payload: axios.get<IDeviceAlert>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IDeviceAlert> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DEVICEALERT,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDeviceAlert> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DEVICEALERT,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDeviceAlert> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DEVICEALERT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
