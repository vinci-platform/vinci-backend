import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DeviceAlert from './device-alert';
import DeviceAlertDetail from './device-alert-detail';
import DeviceAlertUpdate from './device-alert-update';
import DeviceAlertDeleteDialog from './device-alert-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DeviceAlertUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DeviceAlertUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DeviceAlertDetail} />
      <ErrorBoundaryRoute path={match.url} component={DeviceAlert} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={DeviceAlertDeleteDialog} />
  </>
);

export default Routes;
