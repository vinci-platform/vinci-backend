import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDevice } from 'app/shared/model/device.model';
import { getEntities as getDevices } from 'app/entities/device/device.reducer';
import { getEntity, updateEntity, createEntity, reset } from './device-alert.reducer';
import { IDeviceAlert } from 'app/shared/model/device-alert.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDeviceAlertUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IDeviceAlertUpdateState {
  isNew: boolean;
  deviceId: string;
}

export class DeviceAlertUpdate extends React.Component<IDeviceAlertUpdateProps, IDeviceAlertUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      deviceId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getDevices();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { deviceAlertEntity } = this.props;
      const entity = {
        ...deviceAlertEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/device-alert');
  };

  render() {
    const { deviceAlertEntity, devices, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.deviceAlert.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.deviceAlert.home.createOrEditLabel">Create or edit a DeviceAlert</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : deviceAlertEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="device-alert-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : (
                  ''
                )}
                <AvGroup>
                  <Label id="labelLabel" for="label">
                    <Translate contentKey="gatewayApp.deviceAlert.label">Label</Translate>
                  </Label>
                  <AvField
                    id="device-alert-label"
                    type="text"
                    name="label"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="valuesLabel" for="values">
                    <Translate contentKey="gatewayApp.deviceAlert.values">Values</Translate>
                  </Label>
                  <AvField
                    id="device-alert-values"
                    type="text"
                    name="values"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }} />
                </AvGroup>
                <AvGroup>
                  <Label id="alertTypeLabel">
                    <Translate contentKey="gatewayApp.deviceAlert.alertType">Alert Type</Translate>
                  </Label>
                  <AvInput
                    id="device-alert-alertType"
                    type="select"
                    className="form-control"
                    name="alertType"
                    value={(!isNew && deviceAlertEntity.alertType) || 'SUCCESS'}
                  >
                    <option value="SUCCESS">{translate('gatewayApp.AlertType.SUCCESS')}</option>
                    <option value="INFO">{translate('gatewayApp.AlertType.INFO')}</option>
                    <option value="WARNING">{translate('gatewayApp.AlertType.WARNING')}</option>
                    <option value="DANGER">{translate('gatewayApp.AlertType.DANGER')}</option>
                  </AvInput>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="userReadLabel" check>
                    <AvInput id="device-alert-userRead" type="checkbox" className="form-control" name="userRead" />
                    <Translate contentKey="gatewayApp.deviceAlert.userRead">User Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="familyReadLabel" check>
                    <AvInput id="device-alert-familyRead" type="checkbox" className="form-control" name="familyRead" />
                    <Translate contentKey="gatewayApp.deviceAlert.familyRead">Family Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup className="col-md-6 formStyle">
                  <Label id="organizationReadLabel" check>
                    <AvInput id="device-alert-organizationRead" type="checkbox" className="form-control" name="organizationRead" />
                    <Translate contentKey="gatewayApp.deviceAlert.organizationRead">Organization Read</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="device.id">
                    <Translate contentKey="gatewayApp.deviceAlert.device">Device</Translate>
                  </Label>
                  <AvInput id="device-alert-device" type="select" className="form-control" name="deviceId">
                    <option value="" key="0" />
                    {devices
                      ? devices.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id} | { otherEntity.name} | { otherEntity.userExtraFirstName} { otherEntity.userExtraLastName}
                          </option>
                        ))
                      : ''}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="createdDate">
                    <Translate contentKey="gatewayApp.ioServerSurveyData.createdDate">Created Date</Translate>
                  </Label>
                  <AvField id="device-created-date" type="text" name="createdDate" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/device-alert" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  devices: storeState.device.entities,
  deviceAlertEntity: storeState.deviceAlert.entity,
  loading: storeState.deviceAlert.loading,
  updating: storeState.deviceAlert.updating,
  updateSuccess: storeState.deviceAlert.updateSuccess
});

const mapDispatchToProps = {
  getDevices,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceAlertUpdate);
