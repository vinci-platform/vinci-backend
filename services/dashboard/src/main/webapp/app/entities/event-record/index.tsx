import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EventRecord from './event-record';
import EventRecordDetail from './event-record-detail';
import EventRecordUpdate from './event-record-update';
import EventRecordDeleteDialog from './event-record-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EventRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EventRecordUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EventRecordDetail} />
      <ErrorBoundaryRoute path={match.url} component={EventRecord} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={EventRecordDeleteDialog} />
  </>
);

export default Routes;
