import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getUserExtras } from 'app/entities/user-extra/user-extra.reducer';

import { getEntity, updateEntity, createEntity, reset } from './event-record.reducer';
import { IEventRecord } from 'app/shared/model/event-record.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IEventRecordUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEventRecordUpdateState {
  isNew: boolean;
}

export class EventRecordUpdate extends React.Component<IEventRecordUpdateProps, IEventRecordUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
    this.props.getUserExtras(null,1000,null);
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { eventRecordEntity } = this.props;
      const entity = {
        ...eventRecordEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/event-record');
  };

  render() {
    const { eventRecordEntity, loading, updating, userExtras} = this.props;
    const { isNew } = this.state;

    return (
      <div className="p-3">
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="gatewayApp.eventRecord.home.createOrEditLabel">
              <Translate contentKey="gatewayApp.eventRecord.home.createOrEditLabel">Create or edit a EventRecord</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center edit-entity-container">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : eventRecordEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="event-record-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="appIdLabel" for="appId">
                    <Translate contentKey="gatewayApp.eventRecord.appId">App Id</Translate>
                  </Label>
                  <AvField id="event-record-appId" type="string" className="form-control" name="appId" />
                </AvGroup>
                <AvGroup>
                  <Label id="userIdLabel" for="userId">
                    <Translate contentKey="gatewayApp.eventRecord.userId">User Id</Translate>
                  </Label>
                  <AvInput id="userIdInput" type="select" className="form-control " name="userId" required>
                        {userExtras
                          ? userExtras.map(otherEntity => (
                              <option value={otherEntity.userId} key={otherEntity.userId} selected>
                                {otherEntity.userFirstName} {otherEntity.userLastName} | {otherEntity.userId}
                              </option>
                            ))
                          : null}
                      </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="timestampLabel" for="timestamp">
                    <Translate contentKey="gatewayApp.eventRecord.timestamp">Timestamp</Translate>
                  </Label>
                  <AvField id="event-record-timestamp" type="string" className="form-control" name="timestamp" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdLabel" for="created">
                    <Translate contentKey="gatewayApp.eventRecord.createdDate">Created</Translate>
                  </Label>
                  <AvField id="event-record-created" type="string" className="form-control" name="created" />
                </AvGroup>
                <AvGroup>
                  <Label id="notifyLabel" for="notify">
                    <Translate contentKey="gatewayApp.eventRecord.notify">Notify</Translate>
                  </Label>
                  <AvField id="event-record-notify" type="string" className="form-control" name="notify" />
                </AvGroup>
                <AvGroup>
                  <Label id="repeatLabel" for="repeat">
                    <Translate contentKey="gatewayApp.eventRecord.repeat">Repeat</Translate>
                  </Label>
                  <AvField id="event-record-repeat" type="text" name="repeat" />
                </AvGroup>
                <AvGroup>
                  <Label id="titleLabel" for="title">
                    <Translate contentKey="gatewayApp.eventRecord.title">Title</Translate>
                  </Label>
                  <AvField id="event-record-title" type="text" name="title" />
                </AvGroup>
                <AvGroup>
                  <Label id="typeLabel" for="type">
                    <Translate contentKey="gatewayApp.eventRecord.type">Type</Translate>
                  </Label>
                  <AvField id="event-record-type" type="text" name="type" />
                </AvGroup>
                <AvGroup>
                  <Label id="textLabel" for="text">
                    <Translate contentKey="gatewayApp.eventRecord.text">Text</Translate>
                  </Label>
                  <AvField id="event-record-text" type="text" name="text" />
                </AvGroup>
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="gatewayApp.eventRecord.data">Data</Translate>
                  </Label>
                  <AvField id="event-record-data" type="text" name="data" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/event-record" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  eventRecordEntity: storeState.eventRecord.entity,
  loading: storeState.eventRecord.loading,
  updating: storeState.eventRecord.updating,
  updateSuccess: storeState.eventRecord.updateSuccess,
  userExtras: storeState.userExtra.entities
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventRecordUpdate);
