import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './event-record.reducer';
import { formatTime } from 'app/shared/util/date-utils';

export interface IEventRecordDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class EventRecordDetail extends React.Component<IEventRecordDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { eventRecordEntity } = this.props;
    return (
      <Row className="detail-entity-container">
        <Col md="8">
          <h2>
            <Translate contentKey="gatewayApp.eventRecord.detail.title">EventRecord</Translate> [<b>{eventRecordEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="appId">
                <Translate contentKey="gatewayApp.eventRecord.appId">App Id</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.appId}</dd>
            <dt>
              <span id="userId">
                <Translate contentKey="gatewayApp.eventRecord.userId">User Id</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.userId}</dd>
            <dt>
              <span id="timestamp">
                <Translate contentKey="gatewayApp.eventRecord.timestamp">Timestamp</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.timestamp}</dd>
            <dt>
              <span id="date">
                <Translate contentKey="sessions.table.date">Date</Translate>
              </span>
            </dt>
            <dd>{formatTime(eventRecordEntity.timestamp)}</dd>
            <dt>
              <span id="created">
                <Translate contentKey="gatewayApp.eventRecord.createdDate">Created</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.created}</dd>
            <dt>
              <span id="notify">
                <Translate contentKey="gatewayApp.eventRecord.notify">Notify</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.notify}</dd>
            <dt>
              <span id="repeat">
                <Translate contentKey="gatewayApp.eventRecord.repeat">Repeat</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.repeat}</dd>
            <dt>
              <span id="title">
                <Translate contentKey="gatewayApp.eventRecord.title">Title</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.title}</dd>
            <dt>
              <span id="type">
                <Translate contentKey="gatewayApp.eventRecord.type">Type</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.type}</dd>
            <dt>
              <span id="text">
                <Translate contentKey="gatewayApp.eventRecord.text">Text</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.text}</dd>
            <dt>
              <span id="data">
                <Translate contentKey="gatewayApp.eventRecord.data">Data</Translate>
              </span>
            </dt>
            <dd>{eventRecordEntity.data}</dd>
          </dl>
          <Button tag={Link} to="/entity/event-record" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/event-record/${eventRecordEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ eventRecord }: IRootState) => ({
  eventRecordEntity: eventRecord.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventRecordDetail);
