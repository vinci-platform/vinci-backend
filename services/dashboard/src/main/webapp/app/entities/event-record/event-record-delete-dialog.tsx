import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getEntity, deleteEntity } from './event-record.reducer';

export interface IEventRecordDeleteDialogProps extends DispatchProps, RouteComponentProps<{ id: string }> {}

export class EventRecordDeleteDialog extends React.Component<IEventRecordDeleteDialogProps> {
  confirmDelete = event => {
    this.props.deleteEntity(this.props.match.params.id);
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { id } = this.props.match.params;
    return (
      <Modal isOpen toggle={this.handleClose}>
        <ModalHeader toggle={this.handleClose}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody id="gatewayApp.eventRecord.delete.question">
          <Translate contentKey="gatewayApp.eventRecord.delete.question" interpolate={{ id }}>
            Are you sure you want to delete this EventRecord?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button id="jhi-confirm-delete-eventRecord" color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />
            &nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapDispatchToProps = { getEntity, deleteEntity };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  () => {},
  mapDispatchToProps
)(EventRecordDeleteDialog);
