import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEventRecord, defaultValue } from 'app/shared/model/event-record.model';

export const ACTION_TYPES = {
  FETCH_EVENTRECORD_LIST: 'eventRecord/FETCH_EVENTRECORD_LIST',
  FETCH_EVENTRECORD: 'eventRecord/FETCH_EVENTRECORD',
  CREATE_EVENTRECORD: 'eventRecord/CREATE_EVENTRECORD',
  UPDATE_EVENTRECORD: 'eventRecord/UPDATE_EVENTRECORD',
  DELETE_EVENTRECORD: 'eventRecord/DELETE_EVENTRECORD',
  RESET: 'eventRecord/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEventRecord>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type EventRecordState = Readonly<typeof initialState>;

// Reducer

export default (state: EventRecordState = initialState, action): EventRecordState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EVENTRECORD_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EVENTRECORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_EVENTRECORD):
    case REQUEST(ACTION_TYPES.UPDATE_EVENTRECORD):
    case REQUEST(ACTION_TYPES.DELETE_EVENTRECORD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_EVENTRECORD_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EVENTRECORD):
    case FAILURE(ACTION_TYPES.CREATE_EVENTRECORD):
    case FAILURE(ACTION_TYPES.UPDATE_EVENTRECORD):
    case FAILURE(ACTION_TYPES.DELETE_EVENTRECORD):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_EVENTRECORD_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_EVENTRECORD):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_EVENTRECORD):
    case SUCCESS(ACTION_TYPES.UPDATE_EVENTRECORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_EVENTRECORD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrlNew = 'api/event-records';

// Actions

export const getEntities: ICrudGetAllAction<IEventRecord> = (page, size, sort) => {
  const requestUrl = `${apiUrlNew}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_EVENTRECORD_LIST,
    payload: axios.get<IEventRecord>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IEventRecord> = id => {
  const requestUrl = `${apiUrlNew}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_EVENTRECORD,
    payload: axios.get<IEventRecord>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IEventRecord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EVENTRECORD,
    payload: axios.post(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEventRecord> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EVENTRECORD,
    payload: axios.put(apiUrlNew, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEventRecord> = id => async dispatch => {
  const requestUrl = `${apiUrlNew}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EVENTRECORD,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
