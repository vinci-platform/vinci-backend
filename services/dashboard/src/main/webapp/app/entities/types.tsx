import { IPayload } from 'react-jhipster';

export declare type ICrudGetAll<T> = (
  page?: number,
  size?: number,
  sort?: string,
  filter?: string
) => IPayload<T> | ((dispatch: any) => IPayload<T>);

export declare type ICrudGetAllFor<T> = (
  id: number,
  timestampFrom: number,
  timestampTo: number,
  sort?: string
) => IPayload<T> | ((dispatch: any) => IPayload<T>);

export enum SurveyType {
  WHOQOLBREF = 'WHOQOL_BREF',
  IPAQ = 'IPAQ',
  DVAMS = 'D_VAMS',
  ELDERLY = 'ELDERLY',
  USER_NEEDS = 'USER_NEEDS',
  DIGITAL_SKILLS = 'DIGITAL_SKILLS',
  FEELINGS = 'FEELINGS'
}
