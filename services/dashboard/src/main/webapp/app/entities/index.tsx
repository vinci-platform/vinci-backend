import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Device from './device';
import DeviceAlert from './device-alert';
import UserAlert from './user-alert';
import UserExtra from './user-extra';
import WatchData from './ioserver/watch-data';
import ShoeData from './ioserver/shoe-data';
import CameraFitnessData from './ioserver/camera-fitness-data';
import CameraMovementData from './ioserver/camera-movement-data';
import SurveyData from './ioserver/survey-data';
import FitbitWatchData from './ioserver/fitbit-watch-data';
import FitbitWatchIntradayData from './ioserver/fitbit-watch-intraday-data';
import MobileAppSettingsData from './mobile-app-settings';
import HealthRecord from './health-record';
import EventRecord from './event-record';

const Routes = ({ match }) => (
  <div className="p-3">
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/device`} component={Device} />
      <ErrorBoundaryRoute path={`${match.url}/device-alert`} component={DeviceAlert} />
      <ErrorBoundaryRoute path={`${match.url}/user-alert`} component={UserAlert} />
      <ErrorBoundaryRoute path={`${match.url}/user-extra`} component={UserExtra} />
      <ErrorBoundaryRoute path={`${match.url}/watch-data`} component={WatchData} />
      <ErrorBoundaryRoute path={`${match.url}/shoe-data`} component={ShoeData} />
      <ErrorBoundaryRoute path={`${match.url}/camera-fitness-data`} component={CameraFitnessData} />
      <ErrorBoundaryRoute path={`${match.url}/camera-movement-data`} component={CameraMovementData} />
      <ErrorBoundaryRoute path={`${match.url}/survey-data`} component={SurveyData} />
      <ErrorBoundaryRoute path={`${match.url}/fitbit-watch-data`} component={FitbitWatchData} />
      <ErrorBoundaryRoute path={`${match.url}/fitbit-watch-intraday-data`} component={FitbitWatchIntradayData} />
      <ErrorBoundaryRoute path={`${match.url}/mobile-app-settings`} component={MobileAppSettingsData} />
      <ErrorBoundaryRoute path={`${match.url}/health-record`} component={HealthRecord} />
      <ErrorBoundaryRoute path={`${match.url}/event-record`} component={EventRecord} />
    </Switch>
  </div>
);

export default Routes;
