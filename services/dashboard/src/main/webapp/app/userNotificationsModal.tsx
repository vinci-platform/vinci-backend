import React from 'react';
import { Translate, translate } from 'react-jhipster';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Alert, Row, Col } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Link } from 'react-router-dom';
import { UserAlertsDetails } from 'app/entities/user-extra/user-alerts-details';
import { IUserExtra } from 'app/shared/model/user-extra.model';

export interface IUserNotificationModalProps {
  showModal: boolean;
  handleClose: Function;
  user: IUserExtra;
  updateEntities: Function;
  userType: string;
}

class UserNotificationModal extends React.Component<IUserNotificationModalProps> {
  render() {
    const { handleClose, user, showModal, updateEntities, userType } = this.props;

    return (
      <UserAlertsDetails
        showModal={showModal}
        userType={userType}
        handleClose={handleClose}
        updateEntities={updateEntities}
        userEntity={user}
        history={this.props.children as any}
        location={this.props.children as any}
        match={this.props.children as any}
      />
    );
  }
}

export default UserNotificationModal;
