import React from 'react';
import { Map as LeafletMap, TileLayer, Marker, Popup, Polyline } from 'react-leaflet';
import './map.css';

interface ICustomInputProps {
  readonly records: any;
}

class VinciMap extends React.Component<ICustomInputProps> {
  render() {
    const { records } = this.props;
    let showMarkers = true;
    let center = [44.4268, 26.1025];

    const locationRecords = [];
    records.forEach(record => {
      const coords = record.data.split(';');
      locationRecords.push([parseFloat(coords[0].slice(1)), parseFloat(coords[1].slice(1))]);
    });

    let firstRecord = {};
    let lastRecord = {};
    if (records.length !== 0) {
      firstRecord = records[0];
      lastRecord = records[records.length - 1];

      firstRecord['coords'] = locationRecords[0];
      lastRecord['coords'] = locationRecords[locationRecords.length - 1];

      firstRecord['timestamp'] = new Date(firstRecord['timestamp']).toLocaleString();
      lastRecord['timestamp'] = new Date(lastRecord['timestamp']).toLocaleString();

      const max1 = Math.max(...locationRecords.map(x => x[0]));
      const min1 = Math.min(...locationRecords.map(x => x[0]));

      const max2 = Math.max(...locationRecords.map(x => x[1]));
      const min2 = Math.min(...locationRecords.map(x => x[1]));

      center = [(max1 + min1) / 2, (max2 + min2) / 2];
    } else {
      showMarkers = false;
    }

    return (
      <LeafletMap
        center={center}
        zoom={13}
        maxZoom={20}
        attributionControl
        zoomControl
        doubleClickZoom
        scrollWheelZoom
        dragging
        animate
        easeLinearity={0.35}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
        />
        <Polyline color={'red'} positions={locationRecords} />
        {showMarkers && (
          <Marker position={firstRecord['coords']}>
            <Popup>
              Timestamp: {firstRecord['timestamp']}
              <br />
              Position: {firstRecord['data']}
            </Popup>
          </Marker>
        )}

        {showMarkers && (
          <Marker position={lastRecord['coords']}>
            <Popup>
              Timestamp: {lastRecord['timestamp']}
              <br />
              Position: {lastRecord['data']}
            </Popup>
          </Marker>
        )}
      </LeafletMap>
    );
  }
}

export default VinciMap;
