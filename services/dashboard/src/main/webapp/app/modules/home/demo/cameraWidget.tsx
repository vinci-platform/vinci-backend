import React from 'react';
import '../home.scss';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { withStyles, Card, CardHeader, CardContent, CardActions, Collapse, Avatar, IconButton, Typography, Badge } from '@material-ui/core';
import purple from '@material-ui/core/colors/purple';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import { getSession } from 'app/shared/reducers/authentication';
import UserAlertNotificationModal from './userAlertNotificationsModal';
import History from './history';
import { getLastEntity } from 'app/entities/ioserver/camera-movement-data/camera-movement-data.reducer';
import * as THREE from 'three';

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: purple[500]
  },
  notification: {
    outline: 'none !important'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, ICameraWidgetProps {}

interface ICameraWidgetProps {
  readonly classes: any;
  readonly cameraMovementData?: any;
  readonly cameraRecognitionData?: any;
}

export interface ICameraWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
}

class CameraWidget extends React.Component<IHomeProp, ICameraWidgetState> {
  readonly state: ICameraWidgetState = { expanded: false, showModal: false, isHistoryOpen: false };
  private mount;

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  onToggleHistoryOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  componentDidMount() {
    // TODO: uncomment
    // setTimeout(() => this.props.getLastEntity(), 5000);
    // === THREE.JS CODE START ===
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 100);
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(800, 400);
    // document.body.appendChild( renderer.domElement );
    this.mount.appendChild(renderer.domElement);
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
    camera.position.z = 5;
    const animate = () => {
      requestAnimationFrame(animate);
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;
      renderer.render(scene, camera);
    };
    animate();
    // === THREE.JS EXAMPLE CODE END ===
  }

  render() {
    const { classes, cameraMovementData, cameraRecognitionData } = this.props;
    const { showModal, isHistoryOpen } = this.state;

    return (
      <Card className="col-sm-5 col-md-5 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Camera" className={classes.avatar}>
              C
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red' }} />
              </Badge>
            </IconButton>
          }
          title={<Translate contentKey="global.devices.camera">Camera </Translate>}
          subheader={<Translate contentKey="global.devices.cameraInfo">Your camera state </Translate>}
        />
        <CardContent>
          {cameraMovementData && <img src={`data:image/png;base64,${cameraMovementData.depthImage}`} alt="camera" />}
        </CardContent>
        <CardContent>
          <div
            style={{ width: '400px', height: '400px', display: 'none' }}
            ref={mount => {
              this.mount = mount;
            }}
          />
        </CardContent>
        <CardContent>
          {cameraRecognitionData &&
            cameraRecognitionData.detectedPersons && (
              <Typography component="p">Detected persons: {cameraRecognitionData.detectedPersons.map(person => person)}</Typography>
            )}
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton aria-label="History data" onClick={this.onToggleHistoryOpen} className={classes.expandHistory}>
            <HistoryIcon />
          </IconButton>
          {isHistoryOpen && <History />}
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Typography>
            <Typography paragraph>
              Arcu odio ut sem nulla pharetra diam sit amet. Leo duis ut diam quam nulla porttitor. Maecenas volutpat blandit aliquam etiam
              erat velit scelerisque in. At volutpat diam ut venenatis tellus in metus vulputate. A arcu cursus vitae congue mauris rhoncus.
              Arcu odio ut sem nulla pharetra diam sit amet. Amet cursus sit amet dictum. Adipiscing elit ut aliquam purus sit amet luctus.
              Enim sed faucibus turpis in eu mi. Pellentesque diam volutpat commodo sed egestas.
            </Typography>
          </CardContent>
        </Collapse>
        <UserAlertNotificationModal showModal={showModal} handleClose={this.handleClose} userAlerts={[]} />
      </Card>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, getLastEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CameraWidget)
);
