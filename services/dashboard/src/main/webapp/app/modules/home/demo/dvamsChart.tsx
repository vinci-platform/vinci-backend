import React from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend } from 'recharts';
import moment from 'moment';
import { SurveyStatusType } from 'app/shared/model/ioserver/survey-data.model';

const formatTime = unixTime => moment(unixTime).format('YYYY-MM-DD');

interface IDVAMSChartProps {
  readonly data?: any;
}

class DvamsChart extends React.Component<IDVAMSChartProps> {
  labelFormatter = value => formatTime(value);

  tooltipFormatter = (value, name, props) => [value, name];

  render() {
    const { data } = this.props;
    return (
      <div style={{ width: '100%', height: 300 }}>
        <ResponsiveContainer>
          <AreaChart
            data={data}
            margin={{
              top: 10,
              right: 30,
              left: 0,
              bottom: 0
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="createdTime" domain={['auto', 'auto']} name="Time" />
            <YAxis domain={['1', '5']} />
            <Tooltip labelFormatter={this.labelFormatter} formatter={this.tooltipFormatter} />
            <Legend />
            <Area type="monotone" dataKey="scoringResult" name="Scoring result" stroke="#8884d8" fill="#8884d8" />
          </AreaChart>
        </ResponsiveContainer>
      </div>
    );
  }
}

export default DvamsChart;
