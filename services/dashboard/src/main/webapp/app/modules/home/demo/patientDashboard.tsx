import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { getUser, reset } from '../../administration/user-management/user-management.reducer';
import { IRootState } from 'app/shared/reducers';
import WatchWidget from 'app/modules/home/demo/watchWidget';
import CameraWidget from 'app/modules/home/demo/cameraWidget';
import SurveyWidget from 'app/modules/home/demo/surveyWidget';
import ShoeWidget from 'app/modules/home/demo/shoeWidget';
import { getUserDevices } from 'app/shared/reducers/user-utils';
import { Typography } from '@material-ui/core';
import { Translate } from 'react-jhipster';
import FitbitWatchWidget from 'app/modules/home/demo/fitbitWatchWidget';
import FitbitWatchIntradayWidget from 'app/modules/home/demo/fitbitWatchIntradayWidget';

export interface IPatientDashboardProps extends StateProps, DispatchProps, RouteComponentProps<{ login: string }> {}

export class PatientDashboard extends React.Component<IPatientDashboardProps> {
  componentDidMount() {
    // this.props.getUser(this.props.match.params.login);
    this.props.getUserDevices(this.props.match.params.login);
  }

  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    const { user, devices, cameraMovement, cameraRecognition } = this.props;
    return (
      <div className="widgetsContainer d-flex flex-wrap align-content-center justify-content-center">
        {user && (
          <Fragment>
            {devices &&
              devices.length > 0 && (
                <Fragment>
                  <FitbitWatchWidget devices={devices} />
                  <FitbitWatchIntradayWidget user={user} devices={devices} />
                  <SurveyWidget user={user} devices={devices} />
                  <ShoeWidget devices={devices} />
                </Fragment>
              )}
            {!devices ||
              (devices.length === 0 && (
                <Typography variant="h2" component="h3">
                  <Translate contentKey="gatewayApp.device.noDevices"> There are no devices associated with this account</Translate>
                </Typography>
              ))}
          </Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  user: storeState.userManagement.user,
  roles: storeState.userManagement.authorities,
  loading: storeState.userManagement.loading,
  updating: storeState.userManagement.updating,
  devices: storeState.userUtils.devices,
  cameraMovement: storeState.userUtils.cameraMovement,
  cameraRecognition: storeState.userUtils.cameraRecognition
});

const mapDispatchToProps = { getUser, reset, getUserDevices };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientDashboard);
