import React from 'react';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import {
  withStyles,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Collapse,
  Avatar,
  IconButton,
  Typography,
  Badge
} from '@material-ui/core';
import orange from '@material-ui/core/colors/orange';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import { getSession } from 'app/shared/reducers/authentication';
import UserAlertNotificationModal from './userAlertNotificationsModal';
import History from './history';
import VinciMap from './map/map';
import { DeviceType } from 'app/shared/model/device.model';
import { IRootState } from 'app/shared/reducers';
import { getEntitiesFor } from 'app/entities/ioserver/watch-data/watch-data.reducer';

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: orange[500]
  },
  notification: {
    outline: 'none !important'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, IWatchWidgetProps {}

interface IWatchWidgetProps {
  readonly classes: any;
  readonly devices?: any;
}

export interface IWatchWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
  center: {};
  zoom: any;
  recordsLoaded: boolean;
  start: Date;
  end: Date;
  getDataTimer: number;
}

const polygon = [
  { lat: 44.353193, lng: 25.962334 },
  { lat: 44.353193, lng: 26.120378 },
  { lat: 44.469753, lng: 26.120378 },
  { lat: 44.469753, lng: 25.962334 }
];

const records = [[44.4268, 26.1027], [44.4269, 26.1028], [44.427, 26.103], [44.4271, 26.1031]];

const center = {
  lat: 44.4268,
  lng: 26.1025
};

const startDate = new Date();
startDate.setDate(startDate.getDate() - 100);

class WatchWidget extends React.Component<IHomeProp, IWatchWidgetState> {
  readonly state: IWatchWidgetState = {
    expanded: false,
    showModal: false,
    isHistoryOpen: false,
    center: {
      lat: 44.4268,
      lng: 26.1025
    },
    zoom: 13,
    recordsLoaded: false,
    start: startDate,
    end: new Date(),
    getDataTimer: null
  };

  constructor(props) {
    super(props);

    this.handleChangeInterval = this.handleChangeInterval.bind(this);
  }

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  handleChangeInterval(e) {
    const left = new Date();
    const right = new Date();
    switch (e.target.value) {
      case 'ONE_DAY':
        left.setDate(left.getDate() - 1);
        break;
      case 'ONE_WEEK':
        left.setDate(left.getDate() - 7);
        break;
      case 'TWO_WEEKS':
        left.setDate(left.getDate() - 14);
        break;
      case 'THREE_WEEKS':
        left.setDate(left.getDate() - 21);
        break;
      case 'ONE_MONTH':
        left.setDate(left.getDate() - 30);
        break;
      case 'CUSTOM':
      default:
        left.setDate(left.getDate() - 100);
        break;
    }

    this.setState({
      start: left,
      end: right,
      recordsLoaded: false
    });
  }

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  onToggleHistoryOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  componentDidMount() {
    if (
      typeof this.props.devices !== 'undefined' &&
      this.props.devices !== null &&
      this.props.devices.filter(device => device.deviceType === DeviceType.WATCH).length > 0
    ) {
      this.props.getEntitiesFor(
        this.props.devices.find(device => device.deviceType === DeviceType.WATCH).id,
        new Date(Date.now() - 100 * 24 * 60 * 60 * 1000),
        new Date()
      );
      const timer = setInterval(
        () =>
          this.props.getEntitiesFor(
            this.props.devices.find(device => device.deviceType === DeviceType.WATCH).id,
            new Date(Date.now() - 100 * 24 * 60 * 60 * 1000),
            new Date()
          ),
        300000 // update every 5 min
      );
      this.setState({ getDataTimer: timer });
    }
  }

  componentWillUnmount() {
    if (this.state.getDataTimer !== null) {
      clearTimeout(this.state.getDataTimer);
    }
  }

  render() {
    const { classes, locationRecords } = this.props;
    const { showModal, isHistoryOpen, start, end } = this.state;

    return (
      <Card className="col-sm-6 col-md-10 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Watch" className={classes.avatar}>
              W
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red', outline: 'none !important' }} />
              </Badge>
            </IconButton>
          }
          title={<Translate contentKey="global.devices.watch">Watch </Translate>}
          subheader={<Translate contentKey="global.devices.watchInfo">Your watch information </Translate>}
        />
        <CardContent>
          <VinciMap records={locationRecords} />
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing onChange={this.handleChangeInterval}>
          <IconButton aria-label="History data" onClick={this.onToggleHistoryOpen} className={classes.expandHistory}>
            <HistoryIcon />
          </IconButton>
          {isHistoryOpen && <History />}
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon style={{ outline: 'none !important' }} />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Typography>
            <Typography paragraph>
              Arcu odio ut sem nulla pharetra diam sit amet. Leo duis ut diam quam nulla porttitor. Maecenas volutpat blandit aliquam etiam
              erat velit scelerisque in. At volutpat diam ut venenatis tellus in metus vulputate. A arcu cursus vitae congue mauris rhoncus.
              Arcu odio ut sem nulla pharetra diam sit amet. Amet cursus sit amet dictum. Adipiscing elit ut aliquam purus sit amet luctus.
              Enim sed faucibus turpis in eu mi. Pellentesque diam volutpat commodo sed egestas.
            </Typography>
          </CardContent>
        </Collapse>
        <UserAlertNotificationModal showModal={showModal} handleClose={this.handleClose} userAlerts={[]} />
      </Card>
    );
  }
}
const mapStateToProps = ({ authentication, watchData }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated,
  locationRecords: watchData.locationAlerts
});

const mapDispatchToProps = {
  getSession,
  getEntitiesFor
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(WatchWidget)
);
