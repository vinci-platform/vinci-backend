import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PatientDashboard from './demo/patientDashboard';
import Home from './home';

const Routes = ({ match }) => (
  <div>
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}/`} component={Home} />
      <ErrorBoundaryRoute path={`${match.url}:login/patient`} component={PatientDashboard} />
    </Switch>
  </div>
);

export default Routes;
