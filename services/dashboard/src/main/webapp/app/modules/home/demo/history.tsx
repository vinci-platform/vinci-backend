import React, { PureComponent } from 'react';
import '../home.scss';
import { Translate } from 'react-jhipster';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import IntervalModal from './intervalModal';
import { interval } from 'app/config/constants';

export interface IDeviceHistorytState {
  interval?: string;
  onSelectInterval?: (val: string) => any;
  handleCustomInterval?: (val: any) => any;
}

class History extends PureComponent<IDeviceHistorytState> {
  readonly state: IDeviceHistorytState = {
    interval: this.props.interval ? this.props.interval : interval.ONE_DAY,
    onSelectInterval: val => {},
    handleCustomInterval: val => {}
  };

  constructor(props) {
    super(props);
    this.handleChangeInterval = this.handleChangeInterval.bind(this);
  }

  handleChangeInterval(e) {
    this.setState({
      interval: e.target.value
    });
    const selectIntervalFn = this.props.onSelectInterval || null;
    if (selectIntervalFn) {
      selectIntervalFn(e.target.value);
    }
  }

  handleCustomInterval = val => {
    const customIntervalFn = this.props.handleCustomInterval || null;
    if (customIntervalFn) {
      customIntervalFn(val);
    }
  };

  render() {
    return (
      <div>
        <AvForm>
          <AvGroup>
            <Label id="deviceTypeLabel">
              <Translate contentKey="gatewayApp.device.timeline.interval">Interval</Translate>
            </Label>
            <AvInput id="deviceHistory" type="select" name="interval" value={this.state.interval} onChange={this.handleChangeInterval}>
              <option value="ONE_DAY">One day</option>
              <option value="ONE_WEEK">One week</option>
              <option value="TWO_WEEKS">Two weeks</option>
              <option value="THREE_WEEKS">Three weeks</option>
              <option value="ONE_MONTH">One month</option>
              <option value="THREE_MONTHS">Three months</option>
              <option value="CUSTOM"> Custom</option>
            </AvInput>
            {this.state.interval === 'CUSTOM' && <IntervalModal onSelectCustomInterval={this.handleCustomInterval} opened />}
          </AvGroup>
        </AvForm>
      </div>
    );
  }
}

export default History;
