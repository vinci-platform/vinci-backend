import React from 'react';
import { Translate, TextFormat } from 'react-jhipster';
import { Alert, Col, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import { AlertType, IUserAlert } from 'app/shared/model/user-alert.model';
import { APP_DATE_FORMAT } from 'app/config/constants';

export interface INotificationModalProps {
  showModal: boolean;
  handleClose: Function;
  userAlerts: IUserAlert[];
}

class UserAlertNotificationModal extends React.Component<INotificationModalProps> {
  render() {
    const { handleClose, userAlerts } = this.props;

    console.log(userAlerts);

    return (
      <Modal isOpen={this.props.showModal} toggle={handleClose} autoFocus={false}>
        <AvForm>
          <ModalHeader toggle={handleClose}>
            <div style={{ color: '#007bff' }}>
              <Translate contentKey="gatewayApp.device.notifications">Notifications</Translate>
            </div>
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col md="12">
                <h6>
                  <Translate
                    contentKey="gatewayApp.device.notificationsDeviceSubtitle"
                    interpolate={{ number: userAlerts.filter(alert => !alert.familyRead).length }}
                  >
                    You have notification(s)
                  </Translate>
                  <span style={{ color: '#007bff' }}>this user</span>
                </h6>
              </Col>
            </Row>
            <div className="mt-1">&nbsp;</div>
            <Row>
              <Col md="12">
                {userAlerts.map((alert, i) => {
                  const [lat, lon] = alert.values.split(';');
                  return (
                    <Alert color="warning" key={`alert-${i}`}>
                      <div style={{ fontWeight: alert.familyRead ? 'normal' : 'bold' }}>
                        <span
                          style={{
                            color:
                              alert.alertType === AlertType.WARNING
                                ? 'orange'
                                : alert.alertType === AlertType.DANGER
                                  ? 'red'
                                  : alert.alertType === AlertType.SUCCESS
                                    ? 'green'
                                    : '#007bff'
                          }}
                        >
                          {alert.alertType}
                        </span>
                        :
                        {/* <Translate contentKey={`${alert.label}`} interpolate={{ lat, lon }}> */}
                          {alert.label} - {alert.values}
                        {/* </Translate> */}
                        @ <TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
                      </div>
                    </Alert>
                  );
                })}
              </Col>
            </Row>
          </ModalBody>
        </AvForm>
      </Modal>
    );
  }
}

export default UserAlertNotificationModal;
