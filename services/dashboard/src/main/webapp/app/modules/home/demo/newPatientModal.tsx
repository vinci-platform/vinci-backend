import React from 'react';
import { Translate, translate } from 'react-jhipster';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Label
} from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput } from 'availity-reactstrap-validation';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { getFirstAndLastNameWithUuid } from 'app/modules/administration/user-management/user-management.reducer';
import { MAX_SIZE } from 'app/shared/util/pagination.constants';
import { IRootState } from 'app/shared/reducers';
import { IUserEssential } from 'app/shared/model/user.model';

export interface INewPatientModalProps extends StateProps, DispatchProps {
  showModal: boolean;
  handleClose: Function;
  associatePatient: Function;
  isFamily: boolean;
}

export interface INewPatientState {
  activeTab: string;
  showEmail: boolean;
  showFilters: boolean;
  nameSearchValue: string;
}

class NewPatientModal extends React.Component<INewPatientModalProps, INewPatientState> {
  readonly state: INewPatientState = {
    activeTab: '1',
    showEmail: false,
    showFilters: false,
    nameSearchValue: ''
  };

  componentDidMount(): void {
    this.props.getFirstAndLastNameWithUuid(null, MAX_SIZE);
  }

  handleAssociatePacient = (generate = false) => (event, values) => {
    this.props.associatePatient(generate, values);
    this.props.handleClose();
  };

  toggle = tab => () => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };

  toggleEmail = () => {
    this.setState({ showEmail: !this.state.showEmail });
  };

  handleFilters = () => {
    this.setState(
      {
        showFilters: !this.state.showFilters
      },
      () => {
        if (!this.state.showFilters) {
          this.setState({ nameSearchValue: '' });
        }
      }
    );
  };

  onNameSearchChange = event => {
    this.setState({ nameSearchValue: event.target.value });
  };

  filterFn = (patient: IUserEssential) => {
    const name = this.state.nameSearchValue.split(' ');

    if (name.length === 1 && name[0] === '') return true;

    return name.some(n => patient.firstName.toLowerCase() === n.toLowerCase() || patient.lastName.toLowerCase() === n.toLowerCase());
  };

  render() {
    const { handleClose, patients } = this.props;
    const { nameSearchValue, showFilters } = this.state;

    return (
      <Modal isOpen={this.props.showModal} toggle={handleClose} autoFocus={false}>
        <ModalHeader toggle={handleClose}>
          <div style={{ color: '#007bff' }}>
            <Translate contentKey="global.patient.addNewPatient">Add new patient</Translate>
          </div>
        </ModalHeader>
        <ModalBody>
          <Nav tabs>
            <NavItem>
              <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={this.toggle('1')}>
                <Translate contentKey="userManagement.existingUser"> Existing user</Translate>
              </NavLink>
            </NavItem>
            {!this.props.isFamily && (
              <NavItem>
                <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={this.toggle('2')}>
                  <Translate contentKey="userManagement.newUser"> New user</Translate>
                </NavLink>
              </NavItem>
            )}
          </Nav>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Button color="primary" onClick={this.handleFilters} style={{ marginTop: '2%' }}>
                {showFilters ? 'Remove filters' : 'Use filters'}
              </Button>
              {showFilters && (
                <AvForm style={{ marginTop: '2%' }}>
                  <Row>
                    <Col>
                      <AvGroup>
                        <Label id="userName">
                          <Translate contentKey="userManagement.usersFirstAndOrLastName">User's name</Translate>
                        </Label>
                        <AvInput
                          id="user-name"
                          type="text"
                          className="form-control"
                          name="user-name"
                          value={nameSearchValue}
                          onChange={this.onNameSearchChange}
                        />
                      </AvGroup>
                    </Col>
                  </Row>
                </AvForm>
              )}
              <AvForm onValidSubmit={this.handleAssociatePacient(false)} id="associate">
                <Row>
                  <Col md="12">
                    <AvGroup>
                      <Label for="user">
                        <Translate contentKey="gatewayApp.userExtra.user">User</Translate>
                      </Label>
                      <AvField name="uuid" type="select">
                        {patients.filter(this.filterFn).map((otherEntity: IUserEssential) => (
                          <option value={otherEntity.uuid} key={otherEntity.id}>
                            {otherEntity.id} {otherEntity.firstName} {otherEntity.lastName}
                          </option>
                        ))}
                      </AvField>
                    </AvGroup>
                  </Col>
                </Row>
              </AvForm>
            </TabPane>
            {!this.props.isFamily && (
              <TabPane tabId="2">
                <AvForm onValidSubmit={this.handleAssociatePacient(true)} id="generate">
                  <Row>
                    <Col sm="12">
                      {/* First name */}
                      <AvField
                        className="form-control"
                        name="firstName"
                        label={translate('settings.form.firstname')}
                        id="firstName"
                        placeholder={translate('settings.form.firstname.placeholder')}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: translate('settings.messages.validate.firstname.required')
                          },
                          minLength: {
                            value: 1,
                            errorMessage: translate('settings.messages.validate.firstname.minlength')
                          },
                          maxLength: {
                            value: 50,
                            errorMessage: translate('settings.messages.validate.firstname.maxlength')
                          }
                        }}
                      />
                      {/* Last name */}
                      <AvField
                        className="form-control"
                        name="lastName"
                        label={translate('settings.form.lastname')}
                        id="lastName"
                        placeholder={translate('settings.form.lastname.placeholder')}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: translate('settings.messages.validate.lastname.required')
                          },
                          minLength: {
                            value: 1,
                            errorMessage: translate('settings.messages.validate.lastname.minlength')
                          },
                          maxLength: {
                            value: 50,
                            errorMessage: translate('settings.messages.validate.lastname.maxlength')
                          }
                        }}
                      />
                      {/* Phone */}
                      <AvField
                        className="form-control"
                        name="phone"
                        type="number"
                        label={translate('settings.form.phone')}
                        id="phone"
                        placeholder={translate('settings.form.phone.placeholder')}
                        validate={{
                          required: {
                            value: true,
                            errorMessage: translate('settings.messages.validate.phone.required')
                          },
                          minLength: {
                            value: 10,
                            errorMessage: translate('settings.messages.validate.phone.minlength')
                          },
                          maxLength: {
                            value: 10,
                            errorMessage: translate('settings.messages.validate.phone.maxlength')
                          }
                        }}
                      />
                      <AvGroup check>
                        <Label check>
                          <AvInput type="checkbox" name="checkbox" onChange={this.toggleEmail} />
                          <Translate contentKey="userManagement.generateAccount"> Generate user account</Translate>
                        </Label>
                      </AvGroup>
                      {/* Email */}
                      {this.state.showEmail && (
                        <AvField
                          name="email"
                          label={translate('global.form.email')}
                          placeholder={translate('global.form.email.placeholder')}
                          type="email"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: translate('global.messages.validate.email.required')
                            },
                            minLength: {
                              value: 5,
                              errorMessage: translate('global.messages.validate.email.minlength')
                            },
                            maxLength: {
                              value: 254,
                              errorMessage: translate('global.messages.validate.email.maxlength')
                            }
                          }}
                        />
                      )}
                    </Col>
                  </Row>
                </AvForm>
              </TabPane>
            )}
          </TabContent>
        </ModalBody>
        <ModalFooter>
          {this.state.activeTab === '1' && (
            <Button color="primary" type="submit" form="associate">
              <Translate contentKey="global.form.submit">Submit</Translate>
            </Button>
          )}
          {this.state.activeTab === '2' && (
            <Button color="primary" type="submit" form="generate">
              <Translate contentKey="global.form.submit">Submit</Translate>
            </Button>
          )}
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ userManagement }: IRootState) => ({ patients: userManagement.usersEssential });

const mapDispatchToProps = { getFirstAndLastNameWithUuid };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPatientModal);
