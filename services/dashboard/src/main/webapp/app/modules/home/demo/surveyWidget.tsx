import React from 'react';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { withStyles, Card, CardHeader, CardContent, CardActions, Collapse, Avatar, IconButton, Typography, Badge } from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import DVAMSChart from './dvamsChart';
import { getSession } from 'app/shared/reducers/authentication';
import UserAlertNotificationModal from 'app/modules/home/demo/userAlertNotificationsModal';
import History from './history';
import { IUser } from 'app/shared/model/user.model';
import {DeviceType, IDevice} from 'app/shared/model/device.model';
import { getEntitiesFor, reset } from 'app/entities/ioserver/survey-data/survey-data.reducer';
import { IRootState } from 'app/shared/reducers';
import moment from 'moment';
import { SurveyStatusType } from 'app/shared/model/ioserver/survey-data.model';
import { interval } from 'app/config/constants';
import { getAdditionToTimestamp } from 'app/entities/utils';
import DeviceAlertNotificationModal from "app/modules/home/demo/deviceAlertNotificationsModal";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: green[500]
  },
  notification: {
    outline: 'none !important'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, ISurveyWidgetProps {}

interface ISurveyWidgetProps {
  readonly classes: any;
  readonly user: IUser;
  readonly devices?: any;
}

export interface ISurveyWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
  historyInterval: string;
  fromDate: string;
  toDate: string;
}

class SurveyWidget extends React.Component<IHomeProp, ISurveyWidgetState> {
  private timer = null;

  readonly state: ISurveyWidgetState = {
    expanded: false,
    showModal: false,
    isHistoryOpen: false,
    historyInterval: interval.THREE_MONTHS,
    fromDate: '',
    toDate: ''
  };

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  onToggleHistoryOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  componentDidMount() {
    let surveyDevices = this.props.devices.filter(device => device.deviceType === DeviceType.SURVEY);
    if (typeof this.props.devices !== 'undefined' && this.props.devices !== null && surveyDevices.length > 0) {
      const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
      this.props.getEntitiesFor(surveyDevices[0].id, Date.now() - timeless);
      this.timer = setInterval(() => {
        const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
        this.props.getEntitiesFor(surveyDevices[0].id, Date.now() - timeless);
      }, 30000);
    }
  }

  componentWillUnmount(): void {
    if (this.timer != null) {
      clearInterval(this.timer);
    }
    this.props.reset();
  }

  filterFn = l => {
    switch (this.state.historyInterval) {
      case interval.ONE_DAY:
        return this.checkDateByDayInterval(l.createdTime, 1);
      case interval.ONE_WEEK:
        return this.checkDateByDayInterval(l.createdTime, 7);
      case interval.TWO_WEEKS:
        return this.checkDateByDayInterval(l.createdTime, 14);
      case interval.THREE_WEEKS:
        return this.checkDateByDayInterval(l.createdTime, 21);
      case interval.ONE_MONTH:
        return this.checkDateByDayInterval(l.createdTime, 30);
      case interval.THREE_MONTHS:
        return this.checkDateByDayInterval(l.createdTime, 90);
      case interval.CUSTOM: {
        const [fromDate] = this.state.fromDate.split('T');
        const [toDate] = this.state.toDate.split('T');

        return fromDate !== '' && toDate !== ''
          ? new Date(l.createdTime) >= new Date(fromDate) && new Date(l.createdTime) <= new Date(toDate)
          : fromDate !== ''
            ? new Date(l.createdTime) >= new Date(fromDate)
            : toDate !== ''
              ? new Date(l.createdTime) <= new Date(toDate)
              : true;
      }
      default:
        return true;
    }
  };

  checkDateByDayInterval = (date: string, daysAgo: number): boolean => {
    const today = new Date();
    return new Date(Date.UTC(today.getFullYear(), today.getMonth(), today.getDate() - daysAgo)) <= new Date(date);
  };

  handleHistory = val => {
    this.setState({ historyInterval: val });
  };

  handleCustomHistory = val => {
    this.setState({ fromDate: val.fromDate, toDate: val.toDate });
  };

  private getAlertsFromUserDevices(userDevices: ReadonlyArray<IDevice>, deviceType: DeviceType) {
    let iDevice = userDevices.find(value => value.deviceType === deviceType);
    if (iDevice.alerts === undefined)return [];
    return iDevice.alerts;
  }

  render() {
    const { classes, user, dvamsSurveyDataList, dvamsStatus, ipaqSurveyDataList, devices } = this.props;
    const { showModal, isHistoryOpen } = this.state;

    return (
      <Card className="col-sm-6 col-md-10 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Survey" className={classes.avatar}>
              S
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red', outline: 'none !important' }} />
              </Badge>
            </IconButton>
          }
          title={
            <Translate contentKey="global.devices.surveyData" interpolate={{ user: `${user.firstName} ${user.lastName}` }}>
              Survey
            </Translate>
          }
          subheader={
            dvamsSurveyDataList.length > 0 ? (
              <Translate contentKey="global.devices.patientState" interpolate={{ status: SurveyStatusType[dvamsStatus - 1] }}>
                Status:
              </Translate>
            ) : (
              'Loading...'
            )
          }
        />
        {dvamsSurveyDataList.length > 0 && (
          <CardContent>
            <p>WHOQOL_BREF</p>
            <DVAMSChart data={dvamsSurveyDataList.filter(this.filterFn)} />
            <p>IPAQ</p>
            <DVAMSChart data={ipaqSurveyDataList.filter(this.filterFn)} />
          </CardContent>
        )}
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton aria-label="History data" onClick={this.onToggleHistoryOpen} className={classes.expandHistory}>
            <HistoryIcon style={{ outline: 'none !important' }} />
          </IconButton>
          {isHistoryOpen && (
            <History
              onSelectInterval={this.handleHistory}
              handleCustomInterval={this.handleCustomHistory}
              interval={this.state.historyInterval}
            />
          )}
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent />
        </Collapse>
          <DeviceAlertNotificationModal showModal={showModal} handleClose={this.handleClose} alerts={this.getAlertsFromUserDevices(devices,DeviceType.SURVEY)} />
      </Card>
    );
  }
}

const mapStateToProps = ({ surveyData }: IRootState) => {
  let dvamsStatus = 0;
  let dvamsStatusTimestamp = '';
  let results = surveyData.entities;
  let whoqol_bref_list = results.filter(data => data.surveyType === 'WHOQOL_BREF');
  let ipaq_list = results.filter(data => data.surveyType === 'IPAQ');
  whoqol_bref_list = whoqol_bref_list.slice().sort((a, b) => (a.createdTime > b.createdTime ? 1 : b.createdTime > a.createdTime ? -1 : 0));
  ipaq_list = ipaq_list.slice().sort((a, b) => (a.createdTime > b.createdTime ? 1 : b.createdTime > a.createdTime ? -1 : 0));
  if (whoqol_bref_list.length > 90) {
    whoqol_bref_list = whoqol_bref_list.slice(-90);
  }
  whoqol_bref_list.forEach(a => {
    if (a.createdTime > dvamsStatusTimestamp) {
      dvamsStatus = a.scoringResult;
      dvamsStatusTimestamp = a.createdTime;
    }
  });
  if (ipaq_list.length > 90) {
    ipaq_list = ipaq_list.slice(-90);
  }
  ipaq_list.forEach(a => {
    if (a.createdTime > dvamsStatusTimestamp) {
      dvamsStatus = a.scoringResult;
      dvamsStatusTimestamp = a.createdTime;
    }
  });

  return {
    dvamsSurveyDataList: whoqol_bref_list.map(data => {
      return { createdTime: moment(data.createdTime).format('YYYY-MM-DD'), scoringResult: data.scoringResult };
    }),
    dvamsStatus,
    ipaqSurveyDataList: ipaq_list.map(data => {
      return { createdTime: moment(data.createdTime).format('YYYY-MM-DD'), scoringResult: data.scoringResult };
    })
  };
};

const mapDispatchToProps = { getSession, getEntitiesFor, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SurveyWidget)
);
