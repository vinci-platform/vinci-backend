import './home.scss';
import { CardHeader } from '@material-ui/core';
import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Storage, Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import SurveyWidget from './demo/surveyWidget';
import { getSession } from 'app/shared/reducers/authentication';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import PatientWidget from './demo/patientWidget';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NewPatientModal from 'app/modules/home/demo/newPatientModal';
import { handleAssociatePatient, getAssociatedPatients, getUserDevices, handleGeneratePatient } from 'app/shared/reducers/user-utils';
import UserExtra from 'app/modules/home/organization/user-extra';
import ShoeWidget from 'app/modules/home/demo/shoeWidget';
import FitbitWatchWidget from 'app/modules/home/demo/fitbitWatchWidget';
import FitbitWatchIntradayWidget from 'app/modules/home/demo/fitbitWatchIntradayWidget';
import { LocaleMenu } from 'app/shared/layout/header/menus';
import { setLocale } from 'app/shared/reducers/locale';
import color from '@material-ui/core/colors/amber';

export interface IHomeProp extends StateProps, DispatchProps, RouteComponentProps<any> { }

export interface IHomeState {
  showModal: boolean;
}

export class Home extends React.Component<IHomeProp> {
  readonly state: IHomeState = { showModal: false };

  componentDidMount() {
    this.props.getSession();
  }

  componentDidUpdate(prevProps: Readonly<IHomeProp>, prevState: Readonly<{}>, snapshot?: any): void {
    if (this.props.isAuthenticated) {
      if ((this.props.isFamily || this.props.isOrganization) && this.props.associatedUsers === null) {
        this.props.getAssociatedPatients(null);
      } else if (this.props.isPatient && this.props.devices === null) {
        this.props.getUserDevices('');
      }
    }
  }

  addPatient = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  handleAssociatePatient = (generate, values) => {
    if (generate) {
      const returnUser = {
        email: values.email,
        firstName: values.firstName,
        lastName: values.lastName,
        phone: values.phone
      }
      this.props.handleGeneratePatient(returnUser);
    } else {
      this.props.handleAssociatePatient(values.uuid, this.props.isFamily, this.props.isOrganization);
    }
  };

  handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    this.props.setLocale(langKey);
  };

  render() {
    const {
      account,
      isPatient,
      isFamily,
      isOrganization,
      isAuthenticated,
      associatedUsers,
      devices,
      cameraMovement,
      cameraRecognition,
      currentLocale
    } = this.props;
    const { showModal } = this.state;
    return (
      <div className="height-100">
        {account && account.login ? (
          <div className="p-3">
            {(isFamily || isOrganization) && (
              <div style={{ height: '3rem' }}>
                <Button onClick={this.addPatient} className="btn btn-primary float-right jh-create-entity new-patient-btn">
                  <FontAwesomeIcon icon="plus" />
                  &nbsp;
                  <Translate contentKey="global.patient.addNewPatient">Add new patient</Translate>
                </Button>
                <NewPatientModal
                  showModal={showModal}
                  handleClose={this.handleClose}
                  associatePatient={this.handleAssociatePatient}
                  isFamily={isFamily}
                />
              </div>
            )}
            {isPatient && (
              <div className="widgetsContainer d-flex flex-wrap align-content-center justify-content-center">
                {devices && <FitbitWatchWidget devices={devices} />}
                {devices && <FitbitWatchIntradayWidget user={account} devices={devices} />}
                {devices && <SurveyWidget user={account} devices={devices} />}
                {devices && <ShoeWidget devices={devices} />}
              </div>
            )}
            {isFamily &&
              associatedUsers !== null && (
                <div
                  className="widgetsContainer d-flex flex-wrap align-content-center justify-content-center"
                  style={{ fontSize: '0.8rem' }}
                >
                  {associatedUsers.map((user, i) => (
                    <PatientWidget key={`patient-${i}`} user={user} />
                  ))}
                </div>
              )}
            {isOrganization &&
              associatedUsers !== null && (
                <UserExtra users={associatedUsers} location={this.props.location} history={this.props.history} match={this.props.match} />
              )}
            {!isPatient && !isFamily && !isOrganization && (
              <div className="home-admin">
                <div className="home-admin-text">
                  <p>Admin user {account.login} for</p>
                </div>
                <br></br>
                <div>
                  <img className="body-img" src="content/images/aal-program-logo-full.png" alt="aal-program-logo-full" />
                </div>
                <div className="home-admin-text">
                  <p>research project</p>
                </div>
                <br></br>
                <div>
                  <img className="body-img" src="content/images/vinci-logo-full.png" alt="vinci-logo-full" />
                </div>
                <div className="home-admin-text">
                  <p>by</p>
                </div>
                <br></br>
                <div>
                  <img className="body-img" src="content/images/comtrade-logo-full.png" alt="comtrade-logo-full" />
                </div>
              </div>
            )}
          </div>
        ) : (
          !isAuthenticated && (
              <div className="home-admin">
                <div className="home-admin-header">
                  <h1 className="welcome-text">vINCI Dashboard</h1>
                  <div>
                    <img className="comtrade-logo" src="content/images/comtrade-logo-full.png" alt="comtrade-logo-full" />
                  </div>
                </div>
                <br></br>
                <div className="welcome-text">
                  <p>Integrated and validated evidence-based IoT framework delivering non-intrusive monitoring and supporting elderly persons (clients) to augment professional health caregiving. It incorporates proven open-data analytics with innovative user-driven IoT devices to collect clients’ data. Collected data are used to automatically learn personal preferences, like daily walking routine and deliver assistive responses, such as the watch showing a sad smiley face to indicate the client to consult a doctor when the vINCI service detects signs of not doing well.</p>
                  <p>The vINCI Dashboard allows caregivers appropriate insight into the client’s health status and provides innovative care for elderly persons at outpatient clinics and outdoors.</p>
                  <div className="welcome-buttons">
                  <div className="welcome-button">
                    <a href="#/welcome" >
                      <Button color="success">Welcome</Button>
                    </a>
                  </div>
                  <div className="welcome-button">
                    <a href="#/login" className="welcome-button">
                      <Button color="success">Login</Button>
                    </a>
                  </div>
                  <div className="welcome-button">
                    <a href="#/register" className="welcome-button">
                      <Button color="success">Register</Button>
                    </a>
                  </div>
                </div>
                <hr className="home-horizontal-line"/>
                  <p>It is the beta testing version of the vINCI service.</p>
                  <p>Send an email to vinci.aal@comtrade.com to participate the beta testing.</p>
                </div>
              </div>
          )
        )}
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  isPatient: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.PACIENT]),
  isFamily: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.FAMILY]),
  isOrganization: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.ORGANIZATION]),
  isAdmin: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.ADMIN]),
  associatedUsers: storeState.userUtils.associatedUsers,
  devices: storeState.userUtils.devices,
  cameraMovement: storeState.userUtils.cameraMovement,
  cameraRecognition: storeState.userUtils.cameraRecognition,
  currentLocale: storeState.locale.currentLocale
});

const mapDispatchToProps = { getSession, handleAssociatePatient, handleGeneratePatient, getAssociatedPatients, getUserDevices, setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
