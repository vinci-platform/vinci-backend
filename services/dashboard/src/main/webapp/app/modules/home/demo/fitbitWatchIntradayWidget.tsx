import React, { Fragment } from 'react';
import '../home.scss';
import { translate, Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Avatar, Badge, Card, CardActions, CardContent, CardHeader, Collapse, IconButton, withStyles } from '@material-ui/core';
import purple from '@material-ui/core/colors/purple';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import { getSession } from 'app/shared/reducers/authentication';
import { getEntitiesForDeviceAndDay, reset } from 'app/entities/ioserver/fitbit-watch-intraday-data/fitbit-watch-intraday-data.reducer';
import UserAlertNotificationModal from './userAlertNotificationsModal';
import { IRootState } from 'app/shared/reducers';
import {DeviceType, IDevice} from 'app/shared/model/device.model';
import { BarChart, Legend, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { IUser } from 'app/shared/model/user.model';
import { formatTime } from 'app/shared/util/date-utils';
import DeviceAlertNotificationModal from "app/modules/home/demo/deviceAlertNotificationsModal";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: purple[500]
  },
  notification: {
    outline: 'none !important'
  },
  traverseDays: {
    display: 'flex',
    gap: '0.5rem',
    justifyContent: 'center'
  },
  noDataDay: {
    display: 'flex',
    justifyContent: 'center'
  }
});

const addMissingMinutes = (date, start, end, data) => {
  while (start !== end) {
    const splitted = start.split(':');
    const minutes = Number(splitted[1]);
    const hours = Number(splitted[0]);
    start = minutes < 59 ? `${hours}:${minutes + 1}:00` : `${hours + 1}:00:00`;
    data.push({
      calories: 0,
      heart: null,
      steps: 0,
      timestamp: `${date} ${start}`
    });
  }
};

export interface IIntradayData {
  timestamp: string;
  heart: number;
  steps: number;
  calories: number;
}

export interface IHomeProp extends StateProps, DispatchProps, IFitbitWatchIntradayWidgetProps {}

interface IFitbitWatchIntradayWidgetProps {
  readonly classes: any;
  readonly user: IUser;
  readonly devices?: any;
}

export interface IFitbitWatchIntradayWidgetState {
  expanded: boolean;
  showModal: boolean;
  currentTimestampIndex: number;
  fitbitWatchIntradayDataMap: Map<string, IIntradayData[]>;
}

class FitbitWatchIntradayWidget extends React.Component<IHomeProp, IFitbitWatchIntradayWidgetState> {
  private dataTimer = null;
  private timestamps = [];
  private startTimestamp = 0;
  private endTimestamp = 0;
  private device;

  readonly state: IFitbitWatchIntradayWidgetState = {
    expanded: false,
    showModal: false,
    currentTimestampIndex: 0,
    fitbitWatchIntradayDataMap: new Map()
  };

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };
  getAlertsFromUserDevices(userDevices: ReadonlyArray<IDevice>, deviceType: DeviceType) {
    console.log(userDevices)
    let iDevice = userDevices.find(value => value.deviceType === deviceType);
    if (iDevice.alerts === undefined)return [];
    return iDevice.alerts;
  }
  mapFitbitWatchIntradayDataToDays = () => {
    const daysMap = this.state.fitbitWatchIntradayDataMap;
    let data = [];
    this.props.fitbitWatchIntradayData.entities.forEach(item => {
      data = JSON.parse(item.data).map(value => ({
        timestamp: formatTime(value.timestamp) || 0,
        heart: value.heart || 0,
        steps: value.steps || 0,
        calories: value.calories || 0
      }));

      if (data.length !== 0) {
        if (data[0].timestamp.split(' ')[1] !== '00:00:00') {
          const timing = data[0].timestamp.split(' ');
          addMissingMinutes(timing[0], '00:00:00', timing[1], data);
        }
        if (data[data.length - 1].timestamp.split(' ')[1] !== '23:59:00') {
          const timing = data[data.length - 1].timestamp.split(' ');
          addMissingMinutes(timing[0], timing[1], '23:59:00', data);
        }
      }
    });

    daysMap.set(formatTime(this.startTimestamp).split(' ')[0], data);

    this.timestamps = Array.from(daysMap.keys());
    this.setState({ fitbitWatchIntradayDataMap: daysMap });
  };

  componentDidMount() {
    if (
      typeof this.props.devices !== 'undefined' &&
      this.props.devices !== null &&
      this.props.devices.filter(device => device.deviceType === DeviceType.FITBIT_WATCH).length > 0
    ) {
      this.device = this.props.devices.find(device => device.deviceType === DeviceType.FITBIT_WATCH).id;

      const today = new Date();
      this.startTimestamp = Date.UTC(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), 0, 0);
      this.endTimestamp = Date.UTC(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), 23, 59);

      this.getFitbitWatchDataForDay(this.startTimestamp, this.endTimestamp).then(() => {
        this.mapFitbitWatchIntradayDataToDays();
      });

      const start = new Date();
      start.setHours(0, 0, 0, 0);
      this.dataTimer = setInterval(
        () => {
          this.getFitbitWatchDataForDay(this.startTimestamp, this.endTimestamp).then(() => {
            this.mapFitbitWatchIntradayDataToDays();
          });
        },
        300000 // update every 5 min
      );
    }
  }

  componentWillUnmount() {
    if (this.dataTimer !== null) {
      clearInterval(this.dataTimer);
    }
    this.props.reset();
  }

  getFitbitWatchDataForDay = (startTimestamp: number, endTimestamp: number) => {
    return this.props.getEntitiesForDeviceAndDay(this.device, startTimestamp, endTimestamp, 'timestamp,desc') as any;
  };

  goToPreviousDay = () => {
    this.startTimestamp -= 86400000;
    this.endTimestamp -= 86400000;

    if (!this.state.fitbitWatchIntradayDataMap.get(formatTime(this.startTimestamp).split(' ')[0])) {
      this.getFitbitWatchDataForDay(this.startTimestamp, this.endTimestamp).then(() => {
        this.mapFitbitWatchIntradayDataToDays();
        this.setState({ currentTimestampIndex: this.state.currentTimestampIndex + 1 });
      });
    } else {
      this.setState({ currentTimestampIndex: this.state.currentTimestampIndex + 1 });
    }
  };

  goToNextDay = () => {
    if (new Date(this.startTimestamp + 86400000) <= new Date()) {
      this.startTimestamp += 86400000;
      this.endTimestamp += 86400000;

      if (!this.state.fitbitWatchIntradayDataMap.get(formatTime(this.startTimestamp).split(' ')[0])) {
        this.getFitbitWatchDataForDay(this.startTimestamp, this.endTimestamp).then(() => {
          this.mapFitbitWatchIntradayDataToDays();
          this.setState({ currentTimestampIndex: this.state.currentTimestampIndex - 1 });
        });
      } else {
        this.setState({ currentTimestampIndex: this.state.currentTimestampIndex - 1 });
      }
    }
  };

  labelFormatter = value => formatTime(value);

  render() {
    const { classes, devices } = this.props;
    const { showModal, currentTimestampIndex, fitbitWatchIntradayDataMap } = this.state;

    const disabledNextBtn = new Date(this.startTimestamp + 86400000) > new Date();

    return (
      <Card className="col-sm-5 col-md-5 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Watch" className={classes.avatar}>
              W
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red' }} />
              </Badge>
            </IconButton>
          }
          title={<Translate contentKey="global.devices.fitbitWatch">Fitbit Watch </Translate>}
          subheader={<Translate contentKey="global.devices.fitbitWatchInfo">Your Fitbit watch information </Translate>}
        />

        {fitbitWatchIntradayDataMap.size > 0 &&
          fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex]) &&
          fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex]).length > 0 && (
            <Fragment>
              <CardContent>
                <div style={{ width: '100%', height: 300 }}>
                  <ResponsiveContainer>
                    <BarChart
                      width={600}
                      height={300}
                      data={fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex])}
                      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                      <YAxis />
                      <Tooltip labelFormatter={this.labelFormatter} />
                      <Legend />
                      <Bar dataKey="heart" name="Heart Rate" fill="#8884d8" />
                    </BarChart>
                  </ResponsiveContainer>
                </div>
              </CardContent>
            </Fragment>
          )}

        {fitbitWatchIntradayDataMap.size > 0 &&
          fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex]) && (
            <div className={classes.traverseDays}>
              <IconButton
                style={{ outline: 'none' }}
                onClick={this.goToPreviousDay}
                title={translate('gatewayApp.ioserverFitbitWatchIntradayData.previousDay')}
              >
                <ArrowLeftIcon color={'primary'} />
              </IconButton>
              <IconButton
                style={{ outline: 'none' }}
                onClick={this.goToNextDay}
                disabled={disabledNextBtn}
                title={translate('gatewayApp.ioserverFitbitWatchIntradayData.nextDay')}
              >
                <ArrowRightIcon color={!disabledNextBtn ? 'primary' : 'disabled'} />
              </IconButton>
            </div>
          )}

        {fitbitWatchIntradayDataMap.size > 0 &&
          fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex]) &&
          fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex]).length === 0 && (
            <div className={classes.noDataDay}>
              <Translate
                contentKey="gatewayApp.ioserverFitbitWatchIntradayData.noDataHeartRate"
                interpolate={{ date: this.timestamps[currentTimestampIndex] }}
              >
                No heart rate data for date {this.timestamps[currentTimestampIndex]}
              </Translate>
            </div>
          )}
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <div style={{ width: '100%', height: 300 }}>
              <ResponsiveContainer>
                <BarChart
                  width={600}
                  height={200}
                  data={fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex])}
                  margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                  <YAxis />
                  <Tooltip labelFormatter={this.labelFormatter} />
                  <Legend />
                  <Bar dataKey="steps" name="Steps" fill="#85C1E9" />
                </BarChart>
              </ResponsiveContainer>
            </div>
            <div style={{ width: '100%', height: 300 }}>
              <ResponsiveContainer>
                <BarChart
                  width={600}
                  height={200}
                  data={fitbitWatchIntradayDataMap.get(this.timestamps[currentTimestampIndex])}
                  margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                  <YAxis />
                  <Tooltip labelFormatter={this.labelFormatter} />
                  <Legend />
                  <Bar dataKey="calories" name="Calories" fill="#48C9B0 " />
                </BarChart>
              </ResponsiveContainer>
            </div>
          </CardContent>
        </Collapse>
        {/*<UserAlertNotificationModal showModal={showModal} handleClose={this.handleClose} userAlerts={[]} />*/}
        <DeviceAlertNotificationModal showModal={showModal} handleClose={this.handleClose} alerts={this.getAlertsFromUserDevices(devices,DeviceType.FITBIT_WATCH)} />
      </Card>
    );
  }
}

const mapStateToProps = ({ fitbitWatchIntradayData }: IRootState) =>{
    return {
        fitbitWatchIntradayData: fitbitWatchIntradayData,
    };
}

const mapDispatchToProps = { getSession, getEntitiesForDeviceAndDay, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FitbitWatchIntradayWidget)
);
