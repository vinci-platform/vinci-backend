import React from 'react';
import '../home.scss';
import {Translate} from 'react-jhipster';
import {connect} from 'react-redux';
import {
    Avatar,
    Badge,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Collapse,
    IconButton,
    Typography,
    withStyles
} from '@material-ui/core';
import purple from '@material-ui/core/colors/purple';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import {getSession} from 'app/shared/reducers/authentication';
import {getEntitiesFor, reset} from 'app/entities/ioserver/fitbit-watch-data/fitbit-watch-data.reducer';
import History from './history';
import {IRootState} from 'app/shared/reducers';
import {DeviceType, IDevice} from 'app/shared/model/device.model';
import {Button} from 'reactstrap';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import moment from 'moment';
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis} from 'recharts';
import {AUTHORITIES, interval} from 'app/config/constants';
import axios from 'axios';
import {getEntity, getMyEntities, updateEntity} from 'app/entities/device/device.reducer';
import {getAdditionToTimestamp} from 'app/entities/utils';
import {addDays, formatTime} from 'app/shared/util/date-utils';
import {toast} from 'react-toastify';
import {hasAnyAuthority} from 'app/shared/auth/private-route';
import DeviceAlertNotificationModal from "app/modules/home/demo/deviceAlertNotificationsModal";
// import DeviceAlertNotificationModal from "app/modules/home/demo/deviceAlertNotificationsModal";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: purple[500]
  },
  notification: {
    outline: 'none !important'
  },
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, IFitbitWatchWidgetProps {}

interface IFitbitWatchWidgetProps {
  readonly classes: any;
  readonly devices?: any;
}

export interface IFitbitWatchWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
  historyInterval: string;
  fromDate: string;
  toDate: string;
  userExtraId: number;
  active: boolean;
  description: string;
}

const fitBitDisconnectedDescription = 'Not Connected to Fitbit watch! Device deactivated through button on dashboard.';

class FitbitWatchWidget extends React.Component<IHomeProp, IFitbitWatchWidgetState> {
  private dataTimer = null;
  private fitbitConnectionInterval = null;

  readonly state: IFitbitWatchWidgetState = {
    expanded: true,
    showModal: false,
    isHistoryOpen: false,
    historyInterval: interval.ONE_WEEK,
    fromDate: '',
    toDate: '',
    userExtraId: null,
    active: null,
    description: ''
  };

  handleExpandClick = () => {
    if (!this.state.expanded) {
      const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
      const fitbitWatch = this.props.devices.find(device => device.deviceType === DeviceType.FITBIT_WATCH);
      this.props.getEntitiesFor(fitbitWatch.id, Date.now() - timeless);
    }

    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  onToggleHistoryOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  setUserExtraId = (id: number) => {
    this.setState({ userExtraId: id });
  };

  setActive = (active: boolean) => {
    this.setState({ active });
  };

  setDescription = (description: string) => {
    this.setState({ description });
  };

  componentDidMount() {
    const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
    let fitbitDevices = this.props.devices.filter(device => device.deviceType === DeviceType.FITBIT_WATCH);
    if (typeof this.props.devices !== 'undefined' && this.props.devices !== null && fitbitDevices.length > 0) {
      const fitbitWatch = fitbitDevices.find(device => device.deviceType === DeviceType.FITBIT_WATCH);
      this.setUserExtraId(fitbitWatch.userExtraId);
      this.setActive(fitbitWatch.active);
      this.setDescription(fitbitWatch.description);
      this.props.getEntitiesFor(fitbitWatch.id, Date.now() - timeless);
      const start = new Date();
      start.setHours(0, 0, 0, 0);
      this.dataTimer = setInterval(
        () => {
          const timelesss: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
          this.props.getEntitiesFor(fitbitWatch.id, Date.now() - timelesss);
        },
        30000 // update every 5 min
      );
      this.checkFitbitConnection(fitbitWatch.id);
    }
  }

  componentWillUnmount() {
    if (this.dataTimer !== null) {
      clearInterval(this.dataTimer);
    }
    if (this.fitbitConnectionInterval !== null) {
      clearInterval(this.fitbitConnectionInterval);
    }
    this.props.reset();
  }

  handleHistory = val => {
    this.setState({ historyInterval: val });
  };

  handleCustomHistory = val => {
    this.setState({ fromDate: val.fromDate, toDate: val.toDate });
  };

  filterFn = l => {
    switch (this.state.historyInterval) {
      case interval.ONE_DAY:
        return this.checkDateByDayInterval(l.timestamp, 1);
      case interval.ONE_WEEK:
        return this.checkDateByDayInterval(l.timestamp, 7);
      case interval.TWO_WEEKS:
        return this.checkDateByDayInterval(l.timestamp, 14);
      case interval.THREE_WEEKS:
        return this.checkDateByDayInterval(l.timestamp, 21);
      case interval.ONE_MONTH:
        return this.checkDateByDayInterval(l.timestamp, 30);
      case interval.THREE_MONTHS:
        return this.checkDateByDayInterval(l.timestamp, 90);
      case interval.CUSTOM: {
        const [fromDate] = this.state.fromDate.split('T');
        const [toDate] = this.state.toDate.split('T');

        return fromDate !== '' && toDate !== ''
          ? new Date(l.timestamp) >= new Date(fromDate) && new Date(l.timestamp) <= new Date(toDate)
          : fromDate !== ''
            ? new Date(l.timestamp) >= new Date(fromDate)
            : toDate !== ''
              ? new Date(l.timestamp) <= new Date(toDate)
              : true;
      }
      default:
        return true;
    }
  };

  checkDateByDayInterval = (date: string, daysAgo: number): boolean => {
    const today = new Date();
    return new Date(Date.UTC(today.getFullYear(), today.getMonth(), today.getDate() - daysAgo)) <= new Date(date);
  };

  labelFormatter = value => formatTime(value);

  fitbitDisconnect = async () => {
    if (this.state.userExtraId !== null) {
      const fitbitWatch = JSON.parse(JSON.stringify(this.props.devices.find(device => device.deviceType === DeviceType.FITBIT_WATCH)));
      fitbitWatch.active = false;
      fitbitWatch.description = fitBitDisconnectedDescription;
      const { value } = (await this.props.updateEntity(fitbitWatch)) as any;
      if (value.status === 200) {
        this.setActive(fitbitWatch.active);
        this.setDescription(fitbitWatch.description);
      } else {
        toast.error('Request failed: cannot disconnect from Fitbit watch');
      }
    } else {
      toast.error('Cannot connect to Fitbit - userExtraId does not exist!');
    }
  };

  private getAlertsFromUserDevices(userDevices: ReadonlyArray<IDevice>, deviceType: DeviceType) {
      console.log(userDevices)
      let iDevice = userDevices.find(value => value.deviceType === deviceType);
      if (iDevice.alerts === undefined)return [];
      return iDevice.alerts;
  }

  fitbitConnect = async () => {
    if (this.state.userExtraId !== null) {
      const requestUrl = `/api/fitbit-auth/authorize-mobile?userExtraId=${this.state.userExtraId}`;
      const res = await axios.get(requestUrl);
      if (res.data) {
        window.open(res.data.url);
      }
    } else {
      toast.error('Cannot connect to Fitbit - userExtraId does not exist!');
    }
  };

  handleFitbitConnection = async () => {
    if (this.state.active) {
      await this.fitbitDisconnect();
    } else {
      await this.fitbitConnect();
    }
  };

  checkFitbitConnection(id: number) {
    this.fitbitConnectionInterval = setInterval(async () => {
      const { value } = (await this.props.getEntity(id)) as any;
      if (value.data) {
        this.setActive(value.data.active);
        this.setDescription(value.data.description);
      }
    }, 60000); // check every min
  }

  render() {
    const { classes, fitbitWatchDataList, fitbitWatchActivityData, isPatient,devices  } = this.props;
    const { showModal, isHistoryOpen, active, description } = this.state;

    return (
      <Card className="col-sm-5 col-md-5 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Watch" className={classes.avatar}>
              W
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red' }} />
              </Badge>
            </IconButton>
          }
          title={<Translate contentKey="global.devices.fitbitWatch">Fitbit Watch </Translate>}
          subheader={<Translate contentKey="global.devices.fitbitWatchInfo">Your Fitbit watch information </Translate>}
        />
        <CardContent>
          {isPatient && <Button
            color="primary"
            className="btn btn-primary float-right"
            onClick={this.handleFitbitConnection}
            title={!active ? description : ''}
            style={{ color: '#FFF' }}
          >
            <span className="d-none d-md-inline">{active ? 'Disconnect from Fitbit' : 'Connect to Fitbit'}</span>
          </Button>}
          {fitbitWatchDataList.length === 1 && (
            <Typography paragraph>
              <Translate contentKey="global.devices.fitbitWatchSleep">Sleep information</Translate>
            </Typography>
          )}
          {fitbitWatchDataList.map((data, key) => {
            if (data.sleepData.sleep.length === 0) {
              return (
                <Typography paragraph key={key + 1}>
                  <Translate contentKey="global.devices.fitbitWatchSleepNoData">No Sleep measurements</Translate>
                </Typography>
              );
            }
            const sleephours = Math.floor(data.sleepData.sleep[0].minutesAsleep / 60);
            const sleepminutes = data.sleepData.sleep[0].minutesAsleep % 60;
            const sleep = sleephours + 'h ' + sleepminutes + 'min';
            let rem = '';
            let light = '';
            let deep = '';
            let wake = '';
            if (data.sleepData.summary.stages !== undefined) {
              const remhours = Math.floor(data.sleepData.summary.stages.rem / 60);
              const remminutes = data.sleepData.summary.stages.rem % 60;
              rem = remhours + 'h ' + remminutes + 'min';
              const lighthours = Math.floor(data.sleepData.summary.stages.light / 60);
              const lightminutes = data.sleepData.summary.stages.light % 60;
              light = lighthours + 'h ' + lightminutes + 'min';
              const wakehours = Math.floor(data.sleepData.summary.stages.wake / 60);
              const wakeminutes = data.sleepData.summary.stages.wake % 60;
              wake = wakehours + 'h ' + wakeminutes + 'min';
              const deephours = Math.floor(data.sleepData.summary.stages.deep / 60);
              const deepminutes = data.sleepData.summary.stages.deep % 60;
              deep = deephours + 'h ' + deepminutes + 'min';
            }
            const startTime = moment(data.sleepData.sleep[0].startTime).format('YYYY-MM-DD HH:mm');
            const endTime = moment(data.sleepData.sleep[0].endTime).format('YYYY-MM-DD HH:mm');
            return (
              <List className={classes.root} key={key}>
                <ListItem className={classes.container}>
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="grinning face" id="grinning face">
                        😴
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '50%' }} primary="Duration" secondary={sleep} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="grinning face" id="grinning face">
                        💯
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '50%' }} primary="Score" secondary={data.sleepData.sleep[0].efficiency} />
                </ListItem>
                <ListItem className={classes.container}>
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Bedtime" id="Bedtime">
                        🌜
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '50%' }} primary="Bedtime" secondary={startTime} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Wakeup" id="Wakeup">
                        🌞
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '50%' }} primary="Wakeup" secondary={endTime} />
                </ListItem>
                {rem !== '' &&
                  light !== '' &&
                  wake !== '' &&
                  deep !== '' && (
                    <ListItem className={classes.container}>
                      <ListItemAvatar>
                        <Avatar>
                          <span role="img" aria-label="Deep" id="Deep">
                            💤
                          </span>
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText style={{ width: '25%' }} primary="Deep" secondary={deep} />
                      <ListItemAvatar>
                        <Avatar>
                          <span role="img" aria-label="Wake" id="Wake">
                            💤
                          </span>
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText style={{ width: '25%' }} primary="Wake" secondary={wake} />
                      <ListItemAvatar>
                        <Avatar>
                          <span role="img" aria-label="Light" id="Light">
                            💤
                          </span>
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText style={{ width: '25%' }} primary="Light" secondary={light} />
                      <ListItemAvatar>
                        <Avatar>
                          <span role="img" aria-label="Rem" id="Rem">
                            💤
                          </span>
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText style={{ width: '25%' }} primary="Rem" secondary={rem} />
                    </ListItem>
                  )}
              </List>
            );
          })}
          {fitbitWatchDataList.length === 1 && (
            <Typography paragraph>
              <Translate contentKey="global.devices.fitbitWatchActivity">Activity information</Translate>
            </Typography>
          )}
          {fitbitWatchDataList.map((data, key) => {
            if (data.activityData === undefined) {
              return (
                <Typography paragraph key={key + 1}>
                  <Translate contentKey="global.devices.fitbitWatchActivityNoData">No Activity measurements</Translate>
                </Typography>
              );
            }
            const distance = data.activityData.distances[0].distance + ' km';
            const heart = data.activityData.restingHeartRate + ' bpm';
            return (
              <List className={classes.root} key={key}>
                <ListItem className={classes.container}>
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Calories" id="Calories">
                        🔥
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Calories" secondary={data.activityData.activityCalories} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Steps" id="Steps">
                        👞
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Steps" secondary={data.activityData.steps} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Heart" id="Heart">
                        ❤
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Heart" secondary={heart} />
                </ListItem>
                <ListItem className={classes.container}>
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Floors" id="Floors">
                        🪜
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Floors" secondary={data.activityData.floors} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Distance" id="Distance">
                        ⬆️
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Distance" secondary={distance} />
                  <ListItemAvatar>
                    <Avatar>
                      <span role="img" aria-label="Elevation" id="Elevation">
                        🛗
                      </span>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText style={{ width: '33%' }} primary="Elevation" secondary={data.activityData.elevation} />
                </ListItem>
              </List>
            );
          })}
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton aria-label="History data" onClick={this.onToggleHistoryOpen} className={classes.expandHistory}>
            <HistoryIcon />
          </IconButton>
          {isHistoryOpen && (
            <History
              onSelectInterval={this.handleHistory}
              handleCustomInterval={this.handleCustomHistory}
              interval={this.state.historyInterval}
            />
          )}
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          {fitbitWatchActivityData.length > 0 && (
            <CardContent>
              <div style={{ width: '100%', height: 200 }}>
                <ResponsiveContainer>
                  <BarChart
                    width={600}
                    height={200}
                    data={fitbitWatchActivityData.filter(this.filterFn)}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                    <YAxis />
                    <Tooltip labelFormatter={this.labelFormatter} />
                    <Legend />
                    <Bar dataKey="heart" name="Heart Rate" fill="#8884d8" />
                  </BarChart>
                </ResponsiveContainer>
              </div>
              <div style={{ width: '100%', height: 200 }}>
                <ResponsiveContainer>
                  <BarChart
                    width={600}
                    height={200}
                    data={fitbitWatchActivityData.filter(this.filterFn)}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                    <YAxis />
                    <Tooltip labelFormatter={this.labelFormatter} />
                    <Legend />
                    <Bar dataKey="steps" name="Steps" fill="#85C1E9" />
                  </BarChart>
                </ResponsiveContainer>
              </div>
              <div style={{ width: '100%', height: 200 }}>
                <ResponsiveContainer>
                  <BarChart
                    width={600}
                    height={200}
                    data={fitbitWatchActivityData.filter(this.filterFn)}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                    <YAxis />
                    <Tooltip labelFormatter={this.labelFormatter} />
                    <Legend />
                    <Bar dataKey="calories" name="Calories" fill="#48C9B0 " />
                  </BarChart>
                </ResponsiveContainer>
              </div>
            </CardContent>
          )}
        </Collapse>
        <DeviceAlertNotificationModal showModal={showModal} handleClose={this.handleClose} alerts={this.getAlertsFromUserDevices(devices,DeviceType.FITBIT_WATCH)} />
      </Card>
    );
  }
}

const mapStateToProps = ({ fitbitWatchData, authentication }: IRootState) => {
  const start = new Date();
  start.setHours(0, 0, 0, 0);
  const result = fitbitWatchData.entities.reduce((r, a) => {
    r[formatTime(a.timestamp, 'YYYY-MM-DD')] = r[formatTime(a.timestamp, 'YYYY-MM-DD')] || 0;
    r[formatTime(a.timestamp, 'YYYY-MM-DD')] = a.activityData;
    return r;
  }, Object.create(null));

  const fitbitWatchDataListResult = fitbitWatchData.entities
    .filter(p => p.timestamp === start.getTime())
    .sort((a, b) => a.timestamp - b.timestamp);

  const fitbitWatchActivityData = Object.keys(result)
    .map(key => {
      return { timestamp: key, heart: result[key].restingHeartRate, steps: result[key].steps, calories: result[key].activityCalories };
    })
    .sort((a, b) => (new Date(a.timestamp) < new Date(b.timestamp) ? -1 : 1));

  // if there are missing days
  if (
    fitbitWatchActivityData.length &&
    fitbitWatchActivityData[fitbitWatchActivityData.length - 1].timestamp !== formatTime(new Date().getTime(), 'YYYY-MM-DD')
  ) {
    let currentDate = new Date(fitbitWatchActivityData[fitbitWatchActivityData.length - 1].timestamp);
    const endDate = new Date();

    while (currentDate <= endDate) {
      fitbitWatchActivityData.push({
        timestamp: formatTime(currentDate, 'YYYY-MM-DD'),
        heart: undefined,
        steps: 0,
        calories: 0
      });
      currentDate = addDays(currentDate, 1);
    }
  }
  return {
    fitbitWatchDataList: fitbitWatchDataListResult,
    fitbitWatchActivityData,
    isPatient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT]),
  };
};

const mapDispatchToProps = { getSession, getEntitiesFor, getEntity, updateEntity, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FitbitWatchWidget)
);
