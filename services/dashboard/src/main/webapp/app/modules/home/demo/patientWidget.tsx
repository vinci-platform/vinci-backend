import React from 'react';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Avatar, Badge, Card, CardActions, CardContent, CardHeader, IconButton, withStyles } from '@material-ui/core';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import AssignmentIcon from '@material-ui/icons/AssignmentInd';
import { getSession } from 'app/shared/reducers/authentication';
import UserAlertNotificationModal from 'app/modules/home/demo/userAlertNotificationsModal';
import History from './history';
import { Alert } from 'reactstrap';
import { IUserAggregated } from 'app/shared/model/user-aggregated.model';
import { updateEntities, getEntities } from 'app/entities/user-alert/user-alert.reducer';
import { Link } from 'react-router-dom';

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    marginLeft: 'auto',
    outline: 'none !important'
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: '#3f8cd1'
  },
  notification: {
    outline: 'none !important'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, IPatientWidgetProps {}

interface IPatientWidgetProps {
  readonly classes: any;
  user: IUserAggregated;
}

export interface IPatientWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
}

class PatientWidget extends React.Component<IHomeProp, IPatientWidgetState> {
  readonly state: IPatientWidgetState = { expanded: false, showModal: false, isHistoryOpen: false };

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
    const alerts = this.props.user.userInfo.alerts.map(alert => {
      if (!alert.familyRead) {
        alert.familyRead = true;
      }
      alert.userExtraId = this.props.user.userInfo.id;
      return alert;
    });

    let alertsToSend = alerts.map((alert => {//quick fix so deviceId is not sent
      return {
        alertType: alert.alertType,
        createdDate: alert.createdDate,
        familyRead: alert.familyRead,
        id: alert.id,
        label: alert.label,
        organizationRead: alert.organizationRead,
        userExtraId: alert.userExtraId,
        userRead: alert.userRead,
        values: alert.values
      };
    }));
    this.props.updateEntities(alertsToSend);
  }

  onToggleUserDashboardOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  render() {
    const { classes, user } = this.props;
    const { showModal, isHistoryOpen } = this.state;


    return (
      <Card className="col-sm-6 col-md-8 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="User" className={classes.avatar}>
              P
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge
                className={classes.margin}
                badgeContent={user.userInfo.alerts.filter(alert => !alert.familyRead).length}
                color="primary"
              >
                <AnnouncementIcon style={{ color: 'red', outline: 'none !important' }} />
              </Badge>
            </IconButton>
          }
          title={`${user.userInfo.userFirstName} ${user.userInfo.userLastName}`}
          subheader={<Translate contentKey="global.patient.patientState">Patient's state </Translate>}
        />
        <CardContent className="d-flex flex-wrap">
          <div className="col-md-2 col-sm-10" style={{ marginBottom: '20px' }}>
            <img src="content/images/patientCard.png" alt="patient" height="100px" width="100px" />
          </div>
          <div className="col-md-8 col-sm-10">
            <Alert>
              <div>
                <span>PULSE</span>: {user.pulse}
              </div>
            </Alert>
            <Alert>
              <div>
                <span>HEART RATE</span>: {user.heartRate}
              </div>
            </Alert>
            <Alert>
              <div>
                <span>MOOD</span>: {user.mood}
              </div>
            </Alert>
            <Alert>
              <div>
                <span>FITNESS</span>: {user.fitness}
              </div>
            </Alert>
          </div>
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          <Link to={`/${user.userInfo.login}/patient`}>
            <IconButton aria-label="User dashboard" className={classes.expandHistory}>
              <AssignmentIcon />
            </IconButton>
          </Link>
          {isHistoryOpen && <History />}
        </CardActions>
        <UserAlertNotificationModal showModal={showModal} handleClose={this.handleClose} userAlerts={user.userInfo.alerts} />
      </Card>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, updateEntities, getEntities };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PatientWidget)
);
