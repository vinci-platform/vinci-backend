import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
// tslint:disable-next-line:no-unused-variable
import { Translate, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Tooltip from '@material-ui/core/Tooltip';
import { IRootState } from 'app/shared/reducers';
import { withStyles } from '@material-ui/core/styles';
import { AlertType } from 'app/shared/model/user-alert.model';
import { IUserAggregated } from 'app/shared/model/user-aggregated.model';
import { UserAlertsDetails } from 'app/entities/user-extra/user-alerts-details';
import { getAssociatedPatients } from 'app/shared/reducers/user-utils';
import { updateEntities } from 'app/entities/user-alert/user-alert.reducer';
import {hasAnyAuthority} from "app/shared/auth/private-route";
import {AUTHORITIES} from "app/config/constants";
// tslint:disable-next-line:no-unused-variable

export interface IUserExtraProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
  readonly users: IUserAggregated[];
}

export interface IUserExtraState extends IPaginationBaseState {
  filter: string;
  showModal: boolean;
  userEntity: any;
}

export class UserExtra extends React.Component<IUserExtraProps, IUserExtraState> {
  state: IUserExtraState = {
    filter: '',
    showModal: false,
    userEntity: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  setFilter = evt => {
    this.setState({
      filter: evt.target.value
    });
  };

  filterFn = l =>
    l.userInfo.userFirstName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()) ||
    l.userInfo.userLastName
      .toString()
      .toUpperCase()
      .includes(this.state.filter.toUpperCase()) ||
    l.userInfo.uuid.toString().includes(this.state.filter);

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop === 'userLastName' ? 'user.lastName' : prop
      },
      () => this.sortUserExtra()
    );
  };

  sortUserExtra = () => {
    this.getUserExtra();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  };

  getUserExtra = () => {
    const { sort, order } = this.state;
    this.props.getAssociatedPatients(`${sort},${order}`);
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  openModal = user => {
    this.setState({ showModal: true, userEntity: user });
  };

  render() {
    const RedTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ff0000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const GreenTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#008000',
        fontSize: '1em'
      }
    }))(Tooltip);
    const OrangeTooltip = withStyles(theme => ({
      tooltip: {
        backgroundColor: '#ffc107',
        fontSize: '1em'
      }
    }))(Tooltip);

    const { isFetching, totalItems, users, match, isAdmin } = this.props;
    const { filter } = this.state;
    return (
      <div>
        <h2 id="user-extra-heading" className="d-flex flex-wrap justify-content-between" style={{ marginBottom: '50px' }}>
          <Translate contentKey="gatewayApp.userExtra.home.title">Users</Translate>
          <div className="d-flex flex-wrap col-md-6 justify-content-between">
            <div className="d-flex col-md-8">
              <div style={{ fontSize: '1.2rem', margin: 'auto' }}>
                <Translate contentKey="logs.search">Search</Translate>
              </div>
              <input
                type="search"
                value={filter}
                onChange={this.setFilter}
                disabled={isFetching}
                className="form-control"
                style={{ marginLeft: '10px', width: '100% !important' }}
              />
              <Button type="submit" color="primary">
                <FontAwesomeIcon icon="search" />
              </Button>
            </div>
          </div>
        </h2>

        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th onClick={this.sort('userLastName')}>
                  <Translate contentKey="gatewayApp.userExtra.user">User</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th onClick={this.sort('uuid')}>
                  <Translate contentKey="gatewayApp.userExtra.uuid">Uuid</Translate>
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userExtra.description">Description</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userExtra.nrDevices">No of devices</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.userExtra.gender">Gender</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.device.status">Status</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {users.filter(this.filterFn).map((userExtra, i) => (
                <tr key={`entity-${i}`}>
                  <td>{userExtra.userInfo.id}</td>
                  <td>
                    <Link to={`/${userExtra.userInfo.login}/patient`}>
                      {userExtra.userInfo.userFirstName ? userExtra.userInfo.userFirstName : ''}{' '}
                      {userExtra.userInfo.userLastName ? userExtra.userInfo.userLastName : ''}
                    </Link>
                  </td>
                  <td>{userExtra.userInfo.uuid ? userExtra.userInfo.uuid : ''}</td>
                  <td>{userExtra.userInfo.description ? userExtra.userInfo.description : ''}</td>
                  <td>{userExtra.userInfo.devices ? userExtra.userInfo.devices.length : 0}</td>
                  <td>{userExtra.userInfo.gender ? userExtra.userInfo.gender : ''}</td>
                  <td>
                    <UserAlertsDetails
                      handleClose={this.handleClose}
                      updateEntities={this.props.updateEntities}
                      userType={'organization'}
                      showModal={this.state.showModal}
                      userEntity={this.state.userEntity}
                      history={`${this.props.location.pathname}/${userExtra.userInfo.id}/alerts` as any}
                      location={this.props.location}
                      match={match.url as any}
                    />
                    {userExtra.userInfo.alerts.length !== 0 &&
                      (userExtra.userInfo.alerts
                        .filter(dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id)))[0]
                        .alertType.toString() === AlertType.DANGER ? (
                        <div>
                          <RedTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.userAlert.${
                                  userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].label
                                }`}
                                interpolate={{
                                  value: userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: 'red' }} onClick={this.openModal.bind(this, userExtra.userInfo)}>
                              <div style={{ color: 'red' }}>
                                <Translate contentKey="gatewayApp.device.errorStatus" />
                                {/* todo: translate iznad ce za DANGER uvek ispisati samo ERROR u crvenom zbog transleta koji je los!!! */}
                              </div>
                            </Button>
                          </RedTooltip>
                        </div>
                      ) : userExtra.userInfo.alerts
                        .filter(dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id)))[0]
                        .alertType.toString() === AlertType.WARNING ? (
                        <div>
                          <OrangeTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.userAlert.${
                                  userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].label
                                }`}
                                interpolate={{
                                  value: userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: '#ffa500' }} onClick={this.openModal.bind(this, userExtra.userInfo)}>
                              <div style={{ color: 'orange' }}>
                                <Translate contentKey="gatewayApp.device.errorStatus" />
                                {/* todo: translate iznad ce za DANGER uvek ispisati samo ERROR u crvenom zbog transleta koji je los!!! */}
                              </div>
                            </Button>
                          </OrangeTooltip>
                        </div>
                      ) : (
                        <div>
                          <GreenTooltip
                            title={
                              <Translate
                                contentKey={`gatewayApp.userAlert.${
                                  userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].label
                                }`}
                                interpolate={{
                                  value: userExtra.userInfo.alerts.filter(
                                    dev => dev.id === Math.max.apply(Math, userExtra.userInfo.alerts.map(o => o.id))
                                  )[0].values
                                }}
                              />
                            }
                          >
                            <Button color="link" style={{ color: '#008000' }} onClick={this.openModal.bind(this, userExtra.userInfo)}>
                              <div style={{ color: 'green' }}>
                                <Translate contentKey="gatewayApp.device.okStatus" />
                              </div>
                            </Button>
                          </GreenTooltip>
                        </div>
                      ))}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}entity/user-extra/${userExtra.userInfo.id}`} color="info"
                                 size="sm">
                            <FontAwesomeIcon icon="eye"/>{' '}
                            <span className="d-none d-md-inline">
                                <Translate contentKey="entity.action.view">View</Translate>
                            </span>
                        </Button>
                        {isAdmin &&
                        <Button tag={Link} to={`${match.url}entity/user-extra/${userExtra.userInfo.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>}
                        {isAdmin &&
                        <Button tag={Link} to={`${match.url}entity/user-extra/${userExtra.userInfo.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userExtra, authentication }: IRootState) => ({
  userExtraList: userExtra.entities,
  isFetching: userExtra.loading,
  totalItems: userExtra.totalItems,
  isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = {
  getAssociatedPatients,
  updateEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserExtra);
