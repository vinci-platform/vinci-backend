import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Alert, Row, Col, Marker, Input } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { APP_LOCAL_DATETIME_FORMAT } from 'app/config/constants';
import { Translate } from 'react-jhipster';
import moment from 'moment';

export interface IIntervalModalProps {
  opened: boolean;
  onSelectCustomInterval: (val: any) => any;
}

const previousMonth = (): string => {
  const now: Date = new Date();
  const fromDate =
    now.getMonth() === 0
      ? new Date(now.getFullYear() - 1, 11, now.getDate(), now.getHours(), now.getMinutes())
      : new Date(now.getFullYear(), now.getMonth() - 1, now.getDate(), now.getHours(), now.getMinutes());

  return moment(fromDate).format(APP_LOCAL_DATETIME_FORMAT);
};

const today = (): string => {
  // Today + 1 day - needed if the current day must be included
  const day: Date = new Date();
  day.setDate(day.getDate() + 1);
  const toDate = new Date(day.getFullYear(), day.getMonth(), day.getDate(), day.getHours(), day.getMinutes());
  return moment(toDate).format(APP_LOCAL_DATETIME_FORMAT);
};

export interface IIntervalModalState {
  modalIsOpen: boolean;
  fromDate: string;
  toDate: string;
  onSelectCustomInterval: (val: any) => any;
}

class IntervalModal extends React.Component<IIntervalModalProps, IIntervalModalState> {
  state: IIntervalModalState = {
    modalIsOpen: this.props.opened,
    fromDate: previousMonth(),
    toDate: today(),
    onSelectCustomInterval: val => {}
  };

  constructor(props) {
    super(props);

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  onChangeFromDate = evt => {
    this.setState({ fromDate: evt.target.value });
  };

  onChangeToDate = evt => {
    this.setState({ toDate: evt.target.value });
  };

  closeModal() {
    this.setState({ modalIsOpen: false });
    this.props.onSelectCustomInterval({ fromDate: this.state.fromDate, toDate: this.state.toDate });
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  render() {
    const { fromDate, toDate } = this.state;
    return (
      <Modal isOpen={this.state.modalIsOpen} autoFocus={false}>
        <AvForm>
          <ModalHeader>
            <div style={{ color: '#007bff' }}>Choose start and end datetime</div>
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col md="12">
                <span>
                  <Translate contentKey="audits.filter.from">from</Translate>
                </span>
                <Input type="datetime-local" value={fromDate} onChange={this.onChangeFromDate} />
                <span>
                  <Translate contentKey="audits.filter.to">to</Translate>
                </span>
                <Input type="datetime-local" value={toDate} onChange={this.onChangeToDate} />
              </Col>
            </Row>
            <div className="mt-1">&nbsp;</div>
            <Row>
              <Col md="12">
                <Button onClick={this.closeModal} color="primary">
                  Ok
                </Button>
              </Col>
            </Row>
          </ModalBody>
        </AvForm>
      </Modal>
    );
  }
}

export default IntervalModal;
