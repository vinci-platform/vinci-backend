import React, { Fragment } from 'react';
import '../home.scss';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Avatar, Badge, Card, CardActions, CardContent, CardHeader, Collapse, IconButton, Typography, withStyles } from '@material-ui/core';
import purple from '@material-ui/core/colors/purple';
import classnames from 'classnames';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import { getSession } from 'app/shared/reducers/authentication';
import { getEntitiesFor, reset } from 'app/entities/ioserver/shoe-data/shoe-data.reducer';
import UserAlertNotificationModal from './userAlertNotificationsModal';
import History from './history';
import { IRootState } from 'app/shared/reducers';
import {DeviceType, IDevice} from 'app/shared/model/device.model';
import { ActivityType } from 'app/shared/model/ioserver/shoe-data-payload-model';
import { BarChart, Legend, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { interval } from 'app/config/constants';
import { IShoeDataPayload } from 'app/shared/model/ioserver/shoe-data-payload-model';
import { getAdditionToTimestamp } from 'app/entities/utils';
import { formatTime } from 'app/shared/util/date-utils';
import DeviceAlertNotificationModal from "app/modules/home/demo/deviceAlertNotificationsModal";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandHistory: {
    transform: 'rotate(0deg)',
    outline: 'none !important',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: purple[500]
  },
  notification: {
    outline: 'none !important'
  }
});

export interface IHomeProp extends StateProps, DispatchProps, IShoeWidgetProps {}

interface IShoeWidgetProps {
  readonly classes: any;
  readonly devices?: any;
}

export interface IShoeWidgetState {
  expanded: boolean;
  showModal: boolean;
  isHistoryOpen: boolean;
  historyInterval: string;
  fromDate: string;
  toDate: string;
}

class ShoeWidget extends React.Component<IHomeProp, IShoeWidgetState> {
  dataTimer = null;

  readonly state: IShoeWidgetState = {
    expanded: false,
    showModal: false,
    isHistoryOpen: false,
    historyInterval: interval.THREE_MONTHS,
    fromDate: '',
    toDate: ''
  };

  handleExpandClick = () => {
    this.setState({
      expanded: !this.state.expanded,
      isHistoryOpen: false
    });
  };

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  onToggleHistoryOpen = () => {
    this.setState({
      isHistoryOpen: !this.state.isHistoryOpen,
      expanded: false
    });
  };

  componentDidMount() {
    const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
    if (
      typeof this.props.devices !== 'undefined' &&
      this.props.devices !== null &&
      this.props.devices.filter(device => device.deviceType === DeviceType.SHOE).length > 0
    ) {
      this.props.getEntitiesFor(this.props.devices.find(device => device.deviceType === DeviceType.SHOE).id, Date.now() - timeless);
      this.dataTimer = setInterval(
        () => {
          const timeless: any = getAdditionToTimestamp(this.state.historyInterval, this.state.fromDate, this.state.toDate);
          this.props.getEntitiesFor(this.props.devices.find(device => device.deviceType === DeviceType.SHOE).id, Date.now() - timeless);
        },
        30000 // update every 5 min
      );
    }
  }

  componentWillUnmount() {
    if (this.dataTimer !== null) {
      clearInterval(this.dataTimer);
    }
    this.props.reset();
  }

  handleHistory = val => {
    this.setState({ historyInterval: val });
  };

  handleCustomHistory = val => {
    this.setState({ fromDate: val.fromDate, toDate: val.toDate });
  };

  filterFn = l => {
    switch (this.state.historyInterval) {
      case interval.ONE_DAY:
        return this.checkDateByDayInterval(l.timestamp, 1);
      case interval.ONE_WEEK:
        return this.checkDateByDayInterval(l.timestamp, 7);
      case interval.TWO_WEEKS:
        return this.checkDateByDayInterval(l.timestamp, 14);
      case interval.THREE_WEEKS:
        return this.checkDateByDayInterval(l.timestamp, 21);
      case interval.ONE_MONTH:
        return this.checkDateByDayInterval(l.timestamp, 30);
      case interval.THREE_MONTHS:
        return this.checkDateByDayInterval(l.timestamp, 90);
      case interval.CUSTOM: {
        const [fromDate] = this.state.fromDate.split('T');
        const [toDate] = this.state.toDate.split('T');

        return fromDate !== '' && toDate !== ''
          ? new Date(l.timestamp) >= new Date(fromDate) && new Date(l.timestamp) <= new Date(toDate)
          : fromDate !== ''
            ? new Date(l.timestamp) >= new Date(fromDate)
            : toDate !== ''
              ? new Date(l.timestamp) <= new Date(toDate)
              : true;
      }
      default:
        return true;
    }
  };

  checkDateByDayInterval = (date: string, daysAgo: number): boolean => {
    const today = new Date();
    return new Date(Date.UTC(today.getFullYear(), today.getMonth(), today.getDate() - daysAgo)) <= new Date(date);
  };

  labelFormatter = value => formatTime(value, 'YYYY-MM-DD');

  getAlertsFromUserDevices(userDevices: ReadonlyArray<IDevice>, deviceType: DeviceType) {
    let iDevice = userDevices.find(value => value.deviceType === deviceType);
    if (iDevice === undefined)return [];
    if (iDevice.alerts === undefined)return [];
    return iDevice.alerts;
  }
  render() {
    const { classes, shoeDataList, shoeStatus, devices } = this.props;
    const { showModal, isHistoryOpen } = this.state;
    return (
      <Card className="col-sm-5 col-md-5 m-3">
        <CardHeader
          avatar={
            <Avatar aria-label="Shoe" className={classes.avatar}>
              S
            </Avatar>
          }
          action={
            <IconButton onClick={this.openNotifications} className={classes.notification}>
              <Badge className={classes.margin} badgeContent={0} color="primary">
                <AnnouncementIcon style={{ color: 'red' }} />
              </Badge>
            </IconButton>
          }
          title={<Translate contentKey="global.devices.shoe">Shoe </Translate>}
          subheader={
            shoeDataList.length > 0 ? (
              <Translate contentKey="global.devices.shoeStatus" interpolate={{ status: ActivityType[shoeStatus] }}>
                Status:
              </Translate>
            ) : (
              'Loading...'
            )
          }
        />
        {shoeDataList.length > 0 && (
          <Fragment>
            <CardContent>
              <div style={{ width: '100%', height: 300 }}>
                <ResponsiveContainer>
                  <BarChart
                    width={600}
                    height={300}
                    data={shoeDataList.filter(this.filterFn)}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="timestamp" domain={['auto', 'auto']} name="Time" />
                    <YAxis />
                    <Tooltip labelFormatter={this.labelFormatter} />
                    <Legend />
                    <Bar dataKey="stepCounter" name="Steps" fill="#8884d8" />
                  </BarChart>
                </ResponsiveContainer>
              </div>
            </CardContent>
          </Fragment>
        )}
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton aria-label="History data" onClick={this.onToggleHistoryOpen} className={classes.expandHistory}>
            <HistoryIcon />
          </IconButton>
          {isHistoryOpen && (
            <History
              onSelectInterval={this.handleHistory}
              handleCustomInterval={this.handleCustomHistory}
              interval={interval.THREE_MONTHS}
            />
          )}
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent />
        </Collapse>
          <DeviceAlertNotificationModal showModal={showModal} handleClose={this.handleClose} alerts={this.getAlertsFromUserDevices(devices,DeviceType.SHOE)} />
      </Card>
    );
  }
}

const mapStateToProps = ({ shoeData }: IRootState) => {
  let shoeStatus = 0;
  let shoeStatusTimestamp = 0;
  let result = shoeData.entities.reduce((r, a) => {
    let data: IShoeDataPayload = JSON.parse(a.data.replace('/', ''));
    r[formatTime(a.timestamp, 'YYYY-MM-DD')] = r[formatTime(a.timestamp, 'YYYY-MM-DD')] || 0;
    r[formatTime(a.timestamp, 'YYYY-MM-DD')] += data.step_counter;
    if (a.timestamp > shoeStatusTimestamp) {
      shoeStatus = data.step_activity;
      shoeStatusTimestamp = a.timestamp;
    }
    return r;
  }, Object.create(null));
  return {
    shoeDataList: Object.keys(result).map(key => {
      return { timestamp: key, stepCounter: result[key] };
    }),
    shoeStatus,
  };
};

const mapDispatchToProps = { getSession, getEntitiesFor, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ShoeWidget)
);
