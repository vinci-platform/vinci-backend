import React from 'react';
import { Translate, TextFormat } from 'react-jhipster';
import { Alert, Col, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import { APP_DATE_FORMAT } from 'app/config/constants';
import {IDeviceAlert, AlertType} from "app/shared/model/device-alert.model";

export interface IDeviceAlertNotificationModalProps {
    showModal: boolean;
    handleClose: Function;
    alerts: IDeviceAlert[];
}

class DeviceAlertNotificationModal extends React.Component<IDeviceAlertNotificationModalProps> {

    sortedAlerts: IDeviceAlert[];

    componentDidMount() {
        this.sortedAlerts = this.props.alerts.sort((deviceAlert1,deviceAlert2) => (deviceAlert1.createdDate > deviceAlert2.createdDate) ? 1 : ((deviceAlert2.createdDate > deviceAlert1.createdDate) ? -1 : 0));
    }

    render() {
        const { handleClose, alerts } = this.props;
        this.sortedAlerts = this.props.alerts.sort((deviceAlert1,deviceAlert2) => (deviceAlert1.createdDate < deviceAlert2.createdDate) ? 1 : ((deviceAlert2.createdDate < deviceAlert1.createdDate) ? -1 : 0));

        return (
            <Modal isOpen={this.props.showModal} toggle={handleClose} autoFocus={false}>
                <AvForm>
                    <ModalHeader toggle={handleClose}>
                        <div style={{ color: '#007bff' }}>
                            <Translate contentKey="gatewayApp.device.notifications">Notifications</Translate>
                        </div>
                    </ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col md="12">
                                <h6>
                                    <Translate
                                        contentKey="gatewayApp.device.notificationsDeviceSubtitle"
                                        interpolate={{ number: alerts.filter(alert => !alert.familyRead).length }}
                                    >
                                        You have notification(s)
                                    </Translate>
                                    <span style={{ color: '#007bff' }}>this device!</span>
                                </h6>
                            </Col>
                        </Row>
                        <div className="mt-1">&nbsp;</div>
                        <Row>
                            <Col md="12">
                                {alerts.map((alert, i) => {
                                    const [lat, lon] = alert.values.split(';');
                                    return (
                                        <Alert color="warning" key={`alert-${i}`}>
                                            <div style={{ fontWeight: alert.familyRead ? 'normal' : 'bold' }}>
                        <span
                            style={{
                                color:
                                    alert.alertType === AlertType.WARNING
                                        ? 'orange'
                                        : alert.alertType === AlertType.DANGER
                                        ? 'red'
                                        : alert.alertType === AlertType.SUCCESS
                                            ? 'green'
                                            : '#007bff'
                            }}
                        >
                          {alert.alertType}
                        </span>
                                                :
                                                {/* <Translate contentKey={`${alert.label}`} interpolate={{ lat, lon }}> */}
                                                {alert.label} - {alert.values}
                                                {/* </Translate> */}
                                                @ <TextFormat value={alert.createdDate} type="date" format={APP_DATE_FORMAT} blankOnInvalid />
                                            </div>
                                        </Alert>
                                    );
                                })}
                            </Col>
                        </Row>
                    </ModalBody>
                </AvForm>
            </Modal>
        );
    }
}

export default DeviceAlertNotificationModal;
