import React from 'react';
import './login.scss';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router-dom';

import { IRootState } from 'app/shared/reducers';
import { adminLogin } from 'app/shared/reducers/authentication';
import { LocaleMenu } from 'app/shared/layout/header/menus';
import { Storage, translate, Translate } from 'react-jhipster';
import { setLocale } from 'app/shared/reducers/locale';
import { AvInput, AvField, AvForm, AvGroup } from 'availity-reactstrap-validation';
import { Label, Button } from 'reactstrap';

export interface ILoginProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export class AdminLogin extends React.Component<ILoginProps> {
  handleLogin = (event, errors, { username, password, rememberMe }) => {
    this.props.adminLogin(username, password, rememberMe);
  };

  handleCancel = () => {
    this.props.history.push('/');
  };

  handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    this.props.setLocale(langKey);
  };

  render() {
    const { location, isAuthenticated, currentLocale, loginError } = this.props;
    const { from } = location.state || { from: { pathname: '/', search: location.search } };
    if (isAuthenticated) {
      return <Redirect to={from} />;
    }
    return (
      <div className="login-wrapper">
        <img src="content/images/login.png" alt="login" />
        <LocaleMenu currentLocale={currentLocale} onClick={this.handleLocaleChange} icon="" id="language" />
        <AvForm className="form-container" onSubmit={this.handleLogin}>
          <div>
            <AvGroup className="form-container__inputs">
              {loginError ? (
                <span>
                  <Translate contentKey="login.messages.error.authentication">
                    <strong>Failed to sign in!</strong>
                    Please check your credentials and try again.
                  </Translate>
                </span>
              ) : null}
              <AvField
                name="username"
                label={translate('global.form.username')}
                placeholder={translate('global.form.username.placeholder')}
                required
                errorMessage="Username cannot be empty!"
                autoFocus
              />
              <AvField
                name="password"
                type="password"
                label={translate('login.form.password')}
                placeholder={translate('login.form.password.placeholder')}
                required
                errorMessage="Password cannot be empty!"
              />
            </AvGroup>
            <AvGroup check inline className="form-container__checkbox">
              <Label>
                <AvInput type="checkbox" name="rememberMe" />
                <Translate contentKey="login.form.rememberme">Remember me</Translate>
              </Label>
            </AvGroup>
          </div>
          <AvGroup className="form-container__reset-register">
            <Translate contentKey="global.messages.login.adminLoginIngoText">This page is for logging in as ADMIN user only!</Translate>
          </AvGroup>
          <AvGroup className="form-container__btn-container">
            <Button className="form_container__btn-container__signin" color="primary" type="submit">
              <Translate contentKey="login.form.button">Sign in</Translate>
            </Button>
            <Button className="form_container__btn-container__cancel" color="secondary" type="button" onClick={this.handleCancel}>
              <Translate contentKey="entity.action.cancel">Cancel</Translate>
            </Button>
          </AvGroup>
        </AvForm>
      </div>
    );
  }
}

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  showModal: authentication.showModalLogin,
  currentLocale: locale.currentLocale
});

const mapDispatchToProps = { adminLogin, setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminLogin);
