import './welcome.scss';
import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Storage, Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { getSession } from 'app/shared/reducers/authentication';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { handleAssociatePatient, getAssociatedPatients, getUserDevices, handleGeneratePatient } from 'app/shared/reducers/user-utils';
import { LocaleMenu } from 'app/shared/layout/header/menus';
import { setLocale } from 'app/shared/reducers/locale';
import color from '@material-ui/core/colors/amber';

export interface IWelcomeProp extends StateProps, DispatchProps, RouteComponentProps<any> { }

export interface IWelcomeState {
  showModal: boolean;
}

export class Welcome extends React.Component<IWelcomeProp> {
  readonly state: IWelcomeState = { showModal: false };

  componentDidMount() {
    this.props.getSession();
  }

  componentDidUpdate(prevProps: Readonly<IWelcomeProp>, prevState: Readonly<{}>, snapshot?: any): void {
    if (this.props.isAuthenticated) {
      if ((this.props.isFamily || this.props.isOrganization) && this.props.associatedUsers === null) {
        this.props.getAssociatedPatients(null);
      } else if (this.props.isPatient && this.props.devices === null) {
        this.props.getUserDevices('');
      }
    }
  }

  addPatient = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  handleAssociatePatient = (generate, values) => {
    if (generate) {
      this.props.handleGeneratePatient(values);
    } else {
      this.props.handleAssociatePatient(values.uuid, this.props.isFamily, this.props.isOrganization);
    }
  };

  handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    this.props.setLocale(langKey);
  };

  render() {
    const {
      account,
      isAuthenticated,
      currentLocale
    } = this.props;
    const { showModal } = this.state;
    return (
      <div className="height-100">
        {account && account.login ? (
          // <div className="p-3">
            {/* {(isFamily || isOrganization) && (
              <div style={{ height: '3rem' }}>
                <Button onClick={this.addPatient} className="btn btn-primary float-right jh-create-entity new-patient-btn">
                  <FontAwesomeIcon icon="plus" />
                  &nbsp;
                  <Translate contentKey="global.patient.addNewPatient">Add new patient</Translate>
                </Button>
                <NewPatientModal
                  showModal={showModal}
                  handleClose={this.handleClose}
                  associatePatient={this.handleAssociatePatient}
                  isFamily={isFamily}
                />
              </div>
            )} */}
          // </div>
        ) : (
          !isAuthenticated && (
            <div className="wrapper">
              <img src="content/images/entry.png" alt="entry" />
              <LocaleMenu currentLocale={currentLocale} onClick={this.handleLocaleChange} icon="" id="language" />
              <div className="form_container">
                <a href="#/login">
                  <Button className="form_container__sign_in_btn" color="primary">
                    <Translate contentKey="global.messages.info.authenticated.link">Sign In</Translate>
                  </Button>
                </a>
                <div className="form_container__registration">
                  <Translate contentKey="global.messages.info.register.noaccount">You do not have an account yet?</Translate>
                  <a href="#/register">
                    <Button className="form_container__registration__register_btn" color="primary">
                      <Translate contentKey="global.messages.info.register.link">Register a new account</Translate>
                    </Button>
                  </a>
                </div>
              </div>
              <div className="about-container">
                <div className="about-container__title">
                  <Translate contentKey="home.description"> DescriptionText</Translate>
                </div>
                <div className="about-container__content">
                  <Translate contentKey="home.descriptionText"> DescriptionText</Translate>
                </div>
              </div>
            </div>
          )
        )}
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  isPatient: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.PACIENT]),
  isFamily: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.FAMILY]),
  isOrganization: hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.ORGANIZATION]),
  associatedUsers: storeState.userUtils.associatedUsers,
  devices: storeState.userUtils.devices,
  cameraMovement: storeState.userUtils.cameraMovement,
  cameraRecognition: storeState.userUtils.cameraRecognition,
  currentLocale: storeState.locale.currentLocale
});

const mapDispatchToProps = { getSession, handleAssociatePatient, handleGeneratePatient, getAssociatedPatients, getUserDevices, setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Welcome);
