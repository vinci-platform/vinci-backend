import React from 'react';
import '../password-reset.scss';
import { Translate, translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';
import { Button, Label } from 'reactstrap';

import { handlePasswordResetInit, reset } from '../password-reset.reducer';
import { RouteComponentProps } from 'react-router-dom';

export interface IPasswordResetInitProps extends DispatchProps, RouteComponentProps {}

export class PasswordResetInit extends React.Component<IPasswordResetInitProps> {
  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    this.props.handlePasswordResetInit(values.email);
    event.preventDefault();
  };

  handleCancel = () => {
    this.props.history.push('/login');
  };

  render() {
    return (
      <div className="reset-wrapper">
        <img src="content/images/password-reset.png" alt="password-reset" />
        <AvForm className="reset-form" onValidSubmit={this.handleValidSubmit}>
          <Label id="reset-title">
            <Translate contentKey="reset.request.title">Reset your password</Translate>
          </Label>
          <div className="reset-form__controls-container">
            <Label id="reset-email">
              <Translate contentKey="reset.request.messages.info">Enter the email address you used to register</Translate>
            </Label>
            <AvField
              name="email"
              label={translate('global.form.email')}
              placeholder={translate('global.form.email.placeholder')}
              type="email"
              validate={{
                required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
                minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
                maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') }
              }}
            />
          </div>
          <AvGroup className="reset-form__btn-container">
            <Button className="reset-form__btn-container__cancel" color="secondary" onClick={this.handleCancel}>
              <Translate contentKey="entity.action.cancel">Cancel</Translate>
            </Button>
            <Button className="reset-form__btn-container__submit" type="submit" color="success">
              <Translate contentKey="reset.request.form.button">Reset password</Translate>
            </Button>
          </AvGroup>
        </AvForm>
      </div>
    );
  }
}

const mapDispatchToProps = { handlePasswordResetInit, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  null,
  mapDispatchToProps
)(PasswordResetInit);
