import React from 'react';
import '../password-reset.scss';
import { connect } from 'react-redux';
import { Button, Label } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, getUrlParameter } from 'react-jhipster';
import { RouteComponentProps } from 'react-router-dom';

import { handlePasswordResetFinish, reset } from '../password-reset.reducer';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';

export interface IPasswordResetFinishProps extends DispatchProps, RouteComponentProps<{ key: string }> {}

export interface IPasswordResetFinishState {
  password: string;
  key: string;
  activate: string;
}

export class PasswordResetFinishPage extends React.Component<IPasswordResetFinishProps, IPasswordResetFinishState> {
  state: IPasswordResetFinishState = {
    password: '',
    key: getUrlParameter('key', this.props.location.search),
    activate: getUrlParameter('activate', this.props.location.search)
  };

  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    this.props.handlePasswordResetFinish(this.state.key, values.newPassword, this.state.activate);
  };

  updatePassword = event => {
    this.setState({ password: event.target.value });
  };

  getResetForm() {
    return (
      <div className="reset-wrapper">
        <img src="content/images/password-reset.png" alt="password-reset" />
        <AvForm id="reset-form-finish" className="reset-form" onValidSubmit={this.handleValidSubmit}>
          <Label id="reset-pass-title">
            <Translate contentKey="reset.finish.title">Reset password</Translate>
          </Label>
          <div className="reset-form__controls-container">
            <AvField
              name="newPassword"
              label={translate('global.form.newpassword')}
              placeholder={translate('global.form.newpassword.placeholder')}
              type="password"
              validate={{
                required: { value: true, errorMessage: translate('global.messages.validate.newpassword.required') },
                pattern: {
                  value: '(?=[A-Za-z0-9@#$%^&+!=-]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-@#$%^&+!=])(?=.{10,}).*$',
                  errorMessage: translate('register.messages.validate.password.pattern')
                }
              }}
              onChange={this.updatePassword}
            />
            <PasswordStrengthBar password={this.state.password} />
            <AvField
              name="confirmPassword"
              label={translate('global.form.confirmpassword')}
              placeholder={translate('global.form.confirmpassword.placeholder')}
              type="password"
              validate={{
                required: { value: true, errorMessage: translate('global.messages.validate.confirmpassword.required') },
                minLength: { value: 10, errorMessage: translate('global.messages.error.dontmatch') },
                maxLength: { value: 50, errorMessage: translate('global.messages.error.dontmatch') },
                match: { value: 'newPassword', errorMessage: translate('global.messages.error.dontmatch') }
              }}
            />
          </div>
          <Button color="success" type="submit" className="reset-form__button-validate">
            <Translate contentKey="reset.finish.form.button">Validate new password</Translate>
          </Button>
        </AvForm>
      </div>
    );
  }

  render() {
    const { key } = this.state;

    return <div className="height-100">{key ? this.getResetForm() : null}</div>;
  }
}

const mapDispatchToProps = { handlePasswordResetFinish, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  null,
  mapDispatchToProps
)(PasswordResetFinishPage);
