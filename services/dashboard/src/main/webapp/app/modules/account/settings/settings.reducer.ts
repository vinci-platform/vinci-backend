import axios from 'axios';
import { translate, Storage } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { getSession } from 'app/shared/reducers/authentication';

export const ACTION_TYPES = {
  UPDATE_ACCOUNT: 'account/UPDATE_ACCOUNT',
  RESET: 'account/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  updateSuccess: false,
  updateFailure: false
};

export type SettingsState = Readonly<typeof initialState>;

// Reducer
export default (state: SettingsState = initialState, action): SettingsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.UPDATE_ACCOUNT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case FAILURE(ACTION_TYPES.UPDATE_ACCOUNT):
      return {
        ...state,
        loading: false,
        updateSuccess: false,
        updateFailure: true
      };
    case SUCCESS(ACTION_TYPES.UPDATE_ACCOUNT):
      return {
        ...state,
        loading: false,
        updateSuccess: true,
        updateFailure: false
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// Actions
const apiUrl = 'api/account';

export const saveAccountSettings = account => async (dispatch, getState) => {
  const data = new FormData();
  if (account.hasOwnProperty('newImages')) {
    account.newImages.forEach((image, index) => {
      if (typeof image !== 'undefined' && image !== null) {
        data.append(`image${index + 1}`, image);
        data.append(`image${index + 1}Id`, account.imageIds[index]);
      }
    });
  }
  delete account.newImages;
  delete account.images;
  delete account.imageIds;
  data.append('account', JSON.stringify(account));
  await dispatch({
    type: ACTION_TYPES.UPDATE_ACCOUNT,
    payload: axios.post(apiUrl, data),
    meta: {
      successMessage: translate('settings.messages.success')
    }
  });

  if (Storage.session.get(`locale`)) {
    Storage.session.remove(`locale`);
  }

  await dispatch(getSession());
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
