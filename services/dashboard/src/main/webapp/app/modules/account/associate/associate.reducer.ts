import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

export const ACTION_TYPES = {
  ASSOCIATE_ACCOUNT: 'associate/ASSOCIATE_ACCOUNT',
  RESET: 'associate/RESET'
};

const initialState = {
  associationSuccess: false,
  associationFailure: false
};

export type AssociateState = Readonly<typeof initialState>;

// Reducer
export default (state: AssociateState = initialState, action): AssociateState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.ASSOCIATE_ACCOUNT):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.ASSOCIATE_ACCOUNT):
      return {
        ...state,
        associationFailure: true
      };
    case SUCCESS(ACTION_TYPES.ASSOCIATE_ACCOUNT):
      return {
        ...state,
        associationSuccess: true
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// Actions
export const associateAction = (user, targetUser, isFamily) => ({
  type: ACTION_TYPES.ASSOCIATE_ACCOUNT,
  payload: axios.get('api/associate?u=' + user + '&tu=' + targetUser + '&f=' + isFamily)
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
