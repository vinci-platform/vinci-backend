import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Row, Col, Alert } from 'reactstrap';
import { Translate, getUrlParameter } from 'react-jhipster';

import { IRootState } from 'app/shared/reducers';
import { associateAction, reset } from './associate.reducer';

const successAlert = (
  <Alert color="success">
    <Translate contentKey="associate.messages.confirmed">
      <strong>Your user account has been associated.</strong>
    </Translate>
  </Alert>
);

const failureAlert = (
  <Alert color="danger">
    <Translate contentKey="associate.messages.confirmationError">
      <strong>Your user could not be associate.</strong>
    </Translate>
  </Alert>
);

export interface IAssociateProps extends StateProps, DispatchProps, RouteComponentProps<{ key: any }> {}

export class AssociatePage extends React.Component<IAssociateProps> {
  componentWillUnmount() {
    this.props.reset();
  }

  componentDidMount() {
    const user = getUrlParameter('u', this.props.location.search);
    const targetUser = getUrlParameter('tu', this.props.location.search);
    const isFamily = getUrlParameter('f', this.props.location.search);
    this.props.associateAction(user, targetUser, isFamily);
  }

  render() {
    const { associationSuccess, associationFailure } = this.props;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h1>
              <Translate contentKey="associate.title">Association</Translate>
            </h1>
            {associationSuccess ? successAlert : undefined}
            {associationFailure ? failureAlert : undefined}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ associate }: IRootState) => ({
  associationSuccess: associate.associationSuccess,
  associationFailure: associate.associationFailure
});

const mapDispatchToProps = { associateAction, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssociatePage);
