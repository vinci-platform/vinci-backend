import React, { Fragment } from 'react';
import { Button, Col, Row, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { Translate, translate } from 'react-jhipster';
import { AvForm, AvField } from 'availity-reactstrap-validation';

import { locales, languages } from 'app/config/translation';
import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import { saveAccountSettings, reset } from './settings.reducer';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';

export interface IUserSettingsProps extends StateProps, DispatchProps {}

export interface IUserSettingsState {
  account: any;
  image1: File;
  image1Id: number;
  image2: File;
  image2Id: number;
}

export class SettingsPage extends React.Component<IUserSettingsProps, IUserSettingsState> {
  state: IUserSettingsState = {
    account: '',
    image1: null,
    image1Id: 0,
    image2: null,
    image2Id: 0
  };

  componentDidMount() {
    this.props.getSession();
  }

  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    const account = {
      ...this.props.account,
      ...values,
      newImages: [this.state.image1, this.state.image2],
      imageIds: [this.state.image1Id, this.state.image2Id]
    };

    this.props.saveAccountSettings(account);
    event.persist();
  };

  onFileChange = imageId => e => {
    if (e.target.name === 'image1') {
      this.setState({
        image1: e.target.files[0],
        image1Id: imageId
      });
    } else {
      this.setState({
        image2: e.target.files[0],
        image2Id: imageId
      });
    }
  };

  render() {
    const { account, isPatient } = this.props;

    return (
      <Row className="justify-content-center p-3 administration-container">
        <Col md="8">
          <h2 id="settings-title" className="mb-4">
            <Translate contentKey="settings.title" interpolate={{ username: account.login }}>
              User settings for {account.login}
            </Translate>
          </h2>
          {/* To be updated with real patient UUID */}
          {isPatient && (
            <h5 id="settings-subtitle" className="mt-4 mb-4">
              <Translate contentKey="settings.uuid">Your UUID is:</Translate>
              {account.uuid}
            </h5>
          )}
          <AvForm id="settings-form" onValidSubmit={this.handleValidSubmit}>
            {/* First name */}
            <AvField
              className="form-control"
              name="firstName"
              label={translate('settings.form.firstname')}
              id="firstName"
              placeholder={translate('settings.form.firstname.placeholder')}
              validate={{
                required: { value: true, errorMessage: translate('settings.messages.validate.firstname.required') },
                minLength: { value: 1, errorMessage: translate('settings.messages.validate.firstname.minlength') },
                maxLength: { value: 50, errorMessage: translate('settings.messages.validate.firstname.maxlength') }
              }}
              value={account.firstName}
            />
            {/* Last name */}
            <AvField
              className="form-control"
              name="lastName"
              label={translate('settings.form.lastname')}
              id="lastName"
              placeholder={translate('settings.form.lastname.placeholder')}
              validate={{
                required: { value: true, errorMessage: translate('settings.messages.validate.lastname.required') },
                minLength: { value: 1, errorMessage: translate('settings.messages.validate.lastname.minlength') },
                maxLength: { value: 50, errorMessage: translate('settings.messages.validate.lastname.maxlength') }
              }}
              value={account.lastName}
            />
            {/* Address */}
            <AvField
              className="form-control"
              name="address"
              label={translate('settings.form.address')}
              id="address"
              placeholder={translate('settings.form.address.placeholder')}
              validate={{
                minLength: { value: 1, errorMessage: translate('settings.messages.validate.address.minlength') },
                maxLength: { value: 70, errorMessage: translate('settings.messages.validate.address.maxlength') }
              }}
              value={account.address}
            />
            {/* Phone */}
            <AvField
              className="form-control"
              name="phone"
              type="number"
              label={translate('settings.form.phone')}
              id="phone"
              placeholder={translate('settings.form.phone.placeholder')}
              validate={{
                required: { value: true, errorMessage: translate('settings.messages.validate.phone.required') },
                minLength: { value: 10, errorMessage: translate('settings.messages.validate.phone.minlength') },
                maxLength: { value: 10, errorMessage: translate('settings.messages.validate.phone.maxlength') }
              }}
              value={account.phone}
            />
            {/* Email */}
            <AvField
              name="email"
              label={translate('global.form.email')}
              placeholder={translate('global.form.email.placeholder')}
              type="email"
              validate={{
                required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
                minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
                maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') }
              }}
              value={account.email}
            />
            {/* Language key */}
            <AvField
              type="select"
              id="langKey"
              name="langKey"
              className="form-control"
              label={translate('settings.form.language')}
              value={account.langKey}
            >
              {locales.map(locale => (
                <option value={locale} key={locale}>
                  {languages[locale].name}
                </option>
              ))}
            </AvField>
            {isPatient && (
              <Fragment>
                {/* Gender */}
                <AvField
                  type="select"
                  id="gender"
                  name="gender"
                  className="form-control"
                  label={translate('gatewayApp.userExtra.gender')}
                  value={account.gender}
                >
                  <option value="" />
                  <option value="Male">
                    <Translate contentKey="gatewayApp.userExtra.MALE" />
                  </option>
                  <option value="Female">
                    <Translate contentKey="gatewayApp.userExtra.FEMALE" />
                  </option>
                </AvField>
                <FormGroup>
                  <Label for="image1">Image 1</Label>
                  <Input
                    type="file"
                    accept="image/x-png,image/jpeg"
                    name="image1"
                    id="image1"
                    onChange={this.onFileChange(account.images.length > 0 ? account.images[0].id : 0)}
                  />
                </FormGroup>
                {account.images.length > 0 && (
                  <img style={{ maxWidth: '200px' }} src={`data:image/${account.images[0].format};base64,${account.images[0].image}`} />
                )}
                <br />
                <FormGroup>
                  <Label for="image2">Image 2</Label>
                  <Input
                    type="file"
                    accept="image/x-png,image/jpeg"
                    name="image2"
                    id="image2"
                    onChange={this.onFileChange(account.images.length > 1 ? account.images[1].id : 0)}
                  />
                </FormGroup>
                {account.images.length > 1 && (
                  <img style={{ maxWidth: '200px' }} src={`data:image/${account.images[1].format};base64,${account.images[1].image}`} />
                )}
                <br />
              </Fragment>
            )}
            <Button color="primary" type="submit" style={{ marginTop: '20px' }}>
              <Translate contentKey="settings.form.button">Save</Translate>
            </Button>
          </AvForm>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated,
  isPatient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT])
});

const mapDispatchToProps = { getSession, saveAccountSettings, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPage);
