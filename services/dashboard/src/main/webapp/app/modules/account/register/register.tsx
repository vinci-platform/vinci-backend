import React from 'react';
import './register.scss';
import { Storage, Translate, translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';
import { Button, Label, Alert } from 'reactstrap';

import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { IRootState } from 'app/shared/reducers';
import { handleRegister, reset } from './register.reducer';
import { AUTHORITIES } from 'app/config/constants';
import { RouteComponentProps } from 'react-router-dom';
import { LocaleMenu } from 'app/shared/layout/header/menus';
import { setLocale } from 'app/shared/reducers/locale';

export interface IRegisterProps extends StateProps, DispatchProps, RouteComponentProps {}

export interface IRegisterState {
  password: string;
}

export class RegisterPage extends React.Component<IRegisterProps, IRegisterState> {
  state: IRegisterState = {
    password: ''
  };

  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    this.props.handleRegister(values.username, values.email, values.firstPassword, this.props.currentLocale, values.role);
    event.preventDefault();
  };

  handleCancel = () => {
    this.props.history.push('/');
  };

  updatePassword = event => {
    this.setState({ password: event.target.value });
  };

  handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    this.props.setLocale(langKey);
  };

  render() {
    return (
      <div className="register-wrapper">
        <img src="content/images/registration.png" alt="register" />
        <LocaleMenu currentLocale={this.props.currentLocale} onClick={this.handleLocaleChange} icon="" id="language" />
        <AvForm className="register-form" onValidSubmit={this.handleValidSubmit}>
          <Label id="register-title">
            <Translate contentKey="register.title">Registration</Translate>
          </Label>
          <AvField
            name="username"
            label={translate('global.form.username')}
            placeholder={translate('global.form.username.placeholder')}
            validate={{
              required: { value: true, errorMessage: translate('register.messages.validate.login.required') },
              pattern: {
                value: '^[_.@A-Za-z0-9-]*$',
                errorMessage: translate('register.messages.validate.login.pattern')
              },
              minLength: { value: 1, errorMessage: translate('register.messages.validate.login.minlength') },
              maxLength: { value: 50, errorMessage: translate('register.messages.validate.login.maxlength') }
            }}
          />
          <AvField
            name="email"
            label={translate('global.form.email')}
            placeholder={translate('global.form.email.placeholder')}
            type="email"
            validate={{
              required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
              minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
              maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') }
            }}
          />
          <AvField
            name="firstPassword"
            label={translate('global.form.newpassword')}
            placeholder={translate('global.form.newpassword.placeholder')}
            type="password"
            onChange={this.updatePassword}
            validate={{
              required: { value: true, errorMessage: translate('global.messages.validate.newpassword.required') },
                pattern: {
                    value: '(?=[A-Za-z0-9@#$%^&+!=-]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=-])(?=.{10,}).*$',
                    errorMessage: translate('register.messages.validate.password.pattern')
                }
            }}
          />
          <PasswordStrengthBar password={this.state.password} />
          <AvField
            name="secondPassword"
            label={translate('global.form.confirmpassword')}
            placeholder={translate('global.form.confirmpassword.placeholder')}
            type="password"
            validate={{
              required: { value: true, errorMessage: translate('global.messages.validate.confirmpassword.required') },
              minLength: { value: 10, errorMessage: translate('global.messages.error.dontmatch') },
              maxLength: { value: 50, errorMessage: translate('global.messages.error.dontmatch') },
              match: { value: 'firstPassword', errorMessage: translate('global.messages.error.dontmatch') }
            }}
          />
          <AvField name="role" label="Role" id="role" type="select" value={AUTHORITIES.PACIENT}>
            <option value={AUTHORITIES.PACIENT}>Patient</option>
            <option value={AUTHORITIES.FAMILY}>Family</option>
            <option value={AUTHORITIES.ORGANIZATION}>Organization/Doctor</option>
          </AvField>
          <AvGroup className="register-form__btn-container">
            <Button className="register-form__btn-container__cancel" color="secondary" onClick={this.handleCancel}>
              <Translate contentKey="entity.action.cancel">Cancel</Translate>
            </Button>
            <Button className="register-form__btn-container__submit" type="submit" color="primary">
              <Translate contentKey="register.form.button">Register</Translate>
            </Button>
          </AvGroup>
        </AvForm>
      </div>
    );
  }
}

const mapStateToProps = ({ locale }: IRootState) => ({
  currentLocale: locale.currentLocale
});

const mapDispatchToProps = { handleRegister, reset, setLocale };
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPage);
