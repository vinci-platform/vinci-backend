import React from 'react';
import { SERVER_API_URL } from 'app/config/constants';
const baseUrl = SERVER_API_URL || `${window.location.origin}`;
var url = `${baseUrl}/swagger-ui/index.html`

const DocsPage = () => (
  <div>
    <iframe src={url} width="100%" height="800" title="Swagger UI" seamless style={{ border: 'none' }} />
  </div>
);

export default DocsPage;
