import 'react-toastify/dist/ReactToastify.css';
import './app.scss';

import React from 'react';
import { connect } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import { IconButton, Badge } from '@material-ui/core';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import { getProfile } from 'app/shared/reducers/application-profile';
import { setLocale } from 'app/shared/reducers/locale';
import Header from 'app/shared/layout/header/header';
import Footer from 'app/shared/layout/footer/footer';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import ErrorBoundary from 'app/shared/error/error-boundary';
import { AUTHORITIES } from 'app/config/constants';
import AppRoutes from 'app/routes';
import NotificationsIcon from '@material-ui/icons/Notifications';
import UserNotificationModal from './userNotificationsModal';
import { getEntities } from 'app/entities/user-extra/user-extra.reducer';
import { updateEntities } from 'app/entities/user-alert/user-alert.reducer';
import { toast, ToastContainer, ToastPosition } from 'react-toastify';

export interface IAppProps extends StateProps, DispatchProps {}

export interface IAppState {
  showModal: boolean;
}

export class App extends React.Component<IAppProps, IAppState> {
  readonly state: IAppState = { showModal: false };

  componentDidMount() {
    this.props.getSession();
    this.props.getProfile();
  }

  componentWillReceiveProps(nextProps: Readonly<IAppProps>, nextContext: any): void {
    if (typeof this.props.userExtras === 'undefined' && nextProps.isAuthenticated) {
      this.props.getEntities();
    }
  }

  openNotifications = () => {
    this.setState({ showModal: true });
  };

  handleClose = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { showModal } = this.state;
    const { account, userExtras, isPatient, isFamily, isAuthenticated } = this.props;
    return (
      <Router>
        <div className="height-100">
          <div className="height-100" style={{ padding: isAuthenticated ? '4rem 0 3rem' : 0 }}>
            <ToastContainer position={toast.POSITION.TOP_LEFT as ToastPosition} className="toastify-container" />
            {isAuthenticated && (
              <ErrorBoundary>
                <Header
                  isAuthenticated={isAuthenticated}
                  isAdmin={this.props.isAdmin}
                  isPatient={this.props.isPatient}
                  isOrganization={this.props.isOrganization}
                  currentLocale={this.props.currentLocale}
                  onLocaleChange={this.props.setLocale}
                  ribbonEnv={this.props.ribbonEnv}
                  isInProduction={this.props.isInProduction}
                  isSwaggerEnabled={this.props.isSwaggerEnabled}
                />
              </ErrorBoundary>
            )}
            <ErrorBoundary>
              <AppRoutes />
            </ErrorBoundary>
            {isAuthenticated && (
              <IconButton className="notificationIcon" onClick={this.openNotifications}>
                <Badge
                  badgeContent={
                    userExtras.find(user => user.userId === account.id) !== undefined &&
                    userExtras.find(user => user.userId === account.id).alerts.length !== 0
                      ? userExtras.find(user => user.userId === account.id).alerts.length
                      : 0
                  }
                  color="primary"
                >
                  <NotificationsIcon style={{ color: '#e5d346', transform: 'rotate(45deg)' }} />
                </Badge>
              </IconButton>
            )}
          </div>
          {isAuthenticated && <Footer />}
          {userExtras.find(user => user.userId === account.id) !== undefined && (
            <UserNotificationModal
              showModal={showModal}
              updateEntities={this.props.updateEntities}
              userType={isPatient ? 'patient' : isFamily ? 'family' : 'organization'}
              handleClose={this.handleClose}
              user={userExtras.find(user => user.userId === account.id)}
            />
          )}
        </div>
      </Router>
    );
  }
}

const mapStateToProps = ({ authentication, applicationProfile, locale, userExtra }: IRootState) => ({
  currentLocale: locale.currentLocale,
  isAuthenticated: authentication.isAuthenticated,
  isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
  isPatient: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.PACIENT]),
  isFamily: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.FAMILY]),
  isOrganization: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ORGANIZATION]),
  ribbonEnv: applicationProfile.ribbonEnv,
  isInProduction: applicationProfile.inProduction,
  isSwaggerEnabled: applicationProfile.isSwaggerEnabled,
  account: authentication.account,
  userExtras: userExtra.entities
});

const mapDispatchToProps = { setLocale, getSession, getProfile, getEntities, updateEntities };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
