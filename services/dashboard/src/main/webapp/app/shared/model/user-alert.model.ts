export const enum AlertType {
  SUCCESS = 'SUCCESS',
  INFO = 'INFO',
  WARNING = 'WARNING',
  DANGER = 'DANGER'
}

export interface IUserAlert {
  id?: number;
  label?: string;
  values?: string;
  alertType?: AlertType;
  userRead?: boolean;
  familyRead?: boolean;
  organizationRead?: boolean;
  userExtraId?: number;
  userExtraFirstName?: string;
  userExtraLastName?: string;
  createdDate?: Date;
}

export const defaultValue: Readonly<IUserAlert> = {
  userRead: false,
  familyRead: false,
  organizationRead: false
};
