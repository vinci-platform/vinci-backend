import { IUserExtra } from 'app/shared/model/user-extra.model';

export enum SurveyStatusType {
  'WORST',
  'BAD',
  'NETUTRAL',
  'GOOD',
  'VERY GOOD'
}

export interface ISurveyData {
  // TODO: revert to commented structure once data flows through ioserver
  // id?: number;
  // data?: any;
  // timestamp?: number;
  // userExtra?: IUserExtra;
  id?: number;
  identifier?: string;
  surveyType?: string;
  assesmentData?: string;
  scoringResult?: number;
  createdTime?: string;
  endTime?: string;
  additionalInfo?: string;
  userExtraId?: number;
  deviceId?: number;
}

export const defaultValue: Readonly<ISurveyData> = {};
