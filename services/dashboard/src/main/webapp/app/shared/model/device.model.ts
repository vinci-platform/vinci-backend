import { IDeviceAlert } from 'app/shared/model/device-alert.model';

export const enum DeviceType {
  WATCH = 'WATCH',
  SHOE = 'SHOE',
  CAMERA_FITNESS = 'CAMERA_FITNESS',
  CAMERA_MOVEMENT = 'CAMERA_MOVEMENT',
  SURVEY = 'SURVEY',
  FITBIT_WATCH = 'FITBIT_WATCH'
}

export interface IDevice {
  id?: number;
  name?: string;
  description?: string;
  uuid?: string;
  deviceType?: DeviceType;
  active?: boolean;
  startTimestamp?: string;
  alerts?: IDeviceAlert[];
  userExtraId?: number;
  userExtraFirstName?: string;
  userExtraLastName?: string;
  userFamilyId?: number;
  userFamilyFirstName?: string;
  userFamilyLastName?: string;
  userOrganisationId?: number;
  userOrganisationFirstName?: string;
  userOrganisationLastName?: string;
}

export const defaultValue: Readonly<IDevice> = {
  active: false
};
