export interface IFitbitWatchIntradayData {
  id?: number;
  data?: any;
  timestamp?: number;
  deviceId?: number;
}

export const defaultValue: Readonly<IFitbitWatchIntradayData> = {};
