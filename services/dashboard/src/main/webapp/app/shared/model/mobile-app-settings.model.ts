export interface IMobileAppSettings {
  id?: number;
  name?: string;
  userId?: number;
  value?: string;
}

export const defaultValue: Readonly<IMobileAppSettings> = {};
