export interface IUserImage {
  id?: number;
  image?: File;
  userExtraId?: number;
  userExtraFirstName?: string;
  userExtraLastName?: string;
}

export const defaultValue: Readonly<IUserImage> = {};
