import { IDevice } from 'app/shared/model/device.model';

export interface IFitbitWatchData {
  id?: number;
  sleepData?: any;
  activityData?: any;
  timestamp?: number;
  deviceId?: IDevice;
}

export const defaultValue: Readonly<IFitbitWatchData> = {};
