import { IDevice } from 'app/shared/model/device.model';

export interface IWatchData {
  id?: number;
  data?: string;
  timestamp?: number;
  deviceId?: number;
}

export const defaultValue: Readonly<IWatchData> = {};
