export enum ActivityType {
  'NOT WARN',
  STANDING,
  WALKING
}

export interface IShoeDataPayload {
  step_counter?: number;
  step_activity?: ActivityType;
}

export const defaultValue: Readonly<IShoeDataPayload> = {};
