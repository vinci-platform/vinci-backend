export interface IEventRecord {
  id?: number;
  appId?: number;
  userId?: number;
  timestamp?: number;
  created?: number;
  notify?: number;
  repeat?: string;
  title?: string;
  type?: string;
  text?: string;
  data?: string;
}

export const defaultValue: Readonly<IEventRecord> = {};
