import { IUserExtra } from 'app/shared/model/user-extra.model';

export interface IUserAggregated {
  userInfo?: IUserExtra;
  pulse?: string;
  heartRate?: string;
  mood?: string;
  fitness?: string;
}

export const defaultValue: Readonly<IUserAggregated> = {};
