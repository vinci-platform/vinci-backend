import { IDevice } from 'app/shared/model/device.model';

export interface ICameraFitnessData {
  id?: number;
  data?: any;
  timestamp?: number;
  device?: IDevice;
}

export const defaultValue: Readonly<ICameraFitnessData> = {};
