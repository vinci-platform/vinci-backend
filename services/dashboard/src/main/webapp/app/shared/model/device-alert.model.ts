export const enum AlertType {
  SUCCESS = 'SUCCESS',
  INFO = 'INFO',
  WARNING = 'WARNING',
  DANGER = 'DANGER'
}

export interface IDeviceAlert {
  id?: number;
  label?: string;
  values?: string;
  alertType?: AlertType;
  userRead?: boolean;
  familyRead?: boolean;
  organizationRead?: boolean;
  deviceId?: number;
  createdDate?: Date;
}

export const defaultValue: Readonly<IDeviceAlert> = {
  userRead: false,
  familyRead: false,
  organizationRead: false
};
