import { IUserAlert } from 'app/shared/model/user-alert.model';
import { IUserImage } from 'app/shared/model/user-image.model';
import { IDevice } from 'app/shared/model/device.model';

export interface IUserExtra {
  id?: number;
  login?: string;
  phone?: string;
  address?: string;
  userId?: number;
  userFirstName?: string;
  userLastName?: string;
  alerts?: IUserAlert[];
  familyFirstName?: string;
  familyLastName?: string;
  familyId?: number;
  organizationFirstName?: string;
  organizationLastName?: string;
  organizationId?: number;
  uuid?: string;
  description?: string;
  gender?: string;
  images?: IUserImage[];
  devices?: IDevice[];
  education?: string;
  maritalStatus?: string;
}

export const defaultValue: Readonly<IUserExtra> = {};
