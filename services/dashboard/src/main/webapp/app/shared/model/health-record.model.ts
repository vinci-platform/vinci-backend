export interface IHealthRecord {
  id?: number;
  userId?: number;
  timestamp?: string;
  score?: number;
  availableScore?: number;
  stepsScore?: number;
  ipaqScore?: number;
  whoqolScore?: number;
  feelingsScore?: number;
}

export const defaultValue: Readonly<IHealthRecord> = {};
