import { IDevice } from 'app/shared/model/device.model';
// import { IShoeDataPayload } from 'app/shared/model/ioserver/shoe-data-payload-model';

export interface IShoeData {
  id?: number;
  // data?: IShoeDataPayload;
  data?: string;
  timestamp?: number;
  device?: IDevice;
}

export const defaultValue: Readonly<IShoeData> = {};
