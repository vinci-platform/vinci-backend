import { IDevice } from 'app/shared/model/device.model';

export interface ICameraMovementData {
  id?: number;
  data?: any;
  timestamp?: number;
  device?: IDevice;
}

export const defaultValue: Readonly<ICameraMovementData> = {};
