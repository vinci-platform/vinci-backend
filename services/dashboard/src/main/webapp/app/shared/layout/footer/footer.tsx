import './footer.scss';
import React from 'react';

const Footer = () => (
  <div className="d-flex flex-column align-content-center">
    <div className="footer">
      <img className="footer-img" src="content/images/vinci-logo.svg" alt="vinci-logo" />
    </div>
  </div>
);

export default Footer;
