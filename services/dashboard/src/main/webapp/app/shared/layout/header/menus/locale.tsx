import React from 'react';
import { DropdownItem } from 'reactstrap';
import { NavDropdown } from '../header-components';
import { locales, languages } from 'app/config/translation';

export const LocaleMenu = props => {
  const { currentLocale, onClick, id, icon } = props;
  return (
    Object.keys(languages).length > 1 && (
      <NavDropdown icon={icon} name={currentLocale ? languages[currentLocale].name : undefined} id={id ? id : 'lang'}>
        {locales.map(locale => (
          <DropdownItem key={locale} value={locale} onClick={onClick}>
            {languages[locale].name}
          </DropdownItem>
        ))}
      </NavDropdown>
    )
  );
};
