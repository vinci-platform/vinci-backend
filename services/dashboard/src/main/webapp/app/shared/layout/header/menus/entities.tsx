import React from 'react';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from '../header-components';

export const EntitiesMenu = props => (
  // tslint:disable-next-line:jsx-self-close
  <NavDropdown icon="th-list" name={translate('global.menu.entities.main')} id="entity-menu">
    <DropdownItem tag={Link} to="/entity/device">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.device" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/device-alert">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.deviceAlert" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/user-alert">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.userAlert" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/user-extra">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.userExtra" />
    </DropdownItem>
    {/* <DropdownItem tag={Link} to="/entity/watch-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioServerWatchData" />
    </DropdownItem> */}
    <DropdownItem tag={Link} to="/entity/shoe-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioServerShoeData" />
    </DropdownItem>
    {/* <DropdownItem tag={Link} to="/entity/camera-fitness-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioServerCameraFitnessData" />
    </DropdownItem> */}
    {/* <DropdownItem tag={Link} to="/entity/camera-movement-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioServerCameraMovementData" />
    </DropdownItem> */}
    <DropdownItem tag={Link} to="/entity/survey-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioServerSurveyData" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/fitbit-watch-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioserverFitbitWatchData" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/fitbit-watch-intraday-data">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.ioserverFitbitWatchIntradayData" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/mobile-app-settings">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.mobileAppSettings" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/health-record">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.healthRecord" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/event-record">
      <FontAwesomeIcon icon="asterisk" fixedWidth />
      &nbsp;
      <Translate contentKey="global.menu.entities.eventRecord" />
    </DropdownItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
