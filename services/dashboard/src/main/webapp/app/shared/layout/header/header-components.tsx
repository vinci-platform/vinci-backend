import React from 'react';
import { Translate } from 'react-jhipster';

import { UncontrolledDropdown, DropdownToggle, DropdownMenu, NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const NavDropdown = props => (
  <UncontrolledDropdown nav inNavbar id={props.id}>
    <DropdownToggle nav caret className="d-flex align-items-center larger-font">
      {props.icon !== '' && <FontAwesomeIcon icon={props.icon} />}
      <span>{props.name}</span>
    </DropdownToggle>
    <DropdownMenu right style={props.style}>
      {props.children}
    </DropdownMenu>
  </UncontrolledDropdown>
);

export const Brand = props => <a href={window.location.origin}>
  <img className="logo-img" src="content/images/360-logo.svg" alt="360-logo" />
</a>;

export const Home = props => (
  <NavItem>
    <NavLink tag={Link} to="/" className="d-flex align-items-center larger-font">
      <FontAwesomeIcon icon="home" />
      <span>
        <Translate contentKey="global.menu.home">Home</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Devices = props => (
  <NavItem>
    <NavLink tag={Link} to="/entity/device" className="d-flex align-items-center larger-font">
      <FontAwesomeIcon icon="desktop" />
      <span>
        <Translate contentKey="global.menu.devices">Devices</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Users = props => (
  <NavItem>
    <NavLink tag={Link} to="/entity/user-extra" className="d-flex align-items-center larger-font">
      <FontAwesomeIcon icon="users" />
      <span>
        <Translate contentKey="global.menu.entities.userExtra">Users</Translate>
      </span>
    </NavLink>
  </NavItem>
);
