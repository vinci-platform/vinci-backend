import { TranslatorContext } from 'react-jhipster';
export const ACTION_TYPES = {
  SET_LOCALE: 'locale/SET_LOCALE'
};

const initialState = {
  currentLocale: undefined
};

export type LocaleState = Readonly<typeof initialState>;

export default (state: LocaleState = initialState, action): LocaleState => {
  switch (action.type) {
    case ACTION_TYPES.SET_LOCALE:
      const currentLocale = action.locale;
      if (state.currentLocale !== currentLocale) {
        TranslatorContext.setLocale(currentLocale);
      }
      return {
        currentLocale
      };
    default:
      return state;
  }
};

export const setLocale = (locale: string) => async dispatch => {
  console.log('Request locale:', locale);

  if (!Object.keys(TranslatorContext.context.translations).includes(locale)) {
    // @ts-ignore
    let language: any = EN_LANG; //setting default language
    switch (locale) {
      case 'ro':
        // @ts-ignore
        language = RO_LANG;
        break;
      case 'el':
        // @ts-ignore
        language = EL_LANG;
        break;
      default:
        // @ts-ignore
        language = EN_LANG;
    }
    TranslatorContext.registerTranslations(locale, language);
  }
  dispatch({
    type: ACTION_TYPES.SET_LOCALE,
    locale
  });
};
