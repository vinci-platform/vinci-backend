import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';
import userUtils, { UserUtilsState } from './user-utils';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import associate, { AssociateState } from 'app/modules/account/associate/associate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import device, {
  DeviceState
} from 'app/entities/device/device.reducer';
// prettier-ignore
import deviceAlert, {
  DeviceAlertState
} from 'app/entities/device-alert/device-alert.reducer';
// prettier-ignore
import userAlert, {
  UserAlertState
} from 'app/entities/user-alert/user-alert.reducer';
// prettier-ignore
import userExtra, {
  UserExtraState
} from 'app/entities/user-extra/user-extra.reducer';
// prettier-ignore
import watchData, {
  WatchDataState
} from 'app/entities/ioserver/watch-data/watch-data.reducer';
// prettier-ignore
import shoeData, {
  ShoeDataState
} from 'app/entities/ioserver/shoe-data/shoe-data.reducer';
// prettier-ignore
import cameraFitnessData, {
  CameraFitnessDataState
} from 'app/entities/ioserver/camera-fitness-data/camera-fitness-data.reducer';
// prettier-ignore
import cameraMovementData, {
  CameraMovementDataState
} from 'app/entities/ioserver/camera-movement-data/camera-movement-data.reducer';
// prettier-ignore
import surveyData, {
  SurveyDataState
} from 'app/entities/ioserver/survey-data/survey-data.reducer';
// prettier-ignore
import fitbitWatchData, {
  FitbitWatchDataState
} from 'app/entities/ioserver/fitbit-watch-data/fitbit-watch-data.reducer';
// prettier-ignore
import fitbitWatchIntradayData, {
  FitbitWatchIntradayDataState
} from 'app/entities/ioserver/fitbit-watch-intraday-data/fitbit-watch-intraday-data.reducer';
import mobileAppSettings, { MobileAppSettingsState } from 'app/entities/mobile-app-settings/mobile-app-settings.reducer';
import healthRecord, { HealthRecordState } from 'app/entities/health-record/health-record.reducer';
import eventRecord, { EventRecordState } from 'app/entities/event-record/event-record.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly userUtils: UserUtilsState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly associate: AssociateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly device: DeviceState;
  readonly deviceAlert: DeviceAlertState;
  readonly userAlert: UserAlertState;
  readonly userExtra: UserExtraState;
  readonly watchData: WatchDataState;
  readonly shoeData: ShoeDataState;
  readonly cameraFitnessData: CameraFitnessDataState;
  readonly cameraMovementData: CameraMovementDataState;
  readonly surveyData: SurveyDataState;
  readonly fitbitWatchData: FitbitWatchDataState;
  readonly fitbitWatchIntradayData: FitbitWatchIntradayDataState;
  readonly mobileAppSettings: MobileAppSettingsState;
  readonly healthRecord: HealthRecordState;
  readonly eventRecord: EventRecordState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  userUtils,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  associate,
  passwordReset,
  password,
  settings,
  device,
  deviceAlert,
  userAlert,
  userExtra,
  watchData,
  shoeData,
  cameraFitnessData,
  cameraMovementData,
  surveyData,
  fitbitWatchData,
  fitbitWatchIntradayData,
  mobileAppSettings,
  healthRecord,
  eventRecord,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
