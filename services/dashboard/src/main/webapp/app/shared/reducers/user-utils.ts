import axios from 'axios';
import { translate, ICrudGetAllAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IDevice } from 'app/shared/model/device.model';
import { IUserAggregated } from 'app/shared/model/user-aggregated.model';

export const ACTION_TYPES = {
  ASSOCIATE_USER: 'userUtils/ASSOCIATE_USER',
  CREATE_USER: 'userUtils/CREATE_USER',
  FETCH_ASSOCIATED_USERS: 'userUtils/FETCH_ASSOCIATED_USERS',
  FETCH_USER_DEVICES: 'userUtils/FETCH_USER_DEVICES',
  WEBSOCKET_CAMERAMOVEMENT_MESSAGE: 'userUtils/WEBSOCKET_CAMERAMOVEMENT_MESSAGE',
  WEBSOCKET_CAMERARECOGNITION_MESSAGE: 'userUtils/WEBSOCKET_CAMERARECOGNITION_MESSAGE',
  RESET: 'userUtils/RESET'
};

const initialState = {
  loading: false,
  associationSuccess: false,
  associationFailure: false,
  creationSuccess: false,
  creationFailure: false,
  errorMessage: null,
  associatedUsers: null as ReadonlyArray<IUserAggregated>,
  devices: null as ReadonlyArray<IDevice>,
  cameraMovement: {} as any,
  cameraRecognition: {} as any
};

export type UserUtilsState = Readonly<typeof initialState>;

// Reducer
export default (state: UserUtilsState = initialState, action): UserUtilsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.ASSOCIATE_USER):
    case REQUEST(ACTION_TYPES.CREATE_USER):
      return {
        ...state,
        loading: true
      };
    case FAILURE(ACTION_TYPES.ASSOCIATE_USER):
      return {
        ...initialState,
        associationFailure: true,
        errorMessage: action.payload.response.data.errorKey
      };
    case FAILURE(ACTION_TYPES.CREATE_USER):
      return {
        ...initialState,
        creationFailure: true,
        errorMessage: action.payload.response.data.errorKey
      };
    case SUCCESS(ACTION_TYPES.ASSOCIATE_USER):
      return {
        ...initialState,
        associationSuccess: true
      };
    case SUCCESS(ACTION_TYPES.CREATE_USER):
      return {
        ...initialState,
        creationSuccess: true
      };
    case REQUEST(ACTION_TYPES.FETCH_ASSOCIATED_USERS):
    case REQUEST(ACTION_TYPES.FETCH_USER_DEVICES):
      return {
        ...state,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ASSOCIATED_USERS):
    case FAILURE(ACTION_TYPES.FETCH_USER_DEVICES):
      return {
        ...initialState,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ASSOCIATED_USERS):
      return {
        ...initialState,
        loading: false,
        associatedUsers: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_USER_DEVICES):
      return {
        ...initialState,
        loading: false,
        devices: action.payload.data
      };
    case ACTION_TYPES.WEBSOCKET_CAMERAMOVEMENT_MESSAGE:
      return {
        ...state,
        cameraMovement: { ...action.payload }
      };
    case ACTION_TYPES.WEBSOCKET_CAMERARECOGNITION_MESSAGE:
      return {
        ...state,
        cameraRecognition: { ...action.payload }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// Actions
// If `isFamily` = true, then associate to a family, otherwise associate to an organization
export const handleAssociatePatient = (uuid, isFamily, isOrganization) => ({
  type: ACTION_TYPES.ASSOCIATE_USER,
  payload: axios.post('api/associate-patient', { uuid, isFamily, isOrganization }),
  meta: {
    successMessage: translate('associate.messages.success')
  }
});

export const handleGeneratePatient = patientData => ({
  type: ACTION_TYPES.CREATE_USER,
  payload: axios.post('api/users/organisation', patientData),
  meta: {
    successMessage:
      patientData.email && patientData.email.length > 0
        ? translate('associate.messages.generate.confirmationSuccess')
        : translate('associate.messages.generate.success')
  }
});

// Get associated patients for current user
export const getAssociatedPatients = sort => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPES.FETCH_ASSOCIATED_USERS,
    payload: axios.get<IUserAggregated>(`api/users/aggregated${sort ? `?sort=${sort}` : ''}`)
  });
};

// Get user devices
export const getUserDevices = (login = '') => async (dispatch, getState) => {
  const requestUrl = `api/user-devices/${login}`;
  await dispatch({
    type: ACTION_TYPES.FETCH_USER_DEVICES,
    payload: axios.get<IDevice>(requestUrl)
  });
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
