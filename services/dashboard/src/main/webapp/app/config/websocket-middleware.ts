import SockJS from 'sockjs-client';

import Stomp from 'webstomp-client';
import { Observable } from 'rxjs'; // tslint:disable-line
import { Observer } from 'rxjs/Observer'; // tslint:disable-line
import { Storage } from 'react-jhipster';

import { ACTION_TYPES as ADMIN_ACTIONS } from 'app/modules/administration/administration.reducer';
import { ACTION_TYPES as AUTH_ACTIONS } from 'app/shared/reducers/authentication';
import { SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ACTION_TYPES as USER_ACTIONS } from 'app/shared/reducers/user-utils';
import { DeviceType } from 'app/shared/model/device.model';

let stompClient = null;

let subscriber = null;
let subscriberCameraMovement = null;
let subscriberCameraRecognition = null;
let connection: Promise<any>;
let connectedPromise: any = null;
let listener: Observable<any>;
let listenerObserver: Observer<any>;
let alreadyConnectedOnce = false;

const createConnection = (): Promise<any> => new Promise((resolve, reject) => (connectedPromise = resolve));

const createListener = (): Observable<any> =>
  new Observable(observer => {
    listenerObserver = observer;
  });

const sendActivity = () => {
  connection.then(() => {
    stompClient.send(
      '/topic/activity', // destination
      JSON.stringify({ page: window.location.hash }), // body
      {} // header
    );
  });
};

const subscribe = cameraDeviceId => {
  connection.then(() => {
    subscriber = stompClient.subscribe('/topic/tracker', data => {
      listenerObserver.next(JSON.parse(data.body));
    });
    if (cameraDeviceId !== -1) {
      subscriberCameraMovement = stompClient.subscribe(`/topic/cameramovement/${cameraDeviceId}`, data => {
        listenerObserver.next(JSON.parse(data.body));
      });
      subscriberCameraRecognition = stompClient.subscribe(`/topic/camerarecognition/${cameraDeviceId}`, data => {
        listenerObserver.next(JSON.parse(data.body));
      });
    }
  });
};

const connect = cameraDeviceId => {
  if (connectedPromise !== null || alreadyConnectedOnce) {
    // the connection is already being established
    return;
  }
  connection = createConnection();
  listener = createListener();

  // building absolute path so that websocket doesn't fail when deploying with a context path
  const loc = window.location;

  const headers = {};
  // let url = '//' + loc.host + loc.pathname + 'websocket/tracker';
  let url = '//' + loc.host + '/websocket/tracker';
  const authToken = Storage.local.get('jhi-authenticationToken') || Storage.session.get('jhi-authenticationToken');
  if (authToken) {
    url += '?access_token=' + authToken;
  }
  const socket = new SockJS(url);
  stompClient = Stomp.over(socket);

  stompClient.connect(
    headers,
    () => {
      connectedPromise('success');
      connectedPromise = null;
      subscribe(cameraDeviceId);
      sendActivity();
      if (!alreadyConnectedOnce) {
        window.onhashchange = () => {
          sendActivity();
        };
        alreadyConnectedOnce = true;
      }
    }
  );
};

const disconnect = () => {
  if (stompClient !== null) {
    stompClient.disconnect();
    stompClient = null;
  }
  window.onhashchange = () => {};
  alreadyConnectedOnce = false;
};

const receive = () => listener;

const unsubscribe = () => {
  if (subscriber !== null) {
    subscriber.unsubscribe();
  }
  if (subscriberCameraMovement !== null) {
    subscriberCameraMovement.unsubscribe();
  }
  if (subscriberCameraRecognition !== null) {
    subscriberCameraRecognition.unsubscribe();
  }
  listener = createListener();
};

export default store => next => action => {
  if (action.type === SUCCESS(USER_ACTIONS.FETCH_USER_DEVICES)) {
    const cameraDevices = action.payload.data.filter(device => device.deviceType === DeviceType.CAMERA_MOVEMENT);
    const cameraDeviceId = cameraDevices.length !== 1 ? -1 : cameraDevices[0].id;
    connect(cameraDeviceId);
    if (!alreadyConnectedOnce) {
      receive().subscribe(activity => {
        if (activity.depthImage) {
          return store.dispatch({
            type: USER_ACTIONS.WEBSOCKET_CAMERAMOVEMENT_MESSAGE,
            payload: activity
          });
        } else if (activity.detectedPersons) {
          return store.dispatch({
            type: USER_ACTIONS.WEBSOCKET_CAMERARECOGNITION_MESSAGE,
            payload: activity
          });
        } else {
          return store.dispatch({
            type: ADMIN_ACTIONS.WEBSOCKET_ACTIVITY_MESSAGE,
            payload: activity
          });
        }
      });
    }
  } else if (action.type === FAILURE(AUTH_ACTIONS.GET_SESSION)) {
    unsubscribe();
    disconnect();
  }
  return next(action);
};
