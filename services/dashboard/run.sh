#!/bin/sh
set -o nounset \
    -o errexit \
    -o verbose \
    -o xtrace

npm run cleanup
# Set environment values if they exist as arguments
if [ $# -ne 0 ]; then
  echo "===> Overriding env params with args ..."
  for var in "$@"
  do
    export "$var"
  done
fi

echo "===> ENV Variables ..."
env | sort

echo "===> User"
id

echo "===> Building ..."
npm run webpack:prod

#docker run --name test -p 9000:9000 -p 9060:9060 -e REACT_APP_ENVIRONMENT=dockerasd -e REACT_APP_TIMEZONE=America/Pacific -e REACT_APP_SERVER_API_URL=https://health.comtrade.com --rm registry.gitlab.com/vinci-aal/dashboarda:test