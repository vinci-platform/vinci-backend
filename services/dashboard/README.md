NOTE: THIS IS README COPYED FROM GATEWAY DASHBOARD AND SHOULD CHANGE.

To start this application first you have to build docker image. This can be done through ci-cd process or you can clone application and build it with(from root project directory where dockerfile is):
docker build -t dashboard:latest . 
Note: Make sure that axios.defaults.baseURL = 'server url, example https://mgdocker.comtrade.com' in index.tsx is set to your server url, but this will likely be changed so it is not hardcoded here. Than you start application with docker-compose up, but make sure it is the correct image in docker-compose.yml file.

Application in container is currently starting in development mode wit webpack, this should be changed so that it can work with production.

TODO: Add better job for testing in gitlab-ci.yml

### Service workers

Service workers are commented by default, to enable them please uncomment the following code.

- The service worker registering script in index.html

```html
<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
        .register('./service-worker.js')
        .then(function() { console.log('Service Worker Registered'); });
    }
</script>
```

Note: workbox creates the respective service worker and dynamically generate the `service-worker.js`

### Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

    npm install --save --save-exact leaflet

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

    npm install --save-dev --save-exact @types/leaflet

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Note: there are still few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

## Building for production

## Testing

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test

For more information, refer to the [Running tests page][].

## Continuous Integration (optional)

The ci-cd is currently not implemented but it will be done with gitlab-ci.yml file in root directory. 