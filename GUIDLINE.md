# vINCI Platform Guidelines

## Contributing

Thank you for your interest in the vInci - elderly care platform! We at [Comtrade 360](https://www.comtrade360.com/) are dedicated to supporting and providing meaningful projects to the open source community. As such we encourage developers to contribute to our projects with providing ways that can help them to get involved and further improve our platform.

To ensure that we are all on the same page regarding the advancement of our projects, we have provided a set of contribution guidelines.

## The Mission

We at vInci strongly believe that the caring and wellbeing for the elderly citizens should be one of the areas in life that would greatly benefit from a continuous effort of providing various software solutions that complement the ambient assisted living domain of [ambient intelligence](https://en.wikipedia.org/wiki/Ambient_intelligence). This is the principal reason why we are open-sourcing our existing solutions to the whole developer community. As a community we can achieve more and provide the most rewarding equal experience for the contributors and consumers alike.

## Platform Status

While you can freely explore the source code for the project in its entirety from just by checking out the desired Git branch, for running the individual applications on your local setup a bit more configuration is needed. For getting the platform services and clients up and running from the current state of the source code, some prerequisites are required.
Third party API keys, cloud service information, specific deployment pipeline settings, testing properties and such should be changed and adapted to your specific needs and configuration options.
All of the third party service API keys required for running the applications should be created on your side and inserted in the appropriate places throughout the source code. While doing that, please make sure not to leak any of the new API keys when submitting pull requests. Pull requests containing sensitive information will be immediately rejected. If you notice code blocks containing sensitive information, please submit a bug fix report so that we can handle the issue in a timely matter.

## Bug Reporting

The most straight-forward way for you to contribute and help us improve this project is by submitting bug reports. We realize that no solution is perfect, but we aim to reach the most satisfactory outcomes for both the developers and the consumers of our product. There is no finite state for this project since it will be ever-evolving and adapting to the latest community needs and requirements set by the most up-to-date development standards.

It should be noted that we cannot guarantee the resolution of every existing bug that will be reported for any particular release build.

## Contributing to the Codebase

Everything revolves around the Code, doesn’t it? One of the easiest way to contribute code to this project is to look into already filed bug and feature reports/requests and start typing code. We are looking forward into reviewing any changes you might submit to the codebase.

A good rule-of-thumb for submitting code is to ensure that the submitting code is small and targeted to specific chunks of the existing codebase. By submitting smaller pieces fo code, we can more efficiently review, provide feedback and incorporate it into the project for any upcoming release.

### Code Styles

To ensure that we are on the same page as far as code styling and formatting is concerned and to avoid endless complications in differentiating what are new and what are existing parts of submitting Git commits, we are providing code styles for every platform segment.

The backend codebase is written entirely in the Java programming language. For the code style we have chosen the official JetBrains [Java code style](https://www.jetbrains.com/help/idea/code-style-java.html#javadoc) included with the [IntelliJ IDE](https://www.jetbrains.com/idea/).

Since the Android client application is written almost entirely in the Kotlin programming language, the contributing code style for that platform segment can be looked at the official [Kotlin style guide](https://developer.android.com/kotlin/style-guide?utm_source=source.android.com&utm_medium=referral).

For the React frontend clients, the code styles are derived from the official Airbnb [React code style](https://airbnb.io/javascript/react/).
